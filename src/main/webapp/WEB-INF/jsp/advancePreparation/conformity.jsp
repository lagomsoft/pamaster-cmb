<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style type="text/css">
._ellipsis {
    width: 200px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td,
    .table>tbody>tr>td, .table>tfoot>tr>td {
    padding: 11px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: none;
}

.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 10px;
}

tr.highlight td.jsgrid-cell {
    background-color: #238192;
    color : white;
}

</style>

<script src="/js/PAMsSelectPub.js"></script>
<script src="/js/PAMsAplDetail.js"></script>

<script type="text/javascript">

    var projectNo = null;
    var projectStep = 0;
    
    var nowDe = "${NOW_DATE}";
    
    var isChange = false;
    
    var isDataUploaded = false;
    
    var isInfo = false;
    
    var tmpDivNum = 0;

    var checkRecord = 0;

    var projectFlTyCd = "";
    
    var fileNo = 0;
    var prjctType = "";
    var stepCd = "";
    var _uploadDataInfoList = new Array();
    <%-- 새로 추가 한 것--%>
    var isNew = false;
    
    var updateColumnList = new Array();
    
    var isBindKey = false;
    
    function initPage() {
        $(".input-val").val("");
        
        $("#APL_LIST_GRID").jsGrid("option", "data", []);
        $("#APL_INFO_DIV").hide();
        $("#APL_LIST_DIV").show();
        $("#PROJECT_INFO_DIV").show();
        updateColumnList = [];
        isChange = false;
        isNew = false
        $("#CND_DT").text("${NOW_DATE}");
    }

    $(function() {
        loadPAMsProjectSelect("PROJECT_SELECT_DIV", 100);

        $("#PAMS_PROJECT_SELECT_BTN").on("click",function() {

            if ($("#PAMS_PROJECT_SELECT").val() == null || $("#PAMS_PROJECT_SELECT").val() == "") {
                alert('<spring:message code="common.038" text="프로젝트를 선택하세요." />');
            } else {

                if (isChange && !confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')) {
                    return;
                }
                isBindKey = false;
                projectNo = $("#PAMS_PROJECT_SELECT").val();
                projectStep = $("#PAMS_PROJECT_SELECT option:checked").attr("step");
                
                if(projectStep>= 150){
                	isBindKey = true;
                }
                if(projectStep<300){
                    prjctType = "K";
                    stepCd = "150"
                }else{
                    prjctType = "D";
                    stepCd = "350"
                }

                
                initPage();
                getProjectData();
                //aplList.init(projectNo, prjctType);
            }
        });
        
        $("#NEW_BTN").on("click",function() {
            if (isChange && !confirm('<spring:message code="common.105" text="이미 신규 작업을 진행중입니다. 작업을 초기화 하시겠습니까?" />')) {
                return;
            }
            // TODO 신규 버튼 클릭시 프로젝트를 선택하여 주십시오.
            $("#PAMS_PROJECT_SELECT").val("");
            $(".ts-control").children("div:first").text("");
            initPage();
            
            projectNo = null;
            projectStep = 0;
            
            $(".load-data").hide();
            $(".init-data").show();

            $("#PROJECT_INFO_DIV").find("input").val("");   

            $("#BND_DIV").hide();

            $("#ADD_APL_BTN").show();
            $("#TMP_SAVE_BTN").show();
            $("#SAVE_BTN").show();
            $(".addCol").show();
            $(".saveInfo").hide();
            $(".input-val").attr("disabled", false);
            
            if($("#BIND_YN").is(":checked")){
                $("#BIND_YN").click();
            }
            
            isNew = true;
            
            aplList.init(0, "K");
        });
        
        $("#PROJECT_INFO_DIV").on("change", "select,input", function(){
            isChange = true;
        });
        
        $("#SAVE_BTN").on("click",function() {

            <%-- 프로젝트를 선택하지 않은 경우 --%>
            if(!isNew && !projectNo) {
                alert('<spring:message code="common.038" text="프로젝트를 선택하세요." />');
                return;
            }

            <%-- 프로젝트명 혹은 프로젝트 상세목적을 기입하지 않은 경우 --%>
            if(!checkProjectData()){
                return;
            }

            <%-- 결합신청자 개수가 적은 경우 --%>
            let aplCount = $("#APL_LIST_GRID").jsGrid("option", "data").length; <%-- 결합신청자 개수 --%>
            if (aplCount<2){
            	alert('2개 이상의 결합신청자가 존재해야 합니다. 결합신청자를 추가하십시오.');
            	return;
            }

            if (aplCount>5){
                alert('6개 이상의 결합신청자는 이용할 수 없습니다. 결합신청자를 삭제하십시오.');
                return;
            }

            <%-- 결합신청자마다 데이터 업로드를 하지 않은 경우 --%>
            let isUpload = true;
            var $needUpload = null;
            $("#APL_LIST_DIV").find('[class="UPL_CNT"]').each(function(idx){
            	if(isUpload){
            		if($(this).text() == '') {
                        isUpload = false;
                    }
            		$needUpload = $(this);
            	}
            });
            if(!isUpload){
            	alert('모든 결합신청자가 데이터를 업로드해야합니다.');
                _aplDetailPopup.setClickTabSeq(1); <%-- 데이터 업로드 클릭하도록 --%>
                $needUpload.click();
            	return;
            }

            <%-- 결합신청자마다 결합키를 지정했는지 여부 --%>
            let aplInfoList = $("#APL_LIST_GRID").jsGrid("option", "data"); <%-- 결합신청자목록 리스트 --%>
            let aplBindKeyMap = new Map(); <%-- APL_NO를 Key 값으로,  결합키가 있는 경우(_bind),  데이터를 확인해야하는 경우(_datacheck) --%>
            aplInfoList.forEach((element, index) => {
                aplBindKeyMap.set(element.APL_NO+'_bind', false); <%-- 해당 결합신청자의 결합키가 있을 경우 true로 바꿈 --%>
                aplBindKeyMap.set(element.APL_NO+'_data', false); <%-- 해당 결합신청자의 데이터가 있을 경우 true로 바꿈 --%>
                aplBindKeyMap.set(element.APL_NO+'_bindKeyCount', 0); <%-- 해당 결합신청자의 결합키 개수 --%>
            });

            _uploadDataInfoList = []; <%-- 전처리 할 데이터 리스트(전역변수) 초기화 --%>
            updateColumnList.forEach((element, index) => {
                updateColumnList[index].updateColumn.forEach((element2, index2) => {
                    let aplNo = element2.APL_NO;
                	if(element2.OBJ_TY=="BIND"){ <%-- 결합키가 있는 경우 --%>
                        aplBindKeyMap.set(aplNo+'_bind', true); <%-- 결합키 있음 체크 --%>
                        aplBindKeyMap.set(aplNo+'_bindKeyCount', aplBindKeyMap.get(aplNo+'_bindKeyCount')+1); <%-- 해당 결합신청자의 결합키 개수 --%>
                	}
                    _uploadDataInfoList.push(element2);
                    aplBindKeyMap.set(aplNo+'_data', true); <%-- 데이터 있음 체크 --%>
                })
            });
            let errorCount = 0;
            aplInfoList.forEach((element, index) => {
                if(errorCount === 0){
                    if(aplBindKeyMap.get(element.APL_NO+'_data') == false) { <%-- 해당 결합신청자의 데이터가 없는 경우 --%>
                        alert("전처리 실행 전, 결합신청기관 '"+element.APL_CT_NM+"'의 데이터를 데이터를 확인하고, 데이터가 정상적이라면 저장 및 다음단계 버튼을 눌러주세요.");
                        aplInfoDataClick(element.APL_NO);
                        errorCount++;
                        return;
                    }
                    if(aplBindKeyMap.get(element.APL_NO+'_bind') == false) { <%-- 해당 결합신청자의 결합키가 없는 경우 --%>
                        alert("결합신청기관 '"+element.APL_CT_NM+"'의 데이터에 결합키가 존재하지 않습니다. 데이터를 확인 후 결합키를 추가하여 주십시오.");
                        aplInfoDataClick(element.APL_NO);
                        errorCount++;
                        return;
                    }
                    if(aplBindKeyMap.get(element.APL_NO+'_bindKeyCount') > 1) { <%-- 해당 결합신청자의 결합키가 둘 이상 존재하는 경우 --%>
                        alert("결합신청기관 '"+element.APL_CT_NM+"'의 데이터에 결합키로 설정된 항목이 "+aplBindKeyMap.get(element.APL_NO+'_bindKeyCount')+'개 존재합니다. 각 결합신청기관마다 결합키를 하나로 맞춰주십시오.');
                        aplInfoDataClick(element.APL_NO);
                        errorCount++;
                        return;
                    }
                }

            });
            if(errorCount > 0) {
                return;
            }

            saveProjectData(false);
        });

        //결합신청자 추가 버튼 클릭 시
        $("#ADD_APL_BTN").on("click",function() {

            if(isNew){ <%-- 새로운 프로젝트인 경우 결합신청자 추가 전 프로젝트 정보를 저장함 --%>
            
                var result = checkProjectData();
                if(result){
                    saveProjectData(true);
                    isNew =false;
                    return true;
                } else {
                    return false;
                }
            }
            aplList.addNewApl();
            
            $("#APL_INFO_DIV").show();
        });
        
    });
    
    function getProjectData() {
        
        $(".init-data").hide(); 
        $(".load-data").show();
        
        $(".load-data").empty();
        
        $("#PROJECT_INFO_DIV").show();
        $("#progressdiv").show();
        
        $(".input-val").attr("disabled", false);
        
        $.ajax({
            url: "/advancePreparation/getConformity",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : projectNo 
            })
        }).done(function(response) {
            if(response.PROJECT_INFO) {
                aplList.init(projectNo, prjctType);
                
                for(var key in response.PROJECT_INFO){
                    $("#" + key).val(response.PROJECT_INFO[key]);
                    $("#" + key).append(response.PROJECT_INFO[key]);
                }
                $("#SAVE_BTN").show();
                $(".saveInfo").hide();
                
                if(Number(response.PROJECT_INFO.STEP_CD) > 130){
                    $("#SAVE_BTN").hide();
                    
                    $(".apl-btn").hide();
                    $(".input-val").attr("disabled", true);

                } else {

                	$(".apl-btn").show();
                	$(".input-val").attr("disabled", false);
                }
                
                if(Number(response.PROJECT_INFO.STEP_CD) >= 300 && Number(response.PROJECT_INFO.STEP_CD) <= 330){
                    $("#SAVE_BTN").show();
                }                
                
            }
        });
    }

    function checkProjectData(){

        var isCheckVal = true;
        var errorCount = _required.requiredCheck($("#PROJECT_INFO_DIV"));

        if(errorCount > 0) {
            return false;
        }

        return isCheckVal;
    }
    
    function popupUbiReportDownload() {<%-- shchae --%>
    //var url = "/common/paPopupUbiReport?PRJCT_NO="+ encodeURI(projectNo);
    var url = "/ubi4/ubihtml.jsp";
    var name = "popupUbiReport";
    var option = 
                "width=917"
               + ",height=750"
               + ",top 100"
               + ",left=800"
               + ",resizable=false"
           
    window.open(url, name, option);
    }
    
    <%-- 신규 프로젝트에서 프로젝트 정보 입력 후 결합신청자 추가 버튼을 누른 경우 일부 화면 스킵 --%>
    function saveProjectData(_isNew) {

        if(!_isNew) {
            if (!confirm('프로젝트 전처리 진행 시 프로젝트 정보 및 결합신청자의 정보를 변경할 수 없습니다. \n다음 단계로 진행하시겠습니까?')) {
	            return;
	        }
        }
        $.ajax({
            url : "/advancePreparation/saveProjectInfo",    
            type : "post",
            contentType : 'application/json',
            data : JSON.stringify({
                 PRJCT_NO           : projectNo
                ,PRJCT_TY_CD        : prjctType
                ,PRJCT_NM           : $("#PRJCT_NM").val()
                //,PRJCT_STEP         : projectStep
                ,PRJCT_DTL          : $("#PRJCT_DTL").val()
                ,UPLOAD_DATA_LIST   : _uploadDataInfoList
                ,STEP_CD            : stepCd
            })
        }).done(function(response) {
        	
            if (response) {
                if(response.RETURN_CODE == 1){
                	alert('각 데이터 별 하나의 결합키가 필요합니다.');
                	return;
                }
                if(response.RETURN_CODE == 2){
                    alert('결합키를 두 개 이상 설정할 수 없습니다.');
                    return;
                }
                if(_isNew) {
                	loadPAMsProjectSelect("PROJECT_SELECT_DIV", 100);
                	
                    pamsSelectPrjctNo = response.SUCCESS.PRJCT_NO;
                    pamsSelectStepCd = response.SUCCESS.STEP_CD;
                    projectNo = pamsSelectPrjctNo;
                    projectStep = pamsSelectStepCd;
                   
                    aplList.addNewApl();
                    
                    if(projectStep<300){
                        prjctType = "K";
                        stepCd = "150"
                    }else{
                        prjctType = "D";
                        stepCd = "350"
                    }
                    
                    pamsSelectPrjctNo = null;
                    pamsSelectStepCd = "";
                    return;
                }
                alert('<spring:message code="common.013" text="저장 되었습니다." />');
                
                location.href = "../advancePreparation/dashboard";
                
            }

        });
        
    }

    <%--결합신청자 목록 획득(ajax)--%>
    var aplList = {
    	selectedItem : null,
    	selectedRow : null,
        settings : {
            autoload : false,
            width : "100%",
            height : "auto",
            altRows : "false",
            sorting : true, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args) {
            	aplList.selectedItem = args.item;
            	aplList.selectedRow = args;
                <%--이전 선택 row highlight 제거--%>
                var selectedRow = $("#APL_LIST_GRID").find('table tr.highlight');
                selectedRow.removeClass('highlight');
                
                <%--현재 선택 row highlight--%>
                var $row = this.rowByItem(args.item);
                $row.toggleClass("highlight");
                
                var getData = args.item;
                _aplDetailPopup.addDetail(getData["PRJCT_NO"],getData["APL_NO"],projectStep,'#APL_INFO_GRID_DIV');
                $("#APL_INFO_DIV").show();
            },      
            controller : {
                loadData : {}
            },
            fields : [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
            {
                name : "APL_CT_NM",
                title : "<spring:message code='project.001' text='결합신청기관' />",
                type : "text",
                width : 50
            }, {
                name : "APL_CHRG",
                title : "<spring:message code='project.002' text='결합신청자명' />",
                type : "text",
                width : 50
            }, {
                name : "FL_NM",
                title : "<spring:message code='project.003' text='업로드파일명' />",
                type : "text",
                width : 75,
                itemTemplate: function (_, item) {
                    var FL_NM = item.FL_NM;
                    if(FL_NM != null) {
                        return $('<div id="FL_NM_'+item.APL_NO+'">' + FL_NM + '</div>');
                    }
                    return $('<div id="FL_NM_'+item.APL_NO+'">' + '</div>');
                }
            }, {
                name : "UPL_CNT",
                title : "<spring:message code='project.004' text='업로드데이터건수' />",
                type : "text",
                width : 50,
                itemTemplate: function (_, item) {
                    var UPL_CNT = item.UPL_CNT;
                    if(UPL_CNT != null) {
                    	return $('<div id="FL_UPL_'+item.APL_NO+'" class="UPL_CNT" data-aplno='+ item.APL_NO +' >' + UPL_CNT + '</div>');
                    }
                    return $('<div id="FL_UPL_'+item.APL_NO+'"  class="UPL_CNT" data-aplno='+ item.APL_NO +' >' + '</div>');
                }
            }, {
                name : "PRJCT_NO",
                type : "text",
                css : "d-none",
                width : 0
            }, {
                name : "APL_NO",
                type : "text",
                css : "d-none",
                width : 0,
                itemTemplate: function (_, item) {
                    var aplNo = item.APL_NO;
                    return $('<div class="APL_NO APL_NO_'+ aplNo +' d-none" value="'+aplNo+'">' + aplNo+ '</div>');
                }
            }, {
                name : "IS_NEW",
                type : "text",
                css : "d-none",
                width : 0
            } ]
        }
        ,init : function(proNo, fileTy){
            var totalSettings = $.extend({},aplList.settings);
            totalSettings.autoload = true;
            totalSettings.controller.loadData = function() {
                var d = $.Deferred();
                $.ajax({
                    url : "/advancePreparation/getAplInfoList",
                    isHideOverlay : true,
                    type : "post",
                    contentType : 'application/json',
                    data : JSON.stringify({
                        PRJCT_NO : proNo
                        ,FL_TY_CD : fileTy
                    })

                }).done(function(response) {
                	d.resolve(response);
                });
                
                return d.promise();
            }               
            totalSettings.onDataLoaded = function(){
                //var gridData = JSON.parse(JSON.stringify($("#APL_LIST_GRID").jsGrid("option", "data")));
            }   
            $("#APL_LIST_GRID").jsGrid(totalSettings);
            
        }
        ,addNewApl : function(){ <%-- 결합신청자 추가 버튼을 누른 경우 --%>
            var newItem = {
                "APL_CT_NM" : "신규 결합신청기관"
               ,"APL_CHRG" : "신규 결합신정자"
               ,"PRJCT_NO" : projectNo
               ,"APL_NO" : "0"
               ,"IS_NEW" : "Y"
            }
            $("#APL_LIST_GRID").jsGrid("insertItem", newItem).done(function() {
                $row = $("#APL_LIST_GRID").jsGrid("rowByItem", newItem);
                $row.click();
            });
        }
    }

    <%-- 해당 aplNo에 해당하는 그리드 로우의 데이터를 클릭하게함 --%>
    function aplInfoDataClick(aplNo){
        var $targetRow = null;
        $("#APL_LIST_DIV").find('.APL_NO').each(function(idx){
            if($(this).text() == aplNo) {
                $targetRow = $(this);
            }
        });
        if($targetRow){
            _aplDetailPopup.setClickTabSeq(1); <%-- 데이터 업로드 클릭하도록 --%>
            $targetRow.click();
            return;
        }
    }

</script>

<section>
    <div id="PROJECT_SELECT_DIV"></div>

    <div class="col-xs-12 btn-area">
        <button id="NEW_BTN" type="button" auth-role="200" class="btn btn-black">
            <spring:message code="common.015" text="신규" />
        </button>
        &nbsp;
        <button id="UbiREPORT_DOWN_BTN" type="button" class="btn btn-warning save-btn" style="" onclick="popupUbiReportDownload();">
                <i class="fa fa-file-excel-o">
                    보고서(Ubi)
                </i>
            </button> &nbsp;
        <%-- 
        <button id="TMP_SAVE_BTN" type="button" auth-role="200" class="btn btn-dark">
            <spring:message code="common.077" text="저장" />
        </button>
        &nbsp;
        --%>
        <button id="SAVE_BTN" type="button" auth-role="200" class="btn btn-primary">
            <spring:message code="common.016" text="저장 및 다음단계" />
        </button>
    </div>

    <div id="PROJECT_INFO_DIV" style="display: none;">
        <div class="x_panel x_result">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_title">
                        <h2>
                            <spring:message code="conformity.058" text="프로젝트 정보" />
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="sub_info x_underline">
                        <spring:message code="conformity.081" text="개인정보처리자는 정당한 처리 범위 내에서 통계작성, 과학적 연구, 공익적 기록보존 등의 목적으로 정보주체의 동의 없이 가명정보를 처리 할 수 있습니다. 가명정보 처리 프로젝트의 기본 정보와 법률이 허용하는 목적 내에서 가명처리 목적을 상세히 작성하여 주십시오. <br> ※ 선택하는 사용 유형에 따라서 입력 항목이 다르게 제시됩니다. " />
                        <br>
                        <br>
                    </p>
                </div>
            </div>
            <div class="x_content">
                <div class="row">
                    <table class="table margin_b0">
                        <colgroup>
                            <col width="10%">
                            <col width="40%">
                            <col width="10%">
                            <col width="40%">
                            <%--
                            <col width="7%">
                            <col width="13%">
                             --%>
                        </colgroup>
                        <tbody>
                            <tr style="display:none;">
                                <td><b><spring:message code="conformity.059" text="프로젝트번호" /> :</b></td>
                                <td>
                                    <div id="DIV_ALIAS_NO" class="load-data"></div>
                                    <div class="init-data">
                                        <spring:message code="conformity.106" text="자동부여" />
                                    </div>
                                </td>

                                <td><b><spring:message code="conformity.084" text="등록일자" /> :</b></td>
                                <td colspan="3">
                                    <div id="DIV_RGSDE" class="init-data load-data"></div>
                                </td>
                            </tr>
                            <tr>
                                <td><b><spring:message code="conformity.060" text="프로젝트명" /> (*) :</b></td>
                                <td><input id="PRJCT_NM" type="text" class="form-control input-val" placeholder='<spring:message code="conformity.060" text="프로젝트명" />' required maxlength="60"></td>
                                
                                <td><b><spring:message code="conformity.084" text="등록일자" /> :</b></td>
                                <td><div id="CND_DT" class="init-data load-data"></div></td>
                            </tr>
                            <tr>
                                <td><label for="inputDescription"><spring:message code="project.016" text="프로젝트 상세목적" /> (*) : </label>&nbsp;&nbsp;<br>
                                <spring:message code="conformity.104" text="(상세하게 기록)" /></td>
                                <td colspan="3"><textarea id="PRJCT_DTL" class="form-control input-val" rows="3" maxlength="1000" required></textarea></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    <div id="APL_LIST_DIV" style="display:none;">
        <div class="x_panel x_result">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_title x_underline">
                        <h2>
                            <spring:message code="project.005" text="결합신청자 목록" />
                        </h2>
                        <button type="button" id="ADD_APL_BTN" class="btn btn-dark float_r m-b-10 apl-btn">
                            <spring:message code="project.006" text="결합신청자 추가" />
                        </button>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
            <div id="APL_LIST_GRID" class="jsgrid"></div>
        </div>
    </div>


    <div id="APL_INFO_DIV" class="x_panel x_result" style="display:none;">
        <div class="row">
            <div class="col-xs-12">
                <div class="x_title x_underline">
                    <h2 id="APL_INFO_TITLE">
                        결합신청자 정보
                    </h2>
                    <button type="button" id="DLT_APL_INFO_BTN" class="btn btn-danger float_r m-b-10 apl-btn">
                        <spring:message code="project.013" text="결합신청자 삭제" />
                    </button>
                    <br>
                    <br>
                </div>
            </div>
            <div class="col-xs-12">
                <div id="APL_INFO_GRID_DIV" class="x_content">
                    
                </div>
            </div>
        </div>
    </div>

</section>

