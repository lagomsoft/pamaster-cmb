<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style type="text/css">
._ellipsis {
	width: 200px;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}

.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td,
	.table>tbody>tr>td, .table>tfoot>tr>td {
	padding: 11px;
	line-height: 1.42857143;
	vertical-align: top;
	border-top: none;
}

.table {
	width: 100%;
	max-width: 100%;
	margin-bottom: 10px;
}
</style>

<script src="/js/PAMsSelectPub.js"></script>

<script type="text/javascript">

    var mappingMap = {
        <c:forEach items="${MAPPING_LIST}" var="mapping" varStatus="status">
            <c:if test="${!status.first}">,</c:if>${mapping.CD} : "${mapping.NM}"
        </c:forEach>
    };
    
    var projectNo = null;
    var projectStep = null;
    var projectRvwsn = null;

    var setTimeoutValue = false;
    $(function () {
        
        loadPAMsProjectSelect("PROJECT_SELECT_DIV", 130);
        
        $("#PAMS_PROJECT_SELECT_BTN").on("click", function(){
            if($("#PAMS_PROJECT_SELECT").val() == null || $("#PAMS_PROJECT_SELECT").val() == ""){
                alert('<spring:message code="common.038" text="프로젝트를 선택하세요." />');
            }else{
                
                $("#AFTER_ROW_DATA_DIV").hide();
                $("#REQUEST_BTN").hide();
                $("#ORG_DATA_DIV").show();
                
                projectNo = $("#PAMS_PROJECT_SELECT").val();
                projectStep = $("#PAMS_PROJECT_SELECT option:checked").attr("step");
                projectRvwsn = $("#PAMS_PROJECT_SELECT option:checked").attr("rvwSn");
                
                <c:if test='${IS_SYS_ADMIN == "Y" or  AUTH_CD.contains("200")}'>
                if(Number( projectStep ) < 400){
                    $(".save-btn").show();
                }else{
                    $(".save-btn").hide();
                }
                </c:if>
                
                if(Number( projectStep ) < 400){
                    $("#DATA_DISPLAY").hide();
                }else{
                    $("#DATA_DISPLAY").show();
                }
                
                <c:if test='${IS_SYS_ADMIN == "Y" or  AUTH_CD.contains("300")}'>
                $(".start-btn").hide();
                </c:if>
                getAplInfo();
                
                if(Number( projectStep ) >= 130 && Number( projectStep ) < 330){
                	getProjectData(projectNo, 150);
                }else {
                	getProjectData(projectNo, 350);
                }
                

            }
        });
        
        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "auto",
            sorting: false, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            controller: {
                loadData: function() {
                    var d = $.Deferred();
                    $.ajax({
                         url: "/advancePreparation/getMatchCnt"
                        ,type : "post"
                        ,contentType: 'application/json'
                        ,data : JSON.stringify({
                            PRJCT_NO : projectNo
                        })
                    }).done(function(response) {
                    	d.resolve(response);
                    });
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                { name: "APL_CT_NM", title:'<spring:message code="dashboard.007" text="결합신청자" />', type: "text", align : "center", width: 30 }
                ,{ name: "APL_CHRG", title:'<spring:message code="dashboard.006" text="담당자" />', type: "text", align : "center", width: 30 }
                ,{ name: "BIND_CNT", title:'<spring:message code="dashboard.004" text="결합키 건수" />', type: "text", align : "center", width: 30 }
                ,{ name: "MATCH_CNT", title:'<spring:message code="dashboard.005" text="불일치 결합키 건수" />', type: "text", align : "center", width: 30 }
            ]
        });
        
    });
    <%-- 데이터 비교 그리드 --%>
    function getAplInfo(){
    	$("#jsGrid1").jsGrid("loadData");
    }
    
    function getProjectData(_proNo, _proStep){
    	$("#ORG_DATA_DIV").show();
        $.ajax({
            url: "/advancePreparation/getProgressCode"
            ,type : "post"
            ,contentType: 'application/json'
            ,isHideOverlay : true
            ,data : JSON.stringify({
                 PROJECT_NO : _proNo
                ,PROJECT_STEP : _proStep
            })
        }).done(function(response) {
    	    if(response.SCHEDULE_MAP){
    	    	
                var progrsCd = Math.floor(response.SCHEDULE_MAP.SCHEDULE_PROGRS_CD);
                //var scheduleRvwSn = response.SCHEDULE_MAP.SCHEDULE_RVW_SN; 현재 미사용
                var progrsList = response.SCHEDULE_LIST;
                if(progrsCd === 900) {
                    $("#NEXT_BTN").show();
                    $("#PREV_BTN").show();
                } else {
                    $("#NEXT_BTN").hide();
                    $("#PREV_BTN").hide();
                }

                switch (progrsCd) {
                    case 100:
                        $("#PREPROCESSING_PROGRESS").css("width", "10%");
                        $("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dashboard.008" text="데이터 전처리 요청" />');
                        if(!setTimeoutValue) {
                            setTimeoutValue = setTimeout(function() {
                                setTimeoutValue = false;
                                    getProjectData($("#PAMS_PROJECT_SELECT").val());
                                }, 50 * 60);

                        }
                        break;
                    case 900:
                        $("#PREPROCESSING_PROGRESS").css("width", "100%");
                        $("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dashboard.002" text="데이터 전처리 완료" />');
                        
                        if(Number( projectStep ) >= 330) {
                        	$("#DATA_DISPLAY").show();         
                        	getAplInfo();
                        }
                        break;
                    default:
                        <%-- 오류발생시 --%>
                        console.log("test default");
                        let err = false;
                        let errMsg = "";
                        progrsList.forEach((element, index) => {
                            if(progrsList[index].PROGRS_CD === 999) {
                                err = true;
                                if(errMsg.length > 0) errMsg += ", ";
                                errMsg += progrsList[index].APL_CT_NM;
                            }
                        })
                        if(err) {
                            $("#PREPROCESSING_PROGRESS").css("width", "100%");
                            $("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dashboard.001" text="데이터 전처리 오류 발생" />, 해당 결합신청자의 데이터를 확인해주세요 : '+errMsg);
                            
                            $("#DATA_DISPLAY").show();
                            
                            break;
                        }
                        <%-- 전처리 도중인 경우 --%>
                        var percent = Number(progrsCd) / 10;
                        if(Number(progrsCd) >=200) {
                            if(!setTimeoutValue) {
                                setTimeoutValue = setTimeout(function() {
                                    setTimeoutValue = false;
                                        getProjectData($("#PAMS_PROJECT_SELECT").val());
                                    }, 50 * 60);

                            }
                        }
                        if(Number(progrsCd) < 900) {
                            $("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dashboard.003" text="데이터 전처리 진행중" />');
                        } else if (Number(progrsCd) == 999) {
                        	$("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dashboard.001" text="데이터 전처리 오류 발생" />');
                        }

                        $("#PREPROCESSING_PROGRESS").css("width", percent + "%");
                        //$("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dataPreprocessing.001" text="데이터 전처리" /> ' + percent + "%" + ' <spring:message code="dataPreprocessing.025" text="완료" />');
                        break;
                }
            }else{
                $("#PREPROCESSING_PROGRESS").css("width", "0%");
                $("#PREPROCESSING_PROGRESS_TITLE").text('<spring:message code="dashboard.009" text="데이터 전처리 요청 전" />');
            }

       });

    }
    


</script>

<section>
	<div id="PROJECT_SELECT_DIV"></div>
	<div class="col-xs-12 btn-area">
        <button type="button" auth-role="300" class="btn btn-primary start-btn" style="display: none;" onclick="startDataPreprocessing('LATER');">
            <spring:message code="dashboard.010" text="스케줄처리" />
        </button>
        &nbsp;&nbsp;
        <button type="button" auth-role="300" class="btn btn-primary start-btn" style="display: none;" onclick="startDataPreprocessing('NOW');">
            <spring:message code="dashboard.011" text="스케줄 즉시처리" />
        </button>
        <button id="PREV_BTN" type="button" auth-role="200" class="btn btn-dark" style="display: none;" onclick="location.href='/advancePreparation/conformity'">
            <spring:message code="common.115" text="이전단계" />
        </button>
        &nbsp;
        <button id="NEXT_BTN" type="button" auth-role="200" class="btn btn-primary" style="display: none;" onclick="location.href='/bind/prebindDefinition'">
            <spring:message code="common.114" text="다음단계" />
        </button>
    </div>
	   <div id="ORG_DATA_DIV" class="x_panel x_result" style="display: none;">
        <div class="row">
            <div class="col-xs-12">
                <div class="x_title">
                    <h2>
                        <spring:message code="dashboard.012" text="전처리 진행 상태" />
                    </h2>
                    
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-12">
                <p class="sub_info">
                    <spring:message code="dashboard.013" text="데이터 전처리" />
                    :
                    <spring:message code="dashboard.014" text="가명처리할 대상으로 선정한 항목들에 대한 데이터 정제작업을 수행합니다. 가명처리 승인자는 가명처리 담당자가 신청한 전처리 내용을 확인하고 전처리 스케쥴 실행하여 주십시오. <br> ※ 대량의 데이터의 경우 많은 시간이 소요될 수 있습니다. " />
                </p>
            </div>
        </div>
        <div class="x_content">
            <div class="progress-area">
                <div class="progress progress_sm">
                    <div id="PREPROCESSING_PROGRESS"
                        class="progress-bar progress-bar-success progress-bar-striped"
                        role="progressbar" aria-valuenow="40" aria-valuemin="0"
                        aria-valuemax="100" style="width: 50%">
                        <span class="sr-only"></span>
                    </div>
                </div>
                <p id="PREPROCESSING_PROGRESS_TITLE" class="txt_c"></p>
            </div>
        </div>
    </div>
    <div id="DATA_DISPLAY" style="display: none;">
        <div class="x_panel x_result">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_title">
                        <h2>
                            데이터 비교
                        </h2>
                    </div>
                </div>
                <div class="col-xs-12">
                    결합담당자가 결합신청자에게 요청한 결합키와 가장 최근 업로드한 결합키가 일치하는지 확인합니다.
                </div>
            </div>
            <br/>
            <hr>
            <div class="col-xs-12">
                <div class="scroll_v">
                    <div id="jsGrid1" class="jsgrid"></div>
                </div>
            </div>
        </div>
    </div>
</section>

