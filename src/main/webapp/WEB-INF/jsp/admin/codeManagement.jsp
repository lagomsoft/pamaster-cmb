<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<style type="text/css">
	._ellipsis {
	      overflow:hidden;
	      text-overflow:ellipsis;
	      white-space:nowrap;
	}
	.input-group {
		position: relative;
		display: flex;
		border-collapse: separate;
		align-items: stretch;
	}
	.d-none{
		display:none !important;
	}
	.select-span{
		width: 15%;
    	margin-block: auto;
    	font-weight: bold;
	}

</style>

<script type="text/javascript">
	
    // 수정 리스트
    var updateGrid1List = new Array();
    var updateGrid2List = new Array();
    var newGrid1List = new Array();
    var newGrid2List = new Array();
    
    //선택 로우
    var selectGrid1Row;
    var selectGrid2Row;
    var isNewRow1 = false;
    var isNewRow2 = false;
    var isUpdateRow1 = false;
    var isUpdateRow2 = false;
    
    var da = {
		data : new Array(),
	 };
    var _searchedValue = {
		CATEGORY : "",
		INPUT : ""
	}
	var da2 = {
		data : new Array(),
	 };
    var _searchedValue2 = {
		CATEGORY : "",
		INPUT : ""
	}
    
    $(function () {
        
        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "400px",

            editing: true,
            sorting: true, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args){
                if(isUpdateRow1){
    	            isUpdateRow1 = false;
    	            $("#jsGrid1").jsGrid("updateItem");
                }
                
                $("#jsGrid1").jsGrid("cancelEdit");
                
                if(updateGrid2List.length > 0 || newGrid2List.length > 0){
                	if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
                    	return;
                	}
                }
                
                initGrid2();
                
                if(args.item.ORG_GRP_CD){
                    $("#CODE_DIV").show();
	                $("#SELECT_GROUP_CD").val(args.item.ORG_GRP_CD);
	                $("#SELECT_GROUP_NM").html(args.item.GRP_NM + "  [" + args.item.ORG_GRP_CD + "]");
	                searchCodeList2();
                }
            },
            rowDoubleClick : function(args){
            	selectGrid1Row = args.itemIndex;
            	this.option("sorting", false);
            	this.editItem($(this._body).find("tr").eq(selectGrid1Row));
            	if(isNewRow1){
            		newGrid1List[selectGrid1Row] = 1;
                }
            	isNewRow1 = false;
            	$(this._body).find("tr").eq(selectGrid1Row).find("input").eq(0).focus();
            },
            controller: {
            	loadData: function(args) {
            		searchFieldListInit2();
                    var d = $.Deferred();
                    d.resolve(da.data);
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                 {name : "GRP_CD", title : "<spring:message code='codeManagement.002' text='그룹코드' />", type: "text", width: 100, isSearch: "Y"}
                ,{name : "GRP_NM", title : "<spring:message code='codeManagement.003' text='그룹명' />", type: "text", width: 200, isSearch: "Y"}
                ,{name : "GRP_DC", title : "<spring:message code='codeManagement.004' text='설명' />", type: "text", width: 100, isSearch: "Y"}
                ,{name : "USE_YN", title : "<spring:message code='codeManagement.005' text='사용여부' />", type: "select", items : [
	                    { Name: "<spring:message code='codeManagement.006' text='사용' />", Id: "Y" },
	                	{ Name: "<spring:message code='codeManagement.007' text='미사용' />", Id: "N" }
                    ], valueField: "Id", textField: "Name", width: 100 }
                ,{name : "ORG_GRP_CD", type: "text", css:"d-none", width: 0 }
            ]
        });
        
        $("#jsGrid2").jsGrid({
            width: "100%",
            height: "400px",
            
            editing: true,
            sorting: true, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args){
                if(isUpdateRow2){
    	            isUpdateRow2 = false;
    	            $("#jsGrid2").jsGrid("updateItem");
                }
                
                $("#jsGrid2").jsGrid("cancelEdit");
                
            },
            rowDoubleClick : function(args){

            	selectGrid2Row = args.itemIndex;
            	this.option("sorting", false);
            	this.editItem($(this._body).find("tr").eq(selectGrid2Row));
            	
            	if(isNewRow2){
            		newGrid2List[selectGrid2Row] = 1;
                }
            	isNewRow2 = false;
            	$(this._body).find("tr").eq(selectGrid2Row).find("input").eq(0).focus();
            },
            controller: {
            	loadData: function(args) {
                    var d = $.Deferred();
                    d.resolve(da2.data);
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                 { name:"CD", title: "<spring:message code='codeManagement.008' text='코드' />", type: "text", width: 150, isSearch: "Y"}
                ,{ name:"CD_NM", title: "<spring:message code='codeManagement.009' text='코드명' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"OUTPT_SN", title: "<spring:message code='codeManagement.010' text='출력순서' />", type: "number", width: 100, isSearch: "Y"}
                ,{ name:"CD_DC", title: "<spring:message code='codeManagement.011' text='설명' />", type: "text", width: 150, isSearch: "Y"}
                ,{ name:"USE_YN", title : "<spring:message code='codeManagement.005' text='사용여부' />", type: "select", items : [
	                    { Name: "<spring:message code='codeManagement.006' text='사용' />", Id: "Y" },
	                	{ Name: "<spring:message code='codeManagement.007' text='미사용' />", Id: "N" }
	                ], valueField: "Id", textField: "Name", width: 100 }
                ,{ name:"META_1", title: "<spring:message code='codeManagement.012' text='메타1' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_2", title: "<spring:message code='codeManagement.013' text='메타2' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_3", title: "<spring:message code='codeManagement.014' text='메타3' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_4", title: "<spring:message code='codeManagement.015' text='메타4' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_5", title: "<spring:message code='codeManagement.016' text='메타5' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_6", title: "<spring:message code='codeManagement.017' text='메타6' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_7", title: "<spring:message code='codeManagement.018' text='메타7' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_8", title: "<spring:message code='codeManagement.019' text='메타8' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_9", title: "<spring:message code='codeManagement.020' text='메타9' />", type: "text", width: 100, isSearch: "Y"}
                ,{ name:"META_10", title: "<spring:message code='codeManagement.021' text='메타10' />", type: "text", width: 100, isSearch: "Y"}
	            ,{ name:"ORG_CD", type: "text", css:"d-none", width: 0 }
            ]
        });
        
        $("#jsGrid1").on("change", "input,select", function(){
        	updateGrid1List[selectGrid1Row] = selectGrid1Row;
        	isUpdateRow1 = true;
        });
        
        $("#jsGrid2").on("change", "input,select", function(){
        	updateGrid2List[selectGrid2Row] = selectGrid2Row;
        	isUpdateRow2 = true;
        });
        
        $("#ADD_GROUP_CODE").on("click", function(){
            
        	initGrid2();
            
            if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            $("#jsGrid1").jsGrid("insertItem").done(function() {
            	isNewRow1 = true;
            	$("#CODE_DIV").hide();
            	
            	$("#jsGrid1 tbody").find("tr").last().find("td").first().dblclick();
            });
        });
        
        $("#SAVE_GROUP_CODE").on("click", function(){
            
        	if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            $("#jsGrid1").jsGrid("cancelEdit");
            
            if(updateGrid1List.length <= 0){
                
				if(newGrid1List.length > 0){
	                alert('<spring:message code="common.010" text="내용을 입력하세요." />');
	                for (var key in newGrid1List) {
		                $("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                }
	                return;
				}else{
	                alert('<spring:message code="common.011" text="변경된 정보가 없습니다." />');
	                return;
				}
            	
            }
            
        	var griddata = $("#jsGrid1").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newGroupList = new Array();
        	
            for (var key in updateGrid1List) {
            	saveDataList[di] = griddata[key];
            	
            	if(newGrid1List[key] > -1){
            		saveDataList[di].NEW = 'Y';
                }

                if(saveDataList[di].GRP_CD != saveDataList[di].ORG_GRP_CD){
	                if( newGroupList.indexOf(saveDataList[di].GRP_CD) > -1 ){
	                	alert('<spring:message code="codeManagement.022" text="중복 되는 코드가 존재합니다." />');
	                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                	return;
		            }
	            	
	                //아이디 중복확인
	            	newGroupList[di] = saveDataList[di].GRP_CD;
                }
            	
            	if(String(saveDataList[di].GRP_CD) == ""){
                	alert('<spring:message code="codeManagement.023" text="그룹 코드를 입력하세요." />');
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(String(saveDataList[di].GRP_NM) == ""){
                	alert('<spring:message code="codeManagement.024" text="그룹명을 입력하세요." />');
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }

            	if(saveDataList[di].GRP_CD.length > 20){
                	alert('<spring:message code="codeManagement.025" text="그룹코드 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].GRP_CD.length + "/ 20)");
                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].GRP_NM.length > 30){
                	alert('<spring:message code="codeManagement.026" text="그룹명 길이를 30자 이하로 입력하세요." />'+" (" + saveDataList[di].GRP_NM.length + "/ 30)");
                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	di++;
            	
            }
            
            //코드그룹 중복확인             
            $.ajax({
                url: "/admin/checkCodeGroup",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	GROUP_LIST : newGroupList
                })
            }).done(function(response) {
            	if(response.CHECK_GROUP != ""){
	            	alert('<spring:message code="codeManagement.027" text="그룹 아이디가 중복 되었습니다." />' + " (" +  '<spring:message code="codeManagement.028" text="중복 그룹 아이디 " />' + ": " + response.CHECK_GROUP + ")");
	            }else{
	            	saveGroupCode(saveDataList);
		        }
            });
            
        });
        
        <%--$("#GET_GROUP_CODE").on("click", function(){
        	$("#jsGrid1").jsGrid("loadData");
        });--%>
        
        $("#ADD_CODE").on("click", function(){
            
            if($("#SELECT_GROUP_CD").val() == ""){
                alert('<spring:message code="codeManagement.029" text="코드 그룹을 선택하세요. " />');
                return;
            }
            
            $("#jsGrid1").jsGrid("cancelEdit");
            
            if(isUpdateRow2){
	            isUpdateRow2 = false;
	            $("#jsGrid2").jsGrid("updateItem");
            }
            
            $("#jsGrid2").jsGrid("insertItem").done(function() {
            	isNewRow2 = true;
            	$("#jsGrid2 tbody").find("tr").last().find("td").first().dblclick();
            });
        });
        
        $("#SAVE_CODE").on("click", function(){
            
            if($("#SELECT_GROUP_CD").val() == ""){
                alert('<spring:message code="codeManagement.029" text="코드 그룹을 선택하세요. " />');
                return;
            }
            
            if(isUpdateRow2){
	            isUpdateRow2 = false;
	            $("#jsGrid2").jsGrid("updateItem");
            }

            $("#jsGrid2").jsGrid("cancelEdit");
            
            if(updateGrid2List.length <= 0){
                
				if(newGrid2List.length > 0){
	                alert('<spring:message code="common.010" text="내용을 입력하세요. " />');
	                for (var key in newGrid2List) {
		                $("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                }
	                return;
				}else{
	                alert('<spring:message code="common.011" text="변경된 정보가 없습니다. " />');
	                return;
				}
            }
            
        	var griddata = $("#jsGrid2").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newCodeList = new Array();
        	
            for (var key in updateGrid2List) {
            	saveDataList[di] = griddata[key];
            	
            	if(newGrid2List[key] > -1){
            		saveDataList[di].NEW = 'Y';
                }
                
                if(saveDataList[di].CD != saveDataList[di].ORG_CD){
                    
	                if( newCodeList.indexOf(saveDataList[di].CD) > -1 ){
	                	alert('<spring:message code="codeManagement.022" text="중복 되는 코드가 존재합니다." />');
	                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                	return;
		            }
	            	
	                //아이디 중복확인
	            	newCodeList[di] = saveDataList[di].CD;
                }
            	
            	if(saveDataList[di].CD == undefined ||  String(saveDataList[di].CD) == ""){
                	alert('<spring:message code="codeManagement.030" text="코드를 입력하세요." />');
                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].CD_NM == undefined || String(saveDataList[di].CD_NM) == ""){
                	alert('<spring:message code="codeManagement.031" text="코드명을 입력하세요." />');
                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }

//             	if( saveDataList[di].CD.length > 20){
//                 	alert('<spring:message code="codeManagement.032" text="코드 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].CD.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
            	if(saveDataList[di].CD_NM ==saveDataList[di].CD_NM.length > 30){
                	alert('<spring:message code="codeManagement.033" text="코드명 길이를 30자 이하로 입력하세요." />' + " (" + saveDataList[di].CD_NM.length + "/ 30)");
                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
//             	if(saveDataList[di].META_1 && saveDataList[di].META_1.length > 20){
//                 	alert('<spring:message code="codeManagement.034" text="메타1 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_1.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_2 && saveDataList[di].META_2.length > 20){
//                 	alert('<spring:message code="codeManagement.035" text="메타2 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_2.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_3 && saveDataList[di].META_3.length > 20){
//                 	alert('<spring:message code="codeManagement.036" text="메타3 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_3.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_4 && saveDataList[di].META_4.length > 20){
//                 	alert('<spring:message code="codeManagement.037" text="메타4 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_4.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_5 && saveDataList[di].META_5.length > 20){
//                 	alert('<spring:message code="codeManagement.038" text="메타5 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_5.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_6 && saveDataList[di].META_6.length > 20){
//                 	alert('<spring:message code="codeManagement.039" text="메타6 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_6.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_7 && saveDataList[di].META_7.length > 20){
//                 	alert('<spring:message code="codeManagement.040" text="메타7 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_7.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_8 && saveDataList[di].META_8.length > 20){
//                 	alert('<spring:message code="codeManagement.041" text="메타8 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_8.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_9 && saveDataList[di].META_9.length > 20){
//                 	alert('<spring:message code="codeManagement.042" text="메타9 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_9.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
//             	if(saveDataList[di].META_10 && saveDataList[di].META_10.length > 20){
//                 	alert('<spring:message code="codeManagement.043" text="메타10 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].META_10.length + "/ 20)");
//                 	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
//                 	return;
//                 }
                
            	di++;
            }
            
            //코드 중복확인            
            $.ajax({
                url: "/admin/checkCode",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                      CODE_LIST : newCodeList
                     ,GROUP_CD  : $("#SELECT_GROUP_CD").val()
                })
            }).done(function(response) {
            	if(response.CHECK_CD != ""){
	            	alert('<spring:message code="codeManagement.044" text="코드가 중복 되었습니다." />' + " (" + '<spring:message code="codeManagement.045" text="중복 코드" />' + " : " + response.CHECK_CD + ")");
	            }else{
	            	saveCode(saveDataList);
		        }
            });
        });

        loadPAMsFile("EXE_FILE_DIV", {
				 maxFileCount 	: 1
				,pamsFileType : ['xls']
		});
		
		$("#CODE_FILE_UPLOAD_BTN").on("click", function(){
			
            if($("#SELECT_GROUP_CD").val() == ""){
                alert('<spring:message code="codeManagement.029" text="코드 그룹을 선택하세요." />');
                return;
            }
            
	        // 등록할 파일 리스트
	        var fileCnt = pamsFileCount();
	        
	        // 파일이 있는지 체크
	        if(fileCnt == 0){
	            // 파일등록 경고창
	            alert('<spring:message code="codeManagement.046" text="코드 정보 파일을 업로드 하세요." />');
	            return false;
	        }
	        
	        if(confirm('<spring:message code="common.007" text="등록 하시겠습니까?" />')){
		        
	        	if(isUpdateRow2){
    	            isUpdateRow2 = false;
    	            $("#jsGrid2").jsGrid("updateItem");
                }
                
	            $.ajax({
	                url:"/admin/uploadCodeFile",
	                type:'POST',
	                enctype:'multipart/form-data',
	                processData:false,
	                contentType:false,
	                dataType:'json',
	                cache:false,
	                data:getFilData(),
	                success:function(result){
	                    if(result.RETURN_TYPE == "SUCCESS"){
		                    
							var listSize = result.CODE_LIST.length;
							
	                    	$("#jsGrid2").jsGrid("option", "sorting", false);
	                    	
	                    	for(var ei = 0; ei < listSize; ei++){
		                    	var codeData = result.CODE_LIST[ei];
		                    	codeData.USE_YN = "Y";
		                    	
		                        $("#jsGrid2").jsGrid("insertItem", codeData).done(function() {
			                        var rowId = $("#jsGrid2").jsGrid("option", "data").length - 1;
		                        	updateGrid2List[rowId] = rowId;
		                        	newGrid2List[rowId] = 1;
		                        });
		                    }
		                    
	                    }else if(result.RETURN_TYPE == "EMPTY"){
		                    alert('<spring:message code="common.008" text="파일이 존재하지 않습니다." />');
	                    }else if(result.RETURN_TYPE == "ERR"){
		                    alert('<spring:message code="common.009" text="엑셀 파일에 문제가 발생하였습니다. 파일을 확인하세요." />');		                    
	                    }else{
		                    
	                    }
	                }
	            });
	        }
	        
		});
		
		searchFieldListInit();
		
		$("#SEARCH_INPUT").keydown(function(key) {
			if(key.keyCode == 13) {
				enterSearch();
			}
		});
		$("#SEARCH_INPUT2").keydown(function(key) {
			if(key.keyCode == 13) {
				enterSearch2();
			}
		});
		
    });

    function saveGroupCode(saveDataList){

        if(confirm('<spring:message code="common.007" text="등록 하시겠습니까?" />')){
            
            $.ajax({
                url: "/admin/saveCodeGroup",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    DATA_LIST : saveDataList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />');
		            location.reload();
		        }
            	
            });
            
        }
        
    }
    
    function saveCode(saveDataList){

        if(confirm('<spring:message code="codeManagement.047" text="코드를 등록 하시겠습니까?" />')){
            
            $.ajax({
                url: "/admin/saveCode",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                     DATA_LIST : saveDataList
                    ,GROUP_CD : $("#SELECT_GROUP_CD").val()
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />');
		            initGrid2();
		        }
            });
            
        }
        
    }
    
	function initAllGrid(){

	    // 수정 리스트
	    updateGrid1List = new Array();
	    newGrid1List = new Array();
	    
	    //선택 로우
	    selectGrid1Row = null;
	    isNewRow1 = false;
	    isUpdateRow1 = false;
	    
		initGrid2();
	}
	
	function initGrid2(){

        $("#SELECT_GROUP_CD").val("");
        $("#SELECT_GROUP_NM").html("");
        
        updateGrid2List = new Array();
        newGrid2List = new Array();
        
        //선택 로우
        selectGrid2Row = null;
        isNewRow2 = false;
        isUpdateRow2 = false;

        $("#CODE_DIV").hide();
	}
    
    function searchFieldListInit(){
		$('#SEARCH_CATEGORY').empty();
		var jsGridFields = $("#jsGrid1").data("JSGrid").fields;
		for(var ji = 0; ji < jsGridFields.length; ji++) {
			if(jsGridFields[ji].isSearch == "Y") {
				$('#SEARCH_CATEGORY').append(
					 '<option value="' + jsGridFields[ji].name + '">' + jsGridFields[ji].title + '</option>'
				);
			}
		}
	}
	
	function enterSearch(){
		if($("#SEARCH_CATEGORY").val().length < 1) {
			alert("올바른 카테고리를 선택해주세요.");
			return;
		}
		
		if( updateGrid1List.length > 0 || newGrid1List.length > 0 ){
			if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
				return;
			}
			initAllGrid();
		}
		
		_searchedValue["CATEGORY"] = $("#SEARCH_CATEGORY").val();
		_searchedValue["INPUT"] = $("#SEARCH_INPUT").val();
		searchCodeList();
		
	}
	
	function searchCodeList() {
		$.ajax({
			url: "/admin/getCodeGroupList",
			type : "post",
			contentType: 'application/json',
			data : JSON.stringify({
				SEARCH_CATEGORY : _searchedValue["CATEGORY"],
				SEARCH_INPUT : _searchedValue["INPUT"]
			})

		}).done(function(response) {
				isSearch = true;
				da = {
					data : response.CODE_GROUP_LIST
				};
			$("#jsGrid1").jsGrid("loadData");
		});
	}
	
	function searchFieldListInit2(){
		$('#SEARCH_CATEGORY2').empty();
		$('#SEARCH_INPUT2').empty();
		var jsGridFields = $("#jsGrid2").data("JSGrid").fields;
		for(var ji = 0; ji < jsGridFields.length; ji++) {
			if(jsGridFields[ji].isSearch == "Y") {
				$('#SEARCH_CATEGORY2').append(
					 '<option value="' + jsGridFields[ji].name + '">' + jsGridFields[ji].title + '</option>'
				);
			}
		}
	}
	
	function enterSearch2(){
		if($("#SEARCH_CATEGORY2").val().length < 1) {
			alert('<spring:message code="codeManagement.049" text="올바른 카테고리를 선택해주세요." />');
			return;
		}
		
		if( updateGrid2List.length > 0 || newGrid2List.length > 0 ){
			if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
				return;
			}
			initAllGrid();
		}
		
		_searchedValue2["CATEGORY"] = $("#SEARCH_CATEGORY2").val();
		_searchedValue2["INPUT"] = $("#SEARCH_INPUT2").val();
		searchCodeList2();
		
	}
	
	function searchCodeList2() {
		$.ajax({
			url: "/admin/getCodeList",
			type : "post",
			isHideOverlay : true,
			contentType: 'application/json',
			data : JSON.stringify({
				GRP_CD : $("#SELECT_GROUP_CD").val(),
				SEARCH_CATEGORY : _searchedValue2["CATEGORY"],
				SEARCH_INPUT : _searchedValue2["INPUT"]
			})

		}).done(function(response) {
				isSearch = true;
				da2 = {
					data : response.CODE_LIST
				};
			$("#jsGrid2").jsGrid("loadData");
		});
	}
    
</script>

<section>

	<div class="col-xs-12 btn-area">
		<button id="GET_GROUP_CODE" type="button" class="btn btn-dark"  onclick="enterSearch();">
			<spring:message code="common.017" text="조회" />
		</button>
		&nbsp;
		<button id="ADD_GROUP_CODE" type="button" class="btn btn-dark">
			<spring:message code="common.015" text="신규" />
		</button>
		&nbsp;
		<button id="SAVE_GROUP_CODE" type="button" class="btn btn-dark">
			<spring:message code="common.047" text="저장" />
		</button>
	</div>
	
	<div class="x_panel">
		<div class="col-xs-12">
			<div class="x_title">
				<h2>
					<spring:message code="codeManagement.050" text="상위코드그룹 검색" />
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="x_content">
			<div class="col-xs-10">
				<div class="input-group">
					<select id="SEARCH_CATEGORY" style="width:20%" name="SEARCH_CATEGORY" class="form-control custom-select"></select>
					<input id="SEARCH_INPUT" class="form-control" style="width:65%; height:auto;" type="search" placeholder="검색어를 입력해주세요.">
				</div>
			</div>
		</div>
	</div>
	
	<div class="x_panel x_result">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="codeManagement.051" text="상위코드그룹" />
					</h2>
				</div>
			</div>
			<div id="jsGrid1" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
			<br><br>
			<div id="CODE_DIV" style="display: none;">
				<div class="col-xs-12">
					<div class="x_underline"></div>
				</div>
				<br><br>
				<input id="SELECT_GROUP_CD" type="hidden" value="">
				<div class="col-xs-6">
					<div class="x_title">
						<h2 id="SELECT_GROUP_NM"></h2>
					</div>
				</div>
				
				<div class="col-xs-6 btn-area">
					<button id="ADD_CODE" type="button" class="btn btn-dark">
						<spring:message code="common.015" text="신규" />
					</button>
					&nbsp;
					<button id="SAVE_CODE" type="button"
						class="btn btn-dark">
						<spring:message code="common.047" text="저장" />
					</button>
					&nbsp;
					<button id="GET_CODE" type="button" class="btn btn-dark"  onclick="enterSearch2();">
						<spring:message code="common.017" text="조회" />
					</button>
				</div>
				
				<div class="col-xs-12">
					<h2 style="font-weight:bold">
						<spring:message code="codeManagement.052" text="하위코드 검색" />
					</h2>
				</div>
				<div class="col-xs-12">
					<div class="col-xs-10">
						<div class="input-group">
							<select id="SEARCH_CATEGORY2" style="width:20%" name="SEARCH_CATEGORY2" class="form-control custom-select"></select>
							<input id="SEARCH_INPUT2" class="form-control" style="width:65%; height:auto;" type="search" placeholder="검색어를 입력해주세요.">
						</div>
						<br>
					</div>
				</div>
				
				<div id="jsGrid2" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
				
			</div>
		</div>
	</div>
	<div class="x_panel">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="codeManagement.048" text="코드 업로드" />
					</h2>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-light">
			<div class="col-xs-12 btn-area">
				<button type="button" class="btn btn-primary" onclick="window.open('../uploadsample/CODE.xls');">
					<spring:message code="common.019" text="양식 다운로드" />
				</button>
				&nbsp;
				<button id="CODE_FILE_UPLOAD_BTN" type="button" class="btn btn-dark">
					<spring:message code="common.020" text="업로드" />
				</button>
			</div>
			<div class="col-xs-12">
				<div id="EXE_FILE_DIV" class="card-body"></div>
			</div>
		</nav>
	</div>

</section>
