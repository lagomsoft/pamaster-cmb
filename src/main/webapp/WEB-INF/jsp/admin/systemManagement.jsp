<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<script type="text/javascript">
    
    var isChange = false;
    
    $(function () {
        
        getSysCodeList();
    
    });
     
    function getSysCodeList() {
    
        $.ajax({
            url: "/admin/getSystemCodeList",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
            })
        }).done(function(response) {
            if(response){
                var sysCodeList = response.CODE_LIST;
                for(var si in sysCodeList) {
                    var sysCode = sysCodeList[si];
                    for(var mi = 1; mi < 11; mi++){
                        if(sysCode["META_"+mi] != null && sysCode["META_"+mi] != "") {
                            var $sysCodeInput = $("input[data-header="+sysCode.GRP_CD+"][data-code="+sysCode.CD+"][data-type=META_"+mi+"]");
                            $("input[data-header="+sysCode.GRP_CD+"][data-code="+sysCode.CD+"][data-type=META_"+mi+"]").val(sysCode[$sysCodeInput.attr("data-type")]);
                            $("input[data-header="+sysCode.GRP_CD+"][data-code="+sysCode.CD+"][type=radio][data-value="+sysCode[$sysCodeInput.attr("data-type")]+"]").click();
                        }
                    }
                }
                $("#SYS_MAIN_DIV").on("change", "select,input,radio", function(){
                    isChange = true;
                });
            }
        });
        
    }
    
    function saveCodeList() {
    
        var codeList = new Array();
        var isFull = true;

        $("input").each(function(idx){
            var code;
            if($(this).attr("type") == "radio" && $(this).is(":checked")) {
                code = {
                    GRP_CD : $(this).attr("data-header"),
                    CD : $(this).attr("data-code"),
                    META_NUM : $(this).attr("data-type"),
                    META_VAL : $(this).attr("data-value")
                }
            } else if($(this).attr("type") != "radio") {
                code = {
                    GRP_CD : $(this).attr("data-header"),
                    CD : $(this).attr("data-code"),
                    META_NUM : $(this).attr("data-type"),
                    META_VAL : $(this).val()
                }
            }
            if(code) {
                if(code.META_VAL != null && code.META_VAL != "") {
                    codeList.push(code);
                } else {
                    isFull = false;
                }
            }
        });
        
        if (!isChange) {
            alert('<spring:message code="systemManagement.016" text="변경사항이 없습니다." />');
            return;
        }
        
        if (!isFull) {
            alert('<spring:message code="systemManagement.017" text="채워지지 않은 항목이 있습니다." />');
            return;
        }

        if (!confirm('<spring:message code="common.049" text="저장하시겠습니까?" />')) {
            return;
        }
        
        $.ajax({
            url : "/admin/saveSystemCodeList",
            type : "post",
            contentType : 'application/json',
            data : JSON.stringify({
                CODE_LIST : codeList 
            })
        }).done(function(response) {
        
            if (response.SUCCESS == "0") {
                alert('<spring:message code="common.013" text="저장 되었습니다." />');
                location.reload();
            }

        });
    }
    
    
</script>

<section>

    <div class="col-xs-12 btn-area">
        <button id="SAVE_DATA" type="button" class="btn btn-dark" onclick="saveCodeList()">
            <spring:message code="common.047" text="저장" />
        </button>
    </div>


    <div class="x_panel x_result">
        <div class="row">
            <div class="col-xs-12">
                <div class="x_title">
                    <h2>
                        <spring:message code="systemManagement.001" text="시스템관리" />
                    </h2>
                </div>
            </div>
            <div class="col-xs-12">
                <div id="SYS_MAIN_DIV" style="margin-left:10px;">
                    <table class="table basic_table margin_b0">
                        <colgroup>
                            <col width="16%">
                            <col width="*">
                        </colgroup>
                        <tr>
                            <th class="th_bg txt_l" colspan="2">
                                <spring:message code="systemManagement.002" text="비밀번호 정책" />
                            </th>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.003" text="비밀번호 오류 시 잠금 제한 횟수" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <input type="text" class="form-control" style="width: 20%;" data-header="PASSWORD_POLICY_CD" data-code="PW_FAILURE_COUNT" data-type="META_1" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.004" text="비밀번호 최대 사용 기간" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <input type="text" class="form-control" style="width: 20%;" data-header="PASSWORD_POLICY_CD" data-code="PW_MAX_USE_DAY" data-type="META_1" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    <label style="margin-top:auto; margin-left:10px;">
                                    <spring:message code="systemManagement.005" text="일 (0일경우 미사용)" />
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.006" text="비밀번호 길이" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <label style="margin-top:auto; margin-right:10px;">
                                    <spring:message code="systemManagement.007" text="최소 길이" />
                                    </label>
                                    <input type="text" class="form-control" style="width: 20%;" data-header="PASSWORD_POLICY_CD" data-code="PW_POLICY" data-type="META_1" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                </div>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <label style="margin-top:auto; margin-right:10px;">
                                    <spring:message code="systemManagement.008" text="최대 길이" />
                                    </label>
                                    <input type="text" class="form-control" style="width: 20%;" data-header="PASSWORD_POLICY_CD" data-code="PW_POLICY" data-type="META_2" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.009" text="비밀번호 요구수준" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left;">
                                    <p class="p_rad">
                                        <input type="radio" id="PW_POLICY1" name="PW_POLICY" class="custom-control-input" data-header="PASSWORD_POLICY_CD" data-code="PW_POLICY" data-type="META_3" data-value="1">
                                        <label for="PW_POLICY1">
                                            <spring:message code="systemManagement.010" text="제약없음" />
                                        </label>
                                    </p>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left;">
                                    <p class="p_rad">
                                        <input type="radio" id="PW_POLICY2" name="PW_POLICY" class="custom-control-input" data-header="PASSWORD_POLICY_CD" data-code="PW_POLICY" data-type="META_3" data-value="2">
                                        <label for="PW_POLICY2">
                                            <spring:message code="systemManagement.011" text="영문, 숫자 모두 포함" />
                                        </label>
                                    </p>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left;">
                                    <p class="p_rad">
                                        <input type="radio" id="PW_POLICY3" name="PW_POLICY" class="custom-control-input" data-header="PASSWORD_POLICY_CD" data-code="PW_POLICY" data-type="META_3" data-value="3">
                                        <label for="PW_POLICY3">
                                            <spring:message code="systemManagement.012" text="영문, 숫자, 특수문자 모두 포함" />
                                        </label>
                                    </p>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="th_bg txt_l" colspan="2">
                                <spring:message code="systemManagement.013" text="자동삭제 관련 정책" />
                            </th>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.014" text="감사로그 삭제 예정일" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <input type="text" class="form-control" style="width: 20%;" data-header="DESTRUCTION_POLICY" data-code="DELETE_LOG_MAX_DAY" data-type="META_1" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    <label style="margin-top:auto; margin-left:10px;">
                                    <spring:message code="systemManagement.005" text="일 (0일경우 미사용)" />
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.015" text="반출 이후 자동파기 예정일" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <input type="text" class="form-control" style="width: 20%;" data-header="DESTRUCTION_POLICY" data-code="DESTRUCTION_MAX_DAY" data-type="META_1" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    <label style="margin-top:auto; margin-left:10px;">
                                    <spring:message code="systemManagement.005" text="일 (0일경우 미사용)" />
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="th_bg txt_l" colspan="2">
                                <spring:message code="systemManagement.018" text="스케줄관리 관련 정책" />
                            </th>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                <spring:message code="systemManagement.019" text="스케줄 동시 배치 개수" />
                            </th>
                            <td>
                                <div class="custom-control custom-radio custom-control-inline txt_l" style="width: 20%; float: left; display: flex;">
                                    <input type="text" class="form-control" style="width: 20%;" data-header="SCHEDULE_SETTING" data-code="BATCH_MAX_VOLUME" data-type="META_1" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    <label style="margin-top:auto; margin-left:10px;">
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="th_bg txt_l" colspan="2">
                                시스템 고정 설정
                            </th>
                        </tr>
                        <tr>
                            <th class="txt_l">
                                세션 타임아웃
                            </th>
                            <td class="txt_l">
                                60분
                            </td>
                        </tr>
                     </table>
                </div>
            </div>
        </div>
    </div>
    
</section>
