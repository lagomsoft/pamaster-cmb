<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
/***
현재 메시지 매핑 된 화면
공통 X
유저 O
프로젝트 생성 O
사전준비 O
가명처리 O (위험도측정X)
적정성 검토 및 추가처리 X (데이터비교O)
결합 X
반출 X
관리자 - 프로젝트관리, 코드관리, 매핑관리, 메시지관리, 위험도관리, 유저관리 O / 메뉴관리, 부서관리, 프로젝트스케줄관리, 이력관리, 권한관리 X
*/
%>
<script type="text/javascript">
	

     // 수정 리스트
     var updateGrid1List = new Array();
     var newGrid1List = new Array();
     var deleteGrid1List = new Array();
     deleteGrid1List["COUNT"] = 0;
     
     //선택 로우
     var isNewRow1 = false;
     var isUpdateRow1 = false;
     var selectGrid1Row = null;
     
     //검색 데이터
     var menu_id = "";
     var msg_cn = "";
     var msg_id = "";
     
     var menuList = [
	<c:forEach items="${MENU_LIST}" var="ml" varStatus="status">
		<c:if test="${ml.UPPER_MENU_SEQ ne ''}">
			<c:if test="${status.first}">{Id : "common" , Name : "common"},</c:if>
			<c:if test="${!status.first}">,</c:if>  {Id : "${ml.MENU_ID}" , Name : "${ml.MENU_NM}"}
		</c:if>
	</c:forEach>
	];
   	
    $(function () {
        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "400px",
            editing: true,
            rowClick : function(args){
            	
                if(isUpdateRow1){
    	            isUpdateRow1 = false;
    	            $("#jsGrid1").jsGrid("updateItem");
                }
                
                <%--
                    $("#jsGrid1").jsGrid("updateItem");
                --%>
                
                $("#jsGrid1").jsGrid("cancelEdit");

            },
            rowDoubleClick : function(args){
                
            	selectGrid1Row = args.itemIndex;
            	this.option("sorting", false);
            	this.editItem($(this._body).find("tr").eq(selectGrid1Row));
            	
            	var selectedVal = $(this._body).find("tr").eq(selectGrid1Row).find("select option:checked").val();
            	menuSelectList($(this._body).find("tr").eq(selectGrid1Row).find("select"));
            	if(isNewRow1){
            		newGrid1List[selectGrid1Row] = 1;
            		if( $("#MENU_ID").val() != "" ) {
            			$(this._body).find("tr").eq(selectGrid1Row).find("select").val(menu_id);
            			$(this._body).find("tr").eq(selectGrid1Row).find("select").change();
            		}
                } else {
                	$(this._body).find("tr").eq(selectGrid1Row).find("select").val(selectedVal);
            		$(this._body).find("tr").eq(selectGrid1Row).find("select").change();
                }
            	isNewRow1 = false;
            	$(this._body).find("tr").eq(selectGrid1Row).find("input").eq(0).focus();
            },
            controller: {
                loadData: function() {
                    
                    if( updateGrid1List.length > 0 || newGrid1List.length > 0 ){
                    	if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
                        	return;
                    	}
                    	
                    	initAllGrid();
                    }
                    
                    var d = $.Deferred();
                    $.ajax({
                        url: "/admin/getMessageList",
                        type : "post",
                        contentType: 'application/json',
                        data : JSON.stringify({
                        	MENU_ID : menu_id,
                        	MSG_CN : msg_cn,
                        	MSG_ID : msg_id,
                        	CNT_CD : "ko"
                        })
                            
                    }).done(function(response) {
                        d.resolve(response.DATA_LIST);
                    });
                    $("#jsGrid1").jsGrid("option", "sorting", true);
                    return d.promise();
                }
            },
            sorting: true,
            fields: [
                 { name: "No", title : '<spring:message code="common.029" text="삭제" />', width: 15 , align: 'center'
                     , itemTemplate: function (_, item) {
                         if(String(item.ORG_MSG_ID) == ""){
                             return "";
                         }
                         var $deleteCheckBox = $('<input type="checkbox" class="form-check-input" id="DELETE_CHECKBOX_'+item.ORG_MSG_ID+'">').on("change", function(){
                             
                             if($(this).is(":checked")){
                            	 deleteGrid1List[item.ORG_MSG_ID] = 1;
                            	 deleteGrid1List["COUNT"] = deleteGrid1List["COUNT"] + 1;
                             }else{
                            	 delete deleteGrid1List[item.ORG_MSG_ID];
                            	 deleteGrid1List["COUNT"] = deleteGrid1List["COUNT"] - 1;
                             }
                         });
                         var $deleteCheckBoxLabel = $('<label class="form-check-label" for="DELETE_CHECKBOX_'+item.ORG_MSG_ID+'"></label>');
                         
                         return $('<div class="form-check">').append($deleteCheckBox).append($deleteCheckBoxLabel);
 	                }
                 }
                ,{ name: "MENU_ID", title : '<spring:message code="messageManagement.010" text="메뉴아이디" />', width: 80, type: "select", items : menuList, valueField: "Id", textField: "Name"}
                ,{ name: "MSG_ID", title : '<spring:message code="messageManagement.002" text="메시지코드" />', type: "text", width: 80, align:'center'}
                ,{ name: "MSG_CN", title : '<spring:message code="messageManagement.003" text="메시지" />', type: "text", width: 200}
                ,{ name: "ORG_MSG_ID", title : '<spring:message code="messageManagement.002" text="메시지코드" />', type: "text", css:"d-none", width: 0 }
            ]
        });

        $("#jsGrid1").on("change", "input,select", function(){
        	
            if($(this).attr("type") != "checkbox"){
	        	updateGrid1List[selectGrid1Row] = selectGrid1Row;
	        	isUpdateRow1 = true;
            }
        });
        
        $("#ADD_ROW").on("click", function(){
            
            if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
	        $("#jsGrid1").jsGrid("insertItem").done(function() {
            	isNewRow1 = true;
            	$("#jsGrid1 tbody").find("tr").last().find("td").first().dblclick();
            	$("#jsGrid1 tbody").find(".jsgrid-edit-row").find("input").eq(0).focus();
            });
        });
        
        $("#DELETE_ROW").on("click", function(){
            
            if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            if( deleteGrid1List["COUNT"] <= 0){
                alert( '<spring:message code="common.031" text="선택된 정보가 없습니다." />' );
	            return;
            }
            
	        if(confirm( '<spring:message code="common.032" text="삭제 하시겠습니까?" />')){

	        	var deleteDataList = new Array();
	        	var di = 0;
	            for (var key in deleteGrid1List) {
		            if(key != "COUNT"){
		            	deleteDataList[di] = key;
		            	di++;
			        }
	            }
	            
	            $.ajax({
	                url: "/admin/deleteMessage",
	                type : "post",
	                contentType: 'application/json',
	                data : JSON.stringify({
	                    DATA_LIST : deleteDataList
	                })
	            }).done(function(response) {
		            if(response.SUCCESS == "0"){
			            alert('<spring:message code="common.033" text="처리 되었습니다." />' );
			            $("#jsGrid1").jsGrid("loadData");
			        }
	            });
	            
	        }
            
        });
        
		$("#FILE_UPLOAD_BTN").on("click", function(){
			
	        var fileCnt = pamsFileCount();
	        
	        if(fileCnt == 0){
	            alert( '<spring:message code="common.006" text="파일을 업로드 하세요." />');
	            return false;
	        }
	        
	        if(confirm( '<spring:message code="common.007" text="등록 하시겠습니까?" />')){
		        
	            if(isUpdateRow1){
		            isUpdateRow1 = false;
		            $("#jsGrid1").jsGrid("updateItem");
	            }
	            
	            $.ajax({
	                url:"/admin/uploadMessageFile",
	                type:'POST',
	                enctype:'multipart/form-data',
	                processData:false,
	                contentType:false,
	                dataType:'json',
	                cache:false,
	                data:getFilData(),
	                success:function(result){
	                    if(result.RETURN_TYPE == "SUCCESS"){
		                    
							var listSize = result.DATA_LIST.length;
							
	                    	$("#jsGrid1").jsGrid("option", "sorting", false);
	                    	
	                    	for(var ei = 0; ei < listSize; ei++){
		                    	
		                        $("#jsGrid1").jsGrid("insertItem", result.DATA_LIST[ei]).done(function() {
			                        var rowId = $("#jsGrid1").jsGrid("option", "data").length - 1;
		                        	updateGrid1List[rowId] = rowId;
		                        	newGrid1List[rowId] = 1;
		                        });
		                        
		                    }
	                    }else if(result.RETURN_TYPE == "EMPTY"){
		                    alert( '<spring:message code="common.008" text="파일이 존재하지 않습니다." />' );
	                    }else if(result.RETURN_TYPE == "ERR"){
		                    alert( '<spring:message code="common.009" text="엑셀 파일에 문제가 발생하였습니다. 파일을 확인하세요." />' );		                    
	                    }else{
		                    
	                    }
	                }
	            });
	        }
	        
		});

        $("#GET_DATA").on("click", function(){
        	menu_id = "";
			msg_cn = "";
			msg_id = "";
			initAllGrid();
	        $("#jsGrid1").jsGrid("loadData");
        });

        $("#SAVE_MESSAGE").on("click", function(){
            
        	if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            $("#jsGrid1").jsGrid("cancelEdit");
            
            if(updateGrid1List.length <= 0){
                
				if(newGrid1List.length > 0){
	                alert( '<spring:message code="common.010" text="내용을 입력하세요." />' );
	                for (var key in newGrid1List) {
		                $("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                }
	                return;
				}else{
	                alert( '<spring:message code="common.011" text="변경된 정보가 없습니다." />' );
	                return;
				}
            	
            }
            
        	var griddata = $("#jsGrid1").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newList = new Array();
        	
            for (var key in updateGrid1List) {
            	saveDataList[di] = griddata[key];
            	
            	if(newGrid1List[key] > -1){
            		saveDataList[di].NEW = 'Y';
                }
                
                if(saveDataList[di].MSG_ID != saveDataList[di].ORG_MSG_ID){
	                if( newList.indexOf(saveDataList[di].MSG_ID) > -1 ){
	                	alert( '<spring:message code="messageManagement.004" text="중복되는 메시지코드가 존재 합니다." />' );
	                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                	return;
		            }
	            	
	                //아이디 중복확인
	            	newList[di] = saveDataList[di].MSG_ID;
                }
            	
            	if(saveDataList[di].MSG_ID == undefined || String(saveDataList[di].MSG_ID) == ""){
                	alert( '<spring:message code="messageManagement.005" text="메시지코드를 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].MSG_CN == undefined || String(saveDataList[di].MSG_CN) == ""){
                	alert( '<spring:message code="messageManagement.006" text="메시지를 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
                if(saveDataList[di].MENU_ID == undefined || String(saveDataList[di].MENU_ID) == ""){
                	alert( '<spring:message code="messageManagement.007" text="메뉴아이디를 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }

            	if(saveDataList[di].MSG_ID.length > 30){
                	alert('<spring:message code="messageManagement.008" text="메시지코드 길이를 30자 이하로 입력하세요." />' + " (" + saveDataList[di].GRP_CD.length + "/ 30)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].MSG_CN.length > 300){
                	alert('<spring:message code="messageManagement.016" text="메시지 길이를 300자 이하로 입력하세요." />' + " (" + saveDataList[di].MSG_CN.length + "/ 300)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	di++;
            	
            }
            
            //코드그룹 중복확인
            $.ajax({
                url: "/admin/checkMessageCode",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	 MESSAGE_LIST : newList
                	,CNT_CD : "ko"
                })
            }).done(function(response) {
            	if(response.CHECK_CD != ""){
	            	alert( '<spring:message code="messageManagement.009" text="메시지코드가 중복 되었습니다." />' + ' (  <spring:message code="common.012" text="중복코드" />  : ' + response.CHECK_CD + ")");
	            }else{
	            	saveMessage(saveDataList);
		        }
            });
            
        });
        
        loadPAMsFile("EXE_FILE_DIV", {
				 maxFileCount 	: 1
				,pamsFileType : ['xls']
		});
		
		$("#SEARCH_BTN").on("click", function() {
			menu_id = $("#MENU_ID").val();
			msg_cn = $("#MSG_CN").val();
			msg_id = $("#MSG_ID").val();

			initAllGrid();
			$("#jsGrid1").jsGrid("loadData");
		});
		
		$(".form-control").keydown(function(key) {
			if(key.keyCode == 13) {
				$("#SEARCH_BTN").click();
			}
		});

    });

    function saveMessage(saveDataList){
        
        if(confirm( '<spring:message code="common.007" text="등록 하시겠습니까?" />')){
            
            $.ajax({
                url: "/admin/saveMessage",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    DATA_LIST : saveDataList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />' );
		            //location.reload();
		            $("#SEARCH_BTN").click();
		        }
            	
            });
            
        }
    }
    
    function menuSelectList(body) {
    	var selectedValue = body.val();
	    body.html("");
		body.append('<option value="common">common</option>');
		<c:forEach var="menu_list" items='${MENU_LIST}' varStatus="status">
			<c:if test="${empty menu_list.UPPER_MENU_SEQ}">
				body.append('<optgroup label="${menu_list.MENU_NM }">');
					<c:forEach var="menu_list2" items='${MENU_LIST}' varStatus="status2">
						<c:if test="${menu_list.SEQ eq menu_list2.UPPER_MENU_SEQ}">
							body.append('<option value="${menu_list2.MENU_ID}">${menu_list2.MENU_NM}</option>');
						</c:if>
					</c:forEach>
				body.append('</optgroup>');
			</c:if>
		</c:forEach>
		
		if (selectedValue != null){
			body.val(menu_id);
		}
    }
    
	function initAllGrid(){
	    // 수정 리스트
	    updateGrid1List = new Array();
	    newGrid1List = new Array();
	    
	    //선택 로우
	    selectGrid1Row = null;
	    isNewRow1 = false;
	    isUpdateRow1 = false;
	}
	
    header = [
            { name : "MENU_ID", title: '<spring:message code="messageManagement.010" text="메뉴아이디" />'},
            { name : "MSG_ID", title: '<spring:message code="messageManagement.002" text="메시지코드" />'},
            { name : "MSG_CN", title: '<spring:message code="messageManagement.003" text="메시지" />'}
    ]
    
    function exportExcel(){
        if($("#PAMS_PROJECT_SELECT").val() != ''){
            var exportFrm = $("<form>",{
                action : "/getMessageList.xls",
                method : 'post'
            })
            exportFrm.append($("<input>",{
                type : 'hidden',
                name : 'header',
                value : JSON.stringify(header)
            }))        
            exportFrm.append($("<input>",{
                type : 'hidden',
                name : 'MENU_ID',
                value :  menu_id
            }))
            exportFrm.append($("<input>",{
                type : 'hidden',
                name : 'MSG_CN',
                value :  msg_cn
            }))
            exportFrm.append($("<input>",{
                type : 'hidden',
                name : 'MSG_ID',
                value :  msg_id
            }))
            exportFrm.append($("<input>",{
                type : 'hidden',
                name : 'CNT_CD',
                value :  "ko"
            }))

            var fileName = menu_id+" 메시지 저장";
            
            exportFrm.append($("<input>",{
                type : 'hidden',
                name : 'fileName',
                value : encodeURI(fileName)
            }))
            $('body').append(exportFrm);
            $(exportFrm)[0].submit();
            $(exportFrm).remove();
        }
    }
	
</script>
<section>
	
	<div class="col-xs-12 btn-area">
		<button id="SEARCH_BTN" type="button" class="btn btn-dark">
			<spring:message code="common.017" text="조회" />
		</button>
		&nbsp;
		<button id="ADD_ROW" type="button" class="btn btn-dark">
			<spring:message code="common.015" text="신규" />
		</button>
		&nbsp;
		<button id="DELETE_ROW" type="button" class="btn btn-dark">
			<spring:message code="common.029" text="삭제" />
		</button>
		&nbsp;
		<button id="SAVE_MESSAGE" type="button" class="btn btn-dark">
			<spring:message code="common.047" text="저장" />
		</button>
		&nbsp;
        <button type="button" class="btn bg-green" id="exportBtn" onclick="exportExcel()">
            <i class="fa fa-file-excel-o"></i>
            EXCEL
        </button>
	</div>
	
	
	<div class="x_panel x_result">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="messageManagement.015" text="메시지 검색" />
					</h2>
				</div>
			</div>
		</div>
		<div class="col-xs-12">
			<div class="row">
				<div class="table-responsive">
					<table class="table basic_table table-hover text-nowrap">
						<col width="7%">
						<col width="*">
						<tbody>
							<tr>
								<td style="margin-bottom: 0;"><spring:message
										code="messageManagement.010" text="메뉴아이디" />&nbsp;&nbsp;&nbsp;:</td>
								<td style="margin-bottom: 0;"><select id="MENU_ID"
									name="MENU_ID"
									class="form-control select2 custom-control-inline search-select "
									style="width: 50%;">
										<option value=''><spring:message
												code="messageManagement.013" text="전체선택" /></option>
										<option value='common'>common</option>
										<c:forEach var="menu_list" items='${MENU_LIST}' varStatus="status">
											<c:if test="${empty menu_list.UPPER_MENU_SEQ}">
												<optgroup label="${menu_list.MENU_NM }">
													<c:forEach var="menu_list2" items='${MENU_LIST}' varStatus="status2">
															<c:if test="${menu_list.SEQ eq menu_list2.UPPER_MENU_SEQ}">
																<option value='${menu_list2.MENU_ID}'>${menu_list2.MENU_NM}</option>
															</c:if>
													</c:forEach>
												</optgroup>
											</c:if>
										</c:forEach>
								</select></td>
							</tr>
							<tr>
								<td style="margin-bottom: 0;"><spring:message
										code="messageManagement.002" text="메시지코드" />&nbsp;&nbsp;&nbsp;:</td>
								<td style="margin-bottom: 0;"><input type="text"
									class="form-control" id="MSG_ID"
									placeholder='<spring:message code="messageManagement.012" text="검색"/>'
									name="MSG_ID"></td>
							</tr>
							<tr>
								<td style="margin-bottom: 0;"><spring:message
										code="messageManagement.015" text="메시지검색" />&nbsp;&nbsp;&nbsp;:</td>
								<td style="margin-bottom: 0;"><input type="text"
									class="form-control" id="MSG_CN"
									placeholder='<spring:message code="messageManagement.012" text="검색"/>'
									name="MSG_CN"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="x_panel">
		<div class="row">
			<div class="col-xs-12">
				<div id="jsGrid1" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
			</div>
		</div>
	</div>
	<div class="x_panel">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="common.018" text="일괄 업로드" />
					</h2>
				</div>
			</div>
			<nav class="navbar navbar-light">
				<div class="col-xs-12 btn-area">
					<button type="button" class="btn btn-primary" onclick="window.open('../uploadsample/MSG.xls');">
						<spring:message code="common.019" text="양식 다운로드" />
					</button>
					&nbsp;
					<button id="FILE_UPLOAD_BTN" type="button" class="btn btn-dark">
						<spring:message code="common.020" text="업로드" />
					</button>
				</div>
				<div class="col-xs-12">
					<div id="EXE_FILE_DIV" class="card-body"></div>
				</div>
			</nav>
		</div>
	</div>
</section>
