<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<style type="text/css">
	.input-group {
		position: relative;
		display: flex;
		border-collapse: separate;
		align-items: stretch;
	}
	
	.nav-link {
		display: flex !important;
	}
</style>

<script type="text/javascript">
	var clickedGroup = null;
	var saveCondition = 0;
	var groupListLength = 0;
	
	$(function () { 
	    <%--상세 페이지 위치 이동--%>
	    var currentPosition = parseInt($("#_detailDiv").css("margin-top").replace("px","")); 
	    $(window).scroll(function() { var position = $(window).scrollTop(); 
	    	$("#_detailDiv").stop().animate({"margin-top":position+currentPosition+"px"},500); 
	    	
	    });

		<%--각 동작별 ajax 그룹--%>
		var cont = {
				selectNodeId : null,
				targetData : null,
				insertGroup : function(obj) {
					
					$.ajax({
						url: "/admin/insertGroupInfo",
						type : "post",
						contentType: 'application/json',					
						data : JSON.stringify(obj),            
						success:function(res){
							$('#departmentManagement').jstree(true)
								.get_node(obj.GP_CD)
								.original
								.DEPTH = res.DEPTH;
							$("#IDX").val(res.IDX);
							$('#DEPTH').val(res.DEPTH)
							return true;  
						},
						error:function(request,status,error) {
							console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
							return false;
						}
		           });
	            },
				updateGroup : function(obj, groupId) {
					$.ajax({
						url: "/admin/updateGroupInfo",
						type : "post",
						contentType: 'application/json',
						data : JSON.stringify(obj),
						success:function(){					
							<%--정상 처리시--%>
				           	$('#departmentManagement').jstree(true).refresh(); 
				            setTimeout(function () {		
				            	$('#departmentManagement').jstree(true).select_node(groupId); 
	                    	},100);		           
				         	
							return true;  
						},
						error:function(request,status,error){
							console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
							return false;
						}
					});
	            },
				deleteGroup : function(obj){
					$.ajax({
						url: "/admin/deleteGroupInfo",
						type : "post",
						contentType: 'application/json',
						data : JSON.stringify(obj),
						success:function(){
							$("#_detailDiv").hide();	
							return true;  
						},
						error:function(request,status,error){
							console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
							return false;
						}
					});
	            },
		}
		<%--좌측 트리 생성--%>
		var chgFormat = function (obj) {
			for(seq in obj) {
				obj[seq].id = obj[seq].GP_CD;
				obj[seq].parent = obj[seq].UPPER_GP_CD;
				obj[seq].text = obj[seq].GP_NM; 
				obj[seq].type = obj[seq].USE_YN == 'Y'? 'default' : 'void';
				if(!obj[seq].parent) obj[seq].parent = 'Home';	
			}
			
			obj.push({id:'ROOT',text:'HOME',parent:'#',type:'root'});

			return obj;
		}
		<%--메뉴 아이디목록을 seq목록으로 리턴--%>
		var getSeqList = function(obj) {
			var seqList = new Array();
			for(seq in obj){
				var groupId = obj[seq];
				var original = $("#departmentManagement").jstree(true).get_node(groupId).original;
				seqList.push(original.IDX);			
			}
			return seqList;		
		}
		
		<%--메뉴 아이디 입력시 코드 체크--%>
		var checkGroupId = function() {
			var oldMenuId =  $("#departmentManagement").jstree('get_selected')[0];
			var menuId = $("#GP_CD").val();
			if(oldMenuId != menuId){
				$.ajax({
					url: "/admin/getGroupCodeCheck",
					type : "post",
					contentType: 'application/json',
					data : JSON.stringify({GP_CD : $("#GP_CD").val()}),
					success:function(res){	
			
						<%--정상 처리시--%>
			            var cnt = Number(res.GROUP_CHECK);
			            if(cnt != 0){
			            	alert("<spring:message code="departmentManagement.001" text="부서 코드가 이미 존재합니다." />");
			            }else {		            	
		        			_modal.show(
	        					"<spring:message code="departmentManagement.002" text="부서수정"/>",
	        					"<spring:message code="departmentManagement.003" text="선택한 부서를 수정하시겠습니까?"/>",
	        					"<spring:message code="departmentManagement.004" text="수정"/>",
	        					"<spring:message code="departmentManagement.005" text="닫기"/>",
		        				updateCallBack
		        			)		        				            	
			            }
					},
					error:function(request,status,error){
						console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
						return false;
					}
				});
			}else{
				_modal.show(
					"<spring:message code="departmentManagement.002" text="부서수정"/>",
					"<spring:message code="departmentManagement.003" text="선택한 부서를 수정하시겠습니까?"/>",
					"<spring:message code="departmentManagement.004" text="수정"/>",
					"<spring:message code="departmentManagement.005" text="닫기"/>",
	   				updateCallBack
	   			)	
			}
		};
		<%--트리 메뉴 설정--%>
		$('#departmentManagement').jstree({
		  "core" : {
			"multiple" : false,
		    "animation" : true,
		    "check_callback" : true,
		    "data" : function(obj, cb){
		    	$.ajax({
	           	 url: "/admin/getGroupListInfo"
	               ,type : "post"
	               ,contentType: 'application/json'              
	           }).done(function(res) {         	
	        	   cb.call(this,chgFormat(res));
	           });
		    }
		  },
		  "types" : {
		    "#" : {
		      "max_children" : 10,
		      "max_depth" : 10,
		      "valid_children" : ["root"]
		    },
		    "root" : {      
		    	"icon" : "/img/bootstrap-icons/house-fill.svg" 
		    },
		    "default" : {
		    	"icon" : "/img/bootstrap-icons/folder-fill.svg"
		    },
		    "void" : {
		    	"icon" : "/img/bootstrap-icons/folder-x.svg"
		    },
		    "process" : {
				"icon" : "/img/bootstrap-icons/segmented-nav.svg" 
			}
		  },
		  "dnd" : {		  
				"drop_finish" : function () {		
				    alert("DROP");		
				}		  
		  },
		  "plugins" : [
			  "contextmenu","types"
			  <%-- 부서 이동 플러그인 --%>
			  ,"dnd"
		  ],
		  'contextmenu' : {
		      "items" : {
		        createMenu : { 
		          "separator_before" : false,
		          "separator_after" : true,
		          "label" : "<spring:message code="departmentManagement.006" text="부서생성"/>",
		          "action" : function (data) {
		                var inst = $.jstree.reference(data.reference);
		                var obj = inst.get_node(data.reference);	                
		                inst.create_node(obj, {}, "last", 
	               			function (new_node) {	                	
			                    new_node.data = {
			                    	'id' : 'new_menu',
									'text' : '<spring:message code="departmentManagement.007" text="새부서"/>'
			                    };
			                    setTimeout(function () {
			                    	inst.deselect_all();
			                    	inst.select_node(new_node);  
		                    	},10);
		                });
		                
		          },
		        },
				deleteMenu : {
					"separator_before" : false,
					"separator_after" : true,
					"label" : "<spring:message code="departmentManagement.009" text="부서삭제"/>",
					"action" : function (data) {
						cont.targetData = data;	
						var inst = $.jstree.reference(data.reference);
						var obj = inst.get_node(data.reference);
						if(obj.id == 'ROOT'){
							return alert("<spring:message code="departmentManagement.008" text="최상위 부서는 삭제할 수 없습니다."/>");
						}
						
						_modal.show(
							"<spring:message code="departmentManagement.009" text="부서삭제"/>",
							"<spring:message code="departmentManagement.010" text="선택한 부서를 삭제하시겠습니까?"/>",
							"<spring:message code="departmentManagement.011" text="삭제"/>",
							"<spring:message code="departmentManagement.005" text="닫기"/>",
							deleteCallBack
						)
					}
		        }
		      }
		    }  
		});
		
		<%--초기 트리 메뉴 호출시 전체 오픈--%>
		$('#departmentManagement').on('loaded.jstree', function () {

		     $("#departmentManagement").jstree('open_all');
		})

		<%--트리메뉴 메뉴 생성시 이벤트--%>
		$('#departmentManagement').on("create_node.jstree", function (e, data) {
			$("#_detailDiv").show();
			<%--메뉴 depth / seq 획득--%>	
			var groupData = $('#departmentManagement').jstree(true).get_json('#', {flat:true});
			var depth=data.node.parents.length-1;
			
			<%--기본 값 삽입--%>		
			data.node.text 					  = '<spring:message code="departmentManagement.007" text="새부서"/>';  
			data.node.original.GP_NM 		  = data.node.text;
			data.node.original.GP_CD 		  = data.node.id;
			data.node.original.UPPER_GP_CD    = data.node.parent;
			data.node.original.DEPTH	 	  = depth;
			data.node.original.USE_YN 		  = 'Y';
			$('#departmentManagement').jstree(true).rename_node (data.node, '<spring:message code="departmentManagement.007" text="새부서"/>');
			cont.insertGroup(data.node.original);
		});
		
		<%--트리 부서 이동시 이벤트--%>
		$("#departmentManagement").on("move_node.jstree",function(e, data){ 
			<%--메뉴 depth / seq 획득--%>
			var groupData = $('#departmentManagement').jstree(true).get_json('#', {flat:true});
			var depth=data.node.parents.length-1;

			<%--이동한 메뉴데이타 업데이트--%>
			data.node.original.isChange = "Y";
			data.node.original.OLD_GP_CD = data.node.original.GP_CD;
			data.node.original.OLD_DEPTH = data.node.original.DEPTH;
			data.node.original.DEPTH 	  = depth;
			data.node.original.UPPER_GP_CD = data.node.parent;
			if(data.node.parent == 'Home') {
				data.node.original.DEPTH = '-1';
			}
			data.node.original.CHILDREN = getSeqList(data.node.children_d);
			<%--이동한 자식 부서 목록--%>
			cont.updateGroup(data.node.original, data.node.original.GP_CD);	
		})
		
		<%--트리 부서 변경시 이벤트--%>
		$('#departmentManagement').on("changed.jstree", function (e, data) {
		
			<%--MENU_SEQ--%>	
			try{
				var orgin = data.node.original;
				$("#departInfoFrm")[0].reset();
				$("#departInfoFrm").find("[type='HIDDEN']").val('');
				for( nm in orgin){		
					$("#"+nm).val(orgin[nm+""]) 
				}	
			}catch(e){}
			$("#_detailDiv").show();
			$("#GP_CD_ERROR").addClass("d-none");
			$("#DEPART_NM_ERROR").addClass("d-none");
		});
		<%--수정 버튼 클릭시 동작--%>
		$("#updateBtn").on("click",function() {
			
			if(document.querySelectorAll('.needs-validation')[0].checkValidity()){
				$("#GP_CD_ERROR").addClass("d-none");
				$("#DEPART_NM_ERROR").addClass("d-none");
				checkGroupId();
			} else {
				if($("#GP_CD").val().length < 1) {
					$("#GP_CD_ERROR").removeClass("d-none");
				} else $("#GP_CD_ERROR").addClass("d-none");
				if($("#GP_NM").val().length < 1) {
					$("#DEPART_NM_ERROR").removeClass("d-none");
				} else $("#DEPART_NM_ERROR").addClass("d-none");
			}
		});
		
		<%--부서 정렬 초기화--%>
		<%--
		$("#initDepartSeq").on("click",function(){
			if(confirm("부서관리에 표출되는 순서로 메뉴를 재정렬합니다.\r\n 진행하시겠습니까?")){
				var groupData = $('#departmentManagement').jstree(true).get_json('#', {flat:true});
				var modelObject = $('#departmentManagement').jstree(true)._model.data;
				var seqList = [];
				for(idx in groupData){
					if(idx !=0) {
						var temp = new Object();
						temp.DEPTH = modelObject[menuData[idx].id].parents.length-1;
						temp.UPPER_GP_CD = $('#departmentManagement').jstree(true).get_node($('#departmentManagement').jstree(true).get_parent(menuData[idx].id)).original.GP_CD;
						temp.id = menuData[idx].id;
						seqList.push(temp)
					}
				}
				
				$.ajax({
					url: "/admin/initGroupSeq",
					type : "post",
					contentType: 'application/json',
					data : JSON.stringify({
						 'seqList' : seqList
					}),
					success:function(){					
						alert("부서정보를 재정렬 하었습니다.");
						location.reload();
					},
					error:function(request,status,error){				
						return false;
					}
				});
			}
			
		});
		--%>
		
		<%--전체 접기 / 펼치기--%>
		$("#openAll").on("click",function(){
			$('#departmentManagement').jstree(true).open_all();
		});
		
		$("#closeAll").on("click",function(){
			$('#departmentManagement').jstree(true).close_all();
		});
		
		
		<%--메뉴 수정시 confirm callback--%>	
		var updateCallBack = function() {
			var groupId = $("#GP_CD").val();
			<%--차후 validate 관련 기능 수정--%>
			var formJson = {};
			$.map($("#departInfoFrm").serializeArray(), function(n, i){
				 formJson[n['name']] = n['value'];
			});
			cont.updateGroup(formJson, groupId);
			_modal.hide();
		}
		<%--메뉴 삭제시 confirm callback--%>
		var deleteCallBack = function() {
			var data = cont.targetData; 
			var inst = $.jstree.reference(data.reference);
	        var obj = inst.get_node(data.reference);
	       
	        cont.deleteGroup({
	        	'IDX' : obj.original.IDX,
	        	'UPPER_GP_CD' : obj.original.UPPER_GP_CD,
	        	'GP_CD' : obj.original.GP_CD,
	        	'CHILDREN' : getSeqList(obj.children_d)
	       	}); 
	        $("#departInfoFrm")[0].reset();
	        inst.delete_node(obj);
	        _modal.hide();
		}
		
		$("#GROUP_FILE_UPLOAD_BTN").on("click", function() {
	        // 등록할 파일 리스트
	        var fileCnt = pamsFileCount();
	        
	        // 파일이 있는지 체크
	        if(fileCnt == 0){
	            // 파일등록 경고창
	            alert("<spring:message code="departmentManagement.021" text="코드 정보 파일을 업로드 하세요."/>");
	            return false;
	        }
	        
	        if(confirm("<spring:message code="departmentManagement.022" text="기존에 있던 부서 정보가 삭제됩니다."/> \r\n <spring:message code="departmentManagement.023" text="등록 하시겠습니까?" />")){
                
	            $.ajax({
	                url:"/admin/uploadGroupFile",
	                type:'POST',
	                enctype:'multipart/form-data',
	                processData:false,
	                contentType:false,
	                dataType:'json',
	                cache:false,
	                data:getFilData(),
	                success:function(result){
	                    if(result.RETURN_TYPE == "SUCCESS") {
	                    	alert("<spring:message code="departmentManagement.026" text="업로드에 성공하셨습니다."/>");
	                    	location.reload();
	                    } else if(result.RETURN_TYPE == "EMPTY"){
		                    alert("<spring:message code="departmentManagement.024" text="파일이 존재하지 않습니다."/>");
	                    } else if(result.RETURN_TYPE == "ERR"){
		                    alert("<spring:message code="common.009" text="엑셀 파일에 문제가 발생하였습니다. 파일을 확인하세요."/>");		                    
	                    } else if(result.RETURN_TYPE == "OVERLAB"){
		                    alert("<spring:message code="departmentManagement.025" text="그룹코드가 중복되었습니다. 파일을 확인하세요."/>");		                    
	                    } else{
		                    alert("<spring:message code="departmentManagement.028" text="관리자께 문의해 주십시오."/>");
		                    location.reload();
	                    }
	                }
	            });
	        }
		});
		
		loadPAMsFile("EXE_FILE_DIV", {
			 maxFileCount 	: 1
			,pamsFileType : ['xls']
		});

	});

    function enterSearch() {
        if(window.event.keyCode == 13) {
			$("#SEARCH_GROUP_BTN").click();
        }
	}
</script>
<section>
<%--
    <div class="x_panel">
        <div class="col-xs-1">
            <div class="x_title">
                <h2>
                    부서검색
                </h2>
            </div>
        </div>
        <div class="col-xs-11">
            <div class="input-group">
                <select id="SEARCH_CATEGORY1" style="width:15%" name="SEARCH_CATEGORY1" class="form-control custom-select">
                    <option value="0" selected>상위부서</option>
                </select>
                <select id="SEARCH_CATEGORY2" style="width:10%" name="SEARCH_CATEGORY2" class="form-control custom-select">
                    <option value="01">부서명</option>
                    <option value="02">부서코드</option>
                </select>
                <input class="form-control" style="width:50%; height:auto;"id="SEARCH" type="search" placeholder="부서명이나 부서코드를 정확히 입력하셔야 합니다." onkeypress="enterSearch();">
                <span class="input-group-btn">
                    <button type="submit" id="SEARCH_GROUP_BTN" class="btn btn-dark">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    </div>
 --%>
    <div class="row">
	    <div class="col-xs-6">
	   	 	<%--부서 트리 표출 위치--%>    	 	
	   	 	<div class="x_panel">
	   	 		<div class="x_underline">
		   	 		<div class="col-xs-6">
						<div class="x_title">
							<h2>
								<spring:message code="departmentManagement.030" text="부서 목록"/>
							</h2>
						</div>
					</div>
					
					<div class="col-xs-6 btn-area">
					<%--
	   	 				<button type="button" class="btn btn-secondary btn-primary" id="initDepartSeq" title="부서관리의 부서순서와 실제 메뉴가 순서가 맞지 않을 때 사용">
							부서정보 재정렬
						</button>
					 --%>		    	 	
						<button type="button" class="btn btn-secondary btn-primary" id="openAll">
							<i class="fa fa-expand "></i>&nbsp;<spring:message code="departmentManagement.012" text="펼치기"/>
						</button>
						<button type="button" class="btn btn-secondary btn-primary" id="closeAll">
							<i class="fa fa-compress"></i>&nbsp;<spring:message code="departmentManagement.013" text="접기"/>
						</button>
		   	 		</div> 
		   	 		<br><br>
		   	 	</div>
	    	 	<div id="departmentManagement" class="col-xs-12"></div>
	   	 	</div>
	    </div>		
	    <div class="col-xs-6 " id="_detailDiv" style="display:none;">
	    	<div class="x_panel">
		    	<div class="x_underline">
		    		<div class="col-xs-6">
						<div class="x_title">
							<h2>
								<spring:message code="departmentManagement.014" text="상세정보"/>
							</h2>
						</div>
					</div>
					<div class="col-xs-6 btn-area">
						<button type="button" class="btn btn-secondary btn-info" id="updateBtn">
							<i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message code='departmentManagement.004' text='수정'/>
						</button> 		
					</div>
					<br><br>
				</div>
	    	 	<div class="x_content">
	    	 		<%--메뉴 상세정보 표출 위치 표출 위치--%>
			        <form class="needs-validation was-validated" id="departInfoFrm">
			        
			        	<input type="hidden" id="IDX" name="IDX"/>
			        <%--
			        	<input type="hidden" id="DEPTH" name="DEPTH"/>	
			        	<input type="hidden" id="GP_NM" name="GP_NM"/>	
			        	<input type="hidden" id="UPPER_GP_CD" name="UPPER_GP_CD"/>
			         --%>				        						        	
						<div class="row g-2">										
							<div class="col-sm-12 p-1">
								<label for="UPPER_GP_CD" class="form-label"><spring:message code="departmentManagement.015" text="상위부서코드"/></label>
								<input type="text" class="form-control was-validated" id="UPPER_GP_CD" name="UPPER_GP_CD" value="" required readonly>	
							</div>
							
							<div class="col-sm-12 p-1">
								<label for="GP_CD" class="form-label"> <spring:message code="departmentManagement.018" text="부서코드"/></label>
								<input type="text" class="form-control" id="GP_CD" name="GP_CD" value="" required>
								<div class="invalid-feedback d-none" id="GP_CD_ERROR">
									<spring:message code="departmentManagement.016" text="부서 코드를 입력하여 주십시오."/> 
								</div>
							</div>
							<div class="col-sm-12 p-1">
								<label for="GP_NM" class="form-label"><spring:message code="departmentManagement.020" text="부서명"/></label>							             
								<input type="text" class="form-control" id="GP_NM" name="GP_NM" value="" required>	
								<div class="invalid-feedback d-none" id="DEPART_NM_ERROR">
							    	<spring:message code="departmentManagement.027" text="부서명을 입력하여 주십시요."/>
							    </div>
							</div>
							<div class="col-sm-12 p-1">
								<label for="GP_DC" class="form-label"><spring:message code="departmentManagement.019" text="부서설명"/></label>
								<input type="text" class="form-control" name="GP_DC" id="GP_DC" placeholder="" value="">							            
							</div>
							<div class="col-sm-6 p-1">
								<label for="DEPTH" class="form-label"><spring:message code="departmentManagement.017" text="깊이" /></label>
								<input type="text" class="form-control" name="DEPTH" id="DEPTH" placeholder="" value="" required readonly>							            
							</div>
							<div class="col-sm-6 p-1">
								<label for="USE_YN" class="form-label"><spring:message code="departmentManagement.029" text="사용유무" /></label>
								<select class="form-control" name="USE_YN" id="USE_YN">
									<option value="Y">Y</option>
									<option value="N">N</option>
								</select>							            
							</div>
						</div>
					</form>		    	 		
	    	 	</div>
	   	 	</div>
		</div>
	</div>
	
	<div class="x_panel">
		<div class="col-xs-12">
			<nav class="navbar navbar-light">
				<div class="col-xs-6">
					<div class="x_title">
						<h5>부서 업로드</h5>
					</div>
				</div>
				<div class="col-xs-6 btn-area" style="padding-top:3px;">
					<button type="button" class="btn btn-primary" onclick="window.open('/uploadsample/GROUP.xls');">
						양식	다운로드
					</button>

					<button id="GROUP_FILE_UPLOAD_BTN" type="button" class="btn btn-dark">
						업로드
					</button>
				</div>
				<div class="col-xs-12">
					<div id="EXE_FILE_DIV" class="card-body"></div>
				</div>
			</nav>
		</div>
	</div>
</section>