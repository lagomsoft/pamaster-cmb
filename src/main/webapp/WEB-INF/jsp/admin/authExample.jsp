<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style type="text/css">  
<c:if test="${'Y' != IS_SYS_ADMIN}"> 
[auth-role] {
	display : none;
}
</c:if>
<c:forEach items="${AUTH_CD.split(',')}" var="authCd">
[auth-role="${authCd}"]{
	display : block;
}
</c:forEach>
</style>    
<script type="text/javascript">
$(function(){
	var auCon = {
		init : function(){
			if("Y" != "${IS_SYS_ADMIN}"){
				<%--시스템 관리자가 아닐 경우에만 동작--%>
				var auList = "${AUTH_CD}".split(",");
				$("[auth-role]").each(function(){
					var role=$(this).attr("auth-role");
					if(auList.indexOf(role) < 0){ 
						$(this).remove();  
					}
				})
			}			
		}
	}	
	auCon.init();
});
</script>
<div>
	권한 목록 :
	<c:forEach items="${AUTH_CD.split(',')}" var="authCd">
	${authCd} 
	</c:forEach><br/><br/>
	권한 목록에 따른 객체 표출 : <br/>
	수정 버튼 : <input type="button" auth-role="202" value="수정버튼"/><br/> 
	승인 버튼 : <input type="button" auth-role="300" value="승인버튼"/><br/><br/>	
	
	권한 목록에 따른 프로세스 흐름 제어시 : <br/> 
	<script type="text/javascript">
	var btnAlert = function(){
		<c:choose>
			<c:when test="${AUTH_CD.contains('202')}">
			alert('수정 권한 있음');
			</c:when>
			<c:otherwise>
			alert('수정 권한 없음.');
			</c:otherwise>
		</c:choose>
	}
	</script>
	버튼 : <input type="button" value="버튼" onclick="btnAlert()"/><br/>
	
	
	<button type="button" class="btn btn-secondary btn-sm btn-primary">
		<i class="fa"><%@include file="/img/bootstrap-icons/arrows-collapse.svg" %></i>&nbsp;접기
	</button>
	<button type="button" class="btn btn-secondary btn-sm btn-primary">
		<i class="fa"><%@include file="/img/bootstrap-icons/arrows-expand.svg" %></i>&nbsp;펼치기
	</button>
	<button type="button" class="btn btn-secondary btn-sm btn-primary">
		<i class="fa"><%@include file="/img/bootstrap-icons/card-list.svg" %></i>&nbsp;조회
	</button>
	<button type="button" class="btn btn-secondary btn-sm btn-primary">
		<i class="fa"><%@include file="/img/bootstrap-icons/chat-square-text-fill.svg" %></i>&nbsp;팝업
	</button>
	<button type="button" class="btn btn-secondary btn-sm btn-success">
		<i class="fa"><%@include file="/img/bootstrap-icons/pencil-fill.svg" %></i>&nbsp;추가
	</button>
	<button type="button" class="btn btn-secondary btn-sm btn-info">
		<i class="fa"><%@include file="/img/bootstrap-icons/pencil-square.svg" %></i>&nbsp;수정
	</button>
	<button type="button" class="btn btn-secondary btn-sm btn-danger">
		<i class="fa"><%@include file="/img/bootstrap-icons/eraser-fill.svg" %></i>&nbsp;삭제
	</button>
</div>