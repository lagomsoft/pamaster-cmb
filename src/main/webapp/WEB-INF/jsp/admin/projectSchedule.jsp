<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<!DOCTYPE html>
<style type="text/css">
._ellipsis {
      overflow:hidden;
      text-overflow:ellipsis;
      white-space:nowrap;
}
</style>



<script type="text/javascript">

    // 수정 리스트
    var checkList = new Array();
    var checkCnt = 0;
    
    var updateGrid1List = new Array();

    var updateKeyList = new Array();
    
	var pageIndex = 1;
	var pageSize = 10;
	var pageButtonCount = 5;

    //선택 로우
    var isUpdateRow1 = false;
	var selectRow = null;
	var selectItme = null;
	var selectGrid1Row = null;
	
	var dataMap = {
		data : new Array(),
		itemsCount : 0
	};
	 
    $(function () {
    	<%--ellipsis처리 및 타이틀 처리를 위한 renderer--%>
    	var _gridRenderer = {
   			cellRenderer : function(value,item){
   	           	var rtn = $("<td>",{
   	           		"text" :value,
   	           		"title" :value,
   	           		"class" : "_ellipsis"
   	           	})
   	           	return rtn;
   	        }	
   		}
        
        $("#jsGrid1").jsGrid({
            width: "100%",
            //height: "650px",
            editing: true,
            sorting: false, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            paging : true,
			pageLoading : true,
			pagerFormat : "{first} {pages} {last}",
			pageFirstText : "<spring:message code='common.067' text='처음' />",
			pageLastText : "<spring:message code='common.109' text='끝' />",
			pageNavigatorNextText: "<spring:message code='common.066' text='다음' />",
    		pageNavigatorPrevText: "<spring:message code='common.065' text='이전' />",
			pageSize : pageSize,
			pageButtonCount : pageButtonCount,
			onPageChanged : function(args) {
				pageIndex = args.pageIndex;
				loadScheduleList(args);
			},

            rowClick : function(args){
                if(isUpdateRow1){
    	            isUpdateRow1 = false;
    	            $("#jsGrid1").jsGrid("updateItem");
                }
                
                $("#jsGrid1").jsGrid("cancelEdit");
                
            	if(checkList[selectGrid1Row]){
                	$('#PROJECT_SCHEDULE_CHECKBOX_'+selectGrid1Row).attr("checked", "checked");
                }
            },
            rowDoubleClick : function(args){
                
            	selectGrid1Row = args.itemIndex;
            	this.editItem($(this._body).find("tr").eq(selectGrid1Row));

            	if(checkList[selectGrid1Row]){
                	$('#PROJECT_SCHEDULE_CHECKBOX_'+selectGrid1Row).attr("checked", "checked");
                }
            	
            },
            controller: {
                loadData: function() {
                    
                    var d = $.Deferred();
                    d.resolve({
                        	 data : $.map(dataMap.data, function (item, itemIndex) {
                       					return $.extend(item, { "index": itemIndex});
							 })
							,itemsCount : dataMap.itemsCount
                    });
                    
                    return d.promise();
                    
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                { name: "RVW_SN",	title:'<spring:message code="projectSchedule.003" text="처리누적" />', width: 60 , align:'center' }
               ,{ name: "RGSDE",	title:'<spring:message code="projectSchedule.021" text="등록일시" />', width: 100 }
               ,{ name: "PRJCT_NM",title:'<spring:message code="projectSchedule.004" text="프로젝트명" />', width: 200 , cellRenderer : _gridRenderer.cellRenderer}
               ,{ name: "STEP_CD",title:'<spring:message code="projectSchedule.020" text="요청내용" />', width: 100
                   , itemTemplate: function (_, item) {
                       
                       var txt = '';
                       
                       switch (item.STEP_CD) {
						<c:forEach var="stepCd" items='${PROJECT_STEP}' varStatus="status">
							case '${stepCd.CD}':
								txt = '${stepCd.CD_NM}';
								break;
						</c:forEach>
                       
						}
                       
                       return txt;
	                }
                }
               
                ,{ name: "APL_CT_NM",title:'<spring:message code="projectSchedule.005" text="신청기관" />', 	 width: 100}
				,{ name: "CHARGER", title : '<spring:message code="projectSchedule.006" text="담당자(ID)" />', width: 100
					, itemTemplate: function (_, item) {
					   var charger = '<spring:message code="projectSchedule.041" text="삭제됨" />';
					   if(item.CHARGER != null) {
					       charger = item.CHARGER;
					   }
					   var register = '<spring:message code="projectSchedule.042" text="삭제된계정" />';
					   if(item.REGISTER_ID != null) {
					       register = item.REGISTER_ID;
					   }
                       var txt = charger + "(" + register + ")";
                       return txt;
	                }
				}
				,{ name: "PRC_CNT", title : '<spring:message code="projectSchedule.007" text="처리건수" />', width: 100}
				<%--,{ name: "IDNTFC_COL_SPECIFIC_CNT", title : '<spring:message code="projectSchedule.027" text="식별가능<br/>컬럼수" />', width: 100}
				,{ name: "ETC_COL_SPECIFIC_CNT", title : '<spring:message code="projectSchedule.028" text="일반정보<br/>컬럼수" />', width: 100} --%>
                ,{ name: "RSVTIME", title:'<spring:message code="projectSchedule.029" text="배치설정" />', type: "select", items : [
	                	{ Name: "<spring:message code='projectSchedule.030' text='실시간처리' />", Id: "NOW" }
	                   ,{ Name: "<spring:message code='projectSchedule.040' text='배치 (1분이내)' />", Id: "NEXT" }
	                   ,{ Name: "00:00", Id: "00:00" }
	                   ,{ Name: "01:00", Id: "01:00" }
	                   ,{ Name: "02:00", Id: "02:00" }
	                   ,{ Name: "03:00", Id: "03:00" }
	                   ,{ Name: "04:00", Id: "04:00" }
	                   ,{ Name: "05:00", Id: "05:00" }
	                   ,{ Name: "06:00", Id: "06:00" }
	                   ,{ Name: "07:00", Id: "07:00" }
	                   ,{ Name: "08:00", Id: "08:00" }
	                   ,{ Name: "09:00", Id: "09:00" }
	                   ,{ Name: "10:00", Id: "10:00" }
	                   ,{ Name: "11:00", Id: "11:00" }
	                   ,{ Name: "12:00", Id: "12:00" }
	                   ,{ Name: "13:00", Id: "13:00" }
	                   ,{ Name: "14:00", Id: "14:00" }
	                   ,{ Name: "15:00", Id: "15:00" }
	                   ,{ Name: "16:00", Id: "16:00" }
	                   ,{ Name: "17:00", Id: "17:00" }
	                   ,{ Name: "18:00", Id: "18:00" }
	                   ,{ Name: "19:00", Id: "19:00" }
	                   ,{ Name: "20:00", Id: "20:00" }
	                   ,{ Name: "21:00", Id: "21:00" }
	                   ,{ Name: "22:00", Id: "22:00" }
	                   ,{ Name: "23:00", Id: "23:00" }
	                   ,{ Name: "24:00", Id: "24:00" }
	               ], valueField: "Id", textField: "Name", width: 100}
               ,{ name: "PRIORT", title:'<spring:message code="projectSchedule.031" text="우선순위" />', type: "select", items : [
	                	{ Name: "<spring:message code='projectSchedule.032' text='최우선처리' />", Id: 1 }
	                   ,{ Name: "<spring:message code='projectSchedule.033' text='우선처리' />", Id: 2 }
	                   ,{ Name: "<spring:message code='projectSchedule.034' text='보통' />", Id: 3 }
	                   ,{ Name: "<spring:message code='projectSchedule.035' text='낮음' />", Id: 4 }
	                   ,{ Name: "<spring:message code='projectSchedule.036' text='매우낮음' />", Id: 5 }
	               ], valueField: "Id", textField: "Name", width: 100}
               ,{ name: "BGNDE",title:'<spring:message code="projectSchedule.008" text="시작시간" />', width: 100}
               ,{ name: "PG_TIME", 	 title:'<spring:message code="projectSchedule.009" text="경과시간" />', width: 100 }
               ,{ name: "PROGRS_CD", title : '<spring:message code="projectSchedule.010" text="진행상태" />', width: 100 , align:'center'
                   , itemTemplate: function (_, item) {
                       
                       var txt = '';
                       
                       switch (item.PROGRS_CD) {
							case '100':
								txt = '<spring:message code="projectSchedule.011" text="승인대기" />';
								break;
							case '200':
								txt = '<spring:message code="projectSchedule.012" text="진행 예약" />';
								break;
							case '900':
								txt = '<spring:message code="projectSchedule.014" text="처리완료" />';
								break;
							case '999':
								txt = '<spring:message code="projectSchedule.015" text="처리오류" />';
								break;
							default:
								txt = '<spring:message code="projectSchedule.013" text="진행 중" />';
						}
                       
                       return txt;
	                }
                }
               ,{ name:"PRJCT_NO", type: "text", css:"d-none", width: 0 }
            ]
        });
        
		$("#GET_DATA").on("click", function(){
			loadScheduleList();
		});
		
        $("#jsGrid1").on("change", "input,select", function(){
        	updateGrid1List[selectGrid1Row] = selectGrid1Row;
        	isUpdateRow1 = true;
        });
        
		$("#SCHEDULE_START").on("click", function(){
			if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
			
			var dataList = new Array();
	        var dataCnt = 0;
	        var gridData = $("#jsGrid1").jsGrid("option", "data");
	        
			for (var key in updateGrid1List) {
				if(!gridData[key].RSVTIME){
					alert("<spring:message code='projectSchedule.037' text='배치 시간을 설정하세요.' />");
					$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
					return;
				}
				if(!gridData[key].RSVTIME){
					alert("<spring:message code='projectSchedule.038' text='우선순위를 선택하세요.' />");
					$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
					return;
				}
				
				dataList.push({
	            	 PRJCT_NO : gridData[key].PRJCT_NO
	            	,RVW_SN : gridData[key].RVW_SN
					,STEP_CD : gridData[key].STEP_CD
					,PROGRS_CD : "200"
					,RSVTIME : gridData[key].RSVTIME
					,PRIORT  : gridData[key].PRIORT
				});
				
				dataCnt++;
	        }
	        
			if(dataList.length == 0){
				alert("<spring:message code='projectSchedule.039' text='처리할 프로젝트의 정보를 변경하세요.' />");
				return;
			}
	        
	        if( confirm('<spring:message code="common.007" text="등록 하시겠습니까?" />') ){
		        
		        $.ajax({
		            url: "/admin/startProjectSchedule",
		            type : "post",
		            contentType: 'application/json',
		            data : JSON.stringify({
		            	SCHEDULE_LIST : dataList
		            })
		        }).done(function(response) {
		            if(response.SUCCESS == "0"){
			            alert("<spring:message code='common.013' text='저장되었습니다.' />");
			            location.reload();
			        }
		        	
		        });
		        
	        }
		});
		
		$("#GET_DATA").click();
    });

    function loadScheduleList(){
        
		updateGrid1List = new Array();
		updateKeyList = new Array();
		
    	$.ajax({
            url: "/admin/getProjectScheduleData",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
            	 CNT_CD : "ko"
				,PAGE_INDEX : (Number(pageIndex)-1) * Number(pageSize)
				,PAGE_SIZE : pageSize
            })
        }).done(function(response) {
            
        	dataMap = {
				data : response.DATA_LIST,
				itemsCount :response.COUNT
			};
			
        	$("#jsGrid1").jsGrid("loadData");
        });
        
	}
    
	</script>

<section>

	<div class="col-xs-12 btn-area">
		 <button id="SCHEDULE_START" type="button" class="btn btn-dark">
			<spring:message code="projectSchedule.018" text="처리" />
		</button>
		&nbsp;
		<button id="GET_DATA" type="button" class="btn btn-dark">
			<spring:message code="common.036" text="조회" />
		</button>
	</div>
	<div class="x_panel">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_content">
					<div id="jsGrid1" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
				</div>
			</div>
		</div>
	</div>
</section>
