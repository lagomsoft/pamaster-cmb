<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<style type="text/css">
._ellipsis {
  	  width:200px;
      overflow:hidden;
      text-overflow:ellipsis;
      white-space:nowrap;
}
</style>

<link rel="stylesheet" href="../plugins/bootstrap-datepicker/css/bootstrap-datetimepicker.min.css">
<script src="../plugins/bootstrap-datepicker/js/bootstrap-datetimepicker.js"></script>

<script type="text/javascript">
<%-- [S]datepicker --%>
$(document).ready(function () {
	$("#ST_DT").datetimepicker({ format: 'yyyy-mm-dd hh:ii', minuteStep: 1 });
	$("#ED_DT").datetimepicker({ format: 'yyyy-mm-dd hh:ii', minuteStep: 1 });
});
<%-- [E]datepicker --%>
</script>

<script type="text/javascript">
	var pageIndex_login = 1;
	var pageIndex_hist = 1;
	var pageIndex_error = 1;
	var pageSize = 10;
	
	var loginLogCount = 0;
	var connectionLogCount = 0;
	
	var pageIndex_proj = 1;
	var pageSize_proj = 6;
	
	var pageButtonCount = 10;
	var search_ty1 = null;
	var search_contents1 = null;
	var search_ty2 = null;
	var search_contents2 = null;
	var search_ty3 = null;
	var search_contents3 = null;
	var type = null;
	var stDt = '';
	var edDt = '';
	var _gridRenderer = {
		cellRenderer : function(value,item){
           	var rtn = $("<td>",{
           		"text" :value,
           		"title" :value
           	})
           	return rtn;
        }	
	};

    $(window).on("load", function() {
		$('.fail').parent().parent().css("color", "red");
	});
	
	
    <%-- [S]접속기록 그리드 --%>
    $(function (type) {
        $("#jsGrid1").jsGrid({
            width			: "100%",
			height			: "auto",
			sorting			: false,
			autoload		: false,
			paging			: true,
			pageLoading 	: true,
			pagerFormat 	: "{first} {pages} {last}",
			pageFirstText	: "<spring:message code='common.067' text='처음' />",
			pageLastText	: "<spring:message code='common.109' text='끝' />",
			pageNavigatorNextText: "<spring:message code='common.066' text='다음' />",
    		pageNavigatorPrevText: "<spring:message code='common.065' text='이전' />",
			pageSize		: pageSize,
			pageButtonCount	: pageButtonCount,
			onPageChanged	: function(args) {
				pageIndex_login = args.pageIndex;
				$("#jsGrid1").jsGrid("loadData").done(function(){
					failEffect();
				});
			},
			rowClick : function(args) {
				$("#_userActionDetail").modal('show');
				var params = {};
				params = args.item;
				retrieveUserActonGrid(params);
			},
			controller : {
				loadData : function(args) {
					var d = $.Deferred();
					$.ajax({
						url : "/admin/loginLog",
						type : "post",
						contentType : 'application/json',
						data : JSON.stringify({
							PAGE_INDEX		: pageIndex_login,
							PAGE_SIZE		: args.pageSize,
							SEARCH_TY		: search_ty1,
							SEARCH_CONTENTS	: search_contents1,
							ST_DT			: stDt,
							ED_DT			: edDt
						})

					}).done(function(response) {
						var da = { data : response.LogList, itemsCount : response.COUNT };
						d.resolve(da);
					});
					return d.promise();
				}
			},
            fields: [
                { name: "CONECT_DT"		, title : "<spring:message code='logManagement.001' text='접속일자' />"	, type: "date", width: 50},
                { name: "END_DT"		, title : "<spring:message code='logManagement.002' text='종료일자' />"	, type: "date", width: 50},
                { name: "USR_ID"		, title : "<spring:message code='logManagement.003' text='유저아이디' />"	, type: "text", width: 50},
                { name: "USR_NM"		, title : "<spring:message code='logManagement.004' text='유저이름' />"	, type: "text", width: 50},
                { name: "CONECT_IP"		, title : "<spring:message code='logManagement.005' text='사용자 IP' />"	, type: "text", width: 50},
                { name: "CONECT_WBSR"	, title : "<spring:message code='logManagement.006' text='브라우저' />"	, type: "text", width: 50},
                { name: "REMARKS"		, title : "<spring:message code='logManagement.007' text='비고' />"		, type: "text", width: 50
                    , itemTemplate: function(value, item) {
						if("FAIL" == value) {
							var obj = $('<font>').text(value).addClass("fail").css("color", "red");
							return obj;
						}
                    }
                }
            ]
        });
        <%-- [E]접속기록 그리드 --%>
        
        <%-- [S]프로젝트별 접속기록 --%>
        $("#jsGrid2").jsGrid({
        	width			: "100%",
			height			: "auto",
			sorting			: false,
			autoload		: true,
			paging			: true,
			pageLoading 	: true,
			pagerFormat 	: "{first} {pages} {last}",
			pageFirstText	: "<spring:message code='common.067' text='처음' />",
			pageLastText	: "<spring:message code='common.109' text='끝' />",
			pageNavigatorNextText: "<spring:message code='common.066' text='다음' />",
    		pageNavigatorPrevText: "<spring:message code='common.065' text='이전' />",
			pageSize		: pageSize,
			pageButtonCount	: pageButtonCount,
			onPageChanged	: function(args) {
				pageIndex_hist = args.pageIndex;
				$("#jsGrid2").jsGrid("loadData");
			},
			rowClick : function(args) {
				if(!args.event.target.firstElementChild && !args.event.target.hasAttribute('checkicon')) {
					$("#_logListDetailByProject").modal('show');
					retrieveLogListByProject(args);
				}
			},
            controller : {
				loadData : function(args) {
					var d = $.Deferred();
					
					$.ajax({
						url : "/admin/connectLogByProject",
						type : "post",
						contentType : 'application/json',
						data : JSON.stringify({
							PAGE_INDEX		: pageIndex_hist,
							PAGE_SIZE		: args.pageSize,
							SEARCH_TY		: search_ty2,
							SEARCH_CONTENTS : search_contents2
						})

					}).done(function(response) {
						var da = { data : response.LogList, itemsCount :response.COUNT };
						d.resolve(da);
					});

					return d.promise();
				}
			},
			editItem: function(item) {
			},
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
            	{ name: "CHECK"		, title: ""				, type: "text"		, width: 5 	, align: 'center'	, readOnly: true
	            	, itemTemplate: function(_, item) {
	            		var checkItem		= $('<input type="checkbox" class="form-check-input checkedItem" checkIcon="icon">')
	            		var checkItemLabel	= $('<label class="form-check-label" for=""></label>');
	            		return $('<div class="form-check">').append(checkItem).append(checkItemLabel);
	            	}
            	},
//            	{ name: "ALIAS_NO"	, title: "<spring:message code='logManagement.025' text='프로젝트번호' />"	, type: "text"	, width: 60 , align: 'center'},
                { name: "PRJCT_NO"  , title: "<spring:message code='logManagement.025' text='프로젝트번호' />" , type: "text"  , width: 60 , align:'center' },
                { name: "PRJCT_NM"	, title: "<spring:message code='logManagement.013' text='프로젝트명' />"	, type: "text"	, width: 220 },
                { name: "STEP_CD"	, title: "<spring:message code='logManagement.026' text='진행상태' />"	, type: "text"	, width: 10 , visible: false },
                { name: "STEP_NM"	, title: "<spring:message code='logManagement.026' text='진행상태' />"	, type: "text"	, width: 60 , align:'center' },
                { name: "JOIN_FLAG"	, title: "<spring:message code='logManagement.027' text='결합여부' />"	, type: "text"	, width: 10	, visible: true	, align: 'center' },
//              { name: "CHARGER"	, title: "<spring:message code='logManagement.028' text='요청자' />"		, type: "text"	, width: 30 align:'center'},
                { name: "RVW_SN"	, title: "RVW_SN"														, type: "text"	, width: 10	, visible: false },
                { name: "ACCESS_CNT", title: "<spring:message code='logManagement.010' text='행 갯수' />"		, type: "text"	, width: 30 , align:'center'},
            ]
        })
        <%-- [E]프로젝트별 접속기록 --%>
        
        
        <%-- [S]프로시저 에러기록 --%>
        $("#jsGrid3").jsGrid({
        	width : "100%",
			height : "auto",
			sorting : false, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
			autoload : true,
			paging : true,
			pageLoading : true,
			pagerFormat : "{first} {pages} {last}",
			pageFirstText : "<spring:message code='common.067' text='처음' />",
			pageLastText : "<spring:message code='common.109' text='끝' />",
			pageNavigatorNextText: "<spring:message code='common.066' text='다음' />",
    		pageNavigatorPrevText: "<spring:message code='common.065' text='이전' />",
			pageSize : pageSize,
			pageButtonCount : pageButtonCount,
			onPageChanged : function(args) {
				pageIndex_error = args.pageIndex;
				$("#jsGrid3").jsGrid("loadData");
			},
			rowClick : function(args) {				
				$("#_detailPopupErr").find("[data-nm]").text("");
				for(idx in args.item){					
					$("#_detailPopupErr").find("[data-nm='"+idx+"']").text(args.item[idx]);
				}		
				$("#_detailPopupErr").find("[data-nm='PROC_NM']").text(
						args.item.PROC_NM != null ? args.item.PROC_NM +"("+args.item.PROC_CD +")" : args.item.PROC_CD
				);
				$("#_detailPopupErr").modal('show');				
			},
            controller : {
				loadData : function(args) {

					var d = $.Deferred();
					$.ajax({
						url : "/admin/procErrorLog",
						type : "post",
						contentType : 'application/json',
						data : JSON.stringify({
							PAGE_INDEX : pageIndex_error,
							PAGE_SIZE : args.pageSize,
							PROC_TY : type,
							SEARCH_TYPE : search_ty3,
							SEARCH_CONTENTS : search_contents3
						})

					}).done(function(response) {
						if(response.LogList){
							$("#custom-tabs-error-tab").show();
							var da = {
								data : response.LogList,
								itemsCount :response.COUNT
							};
							d.resolve(da);
						}else{
							$("#custom-tabs-error-tab").hide();
						}

					});
					return d.promise();
				}
			},
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
            	{ name: "RGSDE", title : "<spring:message code='logManagement.011' text='일자' />", type: "date", width: 50 },
            	{ name: "PROC_TY", title : "<spring:message code='logManagement.012' text='프로젝트 타입' />", type: "text", width: 50 },
                { name: "PRJCT_NM", title : "<spring:message code='logManagement.013' text='프로젝트명' />", type: "text", width: 100 ,css : "_ellipsis", cellRenderer : _gridRenderer.cellRenderer},
                { name: "PROC_NM", title : "<spring:message code='logManagement.014' text='프로시저명' />", type: "text", width: 100 ,css : "_ellipsis", cellRenderer : function(value,item){
	                   	var rtn = $("<td>",{
	                   		"text" :item.PROC_NM != null ? item.PROC_NM +"("+item.PROC_CD +")" : item.PROC_CD,
	                   		"title" : item.PROC_NM != null ? item.PROC_NM +"("+item.PROC_CD +")" : item.PROC_CD
	                   	})
	                   	return rtn;
	                }	
                },               
                { name: "ERR_MSG", title : "<spring:message code='logManagement.015' text='에러메세지' />", type: "text", width: 200 ,css : "_ellipsis", cellRenderer : _gridRenderer.cellRenderer}              
            ]
        })
        <%-- [E]프로시저 에러기록 --%>

        
        $("#SEARCH_BTN1").on("click", function() {
			search_ty1 = $("#SEARCH_TY").val();
			search_contents1 = $("#SEARCH").val();
			stDt = $("#ST_DT").val();
			edDt = $("#ED_DT").val();
			
			if(stDt > edDt) {
				alert('조회 시작일은 종료일보다 이전이어야 합니다.');
				return;
			}
			
			var $jsGrid = $("#jsGrid1");
			$jsGrid.jsGrid({pageIndex : 1});
			pageIndex_login = 1;
			$jsGrid.jsGrid("loadData");
			
		});

        $("#SEARCH_BTN2").on("click", function() {
			search_ty2 = $("#SEARCH_STEP_CD").val();
			search_contents2 = search_ty2 != '000' ? $('#SEARCH_PROJECT_CD').val() : $("#SEARCH2").val();
			
			var $jsGrid = $("#jsGrid2");
			$jsGrid.jsGrid({pageIndex : 1});
			pageIndex_hist = 1;
			$jsGrid.jsGrid("loadData");
		});
        
        <%-- 프로젝트별 진행상태 변경 선택 --%>
        $('#SEARCH_STEP_CD').on('change', function() {
        	<%-- 프로젝트 선택 초기화 --%>
        	$('#SEARCH_PROJECT_CD').empty();
    		$('#SEARCH_PROJECT_CD').append($('<option value="">프로젝트 선택</option>'));
    		
    		if($(this).val() == '000') {
        		$('.inputConvert').eq(0).css('display', 'none');
        		$('.inputConvert').eq(1).css('display', 'block');
        	} else {
        		$('.inputConvert').eq(0).css('display', 'block');
        		$('.inputConvert').eq(1).css('display', 'none');
        	}
    		
        	if($(this).val() != '' && $(this).val() != '000') {
        		getProjectList($(this).val());
        	}
        })
        
        $("#SEARCH_BTN3").on("click", function() {
			search_ty3 = $("#SEARCH_TY3").val();
			search_contents3 = $("#SEARCH3").val();
			type = $("#SEARCH_TYPE").val();
			var $jsGrid = $("#jsGrid3");
			$jsGrid.jsGrid({pageIndex : 1});
			pageIndex_error = 1;
			$jsGrid.jsGrid("loadData");
		});
		
        <%-- [S]사용자 이력 Excel Export --%>
		$("#EXCEL_DOWNLOAD1").on("click", function(e) {
			
			e.preventDefault();
			var stDt			= $('#ST_DT').val();
			var edDt			= $('#ED_DT').val();
			var searchType		= $("#SEARCH_TY").val();
			var searchContent	= $('#SEARCH').val();
			
			if("" == stDt || "" == edDt) {
				alert('파일 다운로드는 기간 지정 후 이용하실 수 있습니다.');
				return;
			}
		
			if(stDt > edDt) {
				alert('시작일이 종료일보다 이전 일자여야 합니다.');
				return;
			}
			
			if(datediff() != 0) {
				alert('기간 지정은 최대 1주일까지 가능합니다.');
				return;
			}
			
			var $exportForm = $('<form>', { action: '/admin/retrieveAccessLogForExcel', method: 'post'})
			var $typeInput	= $('<input type="hidden" name="SEARCH_TY" value="'+searchType+'" >');
			var $contInput	= $('<input type="hidden" name="SEARCH_CONTENTS" value="'+searchContent+'" >');
			var $startInput	= $('<input type="hidden" name="ST_DT" value="'+stDt+'" >');
			var $endInput	= $('<input type="hidden" name="ED_DT" value="'+edDt+'" >');
			var $viewType	= $('<input type="hidden" name="VIEW_TYPE" value="U">');

			$exportForm.append($typeInput);
			$exportForm.append($contInput);
			$exportForm.append($startInput);
			$exportForm.append($endInput);
			$exportForm.append($viewType);
			$('body').append($exportForm);
			
			$exportForm.submit();
			$exportForm.remove();
		});
		<%-- [E]사용자 이력 Excel Export --%>
		
		<%-- [S]프로젝트별 Excel Export --%>
		$('#EXCEL_DOWNLOAD2').on('click', function(e) {
			var checkedList		= $('.checkedItem:checked');
			var checkedArray	= [];
			
			if(checkedList.length == 0) {
				alert('프로젝트 선택 후 다운로드가 가능합니다.');
				return;
			}
			
			for(var i=0; i<checkedList.length; i++) {
				var prjctNum = checkedList[i].parentElement.parentElement.parentElement.childNodes[1].innerText
				checkedArray.push(prjctNum);
			}
			
			var $exportForm = $('<form>', { action: '/admin/retrieveAccessLogForExcel', method: 'post'});
			var $projectNum	= $('<input type="hidden" name="PROJECT_NUM" value="'+checkedArray+'" >');
			var $viewType	= $('<input type="hidden" name="VIEW_TYPE" value="P">');
			$exportForm.append($projectNum);
			$exportForm.append($viewType);
			$('body').append($exportForm);
			
			$exportForm.submit();
			$exportForm.remove();
		});
		<%-- [E]프로젝트별 Excel Export --%>
		
        $("#custom-tabs-login-tab").on("click", function() {
			$("#jsGrid1").jsGrid("loadData").done( function() { });
        });
        
		$("#custom-tabs-login-tab").click();
		
		$("#SEARCH").keydown(function(key) {
			if(key.keyCode == 13) {
				$("#SEARCH_BTN1").click();
			}
		});
		$("#SEARCH2").keydown(function(key) {
			if(key.keyCode == 13) {
				$("#SEARCH_BTN2").click();
			}
		});
		$("#SEARCH3").keydown(function(key) {
			if(key.keyCode == 13) {
				$("#SEARCH_BTN3").click();
			}
		});

    })
    
    <%-- [S]사용자 활동내역 --%>
    function retrieveUserActonGrid(args) {
    	var items = args;
    	
    	$("#userActionGrid").jsGrid("destroy");
    	
    	$('#userActionGrid').jsGrid({
    		width      : '100%',
    		height     : '500px',
    		sorting    : false,
    		paging     : false,
    		autoload   : true,
    		pageLoading: true,
    		rowClick   : function(args) {
    			if(args.item.EXECUT_SQL != undefined ) {
	    			$("#_userActionQueryDetail").find("[data-nm]").text("");
	    			for(idx in args.item) {					
						$("#_userActionQueryDetail").find("[data-nm='"+idx+"']").text(args.item[idx]);
					}		
	    			$('#_userActionQueryDetail').modal('show');
    			}
    		},
    		
    		controller: {
    			loadData: function() {
    				var d = $.Deferred();
    				$.ajax({
    					url          : '/admin/actionLog',
    					type         : 'post',
    					contentType  : 'application/json',
    					data: JSON.stringify({
    						CONECT_DT: items.CONECT_DT,
    						CONECT_IP: items.CONECT_IP,
    						USR_ID   : items.USR_ID,
    						USR_NO   : items.USR_NO,
    						END_DT   : items.END_DT
    					})
    				}).done(function(response) {
    					$("#_userActionDetail").find("[data-nm]").text("");
    					args.RESULT_CNT = response.accessLogs.length; 
    					
						for(idx in args) {					
							$("#_userActionDetail").find("[data-nm='"+idx+"']").text(args[idx]);
						}		
    					
						var responseData = response.accessLogs;
						for (var t=0; t<responseData.length; t++) {
							if (responseData[t].SQL_EXIST == "Y") {
								responseData[t].SQL_EXIST = $("<button>").attr({"type":"button", "class":"btn bg-green"}).text("확인");
							} else {
								responseData[t].SQL_EXIST = '';
							}
						}
						
    					var da = { data: response.accessLogs, itemsCount: response.accessLogs.length };
    					d.resolve(da);
    				});
    				return d.promise();
    			}
    		},
    		fields: [
    			{ name: "CONECT_DT"  , title : "<spring:message code='logManagement.001' text='접속일자' />"	, type: "date" , width: 40 },
                { name: "PRJCT_NM"   , title : "<spring:message code='logManagement.013' text='프로젝트명' />"	, type: "text" , width: 100 },
                { name: "MENU_NM"    , title : "<spring:message code='menuList.018' text='메뉴명' />"			, type: "text" , width: 140 },
                { name: "QRY_ID"  	 , title : "<spring:message code='logManagement.022' text='쿼리 ID' />"	, type: "text" , width: 60	, visible: false },    			
                { name: "SQL_EXIST"  , title : "<spring:message code='logManagement.023' text='쿼리' />"		, type: "text" , width: 30 },    			
                { name: "EXECUT_SQL" , title : "<spring:message code='logManagement.009' text='접속기록' />"	, type: "text" , width: 140	, visible: false	, css : "_ellipsis"	, cellRenderer : _gridRenderer.cellRenderer},    			
                { name: "RESULT_CNT" , title : "<spring:message code='logManagement.010' text='행 갯수' />"	, type: "text" , width: 30 },
    		]
    	})
    }
    <%-- [E]사용자 활동내역 --%>
    
    
    <%-- [S]프로젝트 선택항목 로그 이력 호출 --%>
    function retrieveLogListByProject(args) {
    	var items		= args;
    	var prjctNo		= items.item.PRJCT_NO;
    	pageIndex_proj	= 1;
    	
    	$("#projectLogGrid").jsGrid("destroy");
    	$('#projectLogGrid').jsGrid({
    		width			: '100%',
    		height			: '500px',
    		sorting			: false,
    		paging			: true,
    		autoload		: true,
    		pageLoading		: true,
    		pagerFormat		: "{first} {pages} {last}",
			pageFirstText	: "<spring:message code='common.067' text='처음' />",
			pageLastText	: "<spring:message code='common.109' text='끝' />",
			pageNavigatorNextText: "<spring:message code='common.066' text='다음' />",
    		pageNavigatorPrevText: "<spring:message code='common.065' text='이전' />",
			pageSize		: pageSize,
			pageButtonCount : pageButtonCount,
			onPageChanged	: function(args) {
				pageIndex_proj = args.pageIndex;
    			$('#projectLogGrid').jsGrid('loadData');
    		},
    		rowClick	: function(args) {
    			if(args.item.EXECUT_SQL != undefined ) {
	    			$("#_userActionQueryDetail").find("[data-nm]").text("");
	    			for(idx in args.item) {					
						$("#_userActionQueryDetail").find("[data-nm='"+idx+"']").text(args.item[idx]);
					}		
	    			$('#_userActionQueryDetail').modal('show');
    			}
    		},
    		controller	: {
    			loadData: function(args) {
    				
    				var d = $.Deferred();
    				$.ajax({
    					url			: '/admin/logListByProject',
    					type		: 'post',
    					contentType	: 'application/json',
    					data		: JSON.stringify({
    						PAGE_INDEX	: pageIndex_proj,
    						PAGE_SIZE	: args.pageSize,
    						PRJCT_NO	: prjctNo,
    					})
    				}).done(function(response) {
    					var responseData = response.projectLogs;
						for (var t=0; t<responseData.length; t++) {
							if (responseData[t].SQL_EXIST == "Y") {
								responseData[t].SQL_EXIST = $("<button>").attr({"type":"button", "class":"btn bg-green"}).text("확인");
							} else {
								responseData[t].SQL_EXIST = '';
							}
						}
						
    					var da = { data: response.projectLogs, itemsCount: response.COUNT };
    					d.resolve(da);
    				});
    				return d.promise();
    			}
    		},
    		fields : [
    			{ name: "CONECT_DT"  , title : "<spring:message code='logManagement.001' text='접속일자' />"	, type: "date" , width: 35 },
                { name: "PRJCT_NM"   , title : "<spring:message code='logManagement.013' text='프로젝트명' />"	, type: "text" , width: 100 },
                { name: "MENU_NM"    , title : "<spring:message code='logManagement.017' text='수행항목' />"	, type: "text" , width: 120 },
                { name: "QRY_ID"  	 , title : "<spring:message code='logManagement.022' text='쿼리 ID' />"	, type: "text" , width: 60	, visible: false },    			
                { name: "SQL_EXIST"  , title : "<spring:message code='logManagement.023' text='쿼리' />"		, type: "text" , width: 30 	, align: "center" },    			
                { name: "USR_ID"	 , title : "<spring:message code='logManagement.003' text='유저ID' />"	, type: "text" , width: 40 },    			
                { name: "USR_NM"	 , title : "<spring:message code='logManagement.004' text='유저이름' />"	, type: "text" , width: 30 },    			
                { name: "EXECUT_SQL" , title : "<spring:message code='logManagement.009' text='접속기록' />"	, type: "text" , width: 140	, visible: false	, css : "_ellipsis"	, cellRenderer : _gridRenderer.cellRenderer},    			
                { name: "RESULT_CNT" , title : "<spring:message code='logManagement.010' text='행 갯수' />"	, type: "text" , width: 30 	, visible: false },
    		]
    	});
    }
    <%-- [E]프로젝트 선택항목 로그 이력 호출 --%>
    
    
    <%-- [S]프로젝트 목록 조회 --%>
    function getProjectList(stepCd) {
    	$.ajax({
    		url: "/common/getProjectList",
        	type : "post",
        	contentType: 'application/json',
        	data : JSON.stringify({
        		PRO_STEP_CD: 'ONLY_'+stepCd,
        		ALL_LIST: 'Y'
        	})
        	
    	}).done(function(response) {
    		<%-- 대상 프로젝트 존재 시 --%>
    		if(response.PROJECT_LIST && response.PROJECT_LIST.length > 0) {
	    		var projectList = response.PROJECT_LIST;
	    		
	    		for(var pi=0; pi<projectList.length; pi++) {
	    			var option = $('<option value="'+projectList[pi].PRJCT_NO+'">['+projectList[pi].ALIAS_NO+'] '+projectList[pi].PRJCT_NM+' / '+projectList[pi].CHARGER+' / '+projectList[pi].DPRTM_NM +'</option>');
	    			$('#SEARCH_PROJECT_CD').append(option);
	    		}
    		}
    	});
    }
    <%-- [E]프로젝트 목록 조회 --%>
    
    <%-- [S]datepicker date validation --%>
    function datediff() {
    	var before	= new Date($('#ST_DT').val());
    	var after	= new Date($('#ED_DT').val());
    	var result	= after.getTime() - before.getTime()
    		result	= Math.ceil(result / 1000 / 60 / 60 / 24);
    	
    	if (result < 8) return 0;
    	else return 1;
    }
    <%-- [E]datepicker date validation --%>

</script>
<section>
	<br />
	<div class="x_panel">
		<%-- [S] 검색탭 --%>
		<ul class="nav nav-tabs bar_tabs" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="custom-tabs-login-tab" data-toggle="pill" href="#custom-tabs-one-home" role="tab"><spring:message code='logManagement.009' /></a>
			</li>
			<li class="nav-item">
				<a class="nav-link"	id="custom-tabs-profile-tab" data-toggle="pill"  href="#custom-tabs-one-profile" role="tab"><spring:message code='logManagement.024' text='프로젝트별 접속기록' /></a>
			</li>
			<li class="nav-item">
				<a class="nav-link" id="custom-tabs-error-tab" style="display:none;" data-toggle="pill" href="#custom-tabs-one-error" role="tab"><spring:message code='logManagement.018' text='프로시저 에러기록' /></a>
			</li>
		</ul>
		<%-- [E] 검색탭 --%>

		<div class="tab-content" id="custom-tabs-one-tabContent">
			<br />
			<%-- [S]사용자별 이력조회 --%>
			<div class="tab-pane fade" id="custom-tabs-one-home" role="tabpanel">
				<div class="input-group col-xs-8">
					<select id="SEARCH_TY" name="SEARCH_TY"	class="form-control custom-select wp10">
						<c:forEach var="searchTy" items='${HIST_SEARCH_TY}'	varStatus="status">
							<option value='${searchTy.CD}'>${searchTy.CD_DC}</option>
						</c:forEach>
					</select>
						<input type="text" id="SEARCH" name="SEARCH" placeholder="Type Message ..." class="form-control wp40">
						<input type="text" id="ST_DT" name="ST_DT" class="form-control datetimepicker-input wp20" placeholder="<spring:message code='logManagement.031' text='조회시작 시간' />" value="" autocomplete="off">
						<input type="text" id="ED_DT" name="ED_DT" class="form-control datetimepicker-input wp20" placeholder="<spring:message code='logManagement.032' text='조회종료 시간' />" value="" autocomplete="off">
					<span class="input-group-append" style="width:20%;">
						<button type="submit" id="SEARCH_BTN1" class="btn btn-dark" style="min-height:34px"><i class="fa fa-search"></i></button>
					</span>
				</div>
				<div class="input-group col-xs-12 btn-area">
					<span class="input-group-append wp20">
						<button type="button" id="EXCEL_DOWNLOAD1" class="btn bg-green" style="min-height:34px"><i class="fa fa-file-excel-o"></i> EXCEL</button>
					</span>
				</div>
				<br />
				<div id="jsGrid1" class="jsgrid wp100" style="position: relative; height: 100%;"></div>
			</div>
			<%-- [E]사용자별 이력조회 --%>

			<%-- [S]프로젝트별 이력조회 --%>
			<div class="tab-pane fade" id="custom-tabs-one-profile"	role="tabpanel">
				<div class="input-group col-xs-12">
					<select id="SEARCH_STEP_CD" class="form-control wp10">
						<option value=""><spring:message code='myInfo.015' text='전체' /></option>
						<c:forEach var="stepCd" items='${PROJECT_STEP}' varStatus="status">
							<c:if test="${fn:endsWith(stepCd.CD, '00')}">
								<option value='${stepCd.CD}'>${stepCd.CD_NM}</option>
							</c:if>
						</c:forEach>
						<option value="000"><spring:message code='logManagement.030' text='직접입력' /></option>
					</select>
					<select id="SEARCH_PROJECT_CD" class="form-control inputConvert wp40"></select>
					<input type="text" id="SEARCH2" name="SEARCH2" placeholder="Type Message ..." class="form-control inputConvert wp40" style="display:none;">
					<span class="input-group-append">
						<button type="button" id="SEARCH_BTN2" class="btn btn-dark" style="min-height:34px"><i class="fa fa-search"></i></button>
					</span>
				</div>
				<div class="input-group col-xs-12 btn-area">
					<span class="input-group-append" style="width:20%;">
						<button type="button" id="EXCEL_DOWNLOAD2" class="btn bg-green" style="min-height:34px"><i class="fa fa-file-excel-o"></i> EXCEL</button>
					</span>
				</div>
				<br />
				<div id="jsGrid2" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
			</div>
			<%-- [E]프로젝트별 이력조회 --%>
			
			<%-- [S]프로시저 에러기록 --%>
			<div class="tab-pane fade" id="custom-tabs-one-error" role="tabpanel">
				<div class="input-group col-xs-10">
					<select id="SEARCH_TYPE" name="SEARCH_TYPE"	class="form-control custom-select wp20">
						<option value="">전체</option>
						<c:forEach var="searchTy" items='${PROC_ERR_TY}' varStatus="status">
							<option value='${searchTy.CD}'>${searchTy.CD_DC}</option>
						</c:forEach>
					</select>

					<select id="SEARCH_TY3" name="SEARCH_TY3" class="form-control select2 custom-control-inline search-select wp20">
						<c:forEach var="searchTy" items='${PROC_ERR_SEARCH_TY}' varStatus="status">
							<option value='${searchTy.CD}'>${searchTy.CD_DC}</option>
						</c:forEach>
					</select> 
					<input type="text" id="SEARCH3" name="SEARCH3" name="message" placeholder="Type Message ..." class="form-control wp50">
					<span class="input-group-append">
						<button type="submit" id="SEARCH_BTN3" class="btn btn-dark" style="min-height:34px"><i class="fa fa-search"></i></button>
					</span>
				</div>
				<br />
				<div id="jsGrid3" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
			</div>
			<%-- [E]프로시저 에러기록 --%>
		</div>
	</div>
</section>

<div class="modal" id="_detailPopup" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">	                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            	<h4 class="modal-title"><spring:message code='logManagement.019' text='상세정보' /></h4>
             	</div>
	        <div class="modal-body">
	        	<div class="table-responsive">
                    <table class="table">
	                      <tbody>
	                      <tr>
	                        <th><spring:message code='logManagement.001' text='접속일자' /> :</th>
	                        <td data-nm="CONECT_DT"></td>		                    
	                        <th><spring:message code='logManagement.005' text='사용자IP' /> :</th>
	                        <td data-nm="CONECT_IP"></td>
	                      </tr>
	                      <tr>
	                        <th><spring:message code='logManagement.003	' text='유저아이디' /> :</th>
	                        <td data-nm="USR_ID"></td>		                      
	                        <th><spring:message code='logManagement.004' text='유저이름' /> :</th>
	                        <td data-nm="USR_NM"></td>
	                      </tr>
	                      <tr>
	                        <th><spring:message code='logManagement.006' text='브라우저' /> :</th>
	                        <td data-nm="CONECT_WBSR"></td>		                    
	                        <th><spring:message code='logManagement.008' text='메뉴ID' /> :</th>
	                        <td data-nm="MENU_ID"></td>
	                      </tr>
	                      <tr>
	                        <th colspan="4"><spring:message code='logManagement.009' text='접속기록' />(<spring:message code='logManagement.010' text='행 갯수' /> : <span data-nm="RESULT_CNT"></span>)</th>                      
	                      </tr>                    
	                    </tbody>
                   </table>
                 </div>
                 <textarea data-nm="EXECUT_SQL" class="form-control" rows="10" disabled="">
                 </textarea>
	        </div>
        </div>
   	</div>
</div>

<div class="modal" id="_detailPopupErr" tabindex="-1" role="dialog"> 
<div class="modal-dialog" role="document">
	<div class="modal-content">
		<div class="modal-header">	                
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			<h4 class="modal-title"><spring:message code='logManagement.019' text='상세정보' /></h4>
		</div>
		<div class="modal-body">
			<div class="table-responsive">
				<table class="table">
					<tbody>
						<tr>
							<th><spring:message code='logManagement.011' text='일자' /> :</th>
							<td data-nm="RGSDE"></td>
						</tr>
						<tr>
							<th><spring:message code='logManagement.012' text='프로젝트 타입' /> :</th>
							<td data-nm="PROC_TY"></td>
						</tr>
						<tr>
							<th><spring:message code='logManagement.013' text='프로젝트명' /> :</th>
							<td data-nm="PRJCT_NM"></td>
						</tr>
						<tr>
							<th><spring:message code='logManagement.014' text='프로시저명' /> :</th>
							<td data-nm="PROC_NM"></td>
						</tr>
						<tr>
							<th colspan="2"><spring:message code='logManagement.021' text='에러내역' /></th>                      
						</tr>                    
					</tbody>
				</table>
		        </div>
				<textarea data-nm="ERR_MSG" class="form-control" rows="10" disabled="">
				</textarea>
			</div>
		</div>
	</div>
</div>


<%-- [S]사용자활동이력 grid 데이터 --%>
<div class="modal" id="_userActionDetail" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">	                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            	<h4 class="modal-title"><spring:message code='logManagement.019' text='상세정보' /></h4>
             	</div>
	        <div class="modal-body">
	        	<div class="table-responsive">
                    <table class="table">
	                      <tbody>
	                      <tr>
	                        <th><spring:message code='logManagement.005' text='사용자IP' /> :</th>
	                        <td data-nm="CONECT_IP"></td>
	                        <th><spring:message code='logManagement.006' text='브라우저' /> :</th>
	                        <td data-nm="CONECT_WBSR"></td>
	                      </tr>
	                      <tr>
	                        <th><spring:message code='logManagement.003	' text='유저아이디' /> :</th>
	                        <td data-nm="USR_ID"></td>		                      
	                        <th><spring:message code='logManagement.004' text='유저이름' /> :</th>
	                        <td data-nm="USR_NM"></td>
	                      </tr>
	                      <tr>
	                        <th colspan="4"><spring:message code='logManagement.009' text='접속기록' />(<spring:message code='logManagement.010' text='행 갯수' /> : <span data-nm="RESULT_CNT"></span>)</th>                      
	                      </tr>                    
	                    </tbody>
                   </table>
                 </div>
                 <div id="userActionGrid" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
	        </div>
        </div>
   	</div>
</div>
<%-- [E]사용자활동이력 grid 데이터 --%>


<%-- [S]프로젝트별 활동이력 grid 데이터 --%>
<div class="modal" id="_logListDetailByProject" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-xlg" role="document">
		<div class="modal-content"> 
			<div class="modal-header">	                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            	<h4 class="modal-title"><spring:message code='logManagement.019' text='상세정보' /></h4>
             	</div>
	        <div class="modal-body">
                 <div id="projectLogGrid" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
	        </div>
        </div>
   	</div>
</div>
<%-- [E]프로젝트별 활동이력 grid 데이터 --%>


<%-- [S]사용자 활동이력 쿼리데이터 --%>
<div class="modal" id="_userActionQueryDetail" tabindex="-1" role="dialog"> 
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">	                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>
            	<h4 class="modal-title"><spring:message code='logManagement.019' text='상세정보' /></h4>
             	</div>
	        <div class="modal-body">
                 <textarea data-nm="EXECUT_SQL" class="form-control" rows="20" disabled=""></textarea>
	        </div>
        </div>
   	</div>
</div>
<%-- [E]사용자 활동이력 쿼리데이터 --%>