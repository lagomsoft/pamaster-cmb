<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<script type="text/javascript">

    // 수정 리스트
    var updateGrid1List = new Array();
    var newGrid1List = new Array();
    var clickIndvdlinfoCd = null;
    
    var updateGrid2List = new Array();
    
    //선택 로우
    var isNewRow1 = false;
    var isUpdateRow1 = false;
    var selectGrid1Row = null;
    
    var isUpdateRow2 = false;
    var selectGrid2Row = null;
	
	var tcnMap = {
		<c:forEach items="${TCN_LIST}" var="tcn" varStatus="status">
			<c:if test="${!status.first}">,</c:if>  ${tcn.CD} : "${tcn.CD_NM}"
		</c:forEach>
	};
	
	var tcnList = [
		<c:forEach items="${TCN_LIST}" var="tcn" varStatus="status">
			<c:if test="${!status.first}">,</c:if>  {Id : "${tcn.CD}" , Name : "${tcn.CD_NM} (${tcn.CD})"}
		</c:forEach>
	];
	
	var seList = [
		<c:forEach items="${SE_LIST}" var="se" varStatus="status">
			<c:if test="${!status.first}">,</c:if>  {Id : "${se.CD}" , Name : "${se.CD_NM}"}
		</c:forEach>
	];
	
	var clList = [
		<c:forEach items="${CL_LIST}" var="cl" varStatus="status">
			<c:if test="${!status.first}">,</c:if>  {Id : "${cl.CD}" , Name : "${cl.CD_NM}"}
		</c:forEach>
	];
	
    $(function () {

        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "400px",
            editing: true,
            sorting: false, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args){
                if(isUpdateRow1){
    	            isUpdateRow1 = false;
    	            $("#jsGrid1").jsGrid("updateItem");
                }
                
                $("#jsGrid1").jsGrid("cancelEdit");
                
                if(updateGrid2List.length > 0){
                	if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
                    	return;
                	}
                }

                updateGrid2List = new Array();
                isUpdateRow2 = false;
                
                if(args.item.ORG_INDVDLINFO_CD){
	                clickIndvdlinfoCd = args.item.ORG_INDVDLINFO_CD;
	                $("#GRID2_DIV").show();
	                $("#jsGrid2").jsGrid("loadData");
                }else{
	                $("#GRID2_DIV").hide();
                }
                
            },
            rowDoubleClick : function(args){
            	this.editItem($(args.event.target).closest("tr"));
            	
            	selectGrid1Row = args.itemIndex;
            	if(isNewRow1){
            		newGrid1List[selectGrid1Row] = 1;
                }
            	isNewRow1 = false;
            	
            },
            controller: {
                loadData: function() {
                    
                    if( updateGrid1List.length > 0 || newGrid1List.length > 0 || updateGrid2List.length > 0 ){
                    	if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
                        	return;
                    	}
                    	
                    	initAllGrid();
                    }
                    
                    var d = $.Deferred();
                    $.ajax({
                        url: "/admin/getMappingList",
                        type : "post",
                        contentType: 'application/json',
                        data : JSON.stringify({
                        	CNT_CD : "ko"
                        })
                            
                    }).done(function(response) {
                        d.resolve(response.MAPPING_LIST);
                    });
                    
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                 { name: "INDVDLINFO_CD", title : '<spring:message code="mappingManagement.002" text="유형코드" />', type: "text", width: 80 }
                ,{ name: "INDVDLINFO_NM", title : '<spring:message code="mappingManagement.003" text="유형명" />', type: "text", width: 120 }
                ,{ name: "INFO_SE", title : '<spring:message code="mappingManagement.004" text="정보구분" />', width: 80, type: "select", items : seList, valueField: "Id", textField: "Name"  }
                ,{ name: "INFO_CL", title : '<spring:message code="mappingManagement.005" text="정보분류" />', width: 80, type: "select", items : clList, valueField: "Id", textField: "Name"  }
                ,{ name: "SNSTIVE_YN", title : '<spring:message code="mappingManagement.036" text="일반정보여부" />', width: 80 , type: "select", items : [
	                	 { Name:  'N', Id: "N" }
	                    ,{ Name:  'Y', Id: "Y" }
	                ], valueField: "Id", textField: "Name"}
                ,{ name: "CLM_TY", title : '<spring:message code="mappingManagement.006" text="컬럼타입" />', width: 80, type: "select", items : [
                    { Name:  '<spring:message code="common.002" text="문자" />&<spring:message code="common.003" text="숫자" />', Id: "A" },
                    { Name:  '<spring:message code="common.002" text="문자" />', Id: "S" },
                	{ Name:  '<spring:message code="common.003" text="숫자" />', Id: "N" },
                	{ Name:  '<spring:message code="common.054" text="날짜" />', Id: "D" }
                ], valueField: "Id", textField: "Name" }
                ,{ name: "SMPLE", title : '<spring:message code="mappingManagement.007" text="샘플데이터" />', type: "text", width: 120 }
                ,{ name: "SYNONMS", title : '<spring:message code="mappingManagement.008" text="동의어(구분자 ;)" />', type: "text", width: 130 }
                ,{ name: "REMOVS", title : '<spring:message code="mappingManagement.009" text="제거값(구분자 ;)" />', type: "text", width: 130 }
                ,{ name: "USE_YN", title : '<spring:message code="mappingManagement.010" text="사용여부" />', width: 50 , type: "select", items : [
	                    { Name:  '<spring:message code="common.004" text="사용" />', Id: "Y" },
	                	{ Name:  '<spring:message code="common.005" text="미사용" />', Id: "N" }
	                ], valueField: "Id", textField: "Name"}
                ,{ name: "ORG_INDVDLINFO_CD", title : '<spring:message code="mappingManagement.002" text="유형코드" />', type: "text", css:"d-none", width: 0 }  
            ]
        });

        $("#jsGrid2").jsGrid({
            width: "100%",
            height: "400px",
            editing: true,
            sorting: false, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args){
                if(isUpdateRow2){
    	            isUpdateRow2 = false;
    	            $("#jsGrid2").jsGrid("updateItem");
                }
                
                $("#jsGrid2").jsGrid("cancelEdit");
            },
            rowDoubleClick : function(args){
            	this.editItem($(args.event.target).closest("tr"));
            	
            	selectGrid2Row = args.itemIndex;
            	
            },
            controller: {
                loadData: function() {
                    var d = $.Deferred();
                    $.ajax({
                        url: "/admin/getMappingTcnList",
                        type : "post",
    					isHideOverlay : true,
                        contentType: 'application/json',
                        data : JSON.stringify({
                        	INDVDLINFO_CD : clickIndvdlinfoCd
                        })
                    }).done(function(response) {
                        d.resolve(response.TCN_LIST);
                    });
                    
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                 { name: "INDVDLINFO_CD", title : '<spring:message code="mappingManagement.002" text="유형코드" />', type: "text", width: 50 }
                ,{ name: "RISK_NM", title : '<spring:message code="mappingManagement.027" text="리스크명" />', type: "text", width: 50 }
                ,{ name: "PRC_NO", title : '<spring:message code="mappingManagement.028" text="추천순위" />', type: "text", width: 50 }
                ,{ name: "PRC_TCN_CD", title : '<spring:message code="mappingManagement.029" text="가명처리기술" />', width: 200, type: "select", items : tcnList, valueField: "Id", textField: "Name" }
                ,{ name: "PRC_TCN_VAL1", title : '<spring:message code="mappingManagement.030" text="입력값 1" />', width: 200, type: "text" }
                ,{ name: "PRC_TCN_VAL2", title : '<spring:message code="mappingManagement.031" text="입력값 2" />', width: 200, type: "text" }
                ,{ name: "PRC_TCN_VAL3", title : '<spring:message code="mappingManagement.032" text="입력값 3" />', width: 200, type: "text" }
                ,{ name: "INDVDLINFO_CD", title : '<spring:message code="mappingManagement.002" text="유형코드" />', type: "text", css:"d-none", width: 0 }  
                ,{ name: "RISK_CD", title : '<spring:message code="mappingManagement.033" text="리스크코드" />', type: "text", css:"d-none", width: 0 }
                ,{ name: "ISNEW", title : '<spring:message code="mappingManagement.034" text="신규여부" />', type: "text", css:"d-none", width: 0 }
            ]
        });
        
        $("#jsGrid1").on("change", "input,select", function(){
        	updateGrid1List[selectGrid1Row] = selectGrid1Row;
        	isUpdateRow1 = true;
        });
        
        $("#jsGrid2").on("change", "input,select", function(){
        	updateGrid2List[selectGrid2Row] = selectGrid2Row;
        	isUpdateRow2 = true;
        });
        
        
        $("#ADD_ROW").on("click", function(){

            if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
	        $("#jsGrid1").jsGrid("insertItem").done(function() {
            	isNewRow1 = true;
            	
            	$("#jsGrid1 tbody").find("tr").last().find("td").first().dblclick();
            	$("#jsGrid1 tbody").find(".jsgrid-edit-row").find("input").eq(0).focus();
            	isUpdateRow1= true;
            });
        });
        
		$("#FILE_UPLOAD_BTN").on("click", function(){
			
	        // 등록할 파일 리스트
	        var fileCnt = pamsFileCount();
	        
	        // 파일이 있는지 체크
	        if(fileCnt == 0){
	            // 파일등록 경고창
	            alert( '<spring:message code="common.006" text="파일을 업로드 하세요." />');
	            return false;
	        }
	        
	        if(confirm( '<spring:message code="common.007" text="등록 하시겠습니까?" />')){
		        
	            if(isUpdateRow1){
		            isUpdateRow1 = false;
		            $("#jsGrid1").jsGrid("updateItem");
	            }
	            
	            $.ajax({
	                url:"/admin/uploadMappingFile",
	                type:'POST',
	                enctype:'multipart/form-data',
	                processData:false,
	                contentType:false,
	                dataType:'json',
	                cache:false,
	                data:getFilData(),
	                success:function(result){
	                    if(result.RETURN_TYPE == "SUCCESS"){
		                    
							var listSize = result.DATA_LIST.length;
							
	                    	for(var ei = 0; ei < listSize; ei++){
		                    	var mappingData = result.DATA_LIST[ei];
		                    	
		                    	mappingData.USE_YN = "Y";
		                    	
		                        $("#jsGrid1").jsGrid("insertItem", mappingData).done(function() {
			                        var rowId = $("#jsGrid1").jsGrid("option", "data").length - 1;
		                        	updateGrid1List[rowId] = rowId;
		                        	newGrid1List[rowId] = 1;
		                        });
		                        
		                    }
	                    	
	                    }else if(result.RETURN_TYPE == "EMPTY"){
		                    alert( '<spring:message code="common.008" text="파일이 존재하지 않습니다." />' );
	                    }else if(result.RETURN_TYPE == "ERR"){
		                    alert( '<spring:message code="common.009" text="엑셀 파일에 문제가 발생하였습니다. 파일을 확인하세요." />' );		                    
	                    }else{
		                    
	                    }
	                }
	            });
	        }
	        
		});

        $("#GET_DATA").on("click", function(){
	        $("#jsGrid1").jsGrid("loadData");
        });
        
        $("#SAVE_MAPPING").on("click", function(){
            
        	if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            $("#jsGrid1").jsGrid("cancelEdit");
            
            if(updateGrid1List.length <= 0){
                
				if(newGrid1List.length > 0){
	                alert( '<spring:message code="common.010" text="내용을 입력하세요." />' );
	                for (var key in newGrid1List) {
		                $("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                }
	                return;
				}else{
	                alert( '<spring:message code="common.011" text="변경된 정보가 없습니다." />' );
	                return;
				}
            	
            }
            
        	var griddata = $("#jsGrid1").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newList = new Array();
        	
            for (var key in updateGrid1List) {
            	saveDataList[di] = griddata[key];
            	
            	if(newGrid1List[key] > -1){
            		saveDataList[di].NEW = 'Y';
                }
                
                if(saveDataList[di].INDVDLINFO_CD != saveDataList[di].ORG_INDVDLINFO_CD){
	                if( newList.indexOf(saveDataList[di].INDVDLINFO_CD) > -1 ){
	                	alert( '<spring:message code="mappingManagement.015" text="중복 되는 유형코드가 존재 합니다." />' );
	                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                	return;
		            }
	            	
	                //아이디 중복확인
	            	newList[di] = saveDataList[di].INDVDLINFO_CD;
                }
            	
            	if(saveDataList[di].INDVDLINFO_CD == undefined || String(saveDataList[di].INDVDLINFO_CD) == ""){
                	alert( '<spring:message code="mappingManagement.016" text="유형코드를 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].INDVDLINFO_NM == undefined || String(saveDataList[di].INDVDLINFO_NM) == ""){
                	alert( '<spring:message code="mappingManagement.017" text="유형코드명을 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }

            	if(saveDataList[di].INDVDLINFO_CD.length > 20){
                	alert('<spring:message code="mappingManagement.018" text="유형코드 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].GRP_CD.length + "/ 20)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].INDVDLINFO_NM.length > 30){
                	alert('<spring:message code="mappingManagement.019" text="유형명 길이를 30자 이하로 입력하세요." />' + " (" + saveDataList[di].GRP_NM.length + "/ 30)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].SMPLE != undefined && String(saveDataList[di].SMPLE) != "" && saveDataList[di].SMPLE.length > 60){
                	alert('<spring:message code="mappingManagement.020" text="샘플데이터 길이를 60자 이하로 입력하세요." />' + " (" + saveDataList[di].SMPLE.length + "/ 60)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
            	}
                
            	if(saveDataList[di].SYNONMS != undefined && String(saveDataList[di].SYNONMS) != "" ){
            		var synonms = saveDataList[di].SYNONMS.split(';');

            		for (var si = 0; si < synonms.length; si++) {
                		if(synonms[si].length > 60){
                			alert( '<spring:message code="mappingManagement.021" text="동의어를 60자 이하로 입력하세요." />' + " (" + synonms[si] + ":"+  synonms[si].length + "/ 60)");
                        	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
		                	return;            			
                    	}
                    	
                    	if(saveDataList[di].INDVDLINFO_NM == synonms[si]){
                        	alert(  '<spring:message code="mappingManagement.022" text="유형명과 동의어가  같습니다." />');
                        	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                        	return;
                        }
            		}
            		
                }
                
            	if(saveDataList[di].REMOVS != undefined && String(saveDataList[di].REMOVS) != "" ){
            		var removs = saveDataList[di].REMOVS.split(';');
            		
            		for (var ri = 0; ri < removs.length; ri++) {
                		if(removs[ri].length > 10){
                			alert('<spring:message code="mappingManagement.023" text="제거값을 10자 이하로 입력하세요." />' + " (" + removs[ri] + ":"+  removs[ri].length + "/ 10)");
                        	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
		                	return;            			
                    	}
            		}
            		
                }
                
            	if(saveDataList[di].PRC_TCN_DC1 != undefined && String(saveDataList[di].PRC_TCN_DC1) != "" && saveDataList[di].PRC_TCN_DC1.length > 60){
                	alert(  '<spring:message code="mappingManagement.024" text="추천기술설명1 길이를 60자 이하로 입력하세요." />' + " (" + saveDataList[di].PRC_TCN_DC1.length + "/ 60)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
            	}
                
            	if(saveDataList[di].PRC_TCN_DC2 != undefined && String(saveDataList[di].PRC_TCN_DC2) != "" && saveDataList[di].PRC_TCN_DC2.length > 60){
                	alert('<spring:message code="mappingManagement.025" text="추천기술설명2 길이를 60자 이하로 입력하세요." />' + " (" + saveDataList[di].PRC_TCN_DC2.length + "/ 60)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
            	}
                
            	di++;
            	
            }
            
            //코드그룹 중복확인
            $.ajax({
                url: "/admin/checkMappingCode",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	MAPPING_LIST : newList
                })
            }).done(function(response) {
            	if(response.CHECK_CD != ""){
	            	alert( '<spring:message code="mappingManagement.026" text="유형코드가 중복 되었습니다." />' + ' (  <spring:message code="common.012" text="중복코드" />  : ' + response.CHECK_CD + ")");
	            }else{
	            	saveMapping(saveDataList);
		        }
            });
            
        });
        $("#SAVE_MAPPING_TCN").on("click", function(){
            
        	if(isUpdateRow2){
	            isUpdateRow2 = false;
	            $("#jsGrid2").jsGrid("updateItem");
            }
            
            $("#jsGrid2").jsGrid("cancelEdit");
            
            if(updateGrid2List.length <= 0){
                alert( '<spring:message code="common.011" text="변경된 정보가 없습니다." />' );
				return;
            }
            
        	var griddata = $("#jsGrid2").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newList = new Array();
        	
            for (var key in updateGrid2List) {
            	saveDataList[di] = griddata[key];
            	di++;
            	
            }
            
            //코드그룹 중복확인
            $.ajax({
                url: "/admin/saveMappingTcn",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	TCN_LIST : saveDataList
                })
            }).done(function(response) {
            	alert('<spring:message code="common.013" text="저장 되었습니다." />');
            	
                updateGrid2List = new Array();
                isUpdateRow2 = false;
            });
            
        });
        
        loadPAMsFile("EXE_FILE_DIV", {
				 maxFileCount 	: 1
				,pamsFileType : ['xls']
		});
    })
    
    function saveMapping(saveDataList){
        
        if(confirm( '<spring:message code="common.007" text="등록 하시겠습니까?" />')){
            
            $.ajax({
                url: "/admin/saveMapping",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    DATA_LIST : saveDataList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />' );
		            location.reload();
		        }
            	
            });
            
        }
    }

	function initAllGrid(){

	    // 수정 리스트
	    updateGrid1List = new Array();
	    newGrid1List = new Array();
	    
	    //선택 로우
	    selectGrid1Row = null;
	    isNewRow1 = false;
	    isUpdateRow1 = false;
	    
	}
	
</script>

<section>

	<div class="col-xs-12 btn-area">
		<button id="ADD_ROW" type="button" class="btn btn-dark">
			<spring:message code="common.015" text="신규" />
		</button>
		&nbsp;
		<button id="SAVE_MAPPING" type="button" class="btn btn-dark">
			<spring:message code="common.047" text="저장" />
		</button>
		&nbsp;
		<button id="GET_DATA" type="button" class="btn btn-dark">
			<spring:message code="common.017" text="조회" />
		</button>
	</div>

	<div class="x_panel x_result">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="mappingManagement.037" text="개인정보유형" />
					</h2>
				</div>
			</div>
			<div id="jsGrid1" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
			<br><br>
			<div id="GRID2_DIV">
				<div class="col-xs-6">
					<div class="x_title">
						<h2>
							<spring:message code="mappingManagement.035" text="추천기술" />
						</h2>
					</div>
				</div>
				<div class="col-xs-6 btn-area">
					<button id="SAVE_MAPPING_TCN" type="button"
						class="btn btn-dark">
						<spring:message code="common.047" text="저장" />
					</button>
				</div>
				<div id="jsGrid2" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
			</div>
		</div>
	</div>

	<div class="x_panel">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="common.018" text="일괄 업로드" />
					</h2>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-light">
			<div class="col-xs-12 btn-area">
				<button type="button" class="btn btn-primary"
					onclick="window.open('../uploadsample/MAPPING.xls');">
					<spring:message code="common.019" text="양식 다운로드" />
				</button>
				&nbsp;
				<button id="FILE_UPLOAD_BTN" type="button"
					class="btn btn-dark">
					<spring:message code="common.020" text="업로드" />
				</button>
			</div>
			<div class="col-xs-12">
				<div id="EXE_FILE_DIV" class="card-body"></div>
			</div>
		</nav>
	</div>

</section>
