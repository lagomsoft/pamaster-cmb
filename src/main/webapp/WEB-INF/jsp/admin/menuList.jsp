<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    

<style type="text/css">
.form-control.is-valid, .was-validated .form-control:valid {
	border-color : #ced4da !important;
	background-image :none !important;
}
#_detailDiv {
	margin-top:0px;
}

.invalid-feedback {
	color: red;
}
</style> 
<script type="text/javascript">
$(function () { 
    <%--상세 페이지 위치 이동--%>
    var currentPosition = parseInt($("#_detailDiv").css("margin-top").replace("px","")); 
    $(window).scroll(function() { var position = $(window).scrollTop(); 
    	$("#_detailDiv").stop().animate({"margin-top":position+currentPosition+"px"},500); 
    	
    });

	<%--각 동작별 ajax 그룹--%>
	var cont = {
			selectNodeId : null,
			targetData : null,
			insertMenu : function(obj){
				
				$.ajax({
					url: "/admin/insertMenuInfo",
					type : "post",
					contentType: 'application/json;',					
					data : JSON.stringify(obj),            
					success:function(res){
						$('#menuList').jstree(true)
							.get_node(obj.MENU_ID)
							.original
							.SEQ = res.SEQ;
						$('[name="SEQ"]').val(res.SEQ)
						return true;  
					},
					error:function(request,status,error){
						console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
						return false;
					}
	           });
            },
			updateMenu : function(obj,menuId){
				$.ajax({
					url: "/admin/updateMenuInfo",
					type : "post",
					contentType: 'application/json',
					data : JSON.stringify(obj),
					success:function(){					
						<%--정상 처리시--%>
			           	$('#menuList').jstree(true).refresh(); 
			            setTimeout(function () {		
			            	$('#menuList').jstree(true).select_node(menuId); 
                    	},100);		           
			         	
						return true;  
					},
					error:function(request,status,error){
						console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
						return false;
					}
				});
            },
			deleteMenu : function(obj){
				$.ajax({
					url: "/admin/deleteMenuInfo",
					type : "post",
					contentType: 'application/json',
					data : JSON.stringify(obj),
					success:function(){
						return true;  
					},
					error:function(request,status,error){
						console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
						return false;
					}
				});
            },
	}
	<%--좌측 트리 생성--%>
	var chgFormat = function (obj){	
		for(idx in obj){
			obj[idx].id = obj[idx].MENU_ID;
			obj[idx].parent = obj[idx].UPPER_MENU_ID;
			obj[idx].text = obj[idx].MENU_NM;
			obj[idx].type = obj[idx].MENU_TYPE == 1? 'default' : (obj[idx].MENU_TYPE ==  2 ? 'file' : 'process') ; 
			//obj[idx].icon = obj[idx].MENU_TYPE == 1? 'jstree-folder' : (obj[idx].MENU_TYPE ==  2 ? 'jstree-file' : 'process');
			if(!obj[idx].parent) obj[idx].parent = 'Home';	
		}	
		obj.push({id:'Home',text:'Home',parent:'#',type:'root'}); 
		return obj;
	}
	<%--메뉴 아이디목록을 seq목록으로 리턴--%>
	var getSeqList = function(obj){
		var seqList = new Array();
		for(idx in obj){
			var menuId = obj[idx];
			var original = $("#menuList").jstree(true).get_node(menuId).original;
			seqList.push(original.SEQ);			
			
		}
		return seqList;		
	}
	<%--메뉴 아이디 입력시 아이디 체크--%>
	var checkMenuId = function(){
		var oldMenuId =  $("#menuList").jstree('get_selected')[0];
		var menuId = $("#MENU_ID").val();
		if(oldMenuId != menuId){
			$.ajax({
				url: "/admin/getMenuIdCount",
				type : "post",
				contentType: 'application/json',
				data : JSON.stringify({MENU_ID : $("#MENU_ID").val()}),
				success:function(res){					
					<%--정상 처리시--%>
		            var cnt =res;
		            if(res !="0"){
		            	alert("<spring:message code="menuList.001" text="메뉴 아이디가 이미 존재합니다." />");
		            }else{		            	
	        			_modal.show(
        					"<spring:message code="menuList.002" text="메뉴수정"/>",
        					"<spring:message code="menuList.003" text="선택한 메뉴를 수정하시겠습니까?"/>",
        					"<spring:message code="menuList.004" text="수정"/>",
        					"<spring:message code="menuList.005" text="닫기"/>",
	        				updateCallBack
	        			)		        				            	
		            }
				},
				error:function(request,status,error){
					console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
					return false;
				}
			});
		}else{
			_modal.show(
				"<spring:message code="menuList.002" text="메뉴수정"/>",
				"<spring:message code="menuList.003" text="선택한 메뉴를 수정하시겠습니까?"/>",
				"<spring:message code="menuList.004" text="수정"/>",
				"<spring:message code="menuList.005" text="닫기"/>",
   				updateCallBack
   			)	
		}
	};
	<%--트리 메뉴 설정--%>
	$('#menuList').jstree({
	  "core" : {
		"multiple" : false,
	    "animation" : true,
	    "check_callback" : true,
	    "data" : function(obj, cb){
	    	$.ajax({
           	 url: "/admin/getMenuListInfo"
               ,type : "post"
               ,contentType: 'application/json'              
           }).done(function(res) {         	   
        	   cb.call(this,chgFormat(res));
           });
	    }
	  },
	  "types" : {
	    "#" : {
	      "max_children" : 10,
	      "max_depth" : 10,
	      "valid_children" : ["root"]
	    },
	    "root" : {      
	    	"icon" : "/img/bootstrap-icons/house-fill.svg" 
	    },
	    "default" : {
	    	"icon" : "/img/bootstrap-icons/folder-fill.svg"
	    },
	    "file" : {
	    	"icon" : "/img/bootstrap-icons/file-earmark-richtext.svg"
	    },
	    "process" : {
			"icon" : "/img/bootstrap-icons/segmented-nav.svg" 
		}
	  },
	  "dnd" : {		  
			"drop_finish" : function () {		
			    alert("DROP");		
			}		  
	  },
	  "plugins" : [
		  "contextmenu","dnd","types"
	  ],
	  'contextmenu' : {
	      "items" : {
	        createMenu : { 
	          "separator_before" : false,
	          "separator_after" : true,
	          "label" : "<spring:message code="menuList.006" text="메뉴생성"/>",
	          "action" : function (data) {
	                var inst = $.jstree.reference(data.reference);
	                var obj = inst.get_node(data.reference);	                
	                inst.create_node(obj, {}, "last", 
               			function (new_node) {	                	
		                    new_node.data = {
		                    	'id' : 'new_menu',
								'text' : '<spring:message code="menuList.007" text="새메뉴"/>'
		                    };
		                    setTimeout(function () {
		                    	inst.deselect_all();
		                    	inst.select_node(new_node);  
	                    	},10);
	                });
	                
	          },
	        },
			deleteMenu : {
				"separator_before" : false,
				"separator_after" : true,
				"label" : "<spring:message code="menuList.009" text="메뉴삭제"/>",
				"action" : function (data) {
					cont.targetData = data;	
					var inst = $.jstree.reference(data.reference);
					var obj = inst.get_node(data.reference);
					if(obj.id == 'Home'){
						return alert("<spring:message code="menuList.008" text="최상위 메뉴는 삭제할 수 없습니다."/>");
					}
					
					_modal.show(
						"<spring:message code="menuList.009" text="메뉴삭제"/>",
						"<spring:message code="menuList.010" text="선택한 메뉴를 삭제하시겠습니까?"/>",
						"<spring:message code="menuList.011" text="삭제"/>",
						"<spring:message code="menuList.005" text="닫기"/>",
						deleteCallBack
					)
				}
	        }
	      }
	    }  
	});
	<%--초기 트리 메뉴 호출시 전체 오픈--%>
	$('#menuList').on('loaded.jstree', function () {
	     $("#menuList").jstree('open_all');
	})

	<%--트리메뉴 메뉴 생성시 이벤트--%>
	$('#menuList').on("create_node.jstree", function (e, data) {
		$("#_detailDiv").show();
		<%--메뉴 depth / seq 획득--%>	
		var menuData = $('#menuList').jstree(true).get_json('#', {flat:true});
		var seq = 0;
		var depth=data.node.parents.length-1;
		for( idx in menuData){
			if(menuData[idx].id == data.node.id){				
				seq = parseInt(idx);
				break;
			}
		}
	
		<%--기본 값 삽입--%>		
		data.node.text = '<spring:message code="menuList.007" text="새메뉴"/>';  
		data.node.original.MENU_NM 		  = data.node.text;
		data.node.original.MENU_ID 		  = data.node.id;
		data.node.original.UPPER_MENU_ID  = data.node.parent;
		data.node.original.UPPER_MENU_SEQ = $('#menuList').jstree(true).get_node($('#menuList').jstree(true).get_parent(data.node.id)).original.SEQ;
		data.node.original.MENU_DEPTH 	  = depth;
		data.node.original.MENU_SEQ 	  = seq;
		data.node.original.MENU_TYPE 	  = 1;
		data.node.original.USE_YN 		  = 'Y';
		data.node.original.VIEW_YN		  = 'Y';	
		$('#menuList').jstree(true).rename_node (data.node, '<spring:message code="menuList.007" text="새메뉴"/>');
		cont.insertMenu(data.node.original);
	});
	<%--트리 메뉴 메뉴 이동시 이벤트--%>
	$("#menuList").on("move_node.jstree",function(e, data){ 
		<%--메뉴 depth / seq 획득--%>
		var menuData = $('#menuList').jstree(true).get_json('#', {flat:true});
		var seq = 0;
		var depth=data.node.parents.length-1;
		for( idx in menuData){
			if(menuData[idx].id == data.node.id){				
				seq = parseInt(idx);
				break;
			}
		}
		
		<%--이동한 메뉴데이타 업데이트--%>
		data.node.original.isChange = "Y";
		data.node.original.OLD_MENU_SEQ = data.node.original.MENU_SEQ;
		data.node.original.OLD_MENU_DEPTH = data.node.original.MENU_DEPTH;
		data.node.original.MENU_DEPTH 	  = depth;
		data.node.original.MENU_SEQ 	  = seq;
		data.node.original.UPPER_MENU_ID = data.node.parent;
		data.node.original.UPPER_MENU_SEQ = $('#menuList').jstree(true).get_node($('#menuList').jstree(true).get_parent(data.node.id)).original.SEQ;
		if(data.node.parent == 'Home'){
			data.node.original.UPPER_MENU_SEQ = '-1';
		}
		data.node.original.CHILDREN = getSeqList(data.node.children_d);
		
		<%--이동한 자식 메뉴 목록--%>
		cont.updateMenu(data.node.original,data.node.original.MENU_ID);	
		
	})
	<%--트리 메뉴 변경시 이벤트--%>
	$('#menuList').on("changed.jstree", function (e, data) {		
		<%--MENU_SEQ--%>	
		try{
			var orgin = data.node.original;
			$("#menuInfoFrm")[0].reset();
			$("#menuInfoFrm").find("[type='HIDDEN']").val('');
			for( nm in orgin){		
				$("#"+nm).val(orgin[nm+""]) 
			}	
		}catch(e){}
		$("#_detailDiv").show();
		$("#MENU_ID_ERROR").addClass("d-none");
		$("#MENU_NM_ERROR").addClass("d-none");
	});
	<%--수정 버튼 클릭시 동작--%>
	$("#updateBtn").on("click",function(){
		if(document.querySelectorAll('.needs-validation')[0].checkValidity()){
			$("#MENU_ID_ERROR").addClass("d-none");
			$("#MENU_NM_ERROR").addClass("d-none");
			checkMenuId()
		} else {
			if($("#MENU_ID").val().length < 1) {
				$("#MENU_ID_ERROR").removeClass("d-none");
			} else $("#MENU_ID_ERROR").addClass("d-none");
			if($("#MENU_NM").val().length < 1) {
				$("#MENU_NM_ERROR").removeClass("d-none");
			} else $("#MENU_NM_ERROR").addClass("d-none");
		}
	});
	
	<%--메뉴 정렬 초기화--%>
	$("#initMenuSeq").on("click",function(){
		if(confirm("메뉴관리에 표출되는 순서로 메뉴를 재정렬합니다.\r\n 진행하시겠습니까?")){
			var menuData = $('#menuList').jstree(true).get_json('#', {flat:true});
			var modelObject = $('#menuList').jstree(true)._model.data;
			var seqList = [];
			for(idx in menuData){
				if(idx !=0){
					var temp = new Object();
					temp.SEQ = modelObject[menuData[idx].id].original.SEQ;
					temp.MENU_DEPTH = modelObject[menuData[idx].id].parents.length-1;
					temp.UPPER_MENU_SEQ = $('#menuList').jstree(true).get_node($('#menuList').jstree(true).get_parent(menuData[idx].id)).original.SEQ;
					temp.MENU_SEQ = idx;
					temp.id = menuData[idx].id;
					seqList.push(temp)
				}
			}
			
			$.ajax({
				url: "/admin/initMenuSeq",
				type : "post",
				contentType: 'application/json',
				data : JSON.stringify({ 'seqList' : seqList}),
				success:function(){					
					alert('<spring:message code="menuList.026" text="메뉴정보를 재정렬 하었습니다." />');
					location.reload();
				},
				error:function(request,status,error){				
					return false;
				}
			});
		}
		
	});
	
	<%--전체 접기 / 펼치기--%>
	$("#openAll").on("click",function(){
		$('#menuList').jstree(true).open_all();
	})
	$("#closeAll").on("click",function(){
		$('#menuList').jstree(true).close_all();
	})
	
	
	<%--메뉴 수정시 confirm callback--%>	
	var updateCallBack = function(){
		var menuId = $("#MENU_ID").val();
		<%--차후 validate 관련 기능 수정--%>
		var formJson = {};
		$.map($("#menuInfoFrm").serializeArray(), function(n, i){
			 formJson[n['name']] = n['value'];
		});
		cont.updateMenu(formJson,menuId);
		_modal.hide();
	}
	<%--메뉴 삭제시 confirm callback--%>
	var deleteCallBack = function(){
		var data = cont.targetData; 
		var inst = $.jstree.reference(data.reference);
        var obj = inst.get_node(data.reference);
       
        cont.deleteMenu({
        	'SEQ' : obj.original.SEQ,
        	'MENU_SEQ' : obj.original.MENU_SEQ,
        	'MENU_ID' : obj.original.MENU_ID,
        	'CHILDREN' : getSeqList(obj.children_d)
       	}); 
        $("#menuInfoFrm")[0].reset();
        inst.delete_node(obj);
        _modal.hide();
	}
	
	
	
	
	
});
</script>
<section>
	<div class="row">
	    <div class="col-xs-6">
	   	 	<%--메뉴 트리 표출 위치--%>    	 	
	   	 	<div class="x_panel">
	   	 		<div class="x_underline">
		   	 		<div class="col-xs-6">
						<div class="x_title">
							<h2>
								<spring:message code="menuList.027" text="메뉴 목록" />
							</h2>
						</div>
					</div>
					<div class="col-xs-6 btn-area">
	   	 				<button type="button" class="btn btn-secondary btn-primary" id="initMenuSeq" title="메뉴관리의 메뉴순서와 실제 메뉴가 순서가 맞지 않을 때 사용">
							<spring:message code="menuList.028" text="메뉴정보 재정렬" />
						</button>		    	 	
						<button type="button" class="btn btn-secondary btn-primary" id="openAll">
							<i class="fa fa-expand "></i>&nbsp;<spring:message code="menuList.012" text="펼치기"/>
						</button>
						<button type="button" class="btn btn-secondary btn-primary" id="closeAll">
							<i class="fa fa-compress"></i>&nbsp;<spring:message code="menuList.013" text="접기"/>
						</button>
		   	 		</div> 
		   	 		<br><br>
		   	 	</div>
	    	 	<div id="menuList" class="col-xs-12"></div>
	   	 	</div>
	    </div>		
	    <div class="col-xs-6 " id="_detailDiv" style="display:none;">
	    	<div class="x_panel">
		    	<div class="x_underline">
		    		<div class="col-xs-6">
						<div class="x_title">
							<h2>
								<spring:message code="menuList.014" text="상세정보"/>
							</h2>
						</div>
					</div>
					<div class="col-xs-6 btn-area">
						<button type="button" class="btn btn-secondary btn-info" id="updateBtn">
							<i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message code='menuList.004' text='수정'/>
						</button> 		
					</div>
					<br><br>
				</div>
	    	 	<div class="x_content">
	    	 		<%--메뉴 상세정보 표출 위치 표출 위치--%>
			        <form class="needs-validation was-validated" id="menuInfoFrm"> 
			        	<input type="hidden" id="SEQ" name="SEQ"/>
			        	<input type="hidden" id="MENU_SEQ" name="MENU_SEQ"/>
			        	<input type="hidden" id="MENU_DEPTH" name="MENU_DEPTH"/>	
			        	<input type="hidden" id="UPPER_MENU_ID" name="UPPER_MENU_ID"/>	
			        	<input type="hidden" id="UPPER_MENU_SEQ" name="UPPER_MENU_SEQ"/>				        						        	
						<div class="row g-2">										
							<div class="col-sm-6 p-1">
								<label for="MENU_ID" class="form-label"><spring:message code="menuList.015" text="메뉴아이디"/></label>
								<input type="text" class="form-control was-validated" id="MENU_ID" name="MENU_ID" value="" required>	
								<div class="invalid-feedback d-none" id="MENU_ID_ERROR">
							    <spring:message code="menuList.016" text="메뉴 아이디를 입력하여 주십시요."/> 
							    </div>
							</div>
							<div class="col-sm-6 p-1">
								<label for="MENU_NM" class="form-label"> <spring:message code="menuList.018" text="메뉴명"/></label>
								<input type="text" class="form-control" id="MENU_NM" name="MENU_NM" value="" required>	
								<div class="invalid-feedback d-none" id="MENU_NM_ERROR">
							    <spring:message code="menuList.019" text="메뉴 명을 입력하여 주십시요."/>
							    </div>
							</div>
							<div class="col-sm-12">
							</div>
							<div class="col-sm-6 p-1">
								<label for="MENU_TYPE" class="form-label"><spring:message code="menuList.020" text="메뉴종류"/></label>							             
								<select class="form-control" id="MENU_TYPE" name="MENU_TYPE" > 
									<c:forEach items="${menuTypeList}" var="menuType">
									<option value="${menuType.CD}">${menuType.CD_NM}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-6 p-1">
								<label for="AUTH_CD" class="form-label"><spring:message code="menuList.021" text="지정권한"/></label>							             
								<select class="form-control" id="AUTH_CD" name="AUTH_CD"> 
									<option value=""><spring:message code="menuList.022" text="지정권한없음"/></option>
									<c:forEach items="${authCodeList}" var="authCd">
									<option value="${authCd.CD}">${authCd.CD_NM}</option>
									</c:forEach>
								</select>
							</div>
							<div class="col-sm-12 p-1">
								<label for="MENU_URL" class="form-label"><spring:message code="menuList.023" text="메뉴 URL"/></label>
								<input type="text" class="form-control" name="MENU_URL" id="MENU_URL" placeholder="" value="">							            
							</div>
							<div class="col-sm-6 p-1">
								<label for="VIEW_YN" class="form-label"><spring:message code="menuList.024" text="표출 유무"/></label>
								<select class="form-control" name="VIEW_YN" id="VIEW_YN">
									<option value="Y" selected>Y</option>
									<option value="N">N</option>											 
								</select>
							</div>
							<div class="col-sm-6 p-1">
								<label for="USE_YN" class="form-label"><spring:message code="menuList.025" text="사용 유무"/></label>
								<select class="form-control" name="USE_YN" id="USE_YN">
									<option value="Y" selected>Y</option>
									<option value="N">N</option>											 
								</select>
							</div>		
						</div>
					</form>		    	 		
	    	 	</div>
	   	 	</div>
		</div>
	</div>

</section>
