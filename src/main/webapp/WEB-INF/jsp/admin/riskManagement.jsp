<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<script type="text/javascript">

    // 수정 리스트
    var updateGrid1List = new Array();
    var newGrid1List = new Array();
    var deleteGrid1List = new Array();
	deleteGrid1List["COUNT"] = 0;
	
	var updateGrid2List = new Array();
    var newGrid2List = new Array();
    var deleteGrid2List = new Array();
	deleteGrid2List["COUNT"] = 0;
    
    //선택 로우
    var isNewRow1 = false;
    var isUpdateRow1 = false;
    var selectGrid1Row = null;
    
    var isNewRow2 = false;
    var isUpdateRow2 = false;
    var selectGrid2Row = null;
	
    
    
	
	
    $(function () {
    
        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "600px",
            editing: true,
            sorting: true,
            rowClick : function(args){
                if(isUpdateRow1){
    	            isUpdateRow1 = false;
    	            $("#jsGrid1").jsGrid("updateItem");
                }
                
                $("#jsGrid1").jsGrid("cancelEdit");
            },
            rowDoubleClick : function(args){
            	selectGrid1Row = args.itemIndex;
            	this.editItem($(args.event.target).closest("tr"));
            	if(isNewRow1){
            		newGrid1List[selectGrid1Row] = 1;
                }
            	isNewRow1 = false;
            	$(this._body).find("tr").eq(selectGrid1Row).find("input").eq(0).focus();
            },
            controller: {
                loadData: function() {
                    
                    if( updateGrid1List.length > 0 || newGrid1List.length > 0 ){
                    	if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
                        	return;
                    	}
                    	
                    	initAllGrid();
                    }
                    
                    
                    var d = $.Deferred();
                    $.ajax({
                        url: "/admin/getRiskList",
                        type : "post",
                        contentType: 'application/json',
                        data : JSON.stringify({
                        	// CNT_CD : "ko"
                        })
                            
                    }).done(function(response) {
                        d.resolve(response.DATA_LIST);
                    });
                    $("#jsGrid1").jsGrid("option", "sorting", true);
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
            	 { name: "No", title : '<spring:message code="common.029" text="삭제" />', width: 20
                     , itemTemplate: function (_, item) {
                         if(String(item.ORG_RISK_CD) == ""){
                             return "";
                         }
                         var $deleteCheckBox = $('<input type="checkbox" class="form-check-input" id="DELETE_CHECKBOX_'+item.ORG_RISK_CD+'">').on("change", function(){
                             
                             if($(this).is(":checked")){
                            	 deleteGrid1List[item.ORG_RISK_CD] = 1;
                            	 deleteGrid1List["COUNT"] = deleteGrid1List["COUNT"] + 1;
                             }else{
                            	 delete deleteGrid1List[item.ORG_RISK_CD];
                            	 deleteGrid1List["COUNT"] = deleteGrid1List["COUNT"] - 1;
                             }
                         });
                         var $deleteCheckBoxLabel = $('<label class="form-check-label" for="DELETE_CHECKBOX_'+item.ORG_RISK_CD+'"></label>');
                         
                         return $('<div class="form-check">').append($deleteCheckBox).append($deleteCheckBoxLabel);
 	                }
                 }
                ,{ name: "RISK_CD", title : '<spring:message code="riskManagement.011" text="위험도 코드" />', type: "text", width: 60 }
                ,{ name: "UPPER_RISK_CD", title : '<spring:message code="riskManagement.012" text="위험도 상위코드" />', type: "text", width: 60 }
                ,{ name: "RISK_NM", title : '<spring:message code="riskManagement.013" text="위험도 내용" />', type: "text", width: 120 }
                ,{ name: "RISK_RELIMP", title : '<spring:message code="riskManagement.014" text="위험도 비중" />', type: "text", width: 20 }
                ,{ name: "RISK_PNT", title : '<spring:message code="riskManagement.015" text="위험도 점수" />', type: "text", width: 20 }
                ,{ name: "META_3", title : '<spring:message code="riskManagement.034" text="자동매핑 적용" />', type: "select", items : [
               	 { Name: '<spring:message code="riskManagement.018" text="미사용" />', Id: "N" }
               	,{ Name: '<spring:message code="riskManagement.017" text="사용" />', Id: "Y" }
               ], valueField: "Id", textField: "Name", width: 20 }
                ,{ name: "USE_YN", title : '<spring:message code="riskManagement.016" text="위험도 사용여부" />', type: "select", items : [
                	 { Name: '<spring:message code="riskManagement.017" text="사용" />', Id: "Y" }
                	,{ Name: '<spring:message code="riskManagement.018" text="미사용" />', Id: "N" }
                ], valueField: "Id", textField: "Name", width: 25 }
            ]
        });
        
        $("#jsGrid1").on("change", "input,select", function(){
        	if($(this).attr("type") != "checkbox"){
        		updateGrid1List[selectGrid1Row] = selectGrid1Row;
        		isUpdateRow1 = true;	
        	}
        });
        
        
        $("#ADD_ROW").on("click", function(){

            if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
	        $("#jsGrid1").jsGrid("insertItem").done(function() {
            	isNewRow1 = true;
            	$("#jsGrid1 tbody").find("tr").last().find("td").first().dblclick();
            	$("#jsGrid1 tbody").find(".jsgrid-edit-row").find("input").eq(0).focus();
            });
        });

        $("#GET_DATA").on("click", function(){
	        $("#jsGrid1").jsGrid("loadData");
        });
        
        $("#DELETE_ROW").on("click", function(){
            
            if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            if( deleteGrid1List["COUNT"] <= 0){
                alert( '<spring:message code="common.031" text="선택된 정보가 없습니다." />' );
	            return;
            }
            
	        if(confirm( '<spring:message code="common.032" text="삭제 하시겠습니까?" />')){

	        	var deleteDataList = new Array();
	        	var di = 0;
	            for (var key in deleteGrid1List) {
		            if(key != "COUNT"){
		            	deleteDataList[di] = key;
			        }
	            }
	            
	            $.ajax({
	                url: "/admin/deleteRisk",
	                type : "post",
	                contentType: 'application/json',
	                data : JSON.stringify({
	                    DATA_LIST : deleteDataList
	                })
	            }).done(function(response) {
		            if(response.SUCCESS == "0"){
			            alert('<spring:message code="common.033" text="처리 되었습니다." />' );
			            location.reload();
			        }
	            });
	        }
            
        });
        
        $("#SAVE_DATA").on("click", function(){
            
        	if(isUpdateRow1){
	            isUpdateRow1 = false;
	            $("#jsGrid1").jsGrid("updateItem");
            }
            
            $("#jsGrid1").jsGrid("cancelEdit");
            
            if(updateGrid1List.length <= 0){
                
				if(newGrid1List.length > 0){
	                alert( '<spring:message code="common.010" text="내용을 입력하세요." />' );
	                for (var key in newGrid1List) {
		                $("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                }
	                return;
				}else{
	                alert( '<spring:message code="common.011" text="변경된 정보가 없습니다." />' );
	                return;
				}
            	
            }
            
        	var griddata = $("#jsGrid1").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newList = new Array();
        	
            for (var key in updateGrid1List) {
            	saveDataList[di] = griddata[key];
            	
            	if(newGrid1List[key] > -1){
            		saveDataList[di].NEW = 'Y';
                }
                
                if(saveDataList[di].RISK_CD != saveDataList[di].ORG_RISK_CD){
	                if( newList.indexOf(saveDataList[di].RISK_CD) > -1 ){
	                	alert( '<spring:message code="riskManagement.003" text="중복 되는 위험도 코드가 존재 합니다." />' );
	                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                	return;
		            }
	            	
	                //아이디 중복확인
	            	newList[di] = saveDataList[di].RISK_CD;
                }
            	
            	if(saveDataList[di].RISK_CD == undefined || String(saveDataList[di].RISK_CD) == ""){
                	alert( '<spring:message code="riskManagement.004" text="위험도 코드를 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].RISK_NM == undefined || String(saveDataList[di].RISK_NM) == ""){
                	alert( '<spring:message code="riskManagement.005" text="위험도 내용을 입력하세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }

            	if(saveDataList[di].RISK_CD.length > 20){
                	alert('<spring:message code="riskManagement.006" text="위험도 코드 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].RISK_CD.length + "/ 20)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].RISK_NM.length > 20){
                	alert('<spring:message code="riskManagement.007" text="위험도 내용을 20자 이하로 입력하세요." />' + " (" + saveDataList[di].RISK_NM.length + "/ 20)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	if(saveDataList[di].UPPER_RISK_CD != undefined && String(saveDataList[di].UPPER_RISK_CD) != "" && saveDataList[di].UPPER_RISK_CD.length > 20){
                	alert('<spring:message code="riskManagement.008" text="위험도 상위 코드 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].UPPER_RISK_CD.length + "/ 20)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
            	}
            	
            	if(saveDataList[di].USE_YN == undefined || String(saveDataList[di].USE_YN) == ""){
                	alert( '<spring:message code="riskManagement.009" text="사용 여부를 입력해주세요." />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	di++;
            	
            }
            
            //코드그룹 중복확인
            $.ajax({
                url: "/admin/checkRiskCode",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	RISK_LIST : newList
                })
            }).done(function(response) {
            	if(response.CHECK_CD != ""){
	            	alert( '<spring:message code="riskManagement.010" text="위험도 코드가 중복 되었습니다." />' + ' (  <spring:message code="common.012" text="중복코드" />  : ' + response.CHECK_CD + ")");
	            }else{
	            	saveRiskList(saveDataList);
		        }
            });
            
        });
        
        $("#jsGrid2").jsGrid({
            width: "100%",
            height: "auto",
            editing: true,
            sorting: true,
            rowClick : function(args){
                if(isUpdateRow2){
    	            isUpdateRow2 = false;
    	            $("#jsGrid2").jsGrid("updateItem");
                }
                
                $("#jsGrid2").jsGrid("cancelEdit");
            },
            rowDoubleClick : function(args){
            	this.editItem($(args.event.target).closest("tr"));
            	selectGrid2Row = args.itemIndex;
            	
            	if(isNewRow2){
            		newGrid2List[selectGrid2Row] = 1;
                }
            	isNewRow2 = false;
            	
            },
            controller: {
                loadData: function() {
                    
                    if( updateGrid2List.length > 0 || newGrid2List.length > 0 ){
                    	if(!confirm('<spring:message code="common.001" text="수정 중인 데이터가 있습니다. 조회 하시겠습니까?" />')){
                        	return;
                    	}
                    	
                    	initAllGrid2();
                    }
                    
                    var d = $.Deferred();
                    $.ajax({
                        url: "/admin/getEvaluationList",
                        type : "post",
                        contentType: 'application/json',
                        data : JSON.stringify({
                        	// CNT_CD : "ko"
                        })
                            
                    }).done(function(response) {
                        d.resolve(response.DATA_LIST);
                    });
                    $("#jsGrid2").jsGrid("option", "sorting", true);
                    return d.promise();
                }
            },
            sorting: true,
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
            	 { name: "No", title : '<spring:message code="common.029" text="삭제" />', width: 20
                     , itemTemplate: function (_, item) {
                         if(String(item.ORG_RISK_CD) == ""){
                             return "";
                         }
                         var $deleteCheckBox = $('<input type="checkbox" class="form-check-input" id="DELETE_CHECKBOX_'+item.ORG_RISK_CD+'">').on("change", function(){
                             
                             if($(this).is(":checked")){
                            	 deleteGrid2List[item.ORG_RISK_CD] = 1;
                            	 deleteGrid2List["COUNT"] = deleteGrid2List["COUNT"] + 1;
                             }else{
                            	 delete deleteGrid2List[item.ORG_RISK_CD];
                            	 deleteGrid2List["COUNT"] = deleteGrid2List["COUNT"] - 1;
                             }
                         });
                         var $deleteCheckBoxLabel = $('<label class="form-check-label" for="DELETE_CHECKBOX_'+item.ORG_RISK_CD+'"></label>');
                         
                         return $('<div class="form-check">').append($deleteCheckBox).append($deleteCheckBoxLabel);
 	                }
                 }
                ,{ name: "RISK_CD", title : '<spring:message code="riskManagement.011" text="위험도 코드" />', type: "text", width: 60 }
                ,{ name: "RISK_NM", title : '<spring:message code="riskManagement.013" text="위험도 내용" />', type: "text", width: 80 }
                ,{ name: "RISK_DC", title : '<spring:message code="riskManagement.019" text="위험도 설명" />', type: "text", width: 80 }
                ,{ name: "USE_YN", title : '<spring:message code="riskManagement.016" text="위험도 사용여부" />', type: "select", items : [
                	 { Name: '<spring:message code="riskManagement.017" text="사용" />', Id: "Y" }
                	,{ Name: '<spring:message code="riskManagement.018" text="미사용" />', Id: "N" }
                ], valueField: "Id", textField: "Name", width: 20 }
                ,{ name: "RISK_SCORE_ST", title : '<spring:message code="riskManagement.020" text="위험도 점수시작" />', type: "text", width: 30 }
                ,{ name: "RISK_SCORE_ED", title : '<spring:message code="riskManagement.021" text="위험도 점수끝" />', type: "text", width: 30 }
                ,{ name: "IDNTFC_YN", title : '<spring:message code="riskManagement.022" text="식별정보 제거 여부" />', type: "select", items : [
                	 { Name: '<spring:message code="riskManagement.017" text="사용" />', Id: "Y" }
                	,{ Name: '<spring:message code="riskManagement.018" text="미사용" />', Id: "N" }
                ], valueField: "Id", textField: "Name", width: 20 }
                ,{ name: "IDNTFC_ROUND", title : '<spring:message code="riskManagement.023" text="식별가능정보 범위" />', type: "text", width: 30 }
                ,{ name: "IDNTFC_K", title : '<spring:message code="riskManagement.024" text="식별가능정보 빈도수" />', type: "text", width: 30 }
                ,{ name: "ETC_NUM_YN", title : '<spring:message code="riskManagement.025" text="일반정보(숫자) 가명처리 필수여부" />', type: "select", items : [
                	 { Name: '<spring:message code="riskManagement.017" text="사용" />', Id: "Y" }
                	,{ Name: '<spring:message code="riskManagement.018" text="미사용" />', Id: "N" }
                ], valueField: "Id", textField: "Name", width: 20 }
                ,{ name: "ETC_NUM_ROUND", title : '<spring:message code="riskManagement.026" text="일반정보(숫자) 범위" />', type: "text", width: 40 }
                ,{ name: "ETC_NUM_K", title : '<spring:message code="riskManagement.027" text="일반정보(숫자) 빈도수" />', type: "text", width: 40 }
                ,{ name: "ETC_TXT_YN", title : '<spring:message code="riskManagement.028" text="일반정보(문자) 가명처리 필수여부" />', type: "select", items : [
                	 { Name: '<spring:message code="riskManagement.017" text="사용" />', Id: "Y" }
                	,{ Name: '<spring:message code="riskManagement.018" text="미사용" />', Id: "N" }
                ], valueField: "Id", textField: "Name", width: 20 }
                ,{ name: "ETC_TXT_ROUND", title : '<spring:message code="riskManagement.029" text="일반정보(문자) 범위" />', type: "text", width: 40 }
                ,{ name: "ETC_TXT_K", title : '<spring:message code="riskManagement.030" text="일반정보(문자) 빈도수" />', type: "text", width: 40 }
                ,{ name: "GROUP_K", title : '<spring:message code="riskManagement.031" text="집합 최소 K값" />', type: "text", width: 30 }
                ,{ name: "IDNTFC_L", title : '<spring:message code="riskManagement.032" text="다양성 L 값" />', type: "text", width: 30 }
                ,{ name: "IDNTFC_T", title : '<spring:message code="riskManagement.033" text="근접성 T 값" />', type: "text", width: 30 }
                
            ]
        });
        
        $("#jsGrid2").on("change", "input,select", function(){
        	if($(this).attr("type") != "checkbox"){
        		updateGrid2List[selectGrid2Row] = selectGrid2Row;
        		isUpdateRow2 = true;
        	}
        });
        
        
        $("#ADD_ROW2").on("click", function(){

            if(isUpdateRow2){
	            isUpdateRow2 = false;
	            $("#jsGrid2").jsGrid("updateItem");
            }
            
	        $("#jsGrid2").jsGrid("insertItem").done(function() {
            	isNewRow2 = true;
            	$("#jsGrid2 tbody").find("tr").last().find("td").first().dblclick();
            	$("#jsGrid2 tbody").find(".jsgrid-edit-row").find("input").eq(0).focus();
            });
        });

        $("#GET_DATA2").on("click", function(){
	        $("#jsGrid2").jsGrid("loadData");
        });
        
        $("#DELETE_ROW2").on("click", function(){
            
            if(isUpdateRow2){
	            isUpdateRow2 = false;
	            $("#jsGrid2").jsGrid("updateItem");
            }
            
            if( deleteGrid2List["COUNT"] <= 0){
                alert( '<spring:message code="common.031" text="선택된 정보가 없습니다." />' );
	            return;
            }
            
	        if(confirm( '<spring:message code="common.032" text="삭제 하시겠습니까?" />')){

	        	var deleteDataList = new Array();
	        	var di = 0;
	            for (var key in deleteGrid2List) {
		            if(key != "COUNT"){
		            	deleteDataList[di] = key;
			        }
	            }
	            
	            $.ajax({
	                url: "/admin/deleteEvaluation",
	                type : "post",
	                contentType: 'application/json',
	                data : JSON.stringify({
	                    DATA_LIST : deleteDataList
	                })
	            }).done(function(response) {
		            if(response.SUCCESS == "0"){
			            alert('<spring:message code="common.033" text="처리 되었습니다." />' );
			            location.reload();
			        }
	            });
	        }
            
        });
        
        $("#SAVE_DATA2").on("click", function(){
            
        	if(isUpdateRow2){
	            isUpdateRow2 = false;
	            $("#jsGrid2").jsGrid("updateItem");
            }
            
            $("#jsGrid2").jsGrid("cancelEdit");
            
            if(updateGrid2List.length <= 0){
                
				if(newGrid2List.length > 0){
	                alert( '<spring:message code="common.010" text="내용을 입력하세요." />' );
	                for (var key in newGrid1List) {
		                $("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                }
	                return;
				}else{
	                alert( '<spring:message code="common.011" text="변경된 정보가 없습니다." />' );
	                return;
				}
            	
            }
            
        	var griddata = $("#jsGrid2").jsGrid("option", "data");
        	
        	var saveDataList = new Array();
        	var di = 0;
        	
        	var newList = new Array();
        	
            for (var key in updateGrid2List) {
            	saveDataList[di] = griddata[key];
            	
            	if(newGrid2List[key] > -1){
            		saveDataList[di].NEW = 'Y';
                }
                
                if(saveDataList[di].RISK_CD != saveDataList[di].ORG_RISK_CD){
	                if( newList.indexOf(saveDataList[di].RISK_CD) > -1 ){
	                	alert( '<spring:message code="riskManagement.003" text="중복 되는 위험도 코드가 존재 합니다." />' );//.015->.003
	                	$("#jsGrid2 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
	                	return;
		            }
	            	
	                //아이디 중복확인
	            	newList[di] = saveDataList[di].RISK_CD;
                }
            	
            	if(saveDataList[di].RISK_CD == undefined || String(saveDataList[di].RISK_CD) == ""){
                	alert( '<spring:message code="riskManagement.016" text="위험도 사용여부" />' );
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }

            	if(saveDataList[di].RISK_CD.length > 20){
                	alert('<spring:message code="riskManagement.006" text="위험도 코드 길이를 20자 이하로 입력하세요." />' + " (" + saveDataList[di].RISK_CD.length + "/ 20)");
                	$("#jsGrid1 tbody").find("tr").eq(key).find("td").eq(0).dblclick();
                	return;
                }
                
            	di++;
            	
            }
            
            //코드그룹 중복확인
            $.ajax({
                url: "/admin/checkEvaluationCode",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	EVALUATION_LIST : newList
                })
            }).done(function(response) {
            	if(response.CHECK_CD != ""){
	            	alert( '<spring:message code="riskManagement.010" text="위험도 코드가 중복 되었습니다." />' + ' (  <spring:message code="common.012" text="위험도 상위코드" />  : ' + response.CHECK_CD + ")");
	            }else{
	            	saveEvaluationList(saveDataList);
		        }
            });
            
        });
        
        
        
        
        
        loadPAMsFile("EXE_FILE_DIV", {
				 maxFileCount 	: 1
				,pamsFileType : ['xls']
		});
    })
    
    function saveRiskList(saveDataList){
        
        if(confirm( '<spring:message code="common.007" text="등록 하시겠습니까?" />')){
            
            $.ajax({
                url: "/admin/saveRiskList",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    DATA_LIST : saveDataList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />' );
		            location.reload();
		        }
            });
            
        }
    }
    
    function saveEvaluationList(saveDataList){
        
        if(confirm( '<spring:message code="common.007" text="등록 하시겠습니까?" />')){
            
            $.ajax({
                url: "/admin/saveEvaluationList",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    DATA_LIST : saveDataList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />' );
		            location.reload();
		        }
            });
            
        }
    }

	function initAllGrid(){

	    // 수정 리스트
	    updateGrid1List = new Array();
	    newGrid1List = new Array();
	    
	    //선택 로우
	    selectGrid1Row = null;
	    isNewRow1 = false;
	    isUpdateRow1 = false;
	    
	}
	
	function initAllGrid2(){

	    // 수정 리스트
	    updateGrid2List = new Array();
	    newGrid2List = new Array();
	    
	    //선택 로우
	    selectGrid2Row = null;
	    isNewRow2 = false;
	    isUpdateRow2 = false;
	    
	}
	
	</script>

<section>

	<div class="col-xs-12 btn-area">
		<button id="GET_DATA" type="button" class="btn btn-dark">
			<spring:message code="common.017" text="조회" />
		</button>
		&nbsp;
		<button id="ADD_ROW" type="button" class="btn btn-dark">
			<spring:message code="common.015" text="신규" />
		</button>
		&nbsp;
		<button id="DELETE_ROW" type="button" class="btn btn-dark">
			<spring:message code="common.029" text="삭제" />
		</button>
		&nbsp;
		<button id="SAVE_DATA" type="button" class="btn btn-dark">
			<spring:message code="common.047" text="저장" />
		</button>
	</div>


	<div class="x_panel x_result">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="riskManagement.001" text="위험도관리" />
					</h2>
				</div>
			</div>
			<div id="jsGrid1" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
		</div>
	</div>
	<br><br>
	<div class="col-xs-12 btn-area">
		<button id="GET_DATA2" type="button" class="btn btn-dark">
			<spring:message code="common.017" text="조회" />
		</button>
		&nbsp;
		<button id="ADD_ROW2" type="button" class="btn btn-dark">
			<spring:message code="common.015" text="신규" />
		</button>
		&nbsp;
		<button id="DELETE_ROW2" type="button" class="btn btn-dark">
			<spring:message code="common.029" text="삭제" />
		</button>
		&nbsp;
		<button id="SAVE_DATA2" type="button" class="btn btn-dark">
			<spring:message code="common.047" text="저장" />
		</button>
	</div>
	
	<div class="x_panel x_result">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="riskManagement.002" text="평가지표 관리" />
					</h2>
				</div>
			</div>
			<div id="jsGrid2" class="jsgrid" style="position: relative; height: 100%; width: 100%;"></div>
		</div>
	</div>
</section>
