<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<script type="text/javascript">
$(function(){
	$.ajax({
		url : '/admin/storageInfo',
		type : 'post'
	}).done(function(res){
		if(res){
			for(idx in res){
				var clone = $("#temp").clone();
				$(clone).find("td").eq(0).text(res[idx].path);
				$(clone).find("td").eq(1).text(Math.floor(res[idx].totalSize*100) /100+"GB");
				$(clone).find("td").eq(2).text(Math.floor(res[idx].useSize*100)/100+"GB");
				$(clone).find("td").eq(3).text(Math.floor(res[idx].freeSize*100)/100+"GB");
				$(clone).find("td").eq(4).find("span").text(Math.floor(res[idx].percent*100)/100+"%");
				$(clone).find("td").eq(4).find(".progress-bar").css("width",Math.floor(res[idx].percent*100)/100+"%");
				if(res[idx].percent <= 20){
					$(clone).find("td").eq(4).find("span").removeClass("bg-success").addClass("bg-danger");
					$(clone).find("td").eq(4).find(".progress-bar").removeClass("bg-success").addClass("bg-danger");
				}
				$(clone).removeAttr("id");
				$(clone).removeClass("d-none");
				$("#storageBody").append(clone);
			}
		}
	})
})
</script>
<div class="container-fluid">
<div class="col-md-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">서버 공간</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>총 공간</th>
                      <th>사옹 공간</th>
                      <th>남은 공간</th>
                      <th style="width:100px;">%</th>
                    </tr>
                  </thead>
                  <tbody id="storageBody">
                    <tr id="temp" class=" d-none">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td>
                      	<span class="badge bg-success">90%</span>
                        <div class="progress progress-xs">
                          <div class="progress-bar bg-success" style="width: 55%"></div>
                        </div>
                      </td>                      
                    </tr>                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
</div>