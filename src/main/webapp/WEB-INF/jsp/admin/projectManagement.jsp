<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<title><spring:message code="projectManagement.001"
		text="프로젝트 관리" /></title>


<script type="text/javascript">
	
    $(function () {

        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "400px",

            sorting: true, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            data: clients, //아래에 있는 client 배열을 데이터를 받아서
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                { name: "<spring:message code='projectManagement.002' text='No' />", type: "number", width: 50 },
                { name: "<spring:message code='projectManagement.003' text='프로젝트명' />", type: "text", width: 200 },
                { name: "<spring:message code='projectManagement.004' text='처리부서' />", type: "text", width: 100},
                { name: "<spring:message code='projectManagement.005' text='상태' />", type: "text", width: 100 },
            ]
        })
    })

    var clients = [
        { "No": 1, "프로젝트명": "시도별 나이 분포에 따른 신용등급,소득 관계 분석", "처리부서": "기업부설연구소", "상태": "데이터업로드 완료" },
        { "No": 2, "이름": "고길동", "성별": 2, "시군구": "Ap #897-1459 Quam Avenue" },
        { "No": 3, "이름": "나길동", "성별": 1, "시군구": "Ap #897-1459 Quam Avenue" },
        { "No": 4, "이름": "다길동", "성별": 2, "시군구": "Ap #897-1459 Quam Avenue" },
        { "No": 5, "이름": "김길동", "성별": 1, "시군구": "Ap #897-1459 Quam Avenue" },
        { "No": 6, "이름": "이길동", "성별": 2, "시군구": "Ap #897-1459 Quam Avenue" }
    ];
    
	</script>

<div class="container-fluid">

	<div class="form-row float-right">
		<button type="button" class="btn bg-gradient-secondary">
			<spring:message code="projectManagement.006" text="파기" />
		</button>
		&nbsp;
		<button type="button" class="btn bg-gradient-secondary">
			<spring:message code="common.017" text="조회" />
		</button>
	</div>

	<br /> <br />

	<div id="jsGrid1" class="jsgrid"
		style="position: relative; height: 100%; width: 100%;"></div>

</div>
