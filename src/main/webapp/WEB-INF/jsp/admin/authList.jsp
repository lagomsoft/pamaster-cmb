<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>  
<link rel="stylesheet" href="/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">  
<script src="/js/jstree-3.3.11/jstree.min.js"></script>
<script src="/js/jstree-grid/jstreegrid.js"></script>
<style type="text/css">
.jsgrid-grid-header {
	font-size: 14px;
}
.jsgrid-grid-body {
	font-size : 12px;
}
.jstree-grid-header-regular {
	background-color : #f9f9f9 !important; 
	border-bottom : 1px solid #e9e9e9 !important;
}
.jstree-grid-separator-regular{
	border : 1px solid #e9e9e9 !important;
}
tr.highlight td.jsgrid-cell {
    background-color: #238192;
    color : white;
}
</style>
<script type="text/javascript">
$(function(){
	
	var patternEnNum = /[^가-힣a-zA-Z0-9_!~@#$%^&*()?+=\/]/; //영문,숫자특수문자허용
	<%--권한 변수--%>
	var clickRoleCd = null;
	var roleCd = null;
	var roleNm = null;
	var roleDc = null;
	var roleUseYn = null;
	
	<%--권한별 유저 변수--%>
	var selectedUserNo = null;
	var selectedUserId = null;
	var selectedUserNm = null;
	
	var deSelectedUserNo = null;
	var deSelectedUserId = null;
	var deSelectedUserNm = null;
	
	var pageIndex = 1;
	var userRoleCount = 0;
	var searchType = 0;
	var searchCategory = null;
	var searchContents = null;
	
	<%--트리 메뉴 설정--%>
	$('#menuList').jstree({
	  "core" : {
		"multiple" : false,
	    "animation" : true,
	    
	    "check_callback" : true,
	    "data" : function(obj, cb){
	    	$.ajax({
           	 url: "/admin/getAuthMenuListInfo"
               ,type : "post"
               ,contentType: 'application/json'              
           }).done(function(res) {         	   
        	   cb.call(this,chgFormat(res));
           });
	    }
	  },
	  "plugins": ["core","ui","grid","types"],	
	  "grid": {
		"columns": [
			{width: '1000', header: "<spring:message code="authList.001" text="메뉴목록"/>"}, 
			{width: '300', header: "<spring:message code="authList.002" text="권한"/>",  
				value: function(node){
				var authBtns = $("<div>");
				if(node.id !='Home'){
					<%--권한 목록 표출--%>
					var authBtns = $("<div>");
					<%--전체 삽입될 접근 권한 (현재 권한 동작 변경으로 미사용)
					authBtns.append(
							$("<input>",{
								'type' : 'checkbox',
								'name' :"AUTH_"+node.original.SEQ,
								'value' : '100'
							})
					);
					authBtns.append($("<span>",{
						'text' : '접근'
					}))
					authBtns.append('&nbsp;');
					--%>
					if(node.original.AUTH_LIST && node.original.MENU_TYPE != '1'){						
						var auths =node.original.AUTH_LIST.split(","); 
						for(idx in auths){
							var authCd = auths[idx].split(":")[0];
							var authNm = auths[idx].split(":")[1];
							var temp = $("<input>",{
								'type' : 'checkbox',
								'class' : 'iCheck-helper',
								'name' : "AUTH_"+node.original.SEQ,
								'value' : authCd
							})
							authBtns.append($(temp).clone());
							authBtns.append($("<span>",{
								'text' : authNm
							}));
							authBtns.append('&nbsp;');
						}
					}
				}
				return(authBtns);
			}}  
		]
	  },
	  "types" : {
		    "#" : {
		      "max_children" : 10,
		      "max_depth" : 10,
		      "valid_children" : ["root"]
		    },
		    "root" : {      
		    	"icon" : "/img/bootstrap-icons/house-fill.svg" 
		    },
		    "default" : {
		    	"icon" : "/img/bootstrap-icons/folder-fill.svg"
		    },
		    "file" : {
		    	"icon" : "/img/bootstrap-icons/file-earmark-richtext.svg"
		    },
		    "process" : {
				"icon" : "/img/bootstrap-icons/segmented-nav.svg" 
			}
	  },
	});
	
	<%--초기 트리 메뉴 호출시 전체 오픈--%>
	$('#menuList').on('loaded.jstree', function () {
	     $("#menuList").jstree('open_all');
	})
	
	<%--트리 메뉴 데이타 포멧--%>
	var chgFormat = function (obj){	
		for(idx in obj){
			obj[idx].id = obj[idx].MENU_ID;
			obj[idx].parent = obj[idx].UPPER_MENU_ID;
			obj[idx].text = obj[idx].MENU_NM; 
			obj[idx].type = obj[idx].MENU_TYPE == 1? 'default' : (obj[idx].MENU_TYPE ==  2 ? 'file' : 'process') ; 
			if(!obj[idx].parent) obj[idx].parent = 'Home';	
		}	
		obj.push({id:'Home',text:'Home',parent:'#',type:'root'}); 
		return obj;
	}
	<%--해당 권한에 대한 메뉴 권한 가져오기--%>
	var setMenuAuthList = function(){
		$.ajax({
			 url: "/admin/getMenuRoleList"
		    ,type : "post"
		    ,data : JSON.stringify({
		    	'ROLE_CD' : clickRoleCd
		    })
		    ,contentType: 'application/json'              
		}).done(function(res) { 
			<%--기존 체크된 메뉴권한 해제--%>
			$("[name^='AUTH_']").each(function(){
				$(this).prop('checked',false);
			})
			<%--메뉴권한 체크--%>
		 	for( idx in res){
		 		if(res){
		 			if(res[idx].AUTH_CD){
			 		var	menuSeq= res[idx].SEQ,
				 			authCdList = res[idx].AUTH_CD.split(",");
				 		
				 		for(idx2 in authCdList){
				 			$("[name='AUTH_"+menuSeq+"'][value='"+authCdList[idx2]+"']").prop('checked',true)
				 		}
		 			}
		 		}
		 	}
		});		
	}
	
	<%--권한 그리드 목록 표출--%>
	$("#jsGrid1").jsGrid({
    	width : "100%",
		height : 300,  
		editing : false,
		selecting : true,
		rowClick : function(args) {
			<%--이전 선택 row highlight 제거--%>
			var selectedRow = $("#jsGrid1").find('table tr.highlight');
			selectedRow.removeClass('highlight');
			
			<%--현재 선택 row highlight--%>
			var $row = this.rowByItem(args.item); 
		    $row.toggleClass("highlight");
		    
			if(clickRoleCd === "NEW") {					
				if(!confirm("<spring:message code="authList.003" text="수정 중인 데이터가 있습니다 조회하시겠습니까?"/>")) {
					return;
				}
			}
			/*
			$('#ROLE_MENU_2').show();
			$('#ROLE_MENU_3').show();
			$('#ROLE_MANAGEMENT').show();
			$('#TITLE_NAME').text("권한 설정");
			*/
			$('#ROLE_MENU_2, #ROLE_MENU_3, #ROLE_MANAGEMENT').show();
			$('#TITLE_NAME').text("<spring:message code="authList.004" text="권한 설정"/>");
			
			var getData = args.item;

			roleCd = (getData["ROLE_CD"]);
			roleNm = (getData["ROLE_NM"]);
			roleDc = (getData["ROLE_DC"]);

			if(getData["USE_YN"] == '<spring:message code="authList.005" text="사용"/>') {
				roleUseYn = 'Y';
			} else {
				roleUseYn = 'N';
			}
			clickRoleCd = roleCd;
			$('#ROLE_CD').val(roleCd);
			$('#ROLE_NM').val(roleNm);
			$('#ROLE_DC').val(roleDc);
			$('#ROLE_USE_YN').val(roleUseYn);
			
			$("input[type=checkbox]").prop("checked",false);

			$('#USER_ROLE_ALL').empty();
			$('#USER_ROLE_SELECT').empty();
			pageIndex = 1;
			getUserRole();
			setMenuAuthList();
			<%--권한별 프로젝트 표출 --%>
			roleProjectStep.load(roleCd);
		},
		controller : {
			loadData : function() {

				var d = $.Deferred();				
				$.ajax({
					url : "/admin/getRoleList",
					type : "post",
					contentType: 'application/json',
					data : JSON.stringify({
					})
				}).done(function(response) {
					d.resolve(response.ROLE_LIST);
				});
				
				return d.promise();
			}
		},
	    fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
	    	{ name: "ROLE_CD", title: '<spring:message code="authList.006" text="권한코드" />', type: "text", readOnly: false, width: 60 },
	        { name: "ROLE_NM", title: '<spring:message code="authList.007" text="권한명" />', type: "text", readOnly: true, width: 40 },
	        { name: "USE_YN", title: '<spring:message code="authList.008" text="사용여부" />',type: "text", readOnly: true, width: 30 }, 
		]
	})

	<%--메뉴 권한 전체 선택 및 해제 이벤트--%>
	$("#checkAll").on("click",function(){
		$("[name^='AUTH_']").each(function(){
			$(this).prop('checked',true);
		})
	})
	$("#uncheckAll").on("click",function(){
		$("[name^='AUTH_']").each(function(){
			$(this).prop('checked',false);
		})
	})
	
	$("#checkAllProject").on("click",function(){
		$("[name^='chk_']").each(function(){
			$(this).prop('checked',true);
		})
	})
	$("#uncheckAllProject").on("click",function(){
		$("[name^='chk_']").each(function(){
			$(this).prop('checked',false);
		})
	})
	
	$("#prc_chk_1").on("click",function(){
		$("[name^='chk_'][name$='_1']").each(function(){
			$(this).prop('checked',$(this).prop('checked')?false:true);
		})
	})
	$("#prc_chk_2").on("click",function(){
		$("[name^='chk_'][name$='_2']").each(function(){
			$(this).prop('checked',$(this).prop('checked')?false:true);
		})
	})
	$("#prc_chk_3").on("click",function(){
		$("[name^='chk_'][name$='_3']").each(function(){
			$(this).prop('checked',$(this).prop('checked')?false:true);
		})
	})
	
	<%--메뉴 권한 저장 관련--%>
	$("#saveAuthMenu").on("click",function(){
		var authData  = {},
			selectedGroups = {};
		
		authData.roleCd = clickRoleCd; 
		$("[name^='AUTH_']:checked").each(function() {			
			var menuCd = $(this).attr("name").replace("AUTH_","");
			var authCd = $(this).val();
			if(selectedGroups[menuCd]){
				selectedGroups[menuCd].push(authCd);
			}else{
				selectedGroups[menuCd] = new Array();
				selectedGroups[menuCd].push(authCd);
			}		    
		});
		authData.menuCd = selectedGroups;
		 
		$.ajax({
			 url: "/admin/insertMenuRole"
		    ,type : "post"
		    ,data : JSON.stringify(authData)
		    ,contentType: 'application/json'              
		}).done(function(res) {         	   
		 		saveAuthMenuCallBack(res);
		});
			
	})
	<%--권한별 프로젝트 표출--%>
	var roleProjectStep = {
		init : function(){
			$.ajax({
				url : "/common/getCodeList"
				,type : "post"
				,contentType: 'application/json'
				,data : JSON.stringify({
					"GRP_CD" : "PROJECT_STEP"
				})
			}).done(function(res){				
				if(res && res.CODE_LIST){
					for(idx in res.CODE_LIST){
						$("#projectBody").append($("<tr>",{
							"html" : "<td role='button'>"+res.CODE_LIST[idx].CD_NM+"</td>"+
									"<td><input type='checkbox' name='chk_"+res.CODE_LIST[idx].CD+"_1'></td>"+
									"<td><input type='checkbox' name='chk_"+res.CODE_LIST[idx].CD+"_2'></td>"+
									"<td><input type='checkbox' name='chk_"+res.CODE_LIST[idx].CD+"_3'></td>"
						}));
					}
				}
				$("#projectBody tr").find("[role='button']").click(function(){
					$(this).parent().find("[type='checkbox']").each(function(){
						$(this).prop("checked",$(this).prop("checked")?false:true);
					})
				})
			});
		},
		load : function(){
			$.ajax({
				url : "/admin/selectRoleProStep"
				,type : "post"
				,contentType: 'application/json'
				,data : JSON.stringify({
					"ROLE_CD" : clickRoleCd
				})
			}).done(function(res){
				if(res){
					$("[name^='chk_']:checked").prop("checked",false);
					for(idx in res){
						var data = res[idx].AUTH;
						if(data/4 >=1){
							data -=4;
							$("[name='chk_"+res[idx].STEP_CD+"_3']").prop("checked","checked");
						}
						
						if(data/2 >=1){
							data -=2;
							$("[name='chk_"+res[idx].STEP_CD+"_2']").prop("checked","checked");
						}
						
						if(data == 1){						
							$("[name='chk_"+res[idx].STEP_CD+"_1']").prop("checked","checked");
						}
					}
				}
			});
		},
		save : function(){
			var data = { "ROLE_CD" : clickRoleCd };
			var nameVal = function(name){
				if(name[2] == '1') return 1;
				
				return (parseInt(name[2])-1)*2;
			}
			$("[name^='chk_']:checked").each(function(){
				var name = $(this).attr("name").split("_");				
				if(data["PRO_STEP_"+name[1]]>0){
					data["PRO_STEP_"+name[1]] +=nameVal(name);
				}else{
					data["PRO_STEP_"+name[1]] =nameVal(name);
				}			
			})
			
			$.ajax({
				url : "/admin/insertRoleProStep"
				,type : "post"
				,contentType: 'application/json'
				,data : JSON.stringify(data)
			}).done(function(){
				saveAuthMenuCallBack();
			})
		}
	}
	
	
	
	var saveAuthMenuCallBack = function(){
		alert("<spring:message code="authList.009" text="저장되었습니다." />");
	}
	

	<%--그리드 데이타 호출--%>
	$("#jsGrid1").jsGrid("loadData");
	
	
	$("#GET_GROUP_ROLE").on("click", function() {
		$("#jsGrid1").jsGrid("loadData");

	});
	
	function getUserRole() {		
		$('#USER_ROLE_ALL').empty();
		$('#USER_ROLE_SELECT').empty();
		
		$.ajax({
			url : "/admin/getUserRole",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({
				ROLE_CD : clickRoleCd
				,PAGE_INDEX : pageIndex
				,SEARCH_TYPE : searchType
				,SEARCH_CATEGORY : searchCategory
				,SEARCH_CONTENTS : searchContents
			})
		}).done(function(response) {
			if (response.SUCCESS == "0") {
				userRoleCount = response.USER_ROLE_COUNT;
				
				var userList = response.USER_ALL_LIST;

				for(var i=0;i<userList.length;i++){
					var userInfo = userList[i];
					
					$('#USER_ROLE_ALL').append(
						'<option id="' + userInfo["USR_ID"] + '" value="' + userInfo["USR_NO"] + '">'+userInfo["USR_NM"]+ '(' + userInfo["USR_ID"] + ')</option>'
					);
				} 

				var userSelectList = response.USER_SELECT_LIST;

				for(var i=0;i<userSelectList.length;i++){
					var userInfo = userSelectList[i];
					
					$('#USER_ROLE_SELECT').append(
						'<option id="' + userInfo["USR_ID"] + '" value="' +	userInfo["USR_NO"] + '">'+userInfo["USR_NM"]+ '(' + userInfo["USR_ID"] + ')</option>'
					);
				}
			}

			pagination(pageIndex);
		});
	}

	$("#ADD_GROUP_ROLE").on("click", function() {
		$('#custom-tabs-one-tab').click();
		
		clickRoleCd = "NEW";
		$('#ROLE_CD').val('');
		$('#ROLE_NM').val('');
		$('#ROLE_DC').val('');
		$('#ROLE_USE_YN').val('Y');

		$('#ROLE_MANAGEMENT').show();
		
		$('#DELETE_GROUP_ROLE').hide();
		$('#ROLE_MENU_2').hide();
		$('#ROLE_MENU_3').hide();
		
		<%--이전 선택 row highlight 제거--%>
		var selectedRow = $("#jsGrid1").find('table tr.highlight');
		selectedRow.removeClass('highlight');
		
		$('#TITLE_NAME').text("<spring:message code="authList.010" text="신규 권한 설정"/>");  
	});

	$("#SAVE_GROUP_ROLE").on("click", function() {
		if(!confirm("<spring:message code="authList.011" text="저장 하시겠습니까?"/>")) {
			return;
		}
		
		if($('#ROLE_CD').val() == '' || document.getElementById("ROLE_CD").value.length > 15) {
			alert("<spring:message code="authList.012" text="권한 코드를 15자 이내로 입력하세요."/> " + $("#ROLE_CD").val().length + "/15");
			$('#ROLE_CD').focus();
			return;
		}

		if($('#ROLE_NM').val() == '' || document.getElementById("ROLE_NM").value.length > 15) {
			alert("<spring:message code="authList.013" text="권한 이름을 30자 이내로 입력하세요."/> " + $("#ROLE_NM").val().length + "/30");
			$('#ROLE_NM').focus();
			return;
		}

		if($('#ROLE_DC').val() == '' || document.getElementById("ROLE_DC").value.length > 60) {
			alert("<spring:message code="authList.014" text="권한 설명을 60자 이내로 입력하세요."/> " + $("#ROLE_DC").val().length + "/60");
			$('#ROLE_DC').focus();
			return;
		}
		
		if(RegExp(patternEnNum).test($('#ROLE_CD').val())){
			alert("<spring:message code="authList.015" text="권한 코드는 영문,숫자만 입력가능합니다."/>"); 
		}
		
		$.ajax({
			url : "/admin/saveRole",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({
				CLICKED_ROLE : clickRoleCd
				,ROLE_CD : $('#ROLE_CD').val()
                ,ROLE_NM : $('#ROLE_NM').val()
                ,ROLE_DC : $('#ROLE_DC').val()
                ,USE_YN : $('#ROLE_USE_YN').val()
			})
		})
		.done(function(response) {
			if (response.SUCCESS == "0") {
				alert('<spring:message code="authList.016" text="권한이 저장되었습니다." />');
			}
			$("#GET_GROUP_ROLE").click();
			$("#ROLE_MANAGEMENT").hide();
			clickRoleCd = null;
		});

	});

	$("#DELETE_ROLE").on("click",function() {

		if(!confirm("<spring:message code="authList.019" text="삭제 하시겠습니까?"/>")){
			return;
		}
		
		$.ajax({
			url : "/admin/deleteRole",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({
				ROLE_CD : clickRoleCd,
			})
		}).done(function(response) {
			if (response.SUCCESS == "0") {
				alert('<spring:message code="authList.017" text="처리 되었습니다." />');
				location.reload();
			}
		});
	});

	$("#SAVE_ROLE_MENU").on("click",function() {
		if(!confirm("<spring:message code="authList.018" text="저장 하시겠습니까?"/>")){
			return;
		}
		setMenuRoleChecked();
		
		$.ajax({
			url : "/admin/saveRoleMenu",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({
				ROLE_CD : clickRoleCd
				,CHECK_LIST : chekedMenuList
			})
		}).done(function(response) {
			if (response.SUCCESS == "0") {
				alert('<spring:message code="authList.017" text="처리 되었습니다." />');
				chekedMenuList = null;
				location.reload();
			}
		});
	});
	
	$("#SELECT_BTN").on("click",function() {
		if(selectedUserId == null) {
			return;
		}
		
		if($("#USER_ROLE_SELECT").find("option[value='"+$("#USER_ROLE_ALL option:selected").val()+"']").length > 0){
			alert("<spring:message code="authList.020" text="중복된 유저에게 권한을 부여할 수 없습니다."/>");
			return;
		}

		$("#USER_ROLE_SELECT").append($("#USER_ROLE_ALL option:selected").clone()[0]);
	});

	$("#DESELECT_BTN").on("click",function() {
		$("#USER_ROLE_SELECT").find("#" + deSelectedUserId).remove();
	});

	$("#USER_ROLE_ALL").change(function() {
		selectedUserNo = $(this).children(":selected").val();
		selectedUserId = $(this).children(":selected").attr("id");
	  });

	$("#USER_ROLE_SELECT").change(function() {
		deSelectedUserNo = $(this).children(":selected").val();
		deSelectedUserId = $(this).children(":selected").attr("id");
	  });

	$("#SAVE_USER_ROLE").on("click",function() {
		if(!confirm("<spring:message code="authList.018" text="저장 하시겠습니까?"/>")){
			return;
		}
		
		var seletedUserArray = $('#USER_ROLE_SELECT').find('option').map(function() {return $(this).val();}).get();

		$.ajax({
			url : "/admin/saveUserRoleList",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({
				USER_LIST : seletedUserArray
				,ROLE_CD : clickRoleCd
			})
		}).done(function(response) {
			if (response.SUCCESS == "0") {
				alert("<spring:message code="authList.021" text="저장 되었습니다."/>");
			}
		});
	});

	$("#SEARCH_BTN").on("click",function() {
		if(!confirm("<spring:message code="authList.022" text="수정 중인 데이터는 없어집니다.. 조회하시겠습니까?"/>")){
			return;
		}
		searchType = 1;
		searchCategory = $('#SEARCH_CATEGORY').val();
		searchContents = $('#SEARCH_CONTENTS').val();
		
		getUserRole();
	});
	
	$("#SAVE_PRC_BTN").on("click",function(){
		roleProjectStep.save();
	});
	
	function pagination(currentPage) {
		var totalData = userRoleCount;    <%-- 총 데이터 수 --%>
		var dataPerPage = 10;    <%-- 한 페이지에 나타낼 데이터 수 --%>
		var pageCount = 5;        <%-- 한 화면에 나타낼 페이지 수 --%>
        var totalPage = Math.ceil(userRoleCount/dataPerPage);    <%-- 총 페이지 수 --%>
        var pageGroup = Math.ceil(currentPage/pageCount);    <%-- 페이지 그룹 --%>
        
        var last = pageGroup * pageCount;    <%-- 화면에 보여질 마지막 페이지 번호 --%>
        if(last > totalPage)
            last = totalPage;
        
        var first = last - (pageCount-1);    <%-- 화면에 보여질 첫번째 페이지 번호 --%>
        if (first < 0)
            first = 1;
        var next = last+1;
        var prev = first-1;
        
        var html = "";
        
        if(prev > 0)
            html += '<a class="page-link" id="prev" href="#"><</a>';
        
        for(var i=first; i <= last; i++){
            html += "<a class='page-link' href='#' id=" + i + ">" + i + "</a> ";
        }
        
        if(last < totalPage)
            html += '<a class="page-link" id="next" href="#">></a>';
            
        $("#PAGING_DIV").html(html);    <%-- 페이지 목록 생성 --%>
        $("#PAGING_DIV a").css("color", "black");
        $("#PAGING_DIV a#" + currentPage).css({"text-decoration":"none", 
                                           "color":"blue", 
                                           "font-weight":"bold"});    <%-- 현재 페이지 표시 --%>
                                           
        $("#PAGING_DIV a").click(function(){
            var $item = $(this);
            var $id = $item.attr("id");
            var selectedPage = $item.text();
            
            if($id == "next")    selectedPage = next;
            if($id == "prev")    selectedPage = prev;
            
            pagination(selectedPage);
            pageIndex = selectedPage;

            getUserRole();
        });
		
	}
	
	$(".nav-item>a").click(function() {
   		clicked_menu = $(this).attr("value")
		
   		if($(this).parent().attr("class") != 'nav-item menu-is-opening menu-open') {

   			$(this).parent().attr('class', 'nav-item menu-is-opening menu-open');
   			
   	   	} else {
   	   		$(this).parent().attr('class', 'nav-item');
   	   	}
	});

	$("#link>a").click(function() {
   		clicked_menu = $(this).attr("value")

	});
	
	roleProjectStep.init();
	
})
</script>
<section>
	<div class="row">
		<div class="col-lg-6" style="max-width:30%;">  
			<div class="x_panel">
				<div class="row">
					<div class="col-xs-12">
						<div class="x_title x_underline">
							<h2><spring:message code="authList.025" text="권한 조회"/></h2> 
							<div class="btn-area" style="display:block">
			   	 				<button type="button" class="btn btn-secondary btn-sm btn-primary" id="GET_GROUP_ROLE">
									<i class="fa fa-bars"></i>&nbsp;<spring:message code="authList.023" text="조회"/>
								</button>
								<button type="button" class="btn btn-secondary btn-sm btn-success" id="ADD_GROUP_ROLE">
									<i class="fa fa-pencil"></i>&nbsp;<spring:message code="authList.024" text="신규"/>
								</button>
				   	 		</div>
						</div>
		   	 		</div>
		   	 	</div>	
				<div class="col-xs-12">
					<div id="jsGrid1" class="jsgrid" style="position: relative; width: 100%;"></div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div id="ROLE_MANAGEMENT" style="display:none;">
				<div class="x_panel">
					<div class="col-xs-12">
						<ul class="nav nav-tabs bar_tabs" id="custom-tabs" role="tablist">
							<li class="nav-item active" id="ROLE_MENU_1">
								<a class="nav-link active" id="custom-tabs-one-tab" data-toggle="pill" 
								href="#custom-tabs-one-home" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true"><spring:message code="authList.002" text="권한"/></a>
							</li>
							<li class="nav-item" id="ROLE_MENU_2">
								<a class="nav-link" id="custom-tabs-two-tab" data-toggle="pill" 
								href="#custom-tabs-two-home" role="tab" aria-controls="custom-tabs-two-home" aria-selected="false"><spring:message code="authList.026" text="권한별 메뉴"/></a>
							</li>
							<li class="nav-item" id="ROLE_MENU_3">
								<a class="nav-link" id="custom-tabs-one-three-tab" data-toggle="pill" 
								href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="false"><spring:message code="authList.027" text="권한별 사용자"/></a>
							</li>
							<li class="nav-item" id="ROLE_MENU_4">
								<a class="nav-link" id="custom-tabs-one-four-tab" data-toggle="pill" 
								href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="false"><spring:message code="authList.040" text="권한별 프로젝트 표출"/></a>
							</li>
						</ul>

						<div class="tab-content" id="custom-tabs-one-tabContent" style="height:auto;">		
							<div class="tab-pane active" id="custom-tabs-one-home" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab">
								<div class="table-responsive">
									<div class="x_title x_underline">
										<h2 id="TITLE_NAME"><spring:message code="authList.004" text="권한설정"/></h2>
										<div class="btn-area" style="display:block">
											<button type="button" class="btn btn-secondary btn-sm btn-danger" id="DELETE_ROLE">
												<i class="fa fa-eraser"></i>&nbsp;<spring:message code="authList.028" text="삭제"/>
											</button>&nbsp;
											<button type="button" class="btn btn-secondary btn-sm btn-info" id="SAVE_GROUP_ROLE">
												<i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message code="authList.029" text="저장"/>
											</button>									
										</div>
									</div>
									
									<blockquote class="quote-secondary">
										<div class="form-group">
											<label for="ROLE_CD"><spring:message code="authList.030" text="권한코드"/></label>
											<input type="text" id="ROLE_CD" class="form-control" value="" placeholder="<spring:message code="authList.030" text="권한코드"/>">
										</div>
										<div class="form-group">
											<label for="ROLE_NM"><spring:message code="authList.031" text="권한명"/></label>
											<input type="text" id="ROLE_NM" class="form-control" value="" placeholder="<spring:message code="authList.031" text="권한명"/>">
										</div>
										<div class="form-group">
											<label for="ROLE_CD"><spring:message code="authList.032" text="권한 설명"/></label>
											<input type="text" id="ROLE_DC" class="form-control" value="" placeholder="권한 설명">
										</div>
										<div class="form-group">
											<label for="ROLE_USE_YN"><spring:message code="authList.008" text="사용여부"/></label><br/>
											<select class="form-control custom-select" id="ROLE_USE_YN" style="width: 200px;">
												<option value="Y"><spring:message code="authList.005" text="사용"/></option>
												<option value="N"><spring:message code="authList.033" text="미사용"/></option>
											</select>
										</div>
									</blockquote>	
								</div>																				
							</div>
							<div class="tab-pane fade" id="custom-tabs-two-home" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">										
								<div class="x_title x_underline">
									<h2 id="TITLE_NAME"><spring:message code="authList.004" text="권한설정"/></h2>
									<div class="btn-area" style="margin-bottom:10px; display:block;">
										<button type="button" class="btn btn-dark btn-sm" style="margin-right : 10px;" id="checkAll">
											<spring:message code="authList.034" text="전체선택"/></button>
							    	 	<button type="button" class="btn btn-dark btn-sm" style="margin-right : 10px;"id="uncheckAll">
							    	 		<spring:message code="authList.035" text="전체 해제"/></button>
							    	 	<button type="button" class="btn btn-sm btn-info" id="saveAuthMenu">
											<i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message code="common.047" text="저장"/>
										</button>				
									</div>
								</div>
								<div class="form-row float-right">		
									<form name = "menuAuthFrm" id="menuAuthFrm">													
						    	 	<%--메뉴 트리 표출 위치--%>
						    	 	<div style="border: 1px solid #cccccc">
							    	 	<div  id="menuList" style="width:100%;">				    	 		
							    	 	</div>					    	 	
						    	 	</div>
						    	 	</form>
						    	</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
								<div class="table-responsive">
									<div class="x_title x_underline">
										<h2 id="MENU_USER_TITLE"><spring:message code="authList.027" text="권한별 사용자"/></h2>
																
										<div class="btn-area" style="display:block">
											<button type="button" class="btn btn-secondary btn-sm btn-info"  id="SAVE_USER_ROLE">
												<i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message code="common.047" text="저장"/>
											</button>
										</div>
									</div>
									<div class="row">
										<br>
										<div class="col-xs-12">									
											<div class="input-group col-xs-6">
												<select id="SEARCH_CATEGORY" name="SEARCH_CATEGORY" class="form-control custom-select" style="width:30%;">
													<option value="ALL"><spring:message code="authList.037" text="전체"/></option>
													<option value="DEPARTMENT"><spring:message code="authList.038" text="부서"/></option>
												</select>
												<input class="form-control" style="width:50%; height:auto;" id="SEARCH_CONTENTS" type="search" placeholder="Search" aria-label="Search">
												<span class="input-group-append" style="width:20%;">
													<button type="submit" id="SEARCH_BTN" class="btn btn-primary" style="min-width:58px; height:34px;"><spring:message code="authList.023" text="조회"/></button>
												</span>
											</div>
											<div class="bootstrap-duallistbox-container row moveonselect mt-3">
												<div class="box1 col-md-6">   
													<label for="bootstrap-duallistbox-nonselected-list_" style="display: none;"></label>   
													<span class="info-container">     
														<span class=" fs-5">전체사용자</span>     
														<button type="button" class="btn btn-sm clear1" style="float:right!important;">show all</button>   
													</span>
													<div class="btn-group buttons">     
														<button type="button" class="btn btn-dark" style="width: inherit;" title="선택" id="SELECT_BTN">&gt;&gt;</button>        
													</div>   
													<select size="10" id="USER_ROLE_ALL" class="form-control p-2" style="overflow-y:hidden;">
													</select>
													<ul class="pagination justify-content-center mt-3" id="PAGING_DIV"></ul>
												</div>
												<div class="box1 col-md-6">   
													<label for="bootstrap-duallistbox-nonselected-list_" style="display: none;"></label>   
													<span class="info-container">     
														<span class="fs-5 mb-2"><spring:message code="authList.040" text="권한별 프로젝트 표출"/></span>     
														<button type="button" class="btn btn-sm clear1" style="float:right!important;">show all</button>   
													</span>
													<div class="btn-group buttons">     
														<button type="button" class="btn moveall btn-dark" title="해제" id="DESELECT_BTN">&lt;&lt;</button>        
													</div>   
													<select size="10" id="USER_ROLE_SELECT" class="form-control p-2" style="overflow-y:hidden;">
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
								<div class="table-responsive">
									<div class="x_title x_underline">
										<h2 id="MENU_USER_TITLE"><spring:message code="authList.040" text="권한별 프로젝트 표출"/></h2>
										<div class="btn-area" style="display:block">
											<button type="button" class="btn btn-dark btn-sm" style="margin-right : 10px;" id="checkAllProject">
												<spring:message code="authList.034" text="전체선택"/></button>
								    	 	<button type="button" class="btn btn-dark btn-sm" style="margin-right : 10px;" id="uncheckAllProject">
								    	 		<spring:message code="authList.035" text="전체 해제"/></button>
											<button type="button" class="btn btn-sm btn-info"  id="SAVE_PRC_BTN">
												<i class="fa fa-pencil-square-o"></i>&nbsp;<spring:message code="common.047" text="저장"/>
											</button>
											<div></div>
										</div>	
									</div>
									<div class="row">
										<div class="col-12">										
											<table class="table">
												<thead>
													<tr>
														<th rowspan="2"><spring:message code="authList.042" text="프로젝트 상태"/></th>
														<th colspan="3"><spring:message code="authList.043" text="소유자 범위에 따른 권한"/></th> 
													</tr>	
													<tr>
														<th style="width:100px;" role="button" id="prc_chk_1"><spring:message code="authList.044" text="소유자"/></th>
														<th style="width:100px;" role="button" id="prc_chk_2"><spring:message code="authList.038" text="부서"/></th>
														<th style="width:100px;" role="button" id="prc_chk_3"><spring:message code="authList.037" text="전체"/></th>
													</tr>												
												</thead>
												<tbody id="projectBody">
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
					    </div>
				    </div>
			    </div>	 
			</div>
		</div>
	</div>
</section>