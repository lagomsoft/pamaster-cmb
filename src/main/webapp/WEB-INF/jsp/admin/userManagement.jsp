<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<style type="text/css">
	.input-group {
		position: relative;
		display: flex;
		border-collapse: separate;
		align-items: stretch;
	}
	.d-none{
		display:none !important;
	}
	.select-span{
		width: 15%;
    	margin-block: auto;
    	font-weight: bold;
	}
</style>

<script type="text/javascript">
	var pageIndex = 1;
	var pageSize = 10;
	var pageButtonCount = 5;
	var da = {
				data : new Array(),
				itemsCount : 0
			 };
	
	<%-- 검색중인지 아닌지--%>
	var isSearch = false;
	var isInit = false;

	var userRoleList = [];
	var userIPList = [];
	var gridChage = false;
	var groupList = [
						<c:forEach items="${GROUP_LIST}" var="group" varStatus="status">
							<c:if test="${!status.first}">,</c:if>  {Id : "${group.GP_CD}" , Name : "${group.GP_NM}"}
						</c:forEach>
					];
	var roleMapList = {
						<c:forEach items="${ROLE_LIST}" var="role" varStatus="status">
							<c:if test="${!status.first}">,</c:if> "${role.ROLE_CD}": "${role.ROLE_NM}"
						</c:forEach>
					};
	var roleList = [
						<c:forEach items="${ROLE_LIST}" var="role" varStatus="status">
							<c:if test="${!status.first}">,</c:if> {Id : "${role.ROLE_CD}" , Name : "${role.ROLE_NM}"}
						</c:forEach>
					];
	var cntFrsList = [
						{Id : "" , Name : ""}
						<c:forEach items="${CNT_FRS_LIST}" var="cntFrs" varStatus="status">
							, {Id : "${cntFrs.CD}" , Name : "${cntFrs.CD_NM}"}
						</c:forEach>
					];
	var rspofcList = [
						<c:forEach items="${RSPOFC_LIST}" var="rspofc" varStatus="status">
							<c:if test="${!status.first}">,</c:if> {Id : "${rspofc.CD}" , Name : "${rspofc.CD_NM}"}
						</c:forEach>
					];
					
	var _selectListMap = {
		GP_CD : groupList,
		ROLE_LIST : roleList,
		USR_RSPOFC : rspofcList
	}
	
	var _searchedValue = {
		CATEGORY : "",
		INPUT : "",
		GP_CD : "",
		USR_RSPOFC : "",
		ROLE_LIST : ""
	}


	// 사용자 권한 수정
	var exitRoleVal = null;
	var $roleObject = null
	var $roleList = null;
	
    $(function (type) {
        
        $("#jsGrid1").jsGrid({
            width: "100%",
            height: "auto",
            editing: true,
            sorting: false,
            paging : true,
			pageLoading : true,
			pagerFormat : "{first} {pages} {last}",
			pageFirstText : "<spring:message code='common.067' text='처음' />",
			pageLastText : "<spring:message code='common.109' text='끝' />",
			pageNavigatorNextText: "<spring:message code='common.066' text='다음' />",
    		pageNavigatorPrevText: "<spring:message code='common.065' text='이전' />",
			pageSize : pageSize,
			pageButtonCount : pageButtonCount,
			onPageChanged : function(args) {/*  */
				if(isInit) {
					pageIndex = 1;
					isInit = false;
				} else if(isSearch) {
					pageIndex = args.pageIndex;
					searchUserList();
				} else {
					pageIndex = args.pageIndex;
					loadUserList(args);
				}
			},
            rowClick : function(args){
            	if(!args.event.target.firstElementChild && !args.event.target.hasAttribute('checkicon')){
            		modalUserInfo.showModifyPopup(args);
            	}
            	
            },
            controller: {
                loadData: function(args) {
                    var d = $.Deferred();
                    d.resolve({data : $.map(da.data, function (item, itemIndex) {
                        return $.extend(item, { "index": itemIndex});
                    }),itemsCount : da.itemsCount});
                    return d.promise();
                }
            },
            fields: [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                { name: "USR_NO", title:'<spring:message code="userManagement.002" text="사용자번호" />', width: 80, isSearch: "Y"
                    , itemTemplate: function (_, item) {
		                    return $("<div>").append(item.USR_NO);
		                }
                }
               ,{ name: "USR_ID", title:'<spring:message code="userManagement.003" text="사용자아이디" />', type: "text", width: 120, isSearch: "Y", editable:"true"}
               ,{ name: "USR_NM", title:'<spring:message code="userManagement.004" text="사용자명" />', type: "text", width: 100, isSearch: "Y" }
               ,{ name: "USR_EMAIL", title:'<spring:message code="userManagement.045" text="이메일" />', type: "text", width: 120, css : '_ellipsis', isSearch: "Y" }
<%--               ,{ name: "GP_CD", title:'<spring:message code="userManagement.005" text="사용자부서" />', type: "select", items : groupList, valueField: "Id", textField: "Name", width: 100, isSearch: "Y", isSelect: "Y"}
               ,{ name: "USR_RSPOFC", title:'<spring:message code="userManagement.053" text="사용자직책" />', type: "select", items : rspofcList, valueField: "Id", textField: "Name", width: 100, isSearch: "Y", isSelect: "Y"}
               ,{ name: "OFFM_TELNO", title:'<spring:message code="userManagement.047" text="사무실번호" />', type: "text", width: 120 ,css : '_ellipsis', isSearch: "Y"}
--%>               ,{ name: "ROLE_LIST", title:'<spring:message code="userManagement.006" text="사용자권한" />', width: 140, isSearch: "Y", isSelect: "Y"
                    , itemTemplate: function (_, item) {
                        var roleText = "";
                    	var usrNo = item.USR_NO;
                    	if(item.NEW_ROW_NO){
                    		usrNo = "TMP_" + item.NEW_ROW_NO;
                        }
                    	if(item.ROLE_LIST){
                    		for(var ri = 0 ; ri < item.ROLE_LIST.length ; ri++ ){
                    			var roleData = item.ROLE_LIST[ri];
                    			if(roleMapList[roleData.ROLE_CD]){
                        			if(roleText != ""){
                        				roleText += ", ";
                            		}
                       				roleText += roleMapList[roleData.ROLE_CD];
                        		}
                    		}
                       		userRoleList[usrNo] = item.ROLE_LIST;
                        }
                    	return $('<div class="USR_ROLE_'+ usrNo +'">' + roleText+ '</div>');
	                }
                }
               <%--,{ name: "ROLE_BTN", title:'<spring:message code="userManagement.007" text="권한수정" />', width: 120
                    , itemTemplate: function (_, item) {

                    	var usrNo = item.USR_NO;
                    	var roleBtn = "";
                    	
                    	if(item.USR_NO != null){
                    		roleBtn = ' <button type="button" class="btn btn-dark" data-index="'+item.index+'" data-toggle="modal" data-target="#MODAL_ROLE" usrno="' + usrNo + '" onclick="openUserRolePopup(this);">' + '<spring:message code="common.068" text="수정" />' + '</button>';
                        }
                    	return $('<div style="text-align:center">').append(roleBtn);
	                }
                }
               ,{ name: "IP_LIST", title:'<spring:message code="userManagement.008" text="사용자IP" />', width: 140
                    , itemTemplate: function (_, item) {
                        var ipText = "";
                        
                        var wIPCnt = 0;
                        var bIPCnt = 0;
                    	var usrNo = item.USR_NO;
                    	
                    	if(item.NEW_ROW_NO){
                    		usrNo = "TMP_" + item.NEW_ROW_NO;
                        }
                        
                    	if(item.IP_LIST){
                    		for(var ii = 0 ; ii < item.IP_LIST.length ; ii++ ){
                    			var ipata = item.IP_LIST[ii];
                    			if(ipata.IP_TYP == "W"){
                    				wIPCnt++;
                        		}else{
                    				bIPCnt++;
                            	}
                    		}
                    		
                    		userIPList[usrNo] = item.IP_LIST;
                        }
                        
                        if(wIPCnt > 0){
                        	ipText = wIPCnt + '<spring:message code="userManagement.009" text="개의 접근 가능 IP" />';
                        }else if(bIPCnt > 0){
                        	ipText = bIPCnt + '<spring:message code="userManagement.010" text="개의 접근 불가 IP" />';
                        }
                                                
                    	return $('<div class="USR_IP_'+ usrNo +'">' + ipText+ '</div>');
	                }
                }
               ,{ name: "IP_BTN", title:'<spring:message code="userManagement.011" text="IP수정" />', width: 120
                    , itemTemplate: function (_, item) {
                        
                    	var usrNo = item.USR_NO;
                    	var ipBtn = "";
                    	
                    	if(item.USR_NO != null){
                    		ipBtn = ' <button type="button" class="btn btn-dark" data-index="'+item.index+'" data-toggle="modal" data-target="#MODAL_IP" usrno="' + usrNo + '" onclick="openUserIPPopup(this);">' + '<spring:message code="common.068" text="수정" />' + '</button>';
                        }
                    	return $('<div style="text-align:center">').append(ipBtn);
	                }
                }--%>
               ,{ name: "CNT_FRS", title:'<spring:message code="userManagement.013" text="연락처 앞자리" />', type: "text", width: 100, isSearch: "N"}
               ,{ name: "CNT_MDL", title:'<spring:message code="userManagement.014" text="연락처 중간자리" />', type: "text", width: 100, isSearch: "N"}
               ,{ name: "CNT_END", title:'<spring:message code="userManagement.015" text="연락처 끝자리" />', type: "text", width: 100, isSearch: "N"}
               ,{ name: "PW_RESET_BTN", title:'<spring:message code="userManagement.048" text="비밀번호초기화" />', width: 120
                    , itemTemplate: function (_, item) {
                        
                    	var usrNo = item.USR_NO;
                    	var tmpRowNoTxt = "";
                    	
                    	if(item.USR_NO != null){
                    		var pwResetBtn = 
                    			  ' <button type="button" class="btn btn-dark"  checkIcon="icon" usrno="' + usrNo + '" ' + tmpRowNoTxt +' onclick="pwReset(this)">'
                    			  + '<spring:message code="userManagement.049" text="초기화" />'
                    			  + '</button>';
                        }
                    	
                    	return $('<div style="text-align:center">').append(pwResetBtn);
	                }
                }
               ,{ name: "PW_INITL_YN", title:'<spring:message code="userManagement.016" text="비밀번호 초기화여부" />', width: 60, isSearch: "Y"
                    , itemTemplate: function (_, item) {
                        if(item.PW_INITL_YN == 0 || item.PW_INITL_YN == 'N'){
		                    return $("<div>").append("N");                            
                        }else{
		                    return $("<div>").append("Y");
                        }
	                }
                }
               ,{ name : "LOCK_YN", title : '<spring:message code="userManagement.017" text="잠금여부" />', type: "select", isSearch: "Y", items : [
	                { Name: "N", Id: "N" }
                   ,{ Name: "Y", Id: "Y" }
               ], valueField: "Id", textField: "Name", width: 100 }
               ,{ name : "APPROVAL_YN", title : '<spring:message code="userManagement.20" text="승인여부" />', type: "select", isSearch: "Y", items : [
	                { Name: "N", Id: "N" }
                   ,{ Name: "Y", Id: "Y" }
               ], valueField: "Id", textField: "Name", width: 100 }
               ,{ name: "USR_DELETE_BTN", title:'<spring:message code="userManagement.064" text="사용자 삭제" />', width: 120
                   , itemTemplate: function (_, item) {
                       
                   	var usrNo = item.USR_NO;
                   	var tmpRowNoTxt = "";
                   	
                   	if(item.USR_NO != null){
                   		var usrDelBtn = 
                   			  ' <button type="button" class="btn btn-dark" checkIcon="icon" usrno="' + usrNo + '" ' + tmpRowNoTxt +' onclick="usrDelete(this)">'
                   			  + '<spring:message code="userManagement.040" text="삭제" />'
                   			  + '</button>';
                       }
                   	 
                   	return $('<div style="text-align:center">').append(usrDelBtn);
	                }
               }
               ,{ name : "NEW_ROW_NO", type: "text", css:"d-none", width: 0 }
            ]
        });
        
        $("#GET_USR").on("click", function(){
	        loadUserList();
        });

        loadPAMsFile("EXE_FILE_DIV", {
				 maxFileCount 	: 1
				,pamsFileType : ['xls']
		});
		
		$("#USR_FILE_UPLOAD_BTN").on("click", function(){
			
	        // 등록할 파일 리스트
	        var fileCnt = pamsFileCount();
	        
	        // 파일이 있는지 체크
	        if(fileCnt == 0){
	            // 파일등록 경고창
	            alert('<spring:message code="common.070" text="사용자 정보 파일을 업로드 하세요." />');
	            return false;
	        }

	        if(confirm('<spring:message code="common.007" text="등록 하시겠습니까?" />')){
                
	            $.ajax({
	                url:"/admin/uploadUserFile",
	                type:'POST',
	                enctype:'multipart/form-data',
	                processData:false,
	                contentType:false,
	                dataType:'json',
	                cache:false,
	                data:getFilData(),
	                success:function(result){
	                    if(result.RETURN_TYPE == "SUCCESS"){
		                    
							$("#jsGrid1").jsGrid("option", "sorting", false);
							
							var listSize = result.USR_LIST.length;
							
	                    	for(var ei = 0; ei < listSize; ei++){

	                    		//newRowNo++;
		                    	
		                    	var userData = result.USR_LIST[ei];
		                    	//userData.PW_INITL_YN  = 1;
		                    	//userData.LOCK_YN  = 0;
		                    	//userData.NEW_ROW_NO = newRowNo;
		                    	
		                        $("#jsGrid1").jsGrid("insertItem", userData).done(function() {
			                        var rowId = $("#jsGrid1").jsGrid("option", "data").length - 1;
		                        	//updateGrid1List[rowId] = rowId;
		                        	//newGrid1List[rowId] = 1;
		                        });
		                    }
	                    	
	                    }else if(result.RETURN_TYPE == "EMPTY"){
		                    alert('<spring:message code="common.008" text="파일이 존재하지 않습니다." />');
	                    }else if(result.RETURN_TYPE == "ERR"){
		                    alert('<spring:message code="common.009" text="엑셀 파일에 문제가 발생하였습니다. 파일을 확인하세요." />');		                    
	                    }else{
		                    
	                    }
	                }
	            });
	        }
	        
		});
		
		searchFieldListInit();
		
		$(".form-control").keydown(function(key) {
			if(key.keyCode == 13) {
				enterSearch();
			}
		});
		
		$("#MODAL_DOMAIN_SELECT").change(function() {
			if($(this).val() != "self") {
				$("#MODAL_EMAIL_DOMAIN").attr("disabled", true);
			} else {
				$("#MODAL_EMAIL_DOMAIN").attr("disabled", false);
			}
		});
		
    })
    
    var modalUserInfo = {
    	email : "",
    	selectItem : {},
    	showAddPopup : function() {
			var $modal =  $('#MODAL_USER');
			$modal.find("[name='modal_user_form']")[0].reset();
			$modal.find("#MODAL_UPDATE_BTN").addClass("d-none");
			$modal.find("#MODAL_CONFIRM_BTN").removeClass("d-none");
			$modal.find(".modify_only_none").addClass("d-none");
			$modal.find(".modify_only_disabled").attr("disabled", false);
			$modal.find(".modal-title").text('<spring:message code="userManagement.060" text="신규 사용자 생성" />');
			$modal.modal();
		},
		showModifyPopup : function(args){
			modalUserInfo.selectItem = args.item;
			var $modal = $('#MODAL_USER');
			$modal.find("[name='modal_user_form']")[0].reset();
			$modal.find("#MODAL_UPDATE_BTN").removeClass("d-none");
			$modal.find("#MODAL_CONFIRM_BTN").addClass("d-none");
			$modal.find(".modify_only_none").removeClass("d-none");
			$modal.find(".modify_only_disabled").attr("disabled", true);
			$modal.find(".modal-title").text('<spring:message code="userManagement.063" text="사용자 수정" />');
			$modal.find("#BTN_ROLE_UPDATE").attr("data-index", args.item.index);
			$modal.find("#BTN_ROLE_UPDATE").attr("usrno", args.item.USR_NO);
			$modal.find("#BTN_IP_UPDATE").attr("data-index", args.item.index);
			$modal.find("#BTN_IP_UPDATE").attr("usrno", args.item.USR_NO);
			for(idx in args.item){					
				$modal.find("[data-type='"+idx+"']").each(function(){
					if($(this).prop("tagName").toUpperCase() == "TD"){
						$(this).text(args.item[idx]);
					}else{
						$(this).val(args.item[idx]);
					}
				})
				if(idx == "USR_EMAIL"){
					var email = emailSplit(args.item[idx]);
					$modal.find("#MODAL_EMAIL").val(email[0]);
					$modal.find("#MODAL_EMAIL_DOMAIN").val(email[1]);
					$modal.find("#MODAL_DOMAIN_SELECT").val("self");
				}
				if(idx == "ROLE_LIST"){
					var roleText = ""
            		for(var ri = 0 ; ri < args.item.ROLE_LIST.length ; ri++ ){
            			var roleData = args.item.ROLE_LIST[ri];
            			if(roleMapList[roleData.ROLE_CD]){
                			if(roleText != ""){
                				roleText += ", ";
                    		}
               				roleText += roleMapList[roleData.ROLE_CD];
                		}
            		}
            		$modal.find("#MODAL_ROLE_TEXT").text(roleText);
                }
                var ipText = "";
                var wIPCnt = 0;
                var bIPCnt = 0;
            	if(idx == "IP_LIST"){
            		for(var ii = 0 ; ii < args.item.IP_LIST.length ; ii++ ){
            			var ipata = args.item.IP_LIST[ii];
            			if(ipata.IP_TYP == "W"){
            				wIPCnt++;
                		}else{
            				bIPCnt++;
                    	}
                    	userIPList[args.item.USR_NO] = args.item.IP_LIST;
            		}
					if(wIPCnt > 0){
	                	ipText += wIPCnt + '<spring:message code="userManagement.009" text="개의 접근 가능 IP" />';
	                }
	                if(bIPCnt > 0){
	                	if(wIPCnt > 0) {
	                		ipText += '\n'
	                	}
	                	ipText += bIPCnt + '<spring:message code="userManagement.010" text="개의 접근 불가 IP" />';
	                }
	                $modal.find("#MODAL_IP_TEXT").text(ipText);
                }
			}
			$modal.modal();
		},
		addRow : function(){
			//---------------------
			var regFrs = /(\d{3}$)/g;
			var regMdl = /(\d{3}|\d{4})/g;
			var regEnd = /(\d{4}$)/g;
			var isID = /^[a-z0-9][a-z0-9_\-]{4,49}$/;
			var isEmail = /^[a-z0-9][a-z0-9_\-]{0,63}$/;
			var isNM = /^[a-zA-Zㄱ-힣]{2,29}/;
			//---------------------
			var $modal = $('#MODAL_USER');
			var check = true;
			modalUserInfo.email = emailMapping();
			var newItem = {
				USR_ID : $modal.find("#MODAL_USR_ID").val(),
				USR_NM : $modal.find("#MODAL_USR_NM").val(),
				USR_EMAIL : modalUserInfo.email,
//				GP_CD : $modal.find("#MODAL_GP_CD").val(),
//				USR_RSPOFC : $modal.find("#MODAL_USR_RSPOFC").val(),
//				OFFM_TELNO : $modal.find("#MODAL_OFFM_TELNO").val(),
				CNT_FRS : $modal.find("#MODAL_CNT_FRS").val(),
				CNT_MDL : $modal.find("#MODAL_CNT_MDL").val(),
				CNT_END : $modal.find("#MODAL_CNT_END").val(),
				PW_INITL_YN : "Y",
				LOCK_YN : "N",
				APPROVAL_YN : "Y",
				NEW : "Y"
			}
			var isNewItem = {
					is_USR_ID : $modal.find("#MODAL_USR_ID").val(),
					is_USR_NM : $modal.find("#MODAL_USR_NM").val(),
					is_USR_EMAIL : modalUserInfo.email,
//					is_GP_CD : $modal.find("#MODAL_GP_CD").val(),
//					is_USR_RSPOFC : $modal.find("#MODAL_USR_RSPOFC").val()
			}
			for(var bi in isNewItem){
				if(isNewItem[bi] == "" || isNewItem[bi] == null) {
					check = false;
				}
			}
			if (document.getElementById('MODAL_USR_ID').value == '') {
	            alert('<spring:message code="joinUser.009" text="아이디를 입력하세요."/>');
	            MODAL_USR_ID.focus();
	            return false;
	        }
			if (!isID.test(document.getElementById('MODAL_USR_ID').value)) {
	            alert('<spring:message code="joinUser.035" text="아이디는 5~20자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다."/>');
	            MODAL_USR_ID.focus();
	            return false;
	        }
			if (document.getElementById('MODAL_EMAIL').value == '') {
	            alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
	            MODAL_EMAIL.focus();
	            return false;
	        }
			if (!isEmail.test(document.getElementById('MODAL_EMAIL').value)) {
	            alert('<spring:message code="joinUser.045" text="이메일은 1~63자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다."/>');
	            MODAL_EMAIL.focus();
	            return false;
	        }
			if (document.getElementById('MODAL_EMAIL_DOMAIN').value == '') {
	            alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
	            MODAL_EMAIL_DOMAIN.focus();
	            return false;
	        }
			if (document.getElementById('MODAL_USR_NM').value == '') {
	            alert('<spring:message code="joinUser.010" text="이름을 입력하세요."/>');
	            MODAL_USR_NM.focus();
	            return false;
	        }
			if (!isNM.test(document.getElementById('MODAL_USR_NM').value)) {
	            alert('<spring:message code="joinUser.100" text="이름은 2~20자의 한글,영문만 사용 가능합니다."/>');
	            MODAL_USR_NM.focus();
	            return false;
	        }
			if(document.getElementById('MODAL_CNT_FRS').value == ''){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				MODAL_CNT_FRS.focus();
	            return false;
			}
			if(document.getElementById('MODAL_CNT_MDL').value == ''){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				MODAL_CNT_MDL.focus();
	            return false;
			}
			if(document.getElementById('MODAL_CNT_END').value == ''){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				MODAL_CNT_END.focus();
	            return false;
			}
			
			<%--
			
			if(check == true){
				if(document.getElementById('MODAL_OFFM_TELNO').value=='') {
					if(document.getElementById('MODAL_CNT_FRS').value == '' &&
					   document.getElementById('MODAL_CNT_MDL').value == '' &&
					   document.getElementById('MODAL_CNT_END').value == '') {
						alert('<spring:message code="joinUser.011" text="연락처와 사무실번호 중 최소 한 항목 이상 입력하세요."/>');
						MODEL_OFFM_TELNO.focus();
						check=false;
						return false;
					} else {
						if(document.getElementById('MODAL_CNT_FRS').value == ''){
							alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
							MODAL_CNT_FRS.focus();
				            return false;
						}
						if(document.getElementById('MODAL_CNT_MDL').value == ''){
							alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
							MODAL_CNT_MDL.focus();
				            return false;
						}
						if(document.getElementById('MODAL_CNT_END').value == ''){
							alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
							MODAL_CNT_END.focus();
				            return false;
						}
					}
				}
			}
			
			--%>
			
			if(check) {
				$("#jsGrid1").jsGrid("insertItem", newItem).done( function() {
					modalUserInfo.checkItem(newItem);
					$modal.modal('hide');
				});
			}
		},
		updateRow : function(){
			var isEmail = /^[a-z0-9][a-z0-9_\-]{0,63}$/;
			var isNM = /^[a-zA-Zㄱ-힣]{2,29}/;
			var $modal = $('#MODAL_USER');
			var check = true;
			modalUserInfo.email = emailMapping();
			var updateItem = {
				USR_NO : $modal.find("#MODAL_USR_NO").val(),
				USR_NM : $modal.find("#MODAL_USR_NM").val(),
				USR_EMAIL : modalUserInfo.email,
//				GP_CD : $modal.find("#MODAL_GP_CD").val(),
//				USR_RSPOFC : $modal.find("#MODAL_USR_RSPOFC").val(),
//				OFFM_TELNO : $modal.find("#MODAL_OFFM_TELNO").val(),
				CNT_FRS : $modal.find("#MODAL_CNT_FRS").val(),
				CNT_MDL : $modal.find("#MODAL_CNT_MDL").val(),
				CNT_END : $modal.find("#MODAL_CNT_END").val(),
				LOCK_YN : $modal.find("#MODAL_LOCK_YN").val(),
				APPROVAL_YN : $modal.find("#MODAL_APPROVAL_YN").val(),
				NEW : "N"
			};
			var isUpdateItem = {
				is_USR_NM : $modal.find("#MODAL_USR_NM").val(),
				is_USR_EMAIL : modalUserInfo.email,
			}
			for(var bi in isUpdateItem){
				if(isUpdateItem[bi] == "" || isUpdateItem[bi] == null) {
					check = false;
				}
			}
			//--------------------------
			if (document.getElementById('MODAL_EMAIL').value == '') {
	            alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
	            MODAL_EMAIL.focus();
	            return false;
	        }
			if (!isEmail.test(document.getElementById('MODAL_EMAIL').value)) {
	            alert('<spring:message code="joinUser.045" text="이메일은 1~63자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다."/>');
	            MODAL_EMAIL.focus();
	            return false;
	        }
			if (document.getElementById('MODAL_EMAIL_DOMAIN').value == '') {
	            alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
	            MODAL_EMAIL_DOMAIN.focus();
	            return false;
	        }
			if (document.getElementById('MODAL_USR_NM').value == '') {
	            alert('<spring:message code="joinUser.010" text="이름을 입력하세요."/>');
	            MODAL_USR_NM.focus();
	            return false;
	        }
			if (!isNM.test(document.getElementById('MODAL_USR_NM').value)) {
	            alert('<spring:message code="joinUser.100" text="이름은 2~20자의 한글,영문만 사용 가능합니다."/>');
	            MODAL_USR_NM.focus();
	            return false;
	        }
			if(document.getElementById('MODAL_CNT_FRS').value == ''){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				MODAL_CNT_FRS.focus();
	            return false;
			}
			if(document.getElementById('MODAL_CNT_MDL').value == ''){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				MODAL_CNT_MDL.focus();
	            return false;
			}
			if(document.getElementById('MODAL_CNT_END').value == ''){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				MODAL_CNT_END.focus();
	            return false;
			}
			
			<%--
			
			if(check == true) {
				if(document.getElementById('MODAL_OFFM_TELNO').value==''){
					if(document.getElementById('MODAL_CNT_FRS').value == '' &&
					   document.getElementById('MODAL_CNT_MDL').value == '' &&
					   document.getElementById('MODAL_CNT_END').value == ''){
						alert('<spring:message code="joinUser.011" text="연락처와 사무실번호 중 최소 한 항목 이상 입력하세요."/>');
						MODEL_OFFM_TELNO.focus();
						check=false;
						return false;
					}else{
						if(document.getElementById('MODAL_CNT_FRS').value == ''){
							alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
							MODAL_CNT_FRS.focus();
				            return false;
						}
						if(document.getElementById('MODAL_CNT_MDL').value == ''){
							alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
							MODAL_CNT_MDL.focus();
				            return false;
						}
						if(document.getElementById('MODAL_CNT_END').value == ''){
							alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
							MODAL_CNT_END.focus();
				            return false;
						}
					}
				}
			}
			
			--%>
			
			//--------------------------
			if(check) {
				$("#jsGrid1").jsGrid("insertItem", updateItem).done( function() {
					modalUserInfo.saveItem(updateItem);
				});
			}
		},
		checkItem : function(_saveItem){
			$.ajax({
				url: "/admin/checkUserID",
				type : "post",
				contentType: 'application/json',
				data : JSON.stringify({
					USER_DATA : _saveItem
				})
			}).done(function(response) {
				if(response.CHECK_USER != ""){
					alert('<spring:message code="userManagement.033" text="사용자 아이디가 중복 되었습니다." />' + " (" + '<spring:message code="userManagement.034" text="중복 아이디" />' + " : " + response.CHECK_USER + ")");
				} else {
					modalUserInfo.saveItem(_saveItem);
				}
			});
		},
		saveItem : function(_saveItem){
			$.ajax({
                url: "/admin/saveUser",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    USER_DATA : _saveItem
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다" />');
		            location.reload();
		        } else if(response.SUCCESS == "2"){
		            alert('<spring:message code="userManagement.052" text="새로운 계정이 생성되었습니다. 새로운 계정의 비밀번호는 아이디+1 입니다." />');
		            location.reload();
		        }
            });
		}
    }
    
    
    function emailMapping() {
		var _email = "";
		if ($("#MODAL_DOMAIN_SELECT").val() == 'self') {
			_email = $("#MODAL_EMAIL").val() + '@' + $("#MODAL_EMAIL_DOMAIN").val();
		} else {
			_email = $("#MODAL_EMAIL").val() + '@' + $("#MODAL_DOMAIN_SELECT").val();
		}
		return _email;
	}
	
	function emailSplit(_email) {
		<%-- 0 : 이메일ID, 1 : 이메일도메인 --%>
		var email = new Array();
		_email = _email.split("@");
		email.push(_email[0]);
		email.push(_email[1]);
		return email;
	}

	var roleUsrNo = "";
	var isNewRoleUsr = false;
	var selectedIndex = "";
	
	function openUserRolePopup(obj){
		
		roleUsrNo = $(obj).attr("usrno");
		
		selectedIndex = $(obj).attr("data-index");
		
		if($(obj).attr("tmpuser")){
			isNewRoleUsr = true;
		}else{
			isNewRoleUsr = false;
		}
		
		$("#ROLE_BODY").empty();

		var usrRoles = userRoleList[roleUsrNo];

		$("#ADD_ROLE_BTN").show();
        $("#TMP_ROLE_SELECT").removeAttr("disabled");

        // 리스트에 있는 시스템관리자 권한 제거
        if(exitRoleVal != null) {
            $("#TMP_ROLE_SELECT option[value='" + exitRoleVal + "']").remove();
            exitRoleVal = null;
        }

		if(usrRoles && usrRoles.length > 0){

			if($roleObject == null) {
				$roleObject = $("#TMP_ROLE_SELECT > option").map(function() {
	                 return $(this).val(); 
	            });
			}

			if($roleList == null) {
				$roleList = Object.values($roleObject);
			}
            
	    	for(var ri = 0; ri < usrRoles.length; ri++){
	    		
	    		if($roleList && $roleList.indexOf(usrRoles[ri]["ROLE_CD"]) == -1) {
	    			usrRoles[ri].DISABLED = true;
	    			$("#ADD_ROLE_BTN").hide();
                    $("#TMP_ROLE_SELECT").attr("disabled", "disabled");

                    exitRoleVal = usrRoles[ri]["ROLE_CD"];
	            } else {
	            	usrRoles[ri].DISABLED = false;
	                $("#ADD_ROLE_BTN").show();
                    $("#TMP_ROLE_SELECT").removeAttr("disabled");
			    }
	    	    
	    		addUserRole(usrRoles[ri]);
	    	}
		}else{
			addUserRole();
		}
		
		
		
	}
	
	var usrPopupInputIdx = 1;
	
	function addUserRole(roleObj) {
		usrPopupInputIdx++;

		if(roleObj && roleObj.DISABLED == true) {
			$("#TMP_ROLE_SELECT").append("<option value='" + roleObj.ROLE_CD + "' disabled>" + roleObj.ROLE_CD + "</option>")
		}
		
		var $roleTr = $('<tr id="TR_USR_ROLE_'+usrPopupInputIdx+'"><td><input class="usrRoleCheckbox" type="checkbox"  id="DEL_ROLE_CHECK_'+ usrPopupInputIdx +'" key="'+ usrPopupInputIdx +'"><label class="form-check-label" for="DEL_ROLE_CHECK_'+ usrPopupInputIdx +'"></label></td></tr>');
		var $roleSelect = $("#TMP_ROLE_SELECT").clone().attr("id", "ROLE_SELECT_" +usrPopupInputIdx);
		var $roleTd = $('<td></td>').append($roleSelect);
		$roleTr.append($roleTd);
		$("#ROLE_BODY").append($roleTr);
		
		if(roleObj) {
			$("#ROLE_SELECT_" +usrPopupInputIdx).val(roleObj.ROLE_CD);
		}

	}
	function removeUserRole(){
		
		$("#ROLE_BODY").find(".usrRoleCheckbox:checked").each(function(){
			var delKey = $(this).attr("key");
			$("#TR_USR_ROLE_"+ delKey).remove();
		});

	}

	function savePopupUsrRole(){
		var newUseRoleList = new Array();
		var gridRoleText = "";
		var gridRoleCheckText = "";
		var usrRoleCheck = true;

		$("#ROLE_BODY").find(".usrRoleCheckbox").each(function(index, item) {
			if(usrRoleCheck){
	
				var addKey = $(this).attr("key");
				newUseRoleList.push({
					ROLE_CD : $("#ROLE_SELECT_" +addKey).val()
				});
				
				if(gridRoleText != ""){
					gridRoleText += ", ";
				}
				
			    if( $roleList != null && ( ($("#ROLE_SELECT_" +addKey).val() && Number($roleList.indexOf($("#ROLE_SELECT_"+addKey).val()) == -1))
					|| (userRoleList[roleUsrNo][index] && Number($roleList.indexOf(userRoleList[roleUsrNo][index]["ROLE_CD"]) == -1)) ) ) {
					
			    	usrRoleCheck = false;
			    	alert('<spring:message code="userManagement.058" text="부여할 수 없는 권한입니다." />');
				};
			    
				if(gridRoleCheckText.indexOf($("#ROLE_SELECT_" +addKey + " option:checked").text()) > -1){
					usrRoleCheck = false;
					alert('<spring:message code="userManagement.035" text="중복된 정보가 존재합니다." />' + " [" + $("#ROLE_SELECT_" +addKey + " option:checked").text() +"]");
					$("#ROLE_SELECT_" +addKey).focus();
				}

				gridRoleText += $("#ROLE_SELECT_" +addKey + " option:checked").text();
				gridRoleCheckText += "," + $("#ROLE_SELECT_" +addKey + " option:checked").text() + ",";
			}
		});
		
		if(!usrRoleCheck){
			return;
		}
		
		if(!isNewRoleUsr){

			if(!confirm('<spring:message code="common.049" text="저장 하시겠습니까?" />')){
				return;
			}

			userRoleList[roleUsrNo] = newUseRoleList;
			$(".USR_ROLE_" + roleUsrNo).html(gridRoleText);
			
            $.ajax({
                url: "/admin/saveUserRole",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	 USR_NO    : roleUsrNo
                	,ROLE_LIST : newUseRoleList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
		            alert('<spring:message code="common.013" text="저장 되었습니다." />');
		            
		            $("#MODAL_ROLE_CLOSE_BTN").click();
		            $("#jsGrid1").jsGrid("option", "data")[selectedIndex].ROLE_LIST = newUseRoleList;
		            $("#MODAL_ROLE_TEXT").text(gridRoleText);
		        }
            });
            
		}else{
			
			userRoleList[roleUsrNo] = newUseRoleList;
			$(".USR_ROLE_" + roleUsrNo).html(gridRoleText);
			
            $("#MODAL_ROLE_CLOSE_BTN").click();
		}
		
	}
	
	var ipUsrNo = "";
	var isNewIPUsr = false;
	
	function openUserIPPopup(obj){
		ipUsrNo = $(obj).attr("usrno");
		selectedIndex = $(obj).attr("data-index");
		
		$("#IP_BODY").empty();
		
		var usrIPs = userIPList[ipUsrNo];
		
		if(usrIPs && usrIPs.length > 0){
	    	for(var ii = 0; ii < usrIPs.length; ii++){
	    		addUserIP(usrIPs[ii]);
	    	}
		}else{
    		addUserIP();
		}

	}
	
	function addUserIP(ipObj){
		usrPopupInputIdx++;

		var $ipTr = $('<tr id="TR_USR_IP_'+usrPopupInputIdx+'"><td><input class="usrIPCheckbox" type="checkbox"  id="DEL_IP_CHECK_'+ usrPopupInputIdx +'" key="'+ usrPopupInputIdx +'"><label class="form-check-label" for="DEL_IP_CHECK_'+ usrPopupInputIdx +'"></label></td></tr>');
		
		var $ipTypeSelect = $("#TMP_IP_TYPE_SELECT").clone().attr("id", "USR_IP_TYPE_SELECT_" +usrPopupInputIdx);
		var $ipTd1 = $('<td></td>').append($ipTypeSelect);
		var $ipTd2 = $('<td><input id="USR_IP_'+ usrPopupInputIdx +'" class="form-control addUsrIPVal" maxlength="20" ></td>');
		$ipTr.append($ipTd1);
		$ipTr.append($ipTd2);
		$("#IP_BODY").append($ipTr);
		
		if(ipObj){
			$("#USR_IP_TYPE_SELECT_" +usrPopupInputIdx).val(ipObj.IP_TYP);
			$("#USR_IP_" +usrPopupInputIdx).val(ipObj.IP);
		}
		
	}
	
	function removeUserIP(){
		
		$("#IP_BODY").find(".usrIPCheckbox:checked").each(function(){
			var delKey = $(this).attr("key");
			$("#TR_USR_IP_"+ delKey).remove();
		});
	}

	function savePopupUsrIP(){
		
		var newUseIPList = new Array();
		var gridIPText = "";
		var usrIPCheck = true;
		var bIPCnt = 0;
		var wIPCnt = 0;
		
		$("#IP_BODY").find(".usrIPCheckbox").each(function(){
			if(usrIPCheck){
				var addKey = $(this).attr("key");
				
				if($("#USR_IP_" +addKey).val() == ""){
					usrIPCheck = false;
					alert('<spring:message code="userManagement.036" text="IP 정보를 입력하세요." />');
					$("#USR_IP_" +addKey).focus();
				}

				newUseIPList.push({
					 IP_TYP : $("#USR_IP_TYPE_SELECT_" +addKey).val()
					,IP     : $("#USR_IP_" +addKey).val()
				});

				if($("#USR_IP_TYPE_SELECT_" +addKey).val() == "B"){
					bIPCnt++;
				}else{
					wIPCnt++;
				}
			}
		});
		
		if(!usrIPCheck){
			return false;
		}
		
        if(wIPCnt > 0){
        	gridIPText = wIPCnt + '<spring:message code="userManagement.009" text="개의 접근 가능 IP" />';
        }else if(bIPCnt > 0){
        	gridIPText = bIPCnt + '<spring:message code="userManagement.010" text="개의 접근 불가 IP" />';
        }
        
		if(!isNewIPUsr){

			if(!confirm('<spring:message code="common.049" text="저장 하시겠습니까?" />')){
				return;
			}

			userIPList[ipUsrNo] = newUseIPList;
			$(".USR_IP_" + ipUsrNo).html(gridIPText);
			
            $.ajax({
                url: "/admin/saveUserIP",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                	 USR_NO    : ipUsrNo
                	,IP_LIST   : newUseIPList
                })
            }).done(function(response) {
	            if(response.SUCCESS == "0"){
	            	$("#jsGrid1").jsGrid("option", "data")[selectedIndex].IP_LIST = newUseIPList;

		            alert('<spring:message code="common.013" text="저장 되었습니다." />');
		            $("#MODAL_IP_CLOSE_BTN").click();
		        }
            });
            
		}else{
			
			userIPList[ipUsrNo] = newUseIPList;
			$(".USR_IP_" + ipUsrNo).html(gridIPText);
			$("#MODAL_IP_TEXT").text(gridIPText);
            $("#MODAL_IP_CLOSE_BTN").click();
		}
	}
	
	function loadUserList() {
		
		$.ajax({
			url: "/admin/getUserList",
			type : "post",
			contentType: 'application/json',
			data : JSON.stringify({
				PAGE_INDEX : pageIndex,
				PAGE_SIZE : pageSize
			})

			}).done(function(response) {
				isSearch = false;
				da = {
					data : response.USER_LIST,
					itemsCount :response.COUNT
				};
			$("#jsGrid1").jsGrid("loadData");
		});

	}
	
	function pwReset(obj) {
		if (confirm('<spring:message code="userManagement.050" text="정말 비밀번호를 초기화하시겠습니까?" />')) {
			var usrno = $(obj).attr("usrno");
		
			$.ajax({
			url: "/admin/userPWReset",
			type : "post",
			contentType: 'application/json',
			data : JSON.stringify({
				USR_NO : usrno,
			})

			}).done(function(response) {
				alert(response.USR_ID + '<spring:message code="userManagement.057" text=" 님의 비밀번호가 초기화됐습니다." />'
					 +"\n"+'<spring:message code="userManagement.051" text="변경된 비밀번호" />' + ' : ' + response.USR_PW);
			});
		
		}
		else {
		}
	
	
	}
	
	function usrDelete(obj) {
		if (confirm('<spring:message code="common.032" text="삭제 하시겠습니까?" />')) {
			var usrno = $(obj).attr("usrno");
			
			$.ajax({
			url: "/admin/userDelete",
			type : "post",
			contentType: 'application/json',
			data : JSON.stringify({
				USR_NO : usrno,
			})

			}).done(function(response) {
				alert('삭제되었습니다');
				$("#SEARCH_GROUP_BTN").click();
			});
		
		}
		else {
		}
	
	}
	
	
	function searchFieldListInit(){
		$('#SEARCH_CATEGORY').empty();
		var jsGridFields = $("#jsGrid1").data("JSGrid").fields;
		for(var ji = 0; ji < jsGridFields.length; ji++) {
			if(jsGridFields[ji].isSearch == "Y") {
				if(jsGridFields[ji].isSelect == "Y") {
					var selectedMap = _selectListMap[jsGridFields[ji].name]
					var tempTxt = '<option value="">'+'<spring:message code="userManagement.054" text="전체" />'+'</option>';
					for(var si = 0; si < selectedMap.length; si++) {
						tempTxt += '<option value="' + selectedMap[si].Id + '">' + selectedMap[si].Name + '</option>'
					}
					$("#SEARCH_SELECT_"+jsGridFields[ji].name).html(tempTxt);
				} else {
					$('#SEARCH_CATEGORY').append(
						 '<option value="' + jsGridFields[ji].name + '">' + jsGridFields[ji].title + '</option>'
					);
				}
				
			}
		}

	}
	
	function enterSearch(){
		if($("#SEARCH_CATEGORY").val().length < 1) {
			alert('<spring:message code="userManagement.055" text="올바른 카테고리를 선택해주세요." />');
			return;
		}
		
		if($("#SEARCH_INPUT").val().length < 1 && !$("#SEARCH_SELECT_GP_CD").val() && !$("#SEARCH_SELECT_USR_RSPOFC").val() && !$("#SEARCH_SELECT_ROLE_LIST").val()) {
			loadUserList();
			return;
		}

		<%-- 페이지 인덱스가 1이 아닐 경우 1로 초기화 후 실행 --%>
		if($("#jsGrid1").jsGrid("option", "pageIndex") != 1){
			isInit = true;
			$("#jsGrid1").jsGrid("option", "pageIndex", 1);
		}
		
		
		_searchedValue["CATEGORY"] = $("#SEARCH_CATEGORY").val();
		_searchedValue["INPUT"] = $("#SEARCH_INPUT").val();
		_searchedValue["GP_CD"] = $("#SEARCH_SELECT_GP_CD").val();
		_searchedValue["USR_RSPOFC"] = $("#SEARCH_SELECT_USR_RSPOFC").val();
		_searchedValue["ROLE_LIST"] = $("#SEARCH_SELECT_ROLE_LIST").val();
		searchUserList();
		
	}
	
	function searchUserList() {
		$.ajax({
			url: "/admin/getSearchUserList",
			type : "post",
			contentType: 'application/json',
			data : JSON.stringify({
				PAGE_INDEX : pageIndex,
				PAGE_SIZE : pageSize,
				SEARCH_CATEGORY : _searchedValue["CATEGORY"],
				SEARCH_INPUT : _searchedValue["INPUT"],
				SEARCH_GP : _searchedValue["GP_CD"],
				SEARCH_RSPOFC : _searchedValue["USR_RSPOFC"],
				SEARCH_ROLE : _searchedValue["ROLE_LIST"]
			})
			
		}).done(function(response) {
			isSearch = true;
			da = {
				data : response.USER_LIST,
				itemsCount :response.COUNT
			};
			$("#jsGrid1").jsGrid("loadData");
		});
	}
	
	</script>

<section>

	<div class="col-xs-12 btn-area">
		<button type="submit" id="SEARCH_GROUP_BTN" class="btn btn-dark" onclick="enterSearch();">
			<spring:message code="common.017" text="조회" />
		</button>
		&nbsp;
		<button id="ADD_USR" type="button" class="btn btn-dark" onclick="modalUserInfo.showAddPopup();">
			<spring:message code="common.015" text="신규" />
		</button>
	</div>

	<div class="x_panel">
		<div class="col-xs-12">
			<div class="x_title">
				<h2>
					<spring:message code="userManagement.056" text="사용자검색" />
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="x_content">
			<div class="col-xs-6">
				<div class="input-group">
					<select id="SEARCH_CATEGORY" style="width:20%" name="SEARCH_CATEGORY" class="form-control custom-select"></select>
					<input id="SEARCH_INPUT" class="form-control" style="width:65%; height:auto;" type="search" placeholder="검색어를 입력해주세요.">
				</div>
			</div>
			
			<%-- 
			
			<div class="col-xs-6">
				<div class="input-group">
					<span class="select-span">
						<spring:message code="userManagement.005" text="사용자부서" /> : 
					</span>
					<select id="SEARCH_SELECT_GP_CD" class="form-control" style="width:70%;">
						<option value=""><spring:message code="userManagement.054" text="전체" /></option>
					</select>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="input-group">
					<span class="select-span">
						<spring:message code="userManagement.053" text="사용자직책" /> : 
					</span>
					<select id="SEARCH_SELECT_USR_RSPOFC" class="form-control" style="width:70%;">
						<option value=""><spring:message code="userManagement.054" text="전체" /></option>
					</select>
				</div>
			</div>
			
			--%>
			
			<div class="col-xs-6">
				<div class="input-group">
					<span class="select-span">
						<spring:message code="userManagement.012" text="사용자권한" /> : 
					</span>
					<select id="SEARCH_SELECT_ROLE_LIST" class="form-control" style="width:70%;">
						<option value=""><spring:message code="userManagement.054" text="전체" /></option>
					</select>
				</div>
			</div>
		</div>
	</div>
	
	<div class="x_panel x_result">
		<div class="row">
			<div id="jsGrid1" class="jsgrid" style="position: relative; height: 100%; width: 100%; text-align: center;"></div>
		</div>
	</div>
	
	<%--
	
	<div class="x_panel">
		<div class="row">
			<div class="col-xs-12">
				<div class="x_title">
					<h2>
						<spring:message code="userManagement.037" text="사용자 업로드" />
					</h2>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-light">	
			<div class="col-xs-12 btn-area">
				<button type="button" class="btn btn-primary" onclick="window.open('../uploadsample/USER.xls');">
					<spring:message code="common.019" text="양식 다운로드" />
				</button>
				&nbsp;
				<button id="USR_FILE_UPLOAD_BTN" type="button"
					class="btn btn-dark">
					<spring:message code="common.020" text="업로드" />
				</button>
			</div>
			<div class="col-xs-12">
				<div id="EXE_FILE_DIV" class="card-body"></div>
			</div>
		</nav>
	</div>
	
	--%>
	
</section>


<div id="MODAL_ROLE" class="modal fade" style="display: none; z-index:3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="userManagement.038" text="사용자권한 설정" />
				</h4>
			</div>

			<div class="modal-body">

				<div class="col-xs-12 btn-area">
					<button type="button" id="ADD_ROLE_BTN" class="btn btn-dark" onclick="addUserRole();">
						<spring:message code="userManagement.039" text="추가" />
					</button>
					&nbsp;
					<button type="button" class="btn btn-dark" onclick="removeUserRole();">
						<spring:message code="userManagement.040" text="삭제" />
					</button>
				</div>
				<br /> <br />
				<div class="table-wrapper-scroll-y my-custom-scrollbar">
					<table class="table table-hover text-nowrap">
						<tbody id="ROLE_BODY">
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer justify-content-between">
				<button id="MODAL_ROLE_CLOSE_BTN" type="button" class="btn btn-default" data-dismiss="modal">
					<spring:message code="userManagement.041" text="닫기" />
				</button>
				<button type="button" class="btn btn-primary" onclick="savePopupUsrRole();">
					<spring:message code="common.047" text="저장" />
				</button>
			</div>
			<div style="display: none;">
				<select id="TMP_ROLE_SELECT" class="form-control custom-select addUsrRole">
					<c:forEach items="${ROLE_LIST}" var="role" varStatus="status">
					   <c:choose>
					       <c:when test='${role.ROLE_CD ne "ROLE_SYS_ADMIN"}'>
                                <option value="${role.ROLE_CD}">${role.ROLE_NM}</option>
                           </c:when>
					   </c:choose>
						
					</c:forEach>
				</select>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div id="MODAL_IP" class="modal fade" style="display: none; z-index:3" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title">
					<spring:message code="userManagement.042" text="사용자IP 설정" />
				</h4>
			</div>

			<div class="modal-body">
				<div class="col-xs-12 btn-area">
					<button type="button" class="btn btn-dark" onclick="addUserIP();">
						<spring:message code="userManagement.039" text="추가" />
					</button>
					&nbsp;
					<button type="button" class="btn btn-dark" onclick="removeUserIP();">
						<spring:message code="userManagement.040" text="삭제" />
					</button>
				</div>
				<br /> <br />
				<div class="table-wrapper-scroll-y my-custom-scrollbar">
					<table class="table table-hover text-nowrap">
						<tbody id="IP_BODY">
						</tbody>
					</table>
				</div>
			</div>
			<div class="modal-footer justify-content-between">
				<button id="MODAL_IP_CLOSE_BTN" type="button" class="btn btn-default" data-dismiss="modal">
					<spring:message code="userManagement.041" text="닫기" />
				</button>
				<button type="button" class="btn btn-primary" data-index="" onclick="savePopupUsrIP();">
					<spring:message code="common.047" text="저장" />
				</button>
			</div>
			<div style="display: none;">
				<select id="TMP_IP_TYPE_SELECT"
					class="form-control custom-select addUsrIPType">
					<option value="B"><spring:message code="userManagement.043" text="IP 제외" /></option>
					<option value="W"><spring:message code="userManagement.044" text="IP 허용" /></option>
				</select>
			</div>
		</div>

		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

<div id="MODAL_USER" class="modal fade" role="dialog" style="z-index:2"> 
	<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">	          
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">×</span>
                </button>      
                <h4 class="modal-title"><spring:message code="userManagement.060" text="신규 사용자 생성" /></h4>
            </div>
	        <div class="modal-body">
	        	<div class="scroll_h" style="height:auto;">
		        	<form class="form-horizontal" id="quickform" name="modal_user_form">
		        		<input type="hidden" id="MODAL_USR_NO" name="USR_NO" data-type="USR_NO"/>
						<div class="card-body">
							<div class="form-group row">
								<label for="id" class="col-sm-2 col-form-label"><spring:message code="joinUser.002" text="아이디" /> (*)</label>
								<div class="col-sm-10">
									<input type="text" class="form-control modify_only_disabled" id="MODAL_USR_ID" name="USR_ID" data-type="USR_ID" maxlength='20'
										placeholder="<spring:message code="joinUser.002" text="아이디"/>">
								</div>
							</div>
							<div class="form-group row" style="margin-top: 10px;">
								<label for="inputEmail" class="col-sm-2 col-form-label"><spring:message
										code="joinUser.003" text="이메일" /> (*)</label>
								<div class="col-sm-3" style="display:flex; padding-right:0px;">
									<input type="text" class="form-control" id="MODAL_EMAIL" name="USR_EMAIL" maxlength=100 style="padding-top: 4px; width:90%;">
									<div><span>@</span></div>
								</div>
								<div class="col-sm-3" style="padding-left:5px;">
									<input type="text" class="form-control"
										id="MODAL_EMAIL_DOMAIN" style="padding-top: 4px;" maxlength=100>
								</div>
								<div class="col-sm-3" >
									<select id="MODAL_DOMAIN_SELECT" class="form-control" style="padding-top: 4px;">
											<option value='self'><spring:message code="joinUser.004" text="직접입력" /></option>
											<c:forEach var="domainList" items='${DOMAIN_LIST}'>
												<option value='${domainList.CD_NM}'>${domainList.CD_NM}</option>
											</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="name" class="col-sm-2 col-form-label"><spring:message
										code="joinUser.005" text="이름" /> (*)</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="MODAL_USR_NM" name="USR_NM" data-type="USR_NM" maxlength='30'
										placeholder="<spring:message code="joinUser.005" text="이름"/>" >
								</div>
							</div>
							<div class="form-group row">
								<label for="phonenumber" class="col-sm-2 col-form-label"><spring:message code="joinUser.006" text="연락처" /></label>
								<div class="col-sm-2">
									<input type="text" class="form-control" placeholder="" id="MODAL_CNT_FRS" data-type="CNT_FRS" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
								</div>
								<div class="col-sm-1"  style="width:5px;text-align:center;margin-top:5px;">-</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" placeholder="" id="MODAL_CNT_MDL" data-type="CNT_MDL" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
								</div>
								<div class="col-sm-1" style="width:5px;text-align:center;margin-top:5px;">-</div>
								<div class="col-sm-2">
									<input type="text" class="form-control" placeholder="" id="MODAL_CNT_END" data-type="CNT_END" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
								</div>
							</div>
							
							<%-- 
							
							<div id="groupDiv" class="form-group row">
								<label class="col-sm-2 col-form-label"><spring:message code="joinUser.007" text="부서" /> (*)</label>
								<div class="col-sm-10">
									<select	class="form-control" id="MODAL_GP_CD" data-type="GP_CD" style="width: 100%;" tabindex="-1" aria-hidden="true" name="GP_CD">
										<c:forEach var="groupList" items='${GROUP_LIST}'>
											<option value='${groupList.GP_CD}'>${groupList.GP_NM}</option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-2 col-form-label"><spring:message code="joinUser.025" text="직책" /> (*)</label>
								<div class="col-sm-10">
								<select	class="form-control" id="MODAL_USR_RSPOFC" data-type="USR_RSPOFC" style="width: 100%;" tabindex="-1" aria-hidden="true" name="USR_RSPOFC">
									<c:forEach var="rspofcList" items='${RSPOFC_LIST}'>
										<option value='${rspofcList.CD}'>${rspofcList.CD_NM}</option>
									</c:forEach>
								</select>
								</div>
							</div>
							<div class="form-group row">
								<label for="office_telno" class="col-sm-2 col-form-label"><spring:message
											code="joinUser.012" text="사무실번호" /></label>
								<div class="col-sm-10" style="padding-top: 4px;">
									<input type="text" class="form-control" placeholder="" id="MODAL_OFFM_TELNO" data-type="OFFM_TELNO" name="OFFM_TELNO" maxlength="50" style="padding-top: 4px;">
								</div>
							</div>
							
							--%>
							
							<div class="modify_only_none">
								<div class="form-group row">
									<label for="LOCK_YN" class="col-sm-2 col-form-label"><spring:message code="userManagement.017" text="잠금여부" /></label>
									<div class="col-sm-10">
										<select	class="form-control select2 select2-hidden-accessible" id="MODAL_LOCK_YN" data-type="LOCK_YN" style="width: 100%;" tabindex="-1" aria-hidden="true" name="LOCK_YN">
											<option value='Y'>잠금</option>
											<option value='N'>미잠금</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="APPROVAL_YN" class="col-sm-2 col-form-label"><spring:message code="userManagement.20" text="승인여부" /></label>
									<div class="col-sm-10">
										<select	class="form-control select2 select2-hidden-accessible" id="MODAL_APPROVAL_YN" data-type="APPROVAL_YN" style="width: 100%;" tabindex="-1" aria-hidden="true" name="APPROVAL_YN">
											<option value='Y'>승인</option>
											<option value='N'>미승인</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label for="ROLE_LIST" class="col-sm-2 col-form-label"><spring:message code="userManagement.61" text="권한목록" />
										<br>
										<button type="button" id="BTN_ROLE_UPDATE" class="btn btn-dark" data-index="" data-toggle="modal" data-target="#MODAL_ROLE" usrno="" onclick="openUserRolePopup(this);"><spring:message code="userManagement.007" text="권한수정" /></button>
									</label>
									<div class="col-sm-8">
										<p id="MODAL_ROLE_TEXT"></p>
									</div>
								</div>
								
								<%--
								
								<div class="form-group row">
									<label for="IP_LIST" class="col-sm-2 col-form-label"><spring:message code="userManagement.62" text="IP제어목록" />
										<br>
										<button type="button" id="BTN_IP_UPDATE" class="btn btn-dark" data-index="" data-toggle="modal" data-target="#MODAL_IP" usrno="" onclick="openUserIPPopup(this);"><spring:message code="userManagement.011" text="IP수정" /></button>
									</label>
									<div class="col-sm-10">
										<p id="MODAL_IP_TEXT"></p>
									</div>
								</div>
								
								--%>
								
							</div>
						</div>
					</form>
				</div>
	        </div>
	        <div class="modal-footer">
            	<small class="float-right">
            		<small class="float-right">
	        			<button id="MODAL_BINDINFO_CLOSE_BTN" type="button" class="btn btn-default" data-dismiss="modal" ><spring:message code="common.076" text="닫기" /></button>
	        		</small>
            		&nbsp;
	            	<small class="float-right d-none ml-2" id="MODAL_UPDATE_BTN">                   
						<button type="button" class="btn btn-dark" onclick="modalUserInfo.updateRow()">
							<spring:message code="common.068" text="수정" />
						</button>
					</small>                   
					<small class="float-right" id="MODAL_CONFIRM_BTN">                   
						<button type="button" class="btn btn-dark" onclick="modalUserInfo.addRow()">
							<spring:message code="common.089" text="등록" />
						</button>
					</small>
			 	</small>
		 	</div>
        </div>
	</div>
</div>

