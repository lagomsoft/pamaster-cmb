<%@page import="org.hibernate.internal.build.AllowSysOut"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<style type="text/css">
	._selected_row > td
	{
		background-color : #c4e2ff;
	}
	._ellipsis {
	      overflow:hidden;
	      text-overflow:ellipsis;
	      white-space:nowrap;
	}
    tr.highlight td.jsgrid-cell {
        background-color: #238192;
        color : white;
    }
</style>
<script src="/js/PAMsProjectDetail.js"></script>
<script type="text/javascript">

    var deleteProjectList = new Array();
    deleteProjectList["COUNT"] = 0;

    var _gridRenderer = {
        cellRenderer : function(value,item){
            var rtn = $("<td>",{
                "text" :value,
                "title" :value
            })
            return rtn;
        }
    }

    <%--초기 호출 메소드--%>
    $(function() {
    
        <%--serialize form data를 json타입으로 변환--%>
        jQuery.fn.serializeObject = function() {
            var obj = null;
            try {
                if (this[0].tagName && this[0].tagName.toUpperCase() == "FORM") {
                    var arr = this.serializeArray();
                    if (arr) {
                        obj = {};
                        jQuery.each(arr, function() {
                            obj[this.name] = this.value;
                        });
                    }
                }
            } catch (e) {
                alert(e.message);
            } finally {
            }
         
            return obj;
        };
        <%-- 프로젝트 정보 획득--%>
        projectList.init();
    });


    <%--프로젝트 현황 획득(ajax)--%>
    var projectList = {
        settings : {
            autoload : false,
            width : "100%",
            height : "400px",
            sorting: true ,
            //sorting : true, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args) { // 더블클릭이벤트
                if(!args.event.target.firstElementChild && !args.event.target.hasAttribute('checkIcon')) {
                    var selectedRow = $("#totalProjectGrid").find('table tr.highlight');
                    selectedRow.removeClass('highlight');

                    <%--현재 선택 row highlight--%>
                    var $row = this.rowByItem(args.item);
                    $row.toggleClass("highlight");

                    var getData = args.item;

                    carryoutList.init(getData.PRJCT_NO);
                }
            },
            controller : {
                loadData : {}
            },
            fields : [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
            {
                name: "No",
                title : '<spring:message code="common.029" text="삭제" />',
                width: 5,
                align: 'center',
                itemTemplate: function (_, item) {
                    if(String(item.PRJCT_NO) == ""){
                        return "";
                    }
                    
                    var checkBoxStatus = "";
                    
                    if(item.DLT_YN == 'Y'){
                    	checkBoxStatus = "disabled";
                    }
                    
                    var $deleteCheckBox = $('<input type="checkbox" class="form-check-input" checkIcon="icon" id="DELETE_CHECKBOX_'+item.PRJCT_NO+'" ' + checkBoxStatus + '>').on("change", function(){

                        if($(this).is(":checked")){
                            deleteProjectList[item.PRJCT_NO] = 1;
                            deleteProjectList["COUNT"] = deleteProjectList["COUNT"] + 1;
                        }else{
                            delete deleteProjectList[item.PRJCT_NO];
                            deleteProjectList["COUNT"] = deleteProjectList["COUNT"] - 1;
                        }

                    });
                    var $deleteCheckBoxLabel = $('<label class="form-check-label" for="DELETE_CHECKBOX_'+item.PRJCT_NO+'"></label>');

                    return $('<div class="form-check">').append($deleteCheckBox).append($deleteCheckBoxLabel);
                }
            }, {
                name : "PRJCT_NM",
                title : "<spring:message code='myInfo.027' text='프로젝트명' />",
                type : "text",
                width : 50
            }, {
                name : "STEP_NM",
                title : "<spring:message code='myInfo.025' text='처리단계' />",
                type : "text",
                width : 50
            }, {
                name : "MDF_DT",
                title : "<spring:message code='myInfo.026' text='처리일자' />",
                type : "text",
                width : 50
            }, {
                name : "DLT_YN",
                title : "<spring:message code='myInfo.042' text='파기여부' />",
                type : "text",
                width : 25,
                align: 'center',
                itemTemplate: function (_, item) {
                    if(item.DLT_YN == 'N'){
                        return $("<div>").append("<spring:message code='myInfo.043' text='미파기' />");
                    }else if(item.DLT_YN == 'S'){
                        return $("<div>").append("<spring:message code='myInfo.045' text='파기예정' />");
                    }else {
                        return $("<div>").append("<spring:message code='myInfo.044' text='파기' />");
                    }
                }
            }, {
                name : "PRJCT_NO",
                type : "text",
                css : "d-none",
                width : 0
            } ]
        }
        ,init : function(){
            var totalSettings = $.extend({},projectList.settings);
            totalSettings.autoload = true;
            totalSettings.controller.loadData = function() {
                var d = $.Deferred();
                $.ajax({
                    url : "/user/myInfo",
                    isHideOverlay : true,
                    type : "post",
                    contentType : 'application/json',
                    data : JSON.stringify({
                        viewCond : "ALL"
                    })

                }).done(function(response) {
                    d.resolve(response.PROJECT_LIST);
                });

                return d.promise();
            }
            totalSettings.onDataLoaded = function(){
                var gridData =  JSON.parse(JSON.stringify($("#totalProjectGrid").jsGrid("option", "data")));
            }
            $("#totalProjectGrid").jsGrid(totalSettings);
	    }
        ,deleteProject : function(){
            if(deleteProjectList["COUNT"] === 0){
                alert('<spring:message code="myInfo.003" text="파기할 프로젝트를 선택하여 주십시오." />');
                return;
            }
            var deleteDataList = new Array();
            var di = 0;
            for (var key in deleteProjectList) {
                if(key != "COUNT"){
                    deleteDataList[di] = key;
                    di++;
                }
            }
            $.ajax({
                url: "/user/deleteProject",
                type : "post",
                contentType: 'application/json',
                data : JSON.stringify({
                    DATA_LIST : deleteDataList
                })
            }).done(function(response) {
            	
                if(response.SUCCESS == "0"){
                    alert('<spring:message code="common.033" text="처리 되었습니다." />' );
                    $("#totalProjectGrid").jsGrid("loadData");
                } else if (response.SUCCESS == "2") {
                    alert("<spring:message code='myInfo.068' text='선택된 프로젝트가 없습니다.' />");
                } else if (response.SUCCESS == "3") {
                	alert("<spring:message code='myInfo.069' text='선택된 프로젝트 중 이미 파기된 프로젝트가 있습니다.' />");
                }
            });

        }
	    ,showDetail : function(projectNo,stepCd,projectTy){
            <%--상세정보 팝업--%>
            _detailPopup.showPopup(projectNo,stepCd,projectTy);
	    }
    }

    <%--반출 현황 획득(ajax)--%>
    var carryoutList = {
        settings : {
            autoload : false,
            width : "100%",
            height : "400px",
            //sorting : true, // 칼럼의 헤더를 눌렀을 때, 그 헤더를 통한 정렬
            rowClick : function(args) { // 더블클릭이벤트
                $("#_detailPopup").find("[data-nm]").text("");
                for(idx in args.item){
                    $("#_detailPopup").find("[data-nm='"+idx+"']").text(args.item[idx]);
                }
                $("#_detailPopup").modal('show');
            },
            controller : {
                loadData : {}
            },
            fields : [ // 그리드 헤더 부분에 넣기 위해서 필드 지정
                {
                    name : "CARRYOUT_DE",
                    title : "<spring:message code='myInfo.006' text='제공일자' />",
                    type : "text",
                    width : 50
                }, {
                    name : "PRJCT_NM",
                    title : "<spring:message code='myInfo.027' text='프로젝트명' />",
                    type : "text",
                    width : 100,
                    cellRenderer : _gridRenderer.cellRenderer,
                    css : "_ellipsis"
                }, {
                    name : "PRIVACY_DTL",
                    title : "<spring:message code='myInfo.028' text='개인정보항목' />",
                    type : "text",
                    width : 100,
                    cellRenderer : _gridRenderer.cellRenderer,
                    css : "_ellipsis"
                }, {
                    name : "USE_DTL",
                    title : "<spring:message code='myInfo.029' text='이용내역' />",
                    type : "text",
                    width : 100,
                    cellRenderer : _gridRenderer.cellRenderer,
                    css : "_ellipsis"
                }, {
                    name : "CUSTOMER_DTL",
                    title : "<spring:message code='myInfo.030' text='제공받는자' />",
                    type : "text",
                    width : 100,
                    cellRenderer : _gridRenderer.cellRenderer,
                    css : "_ellipsis"
                }, {
                    name : "HOW_DTL",
                    title : "<spring:message code='myInfo.031' text='제공방법' />",
                    type : "text",
                    width : 100,
                    cellRenderer : _gridRenderer.cellRenderer,
                    css : "_ellipsis"
                }, {
                    name : "CARRYOUT_DTL",
                    title : "<spring:message code='myInfo.032' text='반출사유' />",
                    type : "text",
                    width : 100,
                    cellRenderer : _gridRenderer.cellRenderer,
                    css : "_ellipsis"
                } ]
        }
        ,init : function(_projectNo){
            var totalSettings = $.extend({},carryoutList.settings);
            totalSettings.autoload = true;
            totalSettings.controller.loadData = function() {
                var d = $.Deferred();
                $.ajax({
                    url : "/user/getCarryoutInfoList",
                    isHideOverlay : true,
                    type : "post",
                    contentType : 'application/json',
                    data : JSON.stringify({
                        PRJCT_NO : _projectNo
                    })

                }).done(function(response) {
                    d.resolve(response);
                });

                return d.promise();
            }
            totalSettings.onDataLoaded = function(){
                var gridData =  JSON.parse(JSON.stringify($("#carryoutGrid").jsGrid("option", "data")));
            }
            $("#carryoutGrid").jsGrid(totalSettings);
        }
    }


</script>

<section>
    <div class="row">
        <div class="col-xs-12 btn-area">
            <button type="button" class="btn btn-dark" auth-role="200"
                onclick='location.href="../advancePreparation/conformity?IS_NEW=Y"'>
                <spring:message code="myInfo.018" text="프로젝트 생성" />
            </button>
        </div>
        <div class="col-md-12">
            <div class="x_panel x_result">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="x_title">
                            <h2>
                                <spring:message code="myInfo.014" text="프로젝트 현황" />
                                <%-- <span style="font-size:0.8em; font-weight: normal;"><spring:message code="myInfo.062" text="(한 달 이내 프로젝트)" /></span> --%>
                            </h2>
                            <button type="button" class="btn btn-danger float_r" onclick="projectList.deleteProject();">
                            <spring:message code="myInfo.063" text="프로젝트 파기" />
                                
                            </button>
                            <br><br>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p class="sub_info">
                            진행종료된 프로젝트의 파기를 신청하거나 프로젝트를 클릭하여 해당하는 프로젝트의 반출내역을 확인할 수 있습니다.
                        </p>
                    </div>
                </div>
                <div class="x_content">
                    <div id="totalProjectGrid" class="jsgrid"></div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="x_panel x_result">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="x_title">
                            <h2>
                                <spring:message code="myInfo.001" text="프로젝트 반출내역" />
                            </h2>
                            <br><br>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p class="sub_info">
                        <spring:message code="myInfo.002" text="위에서 선택한 프로젝트의 반출 내역을 확인할 수 있습니다." />
                        </p>
                    </div>
                </div>
                <div class="x_content">
                    <div id="carryoutGrid" class="jsgrid"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal" id="_detailPopup" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><spring:message code="myInfo.004" text="상세정보" /></h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table">
                        <colgroup>
                            <col width="20%">
                            <col width="80%">
                        </colgroup>
                        <tbody>
                            <tr>
                                <th><spring:message code="myInfo.006" text="제공일자" /> :</th>
                                <td data-nm="CARRYOUT_DE"></td>
                            </tr>
                            <tr>
                                <th><spring:message code="myInfo.027" text="프로젝트명" /> :</th>
                                <td data-nm="PRJCT_NM"></td>
                            </tr>
                            <tr>
                                <th colspan="2"><spring:message code="myInfo.005" text="가명처리한 개인정보의 항목" /> :</th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea data-nm="PRIVACY_DTL" class="form-control" rows="3" disabled=""></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2"><spring:message code="myInfo.007" text="가명정보의 이용내역" /> :</th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea data-nm="USE_DTL" class="form-control" rows="3" disabled=""></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2"><spring:message code="myInfo.008" text="제3자 제공시 제공받는자" /> :</th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea data-nm="CUSTOMER_DTL" class="form-control" rows="3" disabled=""></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2"><spring:message code="myInfo.009" text="가명정보 제공방법" /> :</th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea data-nm="HOW_DTL" class="form-control" rows="3" disabled=""></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th colspan="2"><spring:message code="myInfo.032" text="반출사유" /> :</th>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <textarea data-nm="CARRYOUT_DTL" class="form-control" rows="3" disabled=""></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>