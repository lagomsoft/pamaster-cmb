<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="loginError.002" text="로그인 실패" /></title>
<script>
alert('<spring:message code="loginError.001" text="해당 IP는 허용되지 않습니다." />');
location.href = "/user/logout";
</script>
</head>
<body>

</body>
</html>