<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head> 
<meta charset="utf-8">
<title><spring:message code="joinUser.001" text="회원가입" /></title>
<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.js"></script>
<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>

<style type="text/css">

input.password { -webkit-text-security:disc; }

.modal {
        text-align: center;
}
 
@media screen and (min-width: 768px) { 
        .modal:before {
                display: inline-block;
                vertical-align: middle;
                content: " ";
                height: 100%;
        }
}
 
.modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
}
  
</style>
</head>
<script type="text/javascript">

    window._isNotsessionCheck = true;
    
    var passwd = "";
    var email = "";
    var checkMailUse = ${CHECK_MAIL_USE};
    var checkCorpUse = ${CHECK_CORP_USE};
    var groupList = [
        <c:forEach items="${GROUP_LIST}" var="group" varStatus="status">
            <c:if test="${!status.first}">,</c:if>  {Id : "${group.GP_CD}" , Name : "${group.GP_NM}"}
        </c:forEach>
    ];
    
    var pwCheckLvl = null;
    var pwMaxSize = null;
    var pwMinSize = null;
    
    $(function() {
        getPwPolicy();
        
        if (checkMailUse) {
            $('#pwdiv').hide();
        }
        if (checkCorpUse) {
             corpUse.corpInit();
        }
    
        $('#JOINUSER_CANCLE').click(function(e) {
            window.history.back();
        });
    
        $("#JOINUSER_ADD").on("click",function() {
        
            var _check = validCheck();
            
            if(_check) {
                email = emailMapping();
                    
                $.ajax({
                url : "/user/checkUserIDandEmail",
                type : "post",
                contentType : 'application/json',
                data : JSON.stringify({
                    USR_ID : $("#id").val(),
                    USR_EMAIL : email
                })
                
                }).done(function(response) {
                    if(response) {
                        sendPasswd();
                    } else {
                        alert('<spring:message code="joinUser.015" text="아이디 혹은 이메일주소가 이미 가입되어 있습니다."/>');
                    }
                    
                });
                
            }
            
        });
        
        $("#domain_select").change(function() {
            if($(this).val() != "self") {
                $("#email_domain").attr("disabled", true);
            } else {
                $("#email_domain").attr("disabled", false);
            }
        });
        
        
    });
    
    function sendPasswd() {
        $.ajax({
            url : "/user/mail",
            type : "post",
            contentType : 'application/json',
            data : JSON.stringify({
                USR_EMAIL : email
            })
    
        }).done(function(response) {
            if(checkMailUse) {
                passwd = response;
            } else {
                passwd = $("#pw").val();
            }
            createUser();
        });
    }
    
    function createUser(){
        var gp_company_nm = null;
        var gp_new_nm = null;
        if(checkCorpUse){
            gp_company_nm = $("#corp_company option:selected").text();
            gp_new_nm = $("#corp_group option:selected").text();
        }

        $.ajax({
            url : "/user/joinUser",
            type : "post",
            contentType : 'application/json',
            data : JSON.stringify({
                usr_id  : $("#id").val(), 
                usr_email : email,
                usr_pw : passwd,
                usr_nm : $("#usr_nm").val(),
                cnt_frs  : $("#cnt_frs").val(),
                cnt_mdl : $("#cnt_mdl").val(),
                cnt_end : $("#cnt_end").val(),
                //gp_cd  : $("#gp_cd").val(),
                //offm_telno : $("#offm_telno").val(),
                //usr_rspofc : $("#usr_rspofc").val(),
                //gp_company_nm : gp_company_nm,
                //gp_new_nm : gp_new_nm
            })

        }).done(function() {
            alert('<spring:message code="joinUser.028" text="회원가입에 성공하였습니다."/>');
            location.href = "/user/login";
        });
    }
    
    function emailMapping() {
        var _email = "";
        if ($("#domain_select").val() == 'self') {
            _email = $("#email").val() + '@' + $("#email_domain").val();
        } else {
            _email = $("#email").val() + '@' + $("#domain_select").val();
        }
        return _email;
    }
    
    function validCheck() {
        var regFrs = /(\d{3}$)/g;
        var regMdl = /(\d{3}|\d{4})/g;
        var regEnd = /(\d{4}$)/g;
        var isID = /^[a-z0-9][a-z0-9_\-]{4,20}$/;
        var isEmail = /^[a-z0-9][a-z0-9_\-]{0,63}$/;
        var isNM = /^[a-zA-Zㄱ-힣]{2,30}/;
        
        if (document.getElementById('id').value == '') {
            alert('<spring:message code="joinUser.009" text="아이디를 입력하세요."/>');
            id.focus();
            return false;
        }
        
        if (!isID.test(document.getElementById('id').value)) {
            alert('<spring:message code="joinUser.035" text="아이디는 5~20자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다."/>');
            id.focus();
            return false;
        }
        
        if ($("#domain_select").val() == 'self') {
            if (document.getElementById('email').value == '') {
                alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
                $('#email').focus();
                return false;
            }
        }
        
        if ($("#domain_select").val() == 'self') {
            if (document.getElementById('email_domain').value == '') {
                alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
                $('#email_domain').focus();
                return false;
            }
        }
        
        if (!isEmail.test(document.getElementById('email').value)) {
            alert('<spring:message code="joinUser.045" text="이메일은 1~63자의 영문 소문자, 숫자와 특수기호(_),(-)만 사용 가능합니다."/>');
            $('#email').focus();
            return false;
        }
    
        if (document.getElementById('usr_nm').value == '') {
            alert('<spring:message code="joinUser.010" text="이름을 입력하세요."/>');
            usr_nm.focus();
            return false;
        }
        
        if (!isNM.test(document.getElementById('usr_nm').value)) {
            alert('<spring:message code="joinUser.100" text="이름은 2~20자의 한글,영문만 사용 가능합니다."/>');
            usr_nm.focus();
            return false;
        }
        <%--
        var error = 0;
        
        if (document.getElementById('offm_telno').value == '') {
            error++;
        }
        
        if (document.getElementById('cnt_frs').value == '' &&
            document.getElementById('cnt_mdl').value == '' &&
            document.getElementById('cnt_end').value == '') {
            error++;
        } else {
        --%>if(!regFrs.test(document.getElementById('cnt_frs').value)){
                alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
                cnt_frs.focus();
                return false;
            }
        
            if(!regMdl.test(document.getElementById('cnt_mdl').value)){
                alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
                cnt_mdl.focus();
                return false;
            }
        
            if(!regEnd.test(document.getElementById('cnt_end').value)){
                alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
                cnt_end.focus();
                return false;
            }
    <%-- }
        
        if (error > 1) {
            alert('<spring:message code="joinUser.011" text="연락처와 사무실번호 중 최소 한 항목 이상 입력하세요."/>');
            offm_telno.focus();
            return false;
        }
        
        if (checkCorpUse) {
            if (document.getElementById('corp_company').value == '') {
                alert('<spring:message code="joinUser.033" text="기업명을 선택하세요."/>');
                return false;
            }
            if (document.getElementById('corp_group').value == '') {
                alert('<spring:message code="joinUser.034" text="부서명을 선택하세요."/>');
                return false;
            }
        }
        --%>
        if (!checkMailUse) {
            return chkPW();
        }
        return true;
    }
    
    function chkPW() {
    
        var pw = $("#pw").val();
        var num = pw.search(/[0-9]/g);
        var eng = pw.search(/[a-z]/ig);
        var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);
        var confirmPW = document.getElementById('pwCheck').value;
            
        if(pw.length < pwMinSize || pw.length > pwMaxSize){
            var pwLengthErrorMessage = '<spring:message code="joinUser.046" text="비밀번호는"/>';
            pwLengthErrorMessage += ' ' + pwMinSize + '<spring:message code="joinUser.047" text="글자 이상,"/>';
            pwLengthErrorMessage += ' ' + pwMaxSize + '<spring:message code="joinUser.048" text="글자 이하만 이용 가능합니다."/>';
            window.alert(pwLengthErrorMessage);
            document.getElementById('pw').value=document.getElementById('pwCheck').value='';
            document.getElementById('same').innerHTML='';
            return false;
        }else if(pw.search(/\s/) != -1){
            alert('<spring:message code="joinUser.022" text="비밀번호는 공백 없이 입력해주세요."/>');
            return false;
        }else if((num < 0 || eng < 0) && Number(pwCheckLvl) == 2){
            alert('<spring:message code="joinUser.026" text="영문,숫자를 혼합하여 입력해주세요."/>');
            return false;
        }else if((num < 0 || eng < 0 || spe < 0) && Number(pwCheckLvl) == 3){
            alert('<spring:message code="joinUser.023" text="영문,숫자, 특수문자를 혼합하여 입력해주세요."/>');
            return false;
        }else {
            if(document.getElementById('pw').value!='' && document.getElementById('pwCheck').value!='') {
               $("#errorDiv").show();
                if(document.getElementById('pw').value == document.getElementById('pwCheck').value) {
                    document.getElementById('same').innerHTML='<spring:message code="joinUser.020" text="비밀번호가 일치합니다."/>';
                    document.getElementById('same').style.color='blue';
                }
                else {
                    document.getElementById('same').innerHTML='<spring:message code="joinUser.016" text="비밀번호가 일치하지 않습니다."/>';
                    document.getElementById('same').style.color='red';
                    return false;
                }
            }
            return true;
        }
    }

    function getPwPolicy() {
        $.ajax({
            url : "/user/getPwPolicy",
            type : "post",
            contentType : 'application/json',
            data : JSON.stringify({})
        }).done(function(response) {
            pwCheckLvl = response.PW_POLICY.pwCheckLvl;
            pwMaxSize = response.PW_POLICY.pwMaxSize;
            pwMinSize = response.PW_POLICY.pwMinSize;
            
            if (Number(pwCheckLvl) == 2) {
                $("#pw").attr("placeholder", '<spring:message code="joinUser.026" text="영문,숫자를 혼합하여 입력해주세요."/>')
            } else if (Number(pwCheckLvl) == 3) {
                $("#pw").attr("placeholder", '<spring:message code="joinUser.023" text="영문,숫자, 특수문자를 혼합하여 입력해주세요."/>')
            } else {
                $("#pw").attr("placeholder", '<spring:message code="joinUser.022" text="비밀번호는 공백 없이 입력해주세요."/>');
            }
        });
        
    };
    
    var idConfirm = '${idConfirm}';
    var pwConfirm = '${pwConfirm}';
    
    if(pwConfirm != '') {
        alert('<spring:message code="joinUser.016" text="비밀번호가 일치하지 않습니다."/>');
    }
    if(idConfirm != '') {
        alert('<spring:message code="joinUser.017" text="아이디가 중복되었습니다."/>');
    }
    
    
    var corpUse = {
        groupCorpList : null,
        corpInit : function() {
            $('#corpDiv').show();
            $('#groupDiv').hide();
            groupCorpList = [
                <c:forEach items="${GROUP_CORP_LIST}" var="corpGroup" varStatus="status">
                    <c:if test="${!status.first}">,</c:if>  {CD : "${corpGroup.GP_CD}" , NM : "${corpGroup.GP_NM}" , UPPER : "${corpGroup.UPPER_GP_CD}" , }
                </c:forEach>
            ];
            if(groupCorpList != null && groupCorpList.length > 0) {
                for(idx in groupCorpList) {
                    var corp = groupCorpList[idx];
                    if(corp.UPPER == "ROOT"){
                        $("#corp_company option[value='']").remove();
                        $("#corp_company").append("<option value='"+corp.CD+"' upper='"+corp.UPPER+"'>"+corp.NM+"</option>");
                    } else {
                        $("#corp_group").append("<option value='"+corp.CD+"' upper='"+corp.UPPER+"' style='display:none;'>"+corp.NM+"</option>");
                    }
                }
                var upperVal = $("#corp_company").val();
                $("#corp_group").find('option[upper="' + upperVal + '"]').show();
            }
            $("#corp_company").change(function() {
                var upperVal = $(this).val();
                $("#corp_group").find('option').hide();
                $("#corp_group").find('option[upper="' + upperVal + '"]').show();
                var groupFirstVal = $("#corp_group").find('option[upper="' +upperVal + '"]').eq(0).val();
                if(groupFirstVal != null && groupFirstVal != ""){
                    $("#corp_group").val(groupFirstVal);
                    $("#corp_group option[value='']").remove();
                } else {
                    $("#corp_group").append("<option value=''><spring:message code='joinUser.036' text='기업을 선택하거나 부서를 추가하십시오.'/></option>")
                    $("#corp_group option:last").prop("selected", true);
                }
            });
        },
        addCompanyModal : function(){
            $("#addCompanyModal").modal();
        },
        addGroupModal : function(){
            if($("#corp_company").val() != null && $("#corp_company").val() != ""){
                $("#corpName").text($("#corp_company option:selected").text());
                $("#addGroupModal").modal();
            } else {
                alert("<spring:message code='joinUser.037' text='기업명을 우선적으로 추가하여 주십시오.'/>");
            }
        },
        addCompanyButton : function(){
            var company = document.getElementById('_companyModal').value
            if (company == '') {
                alert('<spring:message code="joinUser.029" text="기업명을 입력하세요."/>');
                return false;
            }
            $("#corp_company").append("<option value='TEMP'>"+company+"</option>");
            $("#corp_company option:last").prop("selected", true).change();
            $("#addCompanyModal").modal('hide');
            $("#corp_company option[value='']").remove();
        },
        addGroupButton : function(){
            var group = document.getElementById('_groupModal').value
            if (group == '') {
                alert('<spring:message code="joinUser.030" text="부서명을 입력하세요."/>');
                return false;
            }
            $("#corp_group").append("<option value='TEMP'>"+group+"</option>");
            $("#corp_group option:last").prop("selected", true).change();
            $("#addGroupModal").modal('hide');
            $("#corp_group option[value='']").remove();
        }
    }


    
    
</script>
<body class="nav-md" style="background-color:#F7F7F7;">
    <div class="container body">
        <div class="main_container">
            <div class="x_content">
                <div class="col-xs-12" style="min-height:150px;"></div>
                <div class="col-xs-12">
                    <div class="w-100 text-center">
                        <img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor:pointer;margin-bottom:1%;" onclick="home();" class="img">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-4" style="margin-left:33%;">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>
                                    <spring:message code="joinUser.001" text="회원가입" />
                                </h2>
                            </div>
                        <form class="form-horizontal" id="quickform">
                            <div class="card-body">
                                <%-- 비밀번호 자동완성 기능 방지 --%>
                                
                                <div class="form-group row">
                                    <label for="id" class="col-sm-3 col-form-label">
                                       <spring:message code="joinUser.002" text="아이디" /> (*)
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="id" name="usr_id" maxlength="20"
                                         placeholder="<spring:message code="joinUser.002" text="아이디"/>" autocomplete="false">
                                    </div>
                                </div>
                                
                                <div id="pwdiv">
                                    <div class="form-group row">
                                        <label for="inputPassword" class="col-sm-3 col-form-label"><spring:message
                                                code="joinUser.018" text="비밀번호" /> (*)</label>
                                        <div class="col-sm-9">
                                            <input class="form-control password" type="password" name="usr_pw" id="pw" onchange="chkPW()"
                                                placeholder="<spring:message code="joinUser.018" text="비밀번호"/>">
                                        </div>
                                    </div>

                                    <div class="form-group row" style="margin-bottom: 0px">
                                        
                                        <label for="inputPasswordConfirm"
                                            class="col-sm-3 col-form-label" style="white-space: nowrap;">
                                            <spring:message code="joinUser.019" text="비밀번호 확인" /> (*)
                                        </label>
                                        <div class="col-sm-9">
                                            <input class="form-control password"
                                                name="wUserPWConfirm" id="pwCheck" onchange="chkPW()"
                                                placeholder='<spring:message code="joinUser.019" text="비밀번호 확인"/>'>
                                        </div>
                                    </div>
                                    <div class="form-group row" id="errorDiv" style="margin-top: 0px; margin-bottom: 0px; display:none;">
                                        <div class="col-sm-3"></div>
                                        <span class="col-sm-9" id="same"></span>
                                    </div>
                                </div>
                                <div class="form-group row" style="margin-top: 10px;">
                                    <label for="inputEmail" class="col-sm-3 col-form-label"><spring:message
                                            code="joinUser.003" text="이메일" /> (*)</label>
                                    <div class="col-sm-3" style="display:flex; padding-right:0px;">
                                        <input type="text" id="email" class="form-control" name="usr_email" style="padding-top: 4px; width:90%;">
                                        <div><span>@</span></div>
                                    </div>
                                    
                                    <div class="col-sm-3" style="padding-left:5px;">
                                        <input type="text" class="form-control" name="usr_email_domain"
                                            id="email_domain" style="padding-top: 4px;" maxlength=100>
                                    </div>
                                    <div class="col-sm-3" >
                                        <select id="domain_select" name="MENU_ID"class="form-control" style="padding-top: 4px;">
                                                <option value='self'><spring:message code="joinUser.004" text="직접입력" /></option>
                                                <c:forEach var="domainList" items='${DOMAIN_LIST}'>
                                                    <option value='${domainList.CD_NM}'>${domainList.CD_NM}</option>
                                                </c:forEach>
                                        </select>
                                    </div>
                                </div>
    
                                <div class="form-group row">
                                    <label for="name" class="col-sm-3 col-form-label"><spring:message
                                            code="joinUser.005" text="이름" /> (*)</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="usr_nm" maxlength="30"
                                            placeholder="<spring:message code="joinUser.005" text="이름"/>"
                                            name="usr_nm">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="phonenumber" class="col-sm-3 col-form-label"><spring:message code="joinUser.006" text="연락처" /></label>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder="" id="cnt_frs" maxlength="3" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    </div>
                                    <div class="col-sm-1"  style="width:5px;text-align:center;margin-top:5px;">-</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder="" id="cnt_mdl" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    </div>
                                    <div class="col-sm-1" style="width:5px;text-align:center;margin-top:5px;">-</div>
                                    <div class="col-sm-2">
                                        <input type="text" class="form-control" placeholder="" id="cnt_end" maxlength="4" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');">
                                    </div>
                                </div>
                                
                                <%--
                                
                                <div id="groupDiv" class="form-group row">
                                    <label class="col-sm-3 col-form-label"><spring:message code="joinUser.007" text="부서" /> (*)</label>
                                    <div class="col-sm-9">
                                        <select class="form-control select2 select2-hidden-accessible"  id="gp_cd" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="gp_cd">
                                            <c:forEach var="groupList" items='${GROUP_LIST}'>
                                                <option value='${groupList.GP_CD}'>${groupList.GP_NM}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div id="corpDiv" style="display:none;">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label"><spring:message code="joinUser.027" text="기업명" /> (*)</label>
                                        <div class="col-sm-9" style="display:flex;">
                                            <select class="form-control select2 select2-hidden-accessible"  id="corp_company" style="width: 80%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option value=''><spring:message code='joinUser.038' text='기업을 추가하십시오'/></option>
                                            </select>
                                            <button type="button" class="btn btn-dark" style="margin-left:20px;" onclick="corpUse.addCompanyModal()"><spring:message code='joinUser.039' text='추가'/></button>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label"><spring:message code="joinUser.007" text="부서" /> (*)</label>
                                        <div class="col-sm-9" style="display:flex;">
                                            <select class="form-control select2 select2-hidden-accessible"  id="corp_group" style="width: 80%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                                <option value=''><spring:message code='joinUser.036' text='기업을 선택하거나 부서를 추가하십시오'/></option>
                                            </select>
                                            <button type="button" class="btn btn-dark" style="margin-left:20px;" onclick="corpUse.addGroupModal()">추가</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"><spring:message code="joinUser.025" text="직책" /> (*)</label>
                                    <div class="col-sm-9">
                                    <select class="form-control select2 select2-hidden-accessible" id="usr_rspofc" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="usr_rspofc">
                                        <c:forEach var="rspofcList" items='${RSPOFC_LIST}'>
                                            <option value='${rspofcList.CD}'>${rspofcList.CD_NM}</option>
                                        </c:forEach>
                                    </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="office_telno" class="col-sm-3 col-form-label"><spring:message
                                                code="joinUser.012" text="사무실번호" /></label>
                                    <div class="col-sm-9" style="padding-top: 4px;">
                                        <input type="text" class="form-control" placeholder="" id="offm_telno" name="offm_telno" style="padding-top: 4px;" maxlength="20">
                                    </div>
                                </div>
                                
                                --%>
                                
                            </div>
                            <br/>
                            <div class="card-footer">
                                <div class="col-sm-6">
                                    <button type="button" id="JOINUSER_ADD" class="btn btn-info">
                                        <spring:message code="joinUser.001" text="회원가입" />
                                    </button>
                                </div>
                                <div class="col-sm-6 btn-area">
                                    <button type="button" id="JOINUSER_CANCLE" class="btn btn-default float-right">
                                        <spring:message code="joinUser.008" text="취소" />
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%-- 
<div id="addCompanyModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width:500px; min-height: 150px;">
        <div class="modal-content" > 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><spring:message code="joinUser.040" text="기업 추가" /></h4>
            </div>
            <div class="modal-body" style="min-height: 150px;">
                <div class="col-xs-12"><spring:message code="joinUser.041" text="목록에 기업이 없는 경우 추가할 수 있습니다." /></div>
                <div class="input-group">
                    <input type="text" class="form-control" id="_companyModal" maxlength="12" placeholder='<spring:message code="joinUser.029" text="기업명을 입력하세요." />'/>
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-dark" onclick="corpUse.addCompanyButton()"><spring:message code="joinUser.039" text="추가" /></button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="addGroupModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm" style="width:500px; min-height: 150px;">
        <div class="modal-content" > 
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title"><spring:message code="joinUser.042" text="부서 추가" /></h4>
            </div>
            <div class="modal-body" style="min-height: 150px;">
                <div class="col-xs-12" style="font-weight:bold; margin-bottom: 10px;"><spring:message code="joinUser.027" text="기업명" /> : <span id="corpName" style="font-weight:bold;"></span></div>
                <br>
                <div class="input-group">
                    <input type="text" class="form-control" id="_groupModal" placeholder='<spring:message code="joinUser.030" text="부서명을 입력하세요." />'/ maxlength="20">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn-dark" onclick="corpUse.addGroupButton()"><spring:message code="joinUser.039" text="추가" /></button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
--%>
</body>

</html>