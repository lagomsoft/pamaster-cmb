<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
<title><spring:message code="changePasswd.001" text="비밀번호 초기화"/></title>

<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>

</head>
<script>
	var pwInitlYn = '${PW_INITL_YN}';
	
	var pwCheckLvl = null;
	var pwMaxSize = null;
	var pwMinSize = null;
	
	$(function() {
		getPwPolicy();

		$("#pwCheck").keyup(function(e){if(e.keyCode == 13)
			changePw();
		});
	});
	
	function isSame() {
		var pw = $("#pw").val();
		var num = pw.search(/[0-9]/g);
		var eng = pw.search(/[a-z]/ig);
		var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);
		var confirmPW = document.getElementById('pwCheck').value;
			
		if(pw.length < pwMinSize || pw.length > pwMaxSize){
			var pwLengthErrorMessage = '<spring:message code="joinUser.046" text="비밀번호는"/>';
			pwLengthErrorMessage += ' ' + pwMinSize + '<spring:message code="joinUser.047" text="글자 이상,"/>';
			pwLengthErrorMessage += ' ' + pwMaxSize + '<spring:message code="joinUser.048" text="글자 이하만 이용 가능합니다."/>';
			window.alert(pwLengthErrorMessage);
		    document.getElementById('pw').value=document.getElementById('pwCheck').value='';
		    document.getElementById('same').innerHTML='';
		  	return false;
		}else if(pw.search(/\s/) != -1){
			alert('<spring:message code="joinUser.022" text="비밀번호는 공백 없이 입력해주세요."/>');
		  	return false;
		}else if((num < 0 || eng < 0) && Number(pwCheckLvl) == 2){
			alert('<spring:message code="joinUser.026" text="영문,숫자를 혼합하여 입력해주세요."/>');
			return false;
		}else if((num < 0 || eng < 0 || spe < 0) && Number(pwCheckLvl) == 3){
			alert('<spring:message code="joinUser.023" text="영문,숫자, 특수문자를 혼합하여 입력해주세요."/>');
		  	return false;
		}else {
			if(document.getElementById('pw').value!='' && document.getElementById('pwCheck').value!='') {
		       $("#SAME_DIV").show();
		        if(document.getElementById('pw').value == document.getElementById('pwCheck').value) {
		            document.getElementById('same').innerHTML='<spring:message code="joinUser.020" text="비밀번호가 일치합니다."/>';
		            document.getElementById('same').style.color='blue';
		        }
		        else {
		            document.getElementById('same').innerHTML='<spring:message code="joinUser.016" text="비밀번호가 일치하지 않습니다."/>';
		            document.getElementById('same').style.color='red';
		            return false;
		        }
		}
		    return true;
		}
	}
	
	function changePw() {

		if(!isSame()) {
			alert('<spring:message code="changePasswd.010" text="비밀번호가 일치하지 않습니다."/>');
			return;
		}
		
		$.ajax({
			url : "/user/changePasswd",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({
				USR_ID : $("#usr_id").val()
				,USR_PW : $("#pw").val()
				,pw_initl_yn : 'N'
			})
			
			}).done(function(response) {
				if("1" == response.SUCCESS) {
					alert('<spring:message code="changePasswd.002" text="이전 비밀번호는 재사용할 수 없습니다."/>');
					return;
				}
				alert('<spring:message code="changePasswd.012" text="비밀번호가 변경되었습니다."/>');
				location.href = "../user/login";
			});
	}

	function getPwPolicy() {
		$.ajax({
			url : "/user/getPwPolicy",
			type : "post",
			contentType : 'application/json',
			data : JSON.stringify({})
		}).done(function(response) {
			pwCheckLvl = response.PW_POLICY.pwCheckLvl;
			pwMaxSize = response.PW_POLICY.pwMaxSize;
			pwMinSize = response.PW_POLICY.pwMinSize;
		});
		
	};
	
	function goBack(){
		if(pwInitlYn == "Y"){
			location.href='/user/logout';
		}else{
			location.href='../../';
		}
	}
</script>
<body class="nav-md" style="background-color:#F7F7F7;">
	<div class="container body">
		<div class="main_container">
			<div class="x_content">
				<div class="col-xs-12" style="min-height:150px;"></div>
				<div class="col-xs-12">
					<div class="w-100 text-center">
			            <img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor:pointer;margin-bottom:1%;" class="img">
		            </div>
			    </div>
				<div class="row">
					<div class="col-xs-5" style="margin-left:29%;">
						<div class="x_panel">
							<div class="x_title">
								<h2><spring:message code="changePasswd.001" text="비밀번호 변경"/></h2>
							</div>
		
							<form class="form-horizontal">
								<div class="card-body">
									<div class="form-group row">
										<label for="inputPassword" class="col-sm-3 col-form-label"><spring:message code="changePasswd.003" text="아이디"/></label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="usr_id" name="usr_id" value="${userId}" readonly>
										</div>
									</div>
									
									<div class="form-group row">
										<label for="inputPassword" class="col-sm-3 col-form-label"><spring:message code="changePasswd.004" text="새 비밀번호"/></label>
										<div class="col-sm-9">
											<input type="password" class="form-control" name="usr_pw" id="pw" onchange="isSame()" placeholder="<spring:message code="changePasswd.004" text="새 비밀번호"/>">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPasswordConfirm"
											class="col-sm-3 col-form-label" style="white-space: nowrap;"><spring:message code="changePasswd.005" text="새 비밀번호 확인"/></label>
										<div class="col-sm-9">
											<input type="password" class="form-control" name="wUserPWConfirm"
												id="pwCheck" onchange="isSame()" placeholder="<spring:message code="changePasswd.005" text="새 비밀번호 확인"/>">
										</div>
									</div>
									<div id="SAME_DIV" class="form-group row" style="display:none;">
									&nbsp;&nbsp;<span id="same"></span>
									</div>
									
								</div>
			
								<hr>
								<!-- /.card-body -->
								<div class="card-footer">
									<div class="col-sm-12 btn-area">
										<button type="button" onclick="javascript: goBack();" class="btn btn-default float-right"><spring:message code="changePasswd.007" text="취소"/></button>
										&nbsp;
										<button type="button" class="btn btn-info float-right" onclick="changePw();"><spring:message code="changePasswd.006" text="변경"/></button>
									</div>
								</div>
								<!-- /.card-footer -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>					
	</div>
</body>
</html>