<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="changeUserInfo.001" text="회원정보수정"/></title>

<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>

</head>
<script>

	var groupList = [
		<c:forEach items="${GROUP_LIST}" var="group" varStatus="status">
			<c:if test="${!status.first}">,</c:if>  {Id : "${group.GP_CD}" , Name : "${group.GP_NM}"}
		</c:forEach>
	];
	
	var checkMailUse = ${CHECK_MAIL_USE};
	
	$(function() {
		if (checkMailUse == true) {
			$("#usr_email").attr("readonly",true);
		}
		if (checkMailUse == false) {
		}
		
		$("#groubSelect").val("${GP_CD}");
		
		
		$("#CHANGE_SUBMIT").on("click",function() {
		
			var _check = validCheck();
			
			if(_check) {
			
				$.ajax({
				url : "/user/changeUserInfo",
				type : "post",
				contentType : 'application/json',
				data : JSON.stringify({
					USR_ID : $("#usr_id").val(),
					USR_NM : $("#usr_nm").val(),
					USR_EMAIL : $("#usr_email").val(),
					CNT_FRS : $("#cnt_frs").val(),
					CNT_MDL : $("#cnt_mdl").val(),
					CNT_END : $("#cnt_end").val(),
					GP_CD : $("#groubSelect option:selected").val(),
					OFFM_TELNO : $("#offm_telno").val()
				})
				
				}).done(function(response) {
					if(response) {
						alert('<spring:message code="joinUser.024" text="회원정보가 변경되었습니다."/>')
					} else {
					
					}
					
				});
				
			}
			
		});
		
	});
	
	
	
	function validCheck() {
	
		var regFrs = /(01[0|1|6|9|7])/g;
		var regMdl = /(\d{3}|\d{4})/g;
		var regEnd = /(\d{4}$)/g;
		
		if (!checkMailUse) {
			if (document.getElementById('usr_email').value == '') {
				alert('<spring:message code="joinUser.013" text="이메일을 입력하세요."/>');
				$('#usr_email').focus();
				
				return false;
			}
		}
	
		if (document.getElementById('usr_nm').value == '') {
			alert('<spring:message code="joinUser.010" text="이름을 입력하세요."/>');
			usr_nm.focus();
	
			return false;
		}
		
		var error = 0;
		if (document.getElementById('offm_telno').value == '') {
			error++;
		}
		
		if (document.getElementById('cnt_frs').value == '' &&
			document.getElementById('cnt_mdl').value == '' &&
			document.getElementById('cnt_end').value == '') {
			error++;
		} else {
			if(!regFrs.test(document.getElementById('cnt_frs').value)){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				cnt_frs.focus();
		
				return false;
			}
		
			if(!regMdl.test(document.getElementById('cnt_mdl').value)){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				cnt_mdl.focus();
		
				return false;
			}
		
			if(!regEnd.test(document.getElementById('cnt_end').value)){
				alert('<spring:message code="createProject.037" text="연락처를 올바르게 입력하세요."/>');
				cnt_end.focus();
		
				return false;
			}
		}
		
		if (error > 1) {
			alert('<spring:message code="joinUser.011" text="연락처와 사무실번호 중 최소 한 항목 이상 입력해주십시오."/>');
			offm_telno.focus();
			return false;
		} 
		
		if (!checkMailUse) {
			return chkPW();
		}

		return true;
	
	}

</script>
<body class="nav-md" style="background-color:#F7F7F7;">
	<div class="container body">
		<div class="main_container">
			<div class="x_content">
				<div class="col-xs-12" style="min-height:200px;"></div>
				<div class="col-xs-12">
					<div class="w-100 text-center">
			            <img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor:pointer;margin-bottom:1%;" class="img">
		            </div>
			    </div>
				<div class="row">
					<div class="col-xs-5" style="margin-left:29%;">
						<div class="x_panel">
							<div class="x_title">
								<h2><spring:message code="changeUserInfo.002" text="나의 정보 수정" /></h2>
							</div>
							<form class="form-horizontal" action="/user/changeUserInfo"	method="POST">
								<div class="card-body">
									<div class="form-group row">
										<label for="usr_id" class="col-sm-3 col-form-label"><spring:message
												code="changeUserInfo.003" text="아이디" /> (*)</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="usr_id"
												name="usr_id" value="${usr_id}" readonly>
										</div>
									</div>
									<!-- 
									<div class="form-group row">
										<label for="inputPassword" class="col-sm-3 col-form-label">비밀번호</label>
										<div class="col-sm-9">
											<input type="password" class="form-control" name="usr_pw" id="pw" onchange="isSame()" placeholder="비밀번호">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputPasswordConfirm"
											class="col-sm-3 col-form-label" style="white-space: nowrap;">비밀번호
											확인</label>
										<div class="col-sm-9">
											<input type="password" class="form-control" name="wUserPWConfirm"
												id="pwCheck" onchange="isSame()" placeholder="비밀번호 확인">
										</div>
									</div>
									<div class="form-group row">
									&nbsp;&nbsp;<span id="same"></span>
									</div>
									 -->
									<div class="form-group row">
										<label for="name" class="col-sm-3 col-form-label"><spring:message
												code="changeUserInfo.004" text="이름" /> (*)</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="usr_nm"
												<spring:message
												code="changeUserInfo.004" text="이름" /> name="usr_nm" value="${usr_nm}">
										</div>
									</div>
									<div class="form-group row">
										<label for="email" class="col-sm-3 col-form-label"><spring:message
												code="changeUserInfo.014" text="이메일" /> (*)</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="usr_email"
												<spring:message
												code="changeUserInfo.014" text="이메일" /> name="usr_email" value="${usr_email}">
										</div>
									</div>
									<div class="form-group">
										<label for="phonenumber" class="col-sm-3 col-form-label">
											<spring:message	code="changeUserInfo.005" text="연락처" />
										</label>
										<div class="col-sm-9">
											<div class="col-sm-2">
												<input type="text" class="form-control" placeholder="" id="cnt_frs"	name="cnt_frs" style="height: 30px;" value="${cnt_frs}">
											</div>
											<div class="col-sm-1" style="width:5px;text-align:center;margin-top:3px;">-</div>
											<div class="col-sm-2">
												<input type="text" class="form-control" placeholder="" id="cnt_mdl" name="cnt_mdl" style="height: 30px;" value="${cnt_mdl}">
											</div>
											<div class="col-sm-1" style="width:5px;text-align:center;margin-top:3px;">-</div>
											<div class="col-sm-2">
												<input type="text" class="form-control" placeholder="" id="cnt_end"	name="cnt_end" style="height: 30px;" value="${cnt_end}">
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 col-form-label">
											<spring:message code="changeUserInfo.06"	text="부서" /> (*)
										</label>
										<div class="col-sm-9">
											<select id="groubSelect" class="form-control select2 select2-hidden-accessible"	style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="gp_cd">
												<c:forEach var="groupList" items='${GROUP_LIST}'>
														<option value='${groupList.GP_CD}'>${groupList.GP_NM}</option>
												</c:forEach>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="offm_telno" class="col-sm-3 col-form-label">
											<spring:message code="changeUserInfo.015" text="사무실번호" />
										</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="offm_telno"
												<spring:message
												code="changeUserInfo.015" text="사무실번호" /> name="offm_telno" value="${offm_telno}">
										</div>
									</div>
								</div>
								<br/>
								<div class="card-footer">
									<div class="col-sm-6">
										<button type="button" class="btn btn-info" id="CHANGE_SUBMIT">
											<spring:message code="changeUserInfo.007" text="수정" />
										</button>
									</div>
									<div class="col-sm-6 btn-area">
										<button type="button" class="btn btn-default float-right" onclick="location.href='./myInfo'">
											<spring:message code="changeUserInfo.008" text="취소" />
										</button>
										<button type="reset" class="btn btn-default float-right" style="margin-right: 1%;" onclick="location.href='./changePasswd?userId=${userId}'">
											<spring:message code="changeUserInfo.009" text="비밀번호변경" />
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				<div class="col-lg-3" style="background-color: #F2F4F6;"></div>
			</div>
		</div>
	</div>
</div>
</body>
</html>