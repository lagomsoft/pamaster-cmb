<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="findPw.001" text="비밀번호 찾기"/></title>
<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>
<script>

	window._isNotsessionCheck = true;

	var checkMailUse = "${CHECK_MAIL_USE}";
	
	$(function() {
		
		if (checkMailUse == "false") {
			alert('<spring:message code="findPw.008" text="비밀번호 찾기는 관리자에게 문의하세요."/>');
			history.back();
		}
	
		$("#SEARCH_PASSWD").on("click",function() {
			findPw();
		});
		
		$("#usr_id").keyup(function(e){if(e.keyCode == 13)
			findPw();
		});
		
		$("#usr_email").keyup(function(e){if(e.keyCode == 13)
			findPw();
		});
		
		$("#CANCEL").on("click",function() {
			history.back();
		});
		
		
	
	});
	
	function findPw() {
		var _check = validCheck();
		
		if(_check) {
		
			$.ajax({
				url : "/user/findPw",
				type : "post",
				contentType : 'application/json',
				data : JSON.stringify({
					USR_ID : $("#usr_id").val(),
					USR_EMAIL : $("#usr_email").val()
				})
				
			}).done(function(response) {
				if(response) {
					alert('<spring:message code="findUser.002" text="계정의 이메일로 임시 비밀번호를 전송했습니다."/>');
					location.href="/user/login";
				} else {
					alert('<spring:message code="findPw.009" text="존재하지 않는 계정입니다. 아이디와 이메일을 확인해주세요."/>');
					$("#usr_nm").focus();
				}
				
			});
		
		}
		
				
	}
	
	
	function validCheck() {
		if (document.getElementById('usr_id').value == '') {
			alert('<spring:message code="findPw.003" text="아이디를 입력하세요."/>');
			usr_id.focus();
	
			return false;
		}
	
		if (document.getElementById('usr_email').value == '') {
			alert('<spring:message code="findUser.007" text="이메일을 입력하세요."/>');
			$("#usr_email").focus();
	
			return false;
		}
		
		return true;
	
	}

	var msg = '${whetherfindPw}';
	if (msg === "true") {
		alert('<spring:message code="findPw.002" text="아이디와 연락처가 일치합니다."/> \n <spring:message code="findPw.002" text="비밀번호 초기화 페이지로 이동합니다."/>');
		location.href="./changePasswd";
	}
	else if (msg === "false") {
		alert('<spring:message code="findPw.003" text="아이디나 연락처가 일치하지 않습니다."/>')
	}
</script>
</head>

<body class="nav-md" style="background-color:#F7F7F7;">
	<div class="container body">
		<div class="main_container">
			<div class="x_content">
				<div class="col-xs-12" style="min-height:200px;"></div>
				<div class="col-xs-12">
					<div class="w-100 text-center">
			            <img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor:pointer;margin-bottom:1%;" class="img">
		            </div>
			    </div>
				<div class="row">
					<div class="col-xs-4" style="margin-left:33%;">
						<div class="x_panel">
							<div class="x_title">
								<h2><spring:message code="findPw.001" text="비밀번호 찾기"/></h2>
							</div>
							<div class="card-body">
								<div class="form-group row">
									<label for="inputID" class="col-sm-3 col-form-label"><spring:message code="findPw.004" text="아이디"/></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" name="usr_id" id="usr_id" placeholder="<spring:message code="findPw.004" text="아이디"/>">
									</div>
								</div>
								<div class="form-group row">
								<label for="email" class="col-sm-3 col-form-label"><spring:message
										code="findUser.003" text="이메일" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="usr_email"
											<spring:message
											code="findUser.003" text="이메일" /> name="usr_email">
									</div>
								</div>
							</div>
							<br/>
							<div class="card-footer">
								<div class="col-sm-6">
									<button type="button" id="SEARCH_PASSWD" class="btn btn-info"><spring:message code="findPw.006" text="찾기"/></button>
								</div>
								<div class="col-sm-6 btn-area">
									<button type="button" id="CANCEL" class="btn btn-default float-right"><spring:message code="findPw.007" text="취소"/></button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>