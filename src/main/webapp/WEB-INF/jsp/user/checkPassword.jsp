<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="passwordCheck.001" text="본인인증" /></title>

<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.css" rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css" rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>
</head>
<script>
	var pwInitlYn = '${PW_INITL_YN}';
	
	$(function() {

		if("Y" == pwInitlYn) {
			alert('<spring:message code="changePasswd.011" text="비밀번호를 변경하십시오."/>')
		}
		
		$("#PASSWORD_CHECK").on("click",function() {
			checkPassword();
		});
		
		$("#usr_pw").keyup(function(e){if(e.keyCode == 13)
			checkPassword();
		});

	});
	
	function checkPassword() {
		
		$.ajax({
				url : "../user/checkPassword",
				type : "post",
				contentType : 'application/json',
				data : JSON.stringify({
					USR_PW : $("#usr_pw").val(),
				})
				
				}).done(function(response) {
					if(response) {
						location.href="${NEXT_STAGE}";
					} else {
						alert('<spring:message code="passwordCheck.003" text="비밀번호가 일치하지 않습니다."/>');
						$("#usr_pw").focus();
					}
					
				});
				
	}
</script>
<body class="nav-md" style="background-color: #F7F7F7;">
	<div class="container body">
		<div class="main_container">
			<div class="x_content">
				<div class="col-xs-12" style="min-height: 200px;"></div>
				<div class="row">
					<div class="col-xs-12">
						<div class="w-100 text-center">
							<img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor: pointer; margin-bottom: 1%;" class="img">
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-xs-5" style="margin-left: 29%;">
						<div class="x_panel">
							<div class="x_title">
								<h2>
									<spring:message code="passwordCheck.001" text="본인인증" />
								</h2>
							</div>

							<div class="form-group">
								<label for="exampleInputPassword1">
									<spring:message code="passwordCheck.004" text="비밀번호를 입력해주시기 바랍니다." />
								</label>
								<div style="display:flex;">
									<input type="password" name="password" class="form-control" style="width:90%" id="usr_pw" placeholder="<spring:message code="login.005" text="PASSWORD"/>">
									<button type="button" id="PASSWORD_CHECK" class="btn btn-primary">
										<spring:message code="common.106" text="확인" />
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>

</html>

