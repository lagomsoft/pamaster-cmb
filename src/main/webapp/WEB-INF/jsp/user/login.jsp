<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="login.001" text="로그인"/></title>

<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>

</head>
<script>

	window._isNotsessionCheck = true;
	var pwConfirm = '${pwConfirm}'
	
	$(function() {
		if (pwConfirm != '') {
			alert('<spring:message code="login.002" text="비밀번호가 변경되었습니다." />');
		}

		var userInputId = getCookie("userInputId");
	    var setCookieYN = getCookie("setCookieYN");
	    
	    if(setCookieYN == 'Y') {
	        $("#idSaveCheck").prop("checked", true);
	    } else {
	        $("#idSaveCheck").prop("checked", false);
	    }
	    
	    $("#USR_ID").val(userInputId); 
	    
	    //로그인 버튼 클릭
	    $('#LOGIN_BTN').click(function() {
	        if($("#idSaveCheck").is(":checked")){ 
	            var userInputId = $("#USR_ID").val();
	            setCookie("userInputId", userInputId, 60); 
	            setCookie("setCookieYN", "Y", 60);
	        } else {
	            deleteCookie("userInputId");
	            deleteCookie("setCookieYN");
	        }
	    });
	})
	
	//쿠키값 Set
	function setCookie(cookieName, value, exdays){
	    var exdate = new Date();
	    exdate.setDate(exdate.getDate() + exdays);
	    var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + 
	    exdate.toGMTString());
	    document.cookie = cookieName + "=" + cookieValue;
	}
	
	//쿠키값 Delete
	function deleteCookie(cookieName){
	    var expireDate = new Date();
	    expireDate.setDate(expireDate.getDate() - 1);
	    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
	}
	
	//쿠키값 가져오기
	function getCookie(cookie_name) {
	    var x, y;
	    var val = document.cookie.split(';');
	    
	    for (var i = 0; i < val.length; i++) {
	        x = val[i].substr(0, val[i].indexOf('='));
	        y = val[i].substr(val[i].indexOf('=') + 1);
	        x = x.replace(/^\s+|\s+$/g, ''); // 앞과 뒤의 공백 제거하기
	        
	        if (x == cookie_name) {
	          return unescape(y); // unescape로 디코딩 후 값 리턴
	        }
	    }
	}
	
	function home() {
// 		location.href = "/common/main";
	}
	
</script>
<body class="nav-md" style="background-color:#F7F7F7;">
	<div class="container body">
		<div class="main_container">
			<div class="x_content">
				<div class="col-xs-12" style="height:20%;"></div>
				<div class="row">
					<div class="col-xs-4" style="margin-left:33%;">
						<div class="w-100 text-center">
				            <img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor:pointer;" onclick="home();" class="img">
			            </div>
			        </div>
			     </div>
		        <br />
		        <br />
				<div class="row">
					<div class="col-xs-3" style="margin-left:37%;">
						
						<div class="x_content">
							<form id="quickForm" novalidate="novalidate" action="/user/login" method="POST">
								<div class="form-group">
									<input type="text" name="username" class="form-control"	id="USR_ID" placeholder="<spring:message code="login.004" text="ID"/>" style="height:5%;">
								</div>
								<div class="form-group">
									<input	type="password" name="password" class="form-control" id="USR_PW" placeholder="<spring:message code="login.005" text="PASSWORD"/>" style="height:5%;">
								</div>
								<button type="submit" style="width:100%;min-height:50px;" id="LOGIN_BTN" class="btn btn-primary"><h2><spring:message code="login.001" text="로그인"/></h2></button>
								<div class="form-group">
									<div class="custom-control custom-checkbox">
										<p class="p_chk p_label">
											<input type="checkbox" name="terms"	class="form-check-input" id="idSaveCheck">
											<label class="form-check-label" for="idSaveCheck"><spring:message code="login.009" text="아이디 저장"/></label>
										</p>
									</div>
								</div>
								<hr>
								<div class="input-group-append" style="padding-left:23%;">
									<a href="./findUser" class=float-right style="margin-right:5%;"><spring:message code="login.006" text="아이디 찾기"/></a> 
									<a href="./findPw" class=float-right style="margin-right:5%;"><spring:message code="login.007" text="비밀번호 찾기"/></a> 
									<a href="./joinUser" class=float-right style="margin-right:5%;"><spring:message code="login.008" text="회원가입"/></a>
								</div>
								
							</form>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

