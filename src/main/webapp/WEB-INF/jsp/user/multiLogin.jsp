<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="loginError.004" text="중복로그인" /></title>
<script type="text/javascript">
	alert('<spring:message code="loginError.005" text="다른 위치에서 로그인하여 로그아웃 되었습니다." />');
	location.href = "/user/login";
</script>
</head>
<body>
</body>
</html>