<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="loginError.002" text="로그인 실패" /></title>
<script>
	window._isNotsessionCheck = true;
	var failure = '<%=session.getAttribute("LOGIN_FAIL_CD")%>';
	
	if(failure == '2') {
		alert('<spring:message code="loginError.006" text="승인되지 않은 아이디 혹은 잘못된 아이디 입니다." />')
	} else if(failure == '0'){
		alert('<spring:message code="loginError.007" text="비밀번호 실패 횟수가 초과되었습니다." />')	
	} else if(failure == '3') {
        alert('<spring:message code="loginError.008" text="사용할 수 없는 계정입니다. 관리자에게 문의 하세요." />');
    } else {
		alert('<spring:message code="loginError.003" text="아이디나 비밀번호가 일치하지 않습니다." />');
	}
	location.href = "/user/login";
</script>
</head>
<body>

</body>
</html>