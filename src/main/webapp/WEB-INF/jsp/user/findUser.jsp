<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title><spring:message code="findUser.001" text="아이디 찾기"/></title>

<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>
<script type="text/javascript">

	window._isNotsessionCheck = true;

	$(function() {

		$("#SEARCH_ID").on("click",function() {
			findUser();
		});
		
		$("#usr_nm").keyup(function(e){if(e.keyCode == 13)
			findUser();
		});
		
		$("#usr_email").keyup(function(e){if(e.keyCode == 13)
			findUser();
		});
		
		$("#CANCEL").on("click",function() {
			history.back();
		});
	
	});
	
	function findUser() {
		var _check = validCheck();
		
		if(_check) {
		
			$.ajax({
				url : "/user/findUser",
				type : "post",
				contentType : 'application/json',
				data : JSON.stringify({
					USR_NM : $("#usr_nm").val(),
					USR_EMAIL : $("#usr_email").val()
				})
				
			}).done(function(response) {
				if(response) {
					alert('<spring:message code="findUser.008" text="사용자의 ID"/>' + " : " + response);
					location.href="/user/login";
				} else {
					alert('<spring:message code="findUser.009" text="존재하지 않는 계정입니다. 이름과 이메일을 확인해주세요."/>');
					$("#usr_nm").focus();
				}
				
			});
		
		}
		
				
	}



	function validCheck() {
		if (document.getElementById('usr_nm').value == '') {
			alert('<spring:message code="findUser.006" text="이름을 입력하세요."/>');
			usr_nm.focus();
	
			return false;
		}
	
		if (document.getElementById('usr_email').value == '') {
			alert('<spring:message code="findUser.007" text="이메일을 입력하세요."/>');
			$("#usr_email").focus();
	
			return false;
		}
		
		return true;
	
	}
	
	

	
	

</script>
</head>
<body class="nav-md" style="background-color:#F7F7F7;">
	<div class="container body">
		<div class="main_container">
			<div class="x_content">
				<div class="col-xs-12" style="min-height:200px;"></div>
				<div class="col-xs-12">
					<div class="w-100 text-center">
			            <img src="/img/PAMASTER_EN.png" width="300" height="60" style="cursor:pointer;margin-bottom:1%;" class="img">
		            </div>
			    </div>
				<div class="row">
					<div class="col-xs-4" style="margin-left:33%;">
						<div class="x_panel">
							<div class="x_title">
								<h2><spring:message code="findUser.001" text="아이디 찾기"/></h2>
							</div>
							<div class="card-body">
								<div class="form-group row">
									<label for="inputName" class="col-sm-3 col-form-label"><spring:message code="findUser.002" text="이름"/></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="usr_nm" name="usr_nm" maxlength="20" placeholder="<spring:message code="findUser.002" text="이름"/>">
									</div>
								</div>
								<div class="form-group row">
								<label for="email" class="col-sm-3 col-form-label"><spring:message
										code="findUser.003" text="이메일" /></label>
									<div class="col-sm-9">
										<input type="text" class="form-control" id="usr_email" maxlength="50"
											<spring:message
											code="findUser.003" text="이메일" /> name="usr_email">
									</div>
								</div>
							</div>
							<br/>
							<div class="card-footer">
								<div class="col-sm-6">
									<button type="button" id="SEARCH_ID" class="btn btn-info"><spring:message code="findUser.004" text="찾기"/></button>
								</div>
								<div class="col-sm-6 btn-area">
									<button type="button" id="CANCEL" class="btn btn-default float-right"><spring:message code="findUser.005" text="취소"/></button>
								</div>
							</div>
							<!-- /.card-footer -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>