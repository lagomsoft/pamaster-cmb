<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html>
<style>
	.my-custom-scrollbar {
	    position: relative;
	    max-height: 360px;
	    overflow: auto;
	}
	
	.table-wrapper-scroll-y {
	    display: block;
	}
</style>
<head>
<script src="/js/PAMsSelectPub.js"></script>

<meta charset="EUC-KR">
<title>Insert title here</title>
<script type="text/javascript">
    var projectNo = null;
    var projectStep = null;
    var projectFlTyCd = null;
    
    var updateList = new Array();
    var selectItemMap = null;
    var selectItemRow = null;
    
    var mainFlNo = null;
    
    var bindTyList = [
        <c:forEach items="${BIND_TY_CD}" var="BTC" varStatus="status">
            <c:if test="${!status.first}">,</c:if>
            <c:if test="${BTC.CD ne ''}">
                {Id : "${BTC.CD}" , Name : "${BTC.CD_NM}"}
            </c:if>
            
        </c:forEach>
    ];
    
    $(function () {

        $("#KEY_CONFIG_TBL").find("td").css("border-top", "#FFF");
        
    	loadPAMsProjectSelect("PROJECT_SELECT_DIV", 200);
    	
    	$("#SHOW_DIV").hide();
    	$(".click-row-data").hide();
    	
        $("#PAMS_PROJECT_SELECT_BTN").on("click",function() {
            
            if ($("#PAMS_PROJECT_SELECT").val() == null || $("#PAMS_PROJECT_SELECT").val() == "") {
                alert('<spring:message code="common.038" text="프로젝트를 선택하세요." />');
                
                
            } else {
                
            	$("#SHOW_DIV").show();
                $(".click-row-data").hide();
            	
                projectNo = $("#PAMS_PROJECT_SELECT").val();
                projectStep = $("#PAMS_PROJECT_SELECT option:checked").attr("step");
                
                if(Number(projectStep) < 330) {
                	projectFlTyCd = "K"
                } else {
                	projectFlTyCd = "D"
                }
                
                if(projectStep == 250 || projectStep == 450) {
                	$(".btn-area").hide();
                } else {
                	$(".btn-area").show();
                }
                
                getBindConfig();
            }
        });
       
     	
    	<%-- 표본, 샘플 사용자 설정으로 변경 시 코드 --%>
        $(".sample-config-radio").change(function(){
            if( $(this).attr("data-yn") == 'Y') {
            	$(".sample-config-tr").show();
            } else {
            	$(".sample-config-tr").hide();
            }	
                
        });
        
        $(".noise-config-radio").change(function(){
            if( $(this).attr("data-yn") == 'Y' ) {
            	$(".noise-config-tr").show();            	
            } else {
            	$(".noise-config-tr").hide();    
            }    
                       
        });
        
        $(".sample-radio").change(function(){
        	
            if( $(this).attr("id") == 'SAMPLE_PCTV_RADIO') {
                $("#SAMPLE_CNT").attr("readonly", "true");
                $("#SAMPLE_PCTV").removeAttr("readonly");
            } else {
                $("#SAMPLE_PCTV").attr("readonly", "true");
                $("#SAMPLE_CNT").removeAttr("readonly");
            }   
                
        });
        
        $(".noise-radio").change(function(){
        	if( $(this).attr("id") == 'NOISE_PCTV_RADIO') {
                $("#NOISE_CNT").attr("readonly", "true");
                $("#NOISE_PCTV").removeAttr("readonly");
            } else {
                $("#NOISE_PCTV").attr("readonly", "true");
                $("#NOISE_CNT").removeAttr("readonly");
            }     
                       
        });
        
        // default
        $("#SAMPLE-CONFIG-RADIO-N").click();
        $("#NOISE-CONFIG-RADIO-Y").click();
        
        
    });

    function getBindConfig() {
    	$("#BIND_CONFIG_TBODY").empty();
        //BIND_CONFIG_TBODY
        $.ajax({
            url: "/bind/getPrebindAplInfoList",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : projectNo,
                FL_TY_CD : projectFlTyCd
            })
                
        }).done(function(response) {
            updateList = response;
            
            for(var ugi in updateList) {
                var updateMap = updateList[ugi];
                var $configTr = $("<tr />"); 
                
                var prjctNo = updateMap["PRJCT_NO"];
                var aplNo = updateMap["APL_NO"];
                var flNo = updateMap["FL_NO"];
                var bindWayCd = updateMap["BIND_WAY_CD"];
                var flTyCd = updateMap["FL_TY_CD"];
                var aplInfo = updateMap["APL_INFO"];
                var aplCtNm = updateMap["APL_CT_NM"];
                var aplChrg = updateMap["APL_CHRG"];
                var aplCntct = updateMap["APL_CNTCT"];
                var aplEmail = updateMap["APL_EMAIL"];
                var status = Number(updateMap["STATUS"]);
                var statusTxt = "";
                
                if( status == 900 ) {
                	statusTxt = "<spring:message code='prebindDefinition.021' text='(오류 발생)' />";
                	//statusTxt = "<font class='bg-danger disabled color-palette'><spring:message code='prebindDefinition.018' text='전처리 오류 발생' /></font>"
                } else if( status == 999 ) {
                	statusTxt = "<font class='bg-danger disabled color-palette'><spring:message code='prebindDefinition.018' text='(오류 발생)' /></font>"
                }
                
                <%-- 결합 장식 --%>
                $configTr.append(
                        "<td class='text-center'>"
                        +    "<select id='BIND_SELECT_" + ugi + "' class='form-control bind-config'>"
                        +        "<option data-prjctno='" + prjctNo + "' data-aplno='" + aplNo + "' data-flno='" + flNo + "' data-bindwaycd='" + bindWayCd + "' value='M'>기준 프로젝트</option>"
                        +        "<option data-prjctno='" + prjctNo + "' data-aplno='" + aplNo + "' data-flno='" + flNo + "' data-bindwaycd='" + bindWayCd + "' value='J'>INNER JOIN</option>"
                        +        "<option data-prjctno='" + prjctNo + "' data-aplno='" + aplNo + "' data-flno='" + flNo + "' data-bindwaycd='" + bindWayCd + "' value='L'>OUTER JOIN</option>"
                        +        "<option data-prjctno='" + prjctNo + "' data-aplno='" + aplNo + "' data-flno='" + flNo + "' data-bindwaycd='" + bindWayCd + "' value='F'>FULL JOIN</option>"
                        +    "</select>"
                        + "</td>"
                );              
                
                if(aplInfo == null) aplInfo = '';
                
                <%-- 결합 신청자 --%>
                $configTr.append("<td class='text-center _ellipsis'>" + statusTxt + "</td>");
                $configTr.append("<td class='text-center _ellipsis'>" + aplCtNm + "</td>");
                $configTr.append("<td class='text-center _ellipsis'>" + aplChrg + "</td>");
                $configTr.append("<td class='text-center _ellipsis'>" + aplCntct + "</td>");
                $configTr.append("<td class='text-center _ellipsis'>" + aplEmail + "</td>");
                
                <%-- 상세 보기 --%>
                $configTr.append("<td class='text-center'><button type='button' class='btn btn-dark' onclick='getPrebindAplInfoDetail(\"" + flNo + "\", \"" + flTyCd + "\")'><i class='fa fa-fw fa-search'></i></button></td>");
                
                $("#BIND_CONFIG_TBODY").append($configTr);
                
                $("#BIND_SELECT_" + ugi).val(bindWayCd);
                
            }
            
        });
    }
    
    function getPrebindAplInfoDetail(flNo, fileTypeCode) {
    	
        $.ajax({
            url: "/bind/getPrebindAplInfoDetail",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                FL_NO : flNo,
                FL_TY_CD : fileTypeCode
            })
                
        }).done(function(response) {

            for(var aplInfoKey in response.APL_INFO) {
            	$("#" + aplInfoKey).empty();
                $("#" + aplInfoKey).text(response.APL_INFO[aplInfoKey]);
                
                var aplInfo = response.APL_INFO[aplInfoKey];
                
                if(aplInfoKey == "HDR_CD") {
                	if(Number(aplInfo) > 0) {
                		$("#HDR_YN").text("<spring:message code='common.143' text='컬럼명 있음' />");	
                	} else {
                		$("#HDR_YN").text("<spring:message code='common.144' text='컬럼명 없음' />");		
                	}
                
                }
            }
            
            $("#COLUMN_STATUS").empty();
            
            var uplCount = response.APL_INFO["UPL_CNT"];
            var clmCount = response.CLM_CNT;
            
            if(uplCount == null) uplCount = 0;
            if(clmCount == null) clmCount = 0;
            	
            if(fileTypeCode == "K") {
	            
	            $("#COLUMN_STATUS").append("<spring:message code='common.137' text='데이터 레코드 건수' /> : <b><u>" + uplCount + "</u></b>");
            } else {
                $("#COLUMN_STATUS").append("<spring:message code='common.137' text='데이터 레코드 건수' /> : <b><u>" + uplCount + "</u></b> ");
                $("#COLUMN_STATUS").append("&nbsp;<spring:message code='common.138' text='컬럼 개수' /> : <b><u>" + clmCount + "</u></b> ");
            }
            showDataDisplay(fileTypeCode, response.COLUMN_LIST, response.DATA_LIST);
            
            $(".click-row-data").show();
        });
    }
    
    function showDataDisplay(fileTypeCode, headerList, dataList) {
    	$("#OBJECT_COLGROUP").empty();
    	$("#OBJECT_HEADER").empty();
    	$("#OBJECT_BODY").empty();
    	
    	if(fileTypeCode == "K") {
    		$("#OBJECT_HEADER").append("<tr class='th_bg'><th>결합키</th></tr>");
    	    	
    		var $dataTr;
    		
    		for( var i in dataList) {
    			$dataTr = $("<tr />");
    		    var dataMap = dataList[i]
    		    for( var key in dataMap) {
    		    	var data = dataMap[key];
    		    	$dataTr.append("<td class='_ellipsis'>" + data + "</td>");
    		    }
    		    
    		    $("#OBJECT_BODY").append($dataTr);
    		}
    		
    		
    		
    	} else {
    		
            var $headerTr = $("<tr class='th_bg'></tr>");

    		for (var hi in headerList) {
    			var headerMap = headerList[hi];
    			$headerTr.append("<th class='_ellipsis'>" + headerMap["OBJ_NM"] + "</th>");	
    		}
    		
    		$("#OBJECT_HEADER").append($headerTr);
    		
            for (var hi in dataList) {
                var dataMap = dataList[hi];
                var $dataTr = $("<tr />");    
                for( var key in dataMap) {
                	var data = dataMap[key];
                    $dataTr.append("<td class='_ellipsis'>" + data + "</td>")
                }
                
                $("#OBJECT_BODY").append($dataTr);
            }
    	}
    }
    
    function prebindConfigureSave(saveCd){
    	
    	if(!confirm("<spring:message code='prebindDefinition.016' text='모의결합을 진행하시겠습니까?' />")){
    		
    		return;
    	}
    	
    	if(projectStep == 130 || projectStep == 330) {
            if(!confirm("<spring:message code='prebindDefinition.019' text='전처리 중 오류가 발생한 데이터 가 있습니다.\n모의결합을 진행하시겠습니까?' />")){
                
                return;
            }    		
    	}
    	
        if(projectStep == 250 || projectStep == 450) {
            alert("<spring:message code='common.139' text='모의결합이 진행중인 프로젝트 입니다.' />");
            return;
        }
        
    	$("#BIND_SELECT_" + ugi + " option:selected").attr("data-aplno");
    	
    	var useMain = false;
    	
    	// Validation check
        for(var ugi in updateList) {            
        	var updateMap = new Object();
        	updateMap["APL_NO"] = $("#BIND_SELECT_" + ugi + " option:selected").attr("data-aplno");
        	updateMap["BIND_WAY_CD"] = $("#BIND_SELECT_" + ugi + " option:selected").val();
 
        	updateList[ugi] = updateMap;       	
        	
        	for(var ugj in updateList) { 
        		var updateJMap = updateList[ugj];
        		
        		if($("#BIND_SELECT_" + ugi + " option:selected").val() == "M"
        		&& $("#BIND_SELECT_" + ugj + " option:selected").val() == "M" && ugi != ugj) {
        			alert("<spring:message code='common.140' text='기준 데이터는 중복될 수 없습니다.' />");
        			return;
        		}
        		
                if($("#BIND_SELECT_" + ugi + " option:selected").val() == "M") {
                    useMain = true;
                }
        	}

        	if($("#BIND_SELECT_" + ugi + " option:selected").val() == "M") {
        		mainFlNo = $("#BIND_SELECT_" + ugi + " option:selected").attr("data-flno");
        	}
        }
              
        if(!useMain) {
            alert("<spring:message code='prebindDefinition.017' text='기준 데이터를 선택하십시오.' />");
            
            return;
        }
    	
        $.ajax({
            url: "/bind/putPrebindConfigureSave",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : projectNo,
                FL_NO : mainFlNo,
                STEP_CD : projectStep,
                SAVE_CD : saveCd,
                FL_TY_CD : projectFlTyCd,
                UPDATE_LIST : updateList
            })
                
        }).done(function(response) {
        	if(response.SUCCESS == "1") {
        		alert("<spring:message code='common.013' text='저장 되었습니다.' />");
        		
        		if(saveCd == "S") {
        			location.href = "/bind/prebindProcessing";
        		}
        	} else {
        		alert("<spring:message code='common.141' text='알 수 없는 오류입니다.' /> \n <spring:message code='common.142' text='관리자에게 문의하십시오.' />");
        		
        	}
        });    	
    }
    
</script>
</head>
<section>
    <div id="PROJECT_SELECT_DIV"></div>
    <div id="SHOW_DIV">
        
	    <div class="col-xs-12 btn-area">
	        <button id="TMP_SAVE_BTN" type="button" auth-role="200" class="btn btn-dark" onclick="prebindConfigureSave('T');">
	            <spring:message code="common.077" text="저장" />
	        </button>
	        &nbsp;
	        <button id="SAVE_BTN" type="button" auth-role="200" class="btn btn-primary" onclick="prebindConfigureSave('S');">
	            <spring:message code="common.016" text="저장 및 다음단계" />
	        </button>
	    </div>
        
	    <div class="x_panel x_result" style="display:none;">
	        <div class="x_content">
		        <div class="row">
			        <div id="KEY_CONFIG_DIV" class="col-xs-6" style="display:none;">
						<table id="KEY_CONFIG_TBL" class="table table-hover text-nowrap margin-b0">
						    <colgroup>
						        <col width="30%">
						        <col width="70%">
						    </colgroup>
						    <tbody>
				                <tr>
				                    <td><h4 class="panel-title">표본추출 설정 &nbsp;<a href="javascript:samplingTooltip.getTooltipMenu();"><i class="fa fa-question-circle fa-2x"></i></a></h4></td>
				                    <td>
				                        <input type="radio" id="SAMPLE-CONFIG-RADIO-Y" class="sample-config-radio" name="sample-config-radio" data-yn="Y"> 예
				                        &nbsp;
				                        <input type="radio" id="SAMPLE-CONFIG-RADIO-N" class="sample-config-radio" name="sample-config-radio" data-yn="N"> 아니오
				                    </td>
				                </tr>		    
						        <tr class="sample-config-tr">
						            <td><b><input id="SAMPLE_PCTV_RADIO" type="radio" class="sample-radio" name="sample-radio"> 표본추출 비율 설정</b></td>
						            <td><input id="SAMPLE_PCTV" type="text" class="form-control" placeholder='비율 입력' maxlength="3" readonly></td>
						        </tr>
				                <tr class="sample-config-tr">
				                    <td><b><input id="SAMPLE_CNT_RADIO" type="radio" class="sample-radio" name="sample-radio"> 표본추출 개수 설정</b></td>
				                    <td><input id="SAMPLE_CNT" type="text" class="form-control" placeholder='개수 입력' maxlength="20" readonly></td>
				                </tr>	
		                        <tr>
		                            <td><h4 class="panel-title">노이즈 설정 &nbsp;<a href="javascript:noiseTooltip.getTooltipMenu();"><i class="fa fa-question-circle fa-2x"></i></a></h4></td>
		                            <td>
		                                <input type="radio" id="NOISE-CONFIG-RADIO-Y" class="noise-config-radio" name="noise-config-radio" data-yn="Y"> 예
		                                &nbsp;
		                                <input type="radio" id="NOISE-CONFIG-RADIO-N" class="noise-config-radio" name="noise-config-radio" data-yn="N"> 아니오
		                            </td>
		                        </tr>   		                	        
	                            <tr class="noise-config-tr">
	                                <td><b><input id="NOISE_PCTV_RADIO" type="radio" class="noise-radio" name="noise-radio"> 노이즈 비율 설정</b></td>
	                                <td><input id="NOISE_PCTV" type="text" class="form-control" placeholder='비율 입력' maxlength="3" readonly></td>
	                            </tr>
	                            <tr class="noise-config-tr">
	                                <td><b><input id="NOISE_CNT_RADIO" type="radio" class="noise-radio" name="noise-radio"> 노이즈 개수 설정</b></td>
	                                <td><input id="NOISE_CNT" type="text" class="form-control" placeholder='개수 입력' maxlength="20" readonly></td>
	                            </tr>                		        
						    </tbody>
						</table>
					</div>
				</div>
		    </div>
		</div>
        <div class="x_panel x_result">
            <div class="x_content">		          		
				<div class="col-xs-12">
	                <div class="x_title">
	                    <h2><spring:message code="prebindDefinition.015" text="결합 정보" /></h2>
	                </div>      				
					<div id="BIND_CONFIG_TABLE_DIV" class="table-responsive">
	                    <table id="BIND_CONFIG_TABLE" class="table basic_table table-hover text-nowrap wp99">
	                       <colgroup>
	                           <col width="15%">
	                           <col width="10%">
	                           <col width="30%">
	                           <col width="10%">
	                           <col width="10%">
	                           <col width="15%">
	                           <col width="10%">
	                       </colgroup>
	                       <thead>
	                           <tr class="th_bg">
	                               <th class="text-center"><spring:message code="prebindDefinition.001" text="결합방식" /></th>
	                               <th class="text-center"><spring:message code="prebindDefinition.020" text="전처리 현황" /></th>
	                               <th class="text-center"><spring:message code="prebindDefinition.004" text="신청기관" /></th>
	                               <th class="text-center"><spring:message code="prebindDefinition.005" text="신청자" /></th>
	                               <th class="text-center"><spring:message code="prebindDefinition.006" text="연락처" /></th>
	                               <th class="text-center"><spring:message code="prebindDefinition.007" text="이메일" /></th>
	                               <th class="text-center"><spring:message code="prebindDefinition.003" text="상세 보기" /></th>
	                           </tr>
	                       </thead>
	                       <tbody id="BIND_CONFIG_TBODY">
	                       </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
        </div>
        
        <div id="BIND_INFO_DIV" class="x_panel x_result click-row-data">
            <div class="x_content">	             
	                <div class="col-xs-12" style="display:none;">
				        <div class="x_title margin_t">
				            <h2><spring:message code="prebindDefinition.002" text="결합 신청자" /></h2>
				        </div>
	
				        <table class="table basic_table table-bordered margin_b0">
				            <colgroup>
				                <col width="20%">
				                <col width="30%">
				                <col width="20%">
				                <col width="30%">
				            </colgroup>
				            <tbody>
				                <tr>
				                    <th class="th_bg"><B><spring:message code="prebindDefinition.004" text="" /></B></th>
				                    <td id="APL_CT_NM" class="txt_l _ellipsis"></td>
				                    <th class="th_bg"><B><spring:message code="prebindDefinition.005" text="" /></B></th>
				                    <td id="APL_CHRG" class="txt_l _ellipsis"></td>
				                </tr>
				                <tr>
				                    <th class="th_bg"><B /><spring:message code="prebindDefinition.006" text="" /></th>
				                    <td id="APL_CNTCT" class="txt_l _ellipsis"></td>
				                    <th class="th_bg"><B /><spring:message code="prebindDefinition.007" text="" /></th>
				                    <td id="APL_EMAIL" class="txt_l _ellipsis"></td>
				                </tr>
				            </tbody>
				        </table>	                
	                </div>
	                	                
	                <div id="BIND_FILE_INFO_DIV" class="col-xs-12 click-row-data">
					    <div class="x_title margin_t">
					        <h2><spring:message code="prebindDefinition.014" text="파일정보" /></h2>
					    </div>
					   <table class="table basic_table table-bordered">
					       <colgroup>
					           <col width="15%">
					           <col width="20%">
					           <col width="15%">
					           <col width="15%">
					           <col width="15%">
                               <col width="20%">                             
					       </colgroup>
					       <tbody>
					         <tr>
					           <th class="bg_info1"><spring:message code="prebindDefinition.008" text="업로드 파일" /></th>
					           
                               <th class="bg_info1"><spring:message code="prebindDefinition.009" text="첫번째 행" /></th>
                               
                               <th class="bg_info1"><spring:message code="prebindDefinition.010" text="필드구분자" /></th>
                               
                               <th class="bg_info1"><spring:message code="prebindDefinition.011" text="인용부호" /></th>
                                
                               <th class="bg_info1"><spring:message code="prebindDefinition.012" text="문자인코딩" /></th>
                               
                               <th class="bg_info1"><spring:message code="prebindDefinition.013" text="건수 및 개수" /></th>
                               
					         </tr>
					         
					         <tr>
					         <td id="FL_NM" class="txt_l _ellipsis"></td>
					         <td id="HDR_YN" class="txt_l _ellipsis"></td>
					         <td id="DLM_CD" class="txt_l _ellipsis"></td>
					         <td id="QTE_CD" class="txt_l _ellipsis"></td>
					         <td id="ECD_CD" class="txt_l _ellipsis"></td>
					         <td id="COLUMN_STATUS" class="txt_l _ellipsis"></td>
					         </tr>
					       </tbody>
					   </table>
					   <div id="DATA_DISPLAY" class="scroll_v">
					        <table class="table basic_table table-bordered margin_b0">
					            <colgroup id="OBJECT_COLGROUP">
					            
					            </colgroup>
					            <thead id="OBJECT_HEADER">			            
					            </thead>
					            <tbody id="OBJECT_BODY">
					            </tbody>
					        </table>
					    </div>                                        
	               </div>
				</div>
			</div>   
		</div>
	</div> 
</section>
</html>