<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
<style type="text/css">
	._ellipsis {
	    width: 200px;
	    overflow: hidden;
	    text-overflow: ellipsis;
	    white-space: nowrap;
	}
	
	.hide {
	    display:none;
	}

</style>

<script type="text/javascript" src="/js/moment.min2.js"></script>
<script type="text/javascript" src="/js/datepicker/daterangepicker.js"></script>

<script src="/js/PAMsSelectPub.js"></script>

<script type="text/javascript">
	var projectNo = null;
	var projectStep = null;
	var projectFlTyCd = null;
    var setTimeoutValue = false;
    var setTimeoutProjectNo = null;
    
    var carryoutInfo = {
    	 carryoutDe   : null
    	 ,privacyDtl  : null
    	 ,prjctDtl    : null
    	 ,customerDtl : null
    	 ,howDtl      : null
    	 ,useDtl      : null
    	 ,customerDtl : null
    	 ,carryoutDtl : null
    }
    
    $(function () {
 	   // style
 	   $("#KEY_CONFIG_TBL").find("td").css("border-top", "#FFF");
 	   $("#SHOW_DIV").hide();
 	   $(".btn-area").hide();
 	   
       $.ajax({
           url: "/common/popupAttachFile"
           ,type : "get",
       }).done(function(response) {
           $("body").append(response);
       });
         
       $('#CARRYOUT_DE').daterangepicker({
           singleDatePicker: true,
           calender_style: "picker_3"
       }, function (start, end, label) {
       });
       
       loadPAMsProjectSelect("PROJECT_SELECT_DIV", 250);
         
       $("#PAMS_PROJECT_SELECT_BTN").on("click",function() {
           
           if ($("#PAMS_PROJECT_SELECT").val() == null || $("#PAMS_PROJECT_SELECT").val() == "") {
               alert('<spring:message code="common.038" text="프로젝트를 선택하세요." />');
               
           } else {
           	$("#SHOW_DIV").show();
           	
               projectNo = $("#PAMS_PROJECT_SELECT").val();
               projectStep = $("#PAMS_PROJECT_SELECT option:checked").attr("step");
               
               if(Number(projectStep) < 400) {
                   projectFlTyCd = "K"
               } else {
                   projectFlTyCd = "D"
               }
               
               setProjectProgressBar();
           }
       });
    })	
   
    function getProjectData() {
		   
        $.ajax({
            url: "/bind/getPrebindProcessingInfo",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : projectNo,
                FL_TY_CD : 'K',
                STEP_CD  : projectStep
            })
                
        }).done(function(response) {
        	$(".new-tr").remove();
        	$(".input-val").val("");
        	
        	var bindInfoList = response.BIND_INFO_LIST;
        	var projectInfoMap = response.PRJCT_INFO;
        	
        	if(Number(projectInfoMap.STEP_CD) == 500) {
        		getBindDataFileInfo();	
        	}
        	
        	var noiseSampleConfig = response.NOISE_SAMPLE_CONFIG;
        	
        	if(bindInfoList.length == 0) {
        		return;
        	}
        	
        	for(var bi = 0; bi < bindInfoList.length ; bi++) {
        		var bindInfoMap = bindInfoList[bi];
        		var $databody = $("<tr class='new-tr'/>");
        		var btnMsg;
        		var errMsg = currentFileStatus(noiseSampleConfig, bindInfoMap);

        		carryoutInfo.prjctDtl = bindInfoMap["PRJCT_DTL"];
        		
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["BIND_WAY"] + "</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["APL_CT_NM"] + "</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["BIND_PCTV"] + "%</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["BIND_CNT"] + "</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["SAMPLE_CNT"] + "</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["NOISE_CNT"] + "</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["BIND_PCTV"] + "%</td>");
        		$databody.append("<td class='_ellipsis'>" + bindInfoMap["BIND_CNT"] + "</td>");
                $databody.append("<td class='new-td _ellipsis'>" + "<button type='button' class='btn btn-success' onclick='openBindDataFileModal(\"" + projectNo + "\", \"" + bindInfoMap["FL_NO"] + "\", \"" + bindInfoMap["FL_TY_CD"] + "\", \"" + errMsg + "\" )' ><spring:message code='combinedProcessing.014' text='결합키 다운로드' /></button>" + "</td>");
                
        		$("#BIND_RESULT_TBODY").append($databody);
        	}
        	
            //FL_TY_CD = "D"
            if(projectFlTyCd == "D" && response.MAIN_DATA_INFO)  {
            	
                $("#DATA_DOWN_BTN").attr("onclick", "openBindDataFileModal(\"" + projectNo + "\", \"" + response.MAIN_DATA_INFO["FL_NO"] + "\", \"" + projectFlTyCd + "\", \"\" )");            
            }
        	
        })
                
	}
	   
	function setProjectProgressBar() {
	    
		$.ajax({
            url: "/bind/getProjectStep",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : projectNo
            }),
            beforeSend:function() {
	            if(!this.isHideOverlay){
	                _overlay.end();
	            }
	        },
	        success:function(response){
	            
	            if(!setTimeoutValue) {
	                setTimeoutProjectNo = Number(projectNo);
	                setTimeoutValue = setTimeout(function() {
	                    setTimeoutValue = false;
	                        if( Number(projectNo) == setTimeoutProjectNo && (Number(response.STEP_CD) == 250 || Number(response.STEP_CD) == 450) ) {
	                            setProjectProgressBar(projectNo);
	                        }
	                        
	                 }, 1000 * 10);
	            }
	            
	            switch (response.STEP_CD) {
	            
		            case '230':
		            	$("#PROGRESS_BAR_TITLE").text('<spring:message code="common.135" text="결합처리 진행 오류" />');
		            	$("#PROGRESS_BAR").removeClass("active");   
		            	
		            	$("#BIND_RESULT_DIV").hide();
		                break;
		            case '250':
		            	$("#PROGRESS_BAR_TITLE").text('<spring:message code="common.134" text="결합처리 요청" />');
		            	$("#PROGRESS_BAR").addClass("active");
		            	
		            	$("#BIND_RESULT_DIV").hide();
		                break;
		            case '430':
		                $("#PROGRESS_BAR_TITLE").text('<spring:message code="common.135" text="결합처리 진행 오류" />');
		                $("#PROGRESS_BAR").removeClass("active");
		                
		                getProjectData();
		                $("#BIND_RESULT_DIV").show();
		                break;
	                case '450':
	                	$("#PROGRESS_BAR_TITLE").text('<spring:message code="common.134" text="결합처리 요청" />');
	                	$("#PROGRESS_BAR").addClass("active");
	                    
	                	getProjectData();
	                	$("#BIND_RESULT_DIV").hide();
	                    $("#BIND_RESULT_DIV").show();
	                    break;	                
		            default:
		            	$("#PROGRESS_BAR_TITLE").text('<spring:message code="common.136" text="결합처리 완료" />');
		                $("#PROGRESS_BAR").removeClass("active");       
		                
		                getProjectData();
		                $("#BIND_RESULT_DIV").show();
		                
		                if(response.STEP_CD == "500") {
		                    <%-- TODO 반출내역 필요시
		                    $("#DATA_FORM_DIV").show();
		                    --%>
		                } else {
		                	$("#DATA_FORM_DIV").hide();
		                }
		                
		                break;
	            }            
	        },
	        complete:function() {
	            
	        },
	        error:function() {
	                  
	        }
	    });
	}	
	
	// 현재 파일의 노이즈 샘플 상태에 따른 errMsg 반환
	function currentFileStatus(configMap, fileMap) {
		var errMsg = "";
		
		var noiseTy = configMap["NOISE_TY"];
		var noiseVal = Number(configMap["NOISE_VAL"]);
		var noisePercent = Number(configMap["NOISE_PERCENT"]);
        var sampleTy = configMap["SAMPLE_TY"];
        var sampleVal = Number(configMap["SAMPLE_VAL"]);
        var samplePercent = Number(configMap["SAMPLE_PERCENT"]); 
		
        var noiseCnt = Number(fileMap["NOISE_CNT"]);
        var sampleCnt = Number(fileMap["SAMPLE_CNT"]);
        var uploadCnt = Number(fileMap["UPLOAD_CNT"]);
        
        var noisePercentCnt = uploadCnt*noisePercent;
        var samplePercentCnt = uploadCnt*samplePercent;
        
        if(noiseTy == "C" && noiseCnt < noiseVal) {
        	errMsg += "<spring:message code='prebindProcessing.001' text='표본이나 잡음이 권장되지 않는 값' />"
        	        + "(" + noiseCnt + ")"
                    + " <spring:message code='prebindProcessing.002' text='입니다.' />";
        } else if (noiseTy == "P" && noiseCnt < noisePercentCnt) {
            errMsg += "<spring:message code='prebindProcessing.001' text='표본이나 잡음이 권장되지 않는 값' />"
                    + "(" + noiseCnt + ")"
                    + " <spring:message code='prebindProcessing.002' text='입니다.' />";
        }
        
        if(sampleTy == "C" && sampleCnt < sampleVal) {
            errMsg += "<spring:message code='prebindProcessing.001' text='표본이나 잡음이 권장되지 않는 값' />"
                    + "(" + sampleCnt + ")"
                    + " <spring:message code='prebindProcessing.002' text='입니다.' />";
        } else if (sampleTy == "P" && sampleCnt < samplePercentCnt) {
            errMsg += "<spring:message code='prebindProcessing.001' text='표본이나 잡음이 권장되지 않는 값' />"
                    + "(" + sampleCnt + ")"
                    + " <spring:message code='prebindProcessing.002' text='입니다.' />";
            
        	<%-- 
        	errMsg += " - " 
        		   + "<spring:message code='prebindProcessing.002' text='권장 표본 수' />" + "(" + samplePercentCnt + ")" 
        		   + "<spring:message code='prebindProcessing.004' text='보다 생성된 표본 수' />" + "(" + sampleCnt + ")"
        		   + "<spring:message code='prebindProcessing.005' text='가 적습니다.' />\n";
        	--%>        	
        }

        return errMsg;
	}
	
	function getBindDataFileInfo() {
        $.ajax({
            url: "/bind/getBindDataFileInfo",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : projectNo
            })
                
        }).done(function(response) {
        	var privacyDtlList = response.PRIVACY_DTL;
        	
        	$("#PRIVACY_DTL").empty();
        	
        	for( var i in privacyDtlList) {
        		if(i != 0) {
        			$("#PRIVACY_DTL").append(", ");
        		}
        		var privacyDtlMap = privacyDtlList[i];
        		$("#PRIVACY_DTL").append(privacyDtlMap["OBJ_NM"]);	
        	}
        	
            $("#CARRYOUT_DE").val(response.NOW_DATE);
            $("#PRJCT_DTL").text(response.PRJCT_DTL);
            $("#COL_DOWNLOAD_SPAN").text(response.BIND_FL_INFO["DOWN_FL_NM"]);
            
            carryoutInfo.privacyDtl = $("#PRIVACY_DTL").text();
        });
	}
	
	function openBindDataFileModal(prjctNo, flNo, flTyCd, errMsg) {

        if(errMsg != null && errMsg != "") {
            <%-- var fileErrMsg = "<spring:message code='common.133' text='현재 아래와 같은 주의사항이 존재합니다.' />\n" --%>
            
            var fileErrMsg = errMsg;
            alert(fileErrMsg);
        }
        
		if(flTyCd == "K") {
            carryoutInfo.carryoutDe  = null;
            carryoutInfo.useDtl      = null;
            carryoutInfo.howDtl      = null;
            carryoutInfo.customerDtl = null;
            carryoutInfo.carryoutDtl = null;
            
            carryoutInfo.privacyDtl  =  "<spring:message code='common.042' text='결합키' />";
		} else if(flTyCd == 'D') {
			
	        if( _required.requiredCheck($("#DATA_FORM_DIV")) > 0 ) {
	        	
	            return false;
	        }
	        carryoutInfo.carryoutDe = $("#CARRYOUT_DE").val();
            carryoutInfo.privacyDtl =  $("#PRIVACY_DTL").text();
            <%-- TODO 반출내역 필요시
	        carryoutInfo.useDtl      = $("#USE_DTL").val();
	        carryoutInfo.howDtl      = $("#HOW_DTL").val();
	        carryoutInfo.customerDtl = $("#CUSTOMER_DTL").val();
	        carryoutInfo.carryoutDtl = $("#CARRYOUT_DTL").val();
	        --%>			
		} 
        
        loadlAttachFilePopup(prjctNo, flNo, flTyCd, carryoutInfo);
        
        $("#MODAL_ATTACH_FILE").modal();	
	}
</script>
</head>
<section>
    <div id="PROJECT_SELECT_DIV"></div>
    
    <div id="SHOW_DIV">
        <div class="col-xs-12 btn-area">
	        
	    </div>
        <div id="BIND_PROGRESS_DIV" class="x_panel x_result">
            <div class="row">
                <div class="col-xs-12">
                    <div class="x_title">
                        <h2><spring:message code="prebindProcessing.006" text="결합 진행 상태" /></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12">
                    <p class="sub_info">
                        <spring:message code="prebindProcessing.007" text="모의결합 담당자는 결합 정의에 따라 결합을 수행합니다. ※ 대량의 데이터의 경우 많은 시간이 소요될 수 있습니다." />
                        <br><br>
                    </p>
                </div>
            </div>
            <div class="x_content">
                <div class="col-xs-12">
                    <div class="progress-area">
                        <div class="progress progress_sm">
                            <div id="PROGRESS_BAR" class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                <span class="sr-only"></span>
                            </div>
                        </div>
                        <p id="PROGRESS_BAR_TITLE" class="txt_c"><br></p>
                    </div>
                </div>
            </div>
        </div>
	    <div id="BIND_RESULT_DIV" class="x_panel x_result" style="display:none;">
	        <div class="x_content">
	            <div class="row">
	                <div class="col-xs-12 click-row-data">
	                    <div class="x_title margin_t"><h2><spring:message code="prebindProcessing.008" text="결합 신청자" /></h2></div>
	                    <div id="BIND_RESULT_TABLE_DIV">
		                    <table class="table basic_table table-hover text-nowrap wp99">
		                        <colgroup id="BIND_RESULT_COL_GROUP"></colgroup>
		                        <tbody id="BIND_RESULT_TBODY">
		                            <tr id="BIND_RESULT_HEADER">
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.009' text='결합 방식' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.025' text='결합 신청자' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.011' text='키 결합률' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.010' text='키 결합건수' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.012' text='키 표본' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.013' text='키 잡음' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.013' text='데이터 결합률' /></B></th>
		                                <th class="th_bg"><B><spring:message code='prebindProcessing.013' text='데이터 결합건수' /></B></th>
		                                <th id='KEY_HEADER' class='th_bg new-td'><B><spring:message code='prebindProcessing.014' text='결합키 다운로드' /></B></th>
		                            </tr>
		                        </tbody>
		                    </table>    
	                    </div>                
	                </div>
	            </div>
	        </div>   
	    </div>
	    <div id="DATA_FORM_DIV" class="x_panel x_result" style="display:none;">
		    <div class="x_content" id="frmDiv">
	                <div class="x_title">
	                    <h2 style="width:20%;"><spring:message code='prebindProcessing.015' text='가명정보 제공기록' /></h2>
	                </div>
	                
	                <input type="hidden" id="DATA_CAUSE" class="prjct-cause initClear" />
	                <input type="hidden" id="DATA_PRIVACY" class="prjct-privacy initClear"  />
	                
	                <div class="x_content">
	                    <table class="table basic_table table-bordered margin_b0">
	                        <colgroup>
	                            <col width="20%;">
	                            <col width="*">
	                        </colgroup>
	                        <tbody>
	                            <tr>
	                                <th class="th_bg"><b><spring:message code='prebindProcessing.016' text='가명정보 제공일시' />(*)</b></th>
	                                <td>
	                                    <div id="reservationdate" class="input-group date" style="width:150px;display:flex;">
	                                        <input id="CARRYOUT_DE" class="input-val  form-control datetimepicker-input hasDatepicker initClear" type="text" name="CARRY_DE" title="<spring:message code='prebindProcessing.016' text='가명정보 제공일시' />" required>
	                                        <div id="CARRYOUT_DE_BTN" class="input-group-append" onclick='$("#CARRYOUT_DE").focus();' >
	                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
	                                        </div>
	                                    </div>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.017' text='가명정보 항목' /></b>
	                                </th>
	                                <td id="PRIVACY_DTL" class="txt_l">
	                                    
	                                </td>
	                            </tr>
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.018' text='가명정보 처리의 목적 ' /></b>
	                                </th>
	                                <td id="PRJCT_DTL" class="txt_l">
	                                    
	                                </td>
	                            </tr>
	                            <%-- TODO 반출내역 필요시
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.019' text='가명정보의 이용내역 ' />(*)</b>
	                                </th>
	                                <td>
	                                    <textarea id="USE_DTL" class="input-val form-control" rows="2" placeholder="<spring:message code='prebindProcessing.019' text='가명정보의 이용내역 ' /> :" title="<spring:message code='prebindProcessing.019' text='가명정보의 이용내역 ' /> :" required></textarea>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.020' text='가명정보 제공방법' />(*)</b>
	                                </th>
	                                <td>
	                                    <textarea id="HOW_DTL" class="input-val form-control" rows="2" placeholder="<spring:message code='prebindProcessing.020' text='가명정보 제공방법' /> :" title="<spring:message code='prebindProcessing.020' text='가명정보 제공방법' />" required></textarea>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.021' text='제3자 제공시 제공받는자' />(*)</b>
	                                </th>
	                                <td>
	                                    <textarea id="CUSTOMER_DTL" class="input-val form-control" rows="2" placeholder="<spring:message code='prebindProcessing.021' text='제3자 제공시 제공받는자' /> :" title="<spring:message code='prebindProcessing.021' text='제3자 제공시 제공받는자' />" required></textarea>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.022' text='반출 사유'  />(*)</b>
	                                </th>
	                                <td colspan="3">
	                                    <textarea id="CARRYOUT_DTL" class="input-val show-data form-control" rows="2" placeholder="<spring:message code='prebindProcessing.022' text='반출 사유'  /> :" title="<spring:message code='prebindProcessing.022' text='반출 사유'  />" required></textarea>
	                                </td>
	                            </tr>
	                            <tr>
	                                <th class="th_bg">
	                                    <b><spring:message code='prebindProcessing.023' text='가명처리 파일 다운로드' /></b>
	                                </th>
	                                <td colspan="3">
	                                    <div class="col-xs-10 col-download">
	                                       <span id="COL_DOWNLOAD_SPAN" style="height:100%;"></span>
	                                   </div>
	                                    <div class="col-xs-2 text-center">
	                                        <button id="DATA_DOWN_BTN" type="button" class="btn btn-primary">
                                                <spring:message code='prebindProcessing.024' text='결합데이터 다운로드' />
                                            </button>
	                                    </div>	                                    
	                                </td>
	                            </tr>
	                             --%>
	                        </tbody>
	                    </table>
	                </div>
	                <br/>
	            </div>
            </div>
	    </div>
	</section>
</html>