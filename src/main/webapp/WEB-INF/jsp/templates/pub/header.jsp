<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>     
<meta charset="UTF-8">
<title>
<c:choose>
	<c:when test="${not empty MENU_INFO}"> 
	${MENU_INFO[MENU_INFO.size()-1].MENU_NM}
	</c:when>
	<c:otherwise> 
	PAMS 
	</c:otherwise>
</c:choose>
</title>
<!-- Bootstrap core CSS -->


<link rel="stylesheet" href="/fonts/css/font-awesome.min.css">
<link rel="stylesheet" href="/css/animate.min.css">
<link rel="stylesheet" href="/css/bootstrap.min.css">

<!-- ion_range -->
<link rel="stylesheet" href="/css/normalize.css" />
<link rel="stylesheet" href="/css/ion.rangeSlider.css" />
<link rel="stylesheet" href="/css/ion.rangeSlider.skinFlat.css" />

<!-- Custom styling plus plugins -->
<link rel="stylesheet" href="/css/custom.css">
<link rel="stylesheet" href="/css/icheck/flat/green.css">
<link rel="stylesheet" href="/plugins/jsgrid/jsgrid.min.css">
<link rel="stylesheet" href="/plugins/jsgrid/jsgrid-theme.min.css">
<link rel="stylesheet" href="/js/jstree-3.3.11/themes/default/style.css" > 
<link rel="stylesheet" href="/css/progressbar.css">
<link rel="stylesheet" href="/css/default.css">
<link rel="stylesheet" href="/css/common.css">

<script src="/plugins/jquery/jquery-3.6.0.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/plugins/jsgrid/jsgrid.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/PAMsFilePub.js"></script>
<script src="/js/jstree-3.3.11/jstree.min.js"></script>
<script src="/js/html2canvas/html2canvas.min.js"></script>
<script src="/js/jspdf/jspdf.2.3.1.umd.min.js"></script>

<!-- chart js -->
<script src="/js/chartjs/Chart.js"></script>
<script src="/js/chartjs/Chart.BoxPlot.PAMs.js"></script>

<!-- bootstrap progress js -->
<script src="/js/progressbar/bootstrap-progressbar.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>

<!-- icheck -->
<script src="/js/icheck/icheck.min.js"></script>

<!-- range slider -->
<script src="/js/ion_range/ion.rangeSlider.min.js"></script>

<script src="/js/common.js"></script>
<script src="/js/format.js"></script>
<script src="/js/custom.js"></script>

<link href="/css/tom-select.bootstrap4.css" rel="stylesheet">
<link href="/css/tom-select.bootstrap4.min.css" rel="stylesheet">
<script src="/js/tom-select.complete.min.js"></script>

<%--권한 관련 버튼 컨트롤--%>
<style type="text/css">  
    <%-- TODO 권한 기능 추가 시 주석 해제
	<c:if test="${'Y' != IS_SYS_ADMIN}"> 
		[auth-role] {
			display : none;
		}
	</c:if>
	<c:forEach items="${AUTH_CD.split(',')}" var="authCd">
		[auth-role="${authCd}"]{
			display : inline-block;
		}
	</c:forEach>
	--%>
</style>    

<script type="text/javascript">
$(function(){
	<%-- 모달 팝업창 종료 시 강제 패딩 현상 임시 수정 --%>
    $(".nav-md").on("click", function(){
        $(".nav-md").css("padding-right", "0px");
    });
	
	var auCon = {
		init : function(){
			if("Y" != "${IS_SYS_ADMIN}"){
			    <%-- NOT SYSADMIN IS WORK --%>
				var auList = "${AUTH_CD}".split(",");
				$("[auth-role]").each(function(){
					var role=$(this).attr("auth-role");
					if(auList.indexOf(role) < 0){ 
						$(this).remove();  
					}
				})
			}			
		}
	}	
	auCon.init();
	
});
</script>
<%--//권한 관련 버튼 컨트롤--%>
