<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<script type="text/javascript">
$(function(){
	$.ajax({
		url : "/common/getUserName",
		type : "post",
		contentType : 'application/json',
		data : JSON.stringify({})
	}).done(function(response) {
		$("#TXT_USR_NM").text(response.USR_NM);
	});	
})
</script>    
<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav class="" role="navigation">
      <div class="nav toggle"> <%-- <a id="menu_toggle"><i class="fa fa-bars"></i></a> --%> <a href="/user/myInfo"><i class="fa fa-home"></i></a> </div>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="/user/logout"><span class="hidden-xs" id="TXT_USR_NM"></span>님  Log-out <i class="fa fa-sign-out"></i></a> </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->