<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	//화면 이동시 로그에 남길 프로젝트 번호 초기화 처리
	session.removeAttribute("SEL_PRO_NO");
%>

<script type="text/javascript">
	var _menuIcon = {
		common : "fa-maxcdn",
		user : "fa-user",
		advancePreparation : "fa-file-text-o",
		processing : "fa-pencil-square-o",
		additionalWork : "fa-clipboard",
		bind : "fa-plus",
		carryout : "fa-share",
		creationStage : "fa-laptop",
		admin : "fa-male"
	}
	
	var roleConfirm = "${ROLE_LIST}";
	
	$(function(){
		<%--사용자 메뉴 세팅--%>	
		$.ajax({
			url : "/common/getUserMenuList",
			type : "post",
			contentType : 'application/json'
		}).done(function(res){
			var wapper = $("<li>")
			var upperMenu = $("<a>",{
				"class" : "parent_a",
				"data-seq" : ""
			})
			var lowerMenuWapper = $("<ul>",{
				"class" : "nav child_menu",
				"style" : "height:auto; display: none;"
			})
			var lowerMenu = $("<li>",{
				"html" : "<a style='width:auto; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;' href='' class='nav-link'></a>"
			})
			var menuBlock = null;
			var lowerMenuBlock = null;
			var subCnt = 0;
			var upperSeq = null;
			
			for(idx in res){
				var menu = res[idx];		

				<%--메뉴 DEPTH가 최상위일 경우 --%>
				if(menu.MENU_DEPTH == 1){				
					if(lowerMenuBlock !=null){
						$(menuBlock).append($(lowerMenuBlock).clone());
						lowerMenuBlock = null;
					}
				    
					if(menuBlock != null){
						if(subCnt > 0)
							$(".side-menu").append($(menuBlock).clone()); 
						subCnt = 0;
					}
					
					menuBlock = $(wapper).clone();
					
					$(menuBlock).append($(upperMenu).clone());
					$(menuBlock).find("a").attr("data-upper", menu.MENU_ID);
					$(menuBlock).find("a").html('<span style="font-size: 13px; display: inline-block; width:80%; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;"> <i class="fa '+_menuIcon[menu.MENU_ID]+'"></i>'+menu.MENU_NM+'</span><span class="fa fa-chevron-down"></span>');
					$(menuBlock).find("a").attr('title', menu.MENU_NM);
					
					upperSeq = menu.SEQ
				} else {
				<%--현재 메뉴 구성이 최대 2Depth 차후 Depth 추가시 수정 필요.--%>
					if(lowerMenuBlock == null){
						lowerMenuBlock = $(lowerMenuWapper).clone();
						<%--lowerMenuWapper.addClass(getIconInfo(menu));--%>
					}
					if(menuBlock != null && menu.UPPER_MENU_SEQ == upperSeq){
						var lowerMenuTemp = $(lowerMenu).clone();
						$(lowerMenuTemp).find(".nav-link").each(function(){
							$(this).attr("href",menu.MENU_URL);
							$(this).attr("data-id", menu.MENU_ID);
							$(this).html(menu.MENU_NM);
						})
						subCnt++;
						lowerMenuBlock.append(lowerMenuTemp.clone()); 
					}
				}
				if(idx == res.length-1 && subCnt > 0){

					if(lowerMenuBlock !=null){
						$(menuBlock).append($(lowerMenuBlock).clone());
						lowerMenuBlock = null;
					}
					$(".side-menu").append($(menuBlock).clone()); 
				}
			}
			
			$('#sidebar-menu li ul').slideUp();
		    $('#sidebar-menu li').removeClass('active');
		
		    $('.parent_a').click(function () {
		    	var p = $(this).parent();
		        if ($(p).is('.active')) {
		            $(p).removeClass('active');
		            $('ul', p).slideUp();
		            //$(p).removeClass('nv');
		            //$(p).addClass('vn');
		        } else {
		        	if(!$(p).hasClass('active')) {
		        		$('#sidebar-menu li ul').slideUp();
			            //$(p).removeClass('vn');
			            //$(p).addClass('nv');
			            $('ul', p).slideDown();
			            $('#sidebar-menu li').removeClass('active');
			            $(p).addClass('active');
		        	}
		        }
		    });
		  
		
		    $('#menu_toggle').click(function () {
		        if ($('body').hasClass('nav-md')) {
		            $('body').removeClass('nav-md');
		            $('body').addClass('nav-sm');
		            $('.left_col').removeClass('scroll-view');
		            $('.left_col').removeAttr('style');
		            $('.sidebar-footer').hide();
		
		            if ($('#sidebar-menu li').hasClass('active')) {
		                $('#sidebar-menu li.active').addClass('active-sm');
		                $('#sidebar-menu li.active').removeClass('active');
		            }
		        } else {
		            $('body').removeClass('nav-sm');
		            $('body').addClass('nav-md');
		            $('.sidebar-footer').show();
		
		            if ($('#sidebar-menu li').hasClass('active-sm')) {
		                $('#sidebar-menu li.active-sm').addClass('active');
		                $('#sidebar-menu li.active-sm').removeClass('active-sm');
		            }
		        }
		    });
		    
		    var url = window.location;
		    $('#sidebar-menu a[href="' + url + '"]').parent('li').addClass('current-page');
		    $('#sidebar-menu a').filter(function () {
		        return this.href == url.href.split("?")[0];
		    }).parent('li').addClass('current-page').parent('ul').slideDown().parent().addClass('active');
		    
		    $('.user_name').html("<%=(String)session.getAttribute("USR_ID")%>");
		    
		    var roleList = "<%=session.getAttribute("ROLE_LIST_MAPPED")%>"
		    var roleListLabel = roleList.substring(1, roleList.length-1);
		    
		    $('.user_role').html(roleListLabel);
		    $('.user_role').attr('title', roleListLabel);
		    
		    $('#userProfile').click(function () {
		    	window.location.href = "/user/myInfo";
		    });
			
		})
		
	})


</script>

<div class="col-md-3 left_col">
	<div class="left_col scroll-view">
		<div class="navbar nav_title"> <a href="/user/myInfo"><img src="../images/logo.svg" alt="logo" class="logo_pa"></a></div>
		<div class="clearfix"></div>
    
		<!-- menu prile quick info -->
		<div class="profile" id="userProfile">
			<div class="profile_pic"> <img src="/images/user.png" alt="..." class="img-circle profile_img"> </div>
			<div class="profile_info">
				<span class="user_role" style="width:auto; display:block; overflow:hidden; text-overflow:ellipsis; white-space:nowrap;"></span>
				<h2 class="user_name"></h2>
			</div>
		</div>
		<!-- /menu prile quick info --> 

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section"> 
				<!--<h3>Super</h3>-->
				<ul class="nav side-menu">
					<li>
						<a href="/user/myInfo">
						    <i class="fa fa-user" aria-hidden="true"></i>
							마이페이지
						</a>
					</li>
					<%-- 스크립트로 추가 --%>
				</ul>
			</div>
		</div>
		<!-- /sidebar menu --> 
	</div>
</div>