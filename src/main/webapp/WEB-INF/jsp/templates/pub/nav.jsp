<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>     
<section>
	<div class="page-title">
		<div class="title_left">
			<h3>
			<b>
			<c:choose>
				<c:when test="${not empty MENU_INFO}">
				${MENU_INFO[MENU_INFO.size()-1].MENU_NM}
				</c:when>
				<c:otherwise>
				</c:otherwise>
			</c:choose>
			</b>
			</h3>
		</div>
	</div>
	<div class="clearfix"></div>
</section>