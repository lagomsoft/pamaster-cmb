<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>   
<%@ taglib prefix="tiles"  uri="http://tiles.apache.org/tags-tiles" %> 
<!DOCTYPE html>
<html>
<%@page import="kr.co.pamaster.util.CheckLicense"%>
<%
//라이센스 체크 TODO
CheckLicense check = new CheckLicense();

boolean isPass = true;

if(check.checkTime()){

	String checkCode = check.checkLicense();
	
	if(!"200".equals(checkCode)) {	
		isPass = true;
	}
}

if(!isPass){
	
%>
<body>	
	License Check
</body>
<%
}else{
%>
<head>
	<tiles:insertAttribute name="header"/>	
</head>
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<tiles:insertAttribute name="top"/>
			<tiles:insertAttribute name="left"/>
			<div class="right_col" role="main">
				<tiles:insertAttribute name="nav"/>
				<tiles:insertAttribute name="body"/>
			</div>
		</div>
	</div>
	<div id="custom_notifications" class="custom-notifications dsp_none">
		<ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
		</ul>
		<div class="clearfix"></div>
		<div id="notif-group" class="tabbed_notifications"></div>
	</div>
	<!-- footer content -->
	<tiles:insertAttribute name="footer"/>
	<!-- /footer content -->
</body>
<%

}
%>
</html>
</html>