<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

	<script type="text/javascript">
		//js 에서 사용할 메세지 세팅
		window.pamsMsg = {
					 PAMS_FILE_UPLOAD :  '<spring:message code="common.021" text="클릭하거나 마우스로 파일을 끌어오세요" />' 
					,PAMS_FILE_NOFILE : '<spring:message code="common.006" text="파일을 업로드 하세요." />'
					,PAMS_FILE_MAX_COUNT_OVER : '<spring:message code="common.022" text="최대 업로드 가능한 파일 수를 초과" />'
					,PAMS_FILE_MAX_COUNT : '<spring:message code="common.023" text="최대 업로드 파일 수" />'
					,PAMS_FILE_MAX_SIZE_OVER : '<spring:message code="common.024" text="총 용량 초과" />'
					,PAMS_FILE_MAX_SIZE : '<spring:message code="common.025" text="총 업로드 가능 용량" />'
					,PAMS_FILE_TYPE : '<spring:message code="common.026" text="해당 확장자 파일만 업로드 가능합니다." />'
					,PAMS_FILE_NOTYPE : '<spring:message code="common.027" text="등록 불가 확장자" />'
					,PAMS_FILE_SIZE_OVER : '<spring:message code="common.028" text="용량 초과" />'
					,PAMS_FILE_DELETE : '<spring:message code="common.029" text="삭제" />'
					,PAMS_FILE_DOWNLOAD : '<spring:message code="common.030" text="다운로드" />'
					,PAMS_SELECT_ALL_CHECK : '<spring:message code="common.034" text="전체" />'
					,PAMS_SELECT : '<spring:message code="common.035" text="선택" />'
					,PAMS_NO_SELECT : '<spring:message code="common.062" text="프로젝트가 존재하지 않습니다." />'
					,PAMS_SELECT_SEARCH : '<spring:message code="common.036" text="조회" />'
					,PAMS_SELECT_TITLE : '<spring:message code="common.037" text="프로젝트 선택" />'
					,PAMS_CHART_MIN : '<spring:message code="common.055" text="최소값" />'
					,PAMS_CHART_MAX : '<spring:message code="common.056" text="최대값" />'
					,PAMS_CHART_LOWQV : '<spring:message code="common.059" text="Q1" />'
					,PAMS_CHART_UPPQV : '<spring:message code="common.060" text="Q3" />'
					,PAMS_CHART_MEDIV : '<spring:message code="common.057" text="중앙값" />'
					,PAMS_CHART_AVGV : '<spring:message code="common.058" text="평균값" />'
		};
		
		var fileSettingTime = "<%=session.getAttribute("FILE_SETTING_TIME")%>";

		var cmmUploadSize = 0;
		var cmmMaxUploadSize = 0;
		var cmmMaxFileCount = 0;
		
		var cmmPamsFileType = [];
		var cmmPamsImprtyFileType = [];
		
		var nowDate = new Date();
		
		if(fileSettingTime && fileSettingTime != "null" && nowDate.getTime() > Number(fileSettingTime) ) {
			
			var cmmFileType =  "<%=session.getAttribute("CMM_FILE_TYPE")%>";
			var cmmImprtyFileType = "<%=session.getAttribute("CMM_IMPRTY_FILE_TYPE")%>";
			
			cmmUploadSize = Number("<%=session.getAttribute("CMM_ONE_FILE_SIZE")%>");
			cmmMaxUploadSize = Number("<%=session.getAttribute("CMM_ALL_FILE_SIZE")%>");
			cmmMaxFileCount = Number("<%=session.getAttribute("CMM_FILE_CNT")%>");
			
			if(cmmFileType){
				cmmPamsFileType = cmmFileType.split(',');				
			}
			
			if(cmmImprtyFileType){
				cmmPamsImprtyFileType = cmmImprtyFileType.split(',');				
			}
		}else{
			$.ajax({
	            url: "/common/getFileSettingList",
	            type : "post",
	            async: false,
	            contentType: 'application/json',
	            data : JSON.stringify({})
	        }).done(function(response) {
	        	
	        	cmmUploadSize = Number(response.FILE_SETTING_LIST.CMM_ONE_FILE_SIZE);
	        	cmmMaxUploadSize = Number(response.FILE_SETTING_LIST.CMM_ALL_FILE_SIZE);
	        	cmmMaxFileCount = Number(response.FILE_SETTING_LIST.CMM_FILE_CNT);
	        	
	        	var cmmFileType = response.FILE_SETTING_LIST.CMM_FILE_TYPE;
	        	var cmmImprtyFileType = response.FILE_SETTING_LIST.CMM_IMPRTY_FILE_TYPE;
	        	
	        	if(cmmFileType){
	        		cmmPamsFileType = cmmFileType.split(',');
	        	}
	        	
	        	if(cmmImprtyFileType){
	        		cmmPamsImprtyFileType = cmmImprtyFileType.split(',');
	        	}
	        	
	        });
			
		}
		
		window.pamsFileSetting = {
				 uploadSize 	: cmmUploadSize
				,maxUploadSize 	: cmmMaxUploadSize
				,maxFileCount 	: cmmMaxFileCount
				,pamsFileType 	: cmmPamsFileType
				,pamsImprtyFileType : cmmPamsImprtyFileType
		}
	</script>
<!-- footer content -->
<footer> <span>Copyright ⓒ 2020 ~ 2021 PAmaster. All rights reserved.</span> </footer>
<!-- /footer content -->