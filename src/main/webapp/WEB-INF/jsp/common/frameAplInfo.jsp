<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<style type="text/css">
	._ellipsis {
	      overflow:hidden;
	      text-overflow:ellipsis;
	      white-space:nowrap;
	}

</style>

<script type="text/javascript">
	
	var _aplInfo = {
         projectNo : null
        ,aplNo : null
        ,stepCd : null
	}	
	
    $(function () {
        _aplInfo.projectNo = ${PRO_NO};
        _aplInfo.aplNo = ${APL_NO};
        _aplInfo.stepCd = ${STEP_CD};
        
        $tomSelect.addItem(${PRO_NO}, true);
        if(Number(_aplInfo.stepCd ) >= 300) {
            projectFlTyCd = "D";
        } else {
            projectFlTyCd = "K";
        } 
        
        if(Number(_aplInfo.stepCd ) >= 150){
            $("#SAVE_APL_INFO_BTN").hide();
            $(".input-val").attr("disabled", true);
        } else {
        	$("#SAVE_APL_INFO_BTN").show();
        	$(".input-val").attr("disabled", false);
        }
        
        $.ajax({
            url: "/advancePreparation/getAplInfo",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
            	PRJCT_NO    : _aplInfo.projectNo
               ,APL_NO      : _aplInfo.aplNo
            })
        }).done(function(response) {
            if(response != "") {
                _aplInfo.projectNo = response.PRJCT_NO;
                _aplInfo.aplNo = response.APL_NO;
                for(var key in response){
                    $("#" + key).val(response[key]);
                }
               
            } else {
                $("#" + key).empty();
            }
        });
        
        $("#SAVE_APL_INFO_BTN").on("click",function(){
            saveAplData();
        });
        
        $("#DLT_APL_INFO_BTN").one("click", function(event){
        	event.stopImmediatePropagation();
            if(!confirm('<spring:message code="conformity.121" text="정말 삭제하시겠습니까?" />')){
                return;
            } 
            //결합신청자 및 업로드 데이터 삭제
            $.ajax({
               url : "/advancePreparaion/deleteAplInfo",
               type : "post",
               contentType: 'application/json',
               data : JSON.stringify({
                   APL_NO : _aplInfo.aplNo
                  ,PRJCT_NO : _aplInfo.projectNo
               })
           }).done(function(response){
               if(response.SUCCESS == "0") {
                   alert('<spring:message code="authList.017" text="처리 되었습니다." />');
                   location.reload();
               }
           });
        });
    });
	
    function checkAplData(){

        var isCheckVal = true;
        var errorCount = _required.requiredCheck($("#APL_INFO_TABLE"));

        if(errorCount > 0) {
            return false;
        }

        return isCheckVal;
    }
    
    function saveAplData() {
        
        var result = checkAplData();
        if(!result){
            return;
        }
        
        if (!confirm('<spring:message code="common.007" text="등록 하시겠습니까?" />')) {
            return;
        }
        
        if (_aplInfo.aplNo == "0") {
            _aplInfo.aplNo = null;
        }

        $.ajax({
            url : "/advancePreparation/saveAplInfo",
            type : "post",
            contentType : 'application/json',
            data : JSON.stringify({
                PRJCT_NO : _aplInfo.projectNo,
                APL_NO : _aplInfo.aplNo,
                APL_CT_NM : $("#APL_CT_NM").val(),
                APL_CHRG : $("#APL_CHRG").val(),
                APL_CNTCT : $("#APL_CNTCT").val(),
                APL_EMAIL : $("#APL_EMAIL").val()
            })
        }).done(function(response) {
            if (response.SUCCESS != "0") {
                alert('<spring:message code="common.013" text="저장 되었습니다." />');
                var updateItem = {
                    PRJCT_NO : _aplInfo.projectNo,
                    APL_NO : response.SUCCESS,
                    APL_CT_NM : $("#APL_CT_NM").val(),
                    APL_CHRG : $("#APL_CHRG").val(),
                    APL_CNTCT : $("#APL_CNTCT").val(),
                    APL_EMAIL : $("#APL_EMAIL").val()
                }
                isUpload = false;
                
                $("#APL_LIST_GRID").jsGrid("updateItem", aplList.selectedItem, updateItem).done(function() {
                    $(".APL_NO_"+response.SUCCESS).parent().parent().click();
                });
                 
            }
        });
        
        
    }

</script>


<div>
    <div class="row">
        <div class="col-xs-12">
            <div class="x_title">
                <h2>
                    <spring:message code="project.008" text="결합신청자" /> 정보
                </h2>
            </div>
        </div>
    </div>
    <div class="x_content">
        <div class="row">
            <table class="table margin_b0" id="APL_INFO_TABLE" >
                <colgroup>
                    <col width="5%">
                    <col width="27%">
                    <col width="5%">
                    <col width="27%">
                </colgroup>
                <tbody>
                    <tr>
                        <td><b><spring:message code="project.009" text="기관명" /> (*) :</b></td>
                        <td><input id="APL_CT_NM" type="text" class="form-control wp80 input-val" placeholder='<spring:message code="project.009" text="기관명" />' required maxlength="60"></td>
                        <td><b><spring:message code="project.010" text="담당자명" /> :</b></td>
                        <td><input id="APL_CHRG" type="text" class="form-control wp80 input-val" placeholder='<spring:message code="project.010" text="담당자명" />' maxlength="60"></td>
                    </tr>
                    <tr>
                        <td><b><spring:message code="project.011" text="연락처" /> :</b></td>
                        <td><input id="APL_CNTCT" type="text" class="form-control wp80 input-val" maxlength="11" onKeyup="this.value=this.value.replace(/[^0-9]/g,'');" placeholder='<spring:message code="project.017" text="연락처" />' maxlength="11"></td>
                        <td><b><spring:message code="project.012" text="이메일" /></b></td>
                        <td><input id="APL_EMAIL" type="text" class="form-control wp80 input-val" maxlength="30" placeholder='<spring:message code="project.012" text="이메일" />' name="APL_EMAIL"></td>
                    </tr>
                </tbody>
            </table>
            <button type="button" id="SAVE_APL_INFO_BTN" class="btn btn-primary" style = "float:right;">
                <spring:message code="common.047" text="저장" />
            </button>
        </div>
    </div>
	
</div>