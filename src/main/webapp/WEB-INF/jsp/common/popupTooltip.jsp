<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<script type="text/javascript">
	
	$(function () {
	
	$('.drag-modal-tooltip').draggable({
			handle: ".modal-header"
		});
		
	});

	<%-- 도움말을 여러개 띄우고 싶은 경우, "문자열, 문자열, 문자열" 과 같이 , 로 구분해주세요 --%>
	function loadTooltipPopup(title, path, imgIDList){
		$(".tooltipImg").remove();
        
        var separator = ",";
        var imgID = new Array();
        var tmpStr = "";
        if(imgIDList) {
        	for(var i = 0; i < imgIDList.length; i++) {
	        	if(imgIDList[i] == separator) {
	        		if(tmpStr.length > 0) {
	        			imgID.push(tmpStr);
	        			tmpStr = "";
	        		}
	        	} else {
	        		tmpStr += imgIDList[i];
	        	}
	        }
	        imgID.push(tmpStr);
        }
        
        var imgFile = new Array();
        var tooltipErrorCount = 0;
        var tooltipSuccessCount = 0;
        for(var i = 0; i < imgID.length; i++) {
        	var $tmpImg = $('<div id="tooltipImg_'+i+'" class="tooltipImg table-responsive"></div>');
        	imgFile[i] = new Image();
        	imgFile[i].onload=function(){
		    	$("#MODAL_TOOLTIP").find(".modal-title").text(title + ' <spring:message code="definitionLevels.061" text="도움말" />');
		    	tooltipSuccessCount++;
	        }
	        imgFile[i].onerror=function(){
	        	tooltipErrorCount++;
	        	if(tooltipErrorCount > 1){
	        		this.remove();
	        		tooltipErrorCount = 0;
	        	}
	        	if(tooltipSuccessCount == 0) {
					this.src = "/img/popup-info/NO_INFO.PNG";
		    		$("#MODAL_TOOLTIP").find(".modal-title").text('<spring:message code="definitionLevels.062" text="도움말이 존재하지 않습니다." />');
	        	} else {
	        		this.remove();
	        	}
	        }
	        imgFile[i].src = "/img/popup-info/"+path+imgID[i]+".PNG";
        	$("#MODAL_TOOLTIP_IMG").append($tmpImg);
        	document.getElementById('tooltipImg_'+i).appendChild(imgFile[i]);
        	imgFile[i].setAttribute('width', "100%");
        }

    }

	
	</script>
	
	<div id="MODAL_TOOLTIP" class="modal fade drag-modal-tooltip" style="display: none; z-index: 9999;" aria-hidden="true" data-backdrop="false">
		<div class="modal-dialog modal-xxlg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title"></h4>
				</div>
				<div class="modal-body">
					<div id="MODAL_TOOLTIP_IMG" class="modal-body text-center">
					</div>
				</div>
				<div class="modal-footer">
					<button id="MODAL_TCN_SELECT_CLOSE_BTN" type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="common.076" text="닫기" /></button>
				</div>
			</div>
		</div>
	</div>