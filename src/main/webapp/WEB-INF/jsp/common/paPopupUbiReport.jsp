<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ page import="com.ubireport.common.util.StrUtil" %>
<%
    // 보안을 위해 설치 후 임시로 false로 변경해서 결과 확인 후 소스 배포 시 무조건 true로 변경해야함
    boolean refererCheck = false;
    String referer = StrUtil.nvl(request.getHeader("referer"), ""); // REFERER 가져오기
    if( refererCheck && (referer.equals("") || referer.indexOf(request.getServerName()) == -1) ) {  // REFERER 체크(브라우저에서 직접 호출 방지)
        out.clear();
        out.print("비정상적인 접근입니다.");
        return;
    }

    //웹어플리케이션명
    String appName = StrUtil.nvl(request.getContextPath(), "");
    if( appName.indexOf("/") == 0 ) {
        appName = appName.substring(1, appName.length());
    }   

    //웹어플리케이션 Root URL, ex)http://localhost:8080/myapp
    String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + ((appName.length() == 0)?"":"/") + appName;

    //UI에서 호출될 때 필요한 정보
    String file = StrUtil.nvl(request.getParameter("file"), "report_test.jrf");
    //StrUtil.encrypt64(StrUtil.nvl(request.getParameter("arg"), "아규먼트 명1#값2#아규먼트 명2#값2#"),"UTF-8");
    String arg = StrUtil.encrypt64(StrUtil.nvl(request.getParameter("arg"), "PRJCT_NO#"+request.getParameter("PRJCT_NO")),"UTF-8");
    String resid = StrUtil.nvl(request.getParameter("resid"), "UBIHTML");
/*
    System.out.println("[appUrl] " + appUrl);
    System.out.println("[file] " + file);
    System.out.println("[resid] " + resid);
*/
    System.out.println("[arg] " + arg);
    out.clearBuffer();
%>
<!DOCTYPE html PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN' 'http://www.w3.org/TR/html4/loose.dtd'>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.5,user-scalable=yes">
<title>가명처리 보고서</title>
<link rel="stylesheet" type="text/css" href="../../ubi4/css/ubieform.css" />
<!--[if IE]><script src='./js/ubiexcanvas.js'></script><![endif]-->
<script src="../../plugins/jquery/jquery-3.6.0.js"></script><!-- shchae -->
<script src='../../ubi4/js/ubicommon.js'></script>
<script src='../../ubi4/js/ubihtml.js'></script>
<script src='../../ubi4/js/msg.js'></script>
<script src='../../ubi4/js/ubinonax.js'></script>
<script src='../../ubi4/js/ubieform.js'></script>

<script language='javascript'>
//<!--
<!-- js로 파라미터 구하기 shchae-->
const urlParams = new URLSearchParams(window.location.search);
var pdfStatusInfo = { 
		prjctNo : Number(urlParams.get('PRJCT_NO'))
		,totalCheckData : null
		}
<!--
$(function () {
	f1();
   
    createReIdentifiTotalCheck();
    getProjectData();
    
});

$( document ).ready(function() {
    console.log( "ready!" );
});
-->	
	<%-- 
	function f1(){
	    $.ajax({
	        url: "/common/getTotalReortData",
	        type : "post",
	        contentType: 'application/json',
	        data : JSON.stringify({
	            PRJCT_NO    : pdfStatusInfo.prjctNo
	            ,IS_JOIN    : false
	        })
	    }).done(function(response) {
	    	pdfStatusInfo.totalCheckData = response.TOTAL_CHECK_DATA;
	    	
	    	let kList = pdfStatusInfo.totalCheckData.K_LIST;
            let numList = pdfStatusInfo.totalCheckData.NUM_LIST;
            let txtList = pdfStatusInfo.totalCheckData.TXT_LIST;
            let kMin = pdfStatusInfo.totalCheckData.K_MIN;
            let lMin = pdfStatusInfo.totalCheckData.L_MIN;
            
	        console.log("SUCCESS!!!!!"+response.PROJECT_INFO.PRJCT_NM
	        		+"kMin: "+kMin);
	        var dataExistCount = 0; 
	       //식별정보/식별가능정보
            for(var ki = 0; ki < kList.length; ki++) {
                let checkData = kList[ki];
                let minVal = Number(pdfStatusInfo.totalCheckData.IDNTFC_K_VAL);
                let resultV = "";
                dataExistCount++;

                if(minVal > 0 && minVal >= Number(checkData.FREQ)){
                    resultV = "미충족";
                } else {
                    resultV = "충족";
                }

            }
	    });
	}
	//종합판단
	function createReIdentifiTotalCheck(){
		 $.ajax({
	            url: "/common/normal/paFrameReIdentifiTotalCheck?PRO_NO="+ pdfStatusInfo.prjctNo
	           ,type : "get"
	        }).done(function(response) {
	        	console.log("SUCCESS!!!!! 2222");
	    });
	}  
	function getProjectData(){
		$.ajax({
            url: "/paAdditionalWork/getReIdentifiTotalCheckData",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                 PRO_NO     : pdfStatusInfo.projectNo
            })
        }).done(function(response) {
        	if(response){
        		var resultCnt = 0;
        		
        		var idntfcSpanHtml = "";
                var idntfcDetailSpanHtml = "";

                var kclmnList = "";
                var lclmnList = "";

                var minKVal = -1;
                
                if(response.IDNTFC_K_VAL && Number(response.IDNTFC_K_VAL) >= 1){
                    minKVal = Number(response.IDNTFC_K_VAL);
                }
                
                var isPass = true;

                if(response.K_LIST){
                    for(var ki = 0 ; ki < response.K_LIST.length ; ki++ ){
                        var kData = response.K_LIST[ki];
                        if(kData){
                            if(kData.EXCEPT_PSE && kData.EXCEPT_PSE != ""){
                                idntfcDetailSpanHtml += '[' + kData.CLM_NM + '] ' + '<spring:message code="reIdentifiTotalCheck.094" text="컬럼에 적용된" /> ' + kData.EXCEPT_PSE + ' ' + '<spring:message code="reIdentifiTotalCheck.095" text="기술은 난수화 기술이므로 평가대상에서 제외됩니다." />'
                            } else if(minKVal > 0 && minKVal >= Number(kData.FREQ)){
                                resultCnt++;
                            }

                        }
                    }
                }
                
                var etcKSpanHtml = "";
                var etcKNumVal = -1;
                var etcKTxtVal = -1;
                var etcSpanHtml = "";
                var etcDetailSpanHtml = "";

                isPass = true;
                
                if(response.NUM_LIST) {
                    for(var ki = 0 ; ki < response.NUM_LIST.length ; ki++ ){
                        var numData = response.NUM_LIST[ki];
                        let isCheck = true;
                        if(numData){

                            if(etcSpanHtml != ""){
                                etcSpanHtml += "</B><br/>"
                            }

                            if(etcKNumVal > 0 && etcKNumVal >= Number(numData.FREQ) ){
                                resultCnt++;
                            }
                        }
                    }
                }
                
                
                
                var resultTxt = "' <spring:message code='reIdentifiTotalCheck.012' text='적정' /> '";
                switch (resultCnt) {
                case 0:
                    resultTxt = "' <spring:message code='reIdentifiTotalCheck.012' text='적정' /> '";
                    $("#RESULT_TXT_SPAN").attr("class", "bg-primary disabled color-palette");

                    break;

                case 1:
                    resultTxt = '\' <spring:message code="reIdentifiTotalCheck.044" text="부분적정" /> \'';
                    $("#RESULT_TXT_SPAN").attr("class", "bg-warning disabled color-palette");

                    break;

                default:
                    resultTxt = '\' <spring:message code="reIdentifiTotalCheck.045" text="부적정" /> \'';
                    $("#RESULT_TXT_SPAN").attr("class", "bg-danger disabled color-palette");

                    break;
            }
                
        	}
        });
	}  --%>
//request.getScheme() 정보를 잘 못 가지고 온다면 아래의 변수 사용해서 호출
var appUrl = self.location.protocol + '//' + self.location.host + '<%= ((appName.length() == 0)?"":"/") + appName %>';


var ubiHtmlViewer = null;
var ubiParams = {
    "appurl": "<%= appUrl %>"  //오류가 발생한다면 "appurl":appUrl 로 호출
    ,"key": "<%= session.getId() %>" 
    ,"jrffile": "<%= file %>"
    ,"arg": "<%= arg %>"
    ,"resid": "<%= resid %>"
};
var ubiEvents ={
     "reportevent.previewend": ubiReportPreviwEnd
    ,"reportevent.printend": ubiReportPrintEnd
    ,"reportevent.exportend": ubiReportExportEnd
//  ,"reportevent.printClicked": ubiReportPrintClicked
//  ,"reportevent.exportClicked": ubiReportExportClicked
//  ,"eformevent.previewend": ubiEformPreviewEnd
//  ,"eformevent.saveend": ubiEformSaveEnd
};


function ubiStart() {
    ubiHtmlViewer = UbiLoad(ubiParams, ubiEvents);
    try { /*ubiHtmlViewer.setUserSaveList('Image,Pdf,Docx,Xls,Pptx,Hml,Cell');*/ }catch(e){}
    try { /*ubiHtmlViewer.setUserPrintList('Ubi,Html,Pdf');*/ }catch(e){}
    try { /*ubiHtmlViewer.setVisibleToolbar('INFO', false);*/ }catch(e){}
    try { 
    ubiHtmlViewer.setVisibleToolbar("SAVE_EXCEL", false);
    ubiHtmlViewer.setVisibleToolbar("SAVE_DOCX", false);
    ubiHtmlViewer.setVisibleToolbar("SAVE_PPTX", false);
    ubiHtmlViewer.setVisibleToolbar("SAVE_CELL", false);
    ubiHtmlViewer.setVisibleToolbar("SAVE_HML", false);
    }catch(e){}

}

function ubiReportPreviwEnd() {
    //console.log('ubiPreviwEnd......');
}

function ubiReportPrintEnd() {
    //console.log('ubiReportPrintEnd......');
}

function ubiReportExportEnd() {
    //console.log('ubiExportEnd......');
}

function ubiReportPrintClicked() {
    //console.log('ubiReportPrintClicked......');
}

function ubiReportExportClicked() {
    //console.log('ubiReportExportClicked......');
}

function ubiEformSaveEnd() {
    //console.log('ubiEformSaveEnd......');
}

function ubiEformPreviewEnd() {
    //console.log('ubiEformPreviewEnd......');
};

function ubiEformSaveEnd(file) {
    //console.log('ubiEformSaveEnd......' + file);
};

 //-->
</script>
</head>
<body onload='ubiStart()'>
</body>
</html>
