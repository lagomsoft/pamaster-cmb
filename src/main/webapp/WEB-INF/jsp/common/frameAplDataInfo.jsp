<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<style type="text/css">
    ._ellipsis {
        overflow:hidden;
        text-overflow:ellipsis;
        white-space:nowrap;
    }

    .obj-header{
        cursor: pointer;
        height: 25px;
        margin: auto;
    }
    .btn-change-column{
        width: 120px !important;
        height: 25px !important;
        margin: auto;
    }
    .min-width-150{
        min-width:150px;
    }

</style>

<script type="text/javascript">

    var _uploadDataInfo = {
        fileNm : null
        ,uplCnt : null
    }

    var _aplDataInfo = {
        fileNo: null
        , fileId: null
        , projectNo: null
        , colNo: null
        , obj_ty: null
        , stepCd: null
        , fileTypeCd: null
        , flTyCd : null
    }

    var changeList = new Array();
    var changeListCnt = 0;

    var aliasFileSize = "${ALIAS_FILE_SIZE}";
    var aliasRowSize = "${ALIAS_ROW_SIZE}";

    <%-- 등록 가능한 파일 사이즈 MB --%>
    var uploadSize = 50;
    <%-- 등록 가능한 총 파일 사이즈 MB --%>
    var maxUploadSize = 500;
    
    var fileOption = {};

    $(function () {
        _aplDataInfo.projectNo = ${PRO_NO};
        _aplDataInfo.aplNo = ${APL_NO};
        _aplDataInfo.stepCd = ${STEP_CD};
        _aplDataInfo.fileTypeCd = null;
        
        if(_aplDataInfo.stepCd != null) {
            if(Number(_aplDataInfo.stepCd) < 300) {
                _aplDataInfo.fileTypeCd = 'K'
            } else {
                _aplDataInfo.fileTypeCd = 'D'
            }
        }
     
        $.ajax({
            url: "/advancePreparation/getAplDataInfo",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO    : _aplDataInfo.projectNo
                ,APL_NO      : _aplDataInfo.aplNo
                ,FL_TY_CD    : _aplDataInfo.fileTypeCd
            })
        }).done(function(response) {
        	
            if(response != "") {
            	if(Number(_aplDataInfo.stepCd) >= 150){
                    fileOption.pamsFileShowDeleteBtn = false;
                    $(".fileInfoDiv").hide();
                    $(".save-btn").hide();
                }else{
                    fileOption.pamsFileShowDeleteBtn = true;
                    $(".save-btn").show();
                }
            	
            	if( Number(_aplDataInfo.stepCd) >= 300 && Number(_aplDataInfo.stepCd) <= 330 ) {
            	    fileOption.pamsFileShowDeleteBtn = true;
            		$(".fileInfoDiv").show();
            		$(".save-btn").show();
            	}
            	
                _aplDataInfo.projectNo = response.PRJCT_NO;
                _aplDataInfo.aplNo = response.APL_NO;
                _aplDataInfo.fileNo = response.FL_NO;

                getDataList(_aplDataInfo.projectNo, _aplDataInfo.aplNo, _aplDataInfo.fileNo, false);

            } else {
                //$("#" + key).empty();
            }
        });

        changeList = new Array();
        changeListCnt = 0;
        $("#PAMS_PROJECT_SELECT").val(_aplDataInfo.projectNo);
        fileOption = loadPAMsFile("CSV_FILE_DIV", {
            maxFileCount : 1
            ,uploadSize : Number( aliasFileSize )
            ,maxUploadSize : Number( aliasFileSize )
            ,pamsFileType : [ 'csv', 'tsv', 'txt' ]
            ,pamsFileShowDownloadBtn : false
            ,pamsFileAddCallback : function(){
                $("#FILE_SETTING_DIV").show();
            }
            ,pamsFileDeleteCallback : function(){
                $("#FILE_SHOW_DIV").find(".pams-file-delete-btn").click();
                checkRecord = 0;

                $("#FILE_SETTING_DIV").hide();
                $("#DATA_DISPLAY").hide();
            }
        }, "FILE_SHOW_DIV");

    });


    function saveFile() {
        if(_aplDataInfo.aplNo == 0) {
            alert('<spring:message code="project.018" text="결합신청자를 생성해 주십시오."/>');
            return;
        }

        // 파일이 있는지 체크
        if (pamsFileCount() == 0) {
            // 파일등록 경고창
            alert('<spring:message code="common.008" text="파일이 존재하지 않습니다." />');
            return;
        }

        if(_required.requiredCheck($("#pageDisplay")) > 0) return;

        var fileData = getFilData();

        if (confirm('<spring:message code="dataUpload.020" text="업로드 하시겠습니까?" />')) {
            <%--등록할 파일 리스트를 formData로 데이터 입력--%>

            fileData.append('HEADER', $("#FILE_HEADER").val());
            fileData.append('TERMINATED', $("#FILE_TERMINATED").val());
            fileData.append('ENCLOSED', $('#FILE_ENCLOSED').val());
            fileData.append('CHARACTERSET', $('#FILE_CHARACTERSET').val());
            fileData.append('PRJCT_NO', _aplDataInfo.projectNo);
            fileData.append('APL_NO', _aplDataInfo.aplNo);
            fileData.append('FL_NO', _aplDataInfo.fileNo);
            fileData.append('FL_TY_CD', _aplDataInfo.fileTypeCd);
            fileData.append("FL_ID", _aplDataInfo.fileId);

            $.ajax({
                url : "/advancePreparation/csvDataUpload",
                type : 'POST',
                enctype : 'multipart/form-data',
                processData : false,
                contentType : false,
                dataType : 'json',
                cache : false,
                data : fileData,
                success : function(result) {
                    if(result.RETURN_CODE == "0"){
                        let isReUpload = false;
                        if(_aplDataInfo.fileNo == result.FL_NO){ <%-- 재업로드된 경우 --%>
                            isReUpload = true;
                        }
                        _aplDataInfo.fileNo = result.FL_NO;
                        getDataList(_aplDataInfo.projectNo, _aplDataInfo.aplNo, _aplDataInfo.fileNo, isReUpload);
                        isUpload = true;
                    }else{
                        alert('<spring:message code="common.063" text="데이터 업로드가 실패했습니다."/>');
                        isUpload = false;
                        $("#DATA_DISPLAY").hide();
                    }

                },
                error : function(e) {
                    alert('<spring:message code="common.063" text="데이터 업로드가 실패했습니다."/>');
                    isUpload = false;
                    $("#DATA_DISPLAY").hide();
                }
            });
        }
    }

    function getDataList(proNo, aplNo, fileNo, isReUpload) {
        $('#pageDisplay').show();
        $("#FILE_SETTING_DIV").hide();
        $("#DATA_DISPLAY").hide();
        $("#DATA_DISPLAY").show();
        $.ajax({
            url: "/advancePreparation/getUploadDataInfo",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
                PRJCT_NO : proNo
                ,APL_NO : aplNo
                ,FL_NO : fileNo
            })

        }).done(function(response) {
            if(!response) {
                return;
            }
            
            var flInfoMap = response.FL_INFO;
            var projectColumns = response.PROJECT_COLUMNS;
            var projectRowdatas = response.PROJECT_ROWDATAS;
            var fileId = response.FL_ID;
            var rowCnt = response.TBL_ROW;
            
            _aplDataInfo.flTyCd = flInfoMap.FL_TY_CD;
             
            isBindKey=true;
            
            if(updateColumnList) {
            	isBindKey=false;
                var findFileIndex = updateColumnList.findIndex(element => element.FL_NO == fileNo); // updateColumnList에 불러온 데이터가 이미 들어가있는지 확인
                if(findFileIndex > -1 && isReUpload) { //기존 데이터를 삭제하고 재업로드 하는 경우 (FL_NO가 동일함)
                    updateColumnList.splice(findFileIndex, 1);
                }
                if(findFileIndex > -1 && !isReUpload) { // 이미 있는 경우 기존에 변경했던 값을 덮어씀 (항목선택 or 항목명 변경 반영)
                    projectColumns = updateColumnList[findFileIndex].updateColumn;
                } else { // 처음 데이터를 불러온 경우 새로 넣음
                    var newColumns = {
                        FL_NO : Number(fileNo)
                        ,updateColumn : projectColumns
                    }
                    updateColumnList.push(newColumns);
                    //console.log("새로 들어간 값 : ", newColumns);
                }
            }
            //console.log("최종값 : ", updateColumnList);

            _aplDataInfo.fileId = fileId;

            _uploadDataInfo.fileNm = response.FL_NM;
        
            getFiles(fileId);
/*             
            $("#APL_LIST_DIV").find('[id="FL_NM_'+aplNo+'"]').text(_uploadDataInfo.fileNm);
            
            $("#APL_LIST_DIV").find('[id="FL_UPL_'+aplNo+'"]').text(rowCnt);
             */
             
            var updateItem = {
            	FL_NM : _uploadDataInfo.fileNm
            	,UPL_CNT : rowCnt
            }

            $("#APL_LIST_GRID").jsGrid("updateItem", aplList.selectedItem, updateItem).done(function() {
                var $selectedRow = $("#APL_LIST_GRID").find(".APL_NO_"+flInfoMap.APL_NO).parent().parent();
                $selectedRow.addClass('highlight');
            });

            
            
            $("#SELECT_OBJECT_DIV").show();

            /* if(Number( projectStep ) < 400){

            if(Number(projectRows) >= passRowSize ){
                $("#SAVE_BTN").show();
            }else{
                $("#SAVE_NOW_BTN").show();
            }


            <c:if test='${IS_SYS_ADMIN == "Y" or  AUTH_CD.contains("400")}'>
            $("#SAVE_ONTSTEP_BTN").show();
            </c:if>

            $("#SAVE_TMP_BTN").show();

            } */
            
            if(flInfoMap) {
            	$("#FILE_TERMINATED").val(flInfoMap.DLM_CD);
            	$("#FILE_HEADER").val(flInfoMap.HDR_CD);
            	$("#FILE_CHARACTERSET").val(flInfoMap.ECD_CD);
            	$("#FILE_ENCLOSED").val(flInfoMap.QTE_CD);
            }

            $("#OBJECT_HEADER").empty();

            var $headerTr1 = $("<tr>").append('<th class="txt_l min-width-150"><spring:message code="selectionObject.004" text="항목선택" /></th>');
            var $headerTr2 = $('<tr>').append('<th class="txt_l bg_info3"><spring:message code="dataUpload.023" text="항목명" /></th>');
            var $headerTr3 = $("<tr class = 'd-none'>").append('<th class="txt_l"><spring:message code="selectionObject.001" text="개인정보 유형 선택" /></th>');

            $("#OBJECT_HEADER").append($headerTr2);
            $("#OBJECT_HEADER").append($headerTr1);
            $("#OBJECT_HEADER").append($headerTr3);

            for(var ci = 0 ; ci < projectColumns.length ; ci++ ){

                var projectColumn = projectColumns[ci];
                var indvdlinfoCd = "PD00001";
                var valCnt = 0;
                var infoSe = "1";
                //값이 숫자인지 문자인지 체크
                var isInitNumber = true;
                //값이 날짜 형식인지 체크

                var isInitDate = true;
                var datatimeRegexp1 = /[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}/;
                var datatimeRegexp2 = /[0-9]{4}[0-1]{1}[0-9]{1}[0-3]{1}[0-9]{1}/;


                for(var ri = 0 ; ri < projectRowdatas.length ; ri++ ){

                    var projectRowdata = projectRowdatas[ri];


                    if(projectRowdata["col" + projectColumn.FL_CLM_NO]){

                        var tmpProjectData = projectRowdata["col" + projectColumn.FL_CLM_NO];

                        if(tmpProjectData){
                            valCnt++;
                        }

                        if(isNaN( String(tmpProjectData).replace(/,/g,"") )){
                            isInitNumber = false;
                        }
                        if ( !datatimeRegexp1.test(tmpProjectData) && !( String(tmpProjectData).length == 8 && datatimeRegexp2.test(tmpProjectData) )  ) {
                            isInitDate = false;
                        }

                    }
                }

                if(valCnt > 0){

                    if(isInitDate){
                        indvdlinfoCd = "PD00003";
                    }else if(isInitNumber){
                        indvdlinfoCd = "PD00002";
                    }

                }

                var $headerTr3ThSelect = $("#TMP_MPPING_SELECT").clone().val(indvdlinfoCd);
                $headerTr3ThSelect.attr("id", "MPPING_SELECT_" + projectColumn.FL_CLM_NO).attr("clmnNo", projectColumn.CLMN_NO).attr("orgIndvdlinfoCd", projectColumn.INDVDLINFO_CD);
                var $headerTr3Th = $("<th width='200px;'>").append($headerTr3ThSelect);

                $headerTr3.append($headerTr3Th);

                var $headerTr1Th = $('<th id="OBJ_TY_'+projectColumn.FL_CLM_NO+'" class="obj-header" onclick="changeColumnsObjTy(\''+projectColumn.FL_CLM_NO+'\')" objTy="'+projectColumn.OBJ_TY+'"  orgObjTy="'+projectColumn.OBJ_TY+'"" >');
                //var columnNum = projectColumn.FL_CLM_NO;
                switch (projectColumn.OBJ_TY) {
                    case 'INCLS':
                        $headerTr1Th.append('<h5><span class="btn btn-primary btn-xs btn-change-column"><spring:message code="common.040" text="포함" /></span></h5>');
                        break;
                    case 'EXCL':
                        $headerTr1Th.append('<h5><span class="btn btn-danger btn-xs btn-change-column"><spring:message code="common.041" text="제외" /></span></h5>');
                        break;
                    case 'BIND':
                        $headerTr1Th.append('<h5><span class="btn btn-success btn-xs btn-change-column"><spring:message code="common.042" text="결합키" /></span></h5>');
                        break;
                    default:

                }

                $headerTr1.append($headerTr1Th);
                //var tmpIdx = ci+1;
                var tmpIdx = projectColumn.FL_CLM_NO;
                var clmTy = $("#MPPING_SELECT_"+tmpIdx).val();
                var $headerTr2Div = '<div id="CHANGE_COLUMN_NM_' + projectColumn.FL_CLM_NO + '" aplNo = "'+_aplDataInfo.aplNo+'"  clmTy = "'+clmTy+'" flclmNo="'+ projectColumn.FL_CLM_NO+'"  name="changeList" fileNo = "' +fileNo +'" objTy="EXCL" onclick="changeColumnNm('+projectColumn.FL_CLM_NO+')" orgColumnNm="' + projectColumn.OBJ_NM + '">'+projectColumn.OBJ_NM+'</div>';
                var $headerTr2Input = '<input id="CHANGE_COLUMN_NM_VAL_' + projectColumn.FL_CLM_NO + '" type="hidden" value="'+projectColumn.OBJ_NM+'">';
                $headerTr2.append($("<th class='bg_info3'>").append($headerTr2Div).append($headerTr2Input));

                var isKCheckVal = false;

            }

            $("#OBJECT_BODY").empty();
            if(projectRowdatas){
                for(var ri = 0 ; ri < projectRowdatas.length ; ri++ ){

                    var projectRowdata = projectRowdatas[ri];

                    var $headerTr2 = $("<tr>").append('<td></td>');

                    for(var ci = 0 ; ci < projectColumns.length ; ci++ ){
                        var projectColumn = projectColumns[ci];

                        var sampleData = "";

                        if(projectRowdata["col" + projectColumn.FL_CLM_NO]){
                            sampleData = projectRowdata["col" + projectColumn.FL_CLM_NO];
                        }

                        $headerTr2.append('<td class="_ellipsis">' + sampleData + '</td>');
                    }

                    $("#OBJECT_BODY").append($headerTr2);
                }
            }


        });
    }

    function changeColumnNm(columnNum){

        var txt = $("#CHANGE_COLUMN_NM_" + columnNum).text();
        var $columnNumInput = $("<input value='"+ txt +"'>");
        $("#CHANGE_COLUMN_NM_" + columnNum).empty();
        $("#CHANGE_COLUMN_NM_" + columnNum).append($columnNumInput);
        $columnNumInput.on("focusout", function(){
            var columnNm = $(this).val();
            if(columnNm){
                $("#CHANGE_COLUMN_NM_" + columnNum).text(columnNm);
                $("#CHANGE_COLUMN_NM_VAL_" + columnNum).val(columnNm);

                changeList[columnNum] = "Y";
                changeListCnt++;

                var findFileIndex = updateColumnList.findIndex(element => element.FL_NO == _aplDataInfo.fileNo);
                updateColumnList[findFileIndex].updateColumn[columnNum-1].OBJ_NM = columnNm;

            }else{
                $("#CHANGE_COLUMN_NM_" + columnNum).text($("#CHANGE_COLUMN_NM_VAL_" + columnNum).val());
            }
        });

        $columnNumInput.focus();

    }

    function changeColumnsObjTy(columnNum){
        
        $("#OBJ_TY_" + columnNum).empty();
        var objTy = $("#OBJ_TY_"+columnNum).attr("objty");
        var objTyMap=[];
        if(_aplDataInfo.flTyCd == "K"){
            objTyMap = ['EXCL', 'BIND'];
        }else{
        	 objTyMap = ['INCLS', 'EXCL', 'BIND'];
        }
        
        /*         if(projectBindYN == "Y"){
                    objTyMap.push('BIND');
                } */

        for(var oi = 0; oi < objTyMap.length; oi++) {
            if(objTy == objTyMap[oi]) {
                var tmpOi = oi+1;
                if(oi == objTyMap.length-1){
                    tmpOi = 0;
                }
                changeColumObjTySwitch(columnNum, objTyMap[tmpOi]);
                $("#OBJ_TY_" + columnNum).attr("objTy", objTyMap[tmpOi]);
                var findFileIndex = updateColumnList.findIndex(element => element.FL_NO == _aplDataInfo.fileNo);
                updateColumnList[findFileIndex].updateColumn[columnNum-1].OBJ_TY = objTyMap[tmpOi];
                break;
            }
        }
    }

    function changeColumObjTySwitch(columnNum, objTy) {
        switch (objTy) {
            case 'INCLS':
                $("#OBJ_TY_" + columnNum).append('<h5><span class="btn btn-primary btn-xs btn-change-column"><spring:message code="common.040" text="포함" /></span></h5>');
                //$("#UPLOAD_DATA_TABLE").find('[id="CHANGE_COLUMN_NM_'+columnNum+'"').attr("objTy", "INCLS");
                break;
            case 'EXCL':
                $("#OBJ_TY_" + columnNum).append('<h5><span class="btn btn-danger btn-xs btn-change-column"><spring:message code="common.041" text="제외" /></span></h5>');
                //$("#UPLOAD_DATA_TABLE").find('[id="CHANGE_COLUMN_NM_'+columnNum+'"').attr("objTy","EXCL");
                break;
            case 'BIND':
                $("#OBJ_TY_" + columnNum).append('<h5><span class="btn btn-success btn-xs btn-change-column"><spring:message code="common.042" text="결합키" /></span></h5>');
                //$("#UPLOAD_DATA_TABLE").find('[id="CHANGE_COLUMN_NM_'+columnNum+'"').attr("objTy","BIND");
                break;
            default:
        }
    }

</script>


<div class="x_panel x_result">
    <div class="row">
        <div class="col-xs-12 fileInfoDiv">
            <div class="x_title x_underline">
                <h2><spring:message code="dataUpload.004" text="file 업로드 (csv, tsv)" /></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li>
                        <a href="javascript:dataUploadTooltip.getTooltipMenu()">
                            <i class="fa fa-question-circle fa-2x"></i>
                        </a>
                    </li>
                    <li>
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="x_content">
        <div class="form-group d-none">
            <div class="row">
                <div class="custom-control-inline col-xs-2">
                    <label><spring:message code="conformity.063" text="개인정보파일의 명칭" />(*)</label>
                </div>
                <div class="col-xs-6">
                    <input type="text" id="IDINFOFILE_NM" class="form-control w-75 custom-control-inline in-val" name="IDINFOFILE_NM" value='' maxlength="100" required>
                </div>
            </div>
        </div>
        <div class="form-group d-none">
            <div class="row">
                <div class="custom-control-inline col-xs-2">
                    <label><spring:message code="conformity.064" text="개인정보파일의 운영 목적" /></label>
                </div>
                <div class="col-xs-6">
                    <input type="text" id="IDINFO_CLC_PRPS" class="form-control w-75 custom-control-inline in-val" name="IDINFO_CLC_PRPS" value='' maxlength="200">
                </div>
            </div>
        </div>
        <div class="x_title margin_t">
            <h2><spring:message code="dataUpload.005" text="파일정보" /></h2>
        </div>

        <div id="FILE_SHOW_DIV" class = "fileInfoDiv"></div>
        <div id="CSV_FILE_DIV" class="card-body fileInfoDiv"></div>
        <div class="row margin_t col-xs-14">
            <div class="col-xs-11">
                <div class="col-xs-1">
                    <label><spring:message code="dataUpload.006" text="첫번째 행" /></label>
                </div>
                <div class="col-xs-2">
                    <select id="FILE_HEADER" class="form-control unset" data-select2-id="1" tabindex="-1" aria-hidden="true" name="header">
                        <c:forEach var="fileHeader" items='${FILE_HEADER}' varStatus="status">
                            <option value='${fileHeader.CD}'>${fileHeader.CD_NM}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-xs-1">
                    <label class="form-check-label" for="exampleCheck2">
                        <spring:message code="dataUpload.009" text="필드구분자" />
                    </label>
                </div>
                <div class="col-xs-2">
                    <select class="form-control unset" id="FILE_TERMINATED" data-select2-id="1" tabindex="-1" aria-hidden="true" name="terminated">
                        <c:forEach var="fileTerminated" items='${FILE_TERMINATED}' varStatus="status">
                            <option value='${fileTerminated.CD}'>${fileTerminated.CD_NM}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-xs-1">
                    <label class="form-check-label" for="exampleCheck3"><spring:message code="dataUpload.012" text="인용부호" />&nbsp;</label>
                </div>
                <div class="col-xs-2">
                    <select class="form-control unset" id="FILE_ENCLOSED" data-select2-id="1"   tabindex="-1" aria-hidden="true" name="enclosed">
                        <c:forEach var="fileEnclosed" items='${FILE_ENCLOSED}' varStatus="status">
                            <option>${fileEnclosed.CD_NM}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="col-xs-1">
                    <label class="form-check-label" for="exampleCheck2">&nbsp;<spring:message code="dataUpload.013" text="문자&nbsp;인코딩" />&nbsp;</label>
                </div>
                <div class="col-xs-2">
                    <select class="form-control unset" id="FILE_CHARACTERSET"   data-select2-id="1" tabindex="-1" aria-hidden="true" name="characterset">
                        <c:forEach var="encodingTy" items='${ENCODING_TY}' varStatus="status">
                            <option value='${encodingTy.CD}'>${encodingTy.CD_NM}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-xs-1">
                <button type="button" onclick="saveFile();"  class="btn btn-warning save-btn">
                    <spring:message code="common.020" text="업로드" />
                </button>
            </div>
            <br/>
        </div>
    </div>

    <div id="DATA_DISPLAY" style="display: none;">

        <div class="row">
            <div class="col-xs-6" id="COLUMN_STATUS"></div>
        </div>
        <br>
        <div class="scroll_v">
            <table id="UPLOAD_DATA_TABLE" class="table basic_table table-bordered margin_b0">
                <colgroup id="OBJECT_COLGROUP">
                </colgroup>
                <thead class="thead-light" id="OBJECT_HEADER">
                </thead>
                <tbody id="OBJECT_BODY">
                </tbody>
            </table>
        </div>
    </div>
</div>

<div style="display:none;">
    <select id="TMP_MPPING_SELECT" class="form-control select2 select2-hidden-accessible wp90" data-select2-id="1" tabindex="-1" aria-hidden="true">
        <c:forEach items="${INFO_CLASSIFICATION_LIST}" var="infoClassification" varStatus="status">
            <optgroup label="${infoClassification.CD_NM}">
                <c:forEach items="${MAPPING_LIST}" var="mapping" varStatus="status">
                    <c:if test="${mapping.INFO_CL == infoClassification.CD}">
                        <option value="${mapping.CD}" >${mapping.NM}</option>
                    </c:if>
                </c:forEach>
            </optgroup>
        </c:forEach>
    </select>
</div>


