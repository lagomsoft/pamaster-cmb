<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>

<style type="text/css">
	.input-group-text {
	    display: -ms-flexbox;
	    display: flex;
	    -ms-flex-align: center;
	    align-items: center;
	    padding: .375rem .75rem;
	    margin-bottom: 0;
	    font-size: 1rem;
	    font-weight: 400;
	    line-height: 1.5;
	    color: #495057;
	    text-align: center;
	    white-space: nowrap;
	    background-color: #e9ecef;
	    border: 1px solid #ced4da;
	    border-radius: .25rem;
	}
	
	.input-group-append {
    	margin-left: -1px;
    	display: flex;
	}
	
</style>

<script type="text/javascript">
	var modalAttachPrjctno = "";
	var modalAttachFilety = "";
	var modalAttachFileId = "";
	var modalAttachFileSn = "";
	var modalAttachFileDt = ""; 
	
	var dataTypeList = ["FILE_SIZE", "COL_CNT", "ROW_CNT", "QOTAT", "SPRTR", "ENCD", "PASSWD", "FILE_NM"];

    var _carryoutInfo = {
            carryoutDe   : null
            ,privacyDtl  : null
            ,prjctDtl    : null
            ,customerDtl : null
            ,howDtl      : null
            ,useDtl      : null
            ,customerDtl : null
            ,carryoutDtl : null
       }
	
	$(document).ready(function () {
		
	});
	
	function loadlAttachFilePopup(prjctNo, flNo, fileTy, carryoutInfo){
		_carryoutInfo = carryoutInfo;
		
		modalAttachPrjctno = prjctNo;
		modalAttachFilety = fileTy;
		modalAttachFileId = "";
		modalAttachFileSn = "";
		
  		if(fileTy == "K") {
			$("#MODAL_ATTACH_FILE_TITLE").html('<spring:message code="common.120" text="결합키 파일 정보" />');
			$(".no-show-key").hide();
  			
  		} else if(fileTy == "D") {
			$("#MODAL_ATTACH_FILE_TITLE").html('<spring:message code="common.121" text="데이터 파일 정보" />');
			$(".no-show-key").show();
  		}
		
    	$.ajax({
    		url : '/common/selectAttachFileInfo',
    		type : 'post',
    		headers: {
			      'Accept': 'application/json',
			      'Content-Type': 'application/json'
				},
    		data : JSON.stringify({
    			 PRJCT_NO : prjctNo
    			,FL_NO    : flNo
    			,FL_TY_CD  : fileTy
    		})	            		
    	}).done(function(res){
    		
    		if(res && res.ATTACH_INFO){
    			
    			modalAttachFileId = res.ATTACH_INFO.FILE_ID;
    			modalAttachFileSn = res.ATTACH_INFO.FILE_SN;
    			modalAttachFileDt = res.ATTACH_INFO.DT_YN;
    			
    			for(var ti = 0; ti < dataTypeList.length; ti++) {
    				$("#" + dataTypeList[ti] + "_TD").html( res.ATTACH_INFO[dataTypeList[ti]] ); 
    			}
    			
    		    if(modalAttachFileDt == "Y") {
    		    	$("#FILE_DOWN_DIV").addClass("col-xs-12");
    		    	$("#FILE_DOWN_DIV").removeClass("col-xs-10");
    		    	
    		        $("#FILE_NM_TD").addClass('bg-danger');
    		        $("#FILE_NM_TD").text('<spring:message code="common.145" text="이미 파기된 파일입니다." />');
    		        
    		        $("#FILE_DOWN_BTN").hide();
    		    } else {
                    $("#FILE_DOWN_DIV").addClass("col-xs-10");
                    $("#FILE_DOWN_DIV").removeClass("col-xs-12");
                    
    		    	$("#FILE_NM_TD").removeClass('bg-danger');
    		    	$("#FILE_DOWN_BTN").show();
    		    }
            }                                                                                                                        
    	});
    	
	}
	
	function modalAttachDown(){
		if(modalAttachFileDt == "Y") {
		    alert('<spring:message code="common.145" text="이미 파기된 파일입니다." />');
		    
		    return;
		}
		
		
		$("#attahchFrame_MODAL").attr("src",'/common/getFile?FL_ID='+modalAttachFileId+'&FL_SN='+modalAttachFileSn);

		<%-- 제공 내역 삽입 --%>
		putCarryOutInfo();
	}
	
	function putCarryOutInfo() {
		
	    $.ajax({
	        url : '/common/postCarryoutInfo',
	        type : 'post',
	        headers: {
	              'Accept': 'application/json',
	              'Content-Type': 'application/json'
	        },
	        data : JSON.stringify({
	             PRJCT_NO     : modalAttachPrjctno
	            ,FL_ID        : modalAttachFileId
	            ,FL_SN        : modalAttachFileSn
	            ,FL_TY_CD     : modalAttachFilety
	            ,CARRYOUT_DE  : _carryoutInfo.carryoutDe
	            ,PRIVACY_DTL  : _carryoutInfo.privacyDtl
	            ,PRJCT_DTL    : _carryoutInfo.prjctDtl
	            ,USE_DTL      : _carryoutInfo.useDtl
	            ,CUSTOMER_DTL : _carryoutInfo.customerDtl
	            ,HOW_DTL      : _carryoutInfo.howDtl
	            ,CARRYOUT_DTL : _carryoutInfo.carryoutDtl
	        })                      
	    }).done(function(res){
	                                                                                                                              
	    });
	}
	
</script>

<div id="MODAL_ATTACH_FILE" class="modal" style="display: none;" aria-hidden="true" data-backdrop="false">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 id="MODAL_ATTACH_FILE_TITLE" class="modal-title"></h4>
			</div>
			
			
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table basic_table">
						<colgroup>
							<col width="30%">
							<col width="70%">
						</colgroup>
						<tbody>
							<tr>
								<th class="th_bg"><spring:message code="common.123" text="파일 사이즈" /></th>
								<td class="txt_l col d-flex align-items-center"><d id="FILE_SIZE_TD"></d> <spring:message code="common.124" text="MB" /></td>
							</tr>
							<tr class="no-show-key">
								<th class="th_bg"><spring:message code="common.125" text="컬럼 수" /></th>
								<td class="txt_l col d-flex align-items-center"><d id="COL_CNT_TD"></d> <spring:message code="common.126" text="개" /></td>
							</tr>
							<tr>
								<th class="th_bg"><spring:message code="common.127" text="행의 수" /></th>
								<td class="txt_l col d-flex align-items-center"><d id="ROW_CNT_TD"></d> <spring:message code="common.128" text="줄" /></td>
							</tr>
							<tr class="no-show-key">
								<th class="th_bg"><spring:message code="common.129" text="인코딩 타입" /></th>
								<td id="ENCD_TD" class="txt_l"></td>
							</tr>
							<tr>
								<th class="th_bg"><spring:message code="common.130" text="인용부호" /></th>
								<td id="QOTAT_TD" class="txt_l"></td>
							</tr>
							<tr>
								<th class="th_bg"><spring:message code="common.131" text="구분자" /></th>
								<td id="SPRTR_TD" class="txt_l"></td>
							</tr>
							<tr>
								<th class="th_bg"><spring:message code="common.132" text="패스워드" /></th>
								<td class="txt_l"><b id="PASSWD_TD" ></b></td>
							</tr>
						</tbody>
						<tbody>
							<tr>
								<th class="th_bg" colspan="2"><spring:message code="common.122" text="반출파일" /></th>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col d-flex align-items-center">
					<div>
						<p id="FILE_DOWN_DIV" class="text-center">
							<a href="javascript: modalAttachDown()" class="wp80">
								<span id="FILE_NM_TD" class="lead" ></span>
							</a>
						</p>
						<div class="btn-area p-t-5">
							<button id="FILE_DOWN_BTN" class="btn btn-warning" onclick="javascript: modalAttachDown()"><spring:message code="common.030" text="다운로드" /></button>
						</div>
					</div>
				</div>
			</div>
			
			<div class="modal-footer">
	        	<div id="btnPopDiv">
					<small class="float-right">
	        			<button id="MODAL_ATTACH_FILE_CLOSE_BTN" type="button" class="btn btn-default" data-dismiss="modal" ><spring:message code="common.076" text="닫기" /></button>
	        		</small>
				</div>	
			</div>
		</div>
	</div>
</div>

<iframe id="attahchFrame_MODAL" style="display:none;"></iframe>