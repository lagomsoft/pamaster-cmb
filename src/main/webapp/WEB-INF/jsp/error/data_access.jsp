<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>data access</title>
</head>
<body>
	<div class="container">
		<h1>DB관련 예외발생!</h1>
		<p class="alert alert-danger">${exception.message }</p>
		<a href="${pageContext.request.contextPath}/index.do">인덱스로 가기</a>
	</div>
</body>
</html>