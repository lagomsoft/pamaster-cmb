<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<link href="/fonts/css/font-awesome.min.css" rel="stylesheet">
<link href="/css/animate.min.css" rel="stylesheet">
<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/normalize.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.css"  rel="stylesheet">
<link href="/css/ion.rangeSlider.skinFlat.css"  rel="stylesheet">
<link href="/css/default.css" rel="stylesheet">
<link href="/css/common.css" rel="stylesheet">
<link href="/css/custom.css" rel="stylesheet">
<link href="/css/icheck/flat/green.css" rel="stylesheet">

<script src="/plugins/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<script src="/js/adminlte.min.js"></script>
<script src="/js/demo.js"></script>
<script src="/js/icheck/icheck.min.js"></script>
<script src="/js/nicescroll/jquery.nicescroll.min.js"></script>
<script src="/js/jquery-ui.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/custom.js"></script>
<head>
<meta charset="UTF-8">
<script>
window._isNotsessionCheck = true;
</script>
<title>Error</title>
</head>
<body>
	<div class="container">
		<br><br>
		<div class="col-xs-12">
			<h1 style="color:#FFFF">Error</h1>
		</div>
		<div class="col-xs-4">
		<p class="alert alert-danger">
			CODE: ${code}<br>
			요청하신 페이지에 에러가 있습니다!<br>
			MSG: ${msg}<br>
			TIMESTAMP: ${timestamp}<br>
			이러한 현상이 지속된다면 시스템 담당자에게 문의해주시기 바랍니다.<br>
			
			
			
			<a href="${pageContext.request.contextPath}/" style="color: #2196F3; font-size: 1.4em;">돌아가기</a>
		</p>
		</div>
	</div>

</body>
</html>