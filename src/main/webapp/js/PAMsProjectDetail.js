/**
 *  상세 컨텐츠 팝업 
 * _detailPopup.addDetail(프로젝트번호,상태번호,결합프로젝트여부,삽입위치) : 컨텐츠 삽입
 * _detailPopup.showPopup(프로젝트번호,상태번호,결합프로젝트여부) : 팝업 표출
 * _detialPopup.appendContent(modal 생성) : css로 레이어 구성  
 * 해당 내용 목록 변경 시 _detailPopup.tabList의 항목을 변경
 */
 
var _detailPopup = {
	projectNo : null,
	nowTabSeq : 0,
	tabList : [
		{
			"name" : "검토이력",
			"url" : "/common/paFrameExaminationList",
			"stepCd" : 140,
		},
		{
			"name" : "프로젝트 정보",
			"url" : "/common/paFrameConformity"
		},
		{
			"name" : "관련서류",
			"url" : "/common/paFrameContractCheck",
			"stepCd" : 120
		},
		{
			"name" : "데이터 정보",
			"url" : "/common/paFrameDataUpload",
			"stepCd" : 121
		},
		{
			"name" : "위험도측정",
			"url" : "/common/paFrameRiskMeasure",
			"stepCd" : 240
		},
		{
			"name" : "가명처리",
			"url" : "/common/paFrameAliasProcessing",				
			"stepCd" : 240
		},
		{
			"name" : "추가가명처리",
			"url" : "/common/paFrameAddAliasProcessing",				
			"stepCd" : 340
		},
		{
			"name" : "일반화수준",
			"url" : "/common/paFrameDataStatisticsComparison",
			"stepCd" : 240
		},
		{
			"name" : "적정성검토",
			"url" : "/common/paFrameReIdentifiTotalCheck",				
			"stepCd" : 300
		},
		{
			"name" : "반출제공내역",
			"url" : "/common/paFrameProvideDocumentsManagement",				
			"stepCd" : 620,
			"isNotShowInPopup" : true
		},
		{
			"name" : "사후관리",
			"url" : "/common/paFramePostManagement",				
			"stepCd" : 620,
			"isNotShowInPopup" : true
		}
	]
	,tabJoinList : [
		{
			"name" : "검토이력",
			"url" : "/common/paFrameExaminationList"
		},
		{
			"name" : "결합정의",
			"url" : "/common/paFrameCombinedDefinition"
		},
		{
			"name" : "추가가명처리",
			"url" : "/common/paFrameCombinedAddAliasProcessing"
		},
		{
			"name" : "일반화수준",
			"url" : "/common/paFrameCombinedStatistics"
		},
		{
			"name" : "적정성종합판단",
			"url" : "/common/paFrameCombinedTotalConfirm"
		},
		{
			"name" : "반출제공내역",
			"url" : "/common/paFrameProvideDocumentsManagement",				
			"stepCd" : 620,
			"PRJCT_TY" : "JOIN",
			"isNotShowInPopup" : true
		},
		{
			"name" : "사후관리",
			"url" : "/common/paFramePostManagement",				
			"stepCd" : 620,
			"PRJCT_TY" : "JOIN",
			"isNotShowInPopup" : true
		}
		
	]
	,isComplete : []
	,appendContent : function(type,target){
		var modal = '<div class="modal" tabindex="-1" role="dialog" id="projectDetailModal">'+
								'<div class="modal-dialog  modal-dialog-scrollable modal-xxlg" role="document">'+
									'<div class="modal-content">'+				 					
										'<div class="modal-header">'+
											'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
												'<span aria-hidden="true">&times;</span>'+
											'</button>'+
											'<h4 class="modal-title">프로젝트 상세정보</h4>'+
										'</div>'+
										'<div class="modal-body overflow-auto">'+
											'<div class="scroll_h-lg">'+			
												'<div class="card card-primary card-tabs" id="contentTabs">'+
													'<div class="card-header p-0 pt-1">'+
														'<ul class="nav nav-tabs bar_tabs" id="custom-tabs-one-tab" role="tablist">'+
														'</ul>'+
													'</div>'+
													'<div class="card-body tab-content">'+
													'</div>'+
												'</div>'+
												'<div class="card card-primary card-tabs d-none" id="tempTabs">'+
													'<div class="card-header p-0 pt-1">'+
														'<ul class="nav nav-tabs bar_tabs" role="tablist">'+
															'<li class="nav-item"><a class="nav-link" id="custom-tabs1"'+
																'data-toggle="pill" href="#custom-tabs-1" role="tab"'+
																'aria-controls="custom-tabs1" aria-selected="true">'+
															'</a></li>'+								
														'</ul>'+
													'</div>'+							
													'<div class="card-body tab-content" id="custom-tabs-one-tabContent">'+
														'<div class="tab-pane fade" id="custom-tabs-1" role="tabpanel"'+
															'aria-labelledby="custom-tabs1">'+										
														'</div>'+								
													'</div>'+						
												'</div>'+
											'</div>'+
										'</div>'+
										'<div class="overlay d-none"></div>'+					
									'</div>'+
								'</div>'+
							'</div>';
		var contents = '<div class="card card-primary card-tabs" id="contentTabs">'+
								'<div class="card-header p-0 pt-1">'+
								'<ul class="nav nav-tabs bar_tabs" id="custom-tabs-one-tab" role="tablist">'+
								'</ul>'+
							'</div>'+
							'<div class="card-body tab-content">'+
							'</div>'+
						'</div>'+
						'<div class="card card-primary card-tabs d-none" id="tempTabs">'+
							'<div class="card-header p-0 pt-1">'+
								'<ul class="nav nav-tabs bar_tabs" role="tablist">'+
									'<li class="nav-item"><a class="nav-link" id="custom-tabs1"'+
										'data-toggle="pill" href="#custom-tabs-1" role="tab"'+
										'aria-controls="custom-tabs1" aria-selected="true">'+
									'</a></li>'+								
								'</ul>'+
							'</div>'+							
							'<div class="card-body tab-content" id="custom-tabs-one-tabContent">'+
								'<div class="tab-pane fade" id="custom-tabs-1" role="tabpanel"'+
									'aria-labelledby="custom-tabs1">'+										
								'</div>'+								
							'</div>'+						
						'</div>';
						
						
		if(type == 'contents' && $("#contentTabs").length <= 0){
			$(target).append(contents);
		}else if($("#projectDetailModal").length <= 0){
			$(target).append(modal);
		}
		
	}
	,addDetail : function(projectNo,stepCd,projectTy,target){
		_detailPopup.appendContent('contents',target);
		_detailPopup.isComplete = [];
		_detailPopup.projectNo = projectNo;
		$("#contentTabs > .card-header > .nav-tabs").empty();
		$("#contentTabs > .card-body").empty();			
		var seq = 0;
		var tabList = null;
		if(projectTy == "JOIN"){
			tabList = _detailPopup.tabJoinList;
		} else {
			tabList = _detailPopup.tabList;
		}
		for(idx in tabList){
			if( !tabList[idx].stepCd || parseInt(stepCd) >= parseInt(tabList[idx].stepCd)){
				_detailPopup.isComplete[seq] = { complete : false }
			
				var tab = $("#tempTabs > .card-header > .nav-tabs > .nav-item").clone();
				$(tab).find("a").attr({
					"id":"detail-tab-"+seq
					,"href":"detail-tab-"+seq
					,"aria-controls" : "detail-tab"+seq						
					,"tabs-seq" : seq
					,"data-idx" : idx						
				});	
				$(tab).find("a").text(tabList[idx].name);
				$(tab).find("a").click(function(){
					$("#contentTabs > .card-body > .tab-pane").removeClass("active in");
					
					_detailPopup.nowTabSeq = $(this).attr("tabs-seq");
					console.log(_detailPopup.isComplete[_detailPopup.nowTabSeq]);
				
					if(!_detailPopup.isComplete[_detailPopup.nowTabSeq].complete){
						$.ajax({
							"url"  : tabList[$(this).attr("data-idx")].url,
							"type" : "get",
							"data" : {"PRO_NO" : projectNo,"STEP_CD" : stepCd,"JOIN_NO" : projectNo, "PRJCT_TY" : projectTy }
						}).done(function(res){
							$("#detail-tab"+_detailPopup.nowTabSeq).append(res);
							_detailPopup.isComplete[_detailPopup.nowTabSeq].complete = true;
							$("#detail-tab"+_detailPopup.nowTabSeq).addClass("active in");
						})
					}else{
						$("#detail-tab"+_detailPopup.nowTabSeq).addClass("active in");
					}
					return true;
				});
				$("#contentTabs > .card-header > .nav-tabs").append(tab);
				
				var contents = $("#tempTabs > .card-body > .tab-pane").clone();
				$(contents).attr({
					"id":"detail-tab"+seq
					,"aria-labelledby":"detail-tab"+seq
				})	
				$("#contentTabs > .card-body").append(contents);
				seq++;
			}
		}
			
		$("#detail-tab-0").click();
	}
	,showPopup : function(projectNo,stepCd,projectTy){
		_detailPopup.appendContent('modal','body');
		_detailPopup.isComplete = [];
		_detailPopup.projectNo = projectNo;
		$("#contentTabs > .card-header > .nav-tabs").empty();
		$("#contentTabs > .card-body").empty();
		
		var seq = 0;
		var tabList = null;
		if(projectTy == "JOIN"){
			tabList = _detailPopup.tabJoinList;
		} else {
			tabList = _detailPopup.tabList;
		}
		for(idx in tabList){
			if(( !tabList[idx].stepCd || parseInt(stepCd) >= parseInt(tabList[idx].stepCd) ) && !tabList[idx].isNotShowInPopup){
				_detailPopup.isComplete[seq] = { complete : false }
				
				var tab = $("#tempTabs > .card-header > .nav-tabs > .nav-item").clone();
				$(tab).find("a").attr({
					"id":"detail-tab-"+seq
					,"href":"detail-tab-"+seq
					,"aria-controls" : "detail-tab"+seq						
					,"tabs-seq" : seq
					,"data-idx" : idx						
				});	
				$(tab).find("a").text(tabList[idx].name);
				$(tab).find("a").click(function(){
					$("#contentTabs > .card-body > .tab-pane").removeClass("active in");
					
					_detailPopup.nowTabSeq = $(this).attr("tabs-seq");
					console.log(_detailPopup.isComplete[_detailPopup.nowTabSeq]);
					
					if(!_detailPopup.isComplete[_detailPopup.nowTabSeq].complete){
					
						$("#projectDetailModal").find(".overlay").removeClass('d-none');

						$.ajax({
							"url"  : tabList[$(this).attr("data-idx")].url,
							"type" : "get",
							"data" : {"PRO_NO" : projectNo,"STEP_CD" : stepCd,"JOIN_NO" : projectNo, "PRJCT_TY" : projectTy }
						}).done(function(res){
							$("#detail-tab"+_detailPopup.nowTabSeq).append(res);
							_detailPopup.isComplete[_detailPopup.nowTabSeq].complete = true;
							$("#detail-tab"+_detailPopup.nowTabSeq).addClass("active in");
							$("#projectDetailModal").find(".overlay").addClass('d-none');
						})
					}else{
						$("#detail-tab"+_detailPopup.nowTabSeq).addClass("active in");
					}
					return true;
				});
				$("#contentTabs > .card-header > .nav-tabs").append(tab);
				
				var contents = $("#tempTabs > .card-body > .tab-pane").clone();
				$(contents).attr({
					"id":"detail-tab"+seq
					,"aria-labelledby":"detail-tab"+seq
				})	
				$("#contentTabs > .card-body").append(contents);
				seq++;
			}
		}			
		
		$("#projectDetailModal").modal();
		$("#detail-tab-0").click();
	}
}