
	var pamFileOption = {
			// 등록 가능한 파일 사이즈 MB
			 uploadSize 	: 50
			// 등록 가능한 총 파일 사이즈 MB
			,maxUploadSize 	: 500
			// 업로드 가능 파일갯수
			,maxFileCount 	: 5
			,pamsFileType : []
			// 업로드 불가능 파일확장자
			,pamsImprtyFileType : ['exe', 'bat', 'sh', 'java', 'jsp', 'html', 'js', 'css', 'xml', 'dll']
			// 파일 추가 콜백함수
			,pamsFileAddCallback : null
			,pamsFileDeleteCallback : null
			,pamsFileShowDeleteBtn : true
			,pamsFileShowDownloadBtn : true
	}
	
	// 파일 리스트 번호
    var pamsFileIndex = 0;
	// 등록할 전체 파일 사이즈
	var pamsTotalFileSize = 0
    // 파일 리스트
    var pamsFileList = new Array();
    // 파일 사이즈 리스트
    var pamsFileSizeList = new Array();
    // 파일 삭제 리스트
    var pamsDeleteFileList = new Array();
    
    var pamFileUploadDiv;
    var pamFileShowDiv;
        
    function initFiles(){
    	
    	$("#"+ pamFileShowDiv + ' .pams-files-div').empty();
    	
    	$("#"+ pamFileShowDiv + " .pams-file-id").val("");
    	
        // 파일 리스트 번호
        pamsFileIndex = 0;
        // 등록할 전체 파일 사이즈
        pamsTotalFileSize = 0;
        // 파일 리스트
        pamsFileList = new Array();
        // 파일 사이즈 리스트
        pamsFileSizeList = new Array();
        // 파일 삭제 리스트
        pamsDeleteFileList = new Array();
    }
    
	function loadPAMsFile(fileDiv, option, fileShowDiv){
		
		pamFileUploadDiv = fileDiv;
		pamFileShowDiv = fileDiv;
		
		if(fileShowDiv){
			pamFileShowDiv = fileShowDiv;
		}
		
		if($("#"+ pamFileUploadDiv).length == 0){
			return;
		}
		
		$("#"+ pamFileUploadDiv).empty();
		$("#"+ pamFileShowDiv).empty();
		
		if(window.pamsFileSetting && pamsFileSetting.uploadSize > 0){
			
        	pamFileOption.uploadSize = pamsFileSetting.uploadSize;
        	pamFileOption.maxUploadSize = pamsFileSetting.maxUploadSize;
        	pamFileOption.maxFileCount = pamsFileSetting.maxFileCount;
        	pamFileOption.pamsFileType = pamsFileSetting.pamsFileType;
        	pamFileOption.pamsImprtyFileType = pamsFileSetting.pamsImprtyFileType;
		}
		
		if(option){
			pamFileOption = $.extend(pamFileOption, option);
		}
		
		var pamsFileHtml = '<div class="pams-file-drop-zone bg-light disabled color-palette text-center" style="border:1px dashed; padding:10px;">';
		pamsFileHtml +=	'&nbsp;';
		pamsFileHtml +=	'<br/>';
		pamsFileHtml +=	'<br/>';
		pamsFileHtml += '<input type="button" class="btn btn-secondary" value="'+pamsMsg.PAMS_FILE_UPLOAD + '" />';
		pamsFileHtml +=	'<br/>';
		pamsFileHtml +=	'<br/>';
		pamsFileHtml +=	'&nbsp;';
		pamsFileHtml +='</div>';
		pamsFileHtml +=	'<input type="file" class="file-uploader" onChange="" style="display:none;"/>';
		
		$("#"+ pamFileUploadDiv).append(pamsFileHtml);
		
		if(pamFileUploadDiv == pamFileShowDiv){
			$("#"+ pamFileUploadDiv).append("<br/>");
			$("#"+ pamFileUploadDiv).append("<br/>");
		}
		
		var pamsFileShowHtml ='<form class="fileForm">';
		pamsFileShowHtml +=	'<input class="pams-file-id" name="fileId" type="hidden" >';
		pamsFileShowHtml +=	'<div class="pams-files-div table table-striped files" >';
		pamsFileShowHtml +=	'</div>';
		pamsFileShowHtml +='</form>';
		
		$("#"+ pamFileShowDiv).append(pamsFileShowHtml);
		
		if(pamFileUploadDiv == pamFileShowDiv){
			$("#"+ pamFileShowDiv).append("<br/>");
			$("#"+ pamFileShowDiv).append("<br/>");
		}
		
		$("#"+ pamFileUploadDiv).on("change", ".file-uploader", function(e){
			addFile(this.files);
		});
		
		fileDropDown();
		
		return pamFileOption;
	}
	
	
	// 파일 드롭 다운
	function fileDropDown(){
		
	    var dropZone = $("#"+ pamFileUploadDiv + " .pams-file-drop-zone");
	    //Drag기능
	    dropZone.on('fileZone',function(e){
	        e.stopPropagation();
	        e.preventDefault();
	        // 드롭다운 영역 css
	        dropZone.css('background-color','#E3F2FC');
	    });
	    dropZone.on('dragleave',function(e){
			
	        e.stopPropagation();
	        e.preventDefault();
	        // 드롭다운 영역 css
	        dropZone.css('background-color','#FFFFFF');
	    });
	    dropZone.on('dragover',function(e){
	        e.stopPropagation();
	        e.preventDefault();
	        // 드롭다운 영역 css
	        dropZone.css('background-color','#E3F2FC');
	    });
	    dropZone.on('click',function(e){
	    	fileUploaderButton();
	    });
	    dropZone.on('drop',function(e){
			var files = e.originalEvent.dataTransfer.files;
	        e.preventDefault();
	        // 드롭다운 영역 css
	        dropZone.css('background-color','#FFFFFF');
	        
	        if(files != null){
	            if(files.length < 1){
	                alert( pamsMsg.PAMS_FILE_NOFILE  );
	                return;
	            }
	            
	            addFile(files)
	        }else{
	            alert("ERROR");
	        }
	    });
	}
	
	function fileUploaderButton() {
        // 업로드 버튼이 클릭되면 파일 찾기 창을 띄운다.
		$("#"+ pamFileUploadDiv).find(".file-uploader").click();
    }
	
	//총 파일수
	function pamsFileCount(){
		return $("#"+ pamFileShowDiv + " [id^='fileTr_']").length;
	}
	
	//업로드된 파일 사이즈
	function pamsFileSize(){
		return pamsTotalFileSize;
	}
	
	//총 변경된 파일 갯수
	function pamsCangeFileCount(){
		var uploadpamsFileList = Object.keys(pamsFileList);
		return uploadpamsFileList.length + pamsDeleteFileList.length;
	}
	
	function getFilData(){
		
        // 파일이 있는지 체크
		var form = $("#"+ pamFileShowDiv + ' .fileForm');
		var formData = new FormData(form[0]);
		
        if(pamsCangeFileCount() == 0){
            return formData;
        }
        
        // 등록할 파일 리스트
        var uploadpamsFileList = Object.keys(pamsFileList);
        for(var i = 0; i < uploadpamsFileList.length; i++){
            formData.append('files', pamsFileList[uploadpamsFileList[i]]);
        }
        
        for (var key in pamsDeleteFileList) {
        	formData.append('deletefiles', pamsDeleteFileList[key]);
        }

		return formData;
	}

    // 파일 추가시
    function addFile(files){
        // 다중파일 등록
        if(files != null){
            for(var i = 0; i < files.length; i++){
                // 파일 이름
                var fileName = files[i].name;
                var fileNameArr = fileName.split("\.");
                // 확장자
                var ext = fileNameArr[fileNameArr.length - 1].toLowerCase();
                // 파일 사이즈(단위 :MB)
                var fileSize = files[i].size / 1024 / 1024;
                
        	    if(fileSize < 1){
        	    	fileSize = 1;
        		}
        		
                if( $("[id^='fileTr_']").length + 1 > pamFileOption.maxFileCount){
                    alert(pamsMsg.PAMS_FILE_MAX_COUNT_OVER+ " \n "+ pamsMsg.PAMS_FILE_MAX_COUNT + " : " + pamFileOption.maxFileCount );
                    break;
                }
                
                // 용량을 500MB를 넘을 경우 업로드 불가
                if(pamsTotalFileSize + fileSize  > pamFileOption.maxUploadSize){
                    // 파일 사이즈 초과 경고창
                    alert(pamsMsg.PAMS_FILE_MAX_SIZE_OVER + "\n" + pamsMsg.PAMS_FILE_MAX_SIZE + " : " + pamFileOption.maxUploadSize + " MB");
                    return;
                }
                
                if(pamFileOption.pamsFileType.length > 0 && $.inArray(ext, pamFileOption.pamsFileType) < 0){
                	// 확장자 체크
                	alert( pamsMsg.PAMS_FILE_TYPE + JSON.stringify(pamFileOption.pamsFileType) );
                	break;
                }
                if($.inArray(ext, pamFileOption.pamsImprtyFileType) >= 0){
                    // 확장자 체크
                    alert(pamsMsg.PAMS_FILE_NOTYPE);
                    break;
                }else if(fileSize > pamFileOption.uploadSize){
                    // 파일 사이즈 체크
                    alert(pamsMsg.PAMS_FILE_SIZE_OVER + "\n" + pamsMsg.PAMS_FILE_MAX_SIZE + " : " + pamFileOption.uploadSize + " MB");
                    break;
                }else{
                    // 전체 파일 사이즈
                	pamsTotalFileSize += fileSize;
                    
                    // 파일 배열에 넣기
                    pamsFileList[pamsFileIndex] = files[i];
                    
                    // 파일 사이즈 배열에 넣기
                    pamsFileSizeList[pamsFileIndex] = fileSize;
                    
                    // 업로드 파일 목록 생성
                    addFileList(fileName, fileSize);
 
                }
            }
        }else{
            alert("ERROR");
        }
    }
    
    // 파일명만 보이게 추가시
    function addTmpFileDiv(filesName, fileSize){

        // 전체 파일 사이즈
    	pamsTotalFileSize += 0;
        
        // 파일 배열에 넣기
        pamsFileList[pamsFileIndex] = 0;
        
        // 파일 사이즈 배열에 넣기
        pamsFileSizeList[pamsFileIndex] = 0;
        
        addFileList(filesName, fileSize);
        
    }
    
    // 업로드 파일 목록 생성
    function addFileList(fileName, fileSize){
    	
	    var html = '<div id="fileTr_' + pamsFileIndex + '" class="row mt-1">';
	    html += 	'<div class="col-auto">';
	    html += 		'<span class="preview"><img src="data:," alt="" data-dz-thumbnail=""></span>';
	    html += 	'</div>';
		
	    html += 	'<div class="col d-flex align-items-center col-xs-10 col-download">';
	    html += 		'<p class="mb-0">';
	    html += 			'<span class="lead" data-dz-name="">' + fileName;
	    if(fileSize){
	    	html += 			' (<strong data-dz-size="">' + fileSize + '</strong> MB)</span>';	    	
	    }
	    html += 		'</p>';
	    html += 		'<strong class="error text-danger" data-dz-errormessage=""></strong>';
	    html += 	'</div>';
		
	    html += 	'<div class="col-auto d-flex align-items-center">';
	    html += 		'<div class="btn-group">';
		html += 			'<div class="col-xs-2 text_r">';
	    html += 				'<button type="button" class="btn pams-file-delete-btn btn-danger" onclick="deleteFile(' + pamsFileIndex + '); return false;">' + pamsMsg.PAMS_FILE_DELETE + '</button>';
	    html += 			'</div>';
		html += 		'</div>';
	    html += 	'</div>';
	    html += '</div>';

	    
        $("#"+ pamFileShowDiv + ' .pams-files-div').append(html);
        
        // 파일 번호 증가
        pamsFileIndex++;
        
        if(pamFileOption["pamsFileAddCallback"] && pamFileOption["pamsFileAddCallback"] instanceof Function){
        	pamFileOption.pamsFileAddCallback(fileName, fileSize);
        }
    }
    
    //파일 정보 조회
	function getFiles(fileId, fileDiv){
		
		if(fileDiv){
			pamFileShowDiv = fileDiv;
			pamFileOption.pamsFileShowDeleteBtn = false;
		}
		
		initFiles();
		
		if(fileId == undefined || fileId == null){ 
			return;
		}
		
		$("#"+ pamFileShowDiv + " .pams-file-id").val(fileId);
		
        $.ajax({
            url:"/common/getFileInfos",
            type:'POST',
            dataType:'json',
            contentType: 'application/json',
            data: JSON.stringify({
            	FILE_ID : fileId
            }),
            success:function(result){
                if(result && result.FILE_INFOS){
                	var fi = 0;
                    for(fi = 0; fi < result.FILE_INFOS.length ; fi++ ){
                        var fileInfo = result.FILE_INFOS[fi];
                        
                        // 전체 파일 사이즈
                        pamsTotalFileSize += fileInfo.FL_SIZE;
                        
                        // 파일 사이즈 배열에 넣기
                        pamsFileSizeList[pamsFileIndex] = fileInfo.FL_SIZE;
                        
                	    var html = '<div id="fileTr_' + pamsFileIndex + '" class="row margin_t">';
                	    html += 	'<div class="col-auto">';
                	    html += 		'<span class="preview"><img src="data:," alt="" data-dz-thumbnail=""></span>';
                	    html += 	'</div>';
                	    
                	    html += 	'<div class="col-xs-8 col-download">';
                	    html += 		'<span data-dz-name="">' + fileInfo.FL_NM + ' (' + fileInfo.FL_SIZE + 'MB)</span>';
                	    html += 		'<strong class="error text-danger" data-dz-errormessage=""></strong>';
                	    html += 	'</div>';
                		
                	    html += 	'<div class="col-xs-2 txt_r" style="display:flex;">';
                	    
                	    if(pamFileOption.pamsFileShowDownloadBtn){
                	    	html += 	'<a class="pams-down-btn btn btn-dark" href="/common/getFile?FL_ID='+fileInfo.FL_ID+'&FL_SN='+fileInfo.FL_SN + '" target="_blank" download >' + pamsMsg.PAMS_FILE_DOWNLOAD + '</a>';
                	    }
                	    
                	    if(pamFileOption.pamsFileShowDeleteBtn){
                	    	html += 	'&nbsp;<button type="button" class="btn pams-file-delete-btn btn-danger" onclick="deleteFile(' + pamsFileIndex + ', ' + fileInfo.FL_ID + ', ' + fileInfo.FL_SN + '); return false;">' + pamsMsg.PAMS_FILE_DELETE + '</button>';
                	    }
                	    
                	    html += 	'</div>';
                	    html += '</div>';
                	    
                        $("#"+ pamFileShowDiv + ' .pams-files-div').append(html);
                        
                        pamsFileIndex++;
                    }
                }
            }
        });
	}
	
    /*
     * 파일 리스트 나오도록 수정
     */
	function loadPAMsShowFile(fileDiv, fileId){
		
		if($("#"+ fileDiv).length == 0){
			return;
		}
		
		$("#"+ fileDiv).empty();
		
		$("#"+ fileDiv).append('<div class="pams-files-div table table-striped files" ></div>');
		
		if(fileId == undefined || fileId == null){ 
			return;
		}
		
        $.ajax({
            url:"/common/getFileInfos",
            type:'POST',
            dataType:'json',
            contentType: 'application/json',
            data: JSON.stringify({
            	FILE_ID : fileId
            }),
            success:function(result){
                if(result && result.FILE_INFOS){
                	var fi = 0;
                    for(fi = 0; fi < result.FILE_INFOS.length ; fi++ ){
                        var fileInfo = result.FILE_INFOS[fi];
                        
                        // 전체 파일 사이즈
                        pamsTotalFileSize += fileInfo.FL_SIZE;
                        
                        // 파일 사이즈 배열에 넣기
                        pamsFileSizeList[pamsFileIndex] = fileInfo.FL_SIZE;
                        
                	    var html = '<div id="fileTr_' + pamsFileIndex + '" class="row mt-1">';
                	    html += 	'<div class="col-auto">';
                	    html += 		'<span class="preview"><img src="data:," alt="" data-dz-thumbnail=""></span>';
                	    html += 	'</div>';
                	    
                	    html += 	'<div class="col d-flex align-items-center">';
                	    html += 		'<p class="mb-0">';
                	    html += 			'<span class="lead" data-dz-name="">' + fileInfo.FL_NM + '</span>';
                	    html += 			' (<span data-dz-size=""><strong>' + fileInfo.FL_SIZE + '</strong> MB</span>)';
                	    html += 		'</p>';
                	    html += 		'<strong class="error text-danger" data-dz-errormessage=""></strong>';
                	    html += 	'</div>';
                		
                	    html += 	'<div class="col-auto d-flex align-items-center">';
                	    html += 		'<div class="btn-group">';
                	    html += 			'<a class="pams-down-btn btn btn-dark" href="/common/getFile?FL_ID='+fileInfo.FL_ID+'&FL_SN='+fileInfo.FL_SN + '" target="_blank" download >' + pamsMsg.PAMS_FILE_DOWNLOAD + '</a>';
                	    html += 		'</div>';
                	    html += 	'</div>';
                	    html += '</div>';
                	    
                        $("#"+ fileDiv + ' .pams-files-div').append(html);
                        
                        pamsFileIndex++;
                    }
                }
            }
        });
	}
	
    // 업로드 파일 삭제
    function deleteFile(fIndex, fileID, fileSn){
        // 전체 파일 사이즈 수정
    	pamsTotalFileSize -= pamsFileSizeList[fIndex];
        
        // 파일 배열에서 삭제
        delete pamsFileList[fIndex];
        
        // 파일 사이즈 배열 삭제
        delete pamsFileSizeList[fIndex];
        
        // 업로드 파일 테이블 목록에서 삭제
        $("#"+ pamFileShowDiv + " #fileTr_" + fIndex).remove();
        
        if(fileID && fileSn){
        	pamsDeleteFileList[fIndex] = fileSn;
        }
        
        if(pamFileOption["pamsFileDeleteCallback"] && pamFileOption["pamsFileDeleteCallback"] instanceof Function){
        	pamFileOption.pamsFileDeleteCallback();
        }
        
    }
    
    