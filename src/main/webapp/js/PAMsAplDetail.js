/**
 *  상세 컨텐츠 팝업 
 * _aplDetailPopup.addDetail(프로젝트번호,상태번호,결합프로젝트여부,삽입위치) : 컨텐츠 삽입
 * _aplDetailPopup.showPopup(프로젝트번호,상태번호,결합프로젝트여부) : 팝업 표출
 * _detialPopup.appendContent(modal 생성) : css로 레이어 구성  
 * 해당 내용 목록 변경 시 _aplDetailPopup.tabList의 항목을 변경
 */
 
var _aplDetailPopup = {
	projectNo : null,
    aplNo : null,
    stepCd : null,
	nowTabSeq : 0,
	clickTabSeq : 0,
	tabList : [
		{
			"name" : "결합신청자 정보",
			"url" : "/common/frameAplInfo"
		},
		{
			"name" : "데이터 정보",
			"url" : "/common/frameAplDataInfo"
		}
	]
	,isComplete : []
	,appendContent : function(type,target){
		var modal = '<div class="modal" tabindex="-1" role="dialog" id="projectDetailModal">'+
								'<div class="modal-dialog  modal-dialog-scrollable modal-xxlg" role="document">'+
									'<div class="modal-content">'+				 					
										'<div class="modal-header">'+
											'<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
												'<span aria-hidden="true">&times;</span>'+
											'</button>'+
											'<h4 class="modal-title">프로젝트 상세정보</h4>'+
										'</div>'+
										'<div class="modal-body overflow-auto">'+
											'<div class="scroll_h-lg">'+			
												'<div class="card card-primary card-tabs" id="contentTabs">'+
													'<div class="card-header p-0 pt-1">'+
														'<ul class="nav nav-tabs bar_tabs" id="custom-tabs-one-tab" role="tablist">'+
														'</ul>'+
													'</div>'+
													'<div class="card-body tab-content">'+
													'</div>'+
												'</div>'+
												'<div class="card card-primary card-tabs d-none" id="tempTabs">'+
													'<div class="card-header p-0 pt-1">'+
														'<ul class="nav nav-tabs bar_tabs" role="tablist">'+
															'<li class="nav-item"><a class="nav-link" id="custom-tabs1"'+
																'data-toggle="pill" href="#custom-tabs-1" role="tab"'+
																'aria-controls="custom-tabs1" aria-selected="true">'+
															'</a></li>'+								
														'</ul>'+
													'</div>'+							
													'<div class="card-body tab-content" id="custom-tabs-one-tabContent">'+
														'<div class="tab-pane fade" id="custom-tabs-1" role="tabpanel"'+
															'aria-labelledby="custom-tabs1">'+										
														'</div>'+								
													'</div>'+						
												'</div>'+
											'</div>'+
										'</div>'+
										'<div class="overlay d-none"></div>'+					
									'</div>'+
								'</div>'+
							'</div>';
		var contents = '<div class="card card-primary card-tabs" id="contentTabs">'+
								'<div class="card-header p-0 pt-1">'+
								'<ul class="nav nav-tabs bar_tabs" id="custom-tabs-one-tab" role="tablist">'+
								'</ul>'+
							'</div>'+
							'<div class="card-body tab-content">'+
							'</div>'+
						'</div>'+
						'<div class="card card-primary card-tabs d-none" id="tempTabs">'+
							'<div class="card-header p-0 pt-1">'+
								'<ul class="nav nav-tabs bar_tabs" role="tablist">'+
									'<li class="nav-item"><a class="nav-link" id="custom-tabs1"'+
										'data-toggle="pill" href="#custom-tabs-1" role="tab"'+
										'aria-controls="custom-tabs1" aria-selected="true">'+
									'</a></li>'+								
								'</ul>'+
							'</div>'+							
							'<div class="card-body tab-content" id="custom-tabs-one-tabContent">'+
								'<div class="tab-pane fade" id="custom-tabs-1" role="tabpanel"'+
									'aria-labelledby="custom-tabs1">'+										
								'</div>'+								
							'</div>'+						
						'</div>';
						
						
		if(type == 'contents' && $("#contentTabs").length <= 0){
			$(target).append(contents);
		}else if(type == 'modal' && $("#projectDetailModal").length <= 0){
			$(target).append(modal);
		}
		
	}
	,setClickTabSeq : function(seq){
		if(Number(seq) >= 0){
			_aplDetailPopup.clickTabSeq = seq;
			return true;
		}
		return false;
	}
	,addDetail : function(projectNo,aplNo, stepCd, target){
        if(_aplDetailPopup.nowTabSeq == 0){
            _aplDetailPopup.appendContent('contents',target);
            _aplDetailPopup.nowTabSeq++;
        }
		
		_aplDetailPopup.isComplete = [];
		_aplDetailPopup.projectNo = projectNo;
        _aplDetailPopup.aplNo = aplNo;
        _aplDetailPopup.stepCd = stepCd;
		$("#contentTabs > .card-header > .nav-tabs").empty();
		$("#contentTabs > .card-body").empty();

		var seq = 0;
		var tabList = null;
        tabList = _aplDetailPopup.tabList;

		for(idx in tabList){
			if( !tabList[idx].stepCd || parseInt(stepCd) >= parseInt(tabList[idx].stepCd)){
				_aplDetailPopup.isComplete[seq] = { complete : false }
			    
				var tab = $("#tempTabs > .card-header > .nav-tabs > .nav-item").clone();
				$(tab).find("a").attr({
					"id":"detail-tab-"+seq
					,"href":"detail-tab-"+seq
					,"aria-controls" : "detail-tab"+seq						
					,"tabs-seq" : seq
					,"data-idx" : idx						
				});	
				$(tab).find("a").text(tabList[idx].name);
				$(tab).find("a").click(function(){
					$("#contentTabs > .card-body > .tab-pane").removeClass("active in");
					
					_aplDetailPopup.nowTabSeq = $(this).attr("tabs-seq");
					console.log(_aplDetailPopup.isComplete[_aplDetailPopup.nowTabSeq]);
				
					if(!_aplDetailPopup.isComplete[_aplDetailPopup.nowTabSeq].complete){
						$.ajax({
							"url"  : tabList[$(this).attr("data-idx")].url,
							"type" : "get",
							"data" : {"PRO_NO" : projectNo, "APL_NO" : aplNo, "STEP_CD" : stepCd }
						}).done(function(res){
							$("#detail-tab"+_aplDetailPopup.nowTabSeq).append(res);
							_aplDetailPopup.isComplete[_aplDetailPopup.nowTabSeq].complete = true;
							$("#detail-tab"+_aplDetailPopup.nowTabSeq).addClass("active in");
						})
					}else{
						$("#detail-tab"+_aplDetailPopup.nowTabSeq).addClass("active in");
					}
					return true;
				});
				$("#contentTabs > .card-header > .nav-tabs").append(tab);
				
				var contents = $("#tempTabs > .card-body > .tab-pane").clone();
				$(contents).attr({
					"id":"detail-tab"+seq
					,"aria-labelledby":"detail-tab"+seq
				})	
				$("#contentTabs > .card-body").append(contents);
				seq++;
			}
		}
			
		$("#detail-tab-"+_aplDetailPopup.clickTabSeq).click();
		_aplDetailPopup.setClickTabSeq(0);
	}
	,showPopup : function(projectNo,aplNo){
		_aplDetailPopup.appendContent('modal','body');
		_aplDetailPopup.isComplete = [];
		_aplDetailPopup.projectNo = projectNo;
        _aplDetailPopup.aplNo = aplNo;
		$("#contentTabs > .card-header > .nav-tabs").empty();
		$("#contentTabs > .card-body").empty();
		
		var seq = 0;
		var tabList = null;
		tabList = _aplDetailPopup.tabList;
		for(idx in tabList){
			if(( !tabList[idx].stepCd || parseInt(stepCd) >= parseInt(tabList[idx].stepCd) ) && !tabList[idx].isNotShowInPopup){
				_aplDetailPopup.isComplete[seq] = { complete : false }
				
				var tab = $("#tempTabs > .card-header > .nav-tabs > .nav-item").clone();
				$(tab).find("a").attr({
					"id":"detail-tab-"+seq
					,"href":"detail-tab-"+seq
					,"aria-controls" : "detail-tab"+seq						
					,"tabs-seq" : seq
					,"data-idx" : idx						
				});	
				$(tab).find("a").text(tabList[idx].name);
				$(tab).find("a").click(function(){
					$("#contentTabs > .card-body > .tab-pane").removeClass("active in");
					
					_aplDetailPopup.nowTabSeq = $(this).attr("tabs-seq");
					console.log(_aplDetailPopup.isComplete[_aplDetailPopup.nowTabSeq]);
					
					if(!_aplDetailPopup.isComplete[_aplDetailPopup.nowTabSeq].complete){
					
						$("#projectDetailModal").find(".overlay").removeClass('d-none');

						$.ajax({
							"url"  : tabList[$(this).attr("data-idx")].url,
							"type" : "get",
							"data" : {"PRO_NO" : projectNo, "APL_NO" : aplNo }
						}).done(function(res){
							$("#detail-tab"+_aplDetailPopup.nowTabSeq).append(res);
							_aplDetailPopup.isComplete[_aplDetailPopup.nowTabSeq].complete = true;
							$("#detail-tab"+_aplDetailPopup.nowTabSeq).addClass("active in");
							$("#projectDetailModal").find(".overlay").addClass('d-none');
						})
					}else{
						$("#detail-tab"+_aplDetailPopup.nowTabSeq).addClass("active in");
					}
					return true;
				});
				$("#contentTabs > .card-header > .nav-tabs").append(tab);
				
				var contents = $("#tempTabs > .card-body > .tab-pane").clone();
				$(contents).attr({
					"id":"detail-tab"+seq
					,"aria-labelledby":"detail-tab"+seq
				})	
				$("#contentTabs > .card-body").append(contents);
				seq++;
			}
		}			
		
		$("#projectDetailModal").modal();
		$("#detail-tab-0").click();
	}
}