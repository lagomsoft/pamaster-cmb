
    var pamSelectJoinDiv;
    
	var pamSelectJoinOption = {
			// 등록 가능한 파일 사이즈 MB
			checkBoxs : ""
	}
	
	var pamsJoinProjectStepCodeMap = {};
	var pamsJoinProjectSelectCodeMap = null;
	
	function settingPamsJoinProjectSelectCodeMap(pamSelectCallFuntion, stepCd){
	    $.ajax({
	        url: "/paAdmin/getCodeList",
	        type : "post",
	        contentType: 'application/json',
	        data : JSON.stringify({
				GRP_CD			:	"PROJECT_STEP"
	        })
	            
	    }).done(function(response) {
	    	
			pamsJoinProjectSelectCodeMap = {};
			
	    	if(response.CODE_LIST){
	    		
	    		pamsJoinProjectStepCodeMap = response.CODE_LIST;
	    		for(var ci = 0; ci < pamsJoinProjectStepCodeMap.length ; ci++ ){
	    			
	    			var pamsJoinProjectStepCode = pamsJoinProjectStepCodeMap[ci].CD;
	    			var pamsJoinProjectStepName = pamsJoinProjectStepCodeMap[ci].CD_NM;
	    			
	    			pamsJoinProjectSelectCodeMap[pamsJoinProjectStepCode] = pamsJoinProjectStepName;
	    		}
	    	}

			if(pamSelectCallFuntion){
        		window[pamSelectCallFuntion](stepCd);
        	}
	    });
    }

    function getString(str){
    	if(str){
    		return str
    	}
    	return "";
    }
    
    function getJoinProjectList(stepCd){
    	if(pamsJoinProjectSelectCodeMap == null){
    		settingPamsJoinProjectSelectCodeMap("getJoinProjectList", stepCd);
    		return;
    	}

        $.ajax({
        	url: "/common/getJoinProjectList",
        	type : "post",
        	contentType: 'application/json',
            data : JSON.stringify({
        		 PRO_STEP_CD : String(stepCd)
            })
        	
        }).done(function(response) {
        	
        	$("#PAMS_JOIN_PROJECT_SELECT").empty();
        	
        	$("#PAMS_JOIN_PROJECT_SELECT_BTN").on("click", function(){
        		var pamsProVal = $("#PAMS_JOIN_PROJECT_SELECT").val();
        		if(pamsProVal){
        	        $.ajax({
        	        	url: "/common/saveSelectJoinProject",
        	        	type : "post",
        	        	contentType: 'application/json',
        	        	data : JSON.stringify({
        	        		 PRO_NO : pamsProVal
        	        	})
        	        });
        		}
        	});
        	
        	if(response.JOIN_PROJECT_LIST && response.JOIN_PROJECT_LIST.length > 0){
        		 
        		var stepCdArr = new Array();
        		var isBeforeSelect = false;
        		for( var jpi = 0 ; jpi < response.JOIN_PROJECT_LIST.length ; jpi++ ){
        			var joinProject = response.JOIN_PROJECT_LIST[jpi];
        			
        			var $stepOptgroup = stepCdArr[joinProject.STEP_CD];
        			
        			if(!$stepOptgroup){
        				$stepOptgroup = $('<optgroup label="'+ pamsJoinProjectSelectCodeMap[joinProject.STEP_CD]+ '"></optgroup>');
        			}
        			
        			var joinProjectCharger = "";
        			var joinProjectDprtm_nm = "";
        			
        			if(joinProject.CHARGER){
        				joinProjectCharger = " / " + getString(joinProject.CHARGER);
        			}
        			
        			if(joinProject.DPRTM_NM){
        				joinProjectDprtm_nm = " / " + getString(joinProject.DPRTM_NM);        			
        			}
        			
        			var $pamsProOption = $("<option value='"+joinProject.JOIN_NO+"' step='"+joinProject.STEP_CD+"' rvwSn='" + joinProject.RVW_SN + "' > "
        					+ "[" + getString(joinProject.ALIAS_NO) + "] "
        					+ getString(joinProject.JOIN_NM) 
        					+ joinProjectCharger
        					+ joinProjectDprtm_nm
        				+" </option>");
        			
        			if(response.BEFORE_SELECT_JOIN_PROJECT == joinProject.JOIN_NO){
        				$pamsProOption.attr("selected", "selected");
        				isBeforeSelect = true;
        			}
        			
        			$stepOptgroup.append($pamsProOption);
        			
        			stepCdArr[joinProject.STEP_CD] = $stepOptgroup;
        			
        		}
        		
        		var beforeVal = 0;
        		
        		for(var key in stepCdArr){
        			
        			var $stepOptgroup = stepCdArr[key];
        			
        			if(beforeVal > key){
        				$("#PAMS_JOIN_PROJECT_SELECT").prepend($stepOptgroup);
        				
        			}else{
        				$("#PAMS_JOIN_PROJECT_SELECT").append($stepOptgroup);
        				
        			}
        			beforeVal = key;
        		}
        		
        		if(isBeforeSelect){
	        		$("#PAMS_JOIN_PROJECT_SELECT_BTN").click();        			
        		}else{
        			if(stepCd){
	        			$("#PAMS_JOIN_PROJECT_SELECT").prepend("<option value='' id='nonSelectProject'>프로젝트를 선택하여 주십시요.</option>");
	        			$("#PAMS_JOIN_PROJECT_SELECT").val("");
        			}else{
	        			$("#PAMS_JOIN_PROJECT_SELECT").val("");        				
        			}
        		}
        		
        	}else{
        		$("#PAMS_JOIN_PROJECT_SELECT").append("<option value=''> "+ pamsMsg.PAMS_NO_SELECT +" </option>");        		
        		
        	}
        		
        	
        });
        
    }
    
	function loadPamsJoinProjectSelect(selectDiv, stepCd){
		
		pamSelectJoinDiv = selectDiv;
		
		if(!stepCd){
			stepCd = "100";
		}
		
		if($("#"+ pamSelectJoinDiv).length == 0){
			return;
		}
		
		$("#"+ pamSelectJoinDiv).empty();
		
		var $pamSelectNav = $('<div id="PA_SELECT_NAV" class="x_panel">');
		
		var $projectCardTitleDiv = $('<div class="col-xs-1 search-title" ></div>').append($('<div class="x_title" style=""><h2>'+pamsMsg.PAMS_SELECT_TITLE+'</h2>'));
		var $projectSelect = $('<select id="PAMS_JOIN_PROJECT_SELECT" class="form-control">');
		var $projectSelectDiv = $('<span class="input-group-btn">').append('<button id="PAMS_JOIN_PROJECT_SELECT_BTN" type="button" class="btn btn-primary btn-flat" onclick="setProgressBar();">'+ pamsMsg.PAMS_SELECT_SEARCH +'</button>');

		var $projectCardSelectDiv = $('<div class="col-xs-9 search-select">');
		var $projectCardSelectDiv_group = $('<div class="x-search input-group">').append($projectSelect).append($projectSelectDiv);
		$projectCardSelectDiv.append($projectCardSelectDiv_group);
		//var $projectCardArrowDiv = $('<div class="col-xs-1 search-arrow">');
		//var $projectCardArrowUl = $('<ul class="nav navbar-right panel_toolbox">').append('<li><a class="collapse-link-progressbar" ><i class="fa fa-chevron-up"></i></a></li>');
		//$projectCardArrowDiv.append($projectCardArrowUl);
		
		var $pamSelectNavRow = $('<div class="row"></div>');

		$pamSelectNavRow.append($projectCardTitleDiv);
		$pamSelectNavRow.append($projectCardSelectDiv);
		//$pamSelectNavRow.append($projectCardArrowDiv);
		$pamSelectNav.append($pamSelectNavRow);
		
		$("#"+ pamSelectJoinDiv).append($pamSelectNav);
		
		getJoinProjectList(stepCd);
		$('.collapse-link-progressbar').on("click", function(){});
		$('.collapse-link-progressbar').click(function () {
		    var x_panel = $(this).closest('div.x_panel');
		    var button = $(this).find('i');
		    var content = x_panel.find('div.x_content');
		    content.slideToggle(200);
		    (x_panel.hasClass('fixed_height_390') ? x_panel.toggleClass('').toggleClass('fixed_height_390') : '');
		    (x_panel.hasClass('fixed_height_320') ? x_panel.toggleClass('').toggleClass('fixed_height_320') : '');
		    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
		    setTimeout(function () {
		        x_panel.resize();
		    }, 50);
		});
	}
	
    function setProgressBar() {
		var step = $("#PAMS_JOIN_PROJECT_SELECT option:checked").attr("step");
		var progressCount = 0;
		var progressWidth = new String();
		
		if( "progressdiv" == $('#progressdiv').attr("id")) {
			$('#progressdiv').remove();
		}
		var $pamsProgressDiv_content = $('<div id="progressdiv" class="x_content"></div>');
		var $pamsProgressDiv_wizard = $('<div class="wizard_horizontal"></div>');
		var $pamsProgressBar = $('<ul class="wizard_steps"></ul>');
		var endCnt = -1;
	    for(var i = 0 ; i < pamsJoinProjectStepCodeMap.length; i++) {
			var pamsProjectStepCode = pamsJoinProjectStepCodeMap[i];
			
			if( "0" == pamsProjectStepCode.META_1 && "J" == pamsProjectStepCode.META_6) {
				progressCount++;
				if( Number(pamsProjectStepCode.CD) <= Number(step) ) {
					var $pamsProgress = $('<li><div class="done"><span class="step_no">'+progressCount+'</span><span class="step_descr">' + pamsProjectStepCode.CD_NM + '</span></div></li>');
					$pamsProgressBar.append($pamsProgress);
				}  else {
					var $pamsProgress = $('<li><div class="disabled"><span class="step_no">'+progressCount+'</span><span class="step_descr">' + pamsProjectStepCode.CD_NM + '</span></div></li>');
					$pamsProgressBar.append($pamsProgress);
				} 
				
				if(endCnt == -1 && Number(pamsProjectStepCode.CD) > Number(step)){
					endCnt = progressCount-2;
				}
				
			}
		}
		if(endCnt > -1){
			$pamsProgressBar.find("li").eq(endCnt).find(".done, .disabled, .selected").attr("class", "selected");
		}
		progressWidth = 100/progressCount + "%";
		$pamsProgressBar.find('li').css('width', progressWidth);
		$pamsProgressDiv_wizard.append($pamsProgressBar);
		$pamsProgressDiv_content.append($pamsProgressDiv_wizard);
		$("#PA_SELECT_NAV").append($pamsProgressDiv_content);
	}
	
    
    