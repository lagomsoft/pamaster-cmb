    var pamSelectDiv;
    var nowPAMsStepCd = "100";
    var isPamsJoinPro = "";
    
	var pamSelectOption = {
			// 등록 가능한 파일 사이즈 MB
			checkBoxs : ""
	}
	
	var pamsProjectStepCodeMap = {};
	var pamsProjectSelectCodeMap = null;
	
	var pamsSelectProgress;
	
	var hasAll = false;
    
    var pamsSelectPrjctNo = null;

	var $tomSelect = null;
	
    function settingPamsProjectSelectCodeMap(pamSelectCallFuntion, stepCd){

        $.ajax({
            url: "/common/getCodeList",
            type : "post",
            contentType: 'application/json',
            data : JSON.stringify({
            	 GRP_CD : "PROJECT_STEP"
            })
        }).done(function(response) {
        	
        	pamsProjectSelectCodeMap = {};
        	
        	if(response.CODE_LIST){
        		
        		pamsProjectStepCodeMap = response.CODE_LIST;
        		
        		for(var ci = 0; ci < pamsProjectStepCodeMap.length ; ci++ ){
        			
        			var pamsProjectStepCode = pamsProjectStepCodeMap[ci].CD;
        			var pamsProjectStepName = pamsProjectStepCodeMap[ci].CD_NM;
        			
        			pamsProjectSelectCodeMap[pamsProjectStepCode] = pamsProjectStepName;
        		}
        	}
        	
        	if(pamSelectCallFuntion){
        		window[pamSelectCallFuntion](stepCd);
        	}
    		
        });
    	
    }
    
    function getString(str){
    	if(str){
    		return str
    	}
    	return "";
    }
    
    function getProjectList(stepCd){
    	
    	if(pamsProjectSelectCodeMap == null){
    		settingPamsProjectSelectCodeMap("getProjectList", stepCd);
    		return;
    	}    	
        $.ajax({
        	url: "/common/getProjectList",
        	type : "post",
        	contentType: 'application/json',
        	data : JSON.stringify({
        		 PRO_STEP_CD : stepCd
        		,MIN_PRO_STEP_CD : nowPAMsStepCd
        		,JOIN_YN  :isPamsJoinPro
        	})
        	
        }).done(function(response) {
        	
        	$("#PAMS_PROJECT_SELECT").empty();
        	
        	$("#PAMS_PROJECT_SELECT_BTN").on("click", function(){
        		var pamsProVal = $("#PAMS_PROJECT_SELECT").val();
        		if(pamsProVal){
        	        $.ajax({
        	        	url: "../common/saveSelectProject",
        	        	type : "post",
        	        	isHideOverlay : true,
        	        	contentType: 'application/json',
        	        	data : JSON.stringify({
        	        		 PRO_NO : pamsProVal
        	        	})
        	        });
        		}
        	});

        	if(response.PROJECT_LIST && response.PROJECT_LIST.length > 0){        		
        		var stepCdArr = new Array();
        		var isBeforeSelect = false;
        		
        		for( var pi = 0 ; pi < response.PROJECT_LIST.length ; pi++ ){
        			var project = response.PROJECT_LIST[pi];
        			var $stepOptgroup = stepCdArr[project.STEP_CD];
                    	
        			if(!$stepOptgroup){
        				$stepOptgroup = $('<optgroup label="'+ pamsProjectSelectCodeMap[project.STEP_CD]+ '"></optgroup>');
        			    $stepOptgroup.attr('data-value', project.STEP_CD);
                    }
        			
                    var $pamsProOption = $("<option value='"+project.PRJCT_NO+"' step='"+project.STEP_CD+"' dltYn='" + project.DLT_YN + "'  projectTy='" + project.PRJCT_TY_CD + "'  > "
//                          + " 프로젝트 번호  : " + project.PRJCT_NO  + "/" 
                            + " " + getString(project.PRJCT_NM) 
                            + " / " + getString(project.USR_NM)  
//                          + " / " + getString(project.GP_NM) 
                        +" </option>");
        			
                    if(pamsSelectPrjctNo == project.PRJCT_NO){
                        $pamsProOption.attr("selected", "selected");
                        isBeforeSelect = true;
                    }
                    
        			if(response.BEFORE_SELECT_PROJECT == project.PRJCT_NO && !isBeforeSelect){
        				$pamsProOption.attr("selected", "selected");
        				isBeforeSelect = true;
        			}
        			
        			$stepOptgroup.append($pamsProOption);
        			
        			stepCdArr[project.STEP_CD] = $stepOptgroup;
        		}
        		
        		var beforeVal = 0;
        		
        		for(var key in stepCdArr){
        			
        			var $stepOptgroup = stepCdArr[key];
        			
        			if(beforeVal > key){
        				$("#PAMS_PROJECT_SELECT").prepend($stepOptgroup);
        				
        			}else{
        				$("#PAMS_PROJECT_SELECT").append($stepOptgroup);
        				
        			}
        			beforeVal = key;
        		}
        		
        		if(isBeforeSelect){
	        		$("#PAMS_PROJECT_SELECT_BTN").click();        			
        		}else{
        			$("#PAMS_PROJECT_SELECT").val("");
        			if(!hasAll){
	        			$("#PAMS_PROJECT_SELECT").prepend("<option value='' id='nonSelectProject'>프로젝트를 선택하여 주십시요.</option>");   			
	        			$("#PAMS_PROJECT_SELECT").on("change",function(){
	        				if($(this).val() !=''){
	        					$("#nonSelectProject").remove();
	        				}
	        			})
        			}
        		}
        		
        	}else{
        		$("#PAMS_PROJECT_SELECT").append("<option value=''> "+ pamsMsg.PAMS_NO_SELECT +" </option>");
        	}
        	if(hasAll){
				$("#PAMS_PROJECT_SELECT").prepend('<option style="font-weight:bold;" value="ALL">전체선택</option>');  
			}
			setProgressBar();
            $("#PAMS_PROJECT_SELECT").attr('placeholder', '프로젝트를 선택하여 주십시요.');  
            $tomSelect = new TomSelect("#PAMS_PROJECT_SELECT",{
                create: false,
                sortField: {
                    field: "text",
                    direction: "asc"
                },
                maxOptions:1000
            });
        });
        
    }
    
	function loadPAMsProjectSelect(selectDiv, nowStepCd, isJoin){
		
		if(isJoin == undefined){
			isPamsJoinPro = "";
		}else{
			if(isJoin){
				isPamsJoinPro = "Y";
			}else{
				isPamsJoinPro = "N";				
			}
		}
		
		pamSelectDiv = selectDiv;
		
		if(nowStepCd){
			nowPAMsStepCd = String(nowStepCd);			
		}
		
		if($("#"+ pamSelectDiv).length == 0){
			return;
		}
		
		$("#"+ pamSelectDiv).empty();
		
		var $pamSelectNav = $('<div id="PA_SELECT_NAV" class="x_panel">');

		if(nowPAMsStepCd == "ALL"){
			nowPAMsStepCd = "0";			
		}
		
		/* 라디오버튼
		var $projectStepRadio = $('<div class="custom-control custom-radio custom-control-inline">')
			.append('<input type="radio" id="PAMS_PROJECT_STEP_0" name="projectStep"  class="custom-control-input" checked="checked">').on("change").on("change", function(){
				getProjectList(nowPAMsStepCd);
			}).append('<label class="custom-control-label" for="PAMS_PROJECT_STEP_0"><h6>'+  pamsMsg.PAMS_SELECT_ALL_CHECK +'</h6></label>');
		$projectCardBodyDiv.append($projectStepRadio);
		

		for(var ci = 0; ci < pamsProjectStepCodeMap.length ; ci++ ){
			
			var pamsProjectStepCode = pamsProjectStepCodeMap[ci].CD;
			var pamsProjectStepName = pamsProjectStepCodeMap[ci].CD_NM;
			
			if(pamsProjectStepCodeMap[ci].META_1 == "0" &&  Number(pamsProjectStepCode) + 99 >= Number(nowPAMsStepCd) ){
				
				var $projectStepRadioDiv = $('<div class="custom-control custom-radio custom-control-inline">');
				var $projectStepRadio = $('<input type="radio" id="PAMS_PROJECT_STEP_'+ pamsProjectStepCode +'" name="projectStep" val="ONLY_' + pamsProjectStepCode + '" class="custom-control-input">');
				var $projectStepRadioLabel = $('<label class="custom-control-label" for="PAMS_PROJECT_STEP_'+ pamsProjectStepCode +'"><h6>'+ pamsProjectStepName +'</h6></label>');
				
				$projectStepRadio.on("change", function(){
					getProjectList($(this).attr("val"));
				});
				
				$projectStepRadioDiv.append($projectStepRadio);
				$projectStepRadioDiv.append($projectStepRadioLabel);
				
				$projectCardBodyDiv.append($projectStepRadioDiv);
				
			}
			
		}*/
		var $projectCardTitleDiv = $('<div class="col-xs-1 search-title" ></div>').append($('<div class="x_title" style=""><h2>'+pamsMsg.PAMS_SELECT_TITLE+'</h2>'));
		var $projectSelect = $('<select id="PAMS_PROJECT_SELECT" class="form-control">');
		var $projectSelectDiv = $('<span class="input-group-btn">').append('<button id="PAMS_PROJECT_SELECT_BTN" type="button" class="btn btn-primary btn-flat" onclick="setProgressBar();">'+ pamsMsg.PAMS_SELECT_SEARCH +'</button>');

		var $projectCardSelectDiv = $('<div class="col-xs-9 search-select ">');
		var $projectCardSelectDiv_group = $('<div class="x-search input-group">').append($projectSelect).append($projectSelectDiv);
		$projectCardSelectDiv.append($projectCardSelectDiv_group);
		//var $projectCardArrowDiv = $('<div class="col-xs-1 search-arrow">');
		//var $projectCardArrowUl = $('<ul class="nav navbar-right panel_toolbox">').append('<li><a class="collapse-link-progressbar" ><i class="fa fa-chevron-up"></i></a></li>');
		//$projectCardArrowDiv.append($projectCardArrowUl);
		
		var $pamSelectNavRow = $('<div class="row"></div>');

		$pamSelectNavRow.append($projectCardTitleDiv);
		$pamSelectNavRow.append($projectCardSelectDiv);
		//$pamSelectNavRow.append($projectCardArrowDiv);
		$pamSelectNav.append($pamSelectNavRow);
		
		$("#"+ pamSelectDiv).append($pamSelectNav);
		
		getProjectList(nowPAMsStepCd);
		$('.collapse-link-progressbar').on("click", function(){});
		$('.collapse-link-progressbar').click(function () {
		    var x_panel = $(this).closest('div.x_panel');
		    var button = $(this).find('i');
		    var content = x_panel.find('div.x_content');
		    content.slideToggle(200);
		    (x_panel.hasClass('fixed_height_390') ? x_panel.toggleClass('').toggleClass('fixed_height_390') : '');
		    (x_panel.hasClass('fixed_height_320') ? x_panel.toggleClass('').toggleClass('fixed_height_320') : '');
		    button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
		    setTimeout(function () {
		        x_panel.resize();
		    }, 50);
		});
	}
	
    function setProgressBar() {
		var bindyn = $("#PAMS_PROJECT_SELECT option:checked").attr("bindyn");
		var step = $("#PAMS_PROJECT_SELECT option:checked").attr("step");
		var progressCount = 0;
		var progressWidth = new String();
		
		_required.initErrorMsg($(document));
		
		if(step == "" || step == null) {
			return;
		}
		
		if( "progressdiv" == $('#progressdiv').attr("id")) {
			$('#progressdiv').remove();
		}
		var $pamsProgressDiv_content = $('<div id="progressdiv" class="x_content"></div>');
		var $pamsProgressDiv_wizard = $('<div class="wizard_horizontal"></div>');
		var $pamsProgressBar = $('<ul class="wizard_steps"></ul>');
		var endCnt = -1;
	    for(var i = 0 ; i < pamsProjectStepCodeMap.length ; i++) {
			var pamsProjectStepCode = pamsProjectStepCodeMap[i];
			if( "0" == pamsProjectStepCode.META_1) {
				if( ("N" == bindyn && "Y" == pamsProjectStepCode.META_2 )|| "J" == pamsProjectStepCode.META_6) {
					continue;
				}
				progressCount++;
				if( Number(pamsProjectStepCode.CD) <= Number(step) ) {
					var $pamsProgress = $('<li><div class="done"><span class="step_no">'+progressCount+'</span><span class="step_descr">' + pamsProjectStepCode.CD_NM + '</span></div></li>');
					$pamsProgressBar.append($pamsProgress);
				}  else {
					var $pamsProgress = $('<li><div class="disabled"><span class="step_no">'+progressCount+'</span><span class="step_descr">' + pamsProjectStepCode.CD_NM + '</span></div></li>');
					$pamsProgressBar.append($pamsProgress);
				} 
				
				if(endCnt == -1 && Number(pamsProjectStepCode.CD) > Number(step)){
					endCnt = progressCount-2;
				}
				
			}
		}
		if(endCnt > -1){
			$pamsProgressBar.find("li").eq(endCnt).find(".done, .disabled, .selected").attr("class", "selected");
		}
		progressWidth = 100/progressCount + "%";
		$pamsProgressBar.find('li').css('width', progressWidth);
		$pamsProgressDiv_wizard.append($pamsProgressBar);
		$pamsProgressDiv_content.append($pamsProgressDiv_wizard);
		$("#PA_SELECT_NAV").append($pamsProgressDiv_content);
	}
	
	    
    