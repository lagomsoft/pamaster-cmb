/**
 *  공통적으로 활용할 script
 *	jquery.js 하위에 삽입 
 *	(required jquery.js)
 */
var checklogin = false;
var checkerror = false;
var playSessionCheck;
var tempValue = 0;
var _common = {
	init : function(){
		/*공통 함수의 초기 호출 코드 전체 삽입*/	
		_setModal.init();
		_overlay.init();
		_ajax.init();
		
		if(!window["_isNotsessionCheck"]){
			_sessionCheck.init();
		}
		
	}
}
/*
 *	공통 overlay modal 설정
 *  : _overlay.call() - overlay를 호출하여 전체 화면을 뒤덮는다.
 *	  _overlay.end()  - overlay를 호출 종료한다. overlay 호출수만큼 호출시 overlay를 제거한다.	
 */
 var _setModal = {
 	init : function(){ 		
 		/*
 		window.alert = function(msg){
 			_alert.show('경고', msg);
 		}
 		*/
 		/* 증복 Modal 처리 추가*/
 		
		 $(document).on('show.bs.modal', '.modal', function () {
	        if ($(".modal-backdrop").length > 1) {
	            $(".modal-backdrop").not(':first').remove();
	        }
	    });

	    $(document).on('hide.bs.modal', '.modal', function () {
	        if ($(".modal-backdrop").length == 1) {
	            $(".modal-backdrop").remove();
	        }
	    });
 		
 	}
 }

 var _overlay = {
 	countProcess : 0,
 	callbackEndProcess : null,
 	init : function(){ 
 		/*현재 body에 overlay 삽입.*/
		if($("#_overlaySpinner").length <= 0){
			var spinnerDialog = $('<div>',{
	 			'id' : '_overlaySpinner',
	 			'role' : 'dialog',
	 			'class' : 'modal',
	 			'style' : 'z-index:10000'
	 		});
	 		var spinnerDocument = $('<div>',{
	 			'role' : 'document',
	 			'class' : 'modal-dialog  modal-dialog-centered',	
				'style'	: 'display:table; overflow: hidden;'
	 		})
	 		var spinner = $('<span>',{
	 			'class' : 'fa fa-circle-o-notch fa-spin fa-3x fa-fw',
	 			'style' : 'color:#2F4F4F; display:table-cell; vertical-align: middle;' 
	 		});
	 		
	 		$(spinnerDocument).append(spinner);
	 		$(spinnerDialog).append(spinnerDocument);
	 		
	 		$('body').append($(spinnerDialog));
	 		$('#_overlaySpinner').on('hide.bs.modal', function (e) {
	 			if(_overlay.countProcess != 0){
				    e.preventDefault();
				    e.stopPropagation();
				    return false;
			    }
			});
		}
 		
 	},
 	call : function(){
 		if(_overlay.countProcess == 0){ 		
 			$('#_overlaySpinner').modal();
 		} 		
 		_overlay.countProcess++;  		
 	},
 	end : function(){
 		if(_overlay.countProcess != 0){
 			_overlay.countProcess--; 
 		}
 		if(_overlay.countProcess == 0){
 			$('#_overlaySpinner').modal('hide');
 			if(_overlay.callbackEndProcess!=null){
 				_overlay.callbackEndProcess();
 				_overlay.callbackEndProcess =null;
 				
 			}
 		}
 	}
 	
 }
  
 /*
 *	공통 ajax 설정
 *	- ajax 호출시 isHideOverlay가 true 가 아닐시 
 *	  : ajax 동작 시 전체 화면 overlay
 *	    ajax 동작 종료 시 overlay 제거
 *	- ajax 호출시 애러가 발생하면 errorCode에 대한 메세지 코드 호출
 */

 var common_isCheckSessionTimeOut = false;

 var _ajax = {
 	init : function(){
 		$.ajaxSetup({
	 	  	beforeSend:function(){
	 	  		if(!common_isCheckSessionTimeOut){
	 	  			getSessionTimeOut();
	 	  		}
	 	  		
	 	  		if(!this.isHideOverlay){
	 	  			_overlay.call();
	 	  		}	 	 	  		
	 	  	},
	 	  	success:function(res){
	 	  		if(res && res.errMsg){
	 	  			return _alert.show('경고',res.errMsg);
	 	  		}	
	 	  		if(res && res.errMsgCode){
	 	  			return _alert.show('경고','페이지 호출 중 문제가 발생하였습니다.<br/>(code : '+res.errMsgCode+')');
	 	  		}	 	  	
	 	  	},
	 	  	complete:function(){
	 	  		if(!this.isHideOverlay){
	 	  			_overlay.end();
	 	  		}
	 	  	},
		 	error:function(res,e){
		 		/* 공통적으로 현재 애러 코드 및 애러 발생을 표출한다. */
		 		var errMsg = "";
		 		switch(res.status){
		 			case 0 :
		 				errMsg = "Not Connect";
		 				break;
		 			case 400 :
		 				errMsg = "";
		 				break;	
		 			case 401 : 
		 				if(checklogin == false) {	 
		 					/* ajax 동작 중 로그아웃 되었을때 */               	
		                    if(confirm("로그인이 필요합니다. 지금 바로 로그인하시겠습니까?")){
		                    	checklogin = true;
		                        window.open("/user/login", "_self");
		                    }
		                    return;
		                }
		 				errMsg = "Unauthorized access.";	 				
		 				break;	
		 			case 403 : 
		 				errMsg = "Forbidden resource can not be accessed. ";
		 				break;	
		 			case 404 :
		 				errMsg = "Requested page not found.";			
		 				break;
		 			case 408 : 
		 				errMsg = "Request Timeout.";
		 				break;	
		 			case 500 :
		 				errMsg = "Internal server error. ";
		 				break;
		 			default :
		 				break;
		 		}
		 		if(errMsg == ""){
		 			_alert.show('경고','페이지 호출 중 문제가 발생하였습니다.<br/>(code : '+res.status+')');
		 		
		 		}else{
		 			_alert.show('경고','페이지 호출 중 문제가 발생하였습니다.<br/>(code : '+res.status+', msg : '+errMsg+')');
		 		}	 		
		 	}
	 	})
 	}
 }
 
 
 $(function(){
 	  _common.init();

 })
 
 /*
 *	공통 Modal 창 사용법
 *	표출 : _modal.show('제목','메시지','버튼1 텍스트','버튼2 텍스트',콜백함수)
 *  숨기기 : _modal.hide();
 */
 var _modal = {
	show : function(title,msg,btn1Text,btn2Text,_callback){
			_modal.init();
			$('#_defaultModal').find('._modalTitle').text(title);
			$('#_defaultModal').find('._modalMsg').html(msg);
			$('#_defaultModal').find('._btn1').text(btn1Text);
			$('#_defaultModal').find('._btn2').text(btn2Text);
			$('#_defaultModal').find('._btn1').on('click',function(){
				_callback();
			});
			$('#_defaultModal').modal();
	},
	hide : function(){
		$('#_defaultModal').modal('hide');
	},
	init : function(){		
		$('#_defaultModal').remove();
		if($('#_defaultModal').length <=0){

			var modal = $('<div>',{
				'class' : 'modal',
				'tabindex' : -1,
				'role' : 'dialog',
				'id' : '_defaultModal'
			});
			var modalDialog = $('<div>',{
				'class' : 'modal-dialog',				
				'role' : 'document'
			});
			var modalContent = $('<div>',{
				'class' : 'modal-content'
			});
			var modalHeader = $('<div>',{
				'class' : 'modal-header',
				'html' : 
		        '<button type="button" class="close" data-dismiss="modal" aria-label="Close">'+
		        '  <span aria-hidden="true">&times;</span>'+
		        '</button>'+
				'<h4 class="modal-title _modalTitle"></h4>'
			})			
			var modalBody = $('<div>',{
				'class' : 'modal-body',
				'html' : '<p class="_modalMsg"></p>'
			})
			var modalFooter = $('<div>',{
				'class' : 'modal-footer',
				'html' : '<button type="button" class="btn btn-primary _btn1" id="_btnProcess"></button>'+
		        '<button type="button" class="btn btn-secondary _btn2" data-dismiss="modal"></button>'
			});	
			modalContent.append(modalHeader);
			modalContent.append(modalBody);	
			modalContent.append(modalFooter);	
			modalDialog.append(modalContent);
			modal.append(modalDialog);
			$('body').append(modal);
		}
	}	
}

var _alert = {
	idx : 0,	
	show : function(title,msg){	
		_alert.idx++;		
		var modal = $('<div>',{
				'class' : 'modal',
				'tabindex' : -1,
				'role' : 'dialog',
				'id' : '_alertModal'+_alert.idx
			});
		var modalDialog = $('<div>',{
			'class' : 'modal-dialog',
			'role' : "document"
		})
		var alertWrapper = $('<div>',{ 
			'class' : 'alert alert-danger alert-dismissible modal-content'			
		})
		var alertClose = $('<button>',{
			'type' : 'button',
			'class' : 'close',
			'data-dismiss' : 'modal',
			'aria-hidden' : 'true',
			'text' :'x'		
		})
		var title = $('<h5>',{
			'html' : '<i class="fa fa-ban"></i>'+title
		})
		$(alertWrapper).append(alertClose);
		$(alertWrapper).append(title);
		$(alertWrapper).append(msg);
		$(modalDialog).append(alertWrapper);	
		$(modal).append(modalDialog);
		$(modal).modal();
	}
}

var common_isSessionTimeOut = false;
 
var _isEmpty = function(data){
	if(data == null || data == ''){
		return true;
	}
	return false;
}

	//session timeout check
	var _sessionCheck = { 
		init : function(){ 
			playSessionCheck = setTimeout(function() {
				getSessionTimeOut(true);
			}, 1000 * 60 * 61);
		}
	}
	
	function getSessionTimeOut(isReayload) {
		
		common_isCheckSessionTimeOut = true;
		
		if(!window["_isNotsessionCheck"]){
			$.ajax({
				url: "/main/loginCheck.jsp"
					,type  : "get"
					,cache : false
					,async: false
					,isHideOverlay : true
					, success : function(response) {
						if(!common_isSessionTimeOut){
							if(response == "N") {
								common_isSessionTimeOut = true;
								alert("세션이 만료되었습니다.");
								location.href="/user/login";
							} else {
								if(isReayload){
									_sessionCheck.init();
								}
							}
						}
					}
			});
		}
		
		common_isCheckSessionTimeOut = false;
	}
	
var  _required = {
	initErrorMsg : function($window){
		$window.find(".invalidErrorText").remove();
		$window.find(".form-control").removeClass("invalidErrorBox");
	},
	getErrorMsg : function($this, errorMsg){
		var $errorTag = $('<p class="invalidErrorText"></p>');
		$errorTag.text(errorMsg);
		$this.focus();
		if(!$this.parent().children(".invalidErrorText").length){
			if($this.parent().css("display") == "flex") {	//날짜의 경우 에러메시지가 우측에 추가돼서 예외처리 해야함
				$this.parent().parent().append($errorTag);
			} else {
				$this.parent().append($errorTag);
			}
			$this.addClass("invalidErrorBox");
			return 1;
		} else {
			return 0;
		}
	},
	requiredCheck : function($window){
		var errorMsg = "";
		var errorCount = 0;
		_required.initErrorMsg($window);
		
		$window.find(':visible').find('input, select, textarea').each(function(i) {
			if($(this).prop("required") && !$(this).prop("disabled")) {
				if($(this).val() == null || $(this).val() == "") {
					errorMsg = '항목을 채워주세요'
					if($(this).attr("required-message") && $(this).attr("required-message").length > 0) errorMsg=$(this).attr("required-message");
					errorCount += _required.getErrorMsg($(this), errorMsg);
				}
			}
			if($(this).prop("maxlength") && !$(this).prop("disabled")) {
				if( $(this).val().length > Number($(this).attr("maxlength")) ) {
					errorMsg = $(this).attr("maxlength") + '자 이하로 입력하세요' + " (" + $(this).val().length + "/ " + $(this).attr("maxlength") + ")" ;
					errorCount += _required.getErrorMsg($(this), errorMsg);
				}
			}
		});
		
		return errorCount;
	}
}