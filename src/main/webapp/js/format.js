/**
 * [연락처]: 자릿수 '-' 추가
 */
var formatAddressNum = (data) => {
	data = data.replace(/[^0-9]/g, ''); 
	let dataLen = data.length;
	
	if (dataLen == 11) {
		data = data.substring(0,3)+'-'+data.substring(3,7)+'-'+data.substring(7);
	} else if (dataLen == 8) {
		data = data.substring(0,4)+'-'+data.substring(4);
	} else {
		if(data.indexOf('02') >= 0) {
			if (dataLen == 9) {
				data = data.substring(0,2)+'-'+data.substring(2,5)+'-'+data.substring(5);
			} else {
				data = data.substring(0,2)+'-'+data.substring(2,6)+'-'+data.substring(6);
			}
		} else {
			data = data.substring(0,2)+'-'+data.substring(2,5)+'-'+data.substring(5);
		}
	}
	return data;
}

/**
 * [숫자]: 1000단위 콤마(,) 추가 
 */
var formatNumberWithComma = (data) => {
	return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}