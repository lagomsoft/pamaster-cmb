package kr.co.pamaster.common.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.pamaster.carryout.mapper.CarryoutMapper;
import kr.co.pamaster.common.mapper.CommonMapper;
import kr.co.pamaster.user.mapper.UserMapper;
import kr.co.pamaster.util.DestructionFile;
import kr.co.pamaster.util.DestructionFileTask;
import kr.co.pamaster.util.PAMsException;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.AesKeyStrength;
import net.lingala.zip4j.model.enums.CompressionLevel;
import net.lingala.zip4j.model.enums.CompressionMethod;
import net.lingala.zip4j.model.enums.EncryptionMethod;

@Service
public class CommonService {

	@Value("${pams.user.connectLog.level:}")
	private int connectLogLevel;

	@Value("${pams.user.loginLog.level:}")
	private int loginLogLevel;

	@Value("${pams.filepath:}")
	private String filepath;

	@Value("${pams.filepath.temp:}")
	private String tempFilepath;

	@Value("${pams.group.name:}")
	private String sysGroupNm;

	@Value("${pams.asekey:}")
	private String asekey;

	@Value("${pams.login.success.url:}")
	private String loginSuccessUrl;

	@Value("${pams.logout.success.url:}")
	private String logoutSuccessUrl;

	@Value("${mail.smtp.use:}")
	private String mailUse;

	@Value("${destruction.process.type:}")
	private String processType;

	// 배치 시간
	@Value("${pams.default.schedule.time:}")
	private String defTime;

	@Value("${pams.user.corp.use:}")
	private String corpUse;

	@Value("${profiles.active:}")
	private String profilesActive;

	@Autowired
	private CommonMapper mapper;

	@Autowired
	private CarryoutMapper carryoutMapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private JavaMailSender emailSender;

	private static String isLocal = null;

	private static Map<String, String> messgeMap = new HashMap<String, String>();

	private static List<Map<String, Object>> detectionCodeList;

	private static Map<String, Map<String, Object>> maskingDataMap = new HashMap<String, Map<String, Object>>();

	private static Map<String, Object> fileSettingMap = new HashMap<String, Object>();

	private static String iv;

	private static Key keySpec;

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * 초기 암호화 키 세팅
	 */
	private void initKey() {

		if (keySpec == null) {
			this.iv = asekey.substring(0, 16);
			byte[] keyBytes = new byte[16];
			byte[] b = asekey.getBytes();
			int len = b.length;
			if (len > keyBytes.length) {
				len = keyBytes.length;
			}
			System.arraycopy(b, 0, keyBytes, 0, len);
			SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");

			this.keySpec = keySpec;
		}
	}

	/**
	 * 암호화 처리
	 * 
	 * @param str
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws UnsupportedEncodingException
	 */
	public String encryptAES(String str) {

		String enStr = "";

		if (str == null || "".equals(str)) {
			return enStr;
		}

		try {
			initKey();
			Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
			byte[] encrypted = c.doFinal(str.getBytes());
			enStr = new String(Base64.encodeBase64(encrypted));
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			log.error(" " + e.getCause());
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			log.error(" " + e.getCause());
		} catch (NoSuchAlgorithmException e) {
			log.error(" " + e.getCause());
		} catch (NoSuchPaddingException e) {
			log.error(" " + e.getCause());
		}

		return enStr;
	}

	/**
	 * 복호화 처리
	 * 
	 * @param str
	 * @return
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws InvalidKeyException
	 * @throws InvalidAlgorithmParameterException
	 * @throws UnsupportedEncodingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 */
	public String decryptAES(String str) {

		String deStr = "";

		if (str == null || "".equals(str)) {
			return deStr;
		}

		try {
			initKey();
			Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
			c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes()));
			byte[] byteStr = Base64.decodeBase64(str.getBytes());
			deStr = new String(c.doFinal(byteStr));
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			log.error(" " + e.getCause());
		} catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
			log.error(" " + e.getCause());
		} catch (IllegalBlockSizeException | BadPaddingException e) {
			log.error(" " + e.getCause());
		}

		return deStr;
	}

	public List<Map<String, Object>> getExcelData(MultipartFile excelFile, String[] titles, int startRow, int startCell)
			throws IOException {

		List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();

		String originFileName = excelFile.getOriginalFilename();

		String fileType = "";

		if (originFileName != null) {
			int subIndex = originFileName.indexOf('.');
			fileType = originFileName.substring(subIndex + 1);
		}

		Workbook workbook = null;

		if ("xlsx".equals(fileType)) {
			workbook = new XSSFWorkbook(excelFile.getInputStream());
		} else if ("xls".equals(fileType)) {
			workbook = new HSSFWorkbook(excelFile.getInputStream());
		}

		if (workbook != null && workbook.getSheetAt(0) != null) {

			Sheet worksheet = workbook.getSheetAt(0);

			int rowSize = worksheet.getPhysicalNumberOfRows();

			for (int ri = startRow - 1; ri < rowSize; ri++) {

				Row row = worksheet.getRow(ri);

				Map<String, Object> excelDataMap = new HashMap<String, Object>();

				// 첫번재가 비어있으면 그만두기
				Cell fCell = row.getCell(startCell - 1);

				if (fCell == null || CellType.BLANK.equals(fCell.getCellType())) {
					break;
				}

				for (int ti = 0; ti < titles.length; ti++) {

					Cell cell = row.getCell(ti + startCell - 1);

					if (cell != null && !CellType.BLANK.equals(fCell.getCellType())) {
						if (CellType.STRING.equals(cell.getCellType())) {
							excelDataMap.put(titles[ti], cell.getStringCellValue());
						} else if (CellType.NUMERIC.equals(cell.getCellType())) {
							excelDataMap.put(titles[ti], cell.getNumericCellValue());
						}
					}

				}

				returnList.add(excelDataMap);
			}

		}

		return returnList;
	}

	/**
	 * 파일 업로드 (일반용)
	 * 
	 * @param request
	 * @param prNo
	 * @param stepCd
	 * @return 파일 아이디
	 * 
	 * @throws Exception
	 */

	
	 public Map<String,Object> saveFils(MultipartHttpServletRequest request, Object prNo, String stepCd) throws PAMsException {
	 
		 List<MultipartFile> fileList = request.getFiles("files"); 
		 Map<String, String[]> param = request.getParameterMap(); 
		 String[] deletefileSns = param.get("deletefiles"); 
		 String fileId = request.getParameter("fileId");
		 
		 return saveFils(fileId, fileList, deletefileSns, prNo, stepCd, false, 1); 
	 }
	 

	/**
	 * 파일 업로드 (일반용)
	 * 
	 * @param request
	 * @param prNo
	 * @param stepCd
	 * @return 파일 아이디
	 * 
	 * @throws Exception
	 */
	public Map<String, Object> saveFils(MultipartHttpServletRequest request, Object prNo, String stepCd, boolean isDestruction) throws PAMsException {

		List<MultipartFile> fileList = request.getFiles("files");
		Map<String, String[]> param = request.getParameterMap();
		String[] deletefileSns = param.get("deletefiles");
		String fileId = request.getParameter("FL_ID");

		Map<String, Object> FileInfoReturnMap = new HashMap<String, Object>();

		FileInfoReturnMap = saveFileInfo(request, fileList, fileId, prNo);// insert into FL_INFO

		Object fileID = FileInfoReturnMap.get("FL_ID");
		Object fileNo = FileInfoReturnMap.get("FL_NO");

		return saveFils(fileID, fileList, deletefileSns, prNo, stepCd, isDestruction, fileNo);
	}

	/**
	 * INSERT INTO FL_INFO
	 * 
	 * @throws PAMsException
	 */
	public Map<String, Object> saveFileInfo(MultipartHttpServletRequest request, List<MultipartFile> fileList,
			Object fileId, Object prNo) throws PAMsException {

		Map<String, Object> returnMap = new HashMap<String, Object>();
		Map<String, Object> deleteParamMap = new HashMap<String, Object>();

		// 파일 아이디 없을 경우 신규 생성
		if (fileId == null || "".equals(fileId) || "null".equals(fileId)) {
			fileId = mapper.getNewFileId();
		}
		Object aplNo = request.getParameter("APL_NO");
		Object dlmCd = request.getParameter("TERMINATED");
		Object ecdCd = request.getParameter("CHARACTERSET");
		Object qteCd = request.getParameter("ENCLOSED");
		Object hdrCd = request.getParameter("HEADER");
		Object fileTyCd = request.getParameter("FL_TY_CD");
		Object flNo = request.getParameter("FL_NO");

		//결합신청자 번호, 프로젝트 번호, FL_TY_CD가 같은 데이터 삭제
		
		deleteParamMap.put("APL_NO", aplNo);
		deleteParamMap.put("PRJCT_NO", prNo);
		deleteParamMap.put("FL_TY_CD", fileTyCd);
		
		int seq = mapper.getFileMaxSeq(fileId);

		List<Integer> uploadFileSns = new ArrayList<Integer>();
		List<String> uploadFiles = new ArrayList<String>();

		Map<String, Object> fileInfoData = new HashMap<String, Object>();

		fileInfoData.put("PRJCT_NO", prNo);
		fileInfoData.put("APL_NO", aplNo);
		fileInfoData.put("FL_ID", fileId);

		fileInfoData.put("DLM_CD", dlmCd);
		fileInfoData.put("ECD_CD", ecdCd);
		fileInfoData.put("QTE_CD", qteCd);
		fileInfoData.put("HDR_CD", hdrCd);
		fileInfoData.put("FL_TY_CD", fileTyCd);
		fileInfoData.put("USR_NO", getLoginUserNo());
		
		returnMap.put("FL_ID", fileId);
		
		if( flNo == null || "".equals(flNo) || "null".equals(flNo) ) {
		    returnMap.put("FL_NO", mapper.selectMaxFlNo());  
		} else {
		    returnMap.put("FL_NO", flNo);
		}
		
		deleteParamMap.put("FL_NO", returnMap.get("FL_NO"));
		mapper.deleteFileClmInfo(deleteParamMap);
		
		// 파일 추가
		for (int fi = 0; fi < fileList.size(); fi++) {
			MultipartFile mf = fileList.get(fi);

			// 파일 사이즈 bytes > MB
			Long fileSize = mf.getSize() / 1024 / 1024;

			if (fileSize == 0) {
				fileSize = 1L;
			}

			String originFileName = mf.getOriginalFilename();

			try {

				// 파일생성

				fileInfoData.put("FL_NO", returnMap.get("FL_NO"));
				fileInfoData.put("FL_NM", originFileName);

				// insert into FL_INFO
				mapper.addFileInfo(fileInfoData);

				uploadFileSns.add(seq + fi);

			} catch (Exception e) {

				for (int di = 0; di < uploadFileSns.size(); di++) {
					int deleteSn = uploadFileSns.get(di);
					File deleteFile = new File(uploadFiles.get(di));

					if (deleteFile.exists()) {
						deleteFile.delete();
					}

					fileInfoData.put("FL_SN", deleteSn);

					mapper.deleteFile(fileInfoData);
				}

				throw new PAMsException("err" + " " + e.getCause());
			}
		}

		return returnMap;
	}

	/**
	 * 파일 업로드 (일반/완전파기)
	 * 
	 * @param request
	 * @param prNo
	 * @param stepCd
	 * @param isDestruction
	 * @return 파일 아이디
	 * 
	 * @throws PAMsException
	 */
	public Map<String, Object> saveFils(Object fileId, List<MultipartFile> fileList, Object[] deletefileSns,
			Object prNo, String stepCd, boolean isDestruction, Object fileNo) throws PAMsException {

		Map<String, Object> saveFilsReturnMap = new HashMap<String, Object>();

		saveFilsReturnMap.put("FL_ID", fileId);
		saveFilsReturnMap.put("FL_NO", fileNo);
		if (deletefileSns != null && deletefileSns.length > 0) {
			for (Object deletefileSn : deletefileSns) {
				deleteFile(prNo, fileId, deletefileSn, isDestruction);
			}
		}

		if (fileList == null || fileList.size() == 0) {
			return saveFilsReturnMap;
		}

		// 파일 아이디 없을 경우 신규 생성
		if (fileId == null || "".equals(fileId) || "NULL".equals(fileId) || "null".equals(fileId) || "Null".equals(fileId)) {
			fileId = mapper.getNewFileId();
		}

		String path = getFilepath();

		path += "/" + prNo + "/" + stepCd;

		File fileDir = new File(path);
		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		int seq = mapper.getFileMaxSeq(fileId);
		List<Integer> uploadFileSns = new ArrayList<Integer>();
		List<String> uploadFiles = new ArrayList<String>();

		Map<String, Object> fileData = new HashMap<String, Object>();

		fileData.put("USR_NO", getLoginUserNo());
		fileData.put("FL_ID", fileId);

		// 파일 추가
		for (int fi = 0; fi < fileList.size(); fi++) {
			MultipartFile mf = fileList.get(fi);

			// 파일 사이즈 bytes > MB
			Long fileSize = mf.getSize() / 1024 / 1024;

			if (fileSize == 0) {
				fileSize = 1L;
			}

			String originFileName = mf.getOriginalFilename();

			// 원본 파일 명
			String saveFileName = String.format("%s_%d", fileId, seq + fi);
			
			try {

				// 파일생성
				mf.transferTo(new File(path, saveFileName));

				fileData.put("FL_SN", seq + fi);
				fileData.put("FL_NM", originFileName);
				fileData.put("FL_SIZE", fileSize);

				if (isDestruction) {
					fileData.put("INDVDLINFO_YN", "Y");
				}

				if (originFileName != null && originFileName.indexOf('.') > 0) {
					String[] subIndex = originFileName.split("\\.");

					String fileType = subIndex[subIndex.length - 1];

					fileData.put("FL_EXTSN", fileType);
				} else {
					fileData.put("FL_EXTSN", "");
				}
				fileData.put("FL_PTH", path + "/" + saveFileName);

				// insert into CMM_FILE
				mapper.addFile(fileData);

				uploadFileSns.add(seq + fi);
				uploadFiles.add(path + "/" + saveFileName);

			} catch (Exception e) {

				for (int di = 0; di < uploadFileSns.size(); di++) {
					int deleteSn = uploadFileSns.get(di);
					File deleteFile = new File(uploadFiles.get(di));

					if (deleteFile.exists()) {
						deleteFile.delete();
					}

					fileData.put("FL_SN", deleteSn);

					mapper.deleteFile(fileData);
				}

				throw new PAMsException("err" + " " + e.getCause());
			}
		}

		return saveFilsReturnMap;
	}

	/**
	 * 파일경로 불러오기
	 * 
	 * @return
	 */
	public String getFilepath() {

		String path = filepath;

		if (getCheckLocal()) {
			path = "c:/PAMs/FILE";
		}

		return path;

	}

	/**
	 * 파일정보 조회
	 * 
	 * @param fileId
	 * @return
	 */
	public List<Map<String, Object>> getFileInfos(Object fileId) {
		return mapper.getFileInfos(fileId);
	}

	/**
	 * 파일 다운로드
	 * 
	 * @param param
	 * @return
	 * @throws IOException
	 */
	public ResponseEntity<InputStreamResource> getFile(Map<String, Object> param) {

		Map<String, String> fileInfo = mapper.getFileInfo(param);
		HttpSession session = getLoginSession();
		Object indvdlinfoCheckYN = "N";

		if (session != null && session.getAttribute("INDVDLINFO_CHECK_Y") != null) {
			indvdlinfoCheckYN = session.getAttribute("INDVDLINFO_CHECK_Y");
		}

		if (session != null) {
			session.removeAttribute("INDVDLINFO_CHECK_Y");
		}

		if (fileInfo != null) {

			if ("Y".equals(fileInfo.get("INDVDLINFO_YN"))) {

				if (!"Y".equals(indvdlinfoCheckYN)) {
					return null;
				}

			}

			try {
			    
				String filePath = fileInfo.get("FL_PTH");
				String fileNm = fileInfo.get("FL_NM");
				String fileType = fileInfo.get("FL_EXTSN");
				String fileSize = String.valueOf(fileInfo.get("FL_SIZE"));

				fileNm = new String(fileNm.getBytes("euc-kr"), "ISO-8859-1");

				File target = new File(filePath);

				if (target.exists()) {

					HttpHeaders respHeaders = new HttpHeaders();

					respHeaders.set("Accept-Ranges", "bytes");
					respHeaders.set("Range", "bytes=" + fileSize + "-");
					
					MediaType mediaType = null;
					
					if (fileType != null && !"".equals(fileType)) {
						mediaType = new MediaType("text", fileType);
						respHeaders.setContentType(mediaType);
					}
					
					respHeaders.setContentDispositionFormData("attachment", fileNm);
					InputStreamResource isr = new InputStreamResource(new FileInputStream(target));
					
					ResponseEntity<InputStreamResource> returnEntity = new ResponseEntity<InputStreamResource>(isr, respHeaders, HttpStatus.OK);
					
					return returnEntity;
				}

			} catch (IOException e) {

				// 파일 오류 발생
				log.error("confirm err : " + e.getCause());

			}

		}

		return null;
	}

	/**
	 * 완전 파기
	 * 
	 * @param fileId
	 * @param sn
	 * @param isDestruction
	 */
	public void deleteFile(Object prNo, Object fileId, Object sn, boolean isDestruction) {

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("FL_ID", fileId);
		param.put("FL_SN", sn);

		// 완전 파기 여부
		if (isDestruction) {

			destructionFile(prNo, fileId, sn);

		} else {

			Map<String, String> fileInfo = mapper.getFileInfo(param);

			File deleteFile = new File(fileInfo.get("FL_PTH"));

			if (deleteFile.exists()) {
				deleteFile.delete();
			}

			mapper.deleteFile(param);
		}

	}

	/**
	 * 코드 그룹으로 코드 리스트 조회
	 * 
	 * @param grpCd
	 * @return
	 */
	public List<Map<String, Object>> getCodeList(String grpCd) {

		Map<String, Object> codeMap = new HashMap<String, Object>();

		codeMap.put("GRP_CD", grpCd);

		return getCodeList(codeMap);
	}

	/**
	 * 코드 명 조회
	 * 
	 * @param grpCd
	 * @param cd
	 * @return
	 */
	public String selectCodeNm(Object grpCd, Object cd) {

		Map<String, Object> codeMap = new HashMap<String, Object>();

		codeMap.put("GRP_CD", grpCd);
		codeMap.put("CD", cd);

		return mapper.selectCodeNm(codeMap);
	}

	/**
	 * 코드 리스트를 조회
	 * 
	 * @param param GRP_CD : 코드 그룹 META_1~10 : 해당 메타 코드 NOT_META_1~10 : 해당 메타 외 코드
	 * @return
	 */
	public List<Map<String, Object>> getCodeList(Map<String, Object> param) {
		return mapper.selectCodeList(param);
	}

	/**
	 * 코드 정보를 조회
	 * 
	 * @param grpCd
	 * @param cd
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getCodeInfo(Object grpCd, Object cd) {
		Map<String, Object> codeMap = new HashMap<String, Object>();
		codeMap.put("GRP_CD", grpCd);
		codeMap.put("CD", cd);

		return mapper.getCodeInfo(codeMap);
	}

	/**
	 * 부서 이름을 조회
	 * 
	 * @param GP_CD
	 * @return Map<String,Object>
	 */
	public Map<String, Object> getGroupNm(Object gp_cd) {
		Map<String, Object> groupMap = new HashMap<String, Object>();
		groupMap.put("GP_CD", gp_cd);

		return mapper.selectGroupNm(groupMap);
	}

	/**
	 * 프로젝트 리스트 조회
	 * 
	 * @param param SCH_PRO_NO PRO_STEP_CD ROLE_CD
	 * 
	 * @return
	 */
	public List<Map<String, Object>> getProjectList(Map<String, Object> param) {

		// 세션에서 정보 조회
		HttpSession httpSession = getLoginSession();

		if (httpSession != null) {
			param.put("USR_NO", httpSession.getAttribute("USR_NO"));
			param.put("USR_GP_CD", httpSession.getAttribute("USR_GP_CD"));

			int checkMenuAuth = checkMenuAuthInfo(httpSession.getAttribute("USR_NO"), "777");

			if (checkMenuAuth > 0) {
				param.put("ALL_LIST", "Y");
			}

		}

		return mapper.selectProjectList(param);

	}

	/**
	 * 결합 프로젝트 리스트 조회
	 * 
	 * @param param GRP_CD : 코드 그룹 META_1~5 : 해당 메타 코드 NOT_META_1~5 : 해당 메타 외 코드
	 * @return
	 */
	public List<Map<String, Object>> getJoinProjectList(Map<String, Object> param) {

		// 세션에서 정보 조회
		HttpSession httpSession = getLoginSession();

		if (httpSession != null) {
			param.put("USR_NO", httpSession.getAttribute("USR_NO"));
			param.put("USR_GP_CD", httpSession.getAttribute("USR_GP_CD"));

			int checkMenuAuth = checkMenuAuthInfo(httpSession.getAttribute("USR_NO"), "777");

			if (checkMenuAuth > 0) {
				param.put("ALL_LIST", "Y");
			}

		}

		return mapper.selectJoinProjectList(param);

	}

	/**
	 * 해당 페이지 권한 확인
	 * 
	 * @param userNo
	 * @param authCd
	 * @return
	 */
	public int checkMenuAuthInfo(Object userNo, String authCd) {

		int checkMenuAuth = 0;

		String tagetUrl = getRequestPageUrl();

		if (tagetUrl != null && userNo != null) {

			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("USR_NO", userNo);
			paramMap.put("MENU_URL", tagetUrl);
			paramMap.put("CHECK_AUTH_CD", authCd);

			checkMenuAuth = mapper.checkMenuAuthInfo(paramMap);

		}

		return checkMenuAuth;

	}

	/**
	 * 호출한 URL 리턴
	 * 
	 * @return
	 */
	public String getRequestPageUrl() {

		return getRequestPageUrl(getRequest());
	}

	public String getRequestPageUrl(HttpServletRequest req) {

		String tagetUrl = null;

		if (req != null) {
			String method = req.getMethod();

			if ("GET".equals(method) || "get".equals(method)) {

				tagetUrl = req.getRequestURI();
			} else if ("POST".equals(method) || "post".equals(method)) {

				String fullUrl = req.getHeader("REFERER");
				String host = req.getHeader("HOST");
				if (fullUrl != null && host != null) {
					String[] urls = fullUrl.split(host);

					if (urls != null && urls.length == 2) {
						tagetUrl = urls[1];
					}
				}

			}

		}

		return tagetUrl;
	}

	/**
	 * 메세지 조회
	 * 
	 * @param msgKey
	 * @param local
	 * @return
	 */
	public String getLocaleMessge(String msgKey, String local) {

		if (messgeMap.isEmpty()) {

			List<Map<String, String>> msgList = mapper.selectMsgList();

			if (msgList != null) {
				for (Map<String, String> msgMap : msgList) {

					// 임시로 수정가능 메세지 표시
//                  if(getCheckLocal()) {                       
//                      messgeMap.put(msgMap.get("MSG_ID"), "(**) " + msgMap.get("MSG_CN"));
//                  }else {
					messgeMap.put(msgMap.get("MSG_ID"), msgMap.get("MSG_CN"));
//                  }
				}
			}
		}

		return messgeMap.get(msgKey + "_" + local);
	}

	/**
	 * 테이블 삭제 처리
	 * 
	 * @param param
	 * @return
	 */
	public int dropProjectTable(String tableNm) {

		int rtn = 0;

		if (tableNm != null) {

			Map<String, Object> projectMap = new HashMap<String, Object>();

			projectMap.put("DROP_PRO_TABLE", tableNm.replaceAll("[^A-Za-z0-9_]", ""));
			projectMap.put("TBL_NM", tableNm.replaceAll("[^A-Za-z0-9_]", ""));

			int cnt = mapper.chkTableCnt(projectMap);
			if (cnt > 0) {
				rtn = mapper.dropProjectTable(projectMap);
			}

		}

		return rtn;
	}

	/**
	 * 테이블 데이터 복사
	 * 
	 * @param selectTableNm
	 * @param copyTableNm
	 * @return
	 */
	public int copyProjectTable(String selectTableNm, String copyTableNm, List<Map<String, Object>> columnList) {

		Map<String, Object> projectMap = new HashMap<String, Object>();

		projectMap.put("SELECT_TABLE", selectTableNm);
		projectMap.put("COPY_TABLE", copyTableNm);
		projectMap.put("COLUMN_LIST", columnList);

		return mapper.copyProjectTable(projectMap);
	}

	/**
	 * 프로젝트 테이블 생성 규칙 리턴
	 *
	 * @param projectNo
	 * @param type      R  :  원본, K : 키, D : 데이터
	 * @return
	 */
	public String getCreatProjectTableNm(Object flNo, Object type) {
	    String tableNm = new String();

	    if("K".equals(type)) {
	        tableNm = "KEY" + "_" +flNo;
	    } else if ("D".equals(type)) {
	        tableNm = "DATA" + "_" +flNo;
	    } else if ("R".equals(type)) {
			tableNm = "RAW" + "_" +flNo;
		} else {
	        tableNm = "BIND" + "_" +flNo;
	    }

	    tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 프로젝트 테이블 생성 규칙 리턴
	 * 
	 * @param projectNo
	 * @param type      K : 키, D : 데이터, KB : 키 결합, DB : 데이터 결합
	 * @return
	 */
	public String getCreatJoinProjectTableNm(Object projectNo) {

		String tableNm = "DATA_" + projectNo + "_JOIN";

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 결합키 테이블 생성 규칙 리턴
	 * 
	 * @param projectNo
	 * @param type      0 : 초기 테이블, 1 : 전처리 테이블, 2 : 가명처리 테이블, 3 : 결합 테이블
	 * @return
	 */
	public String getCreatKeyTableNm(Object projectNo, int type) {

		String tableNm = "KEY_" + projectNo + "_" + type;

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 모의 결합 키 테이블 생성 규칙 리턴
	 * 
	 * @param flNo
	 * @return
	 */
	public String getCreatKeyTblNm(Object flNo) {

		String tableNm = "KEY_" + flNo;

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 모의 결합 데이터 테이블 생성 규칙 리턴
	 * 
	 * @param flNo
	 * @return
	 */
	public String getCreatDataTblNm(Object flNo) {

		String tableNm = "DATA_" + flNo;

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 결합 테이블 생성 규칙 리턴
	 * 
	 * @param projectNo
	 * @return
	 */
	public String getCreatBindProjectTableNm(Object flNo) {

		String tableNm = "BIND_" + flNo;

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 프로젝트 결합키 테이블 생성 규칙 리턴
	 * 
	 * @param projectNo
	 * @return
	 */
	public String getCreatProjectJoinKeyTable(Object projectNo) {

		String tableNm = "DATA_" + projectNo + "_KEY";

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;
	}

	/**
	 * 프로젝트 매핑 테이블 생성 규칙 리턴
	 * 
	 * @param projectNo
	 * @return
	 */
	public String getCreatProjectMappingTable(Object projectNo) {

		String tableNm = "DATA_" + projectNo + "_MAPPING";

		tableNm = tableNm.replaceAll("[^A-Za-z0-9_]", "");

		return tableNm;

	}

	/**
	 * 테이블 정보명 조회
	 * 
	 * @param param
	 * @return
	 */
	public Map<String, String> getProjectTable(Object projectNo) {

		Map<String, Object> projectMap = new HashMap<String, Object>();

		projectMap.put("PRJCT_NO", projectNo);
		projectMap.put("ORG_TBL_NM", getCreatProjectTableNm(projectNo, "K"));
		projectMap.put("HNDL_TBL_NM", getCreatProjectTableNm(projectNo, "D"));

		List<Map<String, String>> projectTableList = mapper.selectProjectTable(projectMap);

		Map<String, String> returnMap = new HashMap<String, String>();

		if (projectTableList != null) {
			for (int ti = 0; ti < projectTableList.size(); ti++) {
				Map<String, String> projectTable = projectTableList.get(ti);
				returnMap.put(projectTable.get("TBL_TY"), projectTable.get("TABLE_NAME"));
				returnMap.put(projectTable.get("TBL_TY") + "_ROWS", String.valueOf(projectTable.get("TABLE_ROWS")));
			}
		}

		return returnMap;
	}
	
	/**
	 * 모의결합 테이블 정보명 조회
	 * 
	 * @param param
	 * @return
	 */
	public Map<String, String> getCmbProjectTable(Object aplNo, Object prjctNo) {

		Map<String, Object> projectMap = new HashMap<String, Object>();

		projectMap.put("APL_NO", aplNo);
		projectMap.put("PRJCT_NO", prjctNo);
		projectMap.put("KEY_TBL_NM", getCreatProjectTableNm(aplNo, "K"));
		projectMap.put("DATA_TBL_NM", getCreatProjectTableNm(aplNo, "D"));
		List<Map<String, String>> projectTableList = mapper.selectCmbProjectTable(projectMap);
		Map<String, String> returnMap = new HashMap<String, String>();

		/*
		 * if (projectTableList != null) { for (int ti = 0; ti <
		 * projectTableList.size(); ti++) { Map<String, String> projectTable =
		 * projectTableList.get(ti); returnMap.put(projectTable.get("TBL_TY"),
		 * projectTable.get("TABLE_NAME")); returnMap.put(projectTable.get("TBL_TY") +
		 * "_ROWS", String.valueOf(projectTable.get("TABLE_ROWS"))); } }
		 */

		return returnMap;
	}



	/**
	 * 테이블에 해당 컬럼이 존재하는지 체크
	 * 
	 * @param tableNm
	 * @param colmnNm
	 * @return
	 */
	private int checkTableColumn(Object tableNm, String colmnNm) {

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("TABLE_NM", tableNm);
		param.put("COLUMN_NM", colmnNm);

		return mapper.checkTableColumn(param);

	}

	/**
	 * 테이블이 존재 하는지 체크
	 * 
	 * @param tableNm
	 * @param colmnNm
	 * @return
	 */
	public int checkCreatTable(Object tableNm) {

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("TABLE_NM", tableNm);

		return mapper.checkCreatTable(param);

	}

	/**
	 * 테이블 정보명 조회
	 * 
	 * @param param
	 * @param type  0 : 초기 테이블, 1 : 전처리 테이블, 2 : 가명처리 테이블
	 * @return
	 */
	
	 public String getProjectTable(Object projectNo, Object type) {
	 
		 String tableNm = null;
		 
		 Map<String, String> tableMap = getProjectTable(projectNo);
		 
		 if(type =="K") { 
			 tableNm = tableMap.get("HNDL_TBL"); 
		 }else if(type =="D") {
			 tableNm = tableMap.get("ALIAS_TBL"); 
		 }
		 
		 return tableNm; 
	  }
	 
	
	/**
	 * 모의결합 테이블 정보명 조회
	 * 
	 * @param param
	 * @param type  K : 결합키 테이블, D : 데이터 테이블
	 * @return
	 */
	public String getCmbProjectTable(Object aplNo, Object prjctNo, Object type) {

		String tableNm = null;

		Map<String, String> tableMap = getCmbProjectTable(aplNo, prjctNo);
		
		if(type =="K") {
			tableNm = tableMap.get("HNDL_TBL");
		}else if(type =="D") {
			tableNm = tableMap.get("ALIAS_TBL");
		}

		return tableNm;
	}
	

	public Map<String, Object> getJoinProjectTable(Object joinNo) {

		Map<String, Object> projectMap = new HashMap<String, Object>();

		projectMap.put("JOIN_NO", joinNo);
		// projectMap.put("JOIN_TBL_NM", getCreatJoinProjectTableNm(joinNo));

		Map<String, Object> joinProjectTable = mapper.selectJoinProjectTable(projectMap);

		return joinProjectTable;

	}

	public Object getJoinProjectTableNm(Object joinNo) {

		return joinNo;
	}

	/**
	 * 프로젝트의 컬럼 조회
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getProjectColumns(Map<String, Object> param) {

		List<Map<String, Object>> projectColumns = mapper.selectCmbProjectColumns(param);
		//List<Map<String, Object>> tmpProjectColumns = mapper.selectProjectColumns(param);


		return projectColumns;
	}
	
	public String getFileNm(Map<String, Object> param) {
		String fileNm = mapper.getFIleNm(param);
		return fileNm;
	}

	/**
	 * 
	 * 결합 프로젝트의 컬럼 조회
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getJoinProjectColumns(Map<String, Object> param) {

		List<Map<String, Object>> projectColumns = mapper.selectJoinProjectColumns(param);

		return projectColumns;
	}

	/**
	 * 프로젝트의 컬럼 상세 정보 조회 비교 분석용
	 * 
	 * @param param
	 * @param type  1 : 전처리, 2 : 가명처리
	 * @return
	 */
	public Map<String, Object> getColumnDetailData(Map<String, Object> param, int type) {

		Map<String, Object> columnDataMap = new HashMap<String, Object>();

		param.put("TYPE", type);

		Map<String, Object> statBaseMap = mapper.selectStatBase(param);

		if (statBaseMap != null) {

			columnDataMap.put("STAT_BASE", statBaseMap);
			columnDataMap.put("KANO_BASE", mapper.selectKanoBase(param));

			if ("BIGINT".equals(statBaseMap.get("CLM_TY")) || "DOUBLE".equals(statBaseMap.get("CLM_TY"))
					|| "DECIMAL".equals(statBaseMap.get("CLM_TY"))) {
				// 숫자 타입만 조회 수정
				columnDataMap.put("NORM_BASE", mapper.selectNormBase(param));
				columnDataMap.put("CORR_BASE", mapper.selectCorrBase(param));
				columnDataMap.put("FREQ_BASE", mapper.selectFreqBase(param));
				if (mapper.selectSigmaBase(param) != null) {
					columnDataMap.put("SIGMA_BASE", mapper.selectSigmaBase(param));
				}

			} else {
				// 문자 타입만 조회
				String Tablenm = getCreatProjectTableNm(param.get("PRO_NO"), type);
				param.put("PRO_TABLE", Tablenm);
				columnDataMap.put("GRTP_BASE", mapper.selectGrtpBase(param));
			}
		}

		return columnDataMap;
	}

	/**
	 * 프로젝트의 컬럼 상세 정보 조회 비교 분석용 (리포트)
	 * 
	 * @param param
	 * @param type  1 : 전처리, 2 : 가명처리
	 * @return
	 */
	public Map<String, Object> getColumnDetailReportData(Map<String, Object> param, int type) {
		Map<String, Object> columnDataMap = new HashMap<String, Object>();

		param.put("TYPE", type);

		Map<String, Object> statBaseMap = mapper.selectStatBase(param);

		if (statBaseMap != null) {

			columnDataMap.put("STAT_BASE", statBaseMap);
			columnDataMap.put("KANO_BASE", mapper.selectKanoBase(param));

			if ("BIGINT".equals(statBaseMap.get("CLM_TY")) || "DOUBLE".equals(statBaseMap.get("CLM_TY"))
					|| "DECIMAL".equals(statBaseMap.get("CLM_TY"))) {
				// 숫자 타입만 조회 수정
				columnDataMap.put("NORM_BASE", mapper.selectNormBase(param));
				columnDataMap.put("CORR_BASE", mapper.selectCorrBase(param));
				columnDataMap.put("FREQ_BASE", mapper.selectFreqBase(param));
				if (mapper.selectSigmaBase(param) != null) {
					columnDataMap.put("SIGMA_BASE", mapper.selectSigmaBase(param));
				}

			} else {
				// 문자 타입만 조회
				String Tablenm = getCreatProjectTableNm(param.get("PRO_NO"), type);
				param.put("PRO_TABLE", Tablenm);
				columnDataMap.put("GRTP_BASE", mapper.selectReportGrtpBase(param));
			}
		}

		return columnDataMap;
	}

	/**
	 * 프로젝트의 컬럼 상세 정보 조회 비교 분석용 (리포트)
	 * 
	 * @param param
	 * @param type  1 : 가명처리, 2 : 결합
	 * @return
	 */
	public Map<String, Object> getColumnDetailReportJoinData(Map<String, Object> param, int join_type) {
		param.put("TYPE", 2);

		Map<String, Object> columnDataMap = new HashMap<String, Object>();

		if (join_type == 1) {
			String joinCol = (String) param.get("COL_NO");
			Pattern patternP = Pattern.compile("P([0-9]+)");
			Pattern patternC = Pattern.compile("col([0-9]+)");

			Matcher matcherP = patternP.matcher(joinCol);
			Matcher matcherC = patternC.matcher(joinCol);

			String prjct_no = new String();
			String colnm = new String();
			if (matcherP.find()) {
				prjct_no = matcherP.group(1);
				param.put("PRO_NO", prjct_no);
			}

			if (matcherC.find()) {
				colnm = matcherC.group(1);
				param.put("COL_NO", colnm);
			}

			Map<String, Object> statBaseMap = mapper.selectStatBase(param);

			if (statBaseMap != null) {

				columnDataMap.put("STAT_BASE", statBaseMap);
				columnDataMap.put("KANO_BASE", mapper.selectKanoBase(param));

				if ("BIGINT".equals(statBaseMap.get("CLM_TY")) || "DOUBLE".equals(statBaseMap.get("CLM_TY"))
						|| "DECIMAL".equals(statBaseMap.get("CLM_TY"))) {
					// 숫자 타입만 조회 수정
					columnDataMap.put("NORM_BASE", mapper.selectNormBase(param));
					columnDataMap.put("CORR_BASE", mapper.selectCorrBase(param));
					columnDataMap.put("FREQ_BASE", mapper.selectFreqBase(param));
					if (mapper.selectSigmaBase(param) != null) {
						columnDataMap.put("SIGMA_BASE", mapper.selectSigmaBase(param));
					}

				} else {
					// 문자 타입만 조회
					String Tablenm = getCreatProjectTableNm(param.get("PRO_NO"), 2);
					param.put("PRO_TABLE", Tablenm);
					columnDataMap.put("GRTP_BASE", mapper.selectReportGrtpBase(param));
				}
			}
		} else {
			Map<String, Object> statBaseMap = mapper.selectJoinStatBase(param);

			if (statBaseMap != null) {

				columnDataMap.put("STAT_BASE", statBaseMap);
				columnDataMap.put("KANO_BASE", mapper.selectJoinKanoBase(param));

				if ("BIGINT".equals(statBaseMap.get("CLM_TY")) || "DOUBLE".equals(statBaseMap.get("CLM_TY"))
						|| "DECIMAL".equals(statBaseMap.get("CLM_TY"))) {
					// 숫자 타입만 조회 수정
					columnDataMap.put("NORM_BASE", mapper.selectJoinNormBase(param));
					columnDataMap.put("CORR_BASE", mapper.selectJoinCorrBase(param));
					columnDataMap.put("FREQ_BASE", mapper.selectJoinFreqBase(param));
					columnDataMap.put("SIGMA_BASE", mapper.selectJoinSigmaBase(param));
				} else {
					// 문자 타입만 조회
					String joinCol = (String) param.get("COL_NO");
					Pattern patternP = Pattern.compile("P([0-9]+)");

					Matcher matcherP = patternP.matcher(joinCol);

					String prjct_no = new String();
					if (matcherP.find()) {
						prjct_no = matcherP.group(1);
						param.put("PRO_NO", prjct_no);
					}
					// String Tablenm = getCreatJoinProjectTableNm(param.get("JOIN_NO"));
					String col = (String) param.get("COL_NO");
					// param.put("JOIN_TABLE", Tablenm);
					param.put("col", col.subSequence(col.indexOf("_") + 1, col.length()));
					columnDataMap.put("GRTP_BASE", mapper.selectJoinReportGrtpBase(param));
				}
			}
		}
		return columnDataMap;

	}

	public List<Map<String, Object>> getGrbmList(Map<String, Object> param, int type) {

		param.put("TYPE", type);
		List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
		List<String> strArr = mapper.getGrbmCols(param);
		String col;
		for (String s : strArr) {
			col = s;
			param.put("cols", col);
			res.addAll(mapper.selectGrbmBase(param));
		}
		return res;
	}

	public List<Map<String, Object>> getKltvList(Map<String, Object> param, int type) {

		param.put("TYPE", type);

		return mapper.selectKltvBase(param);
	}

	public List<Map<String, Object>> getKanoBaseColumnData(Map<String, Object> param, int type) {

		param.put("TYPE", type);

		return mapper.selectKanoBase(param);
	}

	public List<Map<String, Object>> getFreqBase(Map<String, Object> param, int type) {

		param.put("TYPE", type);

		return mapper.selectFreqBase(param);
	}

	public Map<String, Object> getStatBase(Map<String, Object> param, int type) {

		param.put("TYPE", type);

		return mapper.selectStatBase(param);
	}

	public List<Map<String, Object>> getJoinGrbmList(Map<String, Object> param) {
		List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
		List<String> strArr = mapper.getGrbmCols(param);
		String col;
		for (String s : strArr) {
			col = s;
			param.put("cols", col);
			res.addAll(mapper.selectJoinGrbmBase(param));
		}
		return res;
	}

	public List<Map<String, Object>> getJoinKltvList(Map<String, Object> param) {

		return mapper.selectJoinKltvBase(param);
	}

	public List<Map<String, Object>> getJoinKanoBaseColumnData(Map<String, Object> param) {

		return mapper.selectJoinKanoBase(param);
	}

	public List<Map<String, Object>> getJoinFreqBase(Map<String, Object> param) {

		return mapper.selectJoinFreqBase(param);
	}

	public Map<String, Object> getJoinStatBase(Map<String, Object> param) {

		return mapper.selectJoinStatBase(param);
	}

	/**
	 * 처리 테이블의 정보 조회
	 * 
	 * @param param
	 * @param type
	 * @return
	 */
	public List<Map<String, Object>> getColumnValue(Map<String, Object> param, int type) {

		List<Map<String, Object>> tmpList = null;
		List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();

		String tableNm = getProjectTable(param.get("PRO_NO"), type);
		String vals = (String) param.get("VALS");

		Map<String, String> lAggrMap = new HashMap<String, String>();

		if (param.get("L_AGGR") != null) {
			String[] lArry = param.get("L_AGGR").toString().split("//");

			for (int li = 0; li < lArry.length; li++) {
				String lData = lArry[li];
				String[] ldatas = lData.split("::");

				if (ldatas.length == 2) {
					lAggrMap.put(ldatas[0], ldatas[1]);
				}

			}
		}

		if (tableNm != null && vals != null) {
			String[] valArr = vals.split(",");

			param.put("PRO_TABLE", tableNm);
			param.put("COL_NO_ARR", valArr);

			tmpList = mapper.getColumnValueList(param);

			for (int i = 0; i < tmpList.size(); i++) {
				Map<String, Object> tmpMap = tmpList.get(i);

				tmpMap.put("L_DATA", lAggrMap.get(tmpMap.get("COL_NO") + ""));

				returnList.add(tmpMap);
			}
		}

		return returnList;
	}

	/**
	 * 
	 * 프로젝트의 데이터 조회
	 * 
	 * @param tableType      0 : 초기 테이블, 1 : 전처리 테이블, 2 : 가명처리 테이블
	 * @param param
	 * @param projectColumns
	 * @return
	 */
	public List<Map<String, Object>> selectProjectRowDatas(int tableType, Map<String, Object> param,
			List<Map<String, Object>> projectColumns) {

		List<Map<String, Object>> rowDatas = null;

		String tableNm = getProjectTable(param.get("PRO_NO"), tableType);

		if (tableNm != null && !"".equals(tableNm)) {

			param.put("PRO_TABLE", tableNm);

			if (projectColumns != null) {

				List<Map<String, Object>> checkColumnList = new ArrayList<Map<String, Object>>();

				for (Map<String, Object> projectColumnMap : projectColumns) {
					if (projectColumnMap.get("CLMN_NO") != null) {
						projectColumnMap.put("CLMN_NO",
								projectColumnMap.get("CLMN_NO").toString().replaceAll("[^A-Za-z0-9_]", ""));
					}

					checkColumnList.add(projectColumnMap);
				}

				param.put("COLUMN_LIST", checkColumnList);
			}

			if (tableType == 0) {
				rowDatas = mapper.selectProjectOrgRowDatas(param);
			} else {
				rowDatas = mapper.selectProjectRowDatas(param);
			}
		}

		return rowDatas;

	}

	
	/**
	 * 
	 * 모의결합 업로드한 데이터 조회
	 * 
	 * @param tableType      0 : 초기 테이블, 1 : 전처리 테이블, 2 : 가명처리 테이블
	 * @param param
	 * @param projectColumns
	 * @return
	 */
	public List<Map<String, Object>> selectCmbProjectRowDatas(Map<String, Object> param, List<Map<String, Object>> projectColumns) {
	    param.put("COLUMN_LIST", projectColumns);
	    
	    if(projectColumns == null || projectColumns.size() <= 0 ) {
	        return null;
	    }
	    
	    if("K".equals(param.get("FL_TY_CD"))) {
	        param.put("TBL_NM", getCreatKeyTblNm(param.get("FL_NO")));
	        return mapper.selectProjectRowDatas(param);
	    } else if("D".equals(param.get("FL_TY_CD"))) {
	        param.put("TBL_NM", getCreatDataTblNm(param.get("FL_NO")));
	        return mapper.selectProjectRowDatas(param);
	    } else {
	        param.put("TBL_NM", getCreatRawTblNm(param.get("FL_NO")));
	        return mapper.selectProjectRowDatas(param);
	    }
	    //String tableType = (String) param.get("FL_TY_CD");
	    


		//String tableNm = getCmbProjectTable(param.get("APL_NO"), param.get("PRJCT_NO"), tableType);

		/*
		 * if (tableNm != null && !"".equals(tableNm)) {
		 * 
		 * param.put("PRO_TABLE", tableNm);
		 * 
		 * if (projectColumns != null) {
		 * 
		 * List<Map<String, Object>> checkColumnList = new ArrayList<Map<String,
		 * Object>>();
		 * 
		 * for (Map<String, Object> projectColumnMap : projectColumns) { if
		 * (projectColumnMap.get("CLMN_NO") != null) { projectColumnMap.put("CLMN_NO",
		 * projectColumnMap.get("CLMN_NO").toString().replaceAll("[^A-Za-z0-9_]", ""));
		 * }
		 * 
		 * checkColumnList.add(projectColumnMap); }
		 * 
		 * param.put("COLUMN_LIST", checkColumnList); }
		 * 
		 * 
		 * rowDatas = mapper.selectProjectRowDatas(param);
		 * 
		 * }
		 */

	}
	/**
	 * 
	 * 결합 프로젝트의 데이터 조회
	 * 
	 * @param param
	 * @param projectColumns
	 * @return
	 */
	public List<Map<String, Object>> selectJoinProjectRowDatas(Map<String, Object> param,
			List<Map<String, Object>> joinProjectColumns) {

		List<Map<String, Object>> rowDatas = null;

		Map<String, Object> joinTableData = getJoinProjectTable(param.get("JOIN_NO"));

		if (joinTableData != null && joinTableData.get("TABLE_NAME") != null) {

			param.put("JOIN_TABLE", joinTableData.get("TABLE_NAME"));
			param.put("COLUMN_LIST", joinProjectColumns);

			rowDatas = mapper.selectJoinProjectRowDatas(param);
		}

		return rowDatas;

	}

	/**
	 * 스케줄 상태 변경
	 * 
	 * @param param
	 * @return
	 */
	public void updateScheduleProgrsCd(Map<String, Object> param) {

		if (param.get("RSVTIME") == null || "".equals(param.get("RSVTIME"))) {

			param.put("RSVTIME", getDefTime());
		}

		mapper.updateScheduleProgrsCd(param);

	}

	/**
	 * 프로젝트 상태 저장
	 * 
	 * @param param
	 */
	public void updateProjectStep(Map<String, Object> param) {

		if (param.get("USR_NO") == null) {
			Object userNo = getLoginUserNo();

			param.put("USR_NO", userNo);
		}

		mapper.updateProjectStep(param);

	}

	/**
	 * 테이블 컨수 조회
	 * 
	 * @param FL_NO
	 * @param FL_TY_CD
	 * @return
	 */
	public Object getTableRowCnt(Object flNo, Object type) {
	    Map<String, Object> param = new HashMap<>();
	    
	    if("K".equals(type)) {
	        param.put("TBL_NM", getCreatKeyTblNm(flNo));
	        return mapper.selectTableRowCnt(param);
	    } else if("D".equals(type)) {
	        param.put("TBL_NM", getCreatDataTblNm(flNo));
	        return mapper.selectTableRowCnt(param);
	    }
	    
	    return null;
	}

	/**
	 * 스케줄 조회
	 * 
	 * @param param
	 */
	public List<Map<String, Object>> selectScheduleList(Map<String, Object> param) {

		return mapper.selectUserScheduleList(param);
	}

	/**
	 * 세션 정보 조회
	 * 
	 * @return
	 */
	public HttpServletRequest getRequest() {

		HttpServletRequest req = null;

		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

		if (requestAttributes != null) {
			req = ((ServletRequestAttributes) requestAttributes).getRequest();
		}

		return req;

	}

	/**
	 * 세션 정보 조회
	 * 
	 * @return
	 */
	public HttpSession getLoginSession() {

		HttpServletRequest req = getRequest();

		if (req != null) {

			HttpSession session = req.getSession();
			return session;

		}

		return null;

	}

	/**
	 * 로그인 사용자 조회 0 일경우 시스템에서 처리
	 * 
	 * @return
	 */
	public Object getLoginUserNo() {

		HttpSession session = getLoginSession();

		if (session == null) {
			return 0;
		}

		Object userNo = session.getAttribute("USR_NO");

		return userNo;

	}

	/**
	 * 로그인 사용자 조회 0 일경우 시스템에서 처리
	 * 
	 * @return
	 */
	public Object getLoginUserID() {

		HttpSession session = getLoginSession();

		if (session == null) {
			return 0;
		}

		Object userId = session.getAttribute("USR_ID");

		return userId;

	}

	/**
	 * 코드 이름 반환
	 * 
	 * @return
	 */
	public String getCdNm(Map<String, Object> codeParam) {

		return mapper.getCdNm(codeParam);
	}

	public Map<String, Object> getCheckData(Object proNo) {

		Map<String, Object> proMap = new HashMap<String, Object>();
		proMap.put("PRJCT_NO", proNo);

		Map<String, Object> checkData = mapper.selectpTotalCheckData(proMap);

		if (checkData == null) {
			checkData = mapper.selectpMaxTotalCheckData();
		}

		return checkData;
	}

	public Map<String, Object> getJoinCheckData(Object joinNo) {

		Map<String, Object> proMap = new HashMap<String, Object>();
		proMap.put("JOIN_NO", joinNo);

		Map<String, Object> checkData = mapper.selectJTotalCheckData(proMap);

		if (checkData == null) {
			checkData = mapper.selectpMaxTotalCheckData();
		}

		return checkData;
	}

	public List<Map<String, Object>> getMenuList(Map<String, Object> param) {
		param.put("USR_NO", getLoginUserNo());
		param.put("ROLE_COUNT", mapper.getRoleCount());

		List<Map<String, Object>> userRoleList = mapper.getUserRole(param);

		Map<String, Object> userRoleMap = new HashMap<>();
		Map<String, Object> menuRoleMap = new HashMap<>();

		for (int i = 0; i < userRoleList.size(); i++) {
			userRoleMap = userRoleList.get(i);

			param.put("USER_ROLE_CD" + (i + 1), "init");
			param.put("USER_ROLE_CD" + (i + 1), userRoleMap.get("ROLE_CD"));
		}

		List<Map<String, Object>> menuRoleList = mapper.getMenuRole(param);

		for (int i = 0; i < menuRoleList.size(); i++) {
			menuRoleMap = menuRoleList.get(i);

			menuRoleMap.put("MENU_ID" + (i + 1), "init");
			menuRoleMap.put("MENU_ID" + (i + 1), menuRoleMap.get("MENU"));
		}

		return mapper.getMenuList(param);
	}

	/**
	 * 사용자 접속이력 로그인, 로그아웃
	 * 
	 * @param param
	 * @param type  1 : 로그인, 2 : 로그아웃, 3 : 처리이력
	 * @return
	 * @throws IOException
	 */
	public String recordHist(Map<String, Object> param) {

		if (loginLogLevel == 0) {
			return null;
		}

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String nowString = now.format(dateTimeFormatter);

		param.put("CONECT_DT", nowString);
		param.put("END_DT", nowString);

		// TYPE = 1 #login, TYPE = 2 #logout, TYPE = 3 #HIST
		if ("1".equals(param.get("TYPE"))) {
			param.put("USR_ID", getUsrId(getLoginUserNo()));
			param.put("USR_NM", getUserNm(getLoginUserNo()));

			// 로그아웃 이력이 없을 경우 로그아웃 처리
			int logoutCnt = mapper.recordConnectionLogout(param);

			if (logoutCnt > 0) {

				param.put("TYPE", "2");
				mapper.recordConnectionLogin(param);

				now = LocalDateTime.now();
				param.put("CONECT_DT", now.format(dateTimeFormatter));

			}

			param.put("TYPE", "1");
			mapper.recordConnectionLogin(param);

			return loginSuccessUrl;

		} else if ("2".equals(param.get("TYPE"))) {
			mapper.recordConnectionLogin(param);
			mapper.recordConnectionLogout(param);

			return logoutSuccessUrl;
		}

		return null;
	}

	/**
	 * 쿼리 이력 저장
	 * 
	 * @param param
	 */
	public void recordSqlHist(Map<String, Object> param) {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String nowString = now.format(dateTimeFormatter);

		param.put("CONECT_DT", nowString);
		param.put("END_DT", nowString);

		if (getLoginSession() != null) {
			param.put("USR_NO", getLoginUserNo());
			param.put("USR_ID", getUsrId(getLoginUserNo()));
			param.put("USR_NM", getUserNm(getLoginUserNo()));

			if ("J".equals(getLoginSession().getAttribute("SEL_PRO_TYP"))) {
				param.put("PRJCT_NO", getLoginSession().getAttribute("SELECT_JOIN_PRO_NO"));
				param.put("IS_JOIN", "Y");

			} else {
				param.put("PRJCT_NO", getLoginSession().getAttribute("SELECT_PRO_NO"));
				param.put("IS_JOIN", "N");

			}

		} else {
			return;
		}
		if (connectLogLevel == 1) {
			mapper.recordHist(param);
		} else if (connectLogLevel == 2 && param.get("EXECUT_SQL").toString().contains("HISTINFO")) {
			param.put("EXECUT_SQL", param.get("EXECUT_SQL").toString().replaceAll("HISTINFO", ""));
			param.put("RESULT_CNT", param.get("RESULT_CNT"));
			mapper.recordHist(param);
		} else {
			return;
		}
	}

	public String getUsrId(Object usr_no) {
		Map<String, Object> param = new HashMap<String, Object>();

		if (usr_no == null) {
			return null;
		}
		param.put("USR_NO", usr_no);

		return mapper.getUsrId(param);
	}

	public String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");

		if (header != null) {
			if (header.indexOf("Trident") > -1) {
				return "IE";
			} else if (header.indexOf("Edg") > -1) {
				return "Edge";
			} else if (header.indexOf("Chrome") > -1) {
				return "Chrome";
			} else if (header.indexOf("Opera") > -1) {
				return "Opera";
			} else if (header.indexOf("iPhone") > -1 && header.indexOf("Mobile") > -1) {
				return "iPhone";
			} else if (header.indexOf("Android") > -1 && header.indexOf("Mobile") > -1) {
				return "Android";
			} else if (header.indexOf("Firefox") > -1) {
				return "Firefox";
			} else
				return header;
		}
		return header;
	}

	/*
	 * 현재 시간 구하기
	 */
	public String currentDateTime() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String nowString = now.format(dateTimeFormatter);

		return nowString;
	}

	public String currentDate() {
		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String nowString = now.format(dateTimeFormatter);

		return nowString;
	}

	public Object getUsrNo(Object usrId) {

		return mapper.getUsrNo(usrId);
	}

	public String currentConnectionIp(HttpServletRequest request) {

		return request.getRemoteAddr();
	}

	public List<Map<String, Object>> getLoginInfo() {
		Map<String, Object> param = new HashMap<>();
		param.put("USR_NO", getLoginUserNo());
		return mapper.getLoginInfo(param);
	}

	/**
	 * IP 제어
	 * 
	 * @param userNo
	 * @return
	 * @throws IOException
	 */
	public int ipCheck(Object userNo) {
		int returnCount = 1;

		Map<String, Object> param = new HashMap<>();

		param.put("USR_NO", userNo);
		param.put("BLACKLIST_TYPE", "B");
		param.put("WHITELIST_TYPE", "W");

		List<Map<String, Object>> ipCheckBlackList = mapper.checkUserBlackIpList(param);
		List<Map<String, Object>> ipCheckWhiteList = mapper.checkUserWhiteIpList(param);

		HttpServletRequest request = getRequest();

		if (request != null) {

			String clientIp = request.getRemoteAddr();

			// ipv4 check
			if (clientIp != null && clientIp.contains(".")) {
				if (ipCheckBlackList.size() != 0) {
					returnCount = ipv4BlackListCheck(ipCheckBlackList, clientIp);
				}

				if (ipCheckWhiteList.size() == 0) {
					returnCount = 1;
				} else {
					returnCount = ipv4WhiteListCheck(ipCheckWhiteList, clientIp);
				}
			}
		}

		return returnCount;
	}

	public int ipv4BlackListCheck(List<Map<String, Object>> ipCheckList, String clientIp) {
		int checkCount = 0;

		for (Map<String, Object> ip : ipCheckList) {
			String blackListIp = ip.get("IP").toString();

			if (blackListIp.contains(":")) {
				continue;
			}

			StringTokenizer blackStringTokenizer = new StringTokenizer(blackListIp, ".");
			StringTokenizer clientStringTokenizer = new StringTokenizer(clientIp, ".");

			int count = 0;

			while (blackStringTokenizer.hasMoreTokens()) {
				String blackDivision = blackStringTokenizer.nextToken();
				String clientDivision = clientStringTokenizer.nextToken();

				if (!blackDivision.equals(clientDivision) && !blackDivision.equals("*")
						&& checkCount == ipCheckList.size()) {
					return 1;
				}

				if (blackDivision.equals("*")) {
					return 0;
				}

				if (!blackDivision.equals(clientDivision)) {
					return 1;
				}

				if (blackDivision.equals(clientDivision)) {
					count++;
				}

				if (count == 4) {
					return 0;
				}

			}
			checkCount++;
		}
		return 1;
	}

	public int ipv4WhiteListCheck(List<Map<String, Object>> ipCheckList, String clientIp) {
		int checkCount = 0;

		for (Map<String, Object> ip : ipCheckList) {

			String whiteListIp = ip.get("IP").toString();
			if (whiteListIp.contains(":")) {
				continue;
			}

			StringTokenizer whiteStringTokenizer = new StringTokenizer(whiteListIp, ".");
			StringTokenizer clientStringTokenizer = new StringTokenizer(clientIp, ".");

			int count = 0;

			while (whiteStringTokenizer.hasMoreTokens()) {
				String whiteDivision = whiteStringTokenizer.nextToken();
				String clientDivision = clientStringTokenizer.nextToken();

				if (!whiteDivision.equals(clientDivision) && !whiteDivision.equals("*")
						&& checkCount == ipCheckList.size()) {
					return 0;
				}

				if (whiteDivision.equals("*")) {
					return 1;
				}

				if (!whiteDivision.equals(clientDivision)) {
					return 0;
				}

				if (whiteDivision.equals(clientDivision)) {
					count++;
				}

				if (count == 4) {
					return 1;
				}

			}

			checkCount++;
		}
		return 1;
	}

	/**
	 * @param userNo
	 * @param menuUrl
	 * @return Map<String,Object>
	 * @comment 해당 사용자의 특정 메뉴에 대한 권한을 가지고 온다.
	 */
	public Map<String, Object> getUserMenuRole(String userNo, String menuUrl) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("USR_NO", userNo);
		param.put("MENU_URL", menuUrl);

		return mapper.getUserMenuRole(param);

	}

	public void makeJoinKey(Object proNo) {

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("PRO_NO", proNo);

		String projectTableNm = getCreatProjectTableNm(proNo, 2);

		param.put("PROJECT_TABLE", projectTableNm);

		// 일련번호 존재 체크
		int checkCnt = checkTableColumn(projectTableNm, "rowNo");
		if (checkCnt > 0) {
			mapper.removeProjectRowNo(param);
		}

		// 일련번호 생성
		mapper.addProjectRowNo(param);
		mapper.addProjectRowNoIdx(param);
		mapper.addProjectRowNoData(param);

		// 결합키 테이블 생성
		String joinKeyTable = getCreatProjectJoinKeyTable(proNo);

		int checkKeyCnt = checkCreatTable(joinKeyTable);

		if (checkKeyCnt == 0) {

			param.put("JOIN_KEY_TABLE", joinKeyTable);

			mapper.createJoinKeyTable(param);
		} else {

			param.put("PROJECT_INFO_TABLE", joinKeyTable);

			mapper.deleteProjectInfoTable(param);
		}

		// 프로젝트 키 조회
		Map<String, Object> joinKeyInfo = mapper.selectJoinKeyInfo(param);

		joinKeyInfo.put("JOIN_KEY_TABLE", joinKeyTable);
		joinKeyInfo.put("PROJECT_TABLE", projectTableNm);

		if (joinKeyInfo.get("KEY_DATA") == null || "".equals(joinKeyInfo.get("KEY_DATA"))) {
			joinKeyInfo.put("KEY_DATA", joinKeyInfo.get("ORG_KEY_DATA"));
		}

		mapper.insertJoinKeyData(joinKeyInfo);

	}

	/**
	 * 메세지 초기화
	 * 
	 */
	public void setClearMessageMap() {
		messgeMap.clear();
	}

	/**
	 * 회원 가입 이메일 문구 처리
	 * 
	 */
	private MimeMessage createJoinMessage(String to, String passwd) throws Exception {
		MimeMessage message = emailSender.createMimeMessage();

		message.addRecipients(RecipientType.TO, to);// 보내는 대상
		message.setSubject("PAMs 회원가입 확인메일");// 제목

		String msgg = "";
		msgg += "<div style='margin:100px;'>";
		msgg += "<h1> PAMs 회원가입이 완료되었습니다. </h1>";
		msgg += "<br>";
		msgg += "<p>계정의 일회용 비밀번호는 아래 안내되니,<p>";
		msgg += "<br>";
		msgg += "<p>로그인 하신 뒤 비밀번호 변경을 하시기 바랍니다.<p>";
		msgg += "<br>";
		msgg += "<div align='center' style='border:1px solid black; font-family:verdana';>";
		msgg += "<h3 style='color:blue;'>계정 비밀번호</h3>";
		msgg += "<div style='font-size:130%'>";
		msgg += "CODE : <strong>";
		msgg += passwd + "</strong><div><br/> ";
		msgg += "</div>";

		message.setText(msgg, "utf-8", "html");// 내용
		message.setFrom(new InternetAddress("hjdo@lagomsoft.co.kr", "PAMs"));// 보내는 사람

		return message;
	}

	/**
	 * Key 임시 생성
	 * 
	 * [개인정보의 기술적·관리적 보호조치 기준 - 4조] 20210708 영문 + 숫자의 경우 최소 10자리 영문 + 숫자 + 특문 의 경우
	 * 최소 8자 이상
	 * 
	 * 길이 8자 , 숫자 영문 특수문자로 처리
	 * 
	 * @return
	 */
	public String createKey() {
		return createKey(8, 3, 0);
	}

	/**
	 * Key 임시 생성
	 * 
	 * [개인정보의 기술적·관리적 보호조치 기준 - 4조] 20210708 영문 + 숫자의 경우 최소 10자리 영문 + 숫자 + 특문 의 경우
	 * 최소 8자 이상
	 * 
	 * @param keySize 키 길이
	 * @param type    처리 타입 / 3 : 숫자 영문 특수문자 / 2 : 숫자 영문 / 1 : 숫자
	 * 
	 * @return
	 */
	public String createKey(int keySize, int type) {
		return createKey(keySize, type, 0);
	}

	/**
	 * Key 임시 생성
	 * 
	 * [개인정보의 기술적·관리적 보호조치 기준 - 4조] 20210708 영문 + 숫자의 경우 최소 10자리 영문 + 숫자 + 특문 의 경우
	 * 최소 8자 이상
	 * 
	 * @param keySize 키 길이
	 * @param type    처리 타입 / 3 : 숫자 영문 특수문자 / 2 : 숫자 영문 / 1 : 숫자
	 * @param cnt     루프 누적수 10번이상 처리 되면 탈출
	 * @return
	 */
	private String createKey(int keySize, int type, int cnt) {
		SecureRandom rnd = new SecureRandom();
		StringBuffer key = new StringBuffer();

		cnt = cnt + 1;

		int cnt1 = 0;
		int cnt2 = 0;
		int cnt3 = 0;

		if (type > 3) {
			type = 3;
		}

		if (type == 1) {
			cnt1 = 1;
		} else if (type == 2) {
			cnt1 = 1;
			cnt2 = 1;
		}

		for (int i = 0; i < keySize; i++) { // 인증코드 8자리
			int index = rnd.nextInt(type) + (3 - type); // 0~2 / 1~ 2 / 2 랜덤
			switch (index) {
			case 0:
				cnt1++;
				key.append(specialChaMapping((int) (rnd.nextInt(4))));
				// !@#&
				break;
			case 1:
				cnt2++;
				key.append((char) ((int) (rnd.nextInt(26)) + 65));
				// A~Z
				break;
			case 2:
				cnt3++;
				key.append((rnd.nextInt(10)));
				// 0~9
				break;
			}
		}

		if (cnt > 10 || (cnt1 > 0 && cnt2 > 0 && cnt3 > 0)) {
			return key.toString();
		} else {
			return createKey(keySize, type, cnt + 1);
		}

	}

	private char specialChaMapping(int i) {
		char returncha = 0;
		if (i == 0) {
			returncha = '!';
		}
		if (i == 1) {
			returncha = '#';
		}
		if (i == 2) {
			returncha = '@';
		}
		if (i == 3) {
			returncha = '&';
		}

		return returncha;
	}

	/**
	 * 회원가입 이메일 발송
	 * 
	 */
	public void sendJoinMessage(String to, String passwd) throws Exception {
		// TODO Auto-generated method stub
		if ("Y".equals(mailUse)) {
			MimeMessage message = createJoinMessage(to, passwd);
			try {// 예외처리
				emailSender.send(message);
			} catch (MailException es) {
				log.error(es.toString());
				throw new IllegalArgumentException();
			}
		}
	}

	/**
	 * 비밀번호 찾기 이메일 발송
	 * 
	 */
	public void sendSearchPwMessage(String to, String passwd) throws Exception {
		// TODO Auto-generated method stub
		if ("Y".equals(mailUse)) {
			MimeMessage message = createSearchPwMessage(to, passwd);
			try {// 예외처리
				emailSender.send(message);
			} catch (MailException es) {
				log.error(es.toString());
				throw new IllegalArgumentException();
			}
		}
	}

	/**
	 * 비밀번호 찾기 이메일 문구 처리
	 * 
	 */
	private MimeMessage createSearchPwMessage(String to, String passwd) throws Exception {
		MimeMessage message = emailSender.createMimeMessage();

		message.addRecipients(RecipientType.TO, to);// 보내는 대상
		message.setSubject("PAMs 임시 비밀번호가 도착했습니다.");// 제목

		String msgg = "";
		msgg += "<div style='margin:100px;'>";
		msgg += "<h1> PAMs 임시 비밀번호를 보내드립니다. </h1>";
		msgg += "<br>";
		msgg += "<p>계정의 임시 비밀번호는 아래 안내되니,<p>";
		msgg += "<br>";
		msgg += "<p>로그인 하신 뒤 비밀번호 변경을 하시기 바랍니다.<p>";
		msgg += "<br>";
		msgg += "<div align='center' style='border:1px solid black; font-family:verdana';>";
		msgg += "<h3 style='color:blue;'>계정 비밀번호</h3>";
		msgg += "<div style='font-size:130%'>";
		msgg += "CODE : <strong>";
		msgg += passwd + "</strong><div><br/> ";
		msgg += "</div>";

		message.setText(msgg, "utf-8", "html");// 내용
		message.setFrom(new InternetAddress("hjdo@lagomsoft.co.kr", "PAMs"));// 보내는 사람

		return message;
	}

	public boolean checkMailUse() {
		if ("Y".equals(mailUse)) {
			return true;
		}
		return false;
	}

	public boolean checkCorpUse() {
		if ("Y".equals(corpUse)) {
			return true;
		}
		return false;
	}

	public String createTempPassword(String usr_id) {
		// SecureRandom rnd = new SecureRandom ();
		StringBuffer key = new StringBuffer();

		key.append(usr_id);
		key.append("1");

		return key.toString();
	}

	/**
	 * 메뉴 목록 표출
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getUserMenuList(Map<String, Object> param) {
		return mapper.getUserMenuList(param);
	}

	/**
	 * 파일 완전삭제 로직 처리
	 * 
	 * @param filePath
	 */
	public void destructionFile(Object proNo, Object fileId, Object sn) {

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("FL_ID", fileId);
		param.put("FL_SN", sn);

		Map<String, String> fileInfo = mapper.getFileInfo(param);

		String flPth = "";
		if (fileInfo != null) {
			flPth = fileInfo.get("FL_PTH");
		}

		threadDestructionFile(proNo, fileId, sn, flPth);

	}

	/**
	 * 파일 전체 완전삭제 로직 처리
	 * 
	 * @param proNo
	 * @param fileId
	 */
	public void destructionFiles(Object proNo, Object fileId) {

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("FL_ID", fileId);

		List<Map<String, Object>> fileInfos = mapper.getFileInfos(fileId);

		if (fileInfos != null) {

			for (Map<String, Object> fileInfo : fileInfos) {

				threadDestructionFile(proNo, fileId, fileInfo.get("FL_SN"), (String) fileInfo.get("FL_PTH"));

			}
		}

	}

	/**
	 * 파일 파기 로직
	 * 
	 * @param proNo
	 * @param fileId
	 * @param sn
	 * @param flPth
	 */
	public void threadDestructionFile(Object proNo, Object fileId, Object sn, String flPth) {

		Map<String, Object> codeParam = new HashMap();
		codeParam.put("GRP_CD", "DESTRUCTION_CODE");
		codeParam.put("META_1", "Y");

		List<Map<String, Object>> codeList = getCodeList(codeParam);

		if (codeList.size() != 0) {
			processType = (String) codeList.get(0).get("CD");
		}

		// 멀티 스래드 처리
		startDestructionFileSchedule(proNo, fileId, sn, flPth, processType);

		Map<String, Object> param = new HashMap<String, Object>();

		param.put("USR_NO", getLoginUserNo());
		param.put("FL_ID", fileId);
		param.put("FL_SN", sn);

		mapper.deleteFile(param);
	}

	/**
	 * 배치시간 리턴
	 * 
	 * @return
	 */
	public String getDefTime() {
		return defTime;
	}

	public List<Map<String, Object>> getGroupList() {
		return mapper.getGroupList();
	}

	/**
	 * 스케줄 등록할 신청기관 정보
	 * 
	 * @param param APL_NO
	 * @param isNow
	 */
	public Map<String, Object> getScheduleAplInfo(Map<String, Object> param) {

		return mapper.selectScheduleAplInfo(param);
	}

	/**
	 * 스케줄 등록 및 처리
	 * 
	 * @param param PRJCT_NO FL_NO STEP_CD
	 * @param isNow
	 */
	public void requestScheduleProcessing(Map<String, Object> param, boolean isNow) {
		Map<String, Object> newParam = new HashMap<>();
		// 테이블 정보 조회
		newParam = getScheduleAplInfo(param);

		Object userNo = getLoginUserNo();
		Object userId = getLoginUserID();

		param.put("USR_NO", userNo);
		param.put("USR_ID", userId);

		if (isNow) {
			param.put("RSVTIME", "NEXT");
		} else {
			param.put("RSVTIME", defTime);
		}

		// 기존 순번으로 스케줄 조회
		Map<String, Object> rvwSnMap = null;

		rvwSnMap = mapper.selectBeforeScheduleProcessing(param);

		if (rvwSnMap != null && rvwSnMap.get("SCHEDULE_RVW_SN") != null) {

			long rvwSn = -1;

			if (rvwSnMap.get("SCHEDULE_RVW_SN") instanceof Integer) {
				rvwSn = (int) rvwSnMap.get("SCHEDULE_RVW_SN");
			} else if (rvwSnMap.get("SCHEDULE_RVW_SN") instanceof Long) {
				rvwSn = (long) rvwSnMap.get("SCHEDULE_RVW_SN");
			} else {
				rvwSn = Integer.parseInt((String) rvwSnMap.get("SCHEDULE_RVW_SN"));
			}

			param.put("NEW_RVW_SN", rvwSn + 1);
			param.put("ORG_RVW_SN", rvwSn);

		} else {
			param.put("NEW_RVW_SN", 1);
		}

		
		
		if("0".equals(param.get("FL_NO"))) {
		  //파기 스케줄 등록
		    mapper.insertDestructScheduleManagement(param);
		} else {
		    mapper.insertScheduleManagement(param);
		    mapper.updateProjectStep(param);
		}
		
	}

	/**
	 * 스케줄 등록 및 처리 (처리 시간 수동 입력)
	 * 
	 * @param param
	 */
	public void requestScheduleProcessing(Map<String, Object> param) {
		// 기존 순번으로 스케줄 조회
		Map<String, Object> rvwSnMap = null;

		rvwSnMap = mapper.selectBeforeScheduleProcessing(param);

		if (rvwSnMap != null && rvwSnMap.get("SCHEDULE_RVW_SN") != null) {

			long rvwSn = -1;

			if (rvwSnMap.get("SCHEDULE_RVW_SN") instanceof Integer) {
				rvwSn = (int) rvwSnMap.get("SCHEDULE_RVW_SN");
			} else if (rvwSnMap.get("SCHEDULE_RVW_SN") instanceof Long) {
				rvwSn = (long) rvwSnMap.get("SCHEDULE_RVW_SN");
			} else {
				rvwSn = Integer.parseInt((String) rvwSnMap.get("SCHEDULE_RVW_SN"));
			}

			param.put("NEW_RVW_SN", rvwSn + 1);
			param.put("ORG_RVW_SN", rvwSn);

		}

		mapper.updateProjectStep(param);

		mapper.insertScheduleManagement(param);
	}

	/**
	 * 실재 반출 파일생성
	 * 
	 * @param proNo
	 * @param isTmpFile 임시 파일 여부
	 * @return
	 */
	public String makeCSVZipFile(Object proNo) throws PAMsException {
		return makeCSVZipFile(proNo, false);
	}

	/**
	 * 반출 파일생성
	 * 
	 * @param proNo
	 * @param isTmpFile 임시 파일 여부
	 * @return
	 */
	public String makeCSVZipFile(Object proNo, boolean isTmpFile) throws PAMsException {

		String errMsg = "";

		Map<String, Object> csvData = mapper.getProjectCSVFileData(proNo);

		csvData.put("PRO_NO", proNo);
		csvData.put("TABLE", 2);

		// csv 파일아이디 저장
		mapper.updateProjectAttachID(csvData);

		String tableNm = getCreatProjectTableNm(proNo, 2);

		/*
		 * CSV 파일 정보
		 */
		String csvFileID = (String) csvData.get("ATTACH_ID");
		String aliasNo = (String) csvData.get("ALIAS_NO");

		// csv 저장경로
		String csvPath = tempFilepath;

		// 저장 파일 경로
		String path = getFilepath();

		path += "/" + proNo + "/600";

		File fileDir = new File(path);

		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		// 파일 생성정보
		Map<String, Object> fileInfo = new HashMap<>();

		// 파일 저장 정보
		Map<String, Object> fileData = new HashMap<String, Object>();

		// 컬럼 정보
		List<Map<String, Object>> columnList = new ArrayList<>();

		try {

			/*
			 * 결합키 관련 처리
			 */
			if (!isTmpFile && "90".equals(csvData.get("ALIAS_TY_CD"))) {

				String keyTableNm = getCreatProjectJoinKeyTable(proNo);

				List<Map<String, Object>> keyColumnList = new ArrayList<>();

				Map<String, Object> rowNoMap = new HashMap<String, Object>();
				rowNoMap.put("CLMN", "rowNo");
				rowNoMap.put("CLMN_NM", "ROW_NO");
				keyColumnList.add(rowNoMap);

				Map<String, Object> keyValMap = new HashMap<String, Object>();
				keyValMap.put("CLMN", "KEY_VAL");
				keyValMap.put("CLMN_NM", "KEY");
				keyColumnList.add(keyValMap);

				/*
				 * CSV 파일 생성
				 */
				String keyCsvFileName = aliasNo + "-KEY.csv";

				File keyCsvFile = new File(csvPath + "/" + keyCsvFileName);

				// 기존에 존재하면 제거
				if (keyCsvFile.exists()) {

					mapper.updateGarantCsvFile(csvPath + "/" + keyCsvFileName);

					keyCsvFile.delete();
				}

				fileInfo.put("QOTAT", csvData.get("QOTAT"));
				fileInfo.put("SPRTR", csvData.get("SPRTR"));
				fileInfo.put("ENCD", csvData.get("JOIN_ENCD"));

				// 생성
				saveDataCsv(csvPath + "/" + keyCsvFileName, keyTableNm, keyColumnList, fileInfo);

				mapper.updateGarantCsvFile(csvPath + "/" + keyCsvFileName);

				// 파일 사이즈 bytes > MB
				keyCsvFile = new File(csvPath + "/" + keyCsvFileName);

				/*
				 * 암호 생성
				 */
				String keyZipKey = createKey(10, 2);

				Map<String, Object> keyData = new HashMap<String, Object>();

				keyData.put("KEYID", proNo + "_" + csvFileID + "_" + 3);
				keyData.put("KEYV", keyZipKey);

				mapper.insertProjectZipKey(keyData);

				/*
				 * 파일 압축하기
				 */
				String keyZipName = aliasNo + "-KEY.zip";

				// 압축 파일 명
				String saveKeyZipFileName = String.format("%s_%d", csvFileID, 2);

				File keyZipFile = new File(path + "/" + saveKeyZipFileName);

				// 기존에 존재하면 제거
				if (keyZipFile.exists()) {
					keyZipFile.delete();
				}

				zip(csvPath + "/" + keyCsvFileName, path + "/" + saveKeyZipFileName, keyZipKey);

				// 파일 사이즈 bytes > MB
				keyZipFile = new File(path + "/" + saveKeyZipFileName);
				Long keyZipFileSize = keyZipFile.length() / 1024 / 1024;

				if (keyZipFileSize == 0) {
					keyZipFileSize = 1L;
				}

				// KEY ZIP 파일생성
				fileData.put("FL_ID", csvFileID);
				fileData.put("FL_SN", 3);
				fileData.put("FL_NM", keyZipName);
				fileData.put("FL_SIZE", keyZipFileSize);
				fileData.put("FL_EXTSN", "zip");
				fileData.put("FL_PTH", path + "/" + saveKeyZipFileName);

				fileData.put("USR_NO", "0");

				mapper.addFile(fileData);

				// key csv 파일 파기
				if (keyCsvFile.exists()) {
					keyCsvFile.delete();
				}

				columnList.add(rowNoMap);

			}

			String csvFileName = aliasNo + ".csv";

			List<Map<String, Object>> tmpColumnList = mapper.selectProjectColumns(csvData);

			for (Map<String, Object> columnData : tmpColumnList) {

				columnData.put("CLMN", "col" + columnData.get("CLMN_NO"));
				columnData.put("CLMN_NM", columnData.get("CLM_NM"));

				columnList.add(columnData);
			}

			File csvFile = new File(csvPath + "/" + csvFileName);

			// 기존에 존재하면 파기
			if (csvFile.exists()) {

				mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

				DestructionFile df = new DestructionFile();
				df.run(csvFile, processType);

				if (csvFile.exists()) {
					csvFile.delete();
				}
			}

			fileInfo.put("QOTAT", csvData.get("QOTAT"));
			fileInfo.put("SPRTR", csvData.get("SPRTR"));
			fileInfo.put("ENCD", csvData.get("ENCD"));

			// 생성
			saveDataCsv(csvPath + "/" + csvFileName, tableNm, columnList, fileInfo);

			mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

			// 파일 사이즈 bytes > MB
			csvFile = new File(csvPath + "/" + csvFileName);
			Long csvFileSize = csvFile.length() / 1024 / 1024;

			if (csvFileSize == 0) {
				csvFileSize = 1L;
			}

			// CSV 파일생성
			fileData.put("FL_ID", csvFileID);
			fileData.put("FL_SN", 1);
			fileData.put("FL_NM", csvFileName);
			fileData.put("FL_SIZE", csvFileSize);
			fileData.put("FL_EXTSN", "csv");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", csvPath + "/" + csvFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

			/*
			 * 암호 생성
			 */
			String key = createKey(10, 2);

			Map<String, Object> keyData = new HashMap<String, Object>();

			keyData.put("KEYID", proNo + "_" + csvFileID + "_" + 2);
			keyData.put("KEYV", key);

			mapper.insertProjectZipKey(keyData);

			/*
			 * 파일 압축하기
			 */
			String zipName = aliasNo + ".zip";

			// 압축 파일 명
			String saveZipFileName = String.format("%s_%d", csvFileID, 1);

			File zipFile = new File(path + "/" + saveZipFileName);

			// 기존에 존재하면 파기
			if (zipFile.exists()) {
				DestructionFile df = new DestructionFile();
				df.run(zipFile, processType);

				if (zipFile.exists()) {
					zipFile.delete();
				}
			}

			zip(csvPath + "/" + csvFileName, path + "/" + saveZipFileName, key);

			// 파일 사이즈 bytes > MB
			zipFile = new File(path + "/" + saveZipFileName);
			Long zipFileSize = zipFile.length() / 1024 / 1024;

			if (zipFileSize == 0) {
				zipFileSize = 1L;
			}

			// ZIP 파일생성
			fileData.put("FL_SN", 2);
			fileData.put("FL_NM", zipName);
			fileData.put("FL_SIZE", zipFileSize);
			fileData.put("FL_EXTSN", "zip");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", path + "/" + saveZipFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

		} catch (Exception e) {

			errMsg = " " + e.getCause();
			log.error("makeCSVZipFile ERR : " + " " + e.getCause());

		} finally {

			deleteFile(proNo, csvFileID, 1, true);

		}

		// 오류 발생시 표출
		if (!"".equals(errMsg)) {
			throw new PAMsException(errMsg);
		}

		return csvFileID;

	}

	/**
	 * CSV 파일 생성
	 * 
	 * @param fileNm
	 * @param tblNm
	 * @param columnList CLMN CLMN_NM
	 * @param fileInfo
	 * @throws IOException
	 */
	private void saveDataCsv(String fileNm, String tblNm, List<Map<String, Object>> columnList,
			Map<String, Object> fileInfo) throws IOException {

		// CSV 파일 생성
		if (fileInfo == null) {
			fileInfo = new HashMap<String, Object>();
		}

		fileInfo.put("FILE_NM", fileNm);
		fileInfo.put("TBL_NM", tblNm);
		fileInfo.put("COLUMN_LIST", columnList);

		mapper.saveDataCsv(fileInfo);

	}

	/**
	 * 파일 압축 처리
	 * 
	 * @param from
	 * @param to
	 * @param password
	 * @throws ZipException
	 */
	public void zip(String from, String to, String password) throws ZipException {

		ZipParameters param = new ZipParameters();
		param.setCompressionMethod(CompressionMethod.DEFLATE);
		param.setCompressionLevel(CompressionLevel.MAXIMUM);

		if (password != null) {
			param.setEncryptFiles(true);
			param.setEncryptionMethod(EncryptionMethod.AES);
			param.setAesKeyStrength(AesKeyStrength.KEY_STRENGTH_256);
		}

		ZipFile zipFile = new ZipFile(to);

		if (password != null) {
			zipFile.setPassword(password.toCharArray());
		}

		zipFile.addFile(new File(from), param);
	}

	/**
	 * 로컬인지 확인
	 * 
	 * @return
	 */
	public static void setIsLocal(String y) {
		CommonService.isLocal = y;
	}

	public boolean getCheckLocal() {

		if ("local".equals(profilesActive)) {
			setIsLocal("Y");
		}

		if (isLocal == null) {
			// 로컬위치 체크로직
			if (RequestContextHolder.getRequestAttributes() != null) {
				ServletRequestAttributes servletRequestAttribute = (ServletRequestAttributes) RequestContextHolder
						.currentRequestAttributes();

				if (servletRequestAttribute != null) {
					String url = servletRequestAttribute.getRequest().getRequestURL().toString();

					if (url != null) {

						if (!url.contains("127.0.0.1") && !url.toLowerCase().contains("localhost")) {
							isLocal = "N";
						} else {
							isLocal = "Y";
						}
					}
				}
			}

		}

		if ("Y".equals(isLocal)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Map의 개인정보 검출 처리
	 * 
	 * @param param
	 * @return
	 */
	public String privacyDetection(Map<String, Object> param) {

		if (param == null) {
			return "";
		}

		Map<String, Object> dataMap = new HashMap<String, Object>();
		dataMap.putAll(param);

		if (maskingDataMap.isEmpty()) {

			Map<String, Object> codeParam = new HashMap<String, Object>();
			codeParam.put("GRP_CD", "MASKING_PATTERN");
			List<Map<String, Object>> maskingCodeList = getCodeList(codeParam);

			for (Map<String, Object> maskingMap : maskingCodeList) {
				maskingDataMap.put((String) maskingMap.get("CD"), maskingMap);
			}
		}

		HttpSession session = getLoginSession();
		param.put("PRJCT_NO", session.getAttribute("SELECT_PRO_NO"));
		param.put("USR_NO", getLoginUserNo());

		if (detectionCodeList == null) {
			detectionCodeList = getCodeList("PRIVACY_DETECTION_CD");
		}

		for (int i = 0; i < detectionCodeList.size(); i++) {

			Map<String, Object> checkCodeMap = detectionCodeList.get(i);

			for (int j = 1; j <= 10; j++) {

				String regex = (String) checkCodeMap.get("META_" + j);

				if ("".equals(regex) || regex == null) {
					continue;
				}
				param.put("REGEX", regex);

				Pattern pattern = Pattern.compile(regex);

				for (String key : param.keySet()) {

					if (key != null && !"".equals(key) && !key.toUpperCase().contains("CONTENTS")
							&& !key.toUpperCase().contains("FILEID") && !key.toUpperCase().contains("DELETEFILES")
							&& !key.toUpperCase().contains("_ID") && !key.toUpperCase().contains("_NO")
							&& param.get(key) instanceof String) {

						Object val = param.get(key);

						String contents = (String) val;

						Matcher matcher = pattern.matcher(param.get(key).toString());

						if (matcher.find()) {
							param.put("DETECTION_TYPE", checkCodeMap.get("CD_NM"));

							if (maskingDataMap.get(checkCodeMap.get("CD")) != null) {

								Map<String, Object> maskingCodeMap = maskingDataMap.get(checkCodeMap.get("CD"));

								contents = matcher.replaceAll((String) maskingCodeMap.get("META_" + j));

							} else {
								contents = matcher.replaceAll("PRIVACY DATA DETECTION.");
							}

							param.put("CONTENT_VAL", contents);

							mapper.setPricacyDetection(param);
						}

						dataMap.put(key, contents);

					} else {
						dataMap.put(key, param.get(key));
					}
				}

			}
		}

		return (String) dataMap.get("CONTENTS");
	}

	/**
	 * 문자열 개인정보 검출 처리
	 * 
	 * @param param
	 * @return
	 */
	public String privacyDetectionString(String key, String val) {

		if (val == null) {
			return val;
		}

		if (key == null || "".equals(key) || key.toUpperCase().contains("FILEID")
				|| key.toUpperCase().contains("DELETEFILES") || key.toUpperCase().contains("_ID")
				|| key.toUpperCase().contains("_NO")) {
			return val;
		}

		String returnVal = val + "";

		if (maskingDataMap.isEmpty()) {

			Map<String, Object> codeParam = new HashMap<String, Object>();
			codeParam.put("GRP_CD", "MASKING_PATTERN");
			List<Map<String, Object>> maskingCodeList = getCodeList(codeParam);

			for (Map<String, Object> maskingMap : maskingCodeList) {
				maskingDataMap.put((String) maskingMap.get("CD"), maskingMap);
			}
		}

		if (detectionCodeList == null) {
			detectionCodeList = getCodeList("PRIVACY_DETECTION_CD");
		}

		for (int i = 0; i < detectionCodeList.size(); i++) {

			Map<String, Object> checkCodeMap = detectionCodeList.get(i);

			for (int j = 1; j <= 10; j++) {

				String regex = (String) checkCodeMap.get("META_" + j);

				if ("".equals(regex) || regex == null) {
					continue;
				}

				Pattern pattern = Pattern.compile(regex);

				Matcher matcher = pattern.matcher(returnVal);

				if (matcher.find()) {

					if (maskingDataMap.get(checkCodeMap.get("CD")) != null) {

						Map<String, Object> maskingCodeMap = maskingDataMap.get(checkCodeMap.get("CD"));

						returnVal = matcher.replaceAll((String) maskingCodeMap.get("META_" + j));

					} else {
						returnVal = matcher.replaceAll("PRIVACY DATA DETECTION.");
					}

				}

			}
		}

		return returnVal;
	}

    /**
     * 반출 파일 가져오기
     */
    public Map<String, Object> selectAttachFileInfo(Map<String, Object> param) {

        Map<String, Object> resultMap = new HashMap<String, Object>();

        if ("K".equals(param.get("FL_TY_CD"))) {

            String proTableNm = getCreatBindProjectTableNm(param.get("FL_NO"));

            param.put("TABLE_NM", proTableNm);

            resultMap.put("ATTACH_INFO", mapper.selectKeyCSVFileInfo(param));

        } else if ("D".equals(param.get("FL_TY_CD"))) {

            String proTableNm = getCreatBindProjectTableNm(param.get("FL_NO"));

            param.put("TABLE_NM", proTableNm);

            resultMap.put("ATTACH_INFO", mapper.selectDataCSVFileInfo(param));

        } else if ("J".equals(param.get("FL_TY_CD"))) {

            String joinTableNm = getCreatJoinProjectTableNm(param.get("PRJCT_NO"));

            param.put("TABLE_NM", joinTableNm);

            resultMap.put("ATTACH_INFO", mapper.selectJoinCSVFileInfo(param));
        } else {

            String proTableNm = getCreatProjectTableNm(param.get("PRJCT_NO"), 2);

            param.put("TABLE_NM", proTableNm);

            resultMap.put("ATTACH_INFO", mapper.selectCSVFileInfo(param));
        }

        return resultMap;
    }

	/**
	 * 에러 정보 조회
	 * 
	 * @param proNo
	 * @param rvwSn
	 * @param stepCd
	 * @return
	 */
	public List<Map<String, Object>> getErrLog(Object proNo, Object rvwSn, Object stepCd) {

		Map<String, Object> proData = new HashMap<String, Object>();

		proData.put("PRJCT_NO", proNo);
		proData.put("RVW_SN", rvwSn);
		proData.put("STEP_CD", stepCd);

		return mapper.selectErrLog(proData);
	}

	/**
	 * 파일 세팅 정보 리스트 조회
	 * 
	 * @param param
	 * @return
	 */
	public Map<String, Object> getFileSettingList() {

		boolean isNewLoading = true;

		long nowTime = new Date().getTime();

		if (fileSettingMap != null && fileSettingMap.get("SETTING_TIME") != null) {

			long settingTime = (long) fileSettingMap.get("SETTING_TIME");

			if (settingTime > nowTime) {
				isNewLoading = false;
			}
		}

		if (isNewLoading) {

			fileSettingMap.clear();

			HttpSession session = getLoginSession();

			List<Map<String, Object>> fileSettingList = getCodeList("FILE_SETTING");

			// 5분 동안 조회 안되도록 설정
			fileSettingMap.put("SETTING_TIME", nowTime + (1000 * 60 * 5));
			session.setAttribute("FILE_SETTING_TIME", nowTime + (1000 * 60 * 5));

			if (fileSettingList != null) {

				for (Map<String, Object> fileSetting : fileSettingList) {

					fileSettingMap.put((String) fileSetting.get("CD"), fileSetting.get("META_1"));
					session.setAttribute((String) fileSetting.get("CD"), fileSetting.get("META_1"));

				}

			}

		}

		return fileSettingMap;
	}

	/**
	 * 파일 세팅 정보 조회
	 * 
	 * @param param
	 * @return
	 */
	public Object getFileSettingInfo(String settingVal) {

		return getFileSettingList().get(settingVal);
	}

	/**
	 * 로그인 완전실패(잠금) 이력 기록
	 * 
	 * @param param
	 */
	public void setFailLoginHistRecording(Map<String, Object> param) {

		mapper.insertFailLoginHistRecording(param);
	}

	/**
	 * 혹시 임시 폴더에 남아 있을 파일 제거 처리
	 */
	public void clearTempFilepath() {
		try {

			File tempFileDir = new File(tempFilepath);

			File[] tempFiles = tempFileDir.listFiles();

			if (tempFiles != null) {
				for (File tempFile : tempFiles) {
					tempFile.delete();
				}
			}

		} catch (Exception e) {
			log.error("clearTempFilepath() : " + " " + e.getCause());
		}
	}

	public Map<String, Object> postCarryoutInfo(Map<String, Object> param) {

		Map<String, Object> resultMap = new HashMap<String, Object>();

		param.put("USR_NO", getLoginUserNo());
		
		carryoutMapper.insertCarryoutInfo(param);

		HttpSession session = getLoginSession();
		session.setAttribute("INDVDLINFO_CHECK_Y", "Y");

		resultMap.put("result", "success");

		return resultMap;
	}

	public String getUserNm(Object usrNo) {
		Map<String, Object> param = new HashMap<>();
		param.put("USR_NO", usrNo);
		if (usrNo == null) {
			return null;
		}
		return userMapper.getUserName(param);
	}

	public String makeJoinCSVZipFile(Object joinNo) throws PAMsException {

		String errMsg = "";

		Map<String, Object> csvData = mapper.getJoinCSVFileData(joinNo);
		String joinTableNm = getCreatJoinProjectTableNm(joinNo);

		csvData.put("JOIN_NO", joinNo);
		csvData.put("TABLE_NM", joinTableNm);

		// csv 파일아이디 저장
		mapper.updateJoinAttachID(csvData);

		/*
		 * CSV 파일 정보
		 */
		String csvFileID = (String) csvData.get("ATTACH_ID");
		String aliasNo = (String) csvData.get("ALIAS_NO");

		// csv 저장경로
		String csvPath = tempFilepath;

		// 저장 파일 경로
		String path = getFilepath();

		path += "/" + joinNo + "/600";

		File fileDir = new File(path);

		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		// 파일 생성정보
		Map<String, Object> fileInfo = new HashMap<>();

		// 파일 저장 정보
		Map<String, Object> fileData = new HashMap<String, Object>();

		try {

			String csvFileName = aliasNo + ".csv";

			// 컬럼 정보
			List<Map<String, Object>> columnList = mapper.selectJoinColumns(csvData);

			File csvFile = new File(csvPath + "/" + csvFileName);

			// 기존에 존재하면 파기
			if (csvFile.exists()) {

				mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

				DestructionFile df = new DestructionFile();
				df.run(csvFile, processType);

				if (csvFile.exists()) {
					csvFile.delete();
				}
			}

			fileInfo.put("QOTAT", csvData.get("QOTAT"));
			fileInfo.put("SPRTR", csvData.get("SPRTR"));
			fileInfo.put("ENCD", csvData.get("ENCD"));

			// 생성
			saveDataCsv(csvPath + "/" + csvFileName, joinTableNm, columnList, fileInfo);

			mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

			// 파일 사이즈 bytes > MB
			csvFile = new File(csvPath + "/" + csvFileName);
			Long csvFileSize = csvFile.length() / 1024 / 1024;

			if (csvFileSize == 0) {
				csvFileSize = 1L;
			}

			// CSV 파일생성
			fileData.put("FL_ID", csvFileID);
			fileData.put("FL_SN", 1);
			fileData.put("FL_NM", csvFileName);
			fileData.put("FL_SIZE", csvFileSize);
			fileData.put("FL_EXTSN", "csv");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", csvPath + "/" + csvFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

			/*
			 * 암호 생성
			 */
			String key = createKey(10, 2);

			Map<String, Object> keyData = new HashMap<String, Object>();

			keyData.put("KEYID", joinNo + "_" + csvFileID + "_JOIN");
			keyData.put("KEYV", key);

			mapper.insertProjectZipKey(keyData);

			/*
			 * 파일 압축하기
			 */
			String zipName = aliasNo + ".zip";

			// 압축 파일 명
			String saveZipFileName = String.format("%s_%d", csvFileID, 1);

			File zipFile = new File(path + "/" + saveZipFileName);

			// 기존에 존재하면 파기
			if (zipFile.exists()) {
				DestructionFile df = new DestructionFile();
				df.run(zipFile, processType);

				if (zipFile.exists()) {
					zipFile.delete();
				}
			}

			zip(csvPath + "/" + csvFileName, path + "/" + saveZipFileName, key);

			// 파일 사이즈 bytes > MB
			zipFile = new File(path + "/" + saveZipFileName);
			Long zipFileSize = zipFile.length() / 1024 / 1024;

			if (zipFileSize == 0) {
				zipFileSize = 1L;
			}

			// ZIP 파일생성
			fileData.put("FL_SN", 2);
			fileData.put("FL_NM", zipName);
			fileData.put("FL_SIZE", zipFileSize);
			fileData.put("FL_EXTSN", "zip");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", path + "/" + saveZipFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

		} catch (Exception e) {

			errMsg = " " + e.getCause();
			log.error("makeJoinCSVZipFile ERR : " + " " + e.getCause());

		} finally {

			deleteFile(joinNo, csvFileID, 1, true);

		}

		// 오류 발생시 표출
		if (!"".equals(errMsg)) {
			throw new PAMsException(errMsg);
		}

		return csvFileID;
	}

	/**
	 * 모의 결합할 키정보 리스트 조회
	 * 
	 * @param param FL_NO
	 * @return
	 */
	public List<Map<String, Object>> getPrebindKeyInfoList(Map<String, Object> param) {

		return mapper.getPrebindKeyInfoList(param);
	}

	/**
	 * 모의 결합할 데이터 정보 리스트 조회
	 * 
	 * @param param FL_NO
	 * @return
	 */
	public List<Map<String, Object>> getPrebindDataInfoList(Map<String, Object> param) {

		return mapper.getPrebindDataInfoList(param);
	}

	/**
	 * 모의결합 키 복제
	 * 
	 * @param paramList BIND_TBL_NM TBL_NM
	 * @return
	 */
	public void postCopyPrebindKey(List<Map<String, Object>> paramList) {
		for (Map<String, Object> param : paramList) {
			dropProjectTable((String) param.get("CREATE_TBL_NM"));

			mapper.createCopyPrebindKey(param);
		}

	}

	/**
	 * 모의결합 키 노이즈 및 샘플 추가
	 * 
	 * @param paramList CREATE_TBL_NM
	 */
	public void postAddNoiseSample(List<Map<String, Object>> paramList) {
		Map<String, Object> resultNoiseMap = getCodeInfo("NOISE_SAMPLE_CD", "NOISE_TY");
		Map<String, Object> resultSampleMap = getCodeInfo("NOISE_SAMPLE_CD", "SAMPLE_TY");

		
		for (Map<String, Object> param : paramList) {
            param.put("NOISE_TY", (String) resultNoiseMap.get("CD_NM"));
            param.put("SAMPLE_TY", (String) resultSampleMap.get("CD_NM"));
            
		    // SENSITIVE INFO YES
		    if(!"Y".equals(param.get("STV_YN"))) {
	            param.put("NOISE_VAL", (String) resultNoiseMap.get("META_1"));
	            param.put("NOISE_PERCENT", (String) resultNoiseMap.get("META_2"));

	            param.put("SAMPLE_VAL", (String) resultSampleMap.get("META_1"));
	            param.put("SAMPLE_PERCENT", (String) resultSampleMap.get("META_2"));

	            // COUNT OR PERCENT
	            if ("C".equals(param.get("SAMPLE_TY"))) {
	                param.put("LIMIT_SAMPLE_VAL", param.get("SAMPLE_VAL"));
	            } else if("P".equals(param.get("SAMPLE_TY"))) {
	                param.put("LIMIT_SAMPLE_VAL", mapper.selectSamplePercentCount(param));
	            }

	            if ("C".equals(param.get("NOISE_TY"))) {
	                param.put("LIMIT_NOISE_VAL", param.get("NOISE_VAL"));
	            } else if("P".equals(param.get("NOISE_TY"))) {
	                param.put("LIMIT_NOISE_VAL", mapper.selectNoisePercentCount(param));
	            }
	            
	        // SENSITIVE INFO NO	            
		    } else {
                param.put("NOISE_VAL", (String) resultNoiseMap.get("META_3"));
                param.put("NOISE_PERCENT", (String) resultNoiseMap.get("META_4"));

                param.put("SAMPLE_VAL", (String) resultSampleMap.get("META_3"));
                param.put("SAMPLE_PERCENT", (String) resultSampleMap.get("META_4"));

                // COUNT OR PERCENT
                if ("C".equals(param.get("SAMPLE_TY"))) {
                    param.put("LIMIT_SAMPLE_VAL", param.get("SAMPLE_VAL"));
                } else if("P".equals(param.get("SAMPLE_TY"))) {
                    param.put("LIMIT_SAMPLE_VAL", mapper.selectSamplePercentCount(param));
                }
                // COUNT OR PERCENT
                if ("C".equals(param.get("NOISE_TY"))) {
                    param.put("LIMIT_NOISE_VAL", param.get("NOISE_VAL"));
                } else if("P".equals(param.get("NOISE_TY"))) {
                    param.put("LIMIT_NOISE_VAL", mapper.selectNoisePercentCount(param));
                }   		        
		    }		    
			
			param.put("TMP_CREATE_TBL_NM", "TMP_" + param.get("CREATE_TBL_NM"));
			
			// TMP TABLE DROP
			dropProjectTable((String) param.get("TMP_CREATE_TBL_NM"));
			
			// SAMPLE
			mapper.createTmpKeyBindTable(param);
			// 표본 수
			param.put("SAMPLE_CNT", mapper.selectTmpBindCount(param));
			
			// NOISE
			mapper.insertTmpKeyBindTable(param);

			// 결합 테이블 로우 수
			param.put("BEFORE_BIND_CNT", mapper.selectBeforeBindCount(param));
			// 모의결합 테이블 로우 수
			param.put("BIND_CNT", mapper.selectTmpBindCount(param));
			// 잡음 수
			param.put("NOISE_CNT", mapper.selectNoiseCount(param));
			// 키 테이블 로우 수
			param.put("KEY_BIND_CNT", mapper.selectBeforeKeyCount(param));

			mapper.updateBindInfo(param);

			// 기존 결합 테이블 삭제
			String bindKeyTable = getCreatBindProjectTableNm(param.get("FL_NO"));
			dropProjectTable(bindKeyTable);

			// 결합 테이블 생성
			param.put("BIND_TBL_NM", bindKeyTable);
			mapper.createBindKeyProjectTable(param);

			// 모의결합 데이터 INSERT
			mapper.insertKeyPrebindBindTable(param);

			// TMP TABLE DROP
			dropProjectTable("TMP_" + param.get("CREATE_TBL_NM"));
		}

	}

	/**
	 * 모의결합 키 파일 생성
	 * 
	 * @param param PRJCT_NO FL_NO CREATE_TBL_NM
	 * @return
	 */
	public void makeBindKeyCSVZipFile(Map<String, Object> param) throws PAMsException {
		Object errMsg = new Object();
		Object flNo = new Object();
		Object csvFileID = new Object();

		try {

			errMsg = "";
			Object prjctNo = param.get("PRJCT_NO");
			flNo = param.get("FL_NO").toString();
			Object tableNm = param.get("CREATE_TBL_NM");

			Map<String, Object> csvData = mapper.selectBindCSVFileData(param);

			csvData.put("FL_NO", flNo);
			csvData.put("TABLE_NM", tableNm);

			// csv 파일아이디 저장
			mapper.updateBindFlID(csvData);

			/*
			 * CSV 파일 정보
			 */
			csvFileID = (String) csvData.get("FL_ID");

			// csv 저장경로
			String csvPath = tempFilepath;

			// 저장 파일 경로
			String path = getFilepath();

			path += "/" + prjctNo + "/300";

			File fileDir = new File(path);

			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}

			// 파일 생성정보
			Map<String, Object> fileInfo = new HashMap<>();

			// 파일 저장 정보
			Map<String, Object> fileData = new HashMap<String, Object>();

			String csvFileName = csvFileID + ".csv";

			// 컬럼 정보
			List<Map<String, Object>> columnList = new ArrayList<>();
			Map<String, Object> columnMap = new HashMap<>();
			columnMap.put("CLMN_NM", "결합키");
			columnMap.put("CLMN", "colKey");

			columnList.add(columnMap);

			File csvFile = new File(csvPath + "/" + csvFileName);

			// 기존에 존재하면 파기
			if (csvFile.exists()) {

				mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

				DestructionFile df = new DestructionFile();
				df.run(csvFile, processType);

				if (csvFile.exists()) {
					csvFile.delete();
				}
			}

			fileInfo.put("QOTAT", csvData.get("QOTAT"));
			fileInfo.put("SPRTR", csvData.get("SPRTR"));
			fileInfo.put("ENCD", csvData.get("ENCD"));

			csvFile = new File(csvPath + "/" + csvFileName);

			// 생성
			saveDataCsv(csvPath + "/" + csvFileName, tableNm.toString(), columnList, fileInfo);

			mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

			// 파일 사이즈 bytes > MB
			Long csvFileSize = csvFile.length() / 1024 / 1024;

			if (csvFileSize == 0) {
				csvFileSize = 1L;
			}

			// CSV 파일생성
			fileData.put("FL_ID", csvFileID);
			fileData.put("FL_SN", 1);
			fileData.put("FL_NM", csvFileName);
			fileData.put("FL_SIZE", csvFileSize);
			fileData.put("FL_EXTSN", "csv");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", csvPath + "/" + csvFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

			/*
			 * 암호 생성
			 */
			String key = createKey(10, 2);

			Map<String, Object> keyData = new HashMap<String, Object>();

			keyData.put("KEYID", flNo + "_" + csvFileID + "_KEY");
			keyData.put("KEYV", key);

			mapper.insertProjectZipKey(keyData);

			/*
			 * 파일 압축하기
			 */
			String zipName = csvFileID + ".zip";

			// 압축 파일 명
			String saveZipFileName = String.format("%s_%d", csvFileID, 1);

			File zipFile = new File(path + "/" + saveZipFileName);

			// 기존에 존재하면 파기
			if (zipFile.exists()) {
				DestructionFile df = new DestructionFile();
				df.run(zipFile, processType);

				if (zipFile.exists()) {
					zipFile.delete();
				}
			}

			File zipFilePath = new File(path);

			if (!zipFilePath.exists()) {
				zipFilePath.mkdir();
			}
			zip(csvPath + "/" + csvFileName, path + "/" + saveZipFileName, key);

			// 파일 사이즈 bytes > MB
			zipFile = new File(path + "/" + saveZipFileName);
			Long zipFileSize = zipFile.length() / 1024 / 1024;

			if (zipFileSize == 0) {
				zipFileSize = 1L;
			}

			// ZIP 파일생성
			fileData.put("FL_SN", 2);
			fileData.put("FL_NM", zipName);
			fileData.put("FL_SIZE", zipFileSize);
			fileData.put("FL_EXTSN", "zip");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", path + "/" + saveZipFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

		} catch (Exception e) {

			errMsg = " " + e.getCause();
			log.error("makeBindKeyCSVZipFile ERR : " + " " + e.getCause());

		} finally {

			deleteFile(flNo, csvFileID, 1, true);

		}

		// 오류 발생시 표출
		if (!"".equals(errMsg)) {
			throw new PAMsException(errMsg.toString());
		}

	}

	/**
	 * 모의결합 데이터 csv 및 압축 파일 생성
	 * 
	 * @param
	 * @return
	 * @throws PAMsException
	 */
	public String makeBindCSVZipFile(Map<String, Object> param) throws PAMsException {

        Object errMsg = "";
        Object prjctNo = param.get("PRJCT_NO");
        Object flNo = param.get("FL_NO");
        Object tableNm = param.get("BIND_TBL_NM");

		Map<String, Object> csvData = mapper.selectBindCSVFileData(param);

		csvData.put("FL_NO", flNo);
		csvData.put("TABLE_NM", tableNm);

		// csv 파일아이디 저장
		mapper.updateBindFlID(csvData);

		/*
		 * CSV 파일 정보
		 */
		String csvFileID = (String) csvData.get("FL_ID");

		// csv 저장경로
		String csvPath = tempFilepath;

		// 저장 파일 경로
		String path = getFilepath();

		path += "/" + prjctNo + "/500";

		File fileDir = new File(path);

		if (!fileDir.exists()) {
			fileDir.mkdirs();
		}

		// 파일 생성정보
		Map<String, Object> fileInfo = new HashMap<>();

		// 파일 저장 정보
		Map<String, Object> fileData = new HashMap<String, Object>();

		try {

			String csvFileName = csvFileID + ".csv";

			// 컬럼 정보
			List<Map<String, Object>> columnList = (List<Map<String, Object>>) param.get("COLUMN_LIST");
			
			int i = 0;
			for( Map<String, Object> columnMap : columnList) {
			    columnMap.put("CLMN", columnMap.get("NEW_CLM_NM"));
			    columnList.set(i, columnMap);
			    i++;
			}
			
			
			File csvFile = new File(csvPath + "/" + csvFileName);

			// 기존에 존재하면 파기
			if (csvFile.exists()) {

				mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

				DestructionFile df = new DestructionFile();
				df.run(csvFile, processType);

				if (csvFile.exists()) {
					csvFile.delete();
				}
			}

			fileInfo.put("QOTAT", csvData.get("QOTAT"));
			fileInfo.put("SPRTR", csvData.get("SPRTR"));
			fileInfo.put("ENCD", csvData.get("ENCD"));

			// 생성
			saveDataCsv(csvPath + "/" + csvFileName, tableNm.toString(), columnList, fileInfo);

			mapper.updateGarantCsvFile(csvPath + "/" + csvFileName);

			// 파일 사이즈 bytes > MB
			csvFile = new File(csvPath + "/" + csvFileName);
			Long csvFileSize = csvFile.length() / 1024 / 1024;

			if (csvFileSize == 0) {
				csvFileSize = 1L;
			}

			// CSV 파일생성
			fileData.put("FL_ID", csvFileID);
			fileData.put("FL_SN", 1);
			fileData.put("FL_NM", csvFileName);
			fileData.put("FL_SIZE", csvFileSize);
			fileData.put("FL_EXTSN", "csv");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", csvPath + "/" + csvFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

			/*
			 * 암호 생성
			 */
			String key = createKey(10, 2);

			Map<String, Object> keyData = new HashMap<String, Object>();

			keyData.put("KEYID", flNo + "_" + csvFileID + "_BIND");
			keyData.put("KEYV", key);

			mapper.insertProjectZipKey(keyData);

			/*
			 * 파일 압축하기
			 */
			String zipName = csvFileID + ".zip";

			// 압축 파일 명
			String saveZipFileName = String.format("%s_%d", csvFileID, 1);

			File zipFile = new File(path + "/" + saveZipFileName);
			
			// 기존에 존재하면 파기
			if (zipFile.exists()) {
				DestructionFile df = new DestructionFile();
				df.run(zipFile, processType);

				if (zipFile.exists()) {
					zipFile.delete();
				}
			}

			zip(csvPath + "/" + csvFileName, path + "/" + saveZipFileName, key);

			// 파일 사이즈 bytes > MB
			zipFile = new File(path + "/" + saveZipFileName);
			Long zipFileSize = zipFile.length() / 1024 / 1024;

			if (zipFileSize == 0) {
				zipFileSize = 1L;
			}

			// ZIP 파일생성
			fileData.put("FL_SN", 2);
			fileData.put("FL_NM", zipName);
			fileData.put("FL_SIZE", zipFileSize);
			fileData.put("FL_EXTSN", "zip");
			fileData.put("INDVDLINFO_YN", "Y");
			fileData.put("FL_PTH", path + "/" + saveZipFileName);

			fileData.put("USR_NO", "0");

			mapper.addFile(fileData);

		} catch (Exception e) {

			errMsg = " " + e.getCause();
			log.error("makeBindCSVZipFile ERR : " + " " + e.getCause());

		} finally {

			deleteFile(flNo, csvFileID, 1, true);

		}

		// 오류 발생시 표출
		if (!"".equals(errMsg)) {
			throw new PAMsException(errMsg.toString());
		}

		return csvFileID;
	}
	
	
	/**
	 * 개인정보유형조회
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getMappingList(){
		
		return mapper.selectIndvdlinfoMappingList();
	}
	
    /**
     * 원본 테이블 이름 조회 
     */
    public String getCreatRawTblNm(Object flNo) {
        
        String tableNm = "RAW_" + flNo;
        
        return tableNm;
    }	
    public Map<String, Object> getLatelyScheduleProcessing(Map<String, Object> param) {
		return mapper.selectLatelyScheduleProcessing(param);
	}
    
    /**
    * 원본 파일 파기
    * @param prjctNo
    */
    public void deleteOrgFile(Object prjctNo) {

        Map<String, Object> param = new HashMap<String, Object>();
        
        param.put("PRJCT_NO", prjctNo);
        
        List<Map<String, Object>> orgTblDataList = mapper.selectOrgTblDataList(param);
        
        if(orgTblDataList != null && orgTblDataList.size() > 0) {
            
            for(Map<String, Object> orgTblData : orgTblDataList) {
                
                //사후관리 이력 획득을 위한 파일 정보 획득
                List<Map<String,Object>> fileList = getFileInfos(orgTblData.get("FILE_ID"));
                StringBuffer strFileDstrc = new StringBuffer();
                
                for(Map<String,Object> file : fileList) {
                    strFileDstrc.append(strFileDstrc.length() <= 0?"":",")
                    .append(file.get("FL_NM"));
                }
                
                destructionFiles(prjctNo, orgTblData.get("FILE_ID"));
                
                //원본 파일 파기 후 파기 이력 사후관리 이력에 포함
                SimpleDateFormat fm = new SimpleDateFormat ("yyyy-MM-dd");         
                Date nowDate = new Date();
                
                Map<String,Object> codeMap = new HashMap<String,Object>();
                codeMap.put("GRP_CD", "DATA_DESTORY");
                codeMap.put("META_3", "Y");
                
                List<Map<String, Object>> codeList = getCodeList(codeMap); 
                Map<String,Object> code = codeList.get(0);
                
                param.put("PRJCT_NO", prjctNo);
                param.put("MG_DE", fm.format(nowDate));
                param.put("MG_GOV", code.get("META_2").toString());
                param.put("INFO", "원본파일 : "+strFileDstrc.toString());
                param.put("DES_YN", "Y");
                param.put("MG_TYPE","1");
                param.put("FILE_ID", orgTblData.get("FILE_ID"));
                
                carryoutMapper.insertPostManagement(param);             
            }
        }
    }
    
    /**
     * 파일 파기 로직
     * @param proNo
     * @param fileId
     * @param sn
     * @param filePath
     * @param processType
     */
    public void startDestructionFileSchedule(Object proNo, Object fileId, Object sn, String filePath, String processType) {
        if (filePath != null) {
            Map<String, Object> setParam = new HashMap<String, Object>();
            
            setParam.put("USR_NO", getLoginUserNo());
            setParam.put("PRJCT_NO", proNo);
            setParam.put("FILE_ID", fileId);
            setParam.put("FILE_SN", sn);
            
            // 1 : US DoD 5220.22-M(8-306./E) , 2 : Peter Gutmann
            if("1".equals(processType)) {
                setParam.put("DSTRC_WAY","US DoD 5220.22-M(8-306./E)");
            } else if("2".equals(processType)) {
                setParam.put("DSTRC_WAY","Peter Gutmann");
            }
            
            mapper.setDstrcMnldg(setParam);
            
            // 현재 매개변수 파일 패스만 적용됨
            DestructionFileTask df = new DestructionFileTask(filePath, processType);
            
            Thread thread = new Thread(df);
            thread.start();
            
            mapper.updateDstrcMnldg(setParam);
        }
        
    }    
    
    /** 프로젝트 파기
     * @param param
     * @return
     * @return Map<String,Object>
     */
    public Map<String,Object> dstrcProject(Object prjctNo, Object rvwSn, Object usrNo){
        Map<String,Object> resultMap = new HashMap<String,Object>(),
                           paramMap = new HashMap<String,Object>(),
                           dstrcParamMap = new HashMap<String,Object>();
        
        StringBuffer strDstrc =  new StringBuffer();
        
        paramMap.put("PRJCT_NO", prjctNo);
        Object stepCd = mapper.selectProjectStep(paramMap);
        
        resultMap.put("STEP_CD", stepCd);
        
        try {
            List<Map<String, Object>> fileInfoList = mapper.selectDstrcTblDataList(paramMap);
            StringBuffer strFileDstrc = new StringBuffer();
            StringBuffer strDataDstrc = new StringBuffer();
            List<Object> tableList = new ArrayList<Object>();
            
            for( Map<String, Object> fileInfoMap : fileInfoList) {
                paramMap.put("FL_TY_CD", fileInfoMap.get("FL_TY_CD"));
                paramMap.put("FL_ID", fileInfoMap.get("FILE_ID"));
                paramMap.put("FL_NO", fileInfoMap.get("FL_NO"));
                paramMap.put("MAIN_FL_NO", fileInfoMap.get("MAIN_FL_NO"));

                Object rawTbl = mapper.confirmTableNm(getCreatRawTblNm(paramMap.get("FL_NO")));
                Object keyTbl = mapper.confirmTableNm(getCreatKeyTblNm(paramMap.get("FL_NO")));
                Object dataTbl = mapper.confirmTableNm(getCreatDataTblNm(paramMap.get("FL_NO")));
                Object bindTbl = mapper.confirmTableNm(getCreatBindProjectTableNm(paramMap.get("MAIN_FL_NO")));
                
                if(rawTbl != null && !tableList.contains(rawTbl) && !"".equals(rawTbl)) {
                    log.debug("confirm table : " + rawTbl);
                    strDataDstrc.append("원본 데이터 : ")
                        .append(rawTbl)
                        .append("\n");
                    
                    tableList.add(rawTbl);    
                }
                                    
                if(keyTbl != null && !tableList.contains(keyTbl) && !"".equals(keyTbl)) {
                    log.debug("confirm table : " + keyTbl);
                    strDataDstrc.append("전처리 데이터(키) : ")
                        .append(keyTbl)
                        .append("\n");
                    
                    tableList.add(keyTbl);    
                }
                
                if(dataTbl != null && !tableList.contains(dataTbl) && !"".equals(dataTbl)) {
                    log.debug("confirm table : " + dataTbl);
                    strDataDstrc.append("전처리 데이터 : ")
                        .append(dataTbl)
                        .append("\n");
                    
                    tableList.add(dataTbl);
                }
                
                
                if(bindTbl != null && !tableList.contains(bindTbl) && !"".equals(bindTbl)) {
                    log.debug("confirm table : " + bindTbl);
                    strDataDstrc.append("결합 데이터 : ")
                        .append(bindTbl)
                        .append("\n");
                    
                    tableList.add(bindTbl);
                }
                
                // 완전 삭제 : 결합 데이터 파일                
                if("D".equals(paramMap.get("FL_TY_CD"))) {
                    
                    List<Map<String,Object>> fileList = mapper.getFileInfos(paramMap.get("FL_ID"));
                    
                    for(Map<String,Object> file : fileList) {
                        strFileDstrc.append(strFileDstrc.length() <= 0?"":",").append(file.get("FL_NM"));
                        deleteFile(prjctNo, file.get("FL_ID"),file.get("FL_SN"), true);
                    }
                // 삭제 : 결합 키 파일                    
                } else {

                    List<Map<String,Object>> fileList = mapper.getFileInfos(paramMap.get("FL_ID"));
                    
                    for(Map<String,Object> file : fileList) {
                        strFileDstrc.append(strFileDstrc.length() <= 0?"":",").append(file.get("FL_NM"));
                        deleteFile(prjctNo, file.get("FL_ID"),file.get("FL_SN"), false);
                    }
                    
                }
            }

            /*반출파일 파기 정보 삽입*/
            if(strFileDstrc.length() > 0) {
                strDstrc.append("반출파일 :").append(strFileDstrc.toString()).append("\n");
            }

            if(strDataDstrc.length() > 0) {
                strDstrc.append(strDstrc.length() <= 0?"":",").append(strDataDstrc.toString());    
            } 

            log.debug("confirm dstrc : " + strDstrc);
            
            //해당 파일들의 파기 정보를 획득
            List<Map<String,Object>> dstrcDataLogList = mapper.selectDstrcDataMnldg(paramMap);
            
            for(Object targetTbl : tableList) {
                if(targetTbl !=null && !"".equals(targetTbl)) {
                    boolean isPass = false;
                    //파기대장에 TABLE이 포함되어 있으면 파기 패스
                    for(Map<String,Object> temp : dstrcDataLogList) {
                        if(targetTbl.equals(temp.get("TBL_NM"))) {
                            isPass = true;
                            break;
                        }
                    }
                    
                    if(!isPass) {       
                        paramMap.clear();
                        paramMap.put("TBL_NM", targetTbl);
                        int cnt = mapper.chkTableCnt(paramMap);
                        //테이블이 존재할경우 파기
                        if(cnt >0) {
                            dstrcParamMap.put("METHOD",0);    //프로시져 내부에서 사용하지 않는 메소드
                            dstrcParamMap.put("PRJCT_NO", prjctNo);
                            dstrcParamMap.put("TBL_NM", targetTbl);
                            
                            mapper.dataDestroy(dstrcParamMap);
                            Map<String,Object> destroyErrExcepMap = getCodeInfo("PROC_ERR_EXECP", "DESTROY_ERR_EXCEP");
                            
                            //프로시저 애러처리
                            if( "ERROR".equals(dstrcParamMap.get("RESULT_MSG")) ) {
                                
                                dstrcParamMap.put("USR_NO", 0); //SYSTEM
                                dstrcParamMap.put("RVW_SN", rvwSn); 
                                dstrcParamMap.put("PROC_TY",3); //3:파기
                                mapper.insertProcErrorLog(dstrcParamMap);
                                
                                if( destroyErrExcepMap != null && !"Y".equals(destroyErrExcepMap.get("META_1")) ) {
                                    
                                    paramMap.put("ERR_MSG",dstrcParamMap.get("ERR_MSG").toString());
                                    paramMap.put("result", "error");
                                    paramMap.put("STEP_CD", stepCd);
                                    return paramMap;                                
                                }
                            }   
                            
                            //파기 후 파기대장에 삽입     
                            paramMap.put("PRJCT_NO", prjctNo);                          
                            paramMap.put("USR_NO", usrNo);
                            
                            mapper.insertDstrcDataMnldg(paramMap);                          
                            
                        }
                    }
                }
            }               
            
            //프로젝트 파기여부 업데이트
            paramMap.clear();
            paramMap.put("PRJCT_NO", prjctNo);
            paramMap.put("DUS_YN","Y");
            paramMap.put("USR_NO", usrNo);
            mapper.updateProjectDltYn(paramMap);
            
            //파기 정보 기록
            SimpleDateFormat fm = new SimpleDateFormat ( "yyyy-MM-dd");         
            Date nowDate = new Date();
            Map<String,Object> codeMap = new HashMap<String,Object>();
            codeMap.put("GRP_CD", "DATA_DESTORY");
            codeMap.put("META_3", "Y");
            List<Map<String, Object>> codeList = getCodeList(codeMap); 
            Map<String,Object> code = codeList.get(0);          
            
            paramMap.clear();
            paramMap.put("PRJCT_NO", prjctNo);
            paramMap.put("MG_DE",fm.format(nowDate));
            paramMap.put("MG_GOV",code.get("META_2").toString());
            paramMap.put("INFO",strDstrc.toString());
            paramMap.put("DES_YN", "Y");
            paramMap.put("MG_TYPE","1");
            carryoutMapper.insertPostManagement(paramMap);
            
            resultMap.put("result","success");
        } catch(Exception ex) {
            log.error("destruction Exception :: {}" + ex.getCause());
            resultMap.put("result","error");
            resultMap.put("ERR_MSG", ex.getCause());
            
            paramMap.clear();
            paramMap.put("PRJCT_NO", prjctNo);
            paramMap.put("DUS_YN","N");
            paramMap.put("USR_NO", usrNo);
            mapper.updateProjectDltYn(paramMap);
        } finally {
            return resultMap;
        }
        
    }    
}
