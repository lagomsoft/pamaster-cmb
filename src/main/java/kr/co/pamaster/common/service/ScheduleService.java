package kr.co.pamaster.common.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import kr.co.pamaster.carryout.service.CarryoutService;
import kr.co.pamaster.common.mapper.CommonMapper;
import kr.co.pamaster.util.DestructionFileTask;
import kr.co.pamaster.util.PAMsException;

@Service("pamScheduleService")
public class ScheduleService {

	static private int multiSchedulePoolSize = 5;
	static private long multiSchedulePoolSizeCheckTime = 0;

	//실행중인 스래드 수
	static private int usedMultiSchedulePoolCnt = 0;

	@Autowired
	private CommonMapper mapper;
	
	@Autowired
	private CommonService service;
	
	@Autowired
	private CarryoutService carryoutService;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
    public boolean getCheckLocal() {
        return service.getCheckLocal();
    }

	/**
	 * 스케줄 조회
	 * @return
	 */
	public List<Map<String, Object>> getScheduleList(){
		
		List<Map<String, Object>> scheduleList = mapper.selectScheduleList();
		//처리 중 상태로 변경
		for (Map<String, Object> scheduleMap : scheduleList) {
			updateStep("300",scheduleMap);
		}
		
		return scheduleList;
	}
	
	
	/**
	 * 즉시 처리 스케줄 조회
	 * @return
	 */
	public List<Map<String, Object>> getNowScheduleList(){

		List<Map<String, Object>> nowScheduleList = mapper.selectNowScheduleList();
		//처리 중 상태로 변경
		for (Map<String, Object> scheduleMap : nowScheduleList) {
			updateStep("300", scheduleMap);
		}
		
		return nowScheduleList;
	}
	
	/**
	 * 임시파일 존재할 경우 삭제 처리
	 */
	public void clearTempFilepath() {
		service.clearTempFilepath();
	}
	
	/**
	 * 프로세스 단계 교체 
	 */
	public void updateStep(String stepCd,Map<String,Object> param) {
		param.put("PROGRS_CD", stepCd);
		mapper.updateScheduleProgrsCd(param);
	}
	
	/**
	 * 스케줄 초기 POOL 사이즈를 세팅
	 */
	public void settingMultiSchedulePoolSize() {

		//1시간마다 시간체크하여 세팅
		if((new Date()).getTime() > (multiSchedulePoolSizeCheckTime +  (1000 * 60 * 60) )) {
			multiSchedulePoolSizeCheckTime= (new Date()).getTime();
			Map<String, Object> codeMap = service.getCodeInfo("SCHEDULE_SETTING", "BATCH_MAX_VOLUME");
			
			if(codeMap != null && codeMap.get("META_1") != null) {
				try {
					multiSchedulePoolSize = Integer.parseInt(codeMap.get("META_1").toString());					
				} catch (NumberFormatException e) {
					multiSchedulePoolSize = 2;
				}
			}
			//최소 1개
			if(multiSchedulePoolSize <= 0) {
				multiSchedulePoolSize = 1;
			}
			//최대 1000개
			if(multiSchedulePoolSize > 1000) {
				multiSchedulePoolSize = 1000;
			}
		}
		
	}
	
	/**
	 * 스래드 가능 수
	 * @return
	 */
	public int getFreeMultiScheduleThreadCnt() {
		return multiSchedulePoolSize - usedMultiSchedulePoolCnt;
	}
	
	/**
	 * 동작 중인 스래드 수
	 * @return
	 */
	public int getUseMultiScheduleThread() {
		
		if(usedMultiSchedulePoolCnt < 0) {
			usedMultiSchedulePoolCnt = 0;
		}
		
		return usedMultiSchedulePoolCnt;
	}
	
	
	
	/**
	 * 스케줄 처리
	 * @param scheduleMap
	 */
	public void runScheduleThread(Map<String, Object> scheduleMap) {

		//시작시 카운트 업
		usedMultiSchedulePoolCnt++;
		
		String proStepCd = "";
		
		try {
            /*
             * 스케줄 시작
             */
            updateStep("350", scheduleMap);
            
            switch ((String) scheduleMap.get("STEP_CD")) {
                /*
                 * 전처리 요청(결합키)
                 */
                case "150" : 
                	
                	List<Map<String, Object>> orgcolumnList = mapper.selectScheduleProjectColumnList(scheduleMap);
    				List<Map<String, Object>> newColumList = new ArrayList<Map<String, Object>>();
    				
    				if (orgcolumnList != null && orgcolumnList.size() > 0) {
    					
    					proStepCd = "130";

						/*
						 * 처리순번 변경
						 */
    					//mapper.updateProjectHndlSn(scheduleMap); // 가명처리기술을 적용하지 않는 이상 처리순번 정렬은 필요없음

    					// 처리 상태저장
    					updateStep("350",scheduleMap);
    					
    					String rawTableNm = service.getCreatProjectTableNm(scheduleMap.get("FL_NO"), "R");
    					String keyTableNm = service.getCreatProjectTableNm(scheduleMap.get("FL_NO"), "K");
    					
    					scheduleMap.put("ORG_TBL", rawTableNm);
    					//scheduleMap.put("ORG_TBL_ROWS", projectTableMap.get("ORG_TBL_ROWS")); // 사용하지 않음
    					scheduleMap.put("PRO_TABLE", keyTableNm);

    					/*
    					 * 컬럼 속성 확인
    					 */
    					for (Map<String, Object> columnMap : orgcolumnList) {

    						columnMap.put("ORG_TBL", scheduleMap.get("ORG_TBL"));

    						List<String> newRemoveValList = new ArrayList<String>();

    						// 숫자일 경우 숫자 제거값 저장
    						if ("N".equals(columnMap.get("CLM_TY"))) {

    							// 숫자 아닌 문자추출
    							List<String> rwmovValList = mapper.selectNumberRwmovVal(columnMap);

    							if (rwmovValList != null && rwmovValList.size() > 0) {
    								
    								int pointCnt = 0;
    								
    								for (String rwmovVal : rwmovValList) {

    									String[] rwmovVals = rwmovVal.split("");

    									for (String val : rwmovVals) {
    										// 소수점 2개 이상일 경우 포인트 제거
    										if (".".equals(val)) {
    											pointCnt++;
    											continue;
    										}
    										if (!newRemoveValList.contains(val)) {
    											newRemoveValList.add(val);
    										}
    									}
    								}
    								
    								/*
    								// 제거 값 저장
    								if(newRemoveValList.size() > 0 && !"PD00002".equals(columnMap.get("INDVDLINFO_CD")) ) {
    									columnMap.put("REMOVE_LIST", newRemoveValList);
    									mapper.addNumberRwmovVal(columnMap);										
    								} INDVDLINFO_CD 미사용
    								*/
    								
    								if(pointCnt > 1) {
    									newRemoveValList.add(".");
    								}
    							}

    						}

    						Map<String, Object> newColumnMap = mapper.checkColumnLenType(columnMap);

    						newColumnMap.put("REMOVE_LIST", newRemoveValList);

    						mapper.updateColumnLenType(newColumnMap);
    						
    						newColumList.add(newColumnMap);
    					}

    					scheduleMap.put("COLUMN_LIST", newColumList);
    					
    					// 처리 상태저장
    					updateStep("500",scheduleMap);

    					/*
    					 * 전처리테이블 생성
    					 */
    					mapper.createProjectTableChar(scheduleMap);
    					scheduleMap.put("ADD_HNDL_TBL", keyTableNm);
    					
    					// 처리 상태저장
    					updateStep("600",scheduleMap);
    					
    					/*
    					 * 전처리 테이블에 데이터 추가
    					 */
    					mapper.addDataPreprocessingData(scheduleMap);
    					
    					// 처리 상태저장
    					updateStep("800",scheduleMap);
    					
    					proStepCd = "200";
    					
    					// 원본파일 삭제
    					service.deleteOrgFile(scheduleMap.get("PRJCT_NO"));

    				}
    
                    break;
    
                /*
                 * 모의결합 요청(결합키)
                 */
                case "250":  
                    // 결합 오류
                    proStepCd = "230";
                    updateStep("400",scheduleMap);
                    
                    // 기존 결합 테이블 삭제 
                    String bindKeyTable = service.getCreatBindProjectTableNm(scheduleMap.get("FL_NO"));
                    service.dropProjectTable(bindKeyTable);
                    
                    Object bindKey = mapper.selectBindKey(scheduleMap);
                    scheduleMap.put("KEY_CLM_NM", bindKey);
                    updateStep("410",scheduleMap);
                    
                    Object mainTable = mapper.selectKeyMainTable(scheduleMap);
                    scheduleMap.put("MAIN_TBL_NM", mainTable);
                    updateStep("420",scheduleMap);
                    
                    // 키결합 설정 정보 조회
                    List<Map<String, Object>> prebindInfoList = new ArrayList<>();
                    prebindInfoList = service.getPrebindKeyInfoList(scheduleMap);
                               
                    updateStep("430",scheduleMap);
                    
                    // 결합 테이블 생성 
                    scheduleMap.put("BIND_TBL_NM", bindKeyTable);
                    mapper.createBindKeyProjectTable(scheduleMap);             
                    updateStep("440",scheduleMap);
                    
                    // 결합 처리
                    scheduleMap.put("FL_LIST", prebindInfoList);
                    mapper.insertBindProjectKey(scheduleMap);              
                    updateStep("500",scheduleMap);
                    
                    // 결합신청자 별 결합 테이블 생성
                    service.postCopyPrebindKey(prebindInfoList);
                    updateStep("600",scheduleMap);
                    
                    // 기준 키 정보 삽입
                    prebindInfoList.add(mapper.selectMainKeyInfo(scheduleMap));
                    
                    //결합건수, 결합률 생성
                    //key
                    //function
                    //data
                    
                    
                    // 노이즈 및 샘플 생성
                    service.postAddNoiseSample(prebindInfoList);
                    updateStep("700",scheduleMap);
                    
                    // csv 파일 생성
                    for(int pi = 0; pi < prebindInfoList.size(); pi++) {
                        
                        service.makeBindKeyCSVZipFile(prebindInfoList.get(pi));
                    }
                    
                    updateStep("800",scheduleMap);
                    
                    proStepCd = "300";                    
                    
                    break;
    
                /*
                 * 전처리 요청(데이터)
                 */
                case "350":

					List<Map<String, Object>> orgcolumnList_data = mapper.selectScheduleProjectColumnList(scheduleMap);
					List<Map<String, Object>> newColumList_data = new ArrayList<Map<String, Object>>();

					if (orgcolumnList_data != null && orgcolumnList_data.size() > 0) {

						proStepCd = "330";

						/*
						 * 처리순번 변경
						 */
						//mapper.updateProjectHndlSn(scheduleMap); // 가명처리기술을 적용하지 않는 이상 처리순번 정렬은 필요없음

						// 처리 상태저장
						updateStep("350",scheduleMap);

						String rawTableNm = service.getCreatProjectTableNm(scheduleMap.get("FL_NO"), "R");
						String dataTableNm = service.getCreatProjectTableNm(scheduleMap.get("FL_NO"), "D");

						scheduleMap.put("ORG_TBL", rawTableNm);
						//scheduleMap.put("ORG_TBL_ROWS", projectTableMap.get("ORG_TBL_ROWS")); // 사용하지 않음
						scheduleMap.put("PRO_TABLE", dataTableNm);

						/*
						 * 컬럼 속성 확인
						 */
						for (Map<String, Object> columnMap : orgcolumnList_data) {

							columnMap.put("ORG_TBL", scheduleMap.get("ORG_TBL"));

							List<String> newRemoveValList = new ArrayList<String>();

							// 숫자일 경우 숫자 제거값 저장
							if ("N".equals(columnMap.get("CLM_TY"))) {

								// 숫자 아닌 문자추출
								List<String> rwmovValList = mapper.selectNumberRwmovVal(columnMap);

								if (rwmovValList != null && rwmovValList.size() > 0) {

									int pointCnt = 0;

									for (String rwmovVal : rwmovValList) {

										String[] rwmovVals = rwmovVal.split("");

										for (String val : rwmovVals) {
											// 소수점 2개 이상일 경우 포인트 제거
											if (".".equals(val)) {
												pointCnt++;
												continue;
											}
											if (!newRemoveValList.contains(val)) {
												newRemoveValList.add(val);
											}
										}
									}

    								/*
    								// 제거 값 저장
    								if(newRemoveValList.size() > 0 && !"PD00002".equals(columnMap.get("INDVDLINFO_CD")) ) {
    									columnMap.put("REMOVE_LIST", newRemoveValList);
    									mapper.addNumberRwmovVal(columnMap);
    								} INDVDLINFO_CD 미사용
    								*/

									if(pointCnt > 1) {
										newRemoveValList.add(".");
									}
								}

							}

							Map<String, Object> newColumnMap = mapper.checkColumnLenType(columnMap);

							newColumnMap.put("REMOVE_LIST", newRemoveValList);

							mapper.updateColumnLenType(newColumnMap);

							newColumList_data.add(newColumnMap);
						}

						scheduleMap.put("COLUMN_LIST", newColumList_data);

						// 처리 상태저장
						updateStep("500",scheduleMap);

						/*
						 * 전처리테이블 생성
						 */
						mapper.createProjectTableChar(scheduleMap);
						scheduleMap.put("ADD_HNDL_TBL", dataTableNm);

						// 처리 상태저장
						updateStep("600",scheduleMap);

						/*
						 * 전처리 테이블에 데이터 추가
						 */
						mapper.addDataPreprocessingData(scheduleMap);
						
						// 처리 상태저장
                        updateStep("700",scheduleMap);
                        /*
                         * 결합키~~
                         */
                        Map<String, Object> dataInfo = mapper.selectDataInfo(scheduleMap);
                        
                        Map<String, Object> bindFileNo = mapper.selectBindKeyFileNo(dataInfo);
                        
                        
                        Map<String, Object> matchMap = new HashMap<String, Object>();
                        matchMap.put("APL_NO", dataInfo.get("APL_NO"));
                        
                        String bindTableNm = service.getCreatBindProjectTableNm(bindFileNo.get("FL_NO"));
                        matchMap.put("BIND_TBL_NM", bindTableNm);
                        
                        String keyTableNm = service.getCreatProjectTableNm(dataInfo.get("FL_NO"), "D");
                        matchMap.put("MAIN_TBL_NM", keyTableNm);
                        
                        matchMap.put("KEY_CLM_NM", dataInfo.get("KEY_CLM_NM"));
                        mapper.updateMatchCnt(matchMap);
                        

						// 처리 상태저장
						updateStep("800",scheduleMap);

						proStepCd = "400";

					}
                    
                    break;
    
                /*
                 * 모의결합 요청(데이터)
                 */
                case "450":
                    
                    // 결합 오류
                    proStepCd = "430";
                    updateStep("400",scheduleMap);
                    
                    // 원본파일 삭제
                    service.deleteOrgFile(scheduleMap.get("PRJCT_NO"));
                    
                    // 기존 결합 테이블 삭제
                    String bindDataTable = service.getCreatBindProjectTableNm(scheduleMap.get("FL_NO"));
                    service.dropProjectTable(bindDataTable);
                    
                    List<Map<String, Object>> columnList = mapper.selectBindColumnList(scheduleMap);
                    scheduleMap.put("COLUMN_LIST", columnList);
                    updateStep("450",scheduleMap);
                    
                    Object mainDataTable = mapper.selectDataMainTable(scheduleMap);
                    scheduleMap.put("MAIN_TBL_NM", mainDataTable);
                    updateStep("500",scheduleMap);
                    
                    // 키결합 설정 정보 조회
                    List<Map<String, Object>> prebindInfoDataList = service.getPrebindDataInfoList(scheduleMap);
                    scheduleMap.put("FL_LIST", prebindInfoDataList);
                    updateStep("430",scheduleMap);
                    
                    // 결합 테이블 생성 
                    scheduleMap.put("BIND_TBL_NM", bindDataTable);
                    mapper.createBindProjectTable(scheduleMap);             
                    updateStep("440",scheduleMap);
                    
                    // 결합 처리
                    mapper.insertBindProjectData(scheduleMap);              
                    updateStep("500",scheduleMap);                    
                    
                    // csv 파일 생성     
                    service.makeBindCSVZipFile(scheduleMap);
                    updateStep("800",scheduleMap);
                    
                    proStepCd = "500";
                    
                    break;
                    
                /*
                 * 프로젝트 파기(전체)
                 */
                case "999":
                    
                    
                    Map<String,Object> result = service.dstrcProject(scheduleMap.get("PRJCT_NO"), scheduleMap.get("RVW_SN"), 0);
                    
                    log.debug("confirm result : " + result.entrySet());
                    
                    if("error".equals(result.get("result"))) {//처리중 애러 발생시 
                        throw new PAMsException(result.get("ERR_MSG").toString());
                    }
                    
                    
                    proStepCd = (String) result.get("STEP_CD");
                    break;
            
            }
            
            // 종료
            updateStep("900",scheduleMap);
            
            scheduleMap.put("STEP_CD", proStepCd);
            scheduleMap.put("PROJECT_NO", scheduleMap.get("PRJCT_NO"));
            
            service.updateProjectStep(scheduleMap);
            
		} catch (Exception e) {
		    log.debug("confirm Exception : " + e.getCause());
            updateStep("999",scheduleMap);
            
            scheduleMap.put("STEP_CD", proStepCd);
            scheduleMap.put("PROJECT_NO", scheduleMap.get("PRJCT_NO"));
            
            service.updateProjectStep(scheduleMap);			
		}
		//종료시 카운트 다운
		usedMultiSchedulePoolCnt--;
		
	}
	
	/* 파기 common으로 이동
	public void startDestructionFileSchedule(Object proNo, Object fileId, Object sn, String filePath, String processType) {
		if (filePath != null) {
			Map<String, Object> setParam = new HashMap<String, Object>();
			
			setParam.put("USR_NO", service.getLoginUserNo());
			setParam.put("PRJCT_NO", proNo);
			setParam.put("FILE_ID", fileId);
			setParam.put("FILE_SN", sn);
			
			if("1".equals(processType)) {
				setParam.put("DSTRC_WAY","US DoD 5220.22-M(8-306./E)");
			} else if("2".equals(processType)) {
				setParam.put("DSTRC_WAY","Peter Gutmann");
			}
			
			mapper.setDstrcMnldg(setParam);
			
			File file = new File(filePath);
			
			DestructionFileTask df = new DestructionFileTask(file, processType);
			
			Thread thread = new Thread(df);
			thread.start();
			
			mapper.updateDstrcMnldg(setParam);
		}
		
	}
	*/
	
	/**
	 * 유저 사용시간 체크
	 * 
	 */
	@Async
	public void userUsingTimeCheck() {
		Map<String, Object> param = new HashMap<String, Object>();
		
		Map<String, Object> codeMap = service.getCodeInfo("PASSWORD_POLICY_CD", "LAST_LOGIN_MAX_DAY");
		
		if(codeMap !=  null && codeMap.get("META_1") != null && !"".equals(codeMap.get("META_1")) && !"0".equals(codeMap.get("META_1")) ) {
			param.put("LOGIN_MAX_DAY", codeMap.get("META_1"));
			
			mapper.updateUserLock(param);
		}
	}
	
	/**
     * 자동파기
     * @throws PAMsException 
     * 
     */
    @Async
    public void autoDestructionSchedule() throws PAMsException {
       Map<String, Object> param = new HashMap<>();
       
       Map<String, Object> codeParam = new HashMap<>();
       codeParam.put("GRP_CD", "DESTRUCTION_POLICY");
       codeParam.put("CD", "DESTRUCTION_MAX_DAY");
       Map<String, Object> codeMap = mapper.getCodeInfo(codeParam);
       
       if((codeMap !=  null && codeMap.get("META_1") != null && !"".equals(codeMap.get("META_1")) && !"0".equals(codeMap.get("META_1")) )) {
           param.put("DESTRUCTION_MAX_DAY", codeMap.get("META_1"));
           
           // 활용 프로젝트, 결합 프로젝트 각각 가져오기
           List<Map<String, Object>> destructionList = mapper.selectDestructionCheckList(param);
           List<Map<String, Object>> joinDestructionList = mapper.selectJoinDestructionCheckList(param);
           
           if(destructionList.size() > 0) {
               for(Map<String, Object> destructionMap : destructionList) {
                   log.debug("confirm PRO : " + destructionMap.entrySet());
                   carryoutService.destruction(destructionMap);
               }
           }
           
           if(joinDestructionList.size() > 0) {
               for(Map<String, Object> joinDestructionMap : joinDestructionList) {
                   log.debug("confirm JOIN : " + joinDestructionMap.entrySet());
                   carryoutService.destruction(joinDestructionMap);
               }
           }
           
       }
       
    }
    
    /**
     * 감사로그 자동삭제
     * 
     */
    @Async
    public void autoDeleteLogSchedule() {
        Map<String, Object> param = new HashMap<>();
        
        Map<String, Object> codeParam = new HashMap<>();
        codeParam.put("GRP_CD", "DESTRUCTION_POLICY");
        codeParam.put("CD", "DELETE_LOG_MAX_DAY");
        Map<String, Object> codeMap = mapper.getCodeInfo(codeParam);
        
        if((codeMap !=  null && codeMap.get("META_1") != null && !"".equals(codeMap.get("META_1")) && !"0".equals(codeMap.get("META_1")) )) {
            param.put("DELETE_LOG_MAX_DAY", codeMap.get("META_1"));
            mapper.deleteDestructionLog(param);
        }
    }
}
