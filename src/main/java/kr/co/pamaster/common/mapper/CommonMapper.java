package kr.co.pamaster.common.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommonMapper {

	public void startAvocadoSigma(Map<String, Object> param);

	public void startAvocadoCorr2(Map<String, Object> param);

    public void saveDataPreprocessingCsv(Map<String, Object> param);

    public void saveDataCsv(Map<String, Object> param);
    
    public void updateJoinProjectStep(Map<String, Object> param);
    
    public void updateProjectStep(Map<String, Object> param);

    public void addDataPreprocessingData(Map<String, Object> param);

    public void startAvocadoStat(Map<String, Object> param);

    public void startAvocadoKano(Map<String, Object> param);

    public void startAvocadoCorr(Map<String, Object> param);

    public void startAvocadoFreq(Map<String, Object> param);

    public void startAvocadoNorm(Map<String, Object> param);

    public void startAvocadoGrtp(Map<String, Object> param);

    public void startAvocadoGrbm(Map<String, Object> param);

    public void startAvocadoKltv(Map<String, Object> param);

    public void startSigmaBase(Map<String, Object> param);
    
    public void startProcess(Map<String, Object> param);
    
    public void insertScheduleManagement(Map<String, Object> param);
    
    public void commentWrite(Map<String, Object> param);

    public void commentDelete(Map<String, Object> param);

    public void commentModify(Map<String, Object> param);   	

    public void setBoard(Map<String, Object> param);

    public void updateBoard(Map<String, Object> param);

    public void deleteBoard(Map<String, Object> param);

    public void deleteFiles(Map<String, Object> param);

    public void startAditProcess(Map<String, Object> param);

    public void deleteAvocadoCorr(Map<String, Object> param);

    public void deleteAvocadoFreq(Map<String, Object> param);

    public void deleteAvocadoGrbm(Map<String, Object> param);

    public void deleteAvocadoGrtp(Map<String, Object> param);

    public void deleteAvocadoKano(Map<String, Object> param);

    public void deleteAvocadoKltv(Map<String, Object> param);

    public void deleteAvocadoLtv(Map<String, Object> param);

    public void deleteAvocadoNorm(Map<String, Object> param);

    public void deleteAvocadoStat(Map<String, Object> param);

    public void removeProjectRowNo(Map<String, Object> param);

    public void addProjectRowNo(Map<String, Object> param);

    public void addProjectRowNoIdx(Map<String, Object> param);

    public void addProjectRowNoData(Map<String, Object> param);

    public void deleteProjectInfoTable(Map<String, Object> param);

    public void createMappingTable(Map<String, Object> param);

    public void createJoinKeyTable(Map<String, Object> param);

    public void insertJoinKeyData(Map<String, Object> param);

    public void deleteAditAliasInfo(Map<String, Object> param);

    public void recordConnectionLogin(Map<String, Object> param);

    public void recordHist(Map<String, Object> param);      
    
    public void setDstrcMnldg(Map<String, Object> param);

    public void updateDstrcMnldg(Map<String, Object> param);
    
    public void updateProjectAttachID(Map<String, Object> param);

    public void updateJoinAttachID(Map<String, Object> param);

    public void insertProjectZipKey(Map<String, Object> param);

    public void setPricacyDetection(Map<String, Object> param);
    
	public void createJoinProjectTable(Map<String,Object> param);
	
	public void createBindProjectTable(Map<String,Object> param);
	
	public void createBindKeyProjectTable(Map<String,Object> param);
	
	public void startAvocadoProcess(Map<String,Object> param);
	  
	public void insertFailLoginHistRecording(Map<String, Object> param);
    
    public void startJoinAditProcess(Map<String, Object> param);
    
    public void updateUserLock(Map<String, Object> param);
    
    public void deleteDestructionLog(Map<String, Object> param);
    
    public void insertAutoRiskCheckData(Map<String, Object> param);
        
    public void createCopyPrebindKey(Map<String, Object> param);
    
    public void updateBindFlID(Map<String, Object> param);
    
    public void createTmpKeyBindTable(Map<String, Object> param);
    
    public void insertTmpKeyBindTable(Map<String, Object> param);
    
    public void insertKeyPrebindBindTable(Map<String, Object> param);
    
    public void updateBindInfo(Map<String, Object> param);
    
    public void updateMatchCnt(Map<String, Object> param);
    
    public void insertDestructScheduleManagement(Map<String, Object> param);
    
    public void truncateProjectTable(Map<String, Object> param);
    
    public int selectMaxFlNo();

    public int getFileMaxSeq(Object fileId);
    
    public int addFile(Map<String, Object> param);

    public int addFileInfo(Map<String, Object> param);

    public int deleteFileClmInfo(Map<String, Object> param);

    public int deleteFile(Map<String, Object> param);
    
    public int updateProjectColumnRvwSn(Map<String, Object> param);
    
    public int addNumberRwmovVal(Map<String, Object> param);

    public Integer selectProjectRowCnt(Map<String, Object> param);

    public int updateProjectHndlSn(Map<String, Object> param);

    public int updateColumnLenType(Map<String, Object> param);

    public int dropProjectTable(Map<String, Object> param);

    public int copyProjectTable(Map<String, Object> param);

    public int updateScheduleProgrsCd(Map<String, Object> param);
    
    public int createProjectTable(Map<String, Object> param);

    public int createProjectTableVarchar(Map<String, Object> param);

    public int createProjectTableChar(Map<String, Object> param);
    
    public int updateClearClmnTcnData(Map<String, Object> param);

    public int addProjectTableData(Map<String, Object> param);    
    
    public int getBoardListCount(Map<String, Object> param);

    public int getBoardListTitleSearchCount(Map<String, Object> param);

    public int getBoardListContentsSearchCount(Map<String, Object> param);

    public int getBoardListWriterSearchCount(Map<String, Object> param);

    public int getBoardListSearchAllCount(Map<String, Object> param);    
    
    public int checkTableColumn(Map<String, Object> param);

    public int checkCreatTable(Map<String, Object> param);  
    
    public int getRoleCount();

    public int recordConnectionLogout(Map<String, Object> param);       
    
    public int insertDstrcDataMnldg(Map<String, Object> param);

    public int dataDestroy(Map<String, Object> param);

    public int chkTableCnt(Map<String, Object> param);

    public int updateProjectDltYn(Map<String, Object> param);
    
    public int updateGarantCsvFile(String path);

    public int checkProjectTable(String tableName);
    
    public int updateJoinDusYn(Map<String, Object> param);    
    
    public int insertJoinProjectData(Map<String, Object> param);
    
    public int insertBindProjectKey(Map<String, Object> param);
    
    public int insertBindProjectData(Map<String, Object> param);
    
    public int insertProcErrorLog(Map<String,Object> param);
    
    public int updateAditAliasInfoRvwSn(Map<String, Object> param);
        
    public int updateMappingCategoryRvwSn(Map<String, Object> param);    
    
    public int updateJoinProjectColumnRvwSn(Map<String, Object> param);
    
    public int updateJoinAditAliasInfoRvwSn(Map<String, Object> param);
    
    public int deleteProjectOverlapKey(Map<String, Object> param);
    
    public int updateDeleteKeyInfo(Map<String, Object> param);
    
    public int checkMenuAuthInfo(Map<String, Object> param);    
    
    public int checkRiskCheckData(Map<String, Object> param);
    
    public int insertAutoSpecificCheck(Map<String, Object> param);
    
    public int insertAutoKvalReIdentifiCheck(Map<String, Object> param);
    
    public int insertAutoLvalReIdentifiCheck(Map<String, Object> param);
    
    public int updateAutoClmnBindSn(Map<String, Object> param);
    
    public int insertAutoJoinKey(Map<String, Object> param);  
    

    public String getCommentCount(Map<String, Object> param);

    public String getCdNm(Map<String, Object> param);    
    
    public String getUsrId(Map<String, Object> param);
    
    public String selectProjectOverlapKey(Map<String, Object> param);
    
    public String selectCodeNm(Map<String, Object> param);
    
    public String getNewFileId();   

    public String getFIleNm(Map<String, Object> param); 
    
    
    public Object selectProjectNowRvwSn(Map<String, Object> param);

    public Object getUsrNo(Object USR_ID);
    
    public Object selectBindKey(Map<String, Object> param);
    
    public Object selectJoinTableRowCnt(Map<String, Object> param);
    
    public Object selectJoinProjectNowRvwSn(Map<String, Object> param);
    
    public Object selectBindClmNm(Map<String, Object> param);
    
    public Object selectKeyMainTable(Map<String, Object> param);
    
    public Object selectDataMainTable(Map<String, Object> param);    
    
    public Object selectTmpBindCount(Map<String, Object> param);
    
    public Object selectNoiseCount(Map<String, Object> param);
    
    public Object selectBeforeBindCount(Map<String, Object> param);
    
    public Object selectBeforeKeyCount(Map<String, Object> param);
    
    public Object selectTableRowCnt(Map<String, Object> param);
  
    public Object selectProjectStep(Map<String, Object> param);
  
    public Object confirmTableNm(Object tblNm);
    
    public Object selectNoisePercentCount(Map<String, Object> param);
    
    public Object selectSamplePercentCount(Map<String, Object> param);
    
    
    public Map<String,Object> getCodeInfo(Map<String,Object> param);

    public Map<String, String> getFileInfo(Map<String, Object> param);

    public Map<String, Object> checkColumnLenType(Map<String, Object> param);
    
    public Map<String, Object> selectJoinProjectTable(Map<String, Object> param);
    
    public Map<String, String> selectCSVFileInfo(Map<String, Object> param);

    public Map<String, String> selectKeyCSVFileInfo(Map<String, Object> param);
    
    public Map<String, String> selectJoinCSVFileInfo(Map<String, Object> param);
    
    public Map<String, Object> selectStatBase(Map<String, Object> param);
    
    public Map<String, Object> getBoardAll(Map<String, Object> param);
    
    public Map<String, Object> selectpTotalCheckData(Map<String, Object> param);
    
    public Map<String, Object> selectJTotalCheckData(Map<String, Object> param);
    
    public Map<String, Object> selectpMaxTotalCheckData();

    public Map<String, Object> selectJoinKeyInfo(Map<String, Object> param);
    
    public Map<String, Object> selectScheduleAplInfo(Map<String, Object> param);
    
    public Map<String, Object> getUserMenuRole(Map<String, Object> param);    
    
    public Map<String, Object> selectGroupNm(Map<String, Object> param);

    public Map<String, Object> selectBeforeJoinScheduleProcessing(Map<String, Object> param);
    
    public Map<String, Object> selectBeforeScheduleProcessing(Map<String, Object> param);

    public Map<String, Object> getProjectDetail(Object proNo);
    
    public Map<String, Object> selectJoinProjectDetail(Map<String, Object> param);
    
    public Map<String, Object> getJoinDetail(Object joinNo);
    
    public Map<String, Object> getProjectCSVFileData(Object proNo);

    public Map<String, Object> getJoinCSVFileData(Object joinNo);   
    
    public Map<String, Object> selectJoinStatBase(Map<String, Object> param);
    
    public Map<String, Object> selectLatelyScheduleProcessing(Map<String, Object> param);
    
    public Map<String, Object> selectCarryoutInfo(Map<String, Object> param);
    
    public Map<String, Object> selectRiskMeasureTotalPoint(Map<String, Object> param);
    
    public Map<String, Object> selectBindCSVFileData(Map<String, Object> param);
    
    public Map<String, Object> selectDataInfo(Map<String, Object> param);
    
    public Map<String, Object> selectBindKeyFileNo(Map<String, Object> param);
    
    public Map<String, String> selectDataCSVFileInfo(Map<String, Object> param);
    
    public Map<String, Object> selectMainKeyInfo(Map<String, Object> param);

    
    public List<Map<String, Object>> selectCodeList(Map<String, Object> param);

    public List<Map<String, Object>> getFileInfos(Object fileId);

    public List<Map<String, String>> selectMsgList();

    public List<Map<String, Object>> selectProjectList(Map<String, Object> param);

    public List<Map<String, Object>> selectJoinProjectList(Map<String, Object> param);

    public List<Map<String, Object>> selectScheduleList();

    public List<Map<String, Object>> selectNowScheduleList();

    public List<Map<String, Object>> selectScheduleProjectColumnList(Map<String, Object> param);    

    public List<Map<String, String>> selectProjectTable(Map<String, Object> param);
    
    public List<Map<String, String>> selectCmbProjectTable(Map<String, Object> param);
    
    public List<Map<String, Object>> selectProjectColumns(Map<String, Object> param);

    public List<Map<String, Object>> selectCmbProjectColumns(Map<String, Object> param);    

    public List<Map<String, Object>> selectJoinProjectColumns(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinColumns(Map<String, Object> param);
    
    public List<Map<String, Object>> selectProjectColumnTcns(Map<String, Object> param);

    public List<Map<String, Object>> selectProjectRowDatas(Map<String, Object> param);    

    public List<Map<String, Object>> selectJoinProjectRowDatas(Map<String, Object> param);

    public List<Map<String, Object>> selectProjectOrgRowDatas(Map<String, Object> param);
    
    public List<String> selectNumberRwmovVal(Map<String, Object> param);    
    
    public List<Map<String, Object>> getBoardList(Map<String, Object> param);

    public List<Map<String, Object>> getBoardListTitleSearch(Map<String, Object> param);

    public List<Map<String, Object>> getBoardListContentsSearch(Map<String, Object> param);

    public List<Map<String, Object>> getBoardListWriterSearch(Map<String, Object> param);

    public List<Map<String, Object>> getBoardListSearchAll(Map<String, Object> param);
    
    public List<Map<String, Object>> getMenuList(Map<String, Object> param);

    public List<Map<String, Object>> getUserRole(Map<String, Object> param);

    public List<Map<String, Object>> getMenuRole(Map<String, Object> param);
    
    public List<Map<String, Object>> getCommentList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectNormBase(Map<String, Object> param);

    public List<Map<String, Object>> selectKanoBase(Map<String, Object> param);

    public List<Map<String, Object>> selectFreqBase(Map<String, Object> param);

    public List<Map<String, Object>> selectCorrBase(Map<String, Object> param);

    public List<Map<String, Object>> selectGrtpBase(Map<String, Object> param);

    public List<Map<String, Object>> getColumnValueList(Map<String, Object> param);

    public List<Map<String, Object>> selectGrbmBase(Map<String, Object> param);
    
    public List<String> getGrbmCols(Map<String, Object> param);

    public List<Map<String, Object>> selectKltvBase(Map<String, Object> param);
    
    public List<Map<String, Object>> selectSigmaBase(Map<String, Object> param);

    public List<Map<String, Object>> selectUserScheduleList(Map<String, Object> param);  
    
    public List<Map<String, Object>> getLoginInfo(Map<String, Object> param);

    public List<Map<String, Object>> checkUserBlackIpList(Map<String, Object> param);

    public List<Map<String, Object>> checkUserWhiteIpList(Map<String, Object> param);
    
    public List<Map<String, Object>> getUserMenuList(Map<String, Object> param);

    public List<Map<String, Object>> selectExaminationList(Map<String, Object> param);

    public List<Map<String, Object>> getGroupList();
    
    public List<Map<String, Object>> getPrebindKeyInfoList(Map<String, Object> param);
    
    public List<Map<String, Object>> getPrebindDataInfoList(Map<String, Object> param);
    
    public List<Map<String, Object>> getJoinProjectList(Object joinNo);    
    
    public List<Map<String, Object>> selectDstrcDataMnldg(Map<String, Object> param);    
    
    public List<Map<String, Object>> selectRadarData(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinOrgRadarData(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinRadarData(Map<String, Object> param);    
    
    public List<Map<String,Object>> selectEncryptionList(Map<String,Object> param);
    
    public List<Map<String,Object>> selectCreatJoinProList(Map<String,Object> param);
    
    public List<Map<String,Object>> selectCreatJoinProClmnList(Map<String,Object> param);
    
    public List<Map<String,Object>> selectCreatBindProList(Map<String,Object> param);   
    
    public List<Map<String,Object>> selectCreatBindProClmnList(Map<String,Object> param);
    
    public List<Map<String, Object>> selectMappingList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectOutlierBase(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinOutlierBase(Map<String, Object> param);
    
    public List<Map<String, Object>> selectErrLog(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinNormBase(Map<String, Object> param);

    public List<Map<String, Object>> selectJoinKanoBase(Map<String, Object> param);
                                          
    public List<Map<String, Object>> selectJoinFreqBase(Map<String, Object> param);
                                           
    public List<Map<String, Object>> selectJoinCorrBase(Map<String, Object> param);
                                           
    public List<Map<String, Object>> selectJoinGrtpBase(Map<String, Object> param);
                                           
    public List<Map<String, Object>> selectJoinGrbmBase(Map<String, Object> param);
                                           
    public List<Map<String, Object>> selectJoinKltvBase(Map<String, Object> param);
                                           
    public List<Map<String, Object>> selectJoinSigmaBase(Map<String, Object> param);
    
    public List<Map<String, Object>> getJoinColumnValueList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectDestructionCheckList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinDestructionCheckList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectProjectIndlColumns(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinRiskMeasure();
    
    public List<Map<String, Object>> selectReportGrtpBase(Map<String, Object> param);
    
    public List<Map<String, Object>> selectJoinReportGrtpBase(Map<String, Object> param);
    
    public List<Map<String, Object>> selectBindColumnList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectIndvdlinfoMappingList();
    
    public List<Map<String, Object>> selectOrgTblDataList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectDstrcTblDataList(Map<String, Object> param);
}
