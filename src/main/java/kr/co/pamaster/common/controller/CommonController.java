package kr.co.pamaster.common.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.nhncorp.lucy.security.xss.XssFilter;

import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.user.service.UserService;

@Controller
public class CommonController {

    @Autowired
    CommonService service;
    
    @Autowired
    UserService userService;
    
    @Value("${pams.board.comment.level}")
    private int commentLevel; 
    
    /**
     * 파일 정보 조회
     * 
     * @param request
     * @return
     */
    @PostMapping("/common/getFileInfos")
    private @ResponseBody Map<String, Object> getFileInfos(@RequestBody Map<String, Object> param) {
        
        Map<String, Object> returnMap = new HashMap<String, Object>();
        
        List<Map<String, Object>> fileInfoList = service.getFileInfos((String)param.get("FILE_ID"));
        
        returnMap.put("FILE_INFOS", fileInfoList);
        
        return returnMap;
    }
    
    /**
     * 파일 다운로드
     * @param param
     * @return
     * @throws IOException 
     */
    @GetMapping("/common/getFile")
    @ResponseBody
    private ResponseEntity<InputStreamResource> getFile(HttpServletRequest request) {
        
        Map<String, Object> requestMap = new HashMap<String, Object>();
        
        requestMap.put("FL_ID", request.getParameter("FL_ID"));
        requestMap.put("FL_SN", request.getParameter("FL_SN"));

        return service.getFile(requestMap);
    }
    
    /**
     * 프로젝트 상태 코드 조회
     * @param param
     * @return
     * @throws IOException 
     */
    @PostMapping("/common/getCodeList")
    private @ResponseBody Map<String, Object> getCodeList(@RequestBody Map<String, Object> requestMap) {
        
        Map<String, Object> returnMap = new HashMap<String, Object>();
        
        returnMap.put("CODE_LIST", service.getCodeList(requestMap));
        
        return returnMap;
    }
    
    /**
     * 프로젝트 리스트 조회
     * @param param
     * @return
     * @throws IOException 
     */
    @PostMapping("/common/getProjectList")
    private @ResponseBody Map<String, Object> getProjectList(@RequestBody Map<String, Object> requestMap) {
        
        Map<String, Object> returnMap = new HashMap<String, Object>();
        
        returnMap.put("PROJECT_LIST", service.getProjectList(requestMap));
        
        HttpSession session = service.getLoginSession();
        
        if(session != null) {
            returnMap.put("BEFORE_SELECT_PROJECT", session.getAttribute("SELECT_PRO_NO"));
        }
        
        return returnMap;
    }
    
    /**
     * 프로젝트 세션저장
     * @param session
     * @param requestMap
     * @return
     */
    @PostMapping("/common/saveSelectProject")
    private @ResponseBody Map<String, Object> saveSelectProject(HttpSession session,@RequestBody Map<String, Object> requestMap) {
        //HttpSession session = service.getLoginSession();
        
        if(session != null && requestMap.get("PRO_NO") != null) {
            session.setAttribute("SELECT_PRO_NO", requestMap.get("PRO_NO"));
            session.setAttribute("SEL_PRO_TYP", "P");
        }
        
        return null;
    }

    /**
     * 결합 프로젝트 리스트 조회
     * @param param
     * @return
     * @throws IOException 
     */
    @PostMapping("/common/getJoinProjectList")
    private @ResponseBody Map<String, Object> getJoinProjectList(@RequestBody Map<String, Object> requestMap) {
        
        Map<String, Object> returnMap = new HashMap<String, Object>();
        
        returnMap.put("JOIN_PROJECT_LIST", service.getJoinProjectList(requestMap));
        
        HttpSession session = service.getLoginSession();
        
        if(session != null) {
            returnMap.put("BEFORE_SELECT_JOIN_PROJECT", session.getAttribute("SELECT_JOIN_PRO_NO"));
        }
        
        return returnMap;
    }
    
    /**
     * 결합 프로젝트 세션저장
     * @param requestMap
     * @return
     */
    @PostMapping("/common/saveSelectJoinProject")
    private @ResponseBody Map<String, Object> saveSelectJoinProject(@RequestBody Map<String, Object> requestMap) {
        HttpSession session = service.getLoginSession();
        
        if(session != null && requestMap.get("PRO_NO") != null) {
            session.setAttribute("SELECT_JOIN_PRO_NO", requestMap.get("PRO_NO"));
            session.setAttribute("SEL_PRO_TYP", "J");
        }
        
        return null;
    }
    
    /**
     * 게시판 리스트 조회
     * 
     * @param model
     * @return
     */
    @GetMapping("/common/sampleBoardList")
    private String listBoard(Model model) {

        return "common/sampleBoardList";
    }

  
    
    @PostMapping("/common/getUserName")
    private @ResponseBody Map<String, Object> getUserName(@RequestBody Map<String, Object> param) {
        String userNm = userService.getUserNm(service.getLoginUserNo());
        
        Map<String, Object> returnMap = new HashMap<>();

        returnMap.put("USR_NM", userNm);
        
        return returnMap;
    }
    


    
    
    /**
     * 공통 프레임 호출
     * 
     * Conformity : 기본정보
     * 
     * @param param
     * @return
     * @throws Exception
     */
    @GetMapping("/common/frame{FRAME_ID}")
    private ModelAndView frameLoad(@PathVariable("FRAME_ID") String frameId, @RequestParam Map<String, Object> param, Model model) throws Exception {
        ModelAndView mav = new ModelAndView();
        
        //XSS 필터 적용
        XssFilter filter = XssFilter.getInstance("lucy-xss-sax.xml");
        for(String paramCode : param.keySet()) {
            Object returnObj = filter.doFilter(param.get(paramCode).toString());
            param.replace(paramCode, returnObj);
        }
        
        if("AplDataInfo".equals(frameId)) {
        	model.addAttribute("ENCODING_TY", service.getCodeList("ENCODING_TY"));
    		model.addAttribute("FILE_ENCLOSED", service.getCodeList("FILE_ENCLOSED"));
    		model.addAttribute("FILE_TERMINATED", service.getCodeList("FILE_TERMINATED"));
    		model.addAttribute("FILE_HEADER", service.getCodeList("FILE_HEADER"));
    		model.addAttribute("MAPPING_LIST", service.getMappingList());
    		model.addAttribute("INFO_CLASSIFICATION_LIST", service.getCodeList("INFO_CLASSIFICATION"));
    		model.addAttribute("PASS_ROW_SIZE", service.getFileSettingInfo("PASS_ROW_SIZE"));
    		model.addAttribute("ALIAS_FILE_SIZE", service.getFileSettingInfo("ALIAS_FILE_SIZE"));
    		model.addAttribute("ALIAS_ROW_SIZE", service.getFileSettingInfo("ALIAS_ROW_SIZE"));
        }
        mav.setViewName("common/frame" + frameId);
        mav.addAllObjects(param);
        
        return mav;
    }
    
    /**
     * 공통 팝업 호출(퍼블임시)
     * 
     * BindKey   : 결합키
     * NT1401    : 데이터 범주화
     * TcnSelect : 가명처리 기술 선택
     * 
     * @param param
     * @return
     * @throws Exception
     */
    @GetMapping("/common/popup{POPUP_ID}")
    private ModelAndView paPopupNTLoad(@PathVariable("POPUP_ID") String popupId, @RequestParam Map<String, Object> param) throws Exception {
        ModelAndView mav = new ModelAndView();
        
        //XSS 필터 적용
        XssFilter filter = XssFilter.getInstance("lucy-xss-sax.xml");
        for(String paramCode : param.keySet()) {
            Object returnObj = filter.doFilter(param.get(paramCode).toString());
            param.replace(paramCode, returnObj);
        }
        
        mav.setViewName("common/popup" + popupId);
        mav.addAllObjects(param);
        
        return mav;
    }
    
    @PostMapping("/common/getMenuList")
    private @ResponseBody Map<String, Object> getMenuList(@RequestBody Map<String, Object> param) {
        Map<String, Object> returnMap = new HashMap<>();
        
//      returnMap.put("MENU_LIST", service.getMenuList(param));
        
        return returnMap;
    }
    
    @PostMapping("/common/getUserMenuList")
    private @ResponseBody List<Map<String,Object>> getUserMenuList(HttpServletRequest request) {
        Map<String,Object> param = new HashMap<String,Object>();
        /* 시스템 관리자 사용자 정보*/
        HttpSession session = request.getSession();
        param.put("IS_SYS_ADMIN", (String) session.getAttribute("IS_SYS_ADMIN"));
        param.put("USR_NO", session.getAttribute("USR_NO"));
        return service.getUserMenuList(param);
    }
    
    @GetMapping("/common/siteMapRenew")
    private String siteMapRenew(HttpServletRequest request){
        return "/common/siteMapRenew"; 
    }
    
    @PostMapping("/common/getGroupList")
    private @ResponseBody List<Map<String,Object>> getGroupList() {
        return service.getGroupList();
    }
    
    @PostMapping("/common/selectAttachFileInfo")
    private @ResponseBody Map<String,Object> selectAttachFileInfo(@RequestBody Map<String,Object> param,HttpSession session){
        
        return service.selectAttachFileInfo(param); 
    }
    
    /**
     * 파일 설정 정보 조회
     * 
     * @param param
     * @return
     */
    @PostMapping("/common/getFileSettingList")
    private @ResponseBody Map<String, Object> getFileSettingList(@RequestBody Map<String, Object> param) {
        
        Map<String, Object> returnMap = new HashMap<String, Object>();
        
        returnMap.put("FILE_SETTING_LIST", service.getFileSettingList() );
        
        return returnMap;
    }

    /**
     * 반출 정보 저장
     * 
     * @param param
     * @return
     */
    @PostMapping("/common/postCarryoutInfo")
    private @ResponseBody Map<String, Object> insertCarryoutInfo(@RequestBody Map<String, Object> param) {
        
        return service.postCarryoutInfo(param);
    }
    
    /**
     * 보고서 가명처리 데이터 조회 - 유비리포트
     *
     */

    @GetMapping("/common/paPopupUbiReport")
    private ModelAndView paPopupUbiReport() {
        ModelAndView mav = new ModelAndView();
        mav.addObject("TCN_LIST", service.getCodeList("PSEUDONYM"));
        mav.setViewName("/common/paPopupUbiReport");
        return mav;
    }
    
}
