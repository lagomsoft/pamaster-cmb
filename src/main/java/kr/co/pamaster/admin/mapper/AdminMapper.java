package kr.co.pamaster.admin.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdminMapper {
	
	public List<Map<String, Object>> selectUserList(Map<String, Object> param);
	
	public List<Map<String, Object>> selectSearchUserList(Map<String, Object> param);
	
	public void userDelete(Map<String, Object> param);
	
	public int getUsersListCount(Map<String, Object> param);
	
	public int getSearchUsersListCount(Map<String, Object> param);

	public List<Map<String, Object>> selectUserRole(Map<String, Object> param);
	
	public List<Map<String, Object>> selectUserIP(Map<String, Object> param);
	
	public List<Map<String, Object>> selectGroup();
	
	public List<Map<String, Object>> selectRole();
	
	public List<String> checkUserID(Map<String, Object> param);
	
	public List<String> checkUserEmail(Map<String, Object> param);
	
	public int insertUser(Map<String, Object> param);
	
	public int updateUser(Map<String, Object> param);

	public int deleteUserRole(Map<String, Object> param);
	
	public int insertUserRole(Map<String, Object> param);
	
	public int deleteUserIP(Map<String, Object> param);
	
	public int insertUserIP(Map<String, Object> param);
	
	public List<Map<String, Object>> selectCodeGroupList(Map<String, Object> param);

	public List<String> checkCodeGroup(Map<String, Object> param);
	
	public int insertCodeGroup(Map<String, Object> param);
	
	public int updateCodeGroup(Map<String, Object> param);
	
	public List<Map<String, Object>> selectCodeList(Map<String, Object> param);
	
	public List<Map<String, Object>> selectAllCodeList(Map<String, Object> param);

	public List<String> checkCode(Map<String, Object> param);
	
	public int insertCode(Map<String, Object> param);
	
	public int updateCode(Map<String, Object> param);
	
	public List<String> checkMappingCode(Map<String, Object> param);
	
	public List<Map<String, Object>> selectMappingCode(Map<String, Object> param);
	
	public List<Map<String, Object>> selectMappingTcnList(Map<String, Object> param);
	
	public int insertMapping(Map<String, Object> param);
	
	public int updateMapping(Map<String, Object> param);
	
	public int insertMappingTcn(Map<String, Object> param);
	
	public int updateMappingTcn(Map<String, Object> param);
	
	public int updateMappingTcnKey(Map<String, Object> param);
	
	public int deleteSynonm(Map<String, Object> param);
	
	public int insertSynonm(Map<String, Object> param);
	
	public int deleteRemov(Map<String, Object> param);
	
	public int insertRemov(Map<String, Object> param);
	
	public List<Map<String, Object>> selectCtgrz(Map<String, Object> param);
	
	public int insertCtgrz(Map<String, Object> param);
	
	public int updateCtgrz(Map<String, Object> param);
	
	public List<Map<String, Object>> selectRiskList(Map<String, Object> param);
	
	public List<String> checkRiskCode(Map<String, Object> param);
	
	public int insertRisk(Map<String, Object> param);
	
	public int deleteRisk(Map<String, Object> param);
	
	public int updateRisk(Map<String, Object> param);
	
	public List<Map<String, Object>> selectEvaluationList(Map<String, Object> param);
	
	public List<String> checkEvaluationCode(Map<String, Object> param);
	
	public int insertEvaluation(Map<String, Object> param);
	
	public int deleteEvaluation(Map<String, Object> param);
	
	public int updateEvaluation(Map<String, Object> param);
	
	public List<Map<String, Object>> selectMessageList(Map<String, Object> param);
	
	public List<Map<String, Object>> searchMessageList(Map<String, Object> param);
	
	public List<String> checkMessageCode(Map<String, Object> param);
	
	public int insertMessage(Map<String, Object> param);
	
	public int deleteMessage(Map<String, Object> param);
	
	public List<Map<String, Object>> selectProjectScheduleData(Map<String, Object> param);
	
	public int getProjectScheduleDataCount(Map<String, Object> param);
	
	public List<Map<String, Object>> selectMenuID();
	
	public List<Map<String, Object>> getRoleList(Map<String, Object> param);
	
	public List<Map<String, Object>> getLoginLogSearchAll(Map<String, Object> param);
	
	public List<Map<String, Object>> getLoginLogSearch(Map<String, Object> param);
	
	public List<Map<String, Object>> getLoginLogList(Map<String, Object> param);
	
	public List<Map<String, Object>> getConnectionLogSearchAll(Map<String, Object> param);
	
	public List<Map<String, Object>> getConnectionLogSearch(Map<String, Object> param);
	
	public List<Map<String, Object>> getConnectionLogList(Map<String, Object> param);
	
	public List<Map<String, Object>> getUserRoleAll(Map<String, Object> param);
	
	public List<Map<String, Object>> getUserRoleSearch(Map<String, Object> param);
	
	public List<Map<String, Object>> getUserRoleDepartmentSearch(Map<String, Object> param);
	
	public List<Map<String, Object>> getUserRoleSelect(Map<String, Object> param);
	
	public List<Map<String, Object>> getGroupInfo(Map<String, Object> param);
	
	public Map<String, Object> getGroupInfoByIDX(Map<String, Object> param);
	
	public List<Map<String, Object>> getUpperGroupList();
	
	public String getGroupName(Map<String, Object> param);
	
	public void deleteUserRoleList(Map<String, Object> param);
	
	public void saveUserRoleList(Map<String, Object> param);
	
	public void deleteMenuUser(Map<String, Object> param);
	
	public void saveRole(Map<String, Object> param);
	
	public void updateRole(Map<String, Object> param);
	
	public void deleteRole(Map<String, Object> param);
	
	public void saveMenuRole(Map<String, Object> param);
	
	public void deleteMenuRole(Map<String, Object> param);
	
	public String getMenuRoleCount(Map<String, Object> param);
	
	public int getLogCount(Map<String, Object> param);
	
	public int getLoginLogSearchAllCount(Map<String, Object> param);
	       
	public int getLoginLogSearchCount(Map<String, Object> param);
	       
	public int getLoginLogListCount(Map<String, Object> param);
	       
	public int getConnectionLogSearchAllCount(Map<String, Object> param);
	       
	public int getConnectionLogSearchCount(Map<String, Object> param);
	       
	public int getConnectionLogListCount(Map<String, Object> param);
	
	public int insertGroupInfo(Map<String,Object> param);
	
	public Map<String, Object> selectGroupInfo(Map<String, Object> param);
	
	/* 해당 메뉴 외 타 메뉴 업데이트*/
	public int updateGroup(Map<String,Object> param);
	
	public String getUserRoleCount();
	
	public String getUserRoleSearchCount(Map<String, Object> param);
	
	public String getUserRoleDepartmentSearchCount(Map<String, Object> param);
	
	public List<Map<String, Object>> getGroupList();
	
	public List<Map<String, Object>> getCorpGroupList(Map<String, Object> param);
	
	public void menuRoleReset();
	
	public void uploadGroup(Map<String, Object> param);
	
	public void truncateGroup();
	
	public void insertSaveGroup(Map<String, Object> param);
	
	public void updateGroupInfo(Map<String, Object> param);
	
	public void updateGroupInfoChild(Map<String, Object> param);
	
	public void deleteGroup(Map<String, Object> param);
	
	public String getGroupIdx(Map<String, Object> param);
	
	public String groupCheck(Map<String, Object> param);
	
	public List<Map<String,Object>> selectProcErrorLog(Map<String,Object> param);
	
	public int selectProcErrorLogCount(Map<String,Object> param);
	
	public void testProcedure(Map<String,Object> param);

	public List<Map<String, Object>> retrieveActionLog(Map<String, Object> params);

	public List<Map<String, Object>> retrieveAccessLogForExcel(Map<String, Object> params);

	public List<Map<String, Object>> retrieveProjectList(Map<String, Object> params);

	public int retrieveProjectListCount(Map<String, Object> params);

	public List<Map<String, Object>> retrieveLogListByProject(Map<String, Object> params);

	public int retrieveLogListByProjectCount(Map<String, Object> params);

	public List<Map<String, Object>> retrieveAccessLogByProjectForExcel(Map<String, Object> params);
	
	public List<Map<String, Object>> selectSystemCodeList(Map<String, Object> param);
	
	public void updateSystemCodeList(Map<String, Object> param);
}

