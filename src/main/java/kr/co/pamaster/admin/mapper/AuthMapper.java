/**
 * 
 */
package kr.co.pamaster.admin.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2021. 3. 24.
 * @author 이민구
 * @name kr.co.lagomsoft.mapper.AuthMapper
 * @comment 
 * 메뉴 및 권한 관련 
 */
@Mapper
public interface AuthMapper {
	
	/* 메뉴 목록 상위 목록들 획득*/
	public List<Map<String,Object>> getMenuNavList(String menuUrl);
	/* 메뉴 목록 획득 */
	public List<Map<String,Object>> getMenuList();
	/* 권한 페이지 메뉴 목록 획득*/
	public List<Map<String,Object>> getAuthMenuList();	
	/* 해당 사용자의 메뉴 권한 획득*/
	public List<Map<String,Object>> getMenuRoleList(String roleCd);	
	/* 메뉴 및 사용자의 메뉴 권한 단순 등록/삭제/수정*/
	public int insertMenuInfo(Map<String,Object> param);
	/* 메뉴 아이디 존재 유무 체크*/
	public List<Map<String,Object>> getMenuId(Map<String,Object> param);
	/* 해당 메뉴 삭제*/
	public int deleteMenuInfo(int seq);
	/* 해당 메뉴 및 자식 삭제*/
	public int deleteMenuList(Map<String,Object> param);
	/* 해당 메뉴 및 자식에 대한 권한 삭제*/
	public int deleteMenuRoleList(Map<String,Object> param);
	/* 메뉴 정보 업데이트*/
	public int updateMenuInfo(Map<String,Object> param);	
	/* 메뉴 자식 정보 업데이트*/
	public int updateMenuChildren(Map<String,Object> param);
	/* 해당 메뉴 및 자식 외 타 메뉴 업데이트*/
	public int updateOtherMenu(Map<String,Object> param);
	/* 해당 메뉴 외 타 메뉴 업데이트*/
	public int updateMenuSeq(Map<String,Object> param);
	/* 해당 권한 모든 메뉴권한 삭제*/
	public int deleteAllMenuRole(Map<String,Object> param);	
	/* 메뉴 권한 삽입*/
	public int insertMenuRole(Map<String,Object> param);
	/* 하휘 메뉴 권한 삽입*/
	public int insertMenuRoleChild(Map<String,Object> param);
	/* 메뉴 권한 삭제*/
	public int deleteMenuRole(Map<String,Object> param);
	/* 메뉴 권한 업데이트*/
	public int updateMenuRole(Map<String,Object> param);	
	/*해당 url에 대한 권한 획득*/
	public List<Map<String,Object>> getMenuAuthInfo(Map<String,Object> param);
	/*해당 권한에 대한 프로젝트 상태 권한 획득*/
	public List<Map<String,Object>> selectRoleProStep(Map<String,Object> param);
	/*해당 권한에 대한 프로젝트 상태 권한 삽입*/
	public int insertRoleProStep(Map<String,Object> param);
	/*해당 권한에 대한 프로젝트 상태 권한 삭제*/
	public int deleteRoleProStep(Map<String,Object> param);
	/*메뉴 순서를 재정렬한다.*/
	public int initMenuSeq(Map<String,Object> param);
}
