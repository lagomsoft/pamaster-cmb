package kr.co.pamaster.admin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pamaster.admin.mapper.AdminMapper;
import kr.co.pamaster.admin.service.AdminService;
import kr.co.pamaster.admin.service.AuthService;
import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.user.service.UserService;
import kr.co.pamaster.util.PAMsException;

@Controller
public class AdminController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AdminService service;

	@Autowired
	private CommonService commonService;
	
    @Autowired
    private AuthService authService;
    
	@Autowired
	private UserService userService;
	
	/**
	 * �봽濡쒖젥�듃 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/projectManagement")
	private String projectManagement(Model model) {
		return "admin/projectManagement";
	}

	/**
	 * �궗�슜�옄 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/userManagement")
	private ModelAndView userManagement(Model model) {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("admin/userManagement");

		mav.addObject("GROUP_LIST", service.getGroup());
		mav.addObject("ROLE_LIST", service.getRole());
		mav.addObject("RSPOFC_LIST", commonService.getCodeList("RSPOFC_CD"));
		mav.addObject("DOMAIN_LIST", commonService.getCodeList("DOMAIN"));
		mav.addObject("CNT_FRS_LIST", commonService.getCodeList("CNT_FRS"));

		return mav;
	}

	/**
	 * �궗�슜�옄 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getUserList")
	private @ResponseBody Map<String, Object> getUserList(@RequestBody Map<String, Object> param) throws Exception {

		List<Map<String, Object>> userList = service.getUsers(param);
		HashMap<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put("COUNT", service.getUsersListCount(param));

		returnMap.put("USER_LIST", userList);

		return returnMap;
	}
	
	/**
	 * �궗�슜�옄 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getSearchUserList")
	private @ResponseBody Map<String, Object> getSearchUserList(@RequestBody Map<String, Object> param) throws Exception {

		List<Map<String, Object>> userList = service.getSearchUsers(param);
		HashMap<String, Object> returnMap = new HashMap<String, Object>();
		returnMap.put("COUNT", service.getSearchUsersListCount(param));
		returnMap.put("USER_LIST", userList);

		return returnMap;
	}

	/**
	 * �뿊�� �쑀�� �젙蹂� json�쑝濡� 留뚮뱾�뼱 由ы꽩�떆�궎湲�
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/admin/uploadUserFile")
	private @ResponseBody Map<String, Object> uploadUserFile(MultipartHttpServletRequest request) {

		String[] titles = { "USR_ID", "USR_NM", "GP_CD", "ROLE_CD", "CNT_FRS", "CNT_MDL", "CNT_END" };

		Map<String, Object> returnMap = new HashMap<String, Object>();

		List<MultipartFile> fileList = request.getFiles("files");
		if (fileList == null || fileList.size() == 0) {

			returnMap.put("RETURN_TYPE", "EMPTY");
			return returnMap;
		}

		try {

			List<Map<String, Object>> excelDataList = commonService.getExcelData(fileList.get(0), titles, 3, 1);
			List<Map<String, Object>> returnList = new ArrayList<Map<String, Object>>();

			for (Map<String, Object> excelData : excelDataList) {

				String[] roleList = {};

				if (excelData.get("ROLE_CD") != null) {
					roleList = excelData.get("ROLE_CD").toString().split(",");
				}

				excelData.put("ROLE_LIST", roleList);

				returnList.add(excelData);
			}

			returnMap.put("USR_LIST", returnList);
			returnMap.put("RETURN_TYPE", "SUCCESS");

		} catch (IOException e) {

			log.error(" " + e.getCause());
			returnMap.put("RETURN_TYPE", "ERR");
		}

		return returnMap;
	}

	/**
	 * �븘�씠�뵒 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkUserID")
	private @ResponseBody Map<String, Object> checkUserID(@RequestBody Map<String, Object> param) {
		
		String checkUser = service.checkUserID((Map<String, Object>) param.get("USER_DATA"));

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_USER", checkUser);

		return returnMap;
	}

	/**
	 * �궗�슜�옄 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveUser")
	private @ResponseBody Map<String, Object> saveUser(@RequestBody Map<String, Object> param) {
		int returnInfo = 1;
		if (param.get("USER_DATA") != null) {
			returnInfo = service.saveUser((Map<String, Object>) param.get("USER_DATA"));
		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", returnInfo);

		return returnMap;
	}

	/**
	 * �궗�슜�옄 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveUserIP")
	private @ResponseBody Map<String, Object> saveUserIP(@RequestBody Map<String, Object> param) {

		int returnInfo = service.saveUserIP(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", returnInfo);

		return returnMap;
	}

	/**
	 * �궗�슜�옄 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveUserRole")
	private @ResponseBody Map<String, Object> saveUserRole(@RequestBody Map<String, Object> param) {

		int returnInfo = service.saveUserRole(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", returnInfo);

		return returnMap;
	}

	/**
	 * 肄붾뱶愿�由� �솕硫� �샇異�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/codeManagement")
	private String codeManagement(Model model) {
		return "admin/codeManagement";
	}

	/**
	 * 肄붾뱶 洹몃９ 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getCodeGroupList")
	private @ResponseBody Map<String, Object> getCodeGroupList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> codeGroupList = service.getCodeGroupList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CODE_GROUP_LIST", codeGroupList);

		return returnMap;
	}

	/**
	 * �뿊�� 肄붾뱶 �젙蹂� json�쑝濡� 留뚮뱾�뼱 由ы꽩�떆�궎湲�
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/admin/uploadCodeFile")
	private @ResponseBody Map<String, Object> uploadCodeFile(MultipartHttpServletRequest request) {

		String[] titles = { "CD", "CD_NM", "OUTPT_SN", "CD_DC", "META_1", "META_2", "META_3", "META_4", "META_5",
				"META_6", "META_7", "META_8", "META_9", "META_10" };

		Map<String, Object> returnMap = new HashMap<String, Object>();

		List<MultipartFile> fileList = request.getFiles("files");
		if (fileList == null || fileList.size() == 0) {

			returnMap.put("RETURN_TYPE", "EMPTY");
			return returnMap;
		}

		try {
			List<Map<String, Object>> returnList = commonService.getExcelData(fileList.get(0), titles, 3, 1);
			returnMap.put("CODE_LIST", returnList);
			returnMap.put("RETURN_TYPE", "SUCCESS");

		} catch (IOException e) {

			log.error(" " + e.getCause());
			returnMap.put("RETURN_TYPE", "ERR");
		}

		return returnMap;
	}

	/**
	 * 肄붾뱶 洹몃９ 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkCodeGroup")
	private @ResponseBody Map<String, Object> checkCodeGroup(@RequestBody Map<String, Object> param) {

		String checkGroup = service.checkCodeGroup(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_GROUP", checkGroup);

		return returnMap;
	}

	/**
	 * 肄붾뱶 洹몃９ ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveCodeGroup")
	private @ResponseBody Map<String, Object> saveCodeGroup(@RequestBody Map<String, Object> param) {

		if (param.get("DATA_LIST") != null) {

			List<Map<String, Object>> codeGroupList = (List<Map<String, Object>>) param.get("DATA_LIST");

			service.saveCodeGroup(codeGroupList);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 肄붾뱶 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getCodeList")
	private @ResponseBody Map<String, Object> getCodeList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> codeList = service.getAllCodeList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CODE_LIST", codeList);

		return returnMap;
	}

	/**
	 * 肄붾뱶 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkCode")
	private @ResponseBody Map<String, Object> checkCode(@RequestBody Map<String, Object> param) {

		String checkCD = service.checkCode(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_CD", checkCD);

		return returnMap;
	}

	/**
	 * 肄붾뱶 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveCode")
	private @ResponseBody Map<String, Object> saveCode(@RequestBody Map<String, Object> param) {

		if (param.get("GROUP_CD") != null && param.get("DATA_LIST") != null) {

			String groupCd = (String) param.get("GROUP_CD");

			List<Map<String, Object>> codeList = (List<Map<String, Object>>) param.get("DATA_LIST");

			service.saveCode(groupCd, codeList);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 留ㅽ븨愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/mappingManagement")
	private ModelAndView mappingManagement(Model model) {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("admin/mappingManagement");

		Map<String, Object> codeMap = new HashMap<String, Object>();

		codeMap.put("GRP_CD", "PSEUDONYM");
		codeMap.put("META_1", "0");

		Map<String, String> headerCodeMap = new HashMap<String, String>();

		List<Map<String, Object>> headerCodeList = commonService.getCodeList(codeMap);

		for (Map<String, Object> map : headerCodeList) {
			headerCodeMap.put(map.get("CD").toString(), map.get("CD_NM").toString());
		}

		mav.addObject("HEADER_TCN_MAP", headerCodeMap);

		codeMap.remove("META_1");
		codeMap.put("NOT_META_1", "0");

		List<Map<String, Object>> returnCodeList = new ArrayList<Map<String, Object>>();

		List<Map<String, Object>> codeList = commonService.getCodeList(codeMap);

		for (Map<String, Object> map : codeList) {

			String headerNm = headerCodeMap.get(map.get("META_1").toString());
			map.put("CD_NM", headerNm + "-" + map.get("CD_NM"));

			returnCodeList.add(map);
		}

		mav.addObject("TCN_LIST", returnCodeList);
		mav.addObject("SE_LIST", commonService.getCodeList("INFO_SEPARATION"));
		mav.addObject("CL_LIST", commonService.getCodeList("INFO_CLASSIFICATION"));

		return mav;
	}

	/**
	 * 留ㅽ븨 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getMappingList")
	private @ResponseBody Map<String, Object> getMappingList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> mappingList = service.getMappingList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("MAPPING_LIST", mappingList);

		return returnMap;
	}

	/**
	 * 留ㅽ븨 湲곗닠 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getMappingTcnList")
	private @ResponseBody Map<String, Object> getMappingTcnList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> tcnList = service.getMappingTcnList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("TCN_LIST", tcnList);

		return returnMap;
	}

	/**
	 * 留ㅽ븨 湲곗닠 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveMappingTcn")
	private @ResponseBody Map<String, Object> saveMappingTcn(@RequestBody Map<String, Object> param) {

		service.saveMappingTcn(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * �뿊�� 肄붾뱶 �젙蹂� json�쑝濡� 留뚮뱾�뼱 由ы꽩�떆�궎湲�
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/admin/uploadMappingFile")
	private @ResponseBody Map<String, Object> uploadMappingFile(MultipartHttpServletRequest request) {

		String[] titles = { "INDVDLINFO_CD", "INDVDLINFO_NM", "INFO_SE", "INFO_CL", "CLM_TY", "SMPLE", "SYNONMS",
				"REMOVS", "PRC_TCN_CD1", "PRC_TCN_DC1", "PRC_TCN_1_VAL1", "PRC_TCN_1_VAL2", "PRC_TCN_CD2",
				"PRC_TCN_DC2", "PRC_TCN_2_VAL1", "PRC_TCN_2_VAL2", "PRC_TCN_CD3", "PRC_TCN_DC3", "PRC_TCN_3_VAL1",
				"PRC_TCN_3_VAL2" };

		Map<String, Object> returnMap = new HashMap<String, Object>();

		List<MultipartFile> fileList = request.getFiles("files");
		if (fileList == null || fileList.size() == 0) {

			returnMap.put("RETURN_TYPE", "EMPTY");
			return returnMap;
		}

		try {
			List<Map<String, Object>> returnList = commonService.getExcelData(fileList.get(0), titles, 4, 1);
			returnMap.put("DATA_LIST", returnList);
			returnMap.put("RETURN_TYPE", "SUCCESS");

		} catch (IOException e) {

			log.error(" " + e.getCause());
			returnMap.put("RETURN_TYPE", "ERR");
		}

		return returnMap;
	}

	/**
	 * 留ㅽ븨肄붾뱶 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkMappingCode")
	private @ResponseBody Map<String, Object> checkMappingCode(@RequestBody Map<String, Object> param) {

		String checkCD = service.checkMappingCode(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_CD", checkCD);

		return returnMap;
	}

	/**
	 * 肄붾뱶 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveMapping")
	private @ResponseBody Map<String, Object> saveMapping(@RequestBody Map<String, Object> param) {

		if (param.get("DATA_LIST") != null) {

			List<Map<String, Object>> list = (List<Map<String, Object>>) param.get("DATA_LIST");

			service.saveMapping(list);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/riskManagement")
	private ModelAndView riskManagement(Model model) {

		ModelAndView mav = new ModelAndView();

		mav.setViewName("admin/riskManagement");

		return mav;
	}

	/**
	 * �쐞�뿕�룄 紐⑸줉 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveRiskList")
	private @ResponseBody Map<String, Object> saveRiskList(Model model, @RequestBody Map<String, Object> param,
			HttpServletRequest request) {

		if (param.get("DATA_LIST") != null) {

			param.put("REGISTER", commonService.getLoginUserNo());

			List<Map<String, Object>> riskList = (List<Map<String, Object>>) param.get("DATA_LIST");

			service.saveRiskList(riskList, param);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 紐⑸줉 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getRiskList")
	private @ResponseBody Map<String, Object> getRiskList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> riskList = service.getRiskList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("DATA_LIST", riskList);

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 肄붾뱶 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkRiskCode")
	private @ResponseBody Map<String, Object> checkRiskCode(@RequestBody Map<String, Object> param) {

		String checkCD = service.checkRiskCode(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_CD", checkCD);

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 �궘�젣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/deleteRisk")
	private @ResponseBody Map<String, Object> deleteRisk(@RequestBody Map<String, Object> param) {

		if (param.get("DATA_LIST") != null) {

			List<String> list = (List<String>) param.get("DATA_LIST");
			service.deleteRisk(list);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 �룊媛�吏��몴 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getEvaluationList")
	private @ResponseBody Map<String, Object> getEvaluationList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> evaluationList = service.getEvaluationList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("DATA_LIST", evaluationList);

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 �룊媛�吏��몴 紐⑸줉 ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveEvaluationList")
	private @ResponseBody Map<String, Object> saveEvaluationList(Model model, @RequestBody Map<String, Object> param,
			HttpServletRequest request) {

		if (param.get("DATA_LIST") != null) {

			param.put("REGISTER", commonService.getLoginUserNo());

			List<Map<String, Object>> evaluationList = (List<Map<String, Object>>) param.get("DATA_LIST");

			service.saveEvaluationList(evaluationList, param);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 �룊媛�吏��몴�쓽 �쐞�뿕�룄 肄붾뱶 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkEvaluationCode")
	private @ResponseBody Map<String, Object> checkEvaluationCode(@RequestBody Map<String, Object> param) {

		String checkCD = service.checkEvaluationCode(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_CD", checkCD);

		return returnMap;
	}

	/**
	 * �쐞�뿕�룄 �룊媛�吏��몴 �궘�젣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/deleteEvaluation")
	private @ResponseBody Map<String, Object> deleteEvaluation(@RequestBody Map<String, Object> param) {

		if (param.get("DATA_LIST") != null) {

			List<String> list = (List<String>) param.get("DATA_LIST");
			service.deleteEvaluation(list);
		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 硫붿꽭吏�愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/messageManagement")
	private ModelAndView messageManagement(Model model) {
		ModelAndView mav = new ModelAndView();

		mav.setViewName("admin/messageManagement");
		mav.addObject("MENU_LIST", service.selectMenuID());

		return mav;
	}

	/**
	 * 硫붿꽭吏� 議고쉶/寃��깋
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getMessageList")
	private @ResponseBody Map<String, Object> getMessageList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> msgList = service.getMessageList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("DATA_LIST", msgList);

		return returnMap;
	}
	
	/**硫붿떆吏� �뿊�� �떎�슫濡쒕뱶
     * @param model
     * @param param
     * @param session
     * @return
     * @throws PAMsException
     * @return String
     */
    @PostMapping("/admin/getMessageList.xls")
    private String  getMessageListExcelDown(Model model,@RequestParam Map<String,Object> param,HttpSession session) throws PAMsException{
        param.put("USR_NO",session.getAttribute("USR_NO"));
        if("ALL".equals(param.get("PRJCT_NO"))) {
            param.remove("PRJCT_NO");
        }
        List<Map<String,Object>> resultList = service.getMessageList(param);    
        List<Object> header = new JSONArray(param.get("header").toString()).toList();        
        
        model.addAttribute("body", resultList);
        model.addAttribute("header", header);
        model.addAttribute("fileName", param.get("fileName"));
        return "ExcelDownload";
    }

	/**
	 * �뿊�� 肄붾뱶 �젙蹂� json�쑝濡� 留뚮뱾�뼱 由ы꽩�떆�궎湲�
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/admin/uploadMessageFile")
	private @ResponseBody Map<String, Object> uploadMessageFile(MultipartHttpServletRequest request) {

		String[] titles = { "MENU_ID", "MSG_ID", "MSG_CN" };

		Map<String, Object> returnMap = new HashMap<String, Object>();

		List<MultipartFile> fileList = request.getFiles("files");
		if (fileList == null || fileList.size() == 0) {

			returnMap.put("RETURN_TYPE", "EMPTY");
			return returnMap;
		}

		try {
			List<Map<String, Object>> returnList = commonService.getExcelData(fileList.get(0), titles, 3, 1);
			returnMap.put("DATA_LIST", returnList);
			returnMap.put("RETURN_TYPE", "SUCCESS");

		} catch (IOException e) {

			log.error(" " + e.getCause());
			returnMap.put("RETURN_TYPE", "ERR");
		}

		return returnMap;
	}

	/**
	 * 硫붿꽭吏� 以묐났 泥댄겕
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/checkMessageCode")
	private @ResponseBody Map<String, Object> checkMessageCode(@RequestBody Map<String, Object> param) {

		String checkCD = service.checkMessageCode(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CHECK_CD", checkCD);

		return returnMap;
	}

	/**
	 * 硫붿꽭吏� �궘�젣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/deleteMessage")
	private @ResponseBody Map<String, Object> deleteMessage(@RequestBody Map<String, Object> param) {

		if (param.get("DATA_LIST") != null) {

			List<String> list = (List<String>) param.get("DATA_LIST");

			service.deleteMessage(list);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 硫붿꽭吏� ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveMessage")
	private @ResponseBody Map<String, Object> saveMessage(@RequestBody Map<String, Object> param) {

		if (param.get("DATA_LIST") != null) {

			List<Map<String, Object>> list = (List<Map<String, Object>>) param.get("DATA_LIST");

			service.saveMessage(list);

		}

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}
	
	/**
	 * 遺��꽌 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/departmentManagement")
	private String departmentManagement(Model model) {
		
		return "admin/departmentManagement";
	}

	/**
	 * �봽濡쒖젥�듃 �뒪耳�以� 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/projectSchedule")
	private ModelAndView projectSchedule(Model model) {
		
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("PROJECT_STEP", commonService.getCodeList("PROJECT_STEP"));
		
		mav.setViewName("admin/projectSchedule");
		return mav;
	}
	
	/**
	 * �뒪耳�以꾨벑濡�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/startProjectSchedule")
	private @ResponseBody Map<String, Object> startProjectSchedule(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> scheduleList = (List<Map<String, Object>>) param.get("SCHEDULE_LIST");
		
		for (Map<String, Object> scheduleMap : scheduleList) {
			
			if(scheduleMap.get("RSVTIME") == null || "".equals(scheduleMap.get("RSVTIME"))) {
				scheduleMap.put("RSVTIME", commonService.getDefTime());				
			}
			
			commonService.updateScheduleProgrsCd(scheduleMap);
		}
		
		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * �봽濡쒖젥�듃 �뒪耳�以� 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getProjectScheduleData")
	private @ResponseBody Map<String, Object> getProjectScheduleData(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> projectScheduleList = service.getProjectScheduleData(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("DATA_LIST", projectScheduleList);
		returnMap.put("COUNT", service.getProjectScheduleDataCount(param));

		return returnMap;
	}

	/**
	 * �씠�젰 愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/logManagement")
	private String logManagement(Model model) {
		model.addAttribute("HIST_SEARCH_TY"		, commonService.getCodeList("HIST_SEARCH_TY"));
		model.addAttribute("PROC_ERR_SEARCH_TY"	, commonService.getCodeList("PROC_ERR_SEARCH_TY"));
		model.addAttribute("PROC_ERR_TY"		, commonService.getCodeList("PROC_ERR_TY"));
		model.addAttribute("PROJECT_STEP"		, commonService.getCodeList("PROJECT_STEP"));

		return "admin/logManagement";
	}

	/**
	 * 濡쒓렇�씤�씠�젰
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/loginLog")
	private @ResponseBody Map<String, Object> loginLog(@RequestBody Map<String, Object> param) throws Exception {
		Object pageIndex = param.get("PAGE_INDEX");
		Object pageSize = param.get("PAGE_SIZE");

		List<Map<String, Object>> projectList = service.getLoginLog(param);

		param.put("PAGE_INDEX", pageIndex);
		param.put("PAGE_SIZE", pageSize);

		HashMap<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("LogList", projectList);
		returnMap.put("COUNT", service.getLoginLogCount(param));

		return returnMap;
	}
	
	/**
	 * �궗�슜�옄 �솢�룞 �궡�뿭
	 * @param param
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/admin/actionLog")
	private @ResponseBody Map<String, Object> actionLog(@RequestBody Map<String, Object> params) throws Exception {
		Map<String, Object> resultMap = new HashMap<>();
		List<Map<String, Object>> resultList = service.retrieveActionLog(params); 
		
		resultMap.put("accessLogs", resultList);
		
		return resultMap;
	}
	
	/**
	 * �궗�슜�옄 �솢�룞�씠�젰 Excel Export
	 * @param params
	 * @return
	 */
	@PostMapping(path="/admin/retrieveAccessLogForExcel", produces="application/vnd.ms-excel")
	private ModelAndView excelFileDownload(@RequestParam Map<String, Object> params) {
		
		Map<String, Object> dataBody = (Map<String, Object>) service.retrieveAccessLogForExcel(params);
		
		ModelAndView mav = new ModelAndView("ExcelDownload", dataBody); 
		return mav;
	}

	
	/**
	 * [�젒�냽�씠�젰愿�由� > �봽濡쒖젥�듃蹂� �젒�냽�씠�젰]
	 * �봽濡쒖젥�듃 紐⑸줉
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/admin/connectLogByProject")
	private @ResponseBody Map<String, Object> connectLogByProject(@RequestBody Map<String, Object> params) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<Map<String, Object>> projectList = service.retrieveProjectList(params);
		
		resultMap.put("LogList"	, projectList);
		resultMap.put("COUNT"	, service.retrieveProjectListCount(params));
		
		return resultMap;
	}
	
	/**
	 * [�젒�냽�씠�젰愿�由� > �봽濡쒖젥�듃蹂� �젒�냽�씠�젰]
	 * �봽濡쒖젥�듃 紐⑸줉 > �봽濡쒖젥�듃蹂� �젒�냽�씠�젰
	 * @param params
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/admin/logListByProject")
	public @ResponseBody Map<String, Object> logListByProject(@RequestBody Map<String, Object> params) throws Exception {
		Map<String, Object> resultMap = new HashMap<String, Object>();
		
		List<Map<String, Object>> logListByProject = service.retrieveLogListByProject(params);
		
		resultMap.put("projectLogs"	, logListByProject);
		resultMap.put("COUNT"		, service.retrieveLogListByProjectCount(params));
		
		return resultMap;
	}
	
	
	/**
	 * �봽濡쒖떆�� �뿉�윭湲곕줉
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping("/admin/procErrorLog")
	private @ResponseBody Map<String,Object> procErrLog(@RequestBody Map<String, Object> param) throws Exception {
		Object pageIndex = param.get("PAGE_INDEX");
		Object pageSize = param.get("PAGE_SIZE");

		List<Map<String, Object>> projectList = service.selectProcErrorLog(param);

		HashMap<String, Object> returnMap = new HashMap<String, Object>();

		param.put("PAGE_INDEX", pageIndex);
		param.put("PAGE_SIZE", pageSize);

		returnMap.put("LogList", projectList);
		returnMap.put("COUNT", service.selectProcErrorLogCount(param));

		return returnMap;
	}
	
	/**
	 * 沅뚰븳愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getRoleList")
	private @ResponseBody Map<String, Object> getRoleList(Model model, @RequestBody Map<String, Object> param) {
		List<Map<String, Object>> roleList = service.getRoleList(param);
		;
		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("ROLE_LIST", roleList);

		return returnMap;
	}
	
	/**
	 * 沅뚰븳愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveRole")
	private @ResponseBody Map<String, Object> saveRole(Model model, @RequestBody Map<String, Object> param) {

		param.put("USR_NO", commonService.getLoginUserNo());

		service.saveRole(param);

		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 沅뚰븳愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/deleteRole")
	private @ResponseBody Map<String, Object> deleteRole(Model model, @RequestBody Map<String, Object> param) {

		service.deleteRole(param);

		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 沅뚰븳愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveRoleMenu")
	private @ResponseBody Map<String, Object> saveRoleMenu(Model model, @RequestBody Map<String, Object> param) {
		service.saveRoleMenu(param);

		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 沅뚰븳愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getUserRole")
	private @ResponseBody Map<String, Object> getUserRole(Model model, @RequestBody Map<String, Object> param) {

		List<Map<String, Object>> userRoleSearchList = service.getUserRoleSearch(param);
		List<Map<String, Object>> userRoleSelectList = service.getUserRoleSelect(param);

		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("USER_ALL_LIST", userRoleSearchList);
		returnMap.put("USER_SELECT_LIST", userRoleSelectList);
		returnMap.put("USER_ROLE_COUNT", service.getUserRoleCount(param));

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}

	/**
	 * 沅뚰븳愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveUserRoleList")
	private @ResponseBody Map<String, Object> saveUserRoleList(Model model, @RequestBody Map<String, Object> param) {
		service.saveUserRoleList(param);

		Map<String, Object> returnMap = new HashMap<>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}
	
	/**
	 * �뿊�� 遺��꽌 �젙蹂� json�쑝濡� 留뚮뱾�뼱 由ы꽩�떆�궎湲�
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value = "/admin/uploadGroupFile")
	private @ResponseBody Map<String, Object> uploadGroupFile(MultipartHttpServletRequest request) {

		String[] titles = { "UPPER_GP_CD", "GP_CD", "GP_NM", "GP_DC", "USE_YN", "DEPTH" };

		Map<String, Object> returnMap = new HashMap<String, Object>();

		List<MultipartFile> fileList = request.getFiles("files");
		if (fileList == null || fileList.size() == 0) {

			returnMap.put("RETURN_TYPE", "EMPTY");
			return returnMap;
		}

		try {
			List<Map<String, Object>> groupList = commonService.getExcelData(fileList.get(0), titles, 3, 1);
			if (!service.uploadGroup(groupList)) {
				returnMap.put("RETURN_TYPE", "OVERLAB");
				return returnMap;
			}
			;

			returnMap.put("GROUP_LIST", groupList);
			returnMap.put("RETURN_TYPE", "SUCCESS");

		} catch (IOException e) {

			log.error(" " + e.getCause());
			returnMap.put("RETURN_TYPE", "ERR");
		}

		return returnMap;
	}
	
	/**
     * 遺��꽌 愿�由� (遺��꽌 �닔�젙)
     * 
     * @return
     */
    @PostMapping("/admin/getGroupCodeCheck")
    private @ResponseBody Map<String, Object> getGroupCodeCheck(@RequestBody Map<String, Object> param) {
        
        Map<String, Object> returnMap = new HashMap<>();
        
        String groupCheck = service.groupCheck(param);
        
        returnMap.put("GROUP_CHECK", groupCheck);  

        return returnMap;
    }
    
	/**
	 * 遺��꽌 愿�由� (遺��꽌 TREE 媛��졇�삤湲�)
	 * 
	 * @return
	 */
	@PostMapping("/admin/getGroupListInfo")
	private @ResponseBody List<Map<String,Object>> getGroupList() {

		return service.getGroupList();
	}

	/**
	 * 遺��꽌 愿�由� (遺��꽌 TREE 媛��졇�삤湲�)
	 * 
	 * @return
	 */
	@PostMapping("/admin/getGroupInfo")
	private @ResponseBody Map<String, Object> getGroupInfo(@RequestBody Map<String, Object> param) {
		List<Map<String, Object>> groupInfo = service.getGroupInfo(param);

		Map<String, Object> returnMap = new HashMap<>();

		if (groupInfo.size() != 0) {
			returnMap.put("GROUP_INFO", groupInfo.get(0));
		}
		return returnMap;
	}
	
	/** 硫붾돱 �벑濡� (ajax)
	 * @param paramMap
	 * @return
	 */
	@PostMapping("/admin/insertGroupInfo")
	public @ResponseBody Map<String,Object> insertGroup(@RequestBody HashMap<String,Object> paramMap,HttpServletRequest request){ 
		Map<String,Object> resultMap = new HashMap<String,Object>();
		paramMap.put("USR_NO",request.getSession().getAttribute("USR_NO"));
		
		/*�젙�긽�쟻�쑝濡� �뜲�씠��媛� �궫�엯�릱�쓣 寃쎌슦 硫붾돱 seq瑜� �젙�젹�떆�궓�떎.*/
		/*
		if(paramMap.get("seq") != null) {
			HashMap<String,Object> paramMap2 = new HashMap<String,Object>();
			paramMap2.put("SEQ",paramMap.get("seq"));
			paramMap2.put("START_SEQ",paramMap.get("MENU_SEQ"));
			paramMap2.put("PROCESS","SUM");
			int result = service.updateGroup(paramMap2);
			resultMap.put("result", result);
			resultMap.put("SEQ", paramMap.get("seq"));
		}
		*/
		resultMap = service.insertGroupInfo(paramMap);
		resultMap.put("result", 0);		
		return resultMap;
	}
	
	/**
	 * 遺��꽌 愿�由� (遺��꽌 �궘�젣)
	 * 
	 * @return
	 */
	@PostMapping("/admin/deleteGroupInfo")
    private @ResponseBody Map<String, Object> deleteGroup(@RequestBody Map<String, Object> param) {
		service.deleteGroup(param);
        
		Map<String, Object> returnMap = new HashMap<>();
		
		returnMap.put("SUCCESS", "1");
		
		return returnMap;
	}
	
	/**
	 * 遺��꽌 愿�由� (遺��꽌 �닔�젙)
	 * 
	 * @return
	 */
	@PostMapping("/admin/updateGroupInfo")
	private @ResponseBody Map<String, Object> saveGroup(@RequestBody Map<String, Object> param) {
		Map<String, Object> returnMap = new HashMap<>();
		
		Map<String, Object> originMap = service.getGroupInfoByIDX(param);
		service.updateGroupInfo(param);
		
		Map<String, Object> childMap = new HashMap<String, Object>();
		childMap.put("ORIGIN_GP_CD", originMap.get("GP_CD"));
		childMap.put("GP_CD", param.get("GP_CD"));
		
		service.updateGroupInfoChild(childMap);
		
		returnMap.put("SUCCESS", "1");

		return returnMap;
	}
	
	/**
	 * 遺��꽌 愿�由� (�긽�쐞遺��꽌 由ъ뒪�듃 媛��졇�삤湲�)
	 * 
	 * @return
	 */
	@PostMapping("/admin/getUpperGroupList")
	private @ResponseBody Map<String, Object> getUpperGroupList(@RequestBody Map<String, Object> param) {
		List<Map<String, Object>> upperGroupList = service.getUpperGroupList();
		
		Map<String, Object> returnMap = new HashMap<>();
		
		returnMap.put("UPPER_GROUP_LIST", upperGroupList);
		
		return returnMap;
	}
	
	/**
	 * �궗�슜�옄 鍮꾨�踰덊샇 珥덇린�솕
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/userPWReset")
	private @ResponseBody Map<String, Object> userPWReset(@RequestBody Map<String, Object> param) {
		param.put("USR_ID", userService.getUser(param.get("USR_NO").toString()));
		param.put("USR_PW", commonService.createTempPassword(param.get("USR_ID").toString()));
		param.put("PW_INITL_YN", "Y");
		
		
		userService.changePw(param);
		
		Map<String, Object> returnMap = new HashMap<>();
		returnMap.put("USR_ID", param.get("USR_ID"));
		returnMap.put("USR_PW", param.get("USR_PW"));
		log.info(returnMap+"agnds;kglnw;lekghwae");
		return returnMap;
	}
	
	/**
	 * �궗�슜�옄 �궘�젣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/userDelete")
	private @ResponseBody void userDelete(@RequestBody Map<String, Object> param) {
		param.put("USR_ID", userService.getUser(param.get("USR_NO").toString()));
		service.userDelete(param);
	}
	
	@GetMapping("/admin/serverInfo")
	private String serverInfo(){
		return "admin/serverInfo";
	}
	
	@PostMapping("/admin/storageInfo")
	private @ResponseBody List<Map<String, Object>> storageInfo(){
		return service.getStorageInfo();
	}
	
	@Autowired
	private AdminMapper mapper;
	
	@GetMapping("/admin/testProcedure")
	private @ResponseBody Map<String,Object> testProcedure() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("PRJCT_NO", "11");
		mapper.testProcedure(map);
		return map;
	}
	
	
	/**
	 * �떆�뒪�뀥愿�由�
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/admin/systemManagement")
	private ModelAndView systemManagement(Model model) {
		ModelAndView mav = new ModelAndView();

		mav.setViewName("admin/systemManagement");

		return mav;
	}

	/**
	 * �떆�뒪�뀥愿�由� 議고쉶
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/getSystemCodeList")
	private @ResponseBody Map<String, Object> getSystemCodeList(@RequestBody Map<String, Object> param) {

		List<Map<String, Object>> codeList = service.getSystemCodeList(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("CODE_LIST", codeList);

		return returnMap;
	}
	
	/**
	 * �떆�뒪�뀥愿�由� ���옣
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/admin/saveSystemCodeList")
	private @ResponseBody Map<String, Object> saveSystemCodeList(@RequestBody Map<String, Object> param) {
		
		List<Map<String, Object>> codeList = (List<Map<String, Object>>) param.get("CODE_LIST");
		
		int returnInfo = service.saveSystemCodeList(codeList);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", returnInfo);

		return returnMap;
	}
	    
    /** 硫붾돱 紐⑸줉 �럹�씠吏�
     * @param model
     * @return
     */
    @GetMapping("/admin/menuList")
    public String menuList(Model model) {
        /*      
        List<Map<String,Object>> menuList = authService.getMenuList();  
        model.addAttribute("menuList", menuList);
        */
        
        /*form�뿉 select濡� �궗�슜�븷 肄붾뱶 紐⑸줉*/
        
        model.addAttribute("menuTypeList", commonService.getCodeList("MENU_TYPE"));
        model.addAttribute("authCodeList", commonService.getCodeList("AUTH_CD"));
        return "admin/menuList";
    }
    
    /**硫붾돱 紐⑸줉 媛��졇�삤湲�(ajax)
     * @param model
     * @return
     */
    @PostMapping("/admin/getMenuListInfo") 
    public @ResponseBody List<Map<String,Object>> getMenuList(Model model){
        return authService.getMenuList();
    } 
    
    
    /**沅뚰븳 �럹�씠吏�硫붾돱 紐⑸줉 媛��졇�삤湲�(ajax)
     * @param model
     * @return
     */
    @RequestMapping("/admin/getAuthMenuListInfo")  
    public @ResponseBody List<Map<String,Object>> getAuthMenuListInfo(Model model){
        return authService.getAuthMenuList(); 
    } 
    
    @PostMapping("/admin/getMenuIdCount")
    public @ResponseBody int getMenuIdCount(@RequestBody HashMap<String,Object> requestMap) {
        List<Map<String,Object>> menuIdList = authService.getMenuId(requestMap);
        return menuIdList.size();
    }
    
    /** 硫붾돱 �벑濡� (ajax)
     * @param paramMap
     * @return
     */
    @PostMapping("/admin/insertMenuInfo")
    public @ResponseBody Map<String,Object> insertMenu(@RequestBody HashMap<String,Object> paramMap){ 
        Map<String,Object> resultMap = new HashMap<String,Object>();
        paramMap.put("USR_NO", commonService.getLoginUserNo());
        
        authService.insertMenuInfo(paramMap);   
        
        /*�젙�긽�쟻�쑝濡� �뜲�씠��媛� �궫�엯�릱�쓣 寃쎌슦 硫붾돱 seq瑜� �젙�젹�떆�궓�떎.*/
        if(paramMap.get("seq") != null) {
            HashMap<String,Object> paramMap2 = new HashMap<String,Object>();
            paramMap2.put("SEQ",paramMap.get("seq"));
            paramMap2.put("START_SEQ",paramMap.get("MENU_SEQ"));
            paramMap2.put("PROCESS","SUM");
            int result = authService.updateMenuSeq(paramMap2);
            resultMap.put("result", result);
            resultMap.put("SEQ", paramMap.get("seq"));
        }
        
        resultMap.put("result", 0);
        return resultMap;
    }
    
    /** 硫붾돱 �궘�젣(ajax)
     * @param seq
     * @return
     */
    @PostMapping("/admin/deleteMenuInfo")
    public @ResponseBody Map<String,Object> deleteMenu(@RequestBody Map<String,Object> requestMap){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        
        try {
            Map<String,Object> paramMap = new HashMap<String,Object>();
            
            List<Object> childrenArray = (List<Object>) requestMap.get("CHILDREN");
            
            if(childrenArray == null) {
                childrenArray = new ArrayList<Object>();
            }
            
            if(requestMap.get("SEQ") != null) {
                childrenArray.add(requestMap.get("SEQ"));               
            }
            
            paramMap.put("MENU_SEQ_LIST", childrenArray);
            
            if(childrenArray.size() > 0) {
                
                authService.deleteMenuList(paramMap);
                authService.deleteMenuRoleList(paramMap);
                
                //�� 硫붾돱 seq�씠�룞
                if(requestMap.get("MENU_SEQ") != null) {
                    
                    paramMap.clear();
                    childrenArray.clear();
                    childrenArray.add("empty");
                    
                    paramMap.put("SEQ_CNT",-1*(childrenArray.size()));
                    paramMap.put("MENU_SEQ_LIST", childrenArray);   //議곌굔 �뾾�쓬
                    paramMap.put("START_SEQ",requestMap.get("MENU_SEQ"));
                    paramMap.put("END_SEQ",999999999);//理쒕�媛�
                    
                    authService.updateOtherMenu(paramMap);
                }
                
                
                resultMap.put("result",1);
            }
            
        }catch(Exception e) {
            log.error(" " + e.getCause());
            resultMap.put("result",0);
        }       
        return resultMap;
    }
    
    /** 硫붾돱 �닔�젙(ajax)
     * @param paramMap
     * @return
     */
    @PostMapping("/admin/updateMenuInfo")
    public @ResponseBody Map<String,Object> updateMenu(HttpServletRequest request,@RequestBody Map<String,Object> paramMap){
        Map<String,Object> resultMap = new HashMap<String,Object>();
//	      try {
            paramMap.put("USR_NO",request.getSession().getAttribute("USR_NO"));     
            int result = authService.updateMenuInfo(paramMap);      
            
            
            if("Y".equals(paramMap.get("isChange")) && result == 1) {
                //�빐�떦 硫붾돱媛� �씠�룞�씪 寃쎌슦
                String seq = paramMap.get("SEQ").toString();
                int menuSeq = Integer.parseInt(paramMap.get("MENU_SEQ").toString()),
                    oldMenuSeq = Integer.parseInt(paramMap.get("OLD_MENU_SEQ").toString()),
                    menuDepth = Integer.parseInt(paramMap.get("MENU_DEPTH").toString()),
                    oldMenuDepth = Integer.parseInt(paramMap.get("OLD_MENU_DEPTH").toString());
                
                int seqDiff = menuSeq- oldMenuSeq,
                    depthDiff = menuDepth - oldMenuDepth;
                
                List<Object> childrenArray = (List<Object>)paramMap.get("CHILDREN");
                
                HashMap<String,Object> paramMap2 = new HashMap<String,Object>();
                
                if(childrenArray != null && childrenArray.size() > 0) {
                    
                    //�옄�떇�씠 �엳�쓣 寃쎌슦
                    //�옄�떇�쓽 depth�� seq瑜� �뾽�뜲�씠�듃 �븳�떎.
                    paramMap2.put("DEPTH_DIFF", depthDiff);
                    paramMap2.put("SEQ_DIFF",seqDiff);
                    
                    paramMap2.put("MENU_SEQ_LIST", childrenArray);
//	                  int res = authService.updateMenuChildren(paramMap2);                
                    authService.updateMenuChildren(paramMap2);              
                    
                    //�옄�떇�쇅 硫붾돱�뱾�쓽 seq瑜� �뾽�뜲�씠�듃 �븳�떎.
                    paramMap2.clear();
                    
                    childrenArray.add( paramMap.get("SEQ") );
                    
                    paramMap2.put("MENU_SEQ_LIST", childrenArray);
                    
                    paramMap2.put("SEQ_CNT", menuSeq>oldMenuSeq ? -1*(childrenArray.size()) : (childrenArray.size())  );
                    paramMap2.put("START_SEQ", menuSeq>oldMenuSeq ? oldMenuSeq : menuSeq );
                    paramMap2.put("END_SEQ", menuSeq<oldMenuSeq ? oldMenuSeq+childrenArray.size() -1 : menuSeq+childrenArray.size() -1);
//	                  res = authService.updateOtherMenu(paramMap2);
                    authService.updateOtherMenu(paramMap2);
                    
                }else {
                    
                    //�옄�떇�씠 �뾾�쓣 寃쎌슦 
                    if(menuSeq < oldMenuSeq) {
                        //�쁽�옱 硫붾돱媛� �씠�쟾 硫붾돱蹂대떎 �쐞濡� �씠�룞�뻽�쓣 寃쎌슦
                        //�쁽�옱 硫붾돱�� �씠�쟾 硫붾돱 �궗�씠�쓽 seq媛믪쓣 1�뵫 利앷�
                        
                        paramMap2.put("SEQ",seq);
                        paramMap2.put("START_SEQ",menuSeq);
                        paramMap2.put("END_SEQ",oldMenuSeq);
                        paramMap2.put("PROCESS","SUM");
                    }else {
                        //�쁽�옱 硫붾돱媛� �씠�쟾 硫붾돱蹂대떎 諛묒쑝濡� �씠�룞�뻽�쓣 寃쎌슦
                        //�쁽�옱 硫붾돱�� �씠�쟾 硫붾돱 �궗�씠�쓽 seq媛믪쓣 1�뵫 媛먯냼
                        paramMap2.put("SEQ",seq);
                        paramMap2.put("START_SEQ",oldMenuSeq);
                        paramMap2.put("END_SEQ",menuSeq);
                        paramMap2.put("PROCESS","MINUS");
                    }               
                    result = authService.updateMenuSeq(paramMap2);
                }           
            }
            resultMap.put("result", 1);
//	      }catch(Exception e) {
//	          log.error(" " + e.getCause());
//	          //泥섎━ 以� �삤瑜섍� 諛쒖깮 �뻽�쓣 寃쎌슦 0�쓣 由ы꽩
//	          resultMap.put("result", 0);
//	      }
        
        return resultMap;
    }
    
    @PostMapping("/admin/initMenuSeq")
    public @ResponseBody Map<String,Object> initMenuSEq(HttpServletRequest request,@RequestBody Map<String,Object> paramMap){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try {
//	          int result = authService.initMenuSeq(paramMap);
            authService.initMenuSeq(paramMap);
            resultMap.put("result", 1);
        }catch(Exception e) {
            log.error(" " + e.getCause());
            //泥섎━ 以� �삤瑜섍� 諛쒖깮 �뻽�쓣 寃쎌슦 0�쓣 由ы꽩
            resultMap.put("result", 0);
            
        }
        return resultMap;
    }
    
    
    /** 沅뚰븳 愿�由� �럹�씠吏�
     * @param request
     * @return
     */
    @GetMapping("/admin/authList")
    public String authList(HttpServletRequest request) {
        return "admin/authList";
    }   
    
    
    /** �궗�슜�옄 硫붾돱 沅뚰븳 �쉷�뱷
     * @param roleCd
     * @return
     */
    @PostMapping("/admin/getMenuRoleList")
    public @ResponseBody List<Map<String,Object>> getMenuAuthList(HttpServletRequest request,@RequestBody Map<String,Object> requestMap){
        String roleCd = requestMap.get("ROLE_CD").toString();
        List<Map<String,Object>> menuAuthList = authService.getMenuRoleList(roleCd);
        return menuAuthList;
    }
    
    /** �궗�슜�옄 沅뚰븳 �벑濡� (ajax)
     * @param paramMap
     * @return
     */
    @PostMapping("/admin/insertMenuRole")
    public @ResponseBody Map<String,Object> insertMenuRole(HttpServletRequest request,@RequestBody Map<String,Object> requestMap){
        
        Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put("USR_NO",request.getSession().getAttribute("USR_NO"));
        String roleCd = requestMap.get("roleCd").toString();
        HashMap<String,Object> menuAuth = ((HashMap<String,Object>)requestMap.get("menuCd"));
        Iterator<String> menuList = menuAuth.keySet().iterator();
        paramMap.put("ROLE_CD",roleCd);
        int rootCnt = 0;
        int result = 0;
        //�빐�떦 沅뚰븳�뿉 ���븳 湲곗〈 濡� �쟾泥� �궘�젣
        result = authService.deleteAllMenuRole(paramMap);
        
        while(menuList.hasNext()) {
            rootCnt++;
            String menuSeq= menuList.next();
            paramMap.put("MENU_SEQ",menuSeq);
            List<String> authList = (ArrayList<String>)menuAuth.get(menuSeq);
            
            for(String authCd : authList) {
                //�빐�떦 沅뚰븳�씠 �떒�닚 �젒洹� 沅뚰븳�씪 寃쎌슦 (�젒洹� 沅뚰븳�� �럹�씠吏��뿉�꽌留� �븘�슂.)
                if("100".equals(authCd)) {
                    paramMap.put("AUTH_CD",authCd);
                    authService.insertMenuRole(paramMap);
                }else {
                    //�젒洹� 沅뚰븳 �쇅 �봽濡쒖꽭�뒪 沅뚰븳�씪 寃쎌슦   
                    paramMap.put("AUTH_CD",authCd);
                    authService.insertMenuRoleChild(paramMap);
                }
            }                   
        }       
        Map<String,Object> resultMap = new HashMap<String,Object>();
        if(rootCnt == result) {
            //�젙�긽 泥섎━
            resultMap.put("result", "1");
        }else {
            //泥섎━以� 臾몄젣 諛쒖깮
            resultMap.put("result", "0");
        }       
        return resultMap;
    }
    
    
    /**�궗�슜�옄 沅뚰븳 �닔�젙(ajax)
     * @param request
     * @param paramMap
     * @return
     */
    @PostMapping("/admin/updateMenuRole")
    public @ResponseBody Map<String,Object> updateMenuRole(HttpServletRequest request,@ModelAttribute Map<String,Object> paramMap){
        
        paramMap.put("USR_NO",request.getSession().getAttribute("USR_NO"));
        int result = authService.updateMenuRole(paramMap);
        Map<String,Object> resultMap = new HashMap<String,Object>();
        resultMap.put("result", result);
        
        return resultMap;
    }
    
    /**�궗�슜�옄 沅뚰븳 �궘�젣(ajax)
     * @param seq
     * @param roleCd
     * @return
     */
    @PostMapping("/admin/deleteMenuRole")
    public @ResponseBody Map<String,Object> deleteMenuRole(@RequestParam String seq, @RequestParam String roleCd){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        try {
            Map<String,Object> paramMap = new HashMap<String,Object>();
            paramMap.put("SEQ", seq);
            paramMap.put("ROLE_CD",roleCd);
            int result = authService.deleteMenuRole(paramMap);  
            resultMap.put("result", result);
        }catch(Exception e) {
            resultMap.put("result",0);
        }       
        return resultMap;
    }
    
    /**沅뚰븳�뿉 ���븳 �봽濡쒖젥�듃 �긽�깭蹂� 沅뚰븳 �쉷�뱷
     * @param param
     * @return List<Map<String,Object>>
     */
    @PostMapping("/admin/selectRoleProStep")
    public @ResponseBody List<Map<String,Object>> selectRoleProStep(@RequestBody Map<String,Object> param){
        List<Map<String,Object>> resultMap = authService.selectRoleProStep(param);
        
        return resultMap;       
    }
    
    /**沅뚰븳�뿉 ���븳 �봽濡쒖젥�듃 �긽�깭蹂� 沅뚰븳 �궫�엯
     * @param session
     * @param param
     * @return Map<String,Object>
     */
    @PostMapping("/admin/insertRoleProStep")
    public @ResponseBody Map<String,Object> insertRoleProStep(HttpSession session,@RequestBody Map<String,Object> param){
        Map<String,Object> resultMap = new HashMap<String,Object>()
                    ,paramMap = new HashMap<String,Object>();
        /*湲곗〈 沅뚰븳 �궘�젣*/
        paramMap.put("ROLE_CD", param.get("ROLE_CD").toString());
        authService.deleteRoleProStep(paramMap);
        paramMap.clear();
        
        /*蹂�寃쎈맂 沅뚰븳 �궫�엯*/
        param.put("USR_NO",session.getAttribute("USR_NO"));
        for(String key : param.keySet()){
            if(key.contains("PRO_STEP_")) {
                paramMap.clear();
                paramMap.put("ROLE_CD",param.get("ROLE_CD").toString());
                paramMap.put("USR_NO",param.get("USR_NO").toString());
                paramMap.put("STEP_CD",key.split("_")[2]);
                paramMap.put("AUTH",param.get(key));
                authService.insertRoleProStep(paramMap);
            }
        }       
        
        return resultMap;
        
    }
    
    /*沅뚰븳 �삁�젣 �럹�씠吏�*/
    @GetMapping("/admin/authExample")
    public String authExample() {
        return "/admin/authExample";
    }	
}
