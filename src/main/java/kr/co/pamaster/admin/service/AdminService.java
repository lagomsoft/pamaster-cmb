package kr.co.pamaster.admin.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.pamaster.admin.mapper.AdminMapper;
import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.user.mapper.UserMapper;
import kr.co.pamaster.user.service.UserService;
import kr.co.pamaster.util.DBMessageSource;

@Service
public class AdminService {

	@Autowired
	private AdminMapper mapper;

	@Autowired
	private UserMapper userMapper;

	@Autowired
	private CommonService commonService;

	@Autowired
	private UserService userService;

	@Autowired
	private DBMessageSource messageSource;

	public List<Map<String, Object>> getGroup() {
		return mapper.selectGroup();
	}

	public List<Map<String, Object>> getRole() {
		return mapper.selectRole();
	}

	public List<Map<String, Object>> getUsers(Map<String, Object> param) {
		param.put("PAGE_INDEX", ((int) param.get("PAGE_INDEX") - 1) * (int) param.get("PAGE_SIZE"));
		param.put("PAGE_SIZE", (int) param.get("PAGE_SIZE"));

		List<Map<String, Object>> userList = mapper.selectUserList(param);
		List<Map<String, Object>> returnUserList = new ArrayList<Map<String, Object>>();
		
//		Map<Integer, String> cntMap = new HashMap<Integer, String>();
		
		if (userList != null) {

			for (int ui = 0; ui < userList.size(); ui++) {

				Map<String, Object> userMap = userList.get(ui);

				String cntFrs = (String) userMap.get("CNT_FRS");
				String cntMdl = (String) userMap.get("CNT_MDL");
				String cntEnd = (String) userMap.get("CNT_END");

				cntMdl = commonService.decryptAES(cntMdl);
				cntEnd = commonService.decryptAES(cntEnd);
				
//				if(cntMdl != null) {
//					String[] CNTTEL = cntMdl.split("-");
//					for(int i=0; i<CNTTEL.length; i++) {
//						cntMap.put(i, CNTTEL[i]);
//					}
//					if(CNTTEL.length == 3) {
//						cntFrs = cntMap.get(0);
//						cntMdl = cntMap.get(1);
//						cntEnd = cntMap.get(2);
//					}
//				}
				
//				userMap.put("CNT_FRS", cntFrs);
				userMap.put("CNT_MDL", cntMdl);
				userMap.put("CNT_END", cntEnd);

				userMap.put("ROLE_LIST", mapper.selectUserRole(userMap));
				userMap.put("IP_LIST", mapper.selectUserIP(userMap));

				returnUserList.add(userMap);
			}

		}

		return returnUserList;
	}

	public void userDelete(Map<String, Object> param) {

		param.put("usr_id", param.get("USR_ID"));
		mapper.userDelete(param);

	}
	 

	public List<Map<String, Object>> getSearchUsers(Map<String, Object> param) {
		param.put("PAGE_INDEX", ((int) param.get("PAGE_INDEX") - 1) * (int) param.get("PAGE_SIZE"));
		param.put("PAGE_SIZE", (int) param.get("PAGE_SIZE"));

		List<Map<String, Object>> userList = mapper.selectSearchUserList(param);
		List<Map<String, Object>> returnUserList = new ArrayList<Map<String, Object>>();
		
//		Map<Integer, String> cntMap = new HashMap<Integer, String>();
		
		if (userList != null) {

			for (int ui = 0; ui < userList.size(); ui++) {

				Map<String, Object> userMap = userList.get(ui);

				String cntFrs = (String) userMap.get("CNT_FRS");
				String cntMdl = (String) userMap.get("CNT_MDL");
				String cntEnd = (String) userMap.get("CNT_END");

				cntMdl = commonService.decryptAES(cntMdl);
				cntEnd = commonService.decryptAES(cntEnd);

//				if(cntMdl != null) {
//					String[] CNTTEL = cntMdl.split("-");
//					for(int i=0; i<CNTTEL.length; i++) {
//						cntMap.put(i, CNTTEL[i]);
//					}
//					if(CNTTEL.length == 3) {
//						cntFrs = cntMap.get(0);
//						cntMdl = cntMap.get(1);
//						cntEnd = cntMap.get(2);
//					}
//				}
				
//				userMap.put("CNT_FRS", cntFrs);
				userMap.put("CNT_MDL", cntMdl);
				userMap.put("CNT_END", cntEnd);

				userMap.put("ROLE_LIST", mapper.selectUserRole(userMap));
				userMap.put("IP_LIST", mapper.selectUserIP(userMap));

				returnUserList.add(userMap);
			}

		}

		return returnUserList;
	}

	public int getUsersListCount(Map<String, Object> param) {

		int usersCount = 0;
		usersCount = mapper.getUsersListCount(param);

		return usersCount;
	}

	public int getSearchUsersListCount(Map<String, Object> param) {

		int usersCount = 0;

		usersCount = mapper.getSearchUsersListCount(param);

		return usersCount;
	}

	public String checkUserID(Map<String, Object> param) {

		String returnStr = "";

		Map<String, Object> userData = param;

		if (userData != null) {

			List<String> users = mapper.checkUserID(param);

			if (users != null && users.size() > 0) {
				returnStr = users.get(0);

				for (int ui = 1; ui < users.size(); ui++) {
					returnStr = returnStr + ", " + users.get(ui);
				}
			}

		}

		return returnStr;
	}

	public String checkUserIDandEmail(Map<String, Object> param) {

		String returnStr = "";

		List<String> userList = (List<String>) param.get("USER_LIST");
		if (userList != null && userList.size() > 0) {

			List<String> users = mapper.checkUserEmail(param);

			if (users != null && users.size() > 0) {
				returnStr = users.get(0);

				for (int ui = 1; ui < users.size(); ui++) {
					returnStr = returnStr + ", " + users.get(ui);
				}
			}

		}

		return returnStr;
	}

	public int saveUser(Map<String, Object> userMap) {

		Object userNo = commonService.getLoginUserNo();
		int returnCD = 0;
		
		if (userMap.get("CNT_MDL") != null) {
			String cntMdl = userMap.get("CNT_MDL").toString();
			cntMdl = commonService.encryptAES(cntMdl);
			userMap.put("CNT_MDL", cntMdl);
		}

//		if (userMap.get("CNT_MDL") != null) {
//			String cntMdl = userMap.get("CNT_FRS").toString()+"-"+userMap.get("CNT_MDL").toString()+"-"+userMap.get("CNT_END").toString();
//			cntMdl = commonService.encryptAES(cntMdl);
//			userMap.put("CNT_MDL", cntMdl);
//			userMap.put("CNT_FRS", null);
//			userMap.put("CNT_END", null);
//		}

		if (userMap.get("CNT_END") != null) {
			String cntEnd = userMap.get("CNT_END").toString();
			cntEnd = commonService.encryptAES(cntEnd);
			userMap.put("CNT_END", cntEnd);
		}

		userMap.put("USER_NO", userNo);

		if ("Y".equals(userMap.get("NEW"))) { // 신규 유저인 경우
			userMap.put("USR_NO", userNo);
			// 유저 최초 생성시 초기화 목록(유저관리 페이지에서 유저생성 시 초기권한 세팅으로 자동매핑시킴)
			List<Map<String, Object>> codeList = commonService.getCodeList("INIT_USER_SETTING");
			Map<String, Object> initRoleMap = new HashMap<>();
			List<String> initRoleList = new ArrayList<>();
			for (Map<String, Object> codeMap : codeList) {
				Object code = codeMap.get("CD");
				// 최초 권한 List -> Array -> Map
				if ("INIT_ROLE".equals(code)) {
					String initRoleSet = (String) codeMap.get("META_1");
					if (!initRoleSet.isEmpty()) {
						initRoleList = Arrays.asList(initRoleSet.split(","));
					}
				}
			}

			mapper.insertUser(userMap);
			for (String initRole : initRoleList) {
				initRoleMap.put("INIT_ROLE", initRole);
				initRoleMap.put("USR_ID", userMap.get("USR_ID"));
				userMapper.joinUserInitRole(initRoleMap);
			}
			userMap.put("USR_PW", commonService.createTempPassword(userMap.get("USR_ID").toString()));
			userService.changePw(userMap);
			returnCD = 2;

		} else {
			mapper.updateUser(userMap);
			// mapper.deleteUserRole(userMap);
			// mapper.deleteUserIP(userMap);
		}
		/*
		 * if (userMap.get("ROLE_LIST") != null) {
		 * 
		 * List<Map<String, Object>> roleList = (List<Map<String, Object>>)
		 * userMap.get("ROLE_LIST");
		 * 
		 * for (Map<String, Object> roleMap : roleList) {
		 * 
		 * if ("Y".equals(userMap.get("NEW"))) { roleMap.put("USR_ID",
		 * userMap.get("USR_ID")); } else { roleMap.put("USR_NO",
		 * userMap.get("USR_NO")); }
		 * 
		 * roleMap.put("USER_NO", userNo);
		 * 
		 * mapper.insertUserRole(roleMap); }
		 * 
		 * }
		 * 
		 * 
		 * if (userMap.get("IP_LIST") != null) {
		 * 
		 * List<Map<String, Object>> ipList = (List<Map<String, Object>>)
		 * userMap.get("IP_LIST");
		 * 
		 * for (Map<String, Object> ipMap : ipList) {
		 * 
		 * if ("Y".equals(userMap.get("NEW"))) { ipMap.put("USR_ID",
		 * userMap.get("USR_ID")); } else { ipMap.put("USR_NO", userMap.get("USR_NO"));
		 * }
		 * 
		 * ipMap.put("USER_NO", userNo);
		 * 
		 * mapper.insertUserIP(ipMap); } }
		 */

		return returnCD;
	}

	public int saveUserIP(Map<String, Object> param) {

		Object userNo = commonService.getLoginUserNo();

		List<Map<String, Object>> userIPList = (List<Map<String, Object>>) param.get("IP_LIST");

		mapper.deleteUserIP(param);

		if (userIPList != null) {
			for (Map<String, Object> userIPMap : userIPList) {

				userIPMap.put("USR_NO", param.get("USR_NO"));
				userIPMap.put("USER_NO", userNo);

				mapper.insertUserIP(userIPMap);
			}
		}

		return 0;
	}

	public int saveUserRole(Map<String, Object> param) {

		Object userNo = commonService.getLoginUserNo();

		List<Map<String, Object>> userRoleList = (List<Map<String, Object>>) param.get("ROLE_LIST");

		mapper.deleteUserRole(param);

		if (userRoleList != null) {
			for (Map<String, Object> userRoleMap : userRoleList) {

				userRoleMap.put("USR_NO", param.get("USR_NO"));
				userRoleMap.put("USER_NO", userNo);

				mapper.insertUserRole(userRoleMap);
			}
		}

		return 0;
	}

	public List<Map<String, Object>> getCodeGroupList(Map<String, Object> param) {
		return mapper.selectCodeGroupList(param);
	}

	public String checkCodeGroup(Map<String, Object> param) {

		String returnStr = "";

		List<String> groupList = (List<String>) param.get("GROUP_LIST");

		if (groupList != null && groupList.size() > 0) {

			List<String> groupCds = mapper.checkCodeGroup(param);

			if (groupCds != null && groupCds.size() > 0) {
				returnStr = groupCds.get(0);

				for (int ui = 1; ui < groupCds.size(); ui++) {
					returnStr = returnStr + ", " + groupCds.get(ui);
				}
			}

		}

		return returnStr;
	}

	public void saveCodeGroup(List<Map<String, Object>> dataList) {

		for (Map<String, Object> codeGroup : dataList) {

			if ("Y".equals(codeGroup.get("NEW"))) {
				mapper.insertCodeGroup(codeGroup);
			} else {
				mapper.updateCodeGroup(codeGroup);
			}

		}
	}

	public List<Map<String, Object>> getCodeList(Map<String, Object> param) {

		return mapper.selectCodeList(param);
	}

	public List<Map<String, Object>> getAllCodeList(Map<String, Object> param) {

		return mapper.selectCodeList(param);
	}

	public String checkCode(Map<String, Object> param) {

		String returnStr = "";

		List<String> cdList = (List<String>) param.get("CODE_LIST");

		if (cdList != null && cdList.size() > 0) {

			List<String> codes = mapper.checkCode(param);

			if (codes != null && codes.size() > 0) {
				returnStr = codes.get(0);

				for (int ui = 1; ui < codes.size(); ui++) {
					returnStr = returnStr + ", " + codes.get(ui);
				}
			}

		}

		return returnStr;
	}

	public void saveCode(String groupCd, List<Map<String, Object>> dataList) {

		for (Map<String, Object> codeGroup : dataList) {

			codeGroup.put("GRP_CD", groupCd);

			if ("Y".equals(codeGroup.get("NEW"))) {
				mapper.insertCode(codeGroup);
			} else {
				mapper.updateCode(codeGroup);
			}

		}
	}

	public String checkMappingCode(Map<String, Object> param) {

		String returnStr = "";

		List<String> list = (List<String>) param.get("MAPPING_LIST");

		if (list != null && list.size() > 0) {

			List<String> mappings = mapper.checkMappingCode(param);

			if (mappings != null && mappings.size() > 0) {
				returnStr = mappings.get(0);

				for (int ui = 1; ui < mappings.size(); ui++) {
					returnStr = returnStr + ", " + mappings.get(ui);
				}
			}

		}

		return returnStr;
	}

	public List<Map<String, Object>> getMappingList(Map<String, Object> param) {
		return mapper.selectMappingCode(param);
	}

	public List<Map<String, Object>> getMappingTcnList(Map<String, Object> param) {
		return mapper.selectMappingTcnList(param);
	}

	public void saveMappingTcn(Map<String, Object> tcnData) {

		Object userNo = commonService.getLoginUserNo();

		if (tcnData.get("TCN_LIST") != null) {
			List<Map<String, Object>> tcnList = (List<Map<String, Object>>) tcnData.get("TCN_LIST");

			for (Map<String, Object> tcnMap : tcnList) {
				tcnMap.put("USER_NO", userNo);

				if ("Y".equals(tcnMap.get("ISNEW"))) {
					mapper.insertMappingTcn(tcnMap);
				} else {
					mapper.updateMappingTcn(tcnMap);
				}
			}

		}
	}

	public void saveMapping(List<Map<String, Object>> dataList) {

		Object userNo = commonService.getLoginUserNo();

		for (Map<String, Object> mappingData : dataList) {

			mappingData.put("USER_NO", userNo);

			if ("Y".equals(mappingData.get("NEW"))) {
				mapper.insertMapping(mappingData);
			} else {
				mapper.updateMapping(mappingData);
				mapper.updateMappingTcnKey(mappingData);
			}

			mapper.deleteSynonm(mappingData);
			mapper.deleteRemov(mappingData);

			// 동의어에 유형명 추가
			mappingData.put("SYNONM", mappingData.get("INDVDLINFO_NM"));
			mapper.insertSynonm(mappingData);

			if (mappingData.get("SYNONMS") != null && !"".equals(mappingData.get("SYNONMS"))) {
				String synonmsStr = mappingData.get("SYNONMS").toString();
				String[] synonms = synonmsStr.split(";");
				for (String synonm : synonms) {
					if (!"".equals(synonm)) {
						mappingData.put("SYNONM", synonm);
						mapper.insertSynonm(mappingData);
					}
				}
			}

			if (mappingData.get("REMOVS") != null && !"".equals(mappingData.get("REMOVS"))) {
				String removsStr = mappingData.get("REMOVS").toString();
				String[] removs = removsStr.split(";");
				for (String remov : removs) {
					if (!"".equals(remov)) {
						mappingData.put("REMOV_VAL", remov);
						mapper.insertRemov(mappingData);
					}
				}
			}

		}
	}

	public List<Map<String, Object>> getRiskList(Map<String, Object> param) {
		return mapper.selectRiskList(param);
	}

	public String checkRiskCode(Map<String, Object> param) {

		String returnStr = "";

		List<String> list = (List<String>) param.get("RISK_LIST");

		if (list != null && list.size() > 0) {

			List<String> risks = mapper.checkRiskCode(param);

			if (risks != null && risks.size() > 0) {
				returnStr = risks.get(0);

				for (int ui = 1; ui < risks.size(); ui++) {
					returnStr = returnStr + ", " + risks.get(ui);
				}
			}

		}

		return returnStr;
	}

	public void deleteRisk(List<String> dataList) {
		Map<String, Object> riskData = new HashMap<String, Object>();

		for (String riskCd : dataList) {
			riskData.put("ORG_RISK_CD", riskCd);
			mapper.deleteRisk(riskData);
		}
	}

	public void saveRiskList(List<Map<String, Object>> dataList, Map<String, Object> param) {

		for (Map<String, Object> riskData : dataList) {

			if (riskData.get("RISK_RELIMP") == "") {
				riskData.put("RISK_RELIMP", 0);
			}
			if (riskData.get("RISK_PNT") == "") {
				riskData.put("RISK_PNT", 0);
			}

			if ("Y".equals(riskData.get("NEW"))) {
				riskData.put("REGISTER", param.get("REGISTER"));
				mapper.insertRisk(riskData);
			} else {
				riskData.put("UPDUSR", param.get("REGISTER"));
				mapper.updateRisk(riskData);
			}
		}
	}

	public List<Map<String, Object>> getEvaluationList(Map<String, Object> param) {
		return mapper.selectEvaluationList(param);
	}

	public String checkEvaluationCode(Map<String, Object> param) {

		String returnStr = "";

		List<String> list = (List<String>) param.get("EVALUATION_LIST");

		if (list != null && list.size() > 0) {

			List<String> risks = mapper.checkEvaluationCode(param);

			if (risks != null && risks.size() > 0) {
				returnStr = risks.get(0);

				for (int ui = 1; ui < risks.size(); ui++) {
					returnStr = returnStr + ", " + risks.get(ui);
				}
			}
		}
		return returnStr;
	}

	public void deleteEvaluation(List<String> dataList) {
		Map<String, Object> evaluationData = new HashMap<String, Object>();

		for (String riskCd : dataList) {
			evaluationData.put("ORG_RISK_CD", riskCd);
			mapper.deleteEvaluation(evaluationData);
		}
	}

	public void saveEvaluationList(List<Map<String, Object>> dataList, Map<String, Object> param) {

		for (Map<String, Object> evaluationData : dataList) {

			if ("Y".equals(evaluationData.get("NEW"))) {
				evaluationData.put("REGISTER", param.get("REGISTER"));
				mapper.insertEvaluation(evaluationData);
			} else {
				evaluationData.put("UPDUSR", param.get("REGISTER"));
				mapper.updateEvaluation(evaluationData);
			}
		}
	}

	public List<Map<String, Object>> getMessageList(Map<String, Object> param) {

		if (param.get("MENU_ID") == null && "".equals(param.get("MSG_CN")) && "".equals(param.get("MSG_ID"))) {
			return mapper.selectMessageList(param);
		} else {
			return mapper.searchMessageList(param);
		}
	}

	public String checkMessageCode(Map<String, Object> param) {

		String returnStr = "";

		List<String> list = (List<String>) param.get("MESSAGE_LIST");

		if (list != null && list.size() > 0) {

			List<String> mappings = mapper.checkMessageCode(param);

			if (mappings != null && mappings.size() > 0) {
				returnStr = mappings.get(0);

				for (int ui = 1; ui < mappings.size(); ui++) {
					returnStr = returnStr + ", " + mappings.get(ui);
				}
			}

		}

		return returnStr;
	}

	public void deleteMessage(List<String> dataList) {
		Map<String, Object> messageData = new HashMap<String, Object>();

		for (String messageCd : dataList) {
			messageData.put("ORG_MSG_ID", messageCd);
			mapper.deleteMessage(messageData);
		}
	}

	public void saveMessage(List<Map<String, Object>> dataList) {

		for (Map<String, Object> messageData : dataList) {

			if (!"Y".equals(messageData.get("NEW"))) {
				mapper.deleteMessage(messageData);
			}

			messageData.put("CNT_CD", "ko");

			mapper.insertMessage(messageData);

		}

		commonService.setClearMessageMap();
	}

	public List<Map<String, Object>> getProjectScheduleData(Map<String, Object> param) {

		param.put("NULL_DEFTIME", commonService.getDefTime());

		return mapper.selectProjectScheduleData(param);
	}

	public int getProjectScheduleDataCount(Map<String, Object> param) {
		return mapper.getProjectScheduleDataCount(param);
	}

	public List<Map<String, Object>> selectMenuID() {

		return mapper.selectMenuID();
	}

	public List<Map<String, Object>> getRoleList(Map<String, Object> param) {
		List<Map<String, Object>> roleList = mapper.getRoleList(param);

		List<Map<String, Object>> returnList = new ArrayList<>();

		for (int i = 0; i < roleList.size(); i++) {
			Map<String, Object> returnMap = roleList.get(i);

			switch (returnMap.get("USE_YN").toString()) {

			case "Y":
				returnMap.put("USE_YN", "사용");
				break;

			case "N":
				returnMap.put("USE_YN", "미사용");
				break;

			}
			returnList.add(returnMap);
		}

		return returnList;
	}

	public void saveRole(Map<String, Object> param) {

		if ("NEW".equals(param.get("CLICKED_ROLE"))) {
			mapper.saveRole(param);
		} else {
			mapper.updateRole(param);
		}

	}

	public void deleteRole(Map<String, Object> param) {
		mapper.deleteMenuRole(param);
		mapper.deleteUserRole(param);
		mapper.deleteRole(param);
	}

	/**
	 * 로그인 이력 조회
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getLoginLog(Map<String, Object> param) {
		param.put("TYPE", 1);

		List<Map<String, Object>> returnList = new ArrayList<>();
		Map<String, Object> codeParam = new HashMap<>();

		param.put("PAGE_INDEX", (((int) param.get("PAGE_INDEX")) - 1) * (int) param.get("PAGE_SIZE"));
		param.put("PAGE_SIZE", (int) param.get("PAGE_SIZE"));

		if (!StringUtils.isEmpty((String) param.get("ST_DT")))
			param.put("ST_DT", (String) param.get("ST_DT") + ":00");
		if (!StringUtils.isEmpty((String) param.get("ED_DT")))
			param.put("ED_DT", (String) param.get("ED_DT") + ":00");

		if (!StringUtils.isEmpty((String) param.get("SEARCH_CONTENTS"))) {
			codeParam.put("GRP_CD", "HIST_SEARCH_TY");
			codeParam.put("CD", param.get("SEARCH_TY"));
			param.put("SEARCH_TY", commonService.getCdNm(codeParam));
			param.put("SEARCH_CONTENTS", "%" + (String) param.get("SEARCH_CONTENTS") + "%");
		}
		returnList = mapper.getLoginLogList(param);

		return returnList;
	}

	/**
	 * [접속이력관리 > 프로젝트별 접속이력] 프로젝트 목록 조회
	 * 
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> retrieveProjectList(Map<String, Object> params) {

		params.put("PAGE_INDEX", (((int) params.get("PAGE_INDEX")) - 1) * 10);

		String STEP_CD = "";

		if (params.get("SEARCH_TY") != null) {
			STEP_CD = String.valueOf(params.get("SEARCH_TY"));
			params.put("STEP_CD", STEP_CD);
		}

		if (STEP_CD != null && !"".equals(STEP_CD)) {
			params.put("MIN_CD", STEP_CD.substring(0, 1) + "00");
			params.put("MAX_CD", STEP_CD.substring(0, 1) + "99");
		}

		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		resultList = mapper.retrieveProjectList(params);

		return resultList;
	}

	/**
	 * [접속이력관리 > 프로젝트별 접속이력] 프로젝트 목록 카운트 조회
	 * 
	 * @param params
	 * @return
	 */
	public int retrieveProjectListCount(Map<String, Object> params) {
		return mapper.retrieveProjectListCount(params);
	}

	/**
	 * [접속이력관리 > 프로젝트별 접속이력] 프로젝트별 로그이력 조회
	 * 
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> retrieveLogListByProject(Map<String, Object> params) {
		params.put("PAGE_INDEX", (((int) params.get("PAGE_INDEX")) - 1) * 10);

		List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
		resultList = mapper.retrieveLogListByProject(params);

		return resultList;
	}

	/**
	 * [접속이력관리 > 프로젝트별 접속이력] 프로젝트별 로그이력 카운트 조회
	 * 
	 * @param params
	 * @return
	 */
	public int retrieveLogListByProjectCount(Map<String, Object> params) {
		return mapper.retrieveLogListByProjectCount(params);
	}

	/**
	 * [접속이력관리 > 접속기록] 사용자 접근 카운트 조회
	 * 
	 * @param param
	 * @return
	 */
	public int getLoginLogCount(Map<String, Object> param) {
		return mapper.getLoginLogListCount(param);
	}

	/**
	 * 메뉴권한저장
	 * 
	 * @param param
	 */
	public void saveRoleMenu(Map<String, Object> param) {
		List<String> menuRoleList = (List<String>) param.get("CHECK_LIST");

		mapper.deleteMenuRole(param);
		mapper.menuRoleReset();

		param.put("USR_NO", commonService.getLoginUserNo());

		for (int i = 0; i < menuRoleList.size(); i++) {
			param.put("MENU_ID", menuRoleList.get(i));

			String menuRoleCount = mapper.getMenuRoleCount(param);

			if (!("0".equals(menuRoleCount))) {
				continue;
			} else {
				mapper.saveMenuRole(param);
			}

		}
	}

	/**
	 * 유저권한조회
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getUserRoleSearch(Map<String, Object> param) {
		String groupCd = new String();

		param.put("PAGE_INDEX", (Integer.parseInt(param.get("PAGE_INDEX").toString()) - 1) * 10);
		param.put("SEARCH_CONTENTS", "%" + param.get("SEARCH_CONTENTS") + "%");

		if ("0".equals(param.get("SEARCH_TYPE").toString())) {

			return mapper.getUserRoleAll(param);
		} else if ("ALL".equals(param.get("SEARCH_CATEGORY").toString())) {

			return mapper.getUserRoleSearch(param);
		} else if ("DEPARTMENT".equals(param.get("SEARCH_CATEGORY").toString())) {
			groupCd = mapper.getGroupName(param);

			param.put("SEARCH_CONTENTS", "%" + groupCd + "%");

			return mapper.getUserRoleDepartmentSearch(param);
		}

		return mapper.getUserRoleAll(param);
	}

	public String getUserRoleCount(Map<String, Object> param) {
		String groupCd = new String();

		if ("0".equals(param.get("SEARCH_TYPE").toString())) {

			return mapper.getUserRoleCount();
		} else if ("ALL".equals(param.get("SEARCH_CATEGORY").toString())) {

			return mapper.getUserRoleSearchCount(param);
		} else if ("DEPARTMENT".equals(param.get("SEARCH_CATEGORY").toString())) {
			groupCd = mapper.getGroupName(param);

			param.put("SEARCH_CONTENTS", "%" + groupCd + "%");

			return mapper.getUserRoleDepartmentSearchCount(param);
		}
		return mapper.getUserRoleCount();
	}

	public List<Map<String, Object>> getUserRoleSelect(Map<String, Object> param) {
		List<Map<String, Object>> userList = mapper.getUserRoleSelect(param);
		List<Map<String, Object>> returnList = new ArrayList<>();

		for (int i = 0; i < userList.size(); i++) {
			Map<String, Object> returnMap = userList.get(i);
			returnMap.put("USR_ID", commonService.getUsrId(returnMap.get("USR_NO")));

			returnList.add(returnMap);
		}

		return returnList;
	}

	public void saveUserRoleList(Map<String, Object> param) {
		mapper.deleteUserRoleList(param);

		param.put("REGISTER", commonService.getLoginUserNo());

		List<String> userList = (List<String>) param.get("USER_LIST");

		for (int i = 0; i < userList.size(); i++) {
			param.put("USR_NO", userList.get(i));
			mapper.saveUserRoleList(param);
		}

		return;
	}

	public Boolean uploadGroup(List<Map<String, Object>> groupList) {
		List<Map<String, Object>> checkGroupList = new ArrayList<>();

		Map<String, Object> param = new HashMap<>();

		for (int i = 0; i < groupList.size(); i++) {
			param = groupList.get(i);

			String gpCd = groupList.get(i).get("GP_CD").toString();

			for (int j = 0; j < checkGroupList.size(); j++) {
				String checkGpCd = checkGroupList.get(j).get("GP_CD").toString();
				if (gpCd.equals(checkGpCd)) {
					return false;
				}
			}
			checkGroupList.add(groupList.get(i));
		}
		mapper.truncateGroup();

		for (int i = 0; i < groupList.size(); i++) {
			param = groupList.get(i);

			if (param.get("DEPTH").toString().equals("1")) {
				param.put("UPPER_GP_CD", "ROOT");
			}
			param.put("USR_NO", commonService.getLoginUserNo());

			mapper.uploadGroup(param);
		}

		return true;
	}

	// conformity 페이지에서만 사용되는 groupList 로 common에 있는 groupList와 사용처가 다름
	public List<Map<String, Object>> getGroupList() {
		/*
		 * if(commonService.checkCorpUse()) { // 사용자별 기업명/부서 사용시 본인의 커스터마이징된 GP_CD만 읽어오며
		 * 나머지는 디폴트로 설정된 것들 읽어봄 String userNo =
		 * commonService.getLoginUserNo().toString(); Map<String, Object> memberMap =
		 * new HashMap<String, Object>(); memberMap.put("no", userNo); List<Map<String,
		 * Object>> memberInfoList = userMapper.getMember(memberMap); Map<String,
		 * Object> memberInfo = memberInfoList.get(0); List<String> memberRoleList =
		 * userMapper.getRoleList(memberInfo.get("USR_NO"));
		 * if(memberRoleList.contains("ROLE_SYS_ADMIN") ||
		 * memberRoleList.contains("ROLE_KISA_ADMIN")) { // 관리자가 아닌 계정은 프로젝트 생성 페이지에서
		 * 본인의 부서만 읽어옴 return mapper.getGroupList(); // 다른 사람의 커스터마이징 부서는 읽어올 수 없음 }
		 * return mapper.getCorpGroupList(memberInfo); }
		 */
		return mapper.getGroupList();
	}

	public void updateGroupInfo(Map<String, Object> param) {

		mapper.updateGroupInfo(param);
	}

	public void updateGroupInfoChild(Map<String, Object> param) {

		mapper.updateGroupInfoChild(param);
	}

	public List<Map<String, Object>> getGroupInfo(Map<String, Object> param) {
		return mapper.getGroupInfo(param);
	}

	public Map<String, Object> getGroupInfoByIDX(Map<String, Object> param) {
		return mapper.getGroupInfoByIDX(param);
	}

	public Map<String, Object> insertGroupInfo(Map<String, Object> param) {
		mapper.insertGroupInfo(param);

		return mapper.selectGroupInfo(param);
	}

	public Map<String, Object> selectGroupInfo(Map<String, Object> param) {
		return mapper.selectGroupInfo(param);
	}

	/* 자신 외 업데이트 */
	public int updateGroup(Map<String, Object> param) {
		return mapper.updateGroup(param);
	}

	public String groupCheck(Map<String, Object> param) {

		return mapper.groupCheck(param);
	}

	public void deleteGroup(Map<String, Object> param) {
		mapper.deleteGroup(param);
		if (!param.isEmpty()) {
			List<Object> childrenGroupList = (List<Object>) param.get("CHILDREN");
			for (Object childrenGroup : childrenGroupList) {
				param.put("C_IDX", childrenGroup);
				mapper.deleteGroup(param);
			}
			return;
		}
	}

	public List<Map<String, Object>> getUpperGroupList() {
		return mapper.getUpperGroupList();
	}

	public List<Map<String, Object>> getStorageInfo() {
		List<Map<String, Object>> storageList = new ArrayList<Map<String, Object>>();

		File[] drives = File.listRoots();

		for (File drive : drives) {
			Map<String, Object> temp = new HashMap<String, Object>();
			temp.put("path", drive.getAbsolutePath());
			temp.put("totalSize", drive.getTotalSpace() / Math.pow(1024, 3));
			temp.put("useSize", drive.getUsableSpace() / Math.pow(1024, 3));
			temp.put("freeSize", (drive.getTotalSpace() - drive.getUsableSpace()) / Math.pow(1024, 3));
			temp.put("percent", drive.getUsableSpace() * 100 / drive.getTotalSpace());
			storageList.add(temp);
		}
		return storageList;
	}

	/**
	 * 프로시저 에러 로그
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> selectProcErrorLog(Map<String, Object> param) {
		param.put("PAGE_INDEX", (((int) param.get("PAGE_INDEX")) - 1) * 10);
		param.put("PAGE_SIZE", (int) param.get("PAGE_SIZE"));
		if (param.get("SEARCH_CONTENTS") != null && !"".equals(param.get("SEARCH_CONTENTS"))) {
			param.put("SEARCH_CONTENTS", "%" + param.get("SEARCH_CONTENTS") + "%");
		}
		return mapper.selectProcErrorLog(param);
	}

	/**
	 * 프로시저 에러 로그 카운트
	 * 
	 * @param param
	 * @return
	 */
	public int selectProcErrorLogCount(Map<String, Object> param) {
		param.put("PAGE_INDEX", (((int) param.get("PAGE_INDEX")) - 1) * 10);
		if (param.get("SEARCH_CONTENTS") != null && !"".equals(param.get("SEARCH_CONTENTS"))) {
			param.put("SEARCH_CONTENTS", "%" + param.get("SEARCH_CONTENTS") + "%");
		}
		return mapper.selectProcErrorLogCount(param);
	}

	/**
	 * 유저 사용기록 조회
	 * 
	 * @param params
	 * @return
	 */
	public List<Map<String, Object>> retrieveActionLog(Map<String, Object> params) {
		Map<String, Object> logOutMap = new HashMap<String, Object>();
		List<Map<String, Object>> resultList = mapper.retrieveActionLog(params);

		if (resultList.size() < 1)
			return resultList;

		// TYPE: 1 데이터에 대하여 END_DT 존재시 로그아웃 row 추가 생성
		for (int i = 0; i < resultList.size(); i++) {
			if ("1".equals(resultList.get(i).get("TYPE").toString()) && resultList.get(i).containsKey("END_DT")) {
				if (!StringUtils.isEmpty(resultList.get(i).get("END_DT").toString())) {
					logOutMap.putAll(resultList.get(i));
					logOutMap.put("CONECT_DT", logOutMap.get("END_DT"));
					logOutMap.put("MENU_NM", "로그아웃");
					resultList.add(logOutMap);
					break;
				}
			}
		}
		return resultList;
	}

	/**
	 * 사용자 활동 이력 excel data 생성
	 * 
	 * @param params
	 * @return
	 */
	public Map<String, Object> retrieveAccessLogForExcel(Map<String, Object> params) {
		String viewType = (String) params.get("VIEW_TYPE");
		String ST_DT = "";
		String ED_DT = "";
		String second = ":00";

		Map<String, Object> codeParam = new HashMap<>();

		// 조회 시작일, 종료일 지정시 해당 구간으로 조회
		if (!StringUtils.isEmpty((String) params.get("ST_DT")) && !StringUtils.isEmpty((String) params.get("ED_DT"))) {
			ST_DT = String.valueOf(params.get("ST_DT"));
			ED_DT = String.valueOf(params.get("ED_DT"));

			// 조회일 미지정 시 현재시간부터 1주일 전까지 조회 (VIEW에서 필수값으로 처리하고 있음)
		} else {
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

			ED_DT = sdf.format(calendar.getTime());
			calendar.add(Calendar.DATE, -1);
			ST_DT = sdf.format(calendar.getTime());
		}

		params.put("ST_DT", ST_DT + second);
		params.put("ED_DT", ED_DT + second);

		// 검색조건 설정
		if (!StringUtils.isEmpty((String) params.get("SEARCH_CONTENTS"))) {
			codeParam.put("GRP_CD", "HIST_SEARCH_TY");
			codeParam.put("CD", params.get("SEARCH_TY"));
			params.put("SEARCH_TY", commonService.getCdNm(codeParam));
			params.put("SEARCH_CONTENTS", "%" + (String) params.get("SEARCH_CONTENTS") + "%");
		}

		List<Map<String, Object>> resultList = null;
		Map<String, Object> dataBody = new HashMap<String, Object>();

		// viewType - [U]:사용자별 수행이력 || [P]:프로젝트별 수행이력
		if ("U".equals(viewType)) {
			resultList = mapper.retrieveAccessLogForExcel(params);

			dataBody.put("fileName", "USER_ACCESS_LOG_[" + ST_DT + second + " - " + ED_DT + second + "]");

		} else if ("P".equals(viewType)) {
			String aliasString = (String) params.get("PROJECT_NUM");
			String[] ALIAS_ARR = aliasString.split(",");
			params.put("ALIAS_ARR", ALIAS_ARR);
			resultList = mapper.retrieveAccessLogByProjectForExcel(params);

			dataBody.put("fileName", "PROJECT_ACCESS_LOG");
		}

		String[] codeList = new String[] { "001", "025", "013", "027", "017", "022", "023", "010", "003", "004", "005",
				"006" };
		String[] viewTitles = new String[codeList.length];
		for (int ci = 0; ci < codeList.length; ci++) {
			viewTitles[ci] = messageSource.getMessage("logManagement." + codeList[ci], null, Locale.KOREA);
		}
		String[] setTitles = new String[] { "CONECT_DT", "ALIAS_NO", "PRJCT_NM", "JOIN_FLAG", "MENU_NM", "QRY_ID",
				"EXECUT_SQL", "RESULT_CNT", "USR_ID", "USR_NM", "CONECT_IP", "CONECT_WBSR" };

		// HEADER 정보 세팅
		List<Object> headList = new ArrayList<>();
		for (int i = 0; i < viewTitles.length; i++) {
			Map<String, Object> headCell = new HashMap<String, Object>();
			headCell.put("title", viewTitles[i]);
			headCell.put("name", setTitles[i]);
			headList.add(headCell);
		}

		dataBody.put("header", headList);
		dataBody.put("body", resultList);
		return dataBody;
	}

	public List<Map<String, Object>> getSystemCodeList(Map<String, Object> param) {
		return mapper.selectSystemCodeList(param);
	}

	public int saveSystemCodeList(List<Map<String, Object>> codeList) {

		for (Map<String, Object> codeData : codeList) {
			mapper.updateSystemCodeList(codeData);
		}

		return 0;
	}

}
