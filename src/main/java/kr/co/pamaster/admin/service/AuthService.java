package kr.co.pamaster.admin.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.pamaster.admin.mapper.AuthMapper;

/**
 * @date 2021. 3. 24.
 * @author 이민구
 * @name kr.co.lagomsoft.service.AuthService
 * @comment 
 * 메뉴 및 권한 관련 서비스
 */

@Service
public class AuthService {
    
    @Autowired
    AuthMapper mapper;
    
	/*해당 권한에 대한 프로젝트 상태 권한 획득*/
	public List<Map<String, Object>> selectRoleProStep(Map<String, Object> param) {
		Map<String,Object> paramMap = new HashMap<String,Object>();		
		paramMap.put("ROLE_CD", param.get("ROLE_CD").toString());

		return mapper.selectRoleProStep(param);
	}
	/*해당 권한에 대한 프로젝트 상태 권한 삽입*/
	public int insertRoleProStep(Map<String, Object> param) {
		Map<String,Object> paramMap = new HashMap<String,Object>();		
		paramMap.put("USR_NO", param.get("USR_NO").toString());
		paramMap.put("ROLE_CD", param.get("ROLE_CD").toString());
		paramMap.put("STEP_CD",param.get("STEP_CD").toString());
		paramMap.put("AUTH", param.get("AUTH").toString());
		
		return mapper.insertRoleProStep(param);
	}
	/*해당 권한에 대한 프로젝트 상태 권한 삭제*/
	public int deleteRoleProStep(Map<String, Object> param) {
		Map<String,Object> paramMap = new HashMap<String,Object>();
		paramMap.put("ROLE_CD", param.get("ROLE_CD").toString());		
		return mapper.deleteRoleProStep(param);
	}
	
	/* 메뉴 목록 상위 목록들 획득*/
	public List<Map<String,Object>> getMenuNavList(String menuUrl){
		return mapper.getMenuNavList(menuUrl);
	}
	
	/* 메뉴 목록 획득 */
	public List<Map<String,Object>> getMenuList(){
		return mapper.getMenuList();
	}
	/* 해당 사용자의 메뉴 권한 획득*/
	public List<Map<String,Object>> getMenuRoleList(String roleCd){
		return mapper.getMenuRoleList(roleCd);		
	}
	/* 메뉴 권한 목록 획득*/
	public List<Map<String,Object>> getAuthMenuList(){
		return mapper.getAuthMenuList();
	}
	/* 메뉴 아이디 체크*/
	public List<Map<String,Object>> getMenuId(Map<String,Object> param){
		return mapper.getMenuId(param);
	}
	
	/* 메뉴 및 사용자의 메뉴 권한 단순 등록/삭제/수정*/
	public int insertMenuInfo(Map<String,Object> param) {
		return mapper.insertMenuInfo(param);
	}
	
	public int deleteMenuInfo(int seq) {
		return mapper.deleteMenuInfo(seq);
	}
	
	public int updateMenuInfo(Map<String,Object> param) {
		return mapper.updateMenuInfo(param);
	}
	/* 자식 메뉴 업데이트*/
	public int updateMenuChildren(Map<String,Object> param) {
		return mapper.updateMenuChildren(param);
	}
	/* 자신 및 자신외의 메뉴 업데이트*/
	public int updateOtherMenu(Map<String,Object> param) {
		return mapper.updateOtherMenu(param);
	}
	/* 메뉴 및 자식 삭제*/
	public int deleteMenuList(Map<String,Object> param) {
		return mapper.deleteMenuList(param);
	}
	/* 메뉴 및 자식 롤 삭제*/
	public int deleteMenuRoleList(Map<String,Object> param) {
		return mapper.deleteMenuRoleList(param);
	}
	
	/* 자신 외 업데이트*/
	public int updateMenuSeq(Map<String,Object> param) {
		return mapper.updateMenuSeq(param);
	}
	
	/* 해당 권한 모든 메뉴권한 삭제*/
	public int deleteAllMenuRole(Map<String,Object> param) {
		return mapper.deleteAllMenuRole(param);
	}
	/* 메뉴 권한 삽입 */
	public int insertMenuRole(Map<String,Object> param) {
		return mapper.insertMenuRole(param);
	}
	
	/* 하휘 메뉴 권한 삽입*/
	public int insertMenuRoleChild(Map<String,Object> param) {
		return mapper.insertMenuRoleChild(param);
	}
	
	public int deleteMenuRole(Map<String,Object> param) {
		return mapper.deleteMenuRole(param);
	}
	
	public int updateMenuRole(Map<String,Object> param) {
		return mapper.updateMenuRole(param);
	}
	
	/*권한 획득*/
	public List<Map<String,Object>> getMenuAuthInfo(Map<String,Object> param) {
		return mapper.getMenuAuthInfo(param);
	}
	
	/*메뉴 재정렬*/
	public int initMenuSeq(Map<String,Object> param) {
		List<Map<String,Object>> seqList = (List<Map<String,Object>>)param.get("seqList");
		
		int initCnt = 0;
		
		if(seqList != null) {
			
			for (Map<String,Object> seqMap : seqList) {
				
				initCnt += mapper.initMenuSeq(seqMap);
				
			}
			
		}
		
		return initCnt;
	}
}
