package kr.co.pamaster.vo;

import java.text.SimpleDateFormat;
import java.util.List;

public class Member {
	
	// USR_NO
	private int usr_no = 0;
	
	// USER_ID
	private String usr_id;
	
	// USER_PASSWORD
	private String usr_pw;
	private String usrPwConfirm;
	
	// USER_NAME
	private String usr_nm;
	
	// USER_EMAIL
	private String usr_email;

	// PhoneNumber
	private String cnt_frs;
	private String cnt_mdl;
	private String cnt_end;

	// PASSWORD_INITIAL_CONFIRM
	private String pw_initl_yn = "Y";
	
	// PASSWORD_INITIAL_DATE
	private String pw_initl_dt;
	
	// ROLE_CODE
	private List<String> roleCdList = null;
	
	// GROUP_CODE
	private String gp_cd;
	// GROUP_NM
	private String gp_nm;	

	private String lock_yn;
	
	private String approval_yn;
	
	private String offm_telno;
	
	private String usr_rspofc;
	
	private String usr_rspofc_nm;
	
	//기업명을 사용하는 경우 회사명과 부서명을 따로 기록
	private String gp_company_nm;
	private String gp_new_nm;
	
	public String getGp_company_nm() {
		return gp_company_nm;
	}

	public void setGp_company_nm(String gp_company_nm) {
		this.gp_company_nm = gp_company_nm;
	}

	public String getGp_new_nm() {
		return gp_new_nm;
	}

	public void setGp_new_nm(String gp_new_nm) {
		this.gp_new_nm = gp_new_nm;
	}

	public String getUsr_email() {
		return usr_email;
	}
	
	SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	
	public void setUsr_email(String usr_email) {
		this.usr_email = usr_email;
	}

	public String getOffm_telno() {
		return offm_telno;
	}

	public void setOffm_telno(String offm_telno) {
		this.offm_telno = offm_telno;
	}

	public String getApproval_yn() {
		return approval_yn;
	}

	public void setApproval_yn(String approval_yn) {
		this.approval_yn = approval_yn;
	}

	public int getUsr_no() {
		return usr_no;
	}

	public void setUsr_no(int usr_no) {
		this.usr_no = usr_no;
	}

	public String getUsr_id() {
		return usr_id;
	}

	public void setUsr_id(String usr_id) {
		this.usr_id = usr_id;
	}

	public String getUsr_pw() {
		return usr_pw;
	}

	public void setUsr_pw(String usr_pw) {
		this.usr_pw = usr_pw;
	}
	
	public void setUsrPwConfirm(String usrPwConfirm) {
		this.usrPwConfirm = usrPwConfirm;
	}
	
	public String getUsrPwConfirm() {
		return usrPwConfirm;
	}

	public String getUsr_nm() {
		return usr_nm;
	}

	public void setUsr_nm(String usr_nm) {
		this.usr_nm = usr_nm;
	}

	public String getCnt_frs() {
		return cnt_frs;
	}

	public void setCnt_frs(String cnt_frs) {
		this.cnt_frs = cnt_frs;
	}

	public String getCnt_mdl() {
		return cnt_mdl;
	}

	public void setCnt_mdl(String cnt_mdl) {
		this.cnt_mdl = cnt_mdl;
	}

	public String getCnt_end() {
		return cnt_end;
	}

	public void setCnt_end(String cnt_end) {
		this.cnt_end = cnt_end;
	}

	public String getPw_initl_yn() {
		return pw_initl_yn;
	}

	public void setPw_initl_yn(String pw_initl_yn) {
		this.pw_initl_yn = pw_initl_yn;
	}
	
	public List<String> getRoleCdList() {
		return roleCdList;
	}
	
	public void setRoleCdList(List<String> roleCdList) {
		this.roleCdList = roleCdList;
	}

	public String getGp_cd() {
		return gp_cd;
	}

	public void setGp_cd(String gp_cd) {
		this.gp_cd = gp_cd;
	}

	public String getLock_yn() {
		return lock_yn;
	}

	public void setLock_yn(String lock_yn) {
		this.lock_yn = lock_yn;
	}

	public String getUsr_rspofc() {
		return usr_rspofc;
	}

	public void setUsr_rspofc(String usr_rspofc) {
		this.usr_rspofc = usr_rspofc;
	}
	
	public String getGp_nm() {
		return gp_nm;
	}

	public void setGp_nm(String gp_nm) {
		this.gp_nm = gp_nm;
	}

	public String getUsr_rspofc_nm() {
		return usr_rspofc_nm;
	}

	public void setUsr_rspofc_nm(String usr_rspofc_nm) {
		this.usr_rspofc_nm = usr_rspofc_nm;
	}

	public String getPw_initl_dt() {
		return pw_initl_dt;
	}

	public void setPw_initl_dt(String pw_initl_dt) {
		
		this.pw_initl_dt = pw_initl_dt;
	}

}
