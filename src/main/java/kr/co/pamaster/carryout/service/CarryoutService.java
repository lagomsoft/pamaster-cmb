package kr.co.pamaster.carryout.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.pamaster.carryout.mapper.CarryoutMapper;
import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.util.PAMsException;

/**
 * @date 2021. 4. 20.
 * @author 이민구
 * @name kr.co.lagomsoft.service.CarryoutService
 * @comment 반출 관련 서비스
 */
@Service
public class CarryoutService {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CarryoutMapper carryoutMapper;
	@Autowired
	private CommonService commonService;

	public int insertPostManagement(Map<String, Object> param) {
		return carryoutMapper.insertPostManagement(param);
	}

	public Map<String, Object> getProjectCarryoutInfo(Map<String, Object> param) {
		
		if("J".equals(param.get("DATA_TY"))){
			
			return carryoutMapper.getJoinProjectCarryoutInfo(param);
			
		} else {
			return carryoutMapper.getProjectCarryoutInfo(param);
		}
	}

	public List<Map<String, Object>> selectPostManagement(Map<String, Object> param) {
		if("ALL".equals(param.get("PRJCT_NO"))) {
			param.remove("PRJCT_NO");
		}
		return carryoutMapper.selectPostManagement(param);
	}

	//public int insertPostManagement(MultipartHttpServletRequest request) throws PAMsException {
		//첨부파일 처리				
		//Object fileID = commonService.saveFils(request, request.getParameter("PRJCT_NO"), "610");
		
	//	Map<String,Object> param = new HashMap<String,Object>();
	//	param.put("PRJCT_NO",request.getParameter("PRJCT_NO"));
	//	param.put("USR_NO", commonService.getLoginUserNo());
	//	param.put("MG_NO", request.getParameter("MG_NO"));
	//	param.put("MG_TYPE", request.getParameter("MG_TYPE"));
	//	param.put("MG_DE", request.getParameter("MG_DE"));
	//	param.put("MG_GOV", request.getParameter("MG_GOV"));
	//	param.put("INFO", request.getParameter("INFO"));
	//	param.put("DES_YN", request.getParameter("DES_YN"));
		//param.put("FL_ID", fileID.toString());
		
	//	return carryoutMapper.insertPostManagement(param);
//	}

	public int deletePostManagement(Map<String, Object> param) {
		return carryoutMapper.deletePostManagement(param);
	}
	
	public Object selectCarryoutNo(Map<String, Object> param) {
	    return carryoutMapper.selectCarryoutNo(param);
	}
	
	public Map<String,Object> insertCarryoutInfo(Map<String, Object> param) {
		
		Map<String,Object> resultMap = new HashMap<String,Object>();

		Object carryoutNo = selectCarryoutNo(param);
		param.put("CARRYOUT_NO", carryoutNo);
		param.put("USR_NO", commonService.getLoginUserNo());
		
		carryoutMapper.insertCarryoutInfo(param);
		carryoutMapper.insertCarryoutFileData(param);
		
		//자동파기 일시 저장
		if( "".equals(param.get("AT_DSTRC_DE")) ) {
		    param.put("AT_DSTRC_DE", null);
		}
		carryoutMapper.updateAutoDestructionDate(param);
		
		HttpSession session = commonService.getLoginSession();
		session.setAttribute("INDVDLINFO_CHECK_Y", "Y");
		
		resultMap.put("result", "success");
		
		return resultMap;
	}


	public int updateCarryoutFileInfo(Map<String, Object> param) {
		return carryoutMapper.updateCarryoutFileInfo(param);
	}

	
	public List<Map<String, Object>> selectCarryoutInfo(Map<String, Object> param) {
		return carryoutMapper.selectCarryoutInfo(param);
	}

	
	
	public Map<String, Object> selectCarryout(Map<String, Object> param) {
		if("JOIN".equals(param.get("PRJCT_TY"))){
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("JOIN_LIST", carryoutMapper.selectJoinProjectList(param));
			return paramMap;
		} else {
			return carryoutMapper.selectCarryout(param);
		}
		
	}

	/**처리 후 STEP_CD 리턴
	 * @param param
	 * @throws PAMsException
	 * @return String (STEP_CD)
	 */
	@Transactional(rollbackFor=Exception.class)
	public String insertCarryoutJudge(Map<String, Object> param) throws PAMsException { 
		
		
		String stepCd = null;
		boolean isBind = false;
		if("JOIN".equals(param.get("PROJECT_TY").toString())) {
			isBind = true;
		}
		//재실행시에만
		if(param.get("IS_RE")!=null && "Y".equals(param.get("IS_RE"))) {
			//최근 승인내역을 획득하여 stepCd 확인
			Map<String,Object> exParamMap = new HashMap<String,Object>();
			exParamMap.put("PRJCT_NO",param.get("PRJCT_NO").toString());
			Map<String,Object> exMap = carryoutMapper.getProjectCarryoutLatestInfo(exParamMap);
			String sttCd = exMap.get("PRCS_STT_CD").toString();
			if(isBind) {
				switch(sttCd) {
					case "100": //승인
						stepCd 	= "625";					
						break;
					case "200"://조건부승인
						stepCd 	= "626";					
						break;
					case "900"://반려
						stepCd 	= "470";
						break;				
				}
			} else {
				switch(sttCd) {
					case "100": //승인
						stepCd 	= "620";					
						break;
					case "200"://조건부승인
						stepCd 	= "621";					
						break;
					case "900"://반려
						stepCd 	= "370";
						break;				
				}
			}
			
			exParamMap.put("USER_NO", param.get("USR_NO").toString());
			exParamMap.put("STEP_CD", stepCd);				//
			exParamMap.put("PROJECT_NO",param.get("PRJCT_NO").toString());

			commonService.updateProjectStep(exParamMap);

			//반려가 아닐때만 스케줄 등록
			if(!("370".equals(stepCd) || "470".equals(stepCd))) {
				/*파일 반출 스케줄 등록*/
				if(isBind) {
					
					//commonService.requestScheduleProcessing(param.get("PRJCT_NO"), "625_ISBIND", true);
				} else {
					//commonService.requestScheduleProcessing(param.get("PRJCT_NO"), "620", true);
				}
				/*파일 압축 스케줄 등록*/
			}
			
			return stepCd;
		}
		
		/*
		 * 코드 : 600 (반출) / 610 (반출신청) / 620 / 700 활용 및 사후처리 370(반출 반려)
		 * */		
		String judgeCd = param.get("JUDGE_CD").toString();		
		String sttCd = null;
		
		if(isBind) {
			switch(judgeCd) {
				case "1": //승인
					stepCd 	= "625";
					sttCd 	= "100";
					break;
				case "2"://조건부승인
					stepCd 	= "626";
					sttCd	= "200";
					break;
				case "3"://반려
					stepCd 	= "470";	
					sttCd	= "900";
					break;				
			}
		} else {
			switch(judgeCd) {
				case "1": //승인
					stepCd 	= "620";
					sttCd 	= "100";
					break;
				case "2"://조건부승인
					stepCd 	= "621";
					sttCd	= "200";
					break;
				case "3"://반려
					stepCd 	= "370";	
					sttCd	= "900";
					break;				
			}
		}
		
		
		Map<String,Object> paramMap = new HashMap<String,Object>();
		
		paramMap.put("PRJCT_NO",param.get("PRJCT_NO").toString());
		paramMap.put("STEP_CD","600"); //검토시 반출 상태 고정삽입
		paramMap.put("USR_NO",param.get("USR_NO").toString());		
		paramMap.put("PRCS_STT_CD",sttCd);		
		paramMap.put("COMMENT",param.get("COMMENT").toString());
		paramMap.put("PROJECT_TY",param.get("PROJECT_TY").toString());
		
		/* 검토 의견 등록*/
		int result = carryoutMapper.insertCarryoutJudge(paramMap);
		if(result <= 0 ) {
			/*정상 등록 되지 않았을 시 롤백*/
			throw new PAMsException("반출 승인 중 오류가 발생하였습니다.");
		}else {
			/* 프로젝트 상태 변경*/
			paramMap.clear();
			paramMap.put("USER_NO", param.get("USR_NO").toString());
			paramMap.put("STEP_CD", stepCd);				//
			paramMap.put("PROJECT_NO",param.get("PRJCT_NO").toString());

			commonService.updateProjectStep(paramMap);
			
			
			//반려가 아닐때만 스케줄 등록
			if(!("370".equals(stepCd) || "470".equals(stepCd))) {				
				/*파일 반출 스케줄 등록*/
				if(isBind) {
					//commonService.requestScheduleProcessing(param.get("PRJCT_NO"), "625_ISBIND", true);
				} else {
					//commonService.requestScheduleProcessing(param.get("PRJCT_NO"), "620", true);
				}
				/*파일 압축 스케줄 등록*/
			}			
		}
		
		return stepCd;
	}
	
	/**반출 파일 다운로드 정보 등록
	 * @param param
	 * @return
	 * @return int
	 */
	public int insertCarryoutFileInfo(Map<String,Object> param) {
	    /*
		Map<String,Object> paramMap  = new HashMap<String,Object>();
		
		paramMap.put("PRJCT_NO", param.get("PRJCT_NO"));
		paramMap.put("CARRYOUT_NO", param.get("CARRYOUT_NO").toString());
		paramMap.put("FL_ID", param.get("FL_ID"));
		paramMap.put("FL_SN", param.get("FL_SN"));		
		paramMap.put("FL_CAUSE", param.get("FL_CAUSE").toString());
		paramMap.put("USR_NO", param.get("USR_NO"));
		*/
		return carryoutMapper.insertCarryoutFileInfo(param);
		
	}

	
	
	/**프로젝트 파기
	 * @param param
	 * @return Map<String,Object>
	 * @throws PAMsException 
	 */
	public Map<String,Object> destruction(Map<String,Object> param) throws PAMsException{
		
		Map<String,Object> resultMap = new HashMap<String,Object>(),
					paramMap = new HashMap<String,Object>();
		try {
			/* 프로젝트 상태 변경*/
			paramMap.clear();
			paramMap.put("USER_NO", param.get("USR_NO").toString());
			
			boolean isBind = false;
			if("JOIN".equals(param.get("PRJCT_TY").toString())) {
				isBind = true;
			}
			
			/* 일반프로젝트 */
			paramMap.put("STEP_CD", "710");				//
			paramMap.put("PROJECT_NO",param.get("PRJCT_NO").toString());
			commonService.updateProjectStep(paramMap);
			
			/*스케줄 등록*/
			//commonService.requestScheduleProcessing(param.get("PRJCT_NO"), "710", false);
			
			
		}catch(Exception ex) {
			log.error(ex.getLocalizedMessage(),ex.getMessage());
			throw new PAMsException("파기 처리중 오류가 발생하였습니다.");
		}
		
		resultMap.put("result","success");
		
		return resultMap;
	}
	
	/**
	 * 결합 프로젝트의 하위프로젝트 리스트
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> selectJoinProjectList(Map<String, Object> param){

		List<Map<String, Object>> returnMap = carryoutMapper.selectJoinProjectList(param);
		
		return returnMap;
	}
	
}
