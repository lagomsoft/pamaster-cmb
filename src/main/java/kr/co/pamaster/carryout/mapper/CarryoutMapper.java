/**
 * 
 */
package kr.co.pamaster.carryout.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

/**
 * @date 2021. 4. 20.
 * @author 이민구
 * @name kr.co.lagomsoft.mapper.CarryoutMapper
 * @comment 반출 관련 Mapper
 */
@Mapper
public interface CarryoutMapper {

	public Map<String,Object> selectCarryout(Map<String,Object> param);
	
	public int insertCarryoutJudge(Map<String,Object> param);
	
	public Object selectCarryoutNo(Map<String, Object> param);
	
	public int insertCarryoutInfo(Map<String,Object> param);
	
	public int insertCarryoutFileData(Map<String,Object> param);
	
	public int insertCarryoutFileInfo(Map<String,Object> param);
	
	public void updateAutoDestructionDate(Map<String,Object> param);
	
	public List<Map<String,Object>> selectCarryoutInfo(Map<String,Object> param);
	
	public int updateCarryoutFileInfo(Map<String,Object> param);
	
	public List<Map<String,Object>> selectPostManagement(Map<String,Object> param);
	
	public int insertPostManagement(Map<String,Object> param);
	
	public int deletePostManagement(Map<String,Object> param);
	
	public Map<String,Object> getProjectCarryoutInfo(Map<String,Object> param);
	
	public Map<String,Object> getJoinProjectCarryoutInfo(Map<String,Object> param);
	
	public Map<String,Object> getProjectCarryoutLatestInfo(Map<String,Object> param);	
	
	public List<Map<String, Object>> selectJoinProjectList(Map<String, Object> param);

}
