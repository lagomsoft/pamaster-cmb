/**
 * 
 */
package kr.co.pamaster.interceptor;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import kr.co.pamaster.admin.service.AuthService;

/**
 * @date 2021. 6. 7.
 * @author 이민구
 * @name kr.co.lagomsoft.interceptor.MenuInfoInterCeptor
 * @comment 메뉴 정보만을 가져오는 InterCeptor
 */
@Component
public class MenuInfoInterCeptor implements HandlerInterceptor {
	
	@Autowired
	AuthService authService;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String requestUrl = request.getRequestURI().toString();	
		HttpSession session = request.getSession();
		
		StringBuffer menuUrl = new StringBuffer()
				.append(requestUrl);	
		
		/* 메뉴 정보 획득 */	
		List<Map<String,Object>> menuInfo = authService.getMenuNavList(menuUrl.toString());
		session.setAttribute("MENU_INFO", menuInfo);
		if(menuInfo!= null && menuInfo.size() >0) {
			Map<String,Object> nowMenu = menuInfo.get(menuInfo.size()-1);
			if("2".equals(nowMenu.get("MENU_TYPE").toString())) {
				//메뉴의 타입이 페이지일 경우
				session.setAttribute("PAGE", nowMenu.get("SEQ"));
			}
		}
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}
}
