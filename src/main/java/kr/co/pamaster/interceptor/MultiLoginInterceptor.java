/**
 * 
 */
package kr.co.pamaster.interceptor;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;

import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.user.service.UserService;

/**
 * @date 2021. 3. 23.
 * @author 이민구
 * @name kr.co.lagomsoft.interceptor.MultiLoginInterceptor
 * @comment 
 * 중복 로그인 체크
 */
@Component
public class MultiLoginInterceptor implements HandlerInterceptor {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CommonService commonService;
	
	@Value("${pams.login.multi}")
	String multiYn;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		/* application.properties의 pams.login.multi 값이 Y일경우 처리 하지 않음.*/		
		if(multiYn != null && multiYn.equals("Y")) {
			return HandlerInterceptor.super.preHandle(request, response, handler);
		}
		
		/* 증복 로그인 예외 처리*/		
		String requestUrl = request.getRequestURI().toString();	
		
		if(!requestUrl.contains(".") && request.getSession().getAttribute("USR_NO") != null) {
			Map<String,Object> loginInfo = 
					userService.getLastLoginHst(request.getSession().getAttribute("USR_NO").toString());
			
			String connectIp = request.getRemoteAddr();
			String connectWbsr = commonService.getBrowser(request);
			
			/* 현재 접속 아이피와 브라우저 정보 중 둘중 하나라도 다를 경우 중복 로그인으로 체크
			 * 규칙 변경시 하단 조건문 변경.
			 * */
			if(!((String) loginInfo.get("CONECT_IP")).equals(connectIp) || !loginInfo.get("CONECT_WBSR").equals(connectWbsr)) {
				//접속 아이피와 접속 브라우저 정보가 다를경우
				SecurityContextHolder.clearContext();	
				request.getSession().invalidate();
				throw new ModelAndViewDefiningException(new ModelAndView("user/multiLogin"));
			}
		}
		
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}
	
}
