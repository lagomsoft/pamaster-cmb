/**
 * 
 */
package kr.co.pamaster.interceptor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.ModelAndViewDefiningException;

import kr.co.pamaster.admin.service.AuthService;
import kr.co.pamaster.common.service.CommonService;

/**
 * @date 2021. 3. 23.
 * @author 이민구
 * @name kr.co.lagomsoft.interceptor.AuthInterceptor
 * @comment 
 * 권한 관련 Interceptor 및 권한 획득 시 메뉴 정보 획득
 * : 적용 권한 룰 - 메뉴 미등록 또는 권한 추가되지 않은 메뉴 권한 체크 하지 않음. 
 * 				시스템 관리자 권한 체크 하지 않음 
 * 				페이지 타입 메뉴의 경우 
 */
@Component
public class AuthInterceptor implements HandlerInterceptor {
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private AuthService authService;
	
	@Value("${pams.login.url:}")
	String loginUrl;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		HttpSession session = request.getSession();
		
		/*접근 권한 체크 유무.*/
		boolean isAuthCheck = false;
		boolean isValid = false;
		
		String requestUrl = request.getRequestURI().toString();		
		
		/*로그인한 사용자이지만 해당 사용자가 초기화 비밀번호를 변경하지 않았을 경우*/
		if(session.getAttribute("USR_NO") != null && "Y".equals(session.getAttribute("PW_INITL_YN"))
				&&!requestUrl.contains("/error")&&!requestUrl.contains("/user/")&&!requestUrl.contains(".")) {
			response.sendRedirect("/user/changePasswdCheck");  
			return false;
		}
		if(session.getAttribute("USR_NO") != null && "Y".equals(session.getAttribute("PW_INITL_YN"))
				&&requestUrl.contains("/user/myInfo")) {
			response.sendRedirect("/user/changePasswdCheck");
			return false;
		}

		/* 주소 권한 체크 후 권한 없을시 권한없음 페이지로 이동*/
		if(isAuthCheck) {
			
			/* 로그인 유무 체크*/
			if(session.getAttribute("USR_NO") == null) {
				/* 미 로그인시 로그인 페이지로 이동*/
				response.sendRedirect("/"); 
				return false;
			}
		}
			
		if(isAuthCheck) {
			List<String> roleList = (List<String>)session.getAttribute("ROLE_LIST");
			/*ROLE_SYS_ADMIN 일 경우 전체 권한*/			
			if(roleList.contains("ROLE_SYS_ADMIN")) {
				session.setAttribute("IS_SYS_ADMIN","Y");
				isAuthCheck = false;
			}else {
				session.setAttribute("IS_SYS_ADMIN","N");
			}	
		}	
		
		StringBuffer menuUrl = new StringBuffer()
				.append(requestUrl);	
		
		if(isAuthCheck) {			
			
			
			/* 로그인 시 권한 정보 획득*/
			Map<String,Object> paramMap = new HashMap<String,Object>();
			paramMap.put("USR_NO", session.getAttribute("USR_NO"));
			paramMap.put("MENU_URL", menuUrl.toString());
			List<Map<String,Object>> resultList = authService.getMenuAuthInfo(paramMap);
			Map<String,Object> result = null;
			if(resultList != null) {
				if(resultList.size() == 1) {
					//값이 하나만 존재할때
					result = resultList.get(0);
				}else {
					try {
						String page = session.getAttribute("PAGE").toString();
						for(Map<String,Object> temp : resultList) {
							if(page.equals(temp.get("UPPER_MENU_SEQ").toString())) {
								result = temp;
								break;
							}
						}
					}catch(Exception ex) {
						
					}
				}				
			}
			
			/*해당 메뉴가 필수 권한이 없을 경우 권한체크 하지 않음.*/			
			if(result != null) {
				String[] authList = {};
				if(result.get("AUTH_CD") != null) {					
					session.setAttribute("AUTH_CD",result.get("AUTH_CD").toString());
					authList = result.get("AUTH_CD").toString().split(",");
				}
				/*해당 메뉴에 적용된 권한이 있을경우*/
				if(result.get("VALID_AUTH_CD") !=null && !"".equals(result.get("VALID_AUTH_CD"))) {
					String validAuth = result.get("VALID_AUTH_CD").toString();								
					for(String authCd : authList) {
						if(authCd.equals(validAuth)) {
							isValid = true;
							break;
						}
					}
				
				}else {/* 메뉴는 등록되어 있으나 권한이 설정되어 있지 않을경우 권한 체크하지 않음.*/
					isValid = true;
				}		
			}else {
				/*등록되어 있지 않은 메뉴는 권한 체크 하지 않음*/
				isValid = true;
			}
			/* 권한이 없을 시 권한 없음 페이지로 이동*/
			if(!isValid) {
				ModelAndView errorModel = new ModelAndView("error/no_auth");
				errorModel.setViewName("error/no_auth");
				throw new ModelAndViewDefiningException(errorModel);
			}
		}
		/* 메뉴 정보 획득 */
		/* MenuInterCeptor 로 이동
		List<Map<String,Object>> menuInfo = authService.getMenuNavList(menuUrl.toString());
		session.setAttribute("MENU_INFO", menuInfo);
		if(menuInfo!= null && menuInfo.size() >0) {
			Map<String,Object> nowMenu = menuInfo.get(menuInfo.size()-1);
			if("2".equals(nowMenu.get("MENU_TYPE").toString())) {
				//메뉴의 타입이 페이지일 경우
				session.setAttribute("PAGE", nowMenu.get("SEQ"));
			}
		}
		*/
				
		return HandlerInterceptor.super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
	}
}
