package kr.co.pamaster.interceptor.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;


@Service
public class InterceptorService {

    public String getRequestPageUrl(HttpServletRequest req) {

        String tagetUrl = null;

        if (req != null) {
            String method = req.getMethod();

            if ("GET".equals(method) || "get".equals(method)) {

                tagetUrl = req.getRequestURI();
            } else if ("POST".equals(method) || "post".equals(method)) {

                String fullUrl = req.getHeader("REFERER");
                String host = req.getHeader("HOST");
                if (fullUrl != null && host != null) {
                    String[] urls = fullUrl.split(host);

                    if (urls != null && urls.length == 2) {
                        tagetUrl = urls[1];
                    }
                }

            }

        }

        return tagetUrl;
    }
    
    public String getBrowser(HttpServletRequest request) {
        String header = request.getHeader("User-Agent");

        if (header != null) {
            if (header.indexOf("Trident") > -1) {
                return "IE";
            } else if (header.indexOf("Edg") > -1) {
                return "Edge";
            } else if (header.indexOf("Chrome") > -1) {
                return "Chrome";
            } else if (header.indexOf("Opera") > -1) {
                return "Opera";
            } else if (header.indexOf("iPhone") > -1 && header.indexOf("Mobile") > -1) {
                return "iPhone";
            } else if (header.indexOf("Android") > -1 && header.indexOf("Mobile") > -1) {
                return "Android";
            } else if (header.indexOf("Firefox") > -1) {
                return "Firefox";
            } else
                return header;
        }
        return header;
    }

}
