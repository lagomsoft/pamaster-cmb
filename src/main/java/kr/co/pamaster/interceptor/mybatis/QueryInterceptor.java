package kr.co.pamaster.interceptor.mybatis;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

import kr.co.pamaster.interceptor.service.InterceptorService;


/**
 * @date 2021. 4. 6.
 * @author 이민구
 * @name kr.co.lagomsoft.interceptor.mybatis.QueryInterceptor
 * @comment 
 * 쿼리 이력 처리를 위한 Intercept
 */
@Component
@Intercepts({@Signature(type = Executor.class, method = "query", args = {MappedStatement.class,Object.class, RowBounds.class, ResultHandler.class}),
@Signature(type = Executor.class, method = "update", args = {MappedStatement.class,Object.class})})
public class QueryInterceptor implements Interceptor{
	
	@Autowired
	InterceptorService interceptorService;
	
	@Value("${pams.user.connectLog.level}")
	private int connectLogLevel;

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public Object intercept(Invocation invocation) throws Throwable {
		/* 차후 쿼리 처리 시간 필요시 사용
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
        */	
		
        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        String id = mappedStatement.getId();
        Object params = invocation.getArgs()[1];
        
        BoundSql bsql =  mappedStatement.getBoundSql(params);
        StringBuffer sql = new StringBuffer(bsql.getSql());
        
        List<Object> valueList = new ArrayList<Object>();
        if(params != null) {
        	for(ParameterMapping param : bsql.getParameterMappings()) {
        		
        		if (param != null) {
        			
        			String property = param.getProperty(); 
        			Object value = null; 
        			if (bsql.hasAdditionalParameter(property)) { 
        				value = bsql.getAdditionalParameter(property); 
        			} else {     			
        				try {
        					value = PropertyUtils.getProperty(params, property);
        				}catch(Exception e) {
        					value = params;
        				}        				
        			} 
        			
        			valueList.add(value); 
        			
        		}
        		
        	}
        	for (Object value : valueList) { 
        		int start = sql.indexOf("?");
        		int end = start + 1; 
        		if (value == null) { 
        				sql.replace(start, end, "NULL"); 
        		} else if (value instanceof String) {
        			sql.replace(start, end, "'" + value.toString() + "'"); 
        		} else { 
        			sql.replace(start, end, value.toString()); 
        		} 
        	}        	
        }
        
        String queryString = sql.toString();
        boolean isWriteLog = false;        
        
        if(connectLogLevel ==1 || queryString.contains("HISTINFO")) {
        	isWriteLog = true;
        }
        if(queryString.contains("NOTHIST")) {
        	isWriteLog = false;
        }
        
        int resultSize = -1;
        Object proceed = null;
        
        try {
            proceed =  invocation.proceed();
        }finally {
        	/* 차후 쿼리 처리 시간 필요시 사용.
           stopWatch.stop();
           */
        	if(proceed !=null)
        		if(proceed instanceof ArrayList) {
        			resultSize = ((ArrayList)proceed).size();
        		}else {
        			resultSize = (int)proceed;
        		}
        }
        
		
		Object commonObject = null; 

    	if(isWriteLog) {
    		
    		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    		
    		HttpServletRequest request = null;
    				
    		if(requestAttributes != null) {
    			request = ((ServletRequestAttributes)requestAttributes).getRequest();
    		}
    		
    		if(request != null) {
    			
        		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
        		
            	if(wac != null) {
        	    	commonObject    = wac.getBean("commonService");
            	}
            	
            	Map<String, Object> param = new HashMap<>();
            	
            	String uri = interceptorService.getRequestPageUrl(request);
            	
            	String queryId = "";
            	if (id.indexOf(".") > 0) queryId = id.substring(id.lastIndexOf(".")+1);
            	
        		param.put("TYPE"			, "3"); 
        		param.put("CONECT_IP"		, request.getRemoteAddr());
        		param.put("CONECT_WBSR"		, interceptorService.getBrowser(request));
        		param.put("MENU_ID"			, uri);
        		param.put("EXECUT_SQL"		, queryString);
        		param.put("RESULT_CNT"		, resultSize);
        		param.put("QRY_ID"			, queryId);

        		Class[] cClass = new Class[] {Map.class};
        		
    			try {
    					Method m = commonObject.getClass().getMethod("recordSqlHist", cClass);
    					
    					try {
    						m.invoke(commonObject, param);
    					
    					} catch (IllegalAccessException e) {
    						// TODO Auto-generated catch block
    						log.error(" " + e.getCause());
    					} catch (IllegalArgumentException e) {
    						// TODO Auto-generated catch block
    						log.error(" " + e.getCause());
    					} catch (InvocationTargetException e) {
    						// TODO Auto-generated catch block
    						log.error(" " + e.getCause());
    					}				
    					
    			} catch (NoSuchMethodException e) {
    				// TODO Auto-generated catch block
    				log.error(" " + e.getCause());
    			} catch (SecurityException e) {
    				// TODO Auto-generated catch block
    				log.error(" " + e.getCause());
    			}
    		}
    		
    	}
        
        return proceed;
	}
	
}
