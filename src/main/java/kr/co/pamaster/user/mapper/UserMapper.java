package kr.co.pamaster.user.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import kr.co.pamaster.vo.Member;

@Mapper
public interface UserMapper {
    
    public void joinUser(Member member);
    
    public void joinUserRole(Member member);
    
    public void changePw(Member member);
    
    public void changePw(Map<String, Object> param);
    
    public void updateUser(Map<String, Object> param);   
    
    /* 유저 비밀번호 실패 횟수 추가 */
    public void addPwFailCount(Map<String, Object> param);
    
    /* 유저 로그인 성공시 유저 정보 업데이트 */
    public void successLoginUser(Map<String, Object> param);
    
    /* 유저 LOCK */
    public void userLock(Object USR_ID);
    
    /* 유저 최초 권한*/
    public void joinUserInitRole(Map<String, Object> param);
    
    public void updateGroupCD(Map<String,Object> param);    
    
    public int findPw(Map<String, Object> param);
    
    public int getProjectsCount(Map<String, Object> param);    
    
    public int checkUserIDandEmail(Map<String, Object> param);
    
    /* 마이페이지 사용자 정보 수정*/
    public int updateMyInfo(Map<String,Object> param);
    
    /* 마이페이지 사용자 접속 목록 갯수*/
    public int getMyConnectInfoCount(int usrNo);
    
    public int insertGroupInfo(Map<String,Object> param);
    
    public int insertCompanyInfo(Map<String,Object> param);    
    
    public String getUser(Object no);
    
    public String getUserName(Map<String, Object> param);
    
    public String getPassword(String id);
    
    public String findUser(Map<String, Object> param);
    
    public String getCdNm(Map<String, Object> param);
    
    public String checkPassword(Map<String, Object> param);
    
    /* 유저 비밀번호 실패 횟수 체크 */
    public Object getPwFailCountCheck(String USR_ID);    
    
    /* 유저 번호 가져오기*/
    public Object selectUserNo(Map<String, Object> param);
    
    public Object selectProjectDltYn(Map<String, Object> param);
    
    /* 사용자의 마지막 접속 정보 획득*/
    public Map<String,Object> getLastLoginHst(String usrNo);
    
    public Map<String, Object> failTypeCheck();    
    
    public List<String> getRoleList(Object no);
    
    public List<String> getRoleListMapped(Object no);
    
    public List<Map<String, String>> findCnt(Member member);
    
    public List<Map<String, String>> pwFindCnt(Member member);
    
    /* 마이페이지 프로젝트 전체 목록*/
    public List<Map<String, Object>> getProjects(Map<String,Object> param); 
    
    /* 마이페이지 프로젝트 진행중 목록*/
    public List<Map<String, Object>> getProjectsIng();
    
    /* 마이페이지 프로젝트 종료 목록*/
    public List<Map<String, Object>> getProjectsEnd();
    
    public List<Map<String, Object>> getProjectsStep(@Param("no") String no,@Param("step") String step);

    public List<Map<String, Object>> getMember(Map<String, Object> param);
    
    public List<Map<String, Object>> getUserIdList(String USR_ID);    

    /* 마이페이지 사용자 스텝별 갯수*/
    public List<Map<String,Object>> getMyProjectStep(Map<String,Object> param);
    
    /* 마이페이지 사용자 스텝별 프로젝트 목록*/
    public List<Map<String,Object>> getMyProjectList(Map<String,Object> param);
    
    public List<Map<String,Object>> selectCorpGroupList();
    
    public List<Map<String,Object>> selectCorpGroupByName(Map<String,Object> param);
    
    /* 마이페이지 사용자 접속 목록*/
    public List<Map<String,Object>> getMyConnectInfo(Map<String,Object> param);    
    
    public Member getUserInfo(Member member);

    public List<Map<String,Object>> selectCarryoutInfoList(Map<String,Object> param);
}

