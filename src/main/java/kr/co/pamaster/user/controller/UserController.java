package kr.co.pamaster.user.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pamaster.admin.service.AdminService;
import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.user.service.UserService;
import kr.co.pamaster.vo.Member;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CommonService commonService;

    @Autowired
    private AdminService adminService;
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * 로그인
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/login")
    private String login(Model model) {

        return "user/login";
    }

    /**
     * 로그아웃
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/logout")
    private String logout(Model model) {
        
        return "user/login";
    }

    /**
     * 로그인실패
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/denied")
    private String denied(Model model) {
        
        return "user/denied";
    }
    
    /**
     * 로그인실패
     * 
     * @param model
     * @return
     */
    @PostMapping("/user/denied")
    private @ResponseBody Map<String, Object> failTypeCheck(Model model) {
        
        Map<String, Object> returnMap = new HashMap<>();
        return returnMap;
    }
    
    /**
     * 회원가입
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/joinUser")
    private ModelAndView joinUser(Model model) {
        ModelAndView mav = new ModelAndView();
        
        mav.setViewName("user/joinUser");
        mav.addObject("GROUP_LIST", commonService.getGroupList());
        mav.addObject("GROUP_CORP_LIST", userService.getCorpGroupList());
        mav.addObject("RSPOFC_LIST", commonService.getCodeList("RSPOFC_CD"));
        mav.addObject("CHECK_MAIL_USE", commonService.checkMailUse());
        mav.addObject("CHECK_CORP_USE", commonService.checkCorpUse());
        mav.addObject("DOMAIN_LIST", commonService.getCodeList("DOMAIN"));
        
        return mav;
    }

    @PostMapping("user/joinUser")
    private @ResponseBody String joinUser(@RequestBody Member member, Model model) {
        
        userService.joinUser(member);

        return "user/login";
    }

    @PostMapping("user/checkUserIDandEmail")
    private @ResponseBody boolean checkUserIDandEmail(@RequestBody Map<String, Object> param) {

        if (userService.checkUserIDandEmail(param)) {
            return true;
        }

        return false;
    }
    
    /**
     *  비밀번호 정책 
     * 
     * 
     */
    @PostMapping("user/getPwPolicy")
    private @ResponseBody Map<String, Object> getPwPolicy() {
        Map<String, Object> returnMap = new HashMap<>();
        
        returnMap.put("PW_POLICY", userService.getPwPolicy());
        
        return returnMap;
    }
    /**
     * 아이디찾기
     * 
     * @param model
     * @param member
     * @return
     */
    @GetMapping("/user/findUser")
    private String findUser(Model model) {

        return "user/findUser";
    }

    /**
     * 아이디찾기
     * 
     * @param model
     * @param member
     * @return
     */
    @PostMapping("/user/findUser")
    private @ResponseBody String findUserGetId(@RequestBody Map<String, Object> param) {
        String userId = userService.findUser(param);

        return userId;
    }

    /**
     * 비밀번호 찾기
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/findPw")
    private ModelAndView findPw(Model model) {
        
        ModelAndView mav = new ModelAndView();
        mav.setViewName("user/findPw");
        mav.addObject("CHECK_MAIL_USE", commonService.checkMailUse());

        return mav;
    }

    /**
     * 비밀번호 찾기
     * 
     * @param member
     * @param model
     * @return
     */
    @PostMapping("user/findPw")
    private @ResponseBody String findPw(@RequestBody Map<String, Object> param)throws Exception {
        String returnStr = "";
        if (userService.findPw(param)) {
            String passwd = commonService.createKey();
            param.put("USR_PW", passwd);
            
            userService.changePw(param);
            commonService.sendSearchPwMessage((String) param.get("USR_EMAIL"), passwd);
            returnStr = "1";
        } 
        
        
        return returnStr;
        
        
    }

    /**
     * 비밀번호 초기화
     * 
     * @param model
     * @param member
     * @return
     */
    @GetMapping("/user/changePasswd")
    private String changePasswd(Model model, HttpServletRequest request) {
        HttpSession session = commonService.getLoginSession();
        
        if(session.getAttribute("identify") != null && (boolean) session.getAttribute("identify")) {
            model.addAttribute("userId", userService.getUser(commonService.getLoginUserNo()));
            model.addAttribute("PW_INITL_YN", session.getAttribute("PW_INITL_YN"));
        }
        
        return "user/changePasswd";
    }

    @PostMapping("/user/changePasswd")
    private @ResponseBody Map<String, Object> changePasswdTry(@RequestBody Map<String, Object> param) {
        HttpSession session = commonService.getLoginSession();
        Map<String, Object> resultMap = new HashMap<>();
        
        param.put("USR_NO", commonService.getLoginUserNo());
        Boolean response = userService.changePw(param); 
        
        if(response == false) {
            resultMap.put("SUCCESS", "0");
            session.invalidate();
            return resultMap;
        }
        
        session.removeAttribute("PW_INITL_YN");
        resultMap.put("SUCCESS", "1");
        return resultMap;
    }

    /**
     * 마이페이지
     * 
     * @param model
     * @return
     * @throws IOException
     */
    @GetMapping("/user/myInfo")
    private String myInfo(Model model, HttpServletRequest request, HttpServletResponse response) throws IOException {
        
        Object usrNo = commonService.getLoginUserNo();
        
        if (usrNo == null || "0".equals(usrNo)) return null;
        
        // 접속정보 기입
        model.addAttribute("LOGIN_INFO", commonService.getLoginInfo());
        model.addAttribute("dept"       , commonService.getGroupList());
        
        Map<String, Object> codeParams = new HashMap<>();
        codeParams.put("GRP_CD"         , "RSPOFC_CD");
        model.addAttribute("grade"      , commonService.getCodeList(codeParams));

        return "user/myInfo";
    }
    
    /**사용자 정보 획득
     * @param session
     * @return
     */
    @PostMapping("/user/getUserInfo")
    private @ResponseBody Member getUserInfo(HttpSession session){
        Member member = new Member();
        try {           
            member.setUsr_no(Integer.parseInt(session.getAttribute("USR_NO").toString()));          
            member=userService.getUserInfo(member);             
            
        }catch(NumberFormatException e) {
            log.error(" " + e.getCause());
        }
        return member;
    } 
    
    /** 마이페이지의 내정보 수정
     * @param paramMap
     * @param model
     * @param session
     * @return Map<String,Object>
     */
    @PostMapping("/user/updateMyInfo")
    private @ResponseBody Map<String,Object> updateUserInfo(@RequestBody Map<String,Object> paramMap, HttpSession session) {
        
        Map<String,Object> resultMap = new HashMap<String,Object>();
        if(!(boolean) session.getAttribute("identify")) {
            //패스워드 
            resultMap.put("errMsg", "내정보 수정시 본인인증이 필요합니다.");
        }else {
            paramMap.put("USR_NO",session.getAttribute("USR_NO"));
            int result = userService.updateMyInfo(paramMap);
            if(result == 1) {
                resultMap.put("success","true");
            }else {
                resultMap.put("errMsg", "수정이 정상적으로 처리되지 않았습니다.");
            }
        }       
        return resultMap;
    }
    
    /**마이페이지의 내정보 수정 취소시 본인인증 정보 제거
     * @param session
     * @return
     * @return Map<String,Object>
     */
    @PostMapping("/user/updateMyInfoExit")
    private @ResponseBody Map<String,Object> checkMyInfoSuccess(HttpSession session){
        Map<String,Object> resultMap = new HashMap<String,Object>();
        session.removeAttribute("identify");
        resultMap.put("success", "true");
        return resultMap;
    }   

    /**마이페이지의 접속정보 목록
     * @param pageIndex
     * @param session
     * @return Map<String,Object>
     */
    @PostMapping("/user/getMyConnectInfo")
    private @ResponseBody Map<String,Object> getMyConnectInfo(@RequestBody Map<String, Object> param,HttpSession session){
        Map<String,Object> resultMap = new HashMap<String,Object>(),
                        paramMap = new HashMap<String,Object>();
        try {
            int usrNo = Integer.parseInt(commonService.getLoginUserNo().toString());
            paramMap.put("USR_NO",usrNo);
            paramMap.put("PAGE_INDEX", (((int) param.get("PAGE_INDEX")) - 1) * (int) param.get("PAGE_SIZE"));
            paramMap.put("PAGE_SIZE", (int) param.get("PAGE_SIZE"));
            resultMap.put("connectList",userService.getMyConnectInfo(paramMap));
            resultMap.put("connectCnt",userService.getMyConnectInfoCount(usrNo));
        }catch(NumberFormatException e) {
            log.error(" " + e.getCause());          
        }
            
        return resultMap;
    }
    
    /**마이페이지의 프로젝트 정보 호출
     * @param model
     * @param request
     * @param param
     * @return Map<String,Object>
     */
    @PostMapping("/user/myInfo")
    private @ResponseBody Map<String, Object> myInfos(Model model, HttpServletRequest request, @RequestBody Map<String, Object> param) {
        
    	Map<String,Object> paramMap = new HashMap<String,Object>();
        
    	paramMap.put("USR_NO", commonService.getLoginUserNo());
        List<Map<String, Object>> projectList = userService.getProjects(paramMap);

        HashMap<String, Object> returnMap = new HashMap<String, Object>();

        returnMap.put("PROJECT_LIST", projectList);

        return returnMap;
        
    }

    /** 프로젝트 파기 신청
     * @param request
     * @param param
     * @return Map<String,Object>
     */
    @PostMapping("/user/deleteProject")
    private @ResponseBody Map<String, Object> deleteProject(HttpServletRequest request, @RequestBody Map<String, Object> param) {
        Map<String, Object> returnMap = new HashMap<String, Object>();

        returnMap = userService.requestDestructionSchedule(param);
        
        return returnMap;

    }

    /**
     * 나의 정보 수정 불러오기 - 본인인증페이지
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/changeUserInfo")
    private ModelAndView changeUserInfo(Model model, HttpServletRequest request, Member member) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("user/checkPassword");
        
        mav.addObject("NEXT_STAGE", "/user/changeUserInfoSuccess");
        
        return mav;
    }
    
    /**
     * 나의 정보 수정 불러오기 - 본인인증성공
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/changeUserInfoSuccess")
    private ModelAndView changeUserInfoSuccess(Model model, HttpServletRequest request, Member member) {
        HttpSession session = request.getSession();
        ModelAndView mav = new ModelAndView();
        if((boolean) session.getAttribute("identify")) {
            mav.setViewName("user/changeUserInfo");
            
            String usr_no = session.getAttribute("USR_NO").toString();
            int no = Integer.parseInt(usr_no);
            member.setUsr_no(no);

            String userId = userService.getUser("" + member.getUsr_no());
            member = userService.getUserInfo(member);

            model.addAttribute("usr_id", userId);
            model.addAttribute("usr_nm", member.getUsr_nm());
            model.addAttribute("cnt_frs", member.getCnt_frs());
            model.addAttribute("cnt_mdl", member.getCnt_mdl());
            model.addAttribute("cnt_end", member.getCnt_end());
            model.addAttribute("usr_email", member.getUsr_email());
            model.addAttribute("offm_telno", member.getOffm_telno());
            
            mav.addObject("GROUP_LIST", adminService.getGroup());
            mav.addObject("GP_CD", member.getGp_cd());
            mav.addObject("CHECK_MAIL_USE", commonService.checkMailUse());
        } else {
            mav.setViewName("user/login");
        }
        
        
        return mav;
    }

    /**나의 정보 수정 요청
     * @param model
     * @return
     */
    @PostMapping("/user/changeUserInfo")
    private String changeUserInfoPost(@RequestBody Map<String, Object> param, HttpServletRequest request) {

        param.put("USR_NO", commonService.getLoginUserNo());

        userService.updateUser(param);

        return "user/myInfo";
    }
    
    /**나의 정보 프로젝트 단계별 숫자 표출
     * @param session
     * @return List<Map<String,Object>>
     */
    @PostMapping("/user/getProjectStepCnt")
    private @ResponseBody List<Map<String,Object>> getProjectStepCnt(HttpSession session) {
        Map<String,Object> paramMap = new HashMap<String,Object>();
        paramMap.put("USR_NO", commonService.getLoginUserNo());
        List<Map<String,Object>> resultMap = userService.getMyProjectStep(paramMap);
        return resultMap;
    }
    
    /**나의 정보 프로젝트 단계별 목록
     * @param session
     * @param stepNo
     * @return List<Map<String,Object>>
     */
    @PostMapping("/user/getProjectStepList")
    private @ResponseBody List<Map<String,Object>> getProjectStepList(@RequestBody Map<String,Object> param,HttpServletRequest request){
        param.put("USR_NO", commonService.getLoginUserNo());
        param.put("IS_PUBLISH", "Y");
        List<Map<String,Object>> resultMap = userService.getMyProjectList(param);
        return resultMap;
    }
    
    
    @PostMapping("user/checkPassword")
    private @ResponseBody boolean passwordCheck(@RequestBody Map<String, Object> param, HttpServletRequest request) {

        HttpSession session = request.getSession();
        param.put("USR_NO", commonService.getLoginUserNo());
        
        if (userService.checkPassword(param)) {
            session.setAttribute("identify", true);
            return true;
        }

        return false;
    }

    /**
     * ip 제어
     * 
     * @param model
     * @return
     */
    @GetMapping("/user/ipDenied")
    private String ipDenied(Model model) {

        return "user/ipDenied";
    }
    
    @PostMapping("/user/mail")
    @ResponseBody
    public String emailConfirm(@RequestBody Map<String, Object> param)throws Exception{
        String passwd = commonService.createKey();
        commonService.sendJoinMessage((String) param.get("USR_EMAIL"), passwd);
        
        return passwd;
    }
    @PostMapping("/user/verifyCode")
    @ResponseBody
    public int verifyCode(String code) {
        
        int result = 0;
        
        return result;
    }
    /**
     * 비밀번호 변경 - 비밀번호 확인 페이지
     * 
    */
    @GetMapping("/user/changePasswdCheck")
    public ModelAndView checkPasswd(Model model) {
        ModelAndView mav = new ModelAndView();
        HttpSession session = commonService.getLoginSession();
        
        mav.setViewName("user/checkPassword");
        
        mav.addObject("userId", userService.getUser(commonService.getLoginUserNo()));
        mav.addObject("PW_INITL_YN", session.getAttribute("PW_INITL_YN"));
        mav.addObject("NEXT_STAGE", "/user/changePasswd");
        
        return mav;
    }

    /**나의 정보 프로젝트 단계별 목록
     * @param session
     * @param stepNo
     * @return List<Map<String,Object>>
     */
    @PostMapping("/user/getCarryoutInfoList")
    private @ResponseBody List<Map<String,Object>> getCarryoutInfoList(@RequestBody Map<String,Object> param){
        List<Map<String,Object>> resultMap = userService.getCarryoutInfoList(param);
        return resultMap;
    }
	

}
