package kr.co.pamaster.user.service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import kr.co.pamaster.common.mapper.CommonMapper;
import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.user.mapper.UserMapper;
import kr.co.pamaster.vo.Member;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserMapper mapper;

    @Autowired
    private CommonService commonService;
    
    @Autowired
    private CommonMapper commonMapper;
    
    private static Map<String,Object> pwPolicyMap = new HashMap<String,Object>();
    
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    private int findIDMaskingCount = 2;

    public String getUser(Object no) {

        return mapper.getUser(no);
    }

    @Transactional
    public void joinUser(Member member) {
        StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
        member.setUsr_pw(passwordEncoder.encode(member.getUsr_pw()));
        member.setCnt_mdl(commonService.encryptAES(member.getCnt_mdl()));
        member.setCnt_end(commonService.encryptAES(member.getCnt_end()));
        
        List<Map<String, Object>> codeList = commonService.getCodeList("INIT_USER_SETTING");
        Map<String, Object> initRoleMap = new HashMap<>();
        List<String> initRoleList = new ArrayList<>();
        // 유저 최초 생성시 초기화 목록
        for(Map<String, Object> codeMap : codeList) {
            Object code = codeMap.get("CD");
            
            // 최초 권한 List -> Array -> Map
            if("INIT_ROLE".equals(code)) {
                String initRoleSet = (String) codeMap.get("META_1");
                if(!initRoleSet.isEmpty()) {
                    initRoleList = Arrays.asList(initRoleSet.split(","));
                }
            }
            // 승인여부
            if("INIT_APPROVAL_YN".equals(code)) {
                String approvalyn = (String) codeMap.get("META_1");
                member.setApproval_yn(approvalyn);
            }
            // 비밀번호 초기화
            if("INIT_PW_YN".equals(code)) {
                String pwinityn = (String) codeMap.get("META_1");
                member.setPw_initl_yn(pwinityn);
            }
        }
        
        // 기업명을 사용하는 버전의 경우 부서를 따로 만들어줘야함
        if(commonService.checkCorpUse() && member.getGp_company_nm() != null) {
            //회사 생성 후 UPPER CD를 받아 부서 생성
            Map<String, Object> companyMap = new HashMap<>();
            companyMap.put("GP_NM", member.getGp_company_nm());
            String memberCompanyIDX = "";
            String memberCompanyCD = "";
            
            List<Map<String, Object>> companyList = mapper.selectCorpGroupByName(companyMap);
            if(companyList != null && companyList.size() > 0) { // 이미 생성된 기업명이 있는 경우 해당 기업과 매핑
                memberCompanyCD = companyList.get(0).get("GP_CD").toString();
            } else {
                mapper.insertCompanyInfo(companyMap);
                memberCompanyIDX = companyMap.get("IDX").toString();
                memberCompanyCD = "TEMP_" + memberCompanyIDX;
                companyMap.put("GP_CD", memberCompanyCD);
                mapper.updateGroupCD(companyMap);
            }
            
            Map<String, Object> groupMap = new HashMap<>();
            groupMap.put("GP_NM", member.getGp_new_nm());
            groupMap.put("UPPER_GP_CD", memberCompanyCD);
            String memberGroupIDX = "";
            String memberGroupCD = "";
            
            List<Map<String, Object>> groupList = mapper.selectCorpGroupByName(groupMap);
            if(groupList != null && groupList.size() > 0) { // 해당 기업명에 이미 생성된 부서가 있는 경우 해당 기업의 부서와 매핑
                memberGroupCD = groupList.get(0).get("GP_CD").toString();
            } else {
                mapper.insertGroupInfo(groupMap);
                memberGroupIDX = groupMap.get("IDX").toString();
                memberGroupCD = "TEMP_" + memberGroupIDX;
                groupMap.put("GP_CD", memberGroupCD);
                mapper.updateGroupCD(groupMap);
            }
            member.setGp_cd(memberGroupCD);
        }
        
        mapper.joinUser(member);
        
        //mapper.joinUserRole(member);
        
        for(String initRole : initRoleList) {
            initRoleMap.put("INIT_ROLE", initRole);
            initRoleMap.put("USR_ID", member.getUsr_id());
            mapper.joinUserInitRole(initRoleMap);
        }
        
    }
    
    @Override
    public UserDetails loadUserByUsername(String id) throws UsernameNotFoundException {
        Member member = new Member();
        Map<String, Object> memberMap = new HashMap<String, Object>();
        memberMap.put("id", id);
        // Login user get info.
        List<Map<String, Object>> memberInfoList = mapper.getMember(memberMap);
        Map<String, Object> memberInfo = new HashMap<>();
        
        if(memberInfoList != null && memberInfoList.size() > 0 ) {
            memberInfo = memberInfoList.get(0);
        } else {
            
            throw new UsernameNotFoundException("User not exist with name :" +id);
        }
        
        member.setUsr_id((String) memberInfo.get("USR_ID"));
        member.setUsr_pw((String) memberInfo.get("USR_PW"));
        member.setApproval_yn((String) memberInfo.get("APPROVAL_YN"));
        member.setPw_initl_yn((String) memberInfo.get("PW_INITL_YN"));
        member.setLock_yn((String) memberInfo.get("LOCK_YN"));
        member.setPw_initl_dt((String) memberInfo.get("PW_INITL_DT"));
        
        // 미허용  / 잠금여부
        if ( "N".equals(  member.getApproval_yn() ) || "Y".equals(member.getLock_yn() ) ) {
            
            throw new UsernameNotFoundException("User lock with name :" +id);
        }
        
        // 권한 추가
        List<GrantedAuthority> authorities = new ArrayList<>();
        
        List<String> memberRoleList = mapper.getRoleList(memberInfo.get("USR_NO"));
        
        if(memberRoleList != null) {
            for (String memberRole : memberRoleList) {
                authorities.add(new SimpleGrantedAuthority(memberRole));            
            }           
        }
        
        if (commonService.ipCheck(memberInfo.get("USR_NO")) == 0) {
            
            throw new UsernameNotFoundException("User lock with name :" +id);
        }
        
        /*
        Map<String, Object> param = new HashMap<>();
        param.put("USR_NO", memberInfo.get("USR_NO"));
        mapper.successLoginUser(param);
        */
        return new User(member.getUsr_id(), member.getUsr_pw(), authorities);
    }
    
    // Get password policy
    public Boolean getPasswordPolicy(int pwInitlDt) {
        
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        
        int maxUseDay = 180;
        
        Map<String, Object> codeMap = commonService.getCodeInfo("PASSWORD_POLICY_CD", "PW_MAX_USE_DAY");
        
        
        if(codeMap != null && codeMap.get("META_1") != null && !"".equals(codeMap.get("META_1")) && !"0".equals( codeMap.get("META_1") ) ) {
            maxUseDay = Integer.parseInt((String) codeMap.get("META_1"));
        }else {
            return false;           
        }
        
        int checkDay = Integer.parseInt(now.plusDays( maxUseDay * -1 ).format(dateTimeFormatter));
        
        if(  checkDay > pwInitlDt) {
            return true;
        }
        
        return false;
    }
    
    public String findUser(Map<String, Object> param) {
        String userId = new String();       
        userId = mapper.findUser(param);
        
        if (userId != null) {
            StringBuffer maskingID = new StringBuffer(userId);
            for (int i = 1; i < findIDMaskingCount+1; i++) {
                if (i > maskingID.length()) {
                    return maskingID.toString();
                }
                maskingID.replace(maskingID.length()-i, maskingID.length()-i+1, "*");
            }
            return maskingID.toString();
        }
        return userId;
    }

    public boolean findPw(Map<String, Object> param) {
        boolean returnbool = false;
        
        int i = mapper.findPw(param);
        
        if (i > 0) {
            returnbool = true;
        }
        
        return returnbool;
    }

    public String changePw(Member member, String pwInitlYn) {
        
        StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
        member.setUsr_pw(passwordEncoder.encode(member.getUsr_pw()));
        member.setPw_initl_yn(pwInitlYn);
        mapper.changePw(member);
        
        return "비밀번호가 성공적으로 초기화 되었습니다.";
    }
    
    public Boolean changePw(Map<String, Object> param) {
        StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
        param.put("usr_pw", passwordEncoder.encode((String) param.get("USR_PW")));
        param.put("usr_id", param.get("USR_ID"));
        if(!"".equals(param.get("PW_INITL_YN")) && param.get("PW_INITL_YN") != null) {
            param.put("pw_initl_yn", param.get("PW_INITL_YN"));
        }
        
        Boolean checkDp = checkPassword(param);
        if(!checkDp) {
            mapper.changePw(param);
            return false;
        }
        return true;
    }
    
    public List<String> getRoleCdList(int username) {
        return mapper.getRoleList(username);
    }
    
    public int getProjectsCount(Map<String,Object> param) {
        int count = mapper.getProjectsCount(param);
        return count;
    }

    public List<Map<String, Object>> getProjects(Map<String,Object> param) {
        if(param.get("PAGE_INDEX") !=  null) {
            param.put("PAGE_INDEX", (((int) param.get("PAGE_INDEX")) - 1) * 10);
        }
        List<Map<String, Object>> projectList = mapper.getProjects(param);
        return projectList;
    }

    public void updateUser(@RequestBody Map<String, Object> param) {
        
        param.replace("CNT_MDL", commonService.encryptAES(param.get("CNT_MDL").toString()));
        param.replace("CNT_END", commonService.encryptAES(param.get("CNT_END").toString()));

        mapper.updateUser(param);
    }

    public Member getUserInfo(Member member) {
        member = mapper.getUserInfo(member);
        member.setCnt_mdl(commonService.decryptAES(member.getCnt_mdl()));
        member.setCnt_end(commonService.decryptAES(member.getCnt_end()));

        return member;
    }

    public boolean idVaildCheck(String memberId) {
        List<Map<String, Object>> idList = mapper.getUserIdList(memberId);
        
        for (int i = 0; i < idList.size(); i++) {
            Map<String, Object> param = idList.get(i);
            
            if(param.get("USR_ID").equals(memberId)) {
                return false;
            }
        }

        return true;
    }
    
    @Transactional
    public boolean checkUserIDandEmail(Map<String, Object> param) {
        
        int i = mapper.checkUserIDandEmail(param);
        
        if (i > 0) {
            return false;
        } else return true;
    }
    
    public boolean checkPassword(Map<String, Object> param) {
        if("Y".equals(param.get("pw_initl_yn"))) {
            return false;
        }
        log.debug("CONFIRM no : " + param.get("USR_NO"));
        StandardPasswordEncoder passwordEncoder = new StandardPasswordEncoder();
        
        boolean returnbool = passwordEncoder.matches((CharSequence) param.get("USR_PW"), mapper.checkPassword(param));

        return returnbool;
    }
    
    public String getUserNm(Object usrNo) {
        Map<String, Object> param = new HashMap<>();
        param.put("USR_NO", usrNo);
        if(usrNo == null) {
            return null;
        }
        return mapper.getUserName(param);
    }
    
    /** 접속자의 마지막 로그인 정보 획득
     * @param usrNo
     * @return Map<String,Object>
     */
    public Map<String,Object> getLastLoginHst(String usrNo){
        return mapper.getLastLoginHst(usrNo);
    }
    
    /**마이페이지 사용자 정보 수정
     * @param param
     * @return int
     */
    public int updateMyInfo(Map<String,Object> param) {
        param.put("CNT_MDL",commonService.encryptAES(param.get("CNT_MDL").toString()));
        param.put("CNT_END",commonService.encryptAES(param.get("CNT_END").toString()));
    
        int updateCheck = mapper.updateMyInfo(param);
        
        if(updateCheck == 1) {
            HttpSession session = commonService.getLoginSession();
            session.setAttribute("USR_GP_CD", param.get("GP_CD"));
        }
        
        return updateCheck;
    }
    
    /**마이페이지 사용자 접속 정보 목록
     * @param param
     * @return List<Map<String,Object>>
     */
    public List<Map<String,Object>> getMyConnectInfo(Map<String,Object> param){
        return mapper.getMyConnectInfo(param);
    }
    
    /**마이페이지 사용자 접속 정보 목록 카운트
     * @param usrNo
     * @return int
     */
    public int getMyConnectInfoCount(int usrNo) {
        return mapper.getMyConnectInfoCount(usrNo);
    }
    
    /**마이페이지 사용자 프로젝트 스텝별 카운트
     * @param param
     * @return List<Map<String,Object>>
     */
    public List<Map<String,Object>> getMyProjectStep(Map<String,Object> param){
        return mapper.getMyProjectStep(param);
    }
    
    /**마이페이지 사용자 프로젝트 스탭별 목록
     * @param param
     * @return List<Map<String,Object>>
     */
    public List<Map<String,Object>> getMyProjectList(Map<String,Object> param){
        return mapper.getMyProjectList(param);
    }
    
    /**
     * 
     * 
     */
     public Map<String, Object> getPwPolicy() {
         
         boolean isNewLoading = true;
         long nowTime = new Date().getTime();
         
         if(pwPolicyMap.get("SETTING_TIME") != null) {
             
             long settingTime = (long)pwPolicyMap.get("SETTING_TIME");
             
             if(settingTime > nowTime) {
                 isNewLoading = false;
             }
         }
         
         if(isNewLoading) {
             
             Map<String,Object> pwPolicyCodeMap = commonService.getCodeInfo("PASSWORD_POLICY_CD", "PW_POLICY");
             
             pwPolicyMap.clear();
             
             //1시간 동안 조회 안되도록 처리
             pwPolicyMap.put("SETTING_TIME", nowTime + (1000 * 60 * 60));
             
             if(pwPolicyCodeMap != null) {
                 
                 pwPolicyMap.put("pwMinSize", pwPolicyCodeMap.get("META_1"));
                 pwPolicyMap.put("pwMaxSize", pwPolicyCodeMap.get("META_2"));
                 pwPolicyMap.put("pwCheckLvl", pwPolicyCodeMap.get("META_3"));
                 
             }else {
                 
                 pwPolicyMap.put("pwMinSize", 8);
                 pwPolicyMap.put("pwMaxSize", 20);
                 pwPolicyMap.put("pwCheckLvl", 3);
                 
             }
         }
         
         return pwPolicyMap;
     }
     
    /**
     * 로그인 성공시 로직 처리
     * 세션 정보 추가
     * 로그인 실패회수 0으로 초기화
     * 
     * @param userid
     * @param response
     * @throws IOException
     * @return void
     */
    public void userLoginInitSetting(Map<String, Object> param) throws IOException {
        
        List<Map<String, Object>> memberInfoList = mapper.getMember(param);
        Map<String, Object> memberInfo = new HashMap<>();
        
        if(memberInfoList != null && memberInfoList.size() > 0 ) {
            memberInfo = memberInfoList.get(0);
        }
        
        List<String> memberRoleListMapped = mapper.getRoleListMapped(memberInfo.get("USR_NO"));

        List<String> memberRoleList = mapper.getRoleList(memberInfo.get("USR_NO"));
        
        HttpSession session = commonService.getLoginSession();
        
        session.setAttribute("USR_ID", memberInfo.get("USR_ID"));
        session.setAttribute("USR_NO", memberInfo.get("USR_NO"));
        session.setAttribute("USR_GP_CD", memberInfo.get("GP_CD"));
        session.setAttribute("ROLE_LIST", memberRoleList);
        session.setAttribute("ROLE_LIST_MAPPED", memberRoleListMapped);
        
        // Get password policy
        Boolean validLimitTime = true;
        
        if(memberInfo.get("PW_INITL_DT") != null) {
            int pwInitlDt = Integer.parseInt( (String)memberInfo.get("PW_INITL_DT"));
            validLimitTime = getPasswordPolicy(pwInitlDt);
        }
        
        // Password valid limit time Check
        if(validLimitTime || "Y".equals(memberInfo.get("PW_INITL_YN"))) {
            session.setAttribute("PW_INITL_YN", "Y");
        } else {
            session.setAttribute("PW_INITL_YN", "N");
        }
        
        if(memberRoleList.contains("ROLE_SYS_ADMIN")) {
            session.setAttribute("IS_SYS_ADMIN","Y");
        }else {
            session.setAttribute("IS_SYS_ADMIN","N");
        }
        
        param.put("USR_NO", commonService.getLoginUserNo());
        mapper.successLoginUser(param);
    }
     
      /**
       * 프로젝트 파기 스케줄 등록
       * @param 
       *    DATA_LIST   
       */
      public Map<String, Object> requestDestructionSchedule(Map<String, Object> param) {
          List<Object> prjctList = (List<Object>) param.get("DATA_LIST");
          Map<String, Object> returnMap = new HashMap<>();
          
          param.clear();
          
          if(prjctList == null || prjctList.size() <= 0) {
              returnMap.put("SUCCESS", "2");
              
              return returnMap;
          }
          
          for( Object prjct : prjctList) {
              param.put("PRJCT_NO", prjct);
              param.put("STEP_CD" , "999");
              param.put("FL_NO" , "0");
              param.put("DLT_YN", mapper.selectProjectDltYn(param));
              
              // 파기 포함된 프로젝트가 있을 경우 
              if("Y".equals(param.get("DLT_YN"))) {
                  returnMap.put("SUCCESS", "3");
                  // 유효성 검사 시 주석 해제
                  return returnMap;
              }
              
              param.put("DUS_YN", "Y");
              commonMapper.updateProjectDltYn(param);
              
              commonService.requestScheduleProcessing(param, false);
          }
          returnMap.put("SUCCESS", "0");
          
          return returnMap;
      }

    /**
     * @throws IOException
     *
     */
    public int checkPwFailCount(String userid, HttpServletRequest request) throws IOException {

        Object failureLimit = "0";
        Object currentFailedCount = 0;

        Map<String, Object> codeMap = commonService.getCodeInfo("PASSWORD_POLICY_CD", "PW_FAILURE_COUNT");
        if(codeMap != null && codeMap.get("META_1") != null && !"".equals(codeMap.get("META_1")) && !"0".equals(codeMap.get("META_1")) ) {
            failureLimit = codeMap.get("META_1");
        } else { // NOT PW_FAILURE_POLICY
            return 2;
        }

        //사용자 체크
        Map<String, Object> chackParam = new HashMap<>();

        chackParam.put("USR_ID", userid);
        if(mapper.selectUserNo(chackParam) == null) {
            return 1;
        }

        // GET PW_FAILURE_COUNT
        currentFailedCount = mapper.getPwFailCountCheck(userid);

        //  USR_ID EXISTS NOT
        if(currentFailedCount == null) {
            return 3;
        }

        // FAILURE COUNT ++
        int addCount = ((int) currentFailedCount) + 1;

        // LIMIT FAILURE COUNT
        if( addCount >= Integer.parseInt((String) failureLimit)) {
            Map<String, Object> param = new HashMap<>();
            param.put("USR_ID", userid);
            param.put("COUNT", 0);

            mapper.addPwFailCount(param);

            // 유저 잠금
            userLock(userid);
            // 유저 로그인 실패 기록
            userFailRecording(param, request);

            return 0;
        }

        Map<String, Object> param = new HashMap<>();
        param.put("USR_ID", userid);
        param.put("COUNT", addCount);
        mapper.addPwFailCount(param);

        return 1;
    }

    public void userLock(Object usrid) {
        mapper.userLock(usrid);
    }

    public void userFailRecording(Map<String, Object> param, HttpServletRequest request) {
        String clientIp = request.getRemoteAddr();
        String browser = commonService.getBrowser(request);
        String conectDt = commonService.currentDateTime();
        Object usrno = getUsrNo(param);

        param.put("USR_NO", usrno);
        param.put("CONECT_DT", conectDt);
        param.put("CONECT_IP", clientIp);
        param.put("CONECT_WBSR", browser);
        param.put("REMARKS", "FAIL");
        param.put("TYPE", 2);

        commonService.setFailLoginHistRecording(param);
    }

    public Object getUsrNo(Map<String, Object> param) {
        return mapper.selectUserNo(param);
    }

    public List<Map<String, Object>> getCorpGroupList() {
        return mapper.selectCorpGroupList();
    }

    /**
    *
    * @param
    *    PRJCT_NO
    */
    public List<Map<String, Object>> getCarryoutInfoList(Map<String, Object> param) {
        List<Map<String, Object>> returnList = mapper.selectCarryoutInfoList(param);

        return returnList;

    }

	  
}