/**
 * 
 */
package kr.co.pamaster.view;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;

/**
 * @date 2021. 5. 20.
 * @author 이민구
 * @name kr.co.lagomsoft.view.ExcelView
 * @comment 
 */
@Component("ExcelDownload")
public class ExcelView extends AbstractXlsView{

	
	/**Cell 생성 
	 * @param row
	 * @param columnCount
	 * @param value
	 * @param style
	 * @param sheet
	 * @return void
	 */
	private void createCell(Row row, int columnCount, Object value, CellStyle style,Sheet sheet) {
        sheet.autoSizeColumn(150);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);
    }
	
	
	/**헤더 추가
	 * @param headerMap
	 * @param sheet
	 * @param workbook
	 * @return void
	 */
	private void writeHeaderLine(List<Object> headerList,Sheet sheet,Workbook workbook) {		
		Row row = sheet.createRow(0);
		//헤더 cell style 설정
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(true);
		//font.setFontHeight((short) 13);
		style.setFont(font);	
	
		int colCnt = 0;
		for(Object header : headerList) {
			Map<String,Object> headerMap = (Map<String,Object>)header;
			createCell(row,colCnt++,headerMap.get("title"),style,sheet);
		}
	}
	
	/**내용 추가
	 * @param headerMap
	 * @param bodyMap
	 * @param sheet
	 * @param workbook
	 * @return void
	 */
	private void writeDataLines(List<Object> headerList,List<Object> bodyList,Sheet sheet,Workbook workbook) {
		//바디 cell style 설정
		CellStyle style = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBold(false);
		//font.setFontHeight((short) 14);
		style.setFont(font);
		
		int rowCnt = 1;
		for(Object body : bodyList) {
			int colCnt = 0;
			Map<String,Object> bodyMap = (Map<String,Object>)body;
			Row row = sheet.createRow(rowCnt++);
			for(Object header : headerList) {
				Map<String,Object> headerMap = (Map<String,Object>)header;				
				createCell(row,colCnt++,bodyMap.get(headerMap.get("name")),style,sheet);
			}
		}
	}
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		response.setHeader("Content-Disposition", "attachment; filename=\""+model.get("fileName")+".xls\"");
		Sheet sheet = workbook.createSheet("sheet1");
		List<Object> headerList = (List<Object>)model.get("header");
		List<Object> bodyList = (List<Object>)model.get("body");
		  
		writeHeaderLine(headerList,sheet,workbook);
		writeDataLines(headerList,bodyList,sheet,workbook);
		
	}

}
