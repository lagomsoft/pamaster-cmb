package kr.co.pamaster.advancePreparation.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AdvancePreparationMapper {
	
	public void insertProjectInfo(Map<String, Object> param);
	
	public void updateProjectInfo(Map<String, Object> param);
	
	public void insertAplInfo(Map<String, Object> param);
	
	public void updateAplInfo(Map<String, Object> param);
	
	public void addColumnList(Map<String, Object> param);
	
	public void cmbAddColumnList(Map<String, Object> param);
	
	public void loadCsvDataInfileExcute(Map<String, Object> param);
	
	public void deleteColumnLst(Map<String, Object> param);
	
	public void createDataTable(Object prjct_no);
	
	public void setBndObjInfo(Map<String, Object> param);
	
	public void setBndInfo(Map<String, Object> param);
	
	public void deleteBndObjInfo(Map<String, Object> param);
	
	public void deleteBndInfo(Map<String, Object> param);
	
	public void deleteUtlInfo(Map<String, Object> param);
	
	public void deleteAplInfo(Object param);
	
	public void deleteFileClmnInfo(Object param);
	
	public void deleteAplFileInfo(Map<String, Object> param);

	public void insertUtlInfo(Map<String, Object> param);
	
	public void updateOrgTbl(Map<String, Object> param);
	
	public void updateUploadData(Map<String, Object> param);
	
	public void updateSelectObject(Map<String, Object> param);
	    
	public void updateUploadCount(Map<String, Object> param);
	    
    public void deleteContractCheck(Map<String, Object> param);
    
    public void insertContractCheck(Map<String, Object> param);
    
    public void updateFileName(Map<String, Object> param);
    
    public void updateColumnName(Map<String, Object> param);

    public void insertConsistencyCheck(Map<String, Object> param);
    
    
    public Object selectFlNo(Map<String, Object> param);
    
    public Object selectStepCd(Map<String, Object> param);
    
    public Object selectMainAplNo(Map<String, Object> param);

    public Object getFileId(Map<String, Object> param);
    
    
    public int selectMaxProNo();

    public int selectMaxAplNo();

    public int selectMaxFileNo();    
    

    public Map<String, Object> selectProjectInfo(Map<String, Object> param);    
    
    public Map<String, Object> selectAplInfo(Map<String, Object> param);    
    
	public Map<String, Object> getContractFileId(Map<String, Object> param);
	
	public Map<String, Object> selectOrgTblData(Map<String, Object> param);

	public Map<String, Object> selectAplDataInfo(Map<String, Object> param);

    public Map<String, Object> selectProgressRate(Map<String, Object> param);	
	
    public Map<String, Object> selectLoginUserInfo(Object userNo);
    
    public Map<String, Object> selectFlInfo(Map<String, Object> param);
    
    
    public List<Map<String, Object>> checkBindInfo(Map<String, Object> param);    
    
    public List<Map<String, Object>> selectAplInfoList(Map<String, Object> param);    
	    
    public List<Map<String, Object>> selectUtlInfo(Map<String, Object> param);
    
    public List<Map<String, Object>> getBndObjInfo(Map<String, Object> param);
    
    public List<Map<String, Object>> getAplInfo(Map<String, Object> param);
    
    public List<Map<String, Object>> getBndInfo(Map<String, Object> param);	

	public List<Map<String, Object>> selectProgressList(Map<String, Object> param);
	
    public List<Map<String, Object>> getContract(Map<String, Object> param);	
	
	public List<Map<String, Object>> selectOrgTblDataList(Map<String, Object> param);
	
	public List<Map<String, Object>> selectMatchCnt(Map<String, Object> param);
}
