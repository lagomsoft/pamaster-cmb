package kr.co.pamaster.advancePreparation.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import kr.co.pamaster.admin.service.AdminService;
import kr.co.pamaster.advancePreparation.service.AdvancePreparationService;
import kr.co.pamaster.common.service.CommonService;

@Controller
public class AdvancePreparationController {

	@Autowired
	AdvancePreparationService service;

	@Autowired
	CommonService commonService;

	@Autowired
	AdminService adminService;

	/**
	 * 프로젝트 생성 및 수정 입력화면
	 * 
	 * @param model
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@GetMapping("advancePreparation/conformity")
	private ModelAndView conformity(@RequestParam Map<String, Object> param) {
		
		ModelAndView mav = new ModelAndView();

		mav.addAllObjects(param);

		mav.addObject("NOW_DATE", commonService.currentDateTime());

		mav.addObject("ALIAS_PRPS", commonService.getCodeList("ALIAS_PRPS"));
		mav.addObject("ALIAS_TY", commonService.getCodeList("ALIAS_TY"));
		mav.addObject("INS_SEP", commonService.getCodeList("INS_SEP"));
		mav.addObject("RSPOFC_CD", commonService.getCodeList("RSPOFC_CD"));
		mav.addObject("DEPT_LIST", commonService.getGroupList());

		mav.addObject("ENCODING_TY", commonService.getCodeList("ENCODING_TY"));
		mav.addObject("FILE_ENCLOSED", commonService.getCodeList("FILE_ENCLOSED"));
		mav.addObject("FILE_TERMINATED", commonService.getCodeList("FILE_TERMINATED"));
		mav.addObject("FILE_HEADER", commonService.getCodeList("FILE_HEADER"));
		
		mav.addObject("DOMAIN_LIST", commonService.getCodeList("DOMAIN"));
		
		mav.setViewName("advancePreparation/conformity");

		return mav;
	}

	/**
	 * 프로젝트 생성 초기 데이터 조회
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/advancePreparation/getInitConformity")
	private @ResponseBody Map<String, Object> getInitConformity(Model model, @RequestBody Map<String, Object> param,
			RedirectAttributes redirect) {

		Map<String, Object> initData = service.getInitConformity();

		return initData;
	}

	/**
	 * 프로젝트 데이터 조회
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/advancePreparation/getConformity")
	private @ResponseBody Map<String, Object> getConformity(Model model, @RequestBody Map<String, Object> param,
			RedirectAttributes redirect) {

		Map<String, Object> projectInfo = service.getProjectInfo(param);
		
		Map<String, Object> returnMap = new HashMap<>();

		// 프로젝트 정보 Map
		returnMap.put("PROJECT_INFO", projectInfo);

		return returnMap;
	}
	
	/**
	 * 결합신청자 리스트 조회
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/advancePreparation/getAplInfoList")
	private @ResponseBody List<Map<String, Object>> getAplInfoList(@RequestBody Map<String, Object> param) {
		
		return  service.getAplInfoList(param);
	}
	/**
	 * 결합신청자 일치 결합키 조회
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/advancePreparation/getMatchCnt")
	private @ResponseBody List<Map<String, Object>> getMatchCnt(@RequestBody Map<String, Object> param) {
	    return  service.getMatchCnt(param);
	}
	

	/**
	 * 결합신청자 데이터 업로드 정보 조회
	 * 
	 * @param 
	 *     PRJCT_NO
	 *     APL_NO
	 *     FL_TY_CD
	 * @return
	 */
	@PostMapping("/advancePreparation/getAplDataInfo")
	private @ResponseBody Map<String, Object> getAplDataInfo(@RequestBody Map<String, Object> param) {
		
		return service.getAplDataInfo(param);
	}


	/**
	 * 프로젝트 정보 저장
	 * 
	 * @param
	 * 		PRJCT_NO
	 * 		PRJCT_TY_CD(K,D)
	 * 		PRJCT_NM
	 * 		PRJCT_STEP
	 * 		PRJCT_DTL
	 * 		UPLOAD_DATA_LIST
	 * @return SUCCESS
	 */
	@PostMapping(value = "/advancePreparation/saveProjectInfo")
	private @ResponseBody Map<String, Object> saveObjects(@RequestBody Map<String, Object> param) {
		List<Object> uploadDataList = (List<Object>) param.get("UPLOAD_DATA_LIST");
		Map<String, Object> returnMap = new HashMap<>();
        if(uploadDataList == null || uploadDataList.size()<=0) {  // 프로젝트 정보만 저장하는 경우
			//결합키 포함하는지 체크
        	Map<String, Object> returnStr = service.saveProjectInfo(param);
			List<Map<String, Object>> checkObjType= service.checkBindInfo(param);
			if(checkObjType.size()>0) {
				int aplNoCnt = 1;
				int tmpAplNo  = (int) checkObjType.get(0).get("APL_NO");;
				int bindKeyCnt = 0;
				for(int i =0 ; i<checkObjType.size() ; i++) {
					if(tmpAplNo != (int)checkObjType.get(i).get("APL_NO")) {
						aplNoCnt++;
						tmpAplNo = (int)checkObjType.get(i).get("APL_NO");
					}
					if(checkObjType.get(i).get("OBJ_TY").equals("BIND")) {
						bindKeyCnt++;
					}
				}
				if(aplNoCnt != bindKeyCnt) { 
					returnMap.put("RETURN_CODE", "1"); 
					return returnMap; 
				}
			}
//			List<String> fileNoList = service.saveSelectionObject(param);
//			param.put("FL_NO_LIST", fileNoList);
//			service.requestDataPreprocessing(param);
			returnMap.put("SUCCESS", returnStr);
			
        }else {
        	int tmpAplNo = (int)((Map<String, Object>) uploadDataList.get(0)).get("APL_NO");
        	int tmpCnt = 0;
        	for(int k =0 ; k < uploadDataList.size() ; k++) {
        		if(tmpAplNo == (int)((Map<String, Object>) uploadDataList.get(k)).get("APL_NO")) {
        			if(((Map<String, Object>) uploadDataList.get(k)).get("OBJ_TY").equals("BIND")) {
        				tmpCnt++;
        				if(tmpCnt>1) {
        					returnMap.put("RETURN_CODE", "2"); 
        					return returnMap; 
        				}
        			}
        		}else {
        			if(tmpCnt ==0) {
        				returnMap.put("RETURN_CODE", "1");
        				return returnMap;
        			}
        			tmpCnt= 0;
        			if(((Map<String, Object>) uploadDataList.get(k)).get("OBJ_TY").equals("BIND")) {
        				tmpCnt++;
        			}
        			tmpAplNo = (int)((Map<String, Object>) uploadDataList.get(k)).get("APL_NO");
        		}
        	}
        	Map<String, Object> returnStr = service.saveProjectInfo(param);
			//결합신청자 업로드 데이터 대상선정 저장


			returnMap.put("SUCCESS", returnStr);
        }
        
		List<String> fileNoList = service.saveSelectionObject(param);        
		if(fileNoList != null) {
			param.put("FL_NO_LIST", fileNoList);
			
			//결합신청자 업로드 데이터 전처리 스케줄 저장
			service.requestDataPreprocessing(param);
		}        
        
		return returnMap;
	}
	
	
	/**
	 * 결합신청자 정보 저장
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping(value = "/advancePreparation/saveAplInfo")
	private @ResponseBody Map<String, Object> saveAplInfo(@RequestBody Map<String, Object> param) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		String returnAplNo = service.saveAplInfo(param);

		returnMap.put("SUCCESS", returnAplNo);

		return returnMap;
	}
	
	
	/**
	 * 결합신청자 정보 조회
	 * 
	 * @param model
	 * @return
	 */
	@PostMapping("/advancePreparation/getAplInfo")
	private @ResponseBody Map<String, Object> getAplInfo(Model model, @RequestBody Map<String, Object> param, RedirectAttributes redirect) {
		
		Map<String, Object> aplInfo = new HashMap<String, Object>();
		
		if("0".equals(param.get("APL_NO"))) {
			return aplInfo;
		}
		
		aplInfo = service.getAplInfo(param);
		
		return aplInfo;
	}
	
	
	/*	
		*//**
			 * 결합정보만 저장
			 * 
			 * @param param
			 * @return
			 */
	/*
	 * @PostMapping(value = "/paAdvancePreparation/saveBindInfo")
	 * private @ResponseBody Map<String, Object> saveBindInfo(@RequestBody
	 * Map<String, Object> param) {
	 * 
	 * Map<String, Object> returnMap = new HashMap<String, Object>();
	 * 
	 * service.saveBindInfo(param);
	 * 
	 * returnMap.put("SUCCESS", "0");
	 * 
	 * return returnMap; }
	 */
	/*
		*//**
			 * 계약서 검토
			 * 
			 * @param model
			 * @return
			 */
	/*
	 * @GetMapping("/paAdvancePreparation/contractCheck") private ModelAndView
	 * contractCheck(@RequestParam Map<String, Object> param) {
	 * 
	 * ModelAndView mav = new ModelAndView();
	 * 
	 * List<Map<String, Object>> contractCheckList =
	 * commonService.getCodeList("CONTRACT_CHECK");
	 * 
	 * mav.addObject("CONTRACT_CHECK_LIST", contractCheckList);
	 * 
	 * mav.setViewName("paAdvancePreparation/contractCheck");
	 * 
	 * return mav; }
	 * 
	 *//**
		 * 계약서 업로드
		 * 
		 * @param request
		 * @return
		 * @throws Exception
		 */
	/*
	 * @PostMapping(value = "/paAdvancePreparation/saveContract")
	 * private @ResponseBody Map<String, Object>
	 * saveContract(MultipartHttpServletRequest request) throws Exception {
	 * 
	 * service.saveContract(request);
	 * 
	 * Map<String, Object> returnMap = new HashMap<String, Object>();
	 * 
	 * returnMap.put("SUCCESS", "0");
	 * 
	 * return returnMap;
	 * 
	 * }
	 * 
	 *//**
		 * 선택한 계약서 파일 삭제
		 * 
		 * @param param
		 * @return
		 * @return
		 * @throws IOException
		 */
	/*
	 * @PostMapping("/paAdvancePreparation/deleteContractFile")
	 * private @ResponseBody int deleteFile(@RequestBody Map<String, Object> param)
	 * {
	 * 
	 * List<Map<String, Object>> getFileInfos =
	 * commonService.getFileInfos(param.get("FL_ID"));
	 * 
	 * if (getFileInfos != null) {
	 * 
	 * for (Map<String, Object> getFileInfo : getFileInfos) {
	 * 
	 * commonService.deleteFile(param.get("PRO_NO"), getFileInfo.get("FL_ID"),
	 * getFileInfo.get("FL_SN"), false);
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return 1; }
	 * 
	 *//**
		 * 계약서 검토 (계약서 검토 여부 가져오기)
		 * 
		 * @param model
		 * @return
		 *//*
			 * @PostMapping("/paAdvancePreparation/getCheck") private @ResponseBody
			 * Map<String, Object> getCheck(@RequestBody Map<String, Object> param) throws
			 * IOException, ServletException {
			 * 
			 * Map<String, Object> contractCheck = service.getContract(param);
			 * 
			 * return contractCheck; }
			 */

	/**
	 * 데이터 업로드
	 * 
	 * @param model
	 * @return
	 */
	/*
	 * @GetMapping("/AdvancePreparation/dataUpload") private String dataUpload(Model
	 * model) {
	 * 
	 * model.addAttribute("ALIAS_FILE_SIZE",
	 * commonService.getFileSettingInfo("ALIAS_FILE_SIZE"));
	 * model.addAttribute("ALIAS_ROW_SIZE",
	 * commonService.getFileSettingInfo("ALIAS_ROW_SIZE"));
	 * 
	 * model.addAttribute("ENCODING_TY", commonService.getCodeList("ENCODING_TY"));
	 * model.addAttribute("FILE_ENCLOSED",
	 * commonService.getCodeList("FILE_ENCLOSED"));
	 * model.addAttribute("FILE_TERMINATED",
	 * commonService.getCodeList("FILE_TERMINATED"));
	 * model.addAttribute("FILE_HEADER", commonService.getCodeList("FILE_HEADER"));
	 * 
	 * return "advancePreparation/conformity"; }
	 */

	/**
	 * 데이터 업로드
	 * 
	 * @param request
	 * @return
	 * @return
	 */
	@PostMapping("/advancePreparation/csvDataUpload")
	private @ResponseBody Map<String, Object> csvDataUpload(MultipartHttpServletRequest request) {
		
		Map<String, Object> returnMap = service.csvDataUpload(request);

		return returnMap;
	}
	
	
	
	/**
	 * 결합신청자 삭제
	 * 
	 * @param aplNo
	 * @return
	 */
	@PostMapping(value = "/advancePreparaion/deleteAplInfo")
	private @ResponseBody Map<String, Object> deleteAplInfo(@RequestBody Map<String, Object> param) {
		
		service.deleteAplInfo(param);
		
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		returnMap.put("SUCCESS", "0");
		
		return returnMap;
		
	}
	

	/**
	 * 데이터 업로드 전 데이터 조회
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping(value = "/AdvancePreparation/getProjectDataList")
	private @ResponseBody Map<String, Object> getProjectDataList(@RequestBody Map<String, Object> param) {

		Map<String, Object> returnMap = service.getOrgTableData(param);

		if (returnMap.get("ORG_FILE_ID") != null || !"".equals(returnMap.get("ORG_FILE_ID"))) {

			param.put("PRO_NO", param.get("PRJCT_NO"));

			List<Map<String, Object>> projectColumns = commonService.getProjectColumns(param);
			List<Map<String, Object>> projectRowDatas = commonService.selectProjectRowDatas(0, param, projectColumns);

			returnMap.put("PROJECT_COLUMNS", projectColumns);
			returnMap.put("PROJECT_ROWDATAS", projectRowDatas);
		}

		return returnMap;
	}
	
	/**
	 * 모의결합 전처리 전 항목선택 부분 데이터 조회
	 * 
	 * @param
	 *     FL_NO
	 *     FL_TY_CD
	 * @return
	 */
	@PostMapping(value = "/advancePreparation/getUploadDataInfo")
	private @ResponseBody Map<String, Object> getUploadDataInfo(@RequestBody Map<String, Object> param) {
		
		return service.getDataInfo(param);
	}

	/**
	 * 컬럼명 변경
	 * 
	 * @param param
	 * @return
	 */
	/*
		 * @PostMapping(value = "/paAdvancePreparation/saveColumnName")
		 * private @ResponseBody Map<String, Object> saveColumnName(@RequestBody
		 * Map<String, Object> param) {
		 * 
		 * service.updateColumnName(param);
		 * 
		 * Map<String, Object> returnMap = new HashMap<String, Object>();
		 * 
		 * returnMap.put("SUCCESS", "0");
		 * 
		 * return returnMap; }
		 */

	/**
	 * 데이터 업로드 저장
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping(value = "/AdvancePreparation/saveUploadData")
	private @ResponseBody Map<String, Object> saveUploadData(@RequestBody Map<String, Object> param) {

		service.saveUploadData(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", "0");

		return returnMap;
	}


    /**
     * 적정성 검토
     * 
     * @param model
     * @return
     */
    @GetMapping("/advancePreparation/dashboard")
    private String consistencyCheck(Model model) {

        return "advancePreparation/dashboard";
    }
	/**
	 * 프레인 네 계약서 검토 정보 조회
	 * 
	 * @param param
	 * @return
	 */
	/*
	 * @PostMapping(value = "/paAdvancePreparation/getFrameContractCheckData")
	 * private @ResponseBody Map<String, Object>
	 * getFrameContractCheckData(@RequestBody Map<String, Object> param) {
	 * 
	 * Map<String, Object> returnMap = service.getContract(param);
	 * 
	 * Map<String, Object> projectInfo = service.getProjectInfo(param);
	 * 
	 * List<Map<String, Object>> contractCheckList =
	 * commonService.getCodeList("CONTRACT_CHECK");
	 * 
	 * returnMap.put("PRO_INFO", projectInfo); returnMap.put("CONTRACT_CHECK_LIST",
	 * contractCheckList);
	 * 
	 * return returnMap; }
	 */
	/**
	 * 프레인 네 계약서 검토 정보 조회
	 * 
	 * @param param
	 * @return
	 */

	/*
	 * @PostMapping(value = "/paAdvancePreparation/getExaminationList")
	 * private @ResponseBody Map<String, Object> getExaminationList(@RequestBody
	 * Map<String, Object> param) {
	 * 
	 * Map<String, Object> returnMap = new HashMap<String, Object>();
	 * 
	 * param.put("STEP_CD", 150); List<Map<String, Object>> examinationList =
	 * commonService.getExaminationList(param);
	 * 
	 * returnMap.put("EXAMINATION_LIST", examinationList);
	 * 
	 * return returnMap; }
	 */
	/**
	 * 데이터 업로드 데이터 조회 (프레임)
	 * 
	 * @param param
	 * @return
	 */
	@PostMapping(value = "/AdvancePreparation/getFrameDataUploadData")
	private @ResponseBody Map<String, Object> getFrameDataUploadData(@RequestBody Map<String, Object> param) {

		Map<String, Object> returnMap = service.getOrgTableData(param);

		if (returnMap.get("ORG_FILE_ID") != null || !"".equals(returnMap.get("ORG_FILE_ID"))) {

			param.put("PRO_NO", param.get("PRJCT_NO"));

			List<Map<String, Object>> projectColumns = commonService.getProjectColumns(param);
			List<Map<String, Object>> projectRowDatas = commonService.selectProjectRowDatas(0, param, projectColumns);

			returnMap.put("PROJECT_COLUMNS", projectColumns);
			returnMap.put("PROJECT_ROWDATAS", projectRowDatas);
		}

		return returnMap;
	}

	@PostMapping(value = "/AdvancePreparation/saveConsistencyCheck")
	private @ResponseBody Map<String, Object> saveConsistencyCheck(@RequestBody Map<String, Object> param) {

		int returnCode = service.saveConsistencyCheck(param);

		Map<String, Object> returnMap = new HashMap<String, Object>();

		returnMap.put("SUCCESS", returnCode);

		return returnMap;
	}
	

	@PostMapping(value = "/advancePreparation/getProgressCode")
	private @ResponseBody Map<String, Object> getProgressCode(@RequestBody Map<String, Object> param) {
		param.put("STEP_CD", param.get("PROJECT_STEP"));
		//일괄 처리 요청
		param.put("ADD_AUTO", "Y");
		param.put("PROJECT_NO", param.get("PROJECT_NO"));
		//Map<String, Object> scheduleMap = commonService.getLatelyScheduleProcessing(param);
		Map<String, Object> progressMap = commonService.getLatelyScheduleProcessing(param);  /*service.getProgressRate(param)*/
		List<Map<String, Object>> progressList = service.getProgressList(param);
		Map<String, Object> returnMap = new HashMap<String, Object>();
		
		returnMap.put("SCHEDULE_MAP", progressMap);
		returnMap.put("SCHEDULE_LIST", progressList);

		return returnMap;
	}

}
