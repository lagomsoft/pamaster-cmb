package kr.co.pamaster.advancePreparation.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import kr.co.pamaster.advancePreparation.mapper.AdvancePreparationMapper;
import kr.co.pamaster.carryout.mapper.CarryoutMapper;
import kr.co.pamaster.common.mapper.CommonMapper;
import kr.co.pamaster.common.service.CommonService;
import kr.co.pamaster.util.PAMsException;

@Service
public class AdvancePreparationService {
	
	@Autowired
	AdvancePreparationMapper mapper;
	
	@Autowired
	CommonService commonService;
	
	@Autowired
	CommonMapper commonMapper;
	
	@Autowired
	CarryoutMapper carryoutMapper;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	/**
	 * 프로젝트 정보 조회
	 * @param param
	 * @return
	 */
	public Map<String, Object> getProjectInfo(Map<String, Object> param) {

		Map<String, Object> projectInfo = mapper.selectProjectInfo(param);
		
		return projectInfo;
	}
	
	/**
	 * 초기 프로젝트 정보 조회
	 * @param param
	 * @return
	 */
	public Map<String, Object> getInitConformity() {
		
		Object userNo = commonService.getLoginUserNo();
		
		if(userNo != null) {
			Map<String, Object> userMap = mapper.selectLoginUserInfo(userNo);
			
			if(userMap != null) {
				
				userMap.put("GRP_CD", "PAGE_SETTING");
				userMap.put("SCH_CD", "CONFORMITY");
				userMap.put("CHARGER_RSPOFC", userMap.get("USR_RSPOFC"));
				userMap.put("CNT_MDL", commonService.decryptAES((String)userMap.get("CNT_MDL")));
				userMap.put("CNT_END", commonService.decryptAES((String)userMap.get("CNT_END")));
				
				return userMap;
			}
			
		}
		
		return null;
	}
	
	
	/**
	 * 모의결합 결합신청자 정보 조회  
	 */
	public Map<String, Object> getAplInfo(Map<String, Object> param) {
		Map<String, Object> returnMap = mapper.selectAplInfo(param);
		
		return returnMap;
	}
	/**
     * 모의결합 결합신청자 일치 결합치 조회  
     */
    public List<Map<String, Object>> getMatchCnt(Map<String, Object> param) {
        List<Map<String, Object>> returnMap = mapper.selectMatchCnt(param);
        
        return returnMap;
    }
	
	
	/**
	 * 결합신청자 리스트 조회
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getAplInfoList(Map<String, Object> param) {
		
		return mapper.selectAplInfoList(param);
	}

	/**
	 * 결합신청자별 업로드 데이터 조회
	 * @param 
	 *     PRJCT_NO
	 *     APL_NO
	 *     FL_TY_CD
	 * @return
	 */
	public Map<String, Object> getAplDataInfo(Map<String, Object> param) {
	    
		return mapper.selectAplDataInfo(param);
	}
	
	/**
	 * 
	 * @param param
	 * @return
	 */
	public List<Map<String, Object>> getUtlInfo(Map<String, Object> param) {
		
		List<Map<String, Object>> bndUtlInfoList = mapper.selectUtlInfo(param);

		if (bndUtlInfoList != null) {
			for (int i = 0; i < bndUtlInfoList.size(); i++) {
				bndUtlInfoList.get(i).put("INS_SEP", commonService.selectCodeNm("INS_SEP", bndUtlInfoList.get(i).get("UTL_INS_CD")));
			}
		}
		
		return bndUtlInfoList;
	}
	
	public List<Map<String, Object>> getBndObjInfo(Map<String, Object> param) {
		
		List<Map<String, Object>> bndObjInfoList = mapper.getBndObjInfo(param);	

		if (bndObjInfoList != null) {
			for (int i = 0; i < bndObjInfoList.size(); i++) {

				bndObjInfoList.get(i).put("INS_SEP", commonService.selectCodeNm("INS_SEP", bndObjInfoList.get(i).get("BND_INS_CD")));
			}
		}
		
		return bndObjInfoList;
	}

	public List<Map<String, Object>> getBndInfo(Map<String, Object> param) {

		return mapper.getBndInfo(param);
	}
	public int getLineInfo(BufferedReader br, List<String> headerInfo, String header, boolean enclosedEnd,  String splitBy, String isHeader, String enclosed, String characterset, int headerCnt) throws IOException {
		
		String line = br.readLine();
		headerCnt++;
		
		if(line == null) {
			line = "";
		}
		
		//인용부호 관련 로직
		if( enclosed != null && !"".equals(enclosed) ) {
			line = line.replaceAll(enclosed + enclosed, "TMP_ENCLOSED");
		}
		
		line = line.replace(splitBy, " " + splitBy + " ");
		
		String[] headers = line.split(splitBy);
		
		for (int hi = 0; hi < headers.length; hi++) {
			
			String tmpHeader = headers[hi];
			
			if( enclosed != null && !"".equals(enclosed) ) {
				
				String[] splitStr = (tmpHeader+ " ").split(enclosed);
				
				if(enclosedEnd) {
					
					if(splitStr.length >= 3 || splitStr.length <= 1) {
						header = tmpHeader.replaceAll(enclosed, "");
						enclosedEnd = true;
					}else {
						header = tmpHeader.replaceAll(enclosed, "");
						enclosedEnd = false;
					}
				}else {
					if(splitStr.length >= 2) {
						enclosedEnd = true;
					}
					
					header += header + tmpHeader.replaceAll(enclosed, "");
				}
				
			}else {
				header = tmpHeader;
			}
			
			/*
			 * 헤더 존재 여부
			 */
			if(enclosedEnd) {
				
				//헤더 없음
				if("0".equals(isHeader)) {
					
					headerInfo.add("DATA" + (headerInfo.size() + 1));
					
				//헤더 있음
				}else {
					
					if( enclosed != null && !"".equals(enclosed) ) {
						headerInfo.add( header.replaceAll("TMP_ENCLOSED", enclosed).trim() );
					}else {
						headerInfo.add( header.trim() );
					}
					
				}
				
			}
			
		}
		
		if(!enclosedEnd) {
			return getLineInfo(br, headerInfo, header, enclosedEnd, splitBy, isHeader, enclosed, characterset, headerCnt);
		}else {
			return headerCnt;
		}
		
	}
	
	public void insertProjectColumnList(List<String> headerList, Object prjctNo, Object aplNo, Object fileNo, Object flTyCd) {
		Map<String, Object> param = new HashMap<>();
		
		param.put("PRJCT_NO", prjctNo);
		param.put("APL_NO", aplNo);
		param.put("FL_NO", fileNo);
		
		mapper.deleteColumnLst(param);
		
		int count = 1;
		
		for (String header : headerList) {
			Map<String, Object> columnList = new HashMap<>();
			
			String headerStr = "" + header;
			if(headerStr.length() > 60) {
				headerStr = ("" + header).substring(0, 50) + "...";
			}
			
			columnList.put("PRJCT_NO", prjctNo);
			columnList.put("OBJ_NM", headerStr);
			columnList.put("APL_NO", aplNo);
			columnList.put("FL_NO", fileNo);
			columnList.put("FL_CLM_NO", "" + count);
			columnList.put("CLM_NM", "col" + count);
			
			if("K".equals(flTyCd)) {
			    columnList.put("OBJ_TY", "EXCL");		    
			} else if ("D".equals(flTyCd)) {
			    columnList.put("OBJ_TY", "INCLS");
			}
			
			if(count == 1) {
			    columnList.put("OBJ_TY", "BIND");
			}
			
			mapper.cmbAddColumnList(columnList);
			
			count++;
		}

	}
	
	/**
	 * 모의결합 insert into PRJCT_INFO
	 * @param param
	 */
	
	public Map<String, Object> saveProjectInfo(Map<String, Object> param) {
		Map<String, Object> returnMap = new HashMap<String, Object>();
		HttpSession session = commonService.getLoginSession();
		
		Object proNo = param.get("PRJCT_NO");
		Object userNo = commonService.getLoginUserNo();
		param.put("USR_NO", userNo);
		
		if(param.get("PRJCT_NO") == null || "".equals(param.get("PRJCT_NO"))) { // 신규 프로젝트를 저장하는 경우
			
			proNo = mapper.selectMaxProNo();
			session.setAttribute("SELECT_PRO_NO", proNo);
			
			param.put("PRJCT_NO", proNo);
		    param.put("STEP_CD", "100");
		    param.put("PRJCT_TY_CD", "K");
			mapper.insertProjectInfo(param);
			returnMap.put("PRJCT_NO", proNo);
			returnMap.put("STEP_CD", "100");
			//return proNo.toString();
			return returnMap;
			
		} else { // 기존 프로젝트를 업데이트 하는 경우
			
			mapper.updateProjectInfo(param);
			
			Object stepCd = mapper.selectStepCd(param);
			returnMap.put("PRJCT_NO", proNo);
			returnMap.put("STEP_CD", stepCd);
			
			//return proNo.toString();
			return returnMap;
		}
		

	}
	
	public List<Map<String, Object>> checkBindInfo(Map<String, Object> param){
		
		return mapper.checkBindInfo(param);
	}
	
	
	/**
	 * 결합신청자 저장 - 생성/수정
	 * @param param
	 */
	
	public String saveAplInfo(Map<String, Object> param) {
		
		Object userNo = commonService.getLoginUserNo();
		param.put("USR_NO", userNo);
		Object mainAplNo = mapper.selectMainAplNo(param);
		
		if(mainAplNo == null) {
		    param.put("BIND_WAY_CD", "M");
		} else if(mainAplNo != null && !mainAplNo.equals(param.get("APL_NO"))){
		    param.put("BIND_WAY_CD", "J");
		}
		
		if(param.get("APL_NO") == null || "".equals(param.get("APL_NO"))) {
			Object aplNo = mapper.selectMaxAplNo();
			param.put("APL_NO", aplNo);
			
			mapper.insertAplInfo(param);
			return aplNo.toString();
		} else {
			mapper.updateAplInfo(param);
			return param.get("APL_NO").toString();
		}
	}
	
	
	/**
	 * 헤더 정보 조회
	 * @param filePath
	 * @param isHeader
	 * @param terminated
	 * @param enclosed
	 * @param characterset
	 * @return  Map 
	 * 			List<String> HEADER_INFO 헤더 정보
	 *			String LINEED 라인정보
	 * 				
	 * 
	 * @throws IOException
	 */
	public Map<String, Object> getCSVHeaderList(String filePath, String isHeader, String terminated, String enclosed, String characterset) throws IOException {
		
		Map<String, Object> returnMap = new HashMap<String, Object>();

		List<String> headerInfo = new ArrayList<>();
		
		String splitBy = terminated;
		
		File fileDir = new File(filePath);
		InputStreamReader isr = new InputStreamReader(new FileInputStream(fileDir), characterset);
		BufferedReader br = new BufferedReader(isr);
		
		String header = ""; 
		
		boolean enclosedEnd = true;
		
		int headerCnt = getLineInfo(br, headerInfo, header, enclosedEnd, splitBy, isHeader, enclosed, characterset, 0);
		
		if( headerCnt > 1 && "1".equals(isHeader) ) {
			returnMap.put("HEADER", headerCnt);
		}
		
		//라인변경이 도스인지 윈도인지 체크
		InputStreamReader isr1 = new InputStreamReader(new FileInputStream(fileDir), characterset);
		BufferedReader br1 = new BufferedReader(isr1);
		String line = br1.readLine();
		
		if(line == null) {
			line = "";
		}
		
		InputStreamReader isr2 = new InputStreamReader(new FileInputStream(fileDir), characterset);
		BufferedReader br2 = new BufferedReader(isr2);
		char[] cbuf = new char[line.getBytes().length + 5];
		br2.read(cbuf);
		
		StringBuffer sb = new StringBuffer(line.getBytes().length + 5);
		sb.append(cbuf, 0, line.getBytes().length + 4);
		String line2 = sb.toString();
		
		if(line2.indexOf("\r\n") > -1) {
			returnMap.put("LINEED", "rn");
		}else {
			returnMap.put("LINEED", "n");			
		}
		
		br.close();
		isr.close();
		
		isr1.close();
		br1.close();
		
		isr2.close();
		br2.close();
		
		returnMap.put("HEADER_INFO", headerInfo);
		returnMap.put("HEADER_CNT", headerCnt);
		
		return returnMap;
	}
	
	public Map<String, Object> getOrgTableData(Map<String, Object> param){
		
		Map<String, Object> infoMap = mapper.selectOrgTblData(param);
		
		if(infoMap == null) {
			infoMap = new HashMap<String, Object>();
		}
		
		return infoMap;
	}
	
	/**
	 * CSV 파일 업로드 처리
	 * @param request
	 * @return
	 */
	public Map<String, Object> csvDataUpload(MultipartHttpServletRequest request) {
		
		String header = request.getParameter("HEADER");
		String terminated = request.getParameter("TERMINATED");
		String characterset = request.getParameter("CHARACTERSET");
		String enclosed = request.getParameter("ENCLOSED");
		Object prjctNo = request.getParameter("PRJCT_NO");
		Object usrNo = commonService.getLoginUserNo();
		String flTyCd = request.getParameter("FL_TY_CD");
		//String step = request.getParameter("STEP_CD");
		//String nowStep = request.getParameter("PRO_STEP_CD");
		//String idinfofileNm = request.getParameter("IDINFOFILE_NM");
		//String idinfoClcPrps = request.getParameter("IDINFO_CLC_PRPS");
		String lineEd = "n";
		
		Object aplNo = request.getParameter("APL_NO"); // 결합신청자 번호 가져오기
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		//상태변경
		/*
		 * if(Integer.parseInt(step) >= Integer.parseInt(nowStep)) {
		 * param.put("STEP_CD", step); }
		 */
		
		param.put("USER_NO", usrNo);
		param.put("USR_NO", usrNo);
		
		try {
			
			//파일 저장
			Map<String, Object> saveFilsReturnMap = commonService.saveFils(request, prjctNo, "121", true);
			
			Object fileID = saveFilsReturnMap.get("FL_ID");
			Object fileNo = new Object();
			
			fileNo = saveFilsReturnMap.get("FL_NO"); 

			//파일 정보 조회
			List<Map<String, Object>> fileInfos = commonService.getFileInfos(fileID);
			if(fileInfos != null && fileInfos.size() > 0) {
				Map<String, String> fileInfo = commonMapper.getFileInfo(fileInfos.get(0));
				//헤더 정보 조회
				Map<String, Object> headerInfoMap = getCSVHeaderList(fileInfo.get("FL_PTH"), header, terminated, enclosed, characterset);
				
				List<String> headerList = (List<String>)headerInfoMap.get("HEADER_INFO");
				lineEd = (String)headerInfoMap.get("LINEED");
				
				//테이블 생성
				String tableName = commonService.getCreatRawTblNm(fileNo);
				commonService.dropProjectTable(tableName);
				
				param.put("TABLENAME", tableName);
                param.put("TBL_NM", tableName);  
				param.put("COLUMN_LIST", headerList);
                param.put("FILE_FULL_PATH", fileInfo.get("FL_PTH")); 
                param.put("PRJCT_NO", prjctNo);
                param.put("ORG_TBL", tableName);
                param.put("ORG_FILE_ID", fileInfo.get("FL_ID"));
                param.put("ENCD", characterset);
                param.put("LINEED", lineEd);
                param.put("SPRTR", terminated);
                param.put("QOTAT", enclosed);
                param.put("CLM_NM_YN", header);
                param.put("FL_NO", fileNo);		
                
				Map<String,Object> codeMap = commonService.getCodeInfo("FILE_SETTING", "CMM_CSV_NULL_SETTING");
				
				if(codeMap != null) {
					param.put("NULL_SETTING", codeMap.get("META_1"));
				}
				
				mapper.createDataTable(param);
				
				//컬럼 생성 (insert into FL_CLM_INFO, 결합신청자 번호, 프로젝트번호, 파일번호) 
				insertProjectColumnList(headerList, prjctNo, aplNo, fileNo, flTyCd);

                //CSV 파일 업로드
				mapper.loadCsvDataInfileExcute(param);
				
				//파일 파기처리
				//commonService.destructionFile(fileInfo.get("FL_PTH"));
				
//				param.put("IDINFOFILE_NM", idinfofileNm);
//				param.put("IDINFO_CLC_PRPS", idinfoClcPrps);
				
				//mapper.updateOrgTbl(param);
				
				// 파일 업로드 로우 수 업데이트
				param.put("UPL_CNT", commonMapper.selectTableRowCnt(param));
				mapper.updateUploadCount(param);
			}
			
			log.debug("confirm : " + param.entrySet());
			param.put("RETURN_CODE", "0");
			
		} catch (IOException e) {
		    log.debug(e.getMessage());
		} catch (PAMsException e) {
		    log.debug(e.getMessage());
		    param.put("RETURN_CODE", "500");
		}
		
		return param;
	}
	
	/**
	 * 데이터 업로드 저장
	 * @param param
	 */
	public void saveUploadData(Map<String, Object> param) {
		
		Object userNo = commonService.getLoginUserNo();
		param.put("USER_NO", userNo);
		
		mapper.updateUploadData(param);
		
		//가명신청건 검토 없이 다음 프로세스 처리
		int stepCd = 100;
		
		if(param.get("STEP_CD") instanceof String) {
			stepCd = Integer.parseInt((String)param.get("STEP_CD"));
		}else if(param.get("STEP_CD") instanceof Integer){
			stepCd = (Integer)param.get("STEP_CD");
		}
		
		if(stepCd >= 200) {
			
			//업로드 CSV 파일 완전 삭제 처리
			deleteOrgFile(param.get("PRJCT_NO"));
			
			param.put("PROJECT_NO", param.get("PRJCT_NO"));
			param.put("STEP_CD", "204");
			
			commonService.updateProjectStep(param);	
		}
	}
	
	/**
	 * 데이터 업로드 저장
	 * @param param
	 */
	public int saveConsistencyCheck(Map<String, Object> param) {
		
		Object userNo = commonService.getLoginUserNo();
		
		param.put("USER_NO", userNo);
		
		mapper.insertConsistencyCheck(param);
		
		int prcsSttCD = 100;
		
		if(param.get("PRCS_STT_CD") instanceof String) {
			prcsSttCD = Integer.parseInt((String)param.get("PRCS_STT_CD"));
		}else if(param.get("PRCS_STT_CD") instanceof Integer){
			prcsSttCD = (Integer)param.get("PRCS_STT_CD");
		}
		
		//반려
		if(prcsSttCD == 900) {
			String rvwSnStr = (String)param.get("RVW_SN");
			
			int rvwSn = Integer.parseInt(rvwSnStr);
			
			param.put("NEW_RVW_SN", rvwSn + 1);
			
			commonMapper.updateProjectColumnRvwSn(param);	
			
			param.put("STEP_CD", "140");
			
		//승인
		}else {
			
			param.put("STEP_CD", "203");
			
			//업로드 CSV 파일 완전 삭제 처리
			deleteOrgFile(param.get("PROJECT_NO"));
			
		}
		
		commonService.updateProjectStep(param);	
		
		return 0;
	}
	
	/**
	 * 원본 파일 삭제
	 * @param prjctNo
	 */
	public void deleteOrgFile(Object prjctNo) {

		Map<String, Object> param = new HashMap<String, Object>();
		
		param.put("PRJCT_NO", prjctNo);
		
		List<Map<String, Object>> orgTblDataList = mapper.selectOrgTblDataList(param);
		
		if(orgTblDataList != null && orgTblDataList.size() > 0) {
		    
		    for(Map<String, Object> orgTblData : orgTblDataList) {
		     
	            log.debug("confirm1 : " + orgTblData.entrySet());
	            log.debug("confirm2 : 파일 완전삭제 로직 시작");
	            
	            //사후관리 이력 획득을 위한 파일 정보 획득
	            List<Map<String,Object>> fileList = commonService.getFileInfos(orgTblData.get("FILE_ID"));
	            StringBuffer strFileDstrc = new StringBuffer();
	            
	            for(Map<String,Object> file : fileList) {
	                strFileDstrc.append(strFileDstrc.length() <= 0?"":",")
	                .append(file.get("FL_NM"));
	            }
	            
	            commonService.destructionFiles(prjctNo, orgTblData.get("FILE_ID"));
	            
	            //원본 파일 파기 후 파기 이력 사후관리 이력에 포함
	            SimpleDateFormat fm = new SimpleDateFormat ( "yyyy-MM-dd");         
	            Date nowDate = new Date();
	            Map<String,Object> codeMap = new HashMap<String,Object>();
	            codeMap.put("GRP_CD", "DATA_DESTORY");
	            codeMap.put("META_3", "Y");
	            List<Map<String, Object>> codeList = commonService.getCodeList(codeMap); 
	            Map<String,Object> code = codeList.get(0);
	            
	            param.put("PRJCT_NO", prjctNo);
	            param.put("MG_DE",fm.format(nowDate));
	            param.put("MG_GOV",code.get("META_2").toString());
	            param.put("INFO","원본파일 : "+strFileDstrc.toString());
	            param.put("DES_YN", "Y");
	            param.put("MG_TYPE","1");
	            
	            carryoutMapper.insertPostManagement(param);		        
		    }
		}
	}
	
	public void deleteAplInfo(Map<String, Object> param) {
		Map<String, Object> deleteFileInfoparam = new HashMap<String, Object>();
		
		Object aplNo = param.get("APL_NO");
		Object proNo = param.get("PRJCT_NO");
		
		//결합신청자 및 해당 결합신청자가 업로드한 데이터의 컬럼 정보 삭제
		mapper.deleteAplInfo(aplNo); 
		mapper.deleteFileClmnInfo(aplNo); 
		
		//해당 결합신청자가 업로드한 파일 삭제
		deleteFileInfoparam.put("APL_NO", aplNo);
		deleteFileInfoparam.put("PRJCT_NO", proNo);
		mapper.deleteAplFileInfo(deleteFileInfoparam);
	}
	
	/**
	 * 결합신청자 데이터 정보 조회
	 * @param 
	 *     FL_NO
	 *     FL_TY_CD
	 * @return
	 * 	   FL_NO
	 *     PROJECT_COLUMNS
	 *     PROJECT_ROWDATAS
	 *     TBL_COLUMNS
	 *     TBL_ROWS
	 */
	public Map<String, Object> getDataInfo(Map<String, Object> param) {
	    Map<String, Object> returnMap = new HashMap<String, Object>();
	    
	    if(param.get("FL_NO") == null) {
	        return null;
	    }
	    String fileName = commonService.getFileNm(param);
	    
	    Object fileId= mapper.getFileId(param);
	    Object rowCnt = commonMapper.selectProjectRowCnt(param);
	    
	    Map<String, Object> flInfoMap = mapper.selectFlInfo(param);
	    
	    List<Map<String, Object>> projectColumns = commonService.getProjectColumns(param);
        List<Map<String, Object>> projectRowDatas = commonService.selectCmbProjectRowDatas(param, projectColumns);
        
		returnMap.put("FL_NO", param.get("FL_NO"));
		returnMap.put("FL_INFO", flInfoMap);
        returnMap.put("PROJECT_COLUMNS", projectColumns);
        returnMap.put("TBL_ROW", rowCnt);
        returnMap.put("FL_ID", fileId);
        returnMap.put("PROJECT_ROWDATAS", projectRowDatas);
        //returnMap.put("TBL_COLUMNS", projectColumns.size());
        returnMap.put("FL_NM", fileName);
        return returnMap;
	}

	/**
	 * 대상선정 , 상태 저장
	 * @param
	 *		UPLOAD_DATA_LIST
	 *		PRJCT_NO
	 * @return
	 *
	 */
	public List<String> saveSelectionObject(Map<String, Object> param) {
		List<String> fileNoList = new ArrayList<>();
		Object userNo = commonService.getLoginUserNo();
		if(param.get("UPLOAD_DATA_LIST") != null) {
			List<Map<String ,Object>> dataList =  (List<Map<String ,Object>>)param.get("UPLOAD_DATA_LIST");

			for(Map<String ,Object> dataMap : dataList) {
				if(dataMap != null) {
					dataMap.put("USR_NO", userNo);
					dataMap.put("PRJCT_NO", param.get("PRJCT_NO"));

					mapper.updateSelectObject(dataMap);
					fileNoList.add(dataMap.get("FL_NO").toString());
				}
			}
		}
		if(fileNoList != null){
			// fileNoList FL_NO 중복제거
			Set<String> fileNoSet = new HashSet<>(fileNoList);
			List<String> returnList = new ArrayList<>(fileNoSet);
			return returnList;
		}
		return null;
	}

	/**
	 * 전처리 요청
	 * @param
	 * 		PRJCT_TY_CD
	 * 		FL_NO_LIST
	 *		STEP_CD
	 *	 	PRJCT_NO
	 *
	 */
	public void requestDataPreprocessing(Map<String, Object> param) {

		Object userNo = commonService.getLoginUserNo();
		param.put("USR_NO", userNo);
		commonService.updateProjectStep(param);

		if(param.get("FL_NO_LIST") != null) {
			List<String> fileNoList = (List<String>)param.get("FL_NO_LIST");
			String projectTypeCode = param.get("PRJCT_TY_CD").toString();
			for(String fileNo : fileNoList) {
				//기존 테이블 삭제
				String tableNm = commonService.getCreatProjectTableNm(fileNo, projectTypeCode);
				commonService.dropProjectTable(tableNm);

				param.put("FL_NO", fileNo);
				param.put("CHARGER", "시스템");
				commonService.requestScheduleProcessing(param, true);
			}
		}
	}

	/** 전처리 스케줄 진행도 확인
	 *
	 * @param
	 * @return
	 */
	public Map<String, Object> getProgressRate(Map<String, Object> param) {
		return mapper.selectProgressRate(param);
	}

	/** 전처리 스케줄 결합신청자 별 진행도 확인
	 *
	 * @param
	 * @return
	 */
	public List<Map<String, Object>> getProgressList(Map<String, Object> param) {
		return mapper.selectProgressList(param);
	}


}
