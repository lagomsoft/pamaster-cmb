package kr.co.pamaster.bind.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BindMapper {
    
    public Object selectProjectStep(Map<String, Object> param);
    
    public Object selectBindClmNm(Map<String, Object> param);
    
    public Object selectPrjctDtl(Map<String, Object> param);
    
    public Map<String, Object> selectProjectData(Map<String, Object> param);
    
    public Map<String, Object> selectPrebindAplInfoDetail(Map<String, Object> param);
    
    public Map<String, Object> selectProjectDetail(Map<String, Object> param);
    
    public Map<String, Object> selectMainData(Map<String, Object> param);
    
    public Map<String, Object> selectBindDataFileInfo(Map<String, Object> param);
    
    public List<Map<String, Object>> selectPrivacyObjNm(Map<String, Object> param);
    
    public List<Map<String, Object>> selectColumnList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectDataList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectPrebindAplInfoList(Map<String, Object> param);
    
    public List<Map<String, Object>> selectBindKey(Map<String, Object> param);
    
    public List<Map<String, Object>> selectBindData(Map<String, Object> param);
    
    public List<Map<String, Object>> selectBindInfoList(Map<String, Object> param);
    
    public void insertPrebindPrjctDefinition(Map<String, Object> param);
    
    public void updatePrebindAplDefinition(Map<String, Object> param);
    
    public void insertPrebindFlDefinition(Map<String, Object> param);
    
    public void insertKeyPrebindBindTable(Map<String, Object> param);
}
