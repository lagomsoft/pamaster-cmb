package kr.co.pamaster.bind.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import kr.co.pamaster.bind.service.BindService;
import kr.co.pamaster.common.service.CommonService;

@Controller
public class BindController {
    
    @Autowired
    BindService service;
    
    @Autowired
    CommonService commonService;
    
    /**
     * 모의결합 설정 
     * 
     * @param model
     * 
     * @return
     */
    @GetMapping("/bind/prebindDefinition")
    private ModelAndView prebindDefinition(Model model) {

        ModelAndView mav = new ModelAndView();

        mav.setViewName("bind/prebindDefinition");
        
        mav.addObject("BIND_TY_CD", commonService.getCodeList("BIND_TY_CD"));
        
        return mav;
    }
    
    /**
     * 모의결합 수행 
     * 
     * @param model
     * 
     * @return
     */
    @GetMapping("/bind/prebindProcessing")
    private ModelAndView prebindProcessing(Model model) {

        ModelAndView mav = new ModelAndView();

        mav.setViewName("bind/prebindProcessing");

        return mav;
    }
    
    /**
     * 모의결합 설정 조회
     * @param Map<String, Object>
     *     PRJCT_NO 
     *     
     * @return 
     *     List<Map<String, Object>>
     */
    @PostMapping("/bind/getPrebindAplInfoList")
    private @ResponseBody List<Map<String, Object>> getPrebindDefinition(@RequestBody Map<String, Object> param) {
        
        return service.getPrebindAplInfoList(param);
    }
    
    /**
     * 모의결합 설정 상세 조회
     * @param Map<String, Object>
     *     PRJCT_NO 
     *     
     * @return 
     *     Map<String, Object>
     */
    @PostMapping("/bind/getPrebindAplInfoDetail")
    private @ResponseBody Map<String, Object> getPrebindDefinitionDetail(@RequestBody Map<String, Object> param) {
        
        return service.getPrebindAplInfoDetail(param);
    }
    
    /**
     * 모의결합 설정 저장
     * @param List<Map<String, Object>>
     *     APL_NO 
     *     FL_TY_CD
     *     
     * @return Map<String, Object>
     *     SUCCESS 
     */
    @PostMapping("/bind/putPrebindConfigureSave")
    private @ResponseBody Map<String, Object> putPrebindConfigureSave(@RequestBody Map<String, Object> param) {
        Map<String, Object> resultMap = new HashMap<>();
        
        service.putPrebindConfigureSave(param);
        
        resultMap.put("SUCCESS", 1);
        
        return resultMap;
    }    
    
    /**
     * 모의결합 진행 정보 조회
     * @param 
     *     PRJCT_NO
     *     FL_TY_CD
     * @return 
     *      
     */
    @PostMapping("/bind/getPrebindProcessingInfo")
    private @ResponseBody Map<String, Object> getPrebindProcessingInfo(@RequestBody Map<String, Object> param) {
        
        return service.getPrebindProcessingInfo(param);
    }
    
    /**
     * 프로젝트 상태 코드 조회
     * @param 
     *     PRJCT_NO
     * @return 
     *     STEP_CD
     */
    @PostMapping("/bind/getProjectStep")
    private @ResponseBody Map<String, Object> getProjectStep(@RequestBody Map<String, Object> param) {
        Map<String, Object> returnMap = new HashMap<>();
        returnMap.put("STEP_CD", service.getProjectStep(param));
        
        return returnMap;
    }
    
    /**
     * 결합 데이터 파일 반출 기본 정보 조회
     * @param 
     *     PRJCT_NO
     * @return 
     *     
     */
    @PostMapping("/bind/getBindDataFileInfo")
    private @ResponseBody Map<String, Object> getBindDataFileInfo(@RequestBody Map<String, Object> param) {

        return service.getBindDataFileInfo(param);
    }    
}
