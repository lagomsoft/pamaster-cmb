package kr.co.pamaster.bind.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.pamaster.bind.mapper.BindMapper;
import kr.co.pamaster.common.service.CommonService;

@Service
public class BindService {
    
    @Autowired
    private BindMapper mapper;
    
    @Autowired
    private CommonService commonService;
    
    /**
     * 모의결합 설정 조회
     * @param Map
     *      PRJCT_NO(프로젝트 번호)
     *      PARAM(파라미터 예시)
     * @return List  
     *     프로젝트 정보 
     */          
    public List<Map<String, Object>> getPrebindAplInfoList(Map<String, Object> param) {
        
        return mapper.selectPrebindAplInfoList(param);
    }
    
    /**
     * 모의결합 설정 상세 조회
     * @param 
     *      FL_NO
     *      FL_TY_CD 
     * @return 
     *      Map<String, Object>    
     */          
    public Map<String, Object> getPrebindAplInfoDetail(Map<String, Object> param) {
        Map<String, Object> resultMap = new HashMap<>();
        
        resultMap.put("APL_INFO", mapper.selectPrebindAplInfoDetail(param));
        
        if( "K".equals(param.get("FL_TY_CD"))) {
            param.put("TBL_NM", commonService.getCreatKeyTblNm(param.get("FL_NO")));
        } else {
            param.put("TBL_NM", commonService.getCreatDataTblNm(param.get("FL_NO")));
        }
        
        List<Map<String, Object>> columnList = mapper.selectColumnList(param);
        
        if(columnList == null || columnList.size() <= 0) {
            return resultMap;
        }
        
        param.put("COLUMN_LIST", columnList);
        
        resultMap.put("COLUMN_LIST", columnList);
        
        resultMap.put("CLM_CNT", columnList.size());
        
        resultMap.put("DATA_LIST", mapper.selectDataList(param));
        
        return resultMap;
    }
    
    /**
     * 모의결합 설정 저장
     * @param
     *      UPDATE_GRID_LIST
     *      SAVE_CD
     *      STEP_CD
     */
    public void putPrebindConfigureSave(Map<String, Object> param) {
        // 노이즈 표본 설정
        //mapper.insertPrebindPrjctDefinition(param); 
        
        Object usrNo = commonService.getLoginUserNo();
        
        List<Map<String, Object>> updateList = (List<Map<String, Object>>) param.get("UPDATE_LIST");
        
        if(updateList != null && updateList.size() > 0) {
            
            for(int udi = 0 ; udi < updateList.size(); udi++) {
                Map<String, Object> updateMap = updateList.get(udi);
                updateMap.put("USR_NO", usrNo);
                
                mapper.updatePrebindAplDefinition(updateMap);
            }
            
        }
        
        
        //임시저장
        if( "T".equals(param.get("SAVE_CD"))) {
            return;
        }
        
        
        // 프로젝트 단계 업데이트
        if( "K".equals(param.get("FL_TY_CD")) ) {
            
            param.put("STEP_CD", "250");
            commonService.updateProjectStep(param);
            
        } else if( "D".equals(param.get("FL_TY_CD")) ) {
            
            param.put("STEP_CD", "450");
            commonService.updateProjectStep(param);
        }
        
        
        commonService.requestScheduleProcessing(param, true);
    }
    
    /**
     * KEY TABLE 이름 생성
     * @param
     *     FL_NO
     * @return
     *     KEY_{FL_NO}
     */
    public Object createKeyTableNm(Object flNo) {
        String keyTableNm = "KEY_" + flNo;
        
        return keyTableNm;
    }
    
    /**
     * DATA TABLE 이름 생성
     * @param
     *     FL_NO
     * @return
     *     DATA_{FL_NO}
     */
    public Object createDataTableNm(Object flNo) {
        String dataTableNm = "DATA_" + flNo;
        
        return dataTableNm;
    }
    
    /**
     * 결합진행 정보 조회
     * @param
     *     PRJCT_NO
     *     FL_TY_CD
     * @return
     *     Map<String, Object>
     */
    public Map<String, Object> getPrebindProcessingInfo(Map<String, Object> param) {
        // 파일 다운로드 위한 필수 로직
        HttpSession session = commonService.getLoginSession();
        session.setAttribute("INDVDLINFO_CHECK_Y", "Y");
        
        //결합키(데이터) 정보 List
        List<Map<String, Object>> bindAplInfoList = mapper.selectPrebindAplInfoList(param);
        
        // 메인 데이터 정보
        Map<String, Object> mainDataInfo = mapper.selectMainData(param);
        param.put("MAIN_DATA_INFO", mainDataInfo);
        
        // TODO mapper의 JOIN으로 변경 
        for(int i = 0 ; i<bindAplInfoList.size() ; i++) {
            Map<String, Object> bindAplInfoMap = bindAplInfoList.get(i);
            bindAplInfoMap.put("BIND_WAY", commonService.selectCodeNm("BIND_TY_CD", bindAplInfoMap.get("BIND_WAY_CD")));
            
            bindAplInfoList.set(i, bindAplInfoMap);
        }
                
        param.put("BIND_INFO_LIST", bindAplInfoList);

        //프로젝트 진행상태 Map        
        param.put("PRJCT_INFO", mapper.selectProjectData(param));
        
        param.put("NOISE_SAMPLE_CONFIG", noiseAndSampleConfiguration());
        
        return param;
    }
    
    /**
     * 프로젝트 상태 조회
     * @param
     *     PRJCT_NO
     * @return
     *     STEP_CD
     */
    public Object getProjectStep(Map<String, Object> param) {
        
        return mapper.selectProjectStep(param);
    }
 
    /**
     * 노이즈 샘플 설정
     *  
     * @return
     *      NOISE_TY
     *      NOISE_VAL
     *      SAMPLE_VAL
     *      NOISE_PERCENT
     *      SAMPLE_TY
     *      SAMPLE_VAL
     *      SAMPLE_PERCENT
     */
    public Map<String, Object> noiseAndSampleConfiguration() {
        // Current noise and sample configuration
        Map<String, Object> resultNoiseMap = commonService.getCodeInfo("NOISE_SAMPLE_CD", "NOISE_TY");
        Map<String, Object> resultSampleMap = commonService.getCodeInfo("NOISE_SAMPLE_CD", "SAMPLE_TY");
        Map<String, Object> param = new HashMap<>();
        
        param.put("NOISE_TY", resultNoiseMap.get("CD_NM"));
        param.put("NOISE_VAL", resultNoiseMap.get("META_1"));
        param.put("NOISE_PERCENT", resultNoiseMap.get("META_2"));
        
        param.put("SAMPLE_TY", resultSampleMap.get("CD_NM"));
        param.put("SAMPLE_VAL", resultSampleMap.get("META_1"));
        param.put("SAMPLE_PERCENT", resultSampleMap.get("META_2"));
        
        return param;
        
    }
    
    /**
     * 노이즈 샘플 설정
     * @param
     *      PRJCT_NO
     * @return
     *      
     */    
    public Map<String, Object> getBindDataFileInfo(Map<String, Object> param) {
        Map<String, Object> resultMap = new HashMap<>();
        
        
        resultMap.put("NOW_DATE", commonService.currentDate());
        
        resultMap.put("PRIVACY_DTL", mapper.selectPrivacyObjNm(param));
        
        resultMap.put("PRJCT_DTL", mapper.selectPrjctDtl(param));
        
        resultMap.put("BIND_FL_INFO", mapper.selectBindDataFileInfo(param));
        

        return resultMap;
    }
}