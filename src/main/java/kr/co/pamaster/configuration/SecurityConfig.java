package kr.co.pamaster.configuration;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import kr.co.pamaster.user.service.UserService;
import kr.co.pamaster.util.springSecurityHandler.PamsFailureHandler;
import kr.co.pamaster.util.springSecurityHandler.PamsLoginSuccessHandler;
import kr.co.pamaster.util.springSecurityHandler.PamsLogoutSuccessHandler;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserService userService;
	
	@Bean
    public PasswordEncoder passwordEncoder() {
		 
		return new StandardPasswordEncoder();
//		return new BCryptPasswordEncoder();
    }
	
	@Override
    public void configure(WebSecurity web) throws Exception
    {
        // static 디렉터리의 하위 파일 목록은 인증 무시 ( = 항상통과 )
        web.ignoring().antMatchers(
        		"/css/**",
        		"/fonts/**",
        		"/images/**",
        		"/img/**",        		
        		"/js/**",
        		"/main/css/**",
        		"/main/js/**",
        		"/plugins/**",
        		"/sample/**",
        		"/*.ico");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception { 
    	
        http.csrf().disable().authorizeRequests()
        		.antMatchers("/**").permitAll()
		        .antMatchers("/**/**").permitAll()
		   	 	.antMatchers("/**/**/**").permitAll()
        	.and()
        	// 로그인 설정
                .formLogin()
                .loginPage("/user/login")
                .successHandler(new PamsLoginSuccessHandler())
                .failureUrl("/user/denied")
                .failureHandler(new PamsFailureHandler())
            .and() // 로그아웃 설정
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/user/logout"))
                .logoutSuccessHandler(new PamsLogoutSuccessHandler())
                .logoutSuccessUrl("/user/login")
                .invalidateHttpSession(false)
            .and()
            // 403 예외처리 핸들링
                .exceptionHandling().accessDeniedPage("/user/denied")
        	.and()
        		.sessionManagement();

        	/*	중복 로그인 처리 authInterceptor 에서 처리하도록 수정
        		.maximumSessions(1)
        		.maxSessionsPreventsLogin(false);
        	*/	
        /* GS인증 : HSTS 설정
        http
            .headers()
            .cacheControl()
            .and()
            .frameOptions()
            .and()
            .contentTypeOptions()
            .and()
            .httpStrictTransportSecurity() 
            .maxAgeInSeconds(31536000)
            .includeSubDomains(true)
            .preload(true);
        */
        http.exceptionHandling()
        	.authenticationEntryPoint(loginUrlAuthenticationEntryPoint());
	}
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
    }
    
    @Bean
    public LoginUrlAuthenticationEntryPoint loginUrlAuthenticationEntryPoint(){
        
        LoginUrlAuthenticationEntryPoint entry = new LoginUrlAuthenticationEntryPoint("/user/login"){
            @Override
            public void commence(final HttpServletRequest request, final HttpServletResponse response,
                    final AuthenticationException authException) throws IOException, ServletException{
                
                String ajaxHeader = request.getHeader("X-Requested-With");
                if(ajaxHeader != null && "XMLHttpRequest".equals(ajaxHeader)){
                    //InsufficientAuthenticationException
                    response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
                }
                else{
                    super.commence(request, response, authException);
                }
            }
        };
        return entry;
    }

}