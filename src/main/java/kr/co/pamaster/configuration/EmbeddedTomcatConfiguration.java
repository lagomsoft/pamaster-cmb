/**
 * 
 */
package kr.co.pamaster.configuration;

import java.util.HashMap;
import java.util.Map;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.webresources.ExtractingRoot;
import org.springframework.boot.web.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ubireport.server.UbiServer4;

/**
 * @date 2021. 5. 27.
 * @author 이민구
 * @name kr.co.lagomsoft.configuration.EmbeddedTomcatConfiguration
 * @comment 
 */
@Configuration
public class EmbeddedTomcatConfiguration {
 
  @Bean
  public ServletWebServerFactory servletContainer() {
    TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
      @Override
      protected void postProcessContext(Context context) {
        context.setResources(new ExtractingRoot());
        /* GS인증 코드 HTTP redirect https
        SecurityConstraint securityConstraint = new SecurityConstraint();
        securityConstraint.setUserConstraint("CONFIDENTIAL");
        SecurityCollection collection = new SecurityCollection();
        collection.addPattern("/*");
        securityConstraint.addCollection(collection);
        context.addConstraint(securityConstraint);
        */
      }
    };

    //tomcat.addAdditionalTomcatConnectors(createSslConnector());
    return tomcat;
  }

  private Connector createSslConnector(){
    Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
    connector.setPort(8181);
    connector.setScheme("http");
    connector.setSecure(false);
    connector.setRedirectPort(8443);
    return connector;
  }
  
  @Bean
  public WebServerFactoryCustomizer<TomcatServletWebServerFactory> servletContainerCustomizer() {
    return new WebServerFactoryCustomizer<TomcatServletWebServerFactory>() {

      @Override
      public void customize(TomcatServletWebServerFactory container) {
        container.addContextCustomizers(
            new TomcatContextCustomizer() {
              @Override
              public void customize(Context cntxt) {
                cntxt.setReloadable(false);
              }
            });
      }
    };
  }
  
  @Bean
  public ServletRegistrationBean<UbiServer4> getServletRegistrationBean() {
      ServletRegistrationBean<UbiServer4> registrationBean = new ServletRegistrationBean<>(new UbiServer4());
      registrationBean.addUrlMappings("/UbiServer");
      registrationBean.setName("UbiServer");
      
      Map<String, String> initParam = new HashMap<>();
//      initParam.put("isAbsolutePath", "false");
//      initParam.put("propertyPath", "/lib");
//      initParam.put("libraryPath", "/lib");
      initParam.put("isAbsolutePath", "true");
      initParam.put("propertyPath", "/pamaster-m/report/UbiService");
      initParam.put("libraryPath", "/pamaster-m/report/UbiService");
    
      registrationBean.setInitParameters(initParam);
      
      return registrationBean;
  }
  
}