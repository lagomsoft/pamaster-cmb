/**
 * 
 */
package kr.co.pamaster.configuration;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import kr.co.pamaster.interceptor.AuthInterceptor;
import kr.co.pamaster.interceptor.MenuInfoInterCeptor;
import kr.co.pamaster.interceptor.MultiLoginInterceptor;

/**
 * @date 2022. 1. 11.
 * @author 
 * @name kr.co.pamaster.configuration.WebMvcConfig
 * @comment 
 *  Web ?��?�� �??�� 
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {	
	
	String[] excludePaths = {
			"/*.*",
			"/plugins/**",
			"/js/**",
			"/images/**",
			"/fonts/**",
			"/img/**",
			"/css/**",
			"/user/login",
			"/user/denied",
			"/user/joinUser",
			"/user/checkUserIDandEmail",
			"/user/getPwPolicy",
			"/user/findUser",
			"/user/findPw",
			"/user/checkPassword",
			"/user/ipDenied",
			"/user/logout",
			"/user/checkUserIDandEmail",
			"/user/mail",
			"/user/multiLogin",
			"/common/**",
			"/error",
			"/*",
			};	
	
	String[] excludeMenuPaths = {
			"/*.*",
			"/plugins/**",
			"/js/**",
			"/images/**",
			"/fonts/**",
			"/img/**",
			"/css/**",
			"/error",
			"/*",
			"/main/**"
			};	
	
	@Autowired
	private AuthInterceptor authInterceptor;
	
	@Autowired
	private MultiLoginInterceptor multiloginInterceptor;
	
	@Autowired
	private MenuInfoInterCeptor menuInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {		
		
		registry.addInterceptor(authInterceptor)
				.excludePathPatterns(Arrays.asList(excludePaths));
		registry.addInterceptor(multiloginInterceptor)		
				.excludePathPatterns(Arrays.asList(excludePaths));
		registry.addInterceptor(menuInterceptor)
				.excludePathPatterns(Arrays.asList(excludeMenuPaths));
				
	}
	

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		// TODO Auto-generated method stub
		WebMvcConfigurer.super.configurePathMatch(configurer);
	}
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("forward:/index.jsp");
	}
}
