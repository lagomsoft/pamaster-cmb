package kr.co.pamaster.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource("classpath:application.properties")
public class EmailConfig {
	
    @Value("${mail.smtp.port:465}")
    private int port;
    @Value("${mail.smtp.socketFactory.port:465}")
    private int socketPort;
    @Value("${mail.smtp.auth:true}")
    private boolean auth;
    @Value("${mail.smtp.starttls.enable:true}")
    private boolean starttls;
    @Value("${mail.smtp.starttls.required:true}")
    private boolean startlls_required;
    @Value("${mail.smtp.socketFactory.fallback:false}")
    private boolean fallback;
    @Value("${AdminMail.id:hjdo@lagomsoft.co.kr}")
    private String id;
    @Value("${AdminMail.password:moiwbptsxubtcbvc}")
    private String password;

	 @Bean
	 public JavaMailSender javaMailService() {
	       JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
	       javaMailSender.setHost("smtp.gmail.com");
	       javaMailSender.setUsername(id);
	       javaMailSender.setPassword(password);
	       javaMailSender.setPort(port);
	       javaMailSender.setJavaMailProperties(getMailProperties());       
	       javaMailSender.setDefaultEncoding("UTF-8");
	       return javaMailSender;
	 }
	 private Properties getMailProperties()
		{
			Properties pt = new Properties();
			pt.put("mail.smtp.socketFactory.port", socketPort); 
			pt.put("mail.smtp.auth", auth);
		    pt.put("mail.smtp.starttls.enable", starttls); 
		    pt.put("mail.smtp.starttls.required", startlls_required);
		    pt.put("mail.smtp.socketFactory.fallback",fallback);
		    pt.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			return pt;
		}
	    

}