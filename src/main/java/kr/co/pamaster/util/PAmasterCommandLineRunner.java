package kr.co.pamaster.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class PAmasterCommandLineRunner implements CommandLineRunner {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Value("${spring.datasource.url:}")
	private String url;
	

	@Value("${spring.datasource.username:}")
	private String user;
	

	@Value("${spring.datasource.password:}")
	private String passwd;
		
	@Override
	public void run(String... args) throws Exception {
		
		Connection con = null;
		Statement stmt = null;
		
		if( url != null && !"".equals(url) ) {
			
			try {
				con = DriverManager.getConnection(url, user, passwd);
				
				stmt = con.createStatement();
				
				/*
				 *  [START] 20211214 재현데이터 테이블 체크 로직
				 */
				String syntheticCheckTableNm = "SYNTHETIC_DATA";
				
				//테이블 확인
				ResultSet syntheticCheckResult = stmt.executeQuery(
						  "SELECT "
						+ "     COUNT(1)"
						+ "FROM "
						+ "  INFORMATION_SCHEMA.TABLES "
						+ "WHERE "
						+ "  TABLE_SCHEMA = 'columnstore_db' "
						+ "  AND "
						+ "  TABLE_NAME = '" + syntheticCheckTableNm + "' "
					);
				
				int syntheticCheckCount = 0;
				
				// 데이터 추출
				while (syntheticCheckResult.next())
				{
					syntheticCheckCount = syntheticCheckResult.getInt(1);
					log.debug(" TABLE_NAME :  " + syntheticCheckTableNm + " /  COUNT : " +  syntheticCheckCount);
				}
				
				if(syntheticCheckCount < 1) {
					
					//존재하지 않은 테이블 생성

					// TODO ♨ 테이블 컬럼 확인
					
//					stmt.executeQuery(
//							 "CREATE TABLE columnstore_db.SYNTHETIC_DATA (  "
//							+"  JOIN_NO bigint(20) NOT NULL COMMENT '결합번호', "
//							+"  BASE_CLMN text DEFAULT NULL COMMENT '기준 컬럼', "
//							+"  STEP_CD varchar(50) DEFAULT NULL COMMENT '상태코드', "
//							+"  EXTRC_COUNT bigint(20) DEFAULT NULL COMMENT '추출개수', "
//							+"  ATTACH_ID varchar(100) DEFAULT NULL COMMENT '반출 파일 아이디', "
//							+"  ERR_MSG text DEFAULT NULL COMMENT '에러메세지', "
//							+"  REGISTER bigint(20) DEFAULT NULL COMMENT '등록자', "
//							+"  RGSDE datetime DEFAULT NULL COMMENT '등록일', "
//							+"  UPDUSR bigint(20) DEFAULT NULL COMMENT '수정자', "
//							+"  UPDDE datetime DEFAULT NULL COMMENT '수정일', "
//							+"  PRIMARY KEY (JOIN_NO) "
//							+") ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='재현 데이터' "
//					);
					
					// TODO ♨ 그외 테이블도 생성필요
					
					
				}
				/*
				 *  [END] 20211214 재현데이터 테이블 체크 로직
				 */
				
				stmt.close();
				con.close();
				
			} catch (SQLException e) {
				
				log.error(e.getMessage());
				
			} finally {

				if(stmt != null) {
					stmt.close();				
				}
				
				if(con != null) {
					con.close();				
				}
				
			}
		}
		
		
		
	}
	
	
}
