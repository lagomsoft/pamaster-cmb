package kr.co.pamaster.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@WebListener
public class SessionListener implements HttpSessionListener {
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	public void sessionDestroyed(HttpSessionEvent event) {
		HttpSession session = event.getSession();
		
		
		if(session != null && session.getAttribute("USR_NO") != null) {
			
			Map<String, Object> param = new HashMap<>();
			Object commonObject = null;
			
			param.put("TYPE", "2");
			param.put("USR_NO", session.getAttribute("USR_NO"));
			param.put("CONECT_IP", session.getAttribute("CONECT_IP"));
			param.put("CONECT_WBSR", session.getAttribute("CONECT_WBSR"));
			
			WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(session.getServletContext());
			commonObject = wac.getBean("commonService");
			
			Class[] cClass = new Class[] {Map.class};
			
			try {
				
				Method m = commonObject.getClass().getMethod("recordHist", cClass);
				m.invoke(commonObject, param);
				
			} catch (NoSuchMethodException e) {
				log.error(" " + e.getCause());
			} catch (SecurityException e) {
				log.error(" " + e.getCause());
			} catch (IllegalAccessException e) {
				log.error(" " + e.getCause());
			} catch (IllegalArgumentException e) {
				log.error(" " + e.getCause());
			} catch (InvocationTargetException e) {
				log.error(" " + e.getCause());
			}
			
		}
    }
}
