package kr.co.pamaster.util;

import java.io.File;

public class DestructionFileTask implements Runnable {

	String destructionFilePath = null;
	String destructionProcessType = null;
	File destructionFile = null;
	
	public DestructionFileTask(String filePath, String processType) {
		destructionFilePath = filePath;
		destructionProcessType = processType;
	}
	
	public DestructionFileTask(File file, String processType) {
		this.destructionFile = file;
		destructionProcessType = processType;
	}

	@Override
	public void run() {
		if (destructionFilePath != null) {
			
			destructionFile = new File(destructionFilePath);
			
			DestructionFile df = new DestructionFile();
            df.run(destructionFile, destructionProcessType);

			
		}

	}

}
