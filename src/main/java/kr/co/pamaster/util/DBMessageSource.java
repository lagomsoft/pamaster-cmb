package kr.co.pamaster.util;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.AbstractMessageSource;
import org.springframework.stereotype.Component;

import kr.co.pamaster.common.service.CommonService;

@Component("messageSource")
public class DBMessageSource extends AbstractMessageSource {

	@Autowired
	CommonService service;
	
	private static final String DEFAULT_LOCALE_CODE = "ko";
	
	@Override
	protected MessageFormat resolveCode(String code, Locale locale) {
		
		String msg = service.getLocaleMessge(code, locale.getLanguage());
		
		if(msg == null) {
			msg = service.getLocaleMessge(code, DEFAULT_LOCALE_CODE);
		}
		
		if(msg != null) {
			return new MessageFormat(msg, locale);
		}
		
		return null;
	}
	
}
