package kr.co.pamaster.util;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
public class ExceptionController {
	//custom예외처리
	@ExceptionHandler(PAMsException.class)
	public ModelAndView pamsException(PAMsException pme) {
		ModelAndView mView=new ModelAndView();
		mView.addObject("exception", pme);
		mView.setViewName("error/data_access");

		return mView;
	}
    
}