package kr.co.pamaster.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DestructionFile {

	private final Logger log = LoggerFactory.getLogger(this.getClass());
	private int length = 1;
	
	public void run(File file, String processType) {
		
		if (file != null && file.exists()) {
			try {
			    
				RandomAccessFile raf = new RandomAccessFile(file, "rw");
				double randomDouble = Math.random();
				int randomInt = (int) (randomDouble * 10);

				int randomComplement = ~randomInt;

				randomDouble = Math.random();
				int randomInt2 = (int) (randomDouble * 10);
				
				// US DoD 5220.22-M(8-306./E)
				if ("1".equals(processType)) {
					log.debug("(One Step) US DoD 5220.22-M(8-306./E) File Destruction");
					fileDestruction(randomInt, raf);
					
					log.debug("(Two Step) US DoD 5220.22-M(8-306./E) File Destruction");
					fileDestruction(randomComplement, raf);
					
					log.debug("(Three Step) US DoD 5220.22-M(8-306./E) File Destruction");
					fileDestruction(randomInt2, raf);	
					
					log.debug("(End Step) US DoD 5220.22-M(8-306./E) File Destruction");
				// Peter Guttmann
//				} else if ("2".equals(processType)) {
					
				} else {
					for (int count = 0; count < 35; count++) {
						fileDestruction(randomInt2, raf);
					}
					log.debug("(End Step) Peter Guttmann File Destruction");
				}
				
				raf.close();
				// file Delete
                if (file != null && file.exists()) {
                    file.delete();                      
                }
				
			} catch (FileNotFoundException e) {
				log.error("DestructionFileTask FileNotFoundException :" + " " + e.getCause());
				if (file != null && file.exists()) {
					file.delete();						
				} 
			} catch (Exception e) {
				
				log.error("DestructionFileTask Exception :" + " " + e.getCause());
				
				if (file != null && file.exists()) {
					file.delete();						
				} 
			} finally {
			    
			}
		}
		
	}

	public void fileDestruction(int stringInt, RandomAccessFile raf) {
		int count = 0;
		StringBuilder sb = new StringBuilder();
		
		try {
			count =0;
			raf.seek(0);
			log.debug("confirm length : " + raf.length());
			
			if(!"".equals(raf.readLine()) && raf.readLine() != null) {
				String readLineString = raf.readLine();
				
				if(!"".equals(readLineString) && readLineString != null) {
					length = readLineString.length();
					log.debug("confirm readLint : " + readLineString);
				} 
			} else {
				raf.seek(0);
				String readLineString = raf.readLine();
				
				if(!"".equals(readLineString) && readLineString != null) {
					length = readLineString.length();
					log.debug("confirm readLint : " + readLineString);
				} 
			}
			
			
			sb.append("");
			for(int i = 0 ; i < length ; i++) {
				sb.append(stringInt);
			}
			
			String randomString = sb.toString();
			
			while(raf.getFilePointer() < raf.length()) {
				raf.seek(count);
				raf.writeChars(randomString);
				count++;
			}
			raf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error(" " + e.getCause());
		}
	}
}
