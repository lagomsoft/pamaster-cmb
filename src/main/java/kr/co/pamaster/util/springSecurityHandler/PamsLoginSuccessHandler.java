package kr.co.pamaster.util.springSecurityHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

import kr.co.pamaster.common.service.CommonService;

public class PamsLoginSuccessHandler implements AuthenticationSuccessHandler {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		resetPwCnt(request,response,authentication);
		loginHistReccord(request, response, authentication);
	}
	
	public void resetPwCnt(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		Object commonObject = null;
		
		Map<String, Object> param = new HashMap<>();

    	if(RequestContextHolder.getRequestAttributes() != null) {
    		
    		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
    		
        	if(wac != null) {
    	    	commonObject    = wac.getBean("userService");
	        	
	        	param.put("id", authentication.getName());
	        	param.put("TYPE", "1");
	        	
	    		Class[] methodClass = new Class[] {Map.class};
	    		
				try {
					
						Method m = commonObject.getClass().getMethod("userLoginInitSetting", methodClass);
						m.invoke(commonObject, param);						
					
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				} catch (InvocationTargetException e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				} catch (NoSuchMethodException e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					log.error(e.getMessage());
				}

        	}
    	}
	}
	

	public void loginHistReccord(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		Object commonObject = null;
		
		Map<String, Object> param = new HashMap<>();
		
		// login request page
		RequestCache requestCache = new HttpSessionRequestCache();
		SavedRequest savedRequest = requestCache.getRequest(request, response);
		if(savedRequest != null) {
			String targetUrl = savedRequest.getRedirectUrl();
			request.setAttribute("TARGET_URL", targetUrl);
		}
		
    	if(RequestContextHolder.getRequestAttributes() != null) {
    		
    		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
    		
        	if(wac != null) {
        		
        		
        		if( request.getSession() != null ) { 
        			
        			HttpSession session = request.getSession();
        			
        			CommonService service = new CommonService();
        			
        			String browser = service.getBrowser(request);
        			String usrIP = request.getRemoteAddr();
        			
        			commonObject   = wac.getBean("commonService");
        			
        			param.put("TYPE", "1");
        			param.put("USR_NO", session.getAttribute("USR_NO"));
					param.put("CONECT_IP", usrIP);
					param.put("CONECT_WBSR", browser);
					
					session.setAttribute("CONECT_IP", usrIP);
					session.setAttribute("CONECT_WBSR", browser);
					
        			Class[] methodClass = new Class[] {Map.class};
        			
        			try {
        				Method m = commonObject.getClass().getMethod("recordHist", methodClass);
        				
        				try {
        					Object loginSuccessUrl = m.invoke(commonObject, param);
        					response.sendRedirect(loginSuccessUrl + "");
        					
        				} catch (IllegalAccessException e) {
        					// TODO Auto-generated catch block
        					log.error(e.getMessage());
        				} catch (IllegalArgumentException e) {
        					// TODO Auto-generated catch block
        					log.error(e.getMessage());
        				} catch (InvocationTargetException e) {
        					// TODO Auto-generated catch block
        					log.error(e.getMessage());
        				}
        			} catch (NoSuchMethodException e) {
        				// TODO Auto-generated catch block
        				log.error(e.getMessage());
        			} catch (SecurityException e) {
        				// TODO Auto-generated catch block
        				log.error(e.getMessage());
        			}
        		}
        		
        	}
    	}
	}
	
}
