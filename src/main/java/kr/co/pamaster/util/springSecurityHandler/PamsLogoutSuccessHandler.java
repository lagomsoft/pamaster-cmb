package kr.co.pamaster.util.springSecurityHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class PamsLogoutSuccessHandler implements LogoutSuccessHandler {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		logoutHistReccord(request, response, authentication);
		
		HttpSession session = request.getSession();
		
		if(session != null) {
			session.invalidate();
		}
		
	}
	
	public void logoutHistReccord(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
		Object commonObject = null;
		Map<String, Object> param = new HashMap<>();
		
		Object logOutSuccessUrl = "/user/login";
		
    	if(RequestContextHolder.getRequestAttributes() != null) {
    		
    		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
    		HttpSession session = request.getSession();
    		
        	if(wac != null && session != null) {
        		
    	    	commonObject = wac.getBean("commonService");
    	    	
    	    	param.put("TYPE", "2");
    	    	param.put("USR_NO", session.getAttribute("USR_NO"));
    	    	param.put("CONECT_IP", session.getAttribute("CONECT_IP"));
    	    	param.put("CONECT_WBSR", session.getAttribute("CONECT_WBSR"));
    	    	
    	    	session.removeAttribute("USR_NO");
    	    	
    	    	Class[] cClass = new Class[] {Map.class};
    	    	
    	    	try {
    	    		Method m = commonObject.getClass().getMethod("recordHist", cClass);
    	    		
    	    		try {
    	    			logOutSuccessUrl = m.invoke(commonObject, param);
    	    			
    	    		} catch (IllegalAccessException e) {
    	    			// TODO Auto-generated catch block
    	    			log.error(e.getMessage());
    	    		} catch (IllegalArgumentException e) {
    	    			// TODO Auto-generated catch block
    	    			log.error(e.getMessage());
    	    		} catch (InvocationTargetException e) {
    	    			// TODO Auto-generated catch block
    	    			log.error(e.getMessage());
    	    		}
    	    	} catch (NoSuchMethodException e) {
    	    		// TODO Auto-generated catch block
    	    		log.error(e.getMessage());
    	    	} catch (SecurityException e) {
    	    		// TODO Auto-generated catch block
    	    		log.error(e.getMessage());
    	    	}
        	}
        	
    	} 
    	
    	response.sendRedirect( logOutSuccessUrl + "" );
	} 
}