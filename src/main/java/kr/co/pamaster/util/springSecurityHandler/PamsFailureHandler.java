package kr.co.pamaster.util.springSecurityHandler;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class PamsFailureHandler implements AuthenticationFailureHandler {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
		
	    int faileCd = loginFailiureMethod(request, exception);
	    
	    
        if(request.getSession(false) != null) {
        	request.getSession().setAttribute("LOGIN_FAIL_CD", faileCd);
        }
        
//        if(faileCd <= 1) {
        	response.sendRedirect("/user/denied");        	
//        }
        
//        request.getRequestDispatcher("/user/denied").include(request, response);
    }
	
	public int loginFailiureMethod(HttpServletRequest request, AuthenticationException exception) {
		
		int returnCd = 1;
		Object userObject = null;
		
		if(request.getParameter("username") != null && !"".equals(request.getParameter("username"))) {
			
			if(RequestContextHolder.getRequestAttributes() != null) {
				
				WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
				
				if(wac != null) {
					userObject = wac.getBean("userService");
				}
				
				Class[] cClass = new Class[] {String.class, HttpServletRequest.class};
				
				try {
					
					Method m = userObject.getClass().getMethod("checkPwFailCount", cClass);
					Object returnCodeObj = m.invoke(userObject, (String) request.getParameter("username"),request);
					
					returnCd = (int)returnCodeObj;
				} catch (NoSuchMethodException e) {
					log.error(e.getMessage());
				} catch (SecurityException e) {
					log.error(e.getMessage());
				} catch (IllegalAccessException e) {
					log.error(e.getMessage());
				} catch (IllegalArgumentException e) {
					log.error(e.getMessage());
				} catch (InvocationTargetException e) {
					log.error(e.getMessage());
				}
			}
			
		}
		
		return returnCd;
		
	}
}
