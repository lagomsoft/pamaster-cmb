package kr.co.pamaster.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class CheckLicense {
	
	private static long checkLicenseTime = 0L;
	
	private static List<String> macList = new ArrayList<String>();
	
	/**
	 * 
	 * 로컬 맥 주소를 가져오는 메소드
	 * 
	 * @return
	 * @throws UnknownHostException
	 * @throws SocketException
	 */
	 private List<String> getLocalMacAddress() throws UnknownHostException, SocketException {
	 	
		 if(macList.size() <= 0) {
			 
			 Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
			 
			 while (n.hasMoreElements()) {
				 NetworkInterface network = n.nextElement();
				 
				 byte[] mac = network.getHardwareAddress();
				 
				 StringBuilder sb = new StringBuilder();
				 
				 if(mac != null) {
					 
					 for (int i = 0; i < mac.length; i++) {
						 sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
					 }
					 
					 macList.add(sb.toString().toUpperCase());
				 }
				 
			 }
		 }
		 
		return macList;
	 }
	 
	 /**
	  * 사용자의 라이센스 체크
	  * 
	  * @param license
	  * @return
	  * @throws UnknownHostException
	  * @throws SocketException
	  */
	 private String checkLicenseString(String license) throws UnknownHostException, SocketException {
		 
		 StringBuffer macAddSb = new StringBuffer();
		 StringBuffer timeSb = new StringBuffer();

         int maxLen = license.length();
         int endLen = 0;
         
         for (int i = 0; i < maxLen; ) {
        	 
        	 if(maxLen <= endLen + 3) {
        		 macAddSb.append(license.substring(endLen, maxLen));
        		 break;
        	 }
        	 
        	 String subAdd = license.substring(endLen, endLen + 3);
        	 
        	 endLen = endLen + 3;
        	 
        	 macAddSb.append(subAdd);

			 String numStr = subAdd.replaceAll("[^0-9]","");
			 
			 if(!"".equals(numStr)) {
				 
				 int subSize = (( Integer.parseInt(numStr) ) % 3) + 1;
				 
				 if(endLen + subSize <= maxLen && timeSb.length() + subSize <= 13) {
					 
					 timeSb.append(license.substring(endLen , endLen + subSize));
					 endLen = endLen + subSize;
					 
				 }
				 
			 }
        	 
         }
         
         BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
         
         List<String> macAddList = getLocalMacAddress();
         
         if(macAddList == null || macAddList.size() == 0) {
        	 return "505";
         }
         
         for(String macAdd : macAddList) {
        	 
        	 if(passwordEncoder.matches(macAdd, macAddSb.toString())) {
        		 
        		 String tmpAddTime = "99999999999999";
        		 
        		 String timeStr = timeSb.toString() + tmpAddTime.substring(0, 13 - timeSb.length());
        		 
        		 long checkTime = Long.parseLong(timeStr);
        		 
        		 Date now = new Date();
        		 
        		 if(checkTime > now.getTime()) {
        			 checkLicenseTime = now.getTime();
        			 
        			 return "200";
        		 }else {
        			 
        			 return "408";        		 
        		 }
        	 }
         }
         
		 return "404";
	 }
	 
	 /**
	  * 
	  * 라이센스 파일 체크
	  * 
	  * @return 
	  * 			200 정상
	  * 			500 라이센스 파일을 찾을 수 없음
	  * 			408 라이센스 유효기간 만료
	  * 			505 Mac 주소 없음
	  * 			404 일치하는 라이센스 존재 하지 않음
	  * 			
	  * @throws IOException
	  */
	 public String checkLicense() throws IOException {
		 
		 String fileDir = "pamaster_license";
		 
		 File licenseFile = new File(fileDir);
		 
		 String returnCheckVal = "404";
		 String checkVal = "404";
		 
		 if(!licenseFile.exists()) {
			 fileDir = "/pamaster/server/pamaster_license";
			 licenseFile = new File(fileDir);
			 
			 if(!licenseFile.exists()) {
				 
				 return "500";
			 }
			 
		 }
		 
		 InputStreamReader isr = new InputStreamReader(new FileInputStream(fileDir));
		 BufferedReader br = new BufferedReader(isr);
		 String line = br.readLine();
		 
         while(line != null) {
        	 
        	 checkVal = checkLicenseString(line);
        	 
        	 if("200".equals(checkVal) || "505".equals(checkVal)) {
        		 returnCheckVal = checkVal;
        		 break;
        	 }
        	 
        	 if(!"404".equals(checkVal)) {
        		 returnCheckVal = checkVal;
        	 }
        	 
             line = br.readLine();
             
         }
         
         if(br != null) {
        	 br.close();        	 
         }
         
		 return returnCheckVal;
		 
	 }
	 
	 /**
	  * 확인 시작
	  * @return
	  */
	 public boolean checkTime() {
		 
    	 Date now = new Date();
    	 
    	 Calendar cal = Calendar.getInstance();
    	 
    	 cal.setTime(now);
    	 cal.add(Calendar.HOUR, -1);
    	 
    	 if( cal.getTimeInMillis() > checkLicenseTime ) {
    		 return true;
    	 } else {
    		 return false;
    	 }
	 }
}
