package kr.co.pamaster.util;

public class PAMsException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PAMsException() {
        super();
    }
	
	//생성자의 인자로 예외메세지를 전달받아서 
	public PAMsException(String msg) {
		//부모생성자에 전달하면
		super(msg);
		//message 필드에 저장된다.
	}
	
	protected PAMsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}