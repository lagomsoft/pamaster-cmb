package kr.co.pamaster.util;

import java.util.concurrent.Executor;

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class PAMsAsyncConfig extends AsyncConfigurerSupport {
	
	@Override
	public Executor getAsyncExecutor() {
		ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor(); 
		
		threadPoolTaskExecutor.setCorePoolSize(1); 
		threadPoolTaskExecutor.setMaxPoolSize(100); 
		threadPoolTaskExecutor.setThreadNamePrefix("pams-pool"); 
		
		threadPoolTaskExecutor.initialize();
		
		return threadPoolTaskExecutor; 
	}
	
	/**
	 * TODO 오류 처리
	 */
	
	@Override
	public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
		
		return super.getAsyncUncaughtExceptionHandler();
	}
		
}
