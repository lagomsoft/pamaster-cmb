package kr.co.pamaster.util.XSSFilter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Xss filter
 *
 * 
 */
public class XssFilter extends OncePerRequestFilter {
	
	public FilterConfig filterConfig;
	
	 private static final List<String> EXCLUDE_URL = Arrays.asList(
			 "/css/**",
     		"/fonts/**",
     		"/images/**",
     		"/img/**",        		
     		"/js/**",
     		"/main/css/**",
     		"/main/js/**",
     		"/plugins/**",
     		"/sample/**",
     		"/*.ico"
			 );

	public void destroy() {
	    this.filterConfig = null;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		filterChain.doFilter(new XssHttpServletRequestWrapper((HttpServletRequest) request), response);
	}
	

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		AntPathMatcher pathMatcher = new AntPathMatcher();		
		return EXCLUDE_URL.stream().anyMatch(exclude -> pathMatcher.match(exclude,request.getServletPath()));
	}
}
