package kr.co.pamaster.util.XSSFilter;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.nhncorp.lucy.security.xss.XssFilter;

public class XssHttpServletRequestWrapper extends HttpServletRequestWrapper {

	private byte[] b;
	
	private final static Logger log = LoggerFactory.getLogger("XssHttpServletRequestWrapper.class");
	
	Map<String, String[]> params;
	
	public XssHttpServletRequestWrapper(HttpServletRequest request) throws IOException {
		super(request);
		this.params = request.getParameterMap();
		
		if(request.getContentType() == null) {
			return;
		}
		
		if( request.getContentType() == null || (request.getContentType() != null && !request.getContentType().startsWith("multipart/form-data"))) {
			log.debug("Start XSS");
			XssFilter filter = XssFilter.getInstance("lucy-xss-sax.xml");
			b = new String(filter.doFilter(getBody(request))).getBytes();	
			log.debug("End XSS");
		}
	}
		
	public static String getBody(HttpServletRequest request) throws IOException {
		String body = null;
		StringBuilder stringBuilder = new StringBuilder();
		BufferedReader bufferedReader = null;
		InputStream inputStream;
		
		try {
			inputStream = request.getInputStream();
			
			if (inputStream != null) {

				bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
				char[] charBuffer = new char[128];
				int bytesRead = -1;

				while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
					stringBuilder.append(charBuffer, 0, bytesRead);
				}
			} else {
				stringBuilder.append("");
			}
		} catch (IOException ex) {
			throw ex;
		} 
		finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException ex) {
					throw ex;
				}
			}
		}
		body = stringBuilder.toString();
		body = privacyDetection(request, body);
		
		return body;
	}
	
	public static String privacyDetection(HttpServletRequest request, String body) {
		Map<String, Object> param = new HashMap<>();
		
		Object commonObject = new Object();
		Object returnObject = new Object();
		
		log.debug("Start privacy Detection : getWAC");
		HttpSession session = request.getSession();
		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(session.getServletContext());
		
		commonObject = wac.getBean("commonService");
		log.debug("Start privacy Detection : END getWAC");
		
		Class[] cClass = new Class[] {Map.class};
		
		String contentType = request.getContentType();
		String menuId = request.getRequestURI();
		String method = request.getMethod();
		
		param.put("CONTENT_TYPE", contentType);
		param.put("MENU_ID", menuId);
		param.put("CONTENTS", body);
		param.put("METHOD", method);
		
		try {
			Method m = commonObject.getClass().getMethod("privacyDetection", cClass);
			try {	
				if( body != null  || !"".equals(body) || (body != null && body.length() != 0)) {
					returnObject = m.invoke(commonObject, param);
				}
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				log.error(" " + e.getCause());
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				log.error(" " + e.getCause());
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				log.error(" " + e.getCause());
			}
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			log.error(" " + e.getCause());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			log.error(" " + e.getCause());
		}
		log.debug("end privacy Detection" + returnObject);
		return (String) returnObject;
	}
	
	public ServletInputStream getInputStream() throws IOException {
		
		final ByteArrayInputStream bis = new ByteArrayInputStream(b);

		return new ServletInputStreamImpl(bis);
	}

	class ServletInputStreamImpl extends ServletInputStream {
		private InputStream is;

		public ServletInputStreamImpl(InputStream bis) {
			is = bis;
		}

		public int read() throws IOException {
			return is.read();
		}

		public int read(byte[] b) throws IOException {
			return is.read(b);
		}

		@Override
		public boolean isFinished() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean isReady() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void setReadListener(ReadListener listener) {
			// TODO Auto-generated method stub
			
		}
	}
	
	 @Override
	    public String getParameter(String name) {

	        String[] paramArray = getParameterValues(name);

	        if (paramArray != null && paramArray.length > 0) {

	            return paramArray[0];

	        } else {

	            return null;

	        }

	    }

	    @Override
	    public Map<String, String[]> getParameterMap() {

	        return Collections.unmodifiableMap(params);

	    }

	    @Override
	    public Enumeration<String> getParameterNames() {

	        return Collections.enumeration(params.keySet());

	    }

	    @Override
	    public String[] getParameterValues(String name) {

	        String[] result = null;
	        String[] temp = params.get(name);

	        if (temp != null) {

	            result = new String[temp.length];
	            System.arraycopy(temp, 0, result, 0, temp.length);

	        }

	        return result;
	    }

	    public void setParameter(String name, String value) {

	        String[] oneParam = {value};
	        setParameter(name, oneParam);

	    }

	    public void setParameter(String name, String[] values) {

	        params.put(name, values);

	    }
}