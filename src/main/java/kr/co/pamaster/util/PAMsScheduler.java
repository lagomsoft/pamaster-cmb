package kr.co.pamaster.util;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import kr.co.pamaster.common.service.ScheduleService;

@Component
public class PAMsScheduler {
	
	@Autowired
	private ScheduleService scheduleService;
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	
	@Scheduled(fixedDelayString = "${pams.scheduled.time:}")
	public void pamsCronJobSch() {
		
		if(scheduleService != null && !scheduleService.getCheckLocal()) {
			
			List<Map<String, Object>> scheduleList = scheduleService.getScheduleList();
			
			//초기 스케줄 POOL 사이즈를 조회 (1시간마다 재조회)
			scheduleService.settingMultiSchedulePoolSize();
			
			startSchedule(scheduleList);
			
			//스래드가 돌지 않을 경우 보안 이슈 파일이 혹시 존재하면 모두 삭제 처리
			if(scheduleService.getUseMultiScheduleThread() <= 0) {
				scheduleService.clearTempFilepath();
			}
			
		}
		
	}
	
	/**
	 * 배치 멀티 배치 처리
	 * 
	 * @param scheduleList
	 */
	public void startSchedule(List<Map<String, Object>> scheduleList) {
		
		if(scheduleList != null) {
			
			int freeThreadSize = scheduleService.getFreeMultiScheduleThreadCnt();
			
			int scheduleSize = scheduleList.size();
			
			for (int si = 0; si < freeThreadSize && si < scheduleSize; si++) {
				SchedulTask task = new SchedulTask(scheduleList.get(scheduleSize - si - 1), scheduleService);
				Thread thread = new Thread(task);
				thread.start();
				
				scheduleList.remove(scheduleSize - si - 1);
			}
			
			//즉시 요청이 있을 경우 배치 실행
			List<Map<String, Object>> nowScheduleList = scheduleService.getNowScheduleList();
			scheduleList.addAll(nowScheduleList);
			
			if(scheduleList.size() > 0) {
				
				try {
					//1초 지연 처리
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					log.debug("setMultiScheduleThread - sleep Err :" + e.getMessage());
				}
				
				startSchedule(scheduleList);
			}
			
		}
		
			
	}
	
	//@Scheduled(cron = "0 0 12 * * * ")
	public void pamsCronDailySch() {
		if(scheduleService != null && !scheduleService.getCheckLocal()) {
			
			scheduleService.userUsingTimeCheck();
		}
	}
	
	
	//@Scheduled(cron = "0 0 1 * * *")
    public void pamsCronDailyDestructionSch() throws PAMsException {
        if(scheduleService != null && !scheduleService.getCheckLocal()) {
            
            // 개인정보데이터 자동파기
            scheduleService.autoDestructionSchedule();
            
            // 감사이력 자동삭제
            scheduleService.autoDeleteLogSchedule();
        }
    }
   
}
