package kr.co.pamaster.util;

import java.util.Map;

import kr.co.pamaster.common.service.ScheduleService;

public class SchedulTask implements Runnable {
	
	private ScheduleService service;
	private Map<String, Object> scheduleMap;
	
	public SchedulTask(Map<String, Object> scheduleMap, ScheduleService service) {
		this.service = service;
		this.scheduleMap = scheduleMap;
	}
	
	@Override
	public void run() {
		
		if(service != null) {
			service.runScheduleThread(scheduleMap);
		}
		
	}

}
